# -*- coding:utf-8 -*-

import grpc
from google.protobuf import empty_pb2 as _empty

import maum.brain.sds.train.sdstrainer_pb2 as sdstr
from maum.brain.sds.train import sdstrainer_pb2_grpc

class SdsTrainerMonitor(sdstrainer_pb2_grpc.TrainerMonitorServicer):
    sds_trainer = None
    """
    Provide methods that implement SdsTrainer
    """
    def __init__(self, sds_trainer):
        self.sds_trainer = sds_trainer

    def Notify(self, proc_stat, context):
        print 'remote peer', context.peer()
        print proc_stat
        if 'ipv4:127.0.0.1' not in context.peer():
            context.set_code(grpc.StatusCode.PERMISSION_DENIED)
            context.set_details('Local internal use only')
            raise grpc.Error("permission denied")
        self.sds_trainer.update_proc(proc_stat)
        return _empty.Empty()
