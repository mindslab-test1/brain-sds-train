#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import signal
import sys
import traceback

import grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

import argparse
from uuid import UUID

from common.config import Config
from maum.common import lang_pb2
from maum.brain.sds.train import sdstrainer_pb2 as sdstr
from maum.brain.sds.train import sdstrainer_pb2_grpc
from sdstrainer.sds_trainer_helper import TrainerHelper
from sdstrainer.sds_trainer import SdsTrainer

__sds_proc__ = None


class SdstrainProcess:
    uuid = None
    proc = None
    model = None
    proc = None

    def report_stat(self, result, sig_no):
        print "report_stat"
        parent_remote = self.helper.get_parent_endpoint()
        print 'REPORT STAT', parent_remote
        print result
        chan = grpc.insecure_channel(parent_remote)
        stub = sdstrainer_pb2_grpc.TrainerMonitorStub(chan)

        proc_stat = sdstr.TrainProcStatus()
        proc_stat.key = str(self.uuid)
        proc_stat.pid = os.getpid()
        proc_stat.result = result
        proc_stat.sig_no = sig_no
        stub.Notify(proc_stat)

    def _handle_signal(self, signal, frame):
        self.report_stat(sdstr.cancelled, signal)
        sys.exit(0)

    def __init__(self, uuid, model):
        """
        :param uuid: 문자열 uuid
        """
        signal.signal(signal.SIGTERM, self._handle_signal)
        signal.signal(signal.SIGSEGV, self._handle_signal)
        signal.signal(signal.SIGINT, self._handle_signal)
        signal.signal(signal.SIGBUS, self._handle_signal)

        self.uuid = UUID(uuid)
        self.helper = TrainerHelper(self.uuid, model)
        self.model = self.helper.load_model()
        self.init_proc()
        #print self.model

    def calc_model_maximum(self):
        point = 0
        # set db
        point += 5
        # train
        #point += (self.param_dict['epoches']) # TODO 변경하기
        # final
        point += 3
        return point

    def init_proc(self):
        proc = sdstr.SdsTrainStatus()

        proc.result = sdstr.training
        proc.key = str(self.uuid)
        proc.step = sdstr.SDS_TRAIN_START
        proc.model = self.model.name + "_" + self.model.id
        proc.maximum = self.calc_model_maximum()
        proc.value = 0

        proc.started.GetCurrentTime()
        proc.elapsed.seconds = 0
        proc.elapsed.nanos = 0

        self.proc = proc
        self.helper.save_proc(self.proc)

    def train(self):
        """
        main train 함수
        :return:
        """
        trainer = SdsTrainer(self.helper, self.proc, self.model)
        model_name = self.model.name + "_" + self.model.id
        try:
            print ' TRAIN :', self.uuid, 'make new project ------------'
            trainer.make_new_project(self.uuid, self.model)

            print ' TRAIN :', self.uuid, 'save task manager ------------'
            trainer.save_task_manager(self.model)

            print ' TRAIN :', self.uuid, 'save canonical form ------------'
            trainer.save_canonical_form(self.model)

            print ' TRAIN :', self.uuid, 'save scenario ------------'
            trainer.save_scenario(self.model)

            print ' TRAIN :', self.uuid, 'save slot structure ------------'
            trainer.save_slot_structure(self.model)

            print ' TRAIN :', self.uuid, 'save da type ------------'
            trainer.save_da_type(self.model)

            print ' TRAIN :', self.uuid, 'save dialog lib ------------'
            trainer.save_dialog_lib(self.model)

            print ' TRAIN :', self.uuid, 'make learn slu ------------'
            trainer.make_learn_slu()

            print ' TRAIN :', self.uuid, 'exec learn slu ------------'
            trainer.exec_learn_slu()

            print ' TRAIN :', self.uuid, 'exec perl ------------'
            trainer.exec_perl()

            print ' TRAIN :', self.uuid,  'save as tar'
            trainer.save_as_tar(model_name)
            print ' TRAIN :', self.uuid,  'clean'
            trainer.remove_workspace(model_name)
            print ' TRAIN :', self.uuid,  'NOTIFY'
            # report success
            self.proc.step = sdstr.SDS_TRAIN_DONE
            self.helper.save_proc(self.proc)
            self.report_stat(sdstr.success, 0)
        except:
            print '==='
            print sys.exc_type
            traceback.print_exc()
            self.report_stat(sdstr.failed, 0)

if __name__ == '__main__':
    print 'Starting child with ', sys.argv
    conf = Config()
    conf.init('brain-sds-train.conf')

    parser = argparse.ArgumentParser(
        description='sds trainer executor')
    parser.add_argument('-u', '--uuid',
                        nargs='?',
                        dest='uuid',
                        required=True,
                        help='Unique trainer id')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Name')

    args = parser.parse_args()

    print args.uuid, args.model

    # TODO signal handling
    # kill signal stop 처리

    sds_proc = SdstrainProcess(args.uuid, args.model)
    __sds_proc__ = sds_proc
    sds_proc.train()
