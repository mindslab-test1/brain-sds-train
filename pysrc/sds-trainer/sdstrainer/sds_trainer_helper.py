# -*- coding:utf-8 -*-
import fcntl
import os
import sys
import time

from google.protobuf import timestamp_pb2

from common.config import Config
from maum.common import lang_pb2
from maum.brain.sds.train import sdstrainer_pb2 as sdstr


class TrainerHelper:
    conf = Config()
    ws_dir = ''
    dial_path = ''

    def __init__(self, uuid, model):
        print 'HELPER', uuid, model
        self.dial_path = self.conf.get('brain-sds-trainer.resource.path')
        self.ws_dir = self.conf.get('brain-sds-trainer.workspace.dir')
        self.uuid = uuid
        self.model = model
        self.model_file = os.path.join(self.ws_dir, str(self.uuid) + '.model')
        self.proc_file = os.path.join(self.ws_dir, str(self.uuid) + '.proc')
        print self.ws_dir, self.uuid, self.model
        if not os.path.isdir(self.ws_dir):
            os.makedirs(self.ws_dir)

    def get_parent_endpoint(self):
        return '127.0.0.1:' + self.conf.get('brain-sds-trainer.front.port')

    def workspace_path(self):
        p = os.path.join(self.ws_dir, str(self.uuid))
        p += '/'
        if not os.path.isdir(p):
            os.makedirs(p)
        return p

    def get_model_file(self):
        return self.model_file

    def get_proc_file(self):
        return self.proc_file

    def load_model(self):
        try:
            f = open(self.model_file, 'rb')
            sds_model = sdstr.SdsModel()
            sds_model.ParseFromString(f.read())
            f.close()
            return sds_model
        except IOError, e:
            print 'open failed', self.model_file, e.errno, e.strerror
            return None

    def save_model(self, model):
        f = open(self.model_file, 'wb')
        f.write(model.SerializeToString())
        f.close()

    def load_proc(self):
        try:
            f = open(self.proc_file, 'rb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_SH)
            proc = sdstr.SdsTrainStatus()
            proc.ParseFromString(f.read())
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            return proc
        except IOError, e:
            print 'open failed', self.proc_file, e.errno, e.strerror
            return None

    def save_proc(self, proc):
        print "save_proc"
        # update elapsed time
        now = time.time()
        seconds = int(now)
        nanos = int((now - seconds) * 10 ** 9)
        end = timestamp_pb2.Timestamp(seconds=seconds, nanos=nanos)
        proc.elapsed.seconds = end.seconds - proc.started.seconds
        proc.elapsed.nanos = end.nanos - proc.started.nanos
        if proc.elapsed.seconds > 0 and proc.elapsed.nanos < 0:
            proc.elapsed.seconds -= 1
            proc.elapsed.nanos += 1000000000

        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()

    def remove_proc(self):
        if os.path.exists(self.proc_file):
            f = open(self.proc_file, 'wb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            os.remove(self.proc_file)

    def child_command_line(self):
        self.init_proc()
        exe_path = os.path.realpath(sys.argv[0])
        bin_path = os.path.dirname(exe_path)
        exe = os.path.join(bin_path, 'brain-sds-train-proc')
        py = '/usr/bin/python'
        print "exe_path: ", exe_path
        print "bin_path: ", bin_path
        print "exe: ", exe
        return py, [py, exe, '-u', str(self.uuid),
                    '-m', str(self.model)]

    def get_tar_work_dir(self):
        tardir = self.model
        ret = os.path.join(self.workspace_path(), tardir)
        if not os.path.isdir(ret):
            os.makedirs(ret)
        return ret

    def get_tar_filename(self):
        tarfile = str(self.uuid)
        tarfile += '.sdsimage.tar.gz'
        return tarfile

    def get_tar_file(self):
        return os.path.join(self.ws_dir, self.get_tar_filename())

    def init_proc(self):
        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)

        proc = sdstr.SdsTrainStatus()
        proc.result = sdstr.preparing

        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()

