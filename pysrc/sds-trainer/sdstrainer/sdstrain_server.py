# -*- coding:utf-8 -*-
import os

import grpc
from google.protobuf.empty_pb2 import Empty

import maum.brain.sds.train.sdstrainer_pb2 as sdstr
from maum.brain.sds.train import sdstrainer_pb2_grpc
from sdstrainer.sds_trainer_proxy import SdsTrainerProxy
from common.config import Config


class SdsTrainer(sdstrainer_pb2_grpc.SdsTrainerServicer):
    conf = Config()
    trainers = {}
    childs = {}
    """
    Provide methods that implement sdstrainer
    """

    def __init__(self):
        pass

    def Open(self, sds_model, context):
        trainer = SdsTrainerProxy()
        trainer.open(sds_model)

        ret = sdstr.SdsTrainKey()
        ret.train_id = trainer.get_key()
        print 'NEW TRAINER ', trainer.get_key(), ret.train_id
        self.trainers[trainer.get_key()] = trainer
        self.childs[trainer.get_pid()] = trainer
        print self.childs
        print self.trainers
        return ret

    def GetProgress(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            return trainer.get_progress()
        else:
            tar_list = os.listdir(
                self.conf.get('brain-sds-trainer.workspace.dir'))
            file_name = str(key.train_id) + '.sdsimage.tar.gz'
            print 'file_name: ', file_name
            if any(tl in file_name for tl in tar_list):
                print 'train success'
                status = sdstr.SdsTrainStatus()
                status.key = key.train_id
                status.step = 4  # SDS_TRAIN_DONE
                status.result = 1  # success
                return status
            else:
                print 'not found train model'
                context.set_code(grpc.StatusCode.NOT_FOUND)
                context.set_details('train id not found')
                raise grpc.RpcError("not found")

    def GetBinary(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[str(key.train_id)]
            return trainer.get_binary()
        else:
            return SdsTrainerProxy.get_binary_by_key(str(key.train_id), context,
                                                     grpc)

    def RemoveBinary(self, key, context):
        SdsTrainerProxy.remove_binary_by_key(str(key.train_id))
        return Empty()

    def Close(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            ret = trainer.get_progress()
            del self.trainers[key.train_id]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            del self.childs[pid]
            print 'NOW delete proxy trainer', pid, key, trainer
            del trainer
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def Stop(self, key, context):
        id = str(key.train_id)
        resource_dir = self.conf.get('brain-sds-trainer.resource.path')
        trainer = self.trainers[id]
        if trainer != None:
            ret = trainer.get_progress()
            trainer.cancel()
            ret.result = sdstr.cancelled

            model_name = ret.model
            dialog_domain = "dialog_domain"
            dialog_path = os.path.join(resource_dir, dialog_domain)
            resource_model = os.path.join(dialog_path, model_name)
            os.remove(resource_model)

            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def GetAllProgress(self, empty, context):
        ret = sdstr.SdsTrainStatusList()
        res = []
        for tr in self.trainers.itervalues():
            print "===========", tr.get_progress
            res.append(tr.get_progress())
        ret.sds_trains.extend(res)
        return ret

    def update_proc(self, proc_stat):
        """
        child에서 종료할 때, 해당 상태를 정리한다.

        :param proc_stat: proc의 상태 값을 전달한다.
        """
        key = str(proc_stat.key)
        if key in self.trainers:
            trainer = self.trainers[key]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            assert pid == proc_stat.pid
            del self.trainers[key]
            del self.childs[pid]
            # 상태를 기록해준다.
            trainer.set_result(proc_stat.result)
            print 'NOW delete proxy trainer', pid, key, trainer
            del trainer
