# -*- coding:utf-8 -*-
import time

import grpc
from concurrent import futures

from sdstrain_monitor import SdsTrainerMonitor
from sdstrain_server import SdsTrainer
from common.config import Config
from maum.brain.sds.train import sdstrainer_pb2
from maum.brain.sds.train import sdstrainer_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def serve():
    conf = Config()
    conf.init('brain-sds-train.conf')

    svc = SdsTrainer()
    mon = SdsTrainerMonitor(svc)
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("brain-sds-trainer.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("brain-sds-trainer.front.timeout")))
    ]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=data)
    sdstrainer_pb2_grpc.add_SdsTrainerServicer_to_server(svc, server)
    sdstrainer_pb2_grpc.add_TrainerMonitorServicer_to_server(mon, server)

    port = conf.get('brain-sds-trainer.front.port')
    server.add_insecure_port('[::]:' + port)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
