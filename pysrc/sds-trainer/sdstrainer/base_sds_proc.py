# -*- coding:utf-8 -*-
import os
import shutil
import sys
import tarfile

import time

from common.config import Config
from maum.brain.sds.train import sdstrainer_pb2 as sdstr
from model_pickle2txt import load_model, save_model_as_txt

class BaseSdsTrainer:
    conf = Config()
    proc = None
    base = ''
    helper = None
    resource_dir = ''

    """
    SDS 학습기
    """
    def __init__(self, helper, proc):
        self.helper = helper
        self.proc = proc
        self.base = self.conf.get('brain-sds-trainer.workspace.dir')
        self.resource_dir = self.conf.get('brain-sds-trainer.resource.dir')
        if not os.path.isdir(self.base):
            os.makedirs(self.base)
        if self.base[-1] != '/':
            self.base += '/'

    def pickle_to_text(self):
        print self.output, self.best_file
        save_model_as_txt(self.output, load_model(self.best_file))

    def save_as_tar(self, model):
        self.proc.step = sdstr.SDS_TRAIN_DONE
        self.helper.save_proc(self.proc)

        tar_file = self.helper.get_tar_file()
        work_dir = self.helper.get_tar_work_dir()

        print tar_file, work_dir

        self.tar = tar_file
        tgz = tarfile.open(self.tar, 'w:gz')
        print 'TAR', self.tar, os.path.basename(work_dir)
        tgz.add(name=work_dir,
                arcname=os.path.basename(work_dir),
                recursive=True)
        tgz.close()
        self.proc.step = sdstr.SDS_TRAIN_DONE
        self.proc.value += 3
        self.helper.save_proc(self.proc)

    def remove_workspace(self, model_name):
        self.proc.step = sdstr.SDS_TRAIN_CLEAN
        self.helper.save_proc(self.proc)

        data_path = self.helper.workspace_path()
        shutil.rmtree(data_path)
        os.remove(self.helper.get_model_file())

        dialog_domain = "dialog_domain"
        dialog_path = os.path.join(self.resource_dir, dialog_domain)
        resource_model = os.path.join(dialog_path, model_name)
        os.remove(resource_model)

        self.proc.step = sdstr.SDS_TRAIN_CLEAN
        self.proc.value += 5
        self.helper.save_proc(self.proc)

        # os.remove(self.helper.get_proc_file())
        # proc file은 proxy에서 상태를 확인한 이후에 삭제하도록 한다
