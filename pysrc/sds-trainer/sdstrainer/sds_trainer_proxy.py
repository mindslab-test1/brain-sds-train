# -*- coding:utf-8 -*-
import glob
import json
import os
import resource
import signal
import requests
from uuid import uuid4

from google.protobuf.timestamp_pb2 import Timestamp

from sds_trainer_helper import TrainerHelper
from common.config import Config
from maum.brain.sds.train import sdstrainer_pb2 as sdstr

_TUTOR_API_KEY = '7c3a778e29ac522de62c4e22aa90c9'


class SdsTrainerProxy:
    """
    공통 Sds Proxy
    """
    uuid = None
    pid = None
    helper = None
    callback_url = ""
    fake_cnt = 0
    sds_model = sdstr.SdsModel()
    train_result = sdstr.training
    started = Timestamp()

    def __init__(self):
        self.dic = {}
        self.uuid = uuid4()
        self.key = str(self.uuid)
        self.model = None

    def __del__(self):
        if self.key != None:
            self.finish_hook()
            self.key = None

    def run_trainer(self, sds_model):
        self.helper.save_model(sds_model)
        cmd, args = self.helper.child_command_line()
        print 'before exec:', cmd, args
        try:
            os.execv(cmd, args)
        except OSError, e:
            print 'execve failed', cmd, args, e.errno, e.strerror

    def open(self, sds_model):
        #self.callback_url = sds_model.callback_url
        self.model = sds_model.name + "_" + sds_model.id
        """
        실지로 실행하는 학습 프로세스를 실행한다.
        학습 프로세스에서는 학습 실행 결과를 주기적으로 파일에 기록한다.
        빠를 실행 속도를 보장하기 위해서 위와 같이 처리한다.
        파일에 기록할 때 FILE LOCK을 실행한다.
        :param sds_model:
        :return:
        """
        self.helper = TrainerHelper(self.uuid, self.model)
        newpid = os.fork()
        if newpid == 0:
            (cur, max) = resource.getrlimit(resource.RLIMIT_NOFILE)
            try:
                for fd in range(3, cur):
                    os.close(fd)
            except OSError, e:
                pass
            self.run_trainer(sds_model)
        else:
            self.pid = newpid
            print 'fork parent:', self.pid, newpid

    def finish_hook(self):
        # 파일을 삭제한다.        self.helper.remove_proc()
        res = sdstr.SdsTrainResult.Name(self.train_result)
        print 'finish_hook', res, self.train_result
        url = self.callback_url + _TUTOR_API_KEY
        print 'call back url: ', url

        cb_msg = {}
        cb_msg['message'] = 'Sds train job ' + self.key + \
                            ', result:' + res
        cb_msg['data'] = {
            'key': self.key,
            'name': self.model,
            'result': res,
            'binary': self.helper.get_tar_filename()
        }

        body = json.dumps(cb_msg)
        #req = requests.post(url, data=body,
                            #headers={'Content-Type': 'application/json'})

        #print 'RESULT : ', req.text

    def get_key(self):
        return self.key

    def get_pid(self):
        return self.pid

    def get_progress(self):
        proc = self.helper.load_proc()
        try:
            self.train_result = proc.result
        except AttributeError, e:
            print 'can not load proc file.', e.message
        finally:
            # if self.train_result != sdstr.training:
            #     self.helper.remove_proc()
            return proc

    def get_binary(self):
        tar = self.helper.get_tar_file()
        if os.path.exists(tar):
            print "get_binary(if)"
            return self.bytes_from_file(tar)
        else:
            print "get_binary(else)"
            return None

    @staticmethod
    def bytes_from_file(filename, chunksize=1024 * 1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    tar = sdstr.SdsTrainBinary()
                    tar.tar = chunk
                    yield tar
                else:
                    break

    @staticmethod
    def get_binary_by_key(uuid, context, grpc):
        conf = Config()
        ws = conf.get('brain-sds-trainer.workspace.dir')
        find = ws + '/' + uuid + '.sdsimage.tar.gz'
        print "binary file name: ", find
        files = glob.glob(find)
        if len(files) > 0:
            return SdsTrainerProxy.bytes_from_file(filename=files[0])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    @staticmethod
    def remove_binary_by_key(uuid):
        conf = Config()
        ws = conf.get('brain-sds-trainer.workspace.dir')
        find = ws + '/' + uuid + '.sdsimage.tar.gz'
        files = glob.glob(find)
        for f in files:
            print 'remove binary by key ', uuid, f
            os.remove(f)

    def cancel(self):
        proc = self.helper.load_proc()
        if proc.result == sdstr.training:
            os.kill(self.pid, signal.SIGTERM)
            self.train_result = sdstr.cancelled
            proc.result = sdstr.cancelled
            self.helper.save_proc(proc)

    def set_result(self, res):
        print 'SET RESULT', sdstr.SdsTrainResult.Name(res)
        self.train_result = res