# -*- coding:utf-8 -*-
import os

import sqlite3

import maum.brain.sds.train.sdstrainer_pb2 as sdstr
from sdstrainer.base_sds_proc import BaseSdsTrainer
from sdstrainer.sds_trainer_proxy import SdsTrainerProxy
from maum.brain.sds.train import sdstrainer_pb2_grpc
from maum.brain.sds.train import sdstrainer_pb2
from common.config import Config


class SdsTrainer(BaseSdsTrainer):
    conf = Config()
    helper = None
    proc = None
    ws_path = ""
    dial_path = ""
    empty_path = ""
    dialog_domain_path = ""
    project_name = ""
    project_path = ""
    db_path = ""
    conn = ""
    sds_model = sdstr.SdsModel()

    def __init__(self, helper, proc, sds_model):
        BaseSdsTrainer.__init__(self, helper, proc)
        self.sds_model = sds_model
        self.ws_path = self.conf.get('brain-sds-trainer.workspace.dir')
        self.dial_path = self.conf.get('brain-sds-trainer.resource.dir')
        self.empty_path = self.dial_path + "/dialog_domain/empty/"
        self.dialog_domain_path = self.dial_path + "/dialog_domain/"
        self.project_name = sds_model.name + "_" + sds_model.id
        self.project_path = self.dialog_domain_path + self.project_name
        self.db_path = self.project_path + "/" + self.project_name + "_DB.sqlite3"
        print "model information", sds_model

    def make_new_project(self, uuid, model):
        ret = os.path.join(self.ws_path, str(uuid))
        model_name = model.name + "_" + model.id
        # $MAUM_ROOT/workspace/sds-train/[uuid]/[model_name]
        ret2 = os.path.join(ret, model_name)

        if not os.path.isdir(ret2):
            os.makedirs(ret2)

        command = "rsync -av " + self.empty_path + " " + ret2 \
                  + "/ >/dev/null 2>/dev/null"
        print "conmmand: ", command
        os.system(command)

        # $MAUM_ROOT/resources/sds-train/dialog_domain/[model_name]
        src_path = self.dial_path + "/dialog_domain/" + model_name
        print "src_path: ", src_path

        # 학습이 도중에 중단되면 심볼릭 링크가 남아있어 다시 학습시 링크를 걸 때 에러가 발생한다.
        # 링크 삭제
        print "os.path.islink(src_path): ", os.path.islink(src_path)
        if os.path.islink(src_path):
            try:
                print "symbolic link delete success, path: ", src_path
                os.remove(src_path)
            except OSError as ex:
                print "symbolic link delete fail, path: ", src_path
                print ex

        # sds 학습 구조상 dial, dialog_domain/[models]가 같은 경로에 있어야 하기 때문에 링크를 걸어준다.
        try:
            os.symlink(ret2, src_path)
        except OSError as ex:
            print "symbolic link ret2: [", ret2, "]"
            print "symbolic link src_path: [", src_path, "]"
            print "symbolic link fail"
            print ex

        file_list = [".dialogLib.txt",
                     ".slot.txt",
                     ".svm",
                     ".svm.slack.txt",
                     ".task.txt",
                     "_asr.txt",
                     "_DB.sqlite3",
                     "_DB.xls",
                     "_pat.txt",
                     "_tm_asr.txt",
                     "_train.txt",
                     "_word.txt",
                     ".DAtype.txt"]

        for tail in file_list:
            command = "mv " + self.project_path + "/empty" + tail \
                + " " + self.project_path + "/" + self.project_name + tail \
                + " >/dev/null 2>/dev/null"

            os.system(command)

    def save_task_manager(self, model):
        conn = sqlite3.connect(self.db_path)
        query_list = []
        sql = ""
        next_task_query = ""
        idx = 2

        with conn:
            cur = conn.cursor()
            for task in model.tasks:
                # 현재 task와 연결된 다음 task들의 정보
                if not task.next_tasks:
                    next_task_query = ""
                else:
                    for next_task in task.next_tasks:
                        query_list.append("<next_task_item>\n<task_name>")
                        # 다음 task의 이름
                        query_list.append(next_task.name)
                        query_list.append("</task_name>\n<condition>")
                        # 다음 task 진입 조건
                        query_list.append(next_task.condition)
                        query_list.append("</condition>\n<controller>")
                        query_list.append(
                            sdstrainer_pb2.Source.Name(next_task.controller))
                        query_list.append("</controller>\n</next_task_item>\n")
                        next_task_query = str(''.join(query_list))
                        del query_list[:]

                # 현재 task 정보 저장
                if task.name == "Greet":
                    query_list.append("UPDATE task_manager set ")
                    query_list.append("\"task-task_goal\"='")
                    query_list.append(task.goal + "'")
                    query_list.append(", \"task-next_task\"='")
                    query_list.append(next_task_query)
                    query_list.append("' WHERE \"task-task_name\"='Greet';\n")
                else:
                    if task.reset_slot:
                        reset_slot = "yes"
                    else:
                        reset_slot = "no"
                        query_list.append("INSERT INTO task_manager ")
                        query_list.append("(\"task-_idx\", \"task-reset_slot\"")
                        query_list.append(
                            ", \"task-task_name\", \"task-task_type\"")
                        query_list.append(
                            ", \"task-task_goal\", \"task-next_task\")")
                        query_list.append(
                            " VALUES ('" + str(idx) + "', '" + reset_slot)
                        # 현재 task 이름
                        query_list.append("', '" + task.name)
                        query_list.append("', '")
                        query_list.append(sdstrainer_pb2.TaskType.Name(task.type))
                        # 현재 task 종료 조건
                        query_list.append("', '" + task.goal)
                        # 다음 task 정보
                        query_list.append("', '" + next_task_query)
                        query_list.append("');\n")

                    idx = idx + 1

                sql += ''.join(query_list)
                del query_list[:]

            print("task_manager sqlite: {}", sql.encode('utf-8'))
            cur.executescript(sql)

    def save_canonical_form(self, model):
        conn = sqlite3.connect(self.db_path)

        sql = ""

        with conn:
            cur = conn.cursor()
            for entity in model.entities:
                if entity.entity_datas is None:
                    continue

                sql = self.make_entity_data(entity.name, entity.entity_datas)

            cur.executescript(sql)

    @staticmethod
    def make_entity_data(entity_name, entity_datas):
        query_list = []
        sql = ""
        count = 1
        for ed in entity_datas:
            for synonym in ed.synonym_datas:
                if synonym is None:
                    continue

                count = count + 1
                query_list.append("INSERT INTO canonical_form "
                                  "(_idx, slot_from, slot_to)")
                query_list.append(" VALUES ('")
                query_list.append(str(count) + "', '")
                query_list.append(entity_name + "=\"")
                query_list.append(synonym.synonym + "\"")
                query_list.append("', '" + entity_name)
                query_list.append("=\"" + ed.entity_data)
                query_list.append("\"" + "');\n")

                sql += ''.join(query_list)
                del query_list[:]
        return sql

    def save_scenario(self, model):
        conn = sqlite3.connect(self.db_path)
        query_list = []
        sql = ""
        count = 1

        with conn:
            cur = conn.cursor()
            for intent in model.intents:
                if intent.intent_utters is None:
                    continue

                for intent_utter in intent.intent_utters:
                    utter_data = intent_utter.intent_utter_datas
                    if utter_data is None:
                        da = "# " + intent.name + "()"
                    else:
                        da = "# " + intent.name + "("
                        # 학습 문장에서 태깅된 슬롯과 값 정보 저장
                        dialog_action = self.make_dialog_action(utter_data)
                        da += dialog_action + ")"

                    count += 1
                    query_list.append("INSERT INTO scenario ")
                    query_list.append(
                        "(_idx, utter, slot_tagged, dialog_action)")
                    query_list.append(" VALUES ('" + str(count) + "', '")
                    query_list.append(intent_utter.user_say + "', '")
                    query_list.append(intent_utter.mapped_user_say)
                    query_list.append("', '" + da + "');\n")

                    sql += ''.join(query_list)
                    del query_list[:]

            cur.executescript(sql)

    @staticmethod
    def make_dialog_action(utter_data):
        dialog_action = ""
        if len(utter_data) > 0:
            final_utter_data \
                = utter_data[len(utter_data) - 1]

            for intent_utter_data in utter_data:
                mapped_entity = intent_utter_data.mapped_entity.name
                mapped_value = intent_utter_data.mapped_value

                if intent_utter_data is final_utter_data:
                    dialog_action += intent_utter_data.mapped_entity.name + "=\"" \
                        + intent_utter_data.mapped_value + "\""
                else:
                    dialog_action += mapped_entity + "=\"" \
                        + mapped_value + "\", "

        return dialog_action

    def save_slot_structure(self, model):
        conn = sqlite3.connect(self.db_path)
        query_list = []
        sql = ""
        slot_idx = 1
        class_idx = 1

        entity_name = ""
        class_name = model.entities[0].class_name

        with conn:
            cur = conn.cursor()
            if class_name != "new_domain":
                query_list.append("INSERT INTO slot_structure (")
                query_list.append(
                    "\"class-name\", \"class-source\", \"class-_idx\") ")
                query_list.append("VALUES ('" + class_name)
                query_list.append("', 'system', '" + str(class_idx))
                query_list.append("');\n")

                class_idx += 1
                sql += ''.join(query_list)
                del query_list[:]

            final_entity = model.entities[len(model.entities) - 1]

            if model.entities is not None:
                for entity in model.entities:
                    query_list.append("INSERT INTO slot_structure ")
                    query_list.append("(\"slot-name\", \"slot-source\"")
                    query_list.append(", \"slot-value_define\", ")
                    query_list.append("\"slot-preceding_slots\", \"slot-_idx\", ")
                    query_list.append("\"slot-_position\", \"slot-type\")")
                    query_list.append(" VALUES ('" + entity.name)
                    query_list.append(
                        "', '" + sdstrainer_pb2.Source.Name(entity.source))
                    query_list.append("', '<define>\n<condition></condition>\n")
                    query_list.append("<target_slot></target_slot>\n")
                    query_list.append("<action></action>\n</define>'")
                    query_list.append(", '\n', '" + str(slot_idx) + "', '\n', '")
                    query_list.append(sdstrainer_pb2.EntityType.Name(entity.type))
                    query_list.append("');\n")

                    slot_idx += 1
                    sql += ''.join(query_list)
                    del query_list[:]

                    entity_name += entity.name + "\n"

                    if entity == final_entity:
                        query_list.append(
                                "UPDATE slot_structure set \"class-slots\"")
                        query_list.append("='" + entity_name + "'")
                        query_list.append(" WHERE \"class-name\"='")
                        query_list.append(class_name + "';")

                        sql += ''.join(query_list)
                        del query_list[:]
                    elif class_name != entity.class_name:
                        query_list.append(
                                "UPDATE slot_structure set \"class-slots\"")
                        query_list.append("='" + entity_name + "'")
                        query_list.append(" WHERE \"class-name\"='")
                        query_list.append(class_name + "';")

                        sql += ''.join(query_list)
                        del query_list[:]

                        class_name = entity.class_name

                        query_list.append("INSERT INTO slot_structure (")
                        query_list.append("\"class-name\", \"class-source\"")
                        query_list.append(", \"class-_idx\") ")
                        query_list.append("VALUES ('" + class_name)
                        query_list.append("', 'system', '" + str(class_idx))
                        query_list.append("');\n")

                        class_idx += 1
                        sql += ''.join(query_list)
                        del query_list[:]

            cur.executescript(sql)

    def save_da_type(self, model):
        conn = sqlite3.connect(self.db_path)
        query_list = []
        sql = ""
        count = 1

        with conn:
            cur = conn.cursor()
            if model.intents is not None:
                for intent in model.intents:
                    count += 1
                    query_list.append("INSERT INTO da_type (\"define-DA_type\"")
                    query_list.append(", \"define-_idx\", \"define-DA_class\")")
                    query_list.append(" SELECT '" + intent.name)
                    query_list.append("', '" + str(count) + "', '")
                    query_list.append(intent.intent_class + "' ")
                    query_list.append(
                        "WHERE NOT EXISTS (SELECT * FROM da_type WHERE")
                    query_list.append(" \"define-DA_type\"='")
                    query_list.append(intent.name + "');\n")

                    sql += ''.join(query_list)
                    del query_list[:]

                cur.executescript(sql)

    def save_dialog_lib(self, model):
        conn = sqlite3.connect(self.db_path)
        query_list = []
        sql = ""
        req_utter = ""
        utter_set = ""
        count = 1

        with conn:
            cur = conn.cursor()
            if model.tasks is not None:
                for task in model.tasks:
                    # start_event (transaction)
                    # start event가 존재하지 않으면 continue
                    utterance_set = ""
                    if task.start_events is None:
                        continue
                    else:
                        for start_event in task.start_events:
                            # 시스템이 응답할 조건이 존재하지 않으면 continue
                            if start_event.condition is None:
                                continue
                            else:
                                utterance = self.make_utterance_xml(self,
                                                                    start_event)
                                utterance_set += utterance

                    count += 1
                    if utterance_set != "":
                        query_list.append("INSERT INTO dialog_lib ")
                        query_list.append("(\"dialog_node-talker\", ")
                        query_list.append("\"dialog_node-_idx\", ")
                        query_list.append("\"dialog_node-utterance_set\", ")
                        query_list.append("\"dialog_node-task\", ")
                        query_list.append("\"dialog_node-subtype\", ")
                        query_list.append("\"dialog_node-dialog_type\")")
                        query_list.append(" VALUES ('system', '")
                        query_list.append(str(count) + "', '" + utterance_set)
                        query_list.append("', '" + task.name)
                        query_list.append("', 'start', 'transaction');")

                        sql += ''.join(query_list)
                        del query_list[:]

                    # event (response)
                    # event가 존재하지 않으면 continue
                    utterance_set = ""
                    if task.events is None:
                        continue
                    else:
                        for event in task.events:
                            # 시스템이 응답할 조건이 존재하지 않으면 continue
                            if event.event_conditions is None:
                                continue
                            else:
                                req_utter = "<DA>" + event.user_intent.name

                                if event.extra_data is None:
                                    req_utter += "()</DA>\n"
                                else:
                                    req_utter += "(" \
                                                 + event.extra_data \
                                                 + ")</DA>\n"
                                req_utter += "<talker>user</talker>\n"
                                req_utter += "<condition>1</condition>\n"

                                for event_condition in event.event_conditions:
                                    utterance = self.make_utterance_xml(self, event_condition)
                                    utterance_set += utterance

                    if utterance_set != "":
                        query_list.append("INSERT INTO dialog_lib ")
                        query_list.append("(\"dialog_node-talker\", ")
                        query_list.append("\"dialog_node-request_utterance\", ")
                        query_list.append("\"dialog_node-_idx\", ")
                        query_list.append("\"dialog_node-utterance_set\", ")
                        query_list.append("\"dialog_node-subtype\", ")
                        query_list.append("\"dialog_node-dialog_type\", ")
                        query_list.append("\"dialog_node-related_task\")")
                        query_list.append(" VALUES ('system', '" + req_utter)
                        query_list.append("', '" + str(count) + "', '" + utter_set)
                        query_list.append("', 'start', 'response', '")
                        query_list.append(task.name + "');")

                        sql += ''.join(query_list)
                        del query_list[:]

                # sql문이 있는 경우에만 실행
                if sql:
                    cur.executescript(sql)

    @staticmethod
    def make_utterance_xml(self, event):
        xml = "<utterance>\n<condition>" + event.condition \
            + "</condition>\n<action>" + event.action \
            + "</action>\n<template>\n" + "<weight>1.0</weight>\n"

        intention_xml = self.make_intention_xml(event.utters)
        xml += intention_xml + "</template>\n</utterance>\n"

        return xml

    @staticmethod
    def make_intention_xml(utters):
        xml = ""
        for utter in utters:
            xml = "<intention>\n<DA>" + utter.system_intent.name

            if utter.extra_data is None:
                xml += "()</DA>\n"
            else:
                xml += "(" + utter.extra_data + ")</DA>\n"

            for utter_data in utter.utters:
                xml += "<pattern>" + utter_data.utter + "</pattern>\n"

            xml += "</intention>"

        return xml

    def make_learn_slu(self):
        train_file_path = self.project_path + "/" \
            + self.project_name + ".txt"
        asr_train_file_path = self.project_path + "/" \
            + self.project_name + ".all.asr.txt"
        shell_path = self.project_path + "/" + "learn_slu.sh"

        try:
            learn_slu = open(shell_path, 'w')

            learn_slu.write("#! /bin/bash\n")

            learn_slu.write("sync\n")

            tmp = "sqlite3 " + self.db_path \
                + " \"SELECT utter FROM scenario\" " \
                + "| iconv -c -f utf8 -t cp949 > " \
                + asr_train_file_path + "\n"
            learn_slu.write(tmp)
            learn_slu.write("sync\n\n")

            tmp = "cd " + self.dialog_domain_path + "\n"
            learn_slu.write(tmp)

            tmp = "sqlite3 " + self.db_path \
                + " \"SELECT utter, slot_tagged, dialog_action " \
                + "FROM scenario\" | tr '|' '\\t' " \
                + "| iconv -c -f utf8 -t cp949 > " \
                + train_file_path + "\n"
            learn_slu.write(tmp)
            learn_slu.write("sync\n")

            tmp = self.dial_path + "/dial -cmd LEARN_SLU -dic " \
                + self.dial_path + "/ -domain " + self.project_name \
                + " -fn " + self.project_path \
                + "/" + self.project_name + ".txt\n"
            learn_slu.write(tmp)
            learn_slu.write("sync\n")

            tmp = "perl " + self.dial_path \
                + "/bin/update_slot_freq.pl -quite -fn " \
                + self.project_path + "/" \
                + self.project_name + "_DB.sqlite3\n"
            learn_slu.write(tmp)
            learn_slu.write("sync\n")

            learn_slu.write("echo 'FINISHED'")

            learn_slu.close()
        except Exception as err:
            print "error exception: ", err

    def exec_learn_slu(self):
        shell_path = self.project_path + "/" + "learn_slu.sh "

        command = "chmod +x " + shell_path
        os.system(command)

        command = shell_path + " " \
            + self.project_name + " >" \
            + self.project_path + "/learn_slu.log 2>&1"

        os.system(command)

    def exec_perl(self):
        exec_script = "perl " + self.dial_path + "/bin/db2xml.pl" \
            + " -fn " + self.db_path

        # make *.task.txt
        command = exec_script + " -table_name task_manager" \
            + " | iconv -c -f utf8 -t cp949 > " \
            + self.project_path + "/" + self.project_name + ".task.txt"
        os.system(command)

        # make *.slot.txt
        command = exec_script + " -table_name slot_structure" \
            + " | iconv -c -f utf8 -t cp949 > " \
            + self.project_path + "/" + self.project_name + ".slot.txt"
        os.system(command)

        # make *.DAtype.txt
        command = exec_script + " -table_name da_type" \
            + " | iconv -c -f utf8 -t cp949 > " \
            + self.project_path + "/" + self.project_name + ".DAtype.txt"
        os.system(command)

        # make *.dialogLib.txt
        command = exec_script + " -table_name dialog_lib" \
            + " | iconv -c -f utf8 -t cp949 > " \
            + self.project_path + "/" + self.project_name + ".dialogLib.txt"
        os.system(command)
