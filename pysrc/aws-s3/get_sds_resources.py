#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import boto3
import shutil


class SdsResource:
    def __init__(self):
        # s3 bucket name
        self.bucket = "private.file.mindslab"
        self.s3 = boto3.client('s3')
        try:
            self.deploy = os.environ['MAUM_BUILD_DEPLOY'] == 'true'
            self.repo_root = os.environ['REPO_ROOT']
            self.outdir = os.path.join(self.repo_root, 'out')
            if not os.path.exists(self.outdir):
                os.makedirs(self.outdir)
        except:
            self.deploy = False
            self.repo_root = ''
            self.outdur = ''

    def touch_file(self, path):
        basedir = os.path.dirname(path)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        with open(path, 'a'):
            os.utime(path, None)

    def get_resources(self, target, version, tar_proc_dir, down_dir):
        if not os.path.isdir(down_dir):
            if os.path.isfile(down_dir):
                os.remove(down_dir)
            os.makedirs(down_dir)

        if not os.path.isdir(tar_proc_dir):
            if os.path.isfile(tar_proc_dir):
                os.remove(tar_proc_dir)
            os.makedirs(tar_proc_dir)
        touch_file = os.path.join(down_dir, '.' + target + '.' + version + '.extracted')
        down_file = 'minds-res-' + target + '-' + version + '.tar.gz'
        down_obj = 'resources/' + down_file
        down_tar = os.path.join(down_dir, down_file)
        if not os.path.isfile(down_tar):
            print 'Downloading', target, 'resource...'
            self.s3.download_file(self.bucket, down_obj, down_tar)
        if not self.deploy:
            tar_target = os.path.join(tar_proc_dir, 'resources', target)
            tar_target_file = os.path.join(tar_target, '.version')
            if os.path.isfile(tar_target_file):
                print target, 'resources already exist at', tar_target
                return
            print 'Unpacking', target, 'resources to', tar_proc_dir
            params = ['tar', 'xvf', down_tar, '-C', tar_proc_dir]
            cmd = ' '.join(params)
            os.system(cmd)
            print 'Done!'
            # self.touch_file(touch_file)
        else:
            repo_tar = os.path.join(self.outdir, down_file)
            if not os.path.isfile(repo_tar):
                shutil.copyfile(down_tar, repo_tar)
                print target, 'is copied as', repo_tar
                # self.touch_file(touch_file)


if __name__ == '__main__':
    receiver = SdsResource()
    receiver.get_resources(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
