#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import grpc
import argparse

import xlrd
import json
from common.config import Config
from util import JsonPrinter

from google.protobuf import json_format
from google.protobuf import text_format
from maum.brain.sds.train import sdstrainer_pb2
from maum.brain.sds.train import sdstrainer_pb2_grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)


class ExcelToJson:
    file_name = None
    json_printer = JsonPrinter()
    xls_file = None
    model = None
    task = None
    intent = None

    def __init__(self, file_name):
        self.file_name = file_name
        self.json_printer = JsonPrinter()
        self.xls_file = xlrd.open_workbook(self.file_name)
        self.model = sdstrainer_pb2.SdsModel()
        self.task = sdstrainer_pb2.Task()
        self.intent = sdstrainer_pb2.Intent()

        end_path = self.file_name.rfind("/")
        self.model.name = self.file_name[end_path + 1: -7]

    def set_da_type(self):
        da_type = self.xls_file.sheet_by_name('da_type')

        rows = da_type.nrows

        for rn in range(rows):
            # colume명, colume의 데이터 타입 건너뛰기
            if rn < 3:
                continue

            self.intent.name = da_type.cell(rn, 1).value
            self.intent.intent_class = da_type.cell(rn, 3).value

            # print intent
            self.model.intents.extend([self.intent])

    def set_task_manager(self):
        task_manager = self.xls_file.sheet_by_name('task_manager')

        rows = task_manager.nrows

        for rn in range(rows):
            # colume명, colume의 데이터 타입 건너뛰기
            if rn < 3:
                continue

            task = sdstrainer_pb2.Task()

            if task_manager.cell(rn, 3).value == "yes":
                task.reset_slot = True
            else:
                task.reset_slot = False

            task.name = task_manager.cell(rn, 4).value
            value = task_manager.cell(rn, 6).value
            task.type = sdstrainer_pb2.TaskType.Value(value)

            # task goal이 1인경우
            if type(task_manager.cell(rn, 7).value) is float:
                tmp = task_manager.cell(rn, 7).value
                task.goal = str(tmp)
            else:
                task.goal = task_manager.cell(rn, 7).value

            next_task_xml = task_manager.cell(rn, 8).value

            if next_task_xml.find("</next_task_item>") == -1:
                continue

            n_task_list = next_task_xml.split("</next_task_item>")
            for t in n_task_list:
                if t.find("task_name") == -1:
                    continue

                next_task = sdstrainer_pb2.NextTask()

                # get next_task name
                s_pos = t.find("<task_name>")
                e_pos = t.rfind("</task_name>")
                next_task.name = t[s_pos + 11:e_pos]

                # get next_task condition
                s_pos = t.find("<condition>")
                e_pos = t.rfind("</condition>")
                next_task.condition = t[s_pos + 11:e_pos]

                # get next_task controller
                s_pos = t.find("<controller>")
                e_pos = t.rfind("</controller>")
                controller = t[s_pos + 12:e_pos].upper()
                next_task.controller = sdstrainer_pb2.Source.Value(controller)

                task.next_tasks.extend([next_task])

            self.model.tasks.extend([task])

        # print json_format.MessageToJson(model, True, True)
        # print "\n\n\n"

    def set_scenario(self):
        scenario = self.xls_file.sheet_by_name('scenario')

        rows = scenario.nrows

        intent = sdstrainer_pb2.Intent()
        utter_data = sdstrainer_pb2.IntentUtterData()
        for rn in range(rows):
            if rn < 3:
                continue

            utter = sdstrainer_pb2.IntentUtter()
            utter.user_say = scenario.cell(rn, 2).value

            utter.mapped_user_say = scenario.cell(rn, 3).value

            dialog_action = scenario.cell(rn, 6).value

            s_pos = dialog_action.find("(")
            e_pos = dialog_action.rfind(")")
            mapping_data = dialog_action[s_pos + 1:e_pos]
            if "=" in mapping_data:
                if "," in mapping_data:
                    mapping_data_list = mapping_data.split(',')
                    for md in mapping_data_list:
                        utter_data.mapped_entity.name = ""
                        utter_data.mapped_value = ""
                        if "=" in md:
                            utter_data.mapped_entity.name = md.split('=')[0]
                            utter_data.mapped_value = md.split('=')[1][1:-1]
                        else:
                            utter_data.mapped_entity.name = md
                        utter.intent_utter_datas.extend([utter_data])
                else:
                    utter_data.mapped_entity.name = ""
                    utter_data.mapped_value = ""
                    if "=" in mapping_data:
                        utter_data.mapped_entity.name = mapping_data.split('=')[
                            0]
                        utter_data.mapped_value = mapping_data.split('=')[1][
                                                  1:-1]
                    else:
                        utter_data.mapped_entity = mapping_data
                    utter.intent_utter_datas.extend([utter_data])

            intent.intent_utters.extend([utter])

        self.model.intents.extend([intent])

    def set_slot_structure(self):
        slot_structure = self.xls_file.sheet_by_name('slot_structure')
        # 줄 수 가져오기
        rows = slot_structure.nrows 

        for rn in range(rows):
            # colume명, colume의 데이터 타입 건너뛰기
            if rn < 3:
                continue

            entity = sdstrainer_pb2.Entity()
            # '4'번째는 entity class 정보가 있는 row
            if type(slot_structure.cell(rn, 4).value) is not str:
                class_name = slot_structure.cell(rn, 4).value
                # class에 속한 entity들의 리스트 "\n"문자열을 기준으로 복수개 정의되어 있음
                entity_list = []
                if type(slot_structure.cell(rn, 14).value) is not str:
                    entity_list = slot_structure.cell(rn, 14).value.split("\n")

                # slot들이 클래스에 속한 순서대로 있지 않을 수 있기 때문에 list와 slot이름 비교
                for s_name in entity_list:
                    for rn2 in range(rows):
                        if s_name == slot_structure.cell(rn2, 0).value:
                            entity.name = s_name

                            value = slot_structure.cell(rn2, 1).value.encode(
                                "UTF-8").upper().strip()
                            # value가 비었을 경우는 pass 
                            if value != "":
                                entity.source = sdstrainer_pb2.Source.Value(value)
                            else:
                                print "slot_source value is empty"
                            value = slot_structure.cell(rn2, 13).value.encode(
                                "UTF-8").upper()
                            # value가 비었을 경우는 pass
                            if value != "":
                                entity.type = sdstrainer_pb2.EntityType.Value(value)
                            else:
                                print "EntityType value is empty"
                            entity.class_name = class_name

                            value = slot_structure.cell(rn, 5).value.encode(
                                "UTF-8").upper()

                            if value.strip() != "":
                                entity.class_source = sdstrainer_pb2.Source\
                                    .Value(value)

                            self.model.entities.extend([entity])

    def set_canonical_form(self):
        canonical_form = self.xls_file.sheet_by_name('canonical_form')

        rows = canonical_form.nrows

        for me in self.model.entities:
            for rn in range(rows):
                if canonical_form.cell(rn, 3).value.find(","):
                    tmp_list = canonical_form.cell(rn, 3).value.split(",")
                    entity_from_list = tmp_list[0].split("=")
                else:
                    entity_from_list = canonical_form.cell(rn, 3).value.split("=")
                entity_to_list = canonical_form.cell(rn, 4).value.split("=")
                if me.name == entity_to_list[0]:
                    self.make_synonym_data(me, entity_from_list, entity_to_list)

    @staticmethod
    def make_synonym_data(me, entity_from_list, entity_to_list):
        med_check_flag = -1
        for med in me.entity_datas:
            if med.entity_data == entity_to_list[1][1:-1]:
                if med_check_flag == -1:
                    med_check_flag = 0

                synonym_data = sdstrainer_pb2.SynonymData()
                synonym_data.synonym = entity_from_list[1][1:-1].encode("UTF-8")
                med.synonym_datas.extend([synonym_data])
        if med_check_flag == -1:
            entity_data = sdstrainer_pb2.EntityData()
            entity_data.entity_data = entity_to_list[1][1:-1].encode("UTF-8")

            synonym_data = sdstrainer_pb2.SynonymData()
            synonym_data.synonym = entity_from_list[1][1:-1].encode("UTF-8")

            entity_data.synonym_datas.extend([synonym_data])
            me.entity_datas.extend([entity_data])

    def set_dialog_lib(self):
        dialog_lib = self.xls_file.sheet_by_name('dialog_lib')

        rows = dialog_lib.nrows

        for task in self.model.tasks:
            for rn in range(rows):
                if (task.name == dialog_lib.cell(rn, 5).value) \
                        or (task.name == dialog_lib.cell(rn, 10).value):

                    # start event
                    if task.name == dialog_lib.cell(rn, 5).value:
                        utter_xml = dialog_lib.cell(rn, 4).value

                        start_event = self.make_utter_data(utter_xml, "start")
                        task.start_events.extend([start_event])

                    # event
                    elif task.name == dialog_lib.cell(rn, 10).value:
                        event = sdstrainer_pb2.Event()

                        user_intent_xml = dialog_lib.cell(rn, 2).value
                        s_pos = user_intent_xml.find("<DA>")
                        m_pos = user_intent_xml.find("(")
                        e_pos = user_intent_xml.find(")")
                        event.user_intent.name = user_intent_xml[s_pos + 4: m_pos]
                        event.extra_data = user_intent_xml[m_pos + 1: e_pos]

                        utter_xml = dialog_lib.cell(rn, 4).value
                        event_condition = self.make_utter_data(utter_xml, "event")

                        event.event_conditions.extend([event_condition])
                        task.events.extend([event])

    @staticmethod
    def make_utter_data(utter_xml, event_type):
        if event_type is "start":
            event = sdstrainer_pb2.StartEvent()
        elif event_type is "event":
            event = sdstrainer_pb2.EventCondition()

        if utter_xml.find("</utterance>") != -1:
            utter_xml_list = utter_xml.split("</utterance>")

            for uxl in utter_xml_list:
                if uxl.find("<condition>") == -1:
                    continue

                s_pos = uxl.find("<condition>")
                e_pos = uxl.find("</condition>")
                event.condition = uxl[s_pos + 11: e_pos]

                s_pos = uxl.find("<action>")
                e_pos = uxl.find("</action>")
                if e_pos - s_pos != 8:
                    event.action = uxl[s_pos + 8: e_pos]

                intention_list = uxl.split("</intention>")

                for intention in intention_list:
                    if intention.find("<DA>") == -1:
                        continue
                    utter = sdstrainer_pb2.Utter()
                    s_pos = intention.find("<DA>")
                    m_pos = intention.find("(", s_pos)
                    e_pos = intention.find(")", s_pos)
                    utter.system_intent.name = intention[s_pos + 4: m_pos]
                    if e_pos - m_pos != 1:
                        utter.extra_data = intention[m_pos + 1: e_pos]

                    pattern_list = intention.split("</pattern>")
                    for pattern in pattern_list:
                        if pattern.find("<pattern>") == -1:
                            continue
                        utter_data = sdstrainer_pb2.UtterData()
                        s_pos = pattern.find("<pattern>")
                        utter_data.utter = pattern[s_pos + 9:]

                        utter.utters.extend([utter_data])

                    event.utters.extend([utter])

        return event

    def print_to_json(self):
        json_text = json_format.MessageToJson(self.model, True, True, 2)
        data = json.loads(json_text)
        json_data = self.json_printer.pformat(data)
        try: 
            if not(os.path.isdir(conf.get('brain-sds-trainer.json.dir'))):
                os.makedirs(os.path.join(conf.get('brain-sds-trainer.json.dir')))
        except OSError as e:
            if e.errno != errno.EEXIST:
                print ("Failed to create directory!!!!!!")
                raise
        json_path = conf.get('brain-sds-trainer.json.dir') + "/" \
                    + self.model.name + ".json"
        print json_path
        f = open(json_path, 'w')
        f.write(json_data)
        f.close()

def model_train(model):
    remote = 'localhost:' + conf.get('brain-sds-trainer.front.port')
    print(remote)
    channel = grpc.insecure_channel(remote)
    stub = sdstrainer_pb2_grpc.SdsTrainerStub(channel)
    stub.Open(model)

def serve():
    parser = argparse.ArgumentParser(description='convert exel to json')
    parser.add_argument('-f', '--filename',
                        nargs='?',
                        dest='filename',
                        required=True,
                        type=str,
                        help='input file name for json export')

    parser.add_argument('-t', '--type',
                        nargs='?',
                        dest='type',
                        required=True,
                        type=str,
                        help='import or export')

    args = parser.parse_args()

    if args.type == "export":
        exel_to_json = ExcelToJson(args.filename)
        exel_to_json.set_da_type()
        exel_to_json.set_task_manager()
        exel_to_json.set_scenario()
        exel_to_json.set_slot_structure()
        exel_to_json.set_canonical_form()
        exel_to_json.set_dialog_lib()

        # exel_to_json.json_print()
        exel_to_json.print_to_json()
    elif args.type == "import":
        f = open(args.filename, 'r')
        json = f.read()
        message = sdstrainer_pb2.SdsModel()
        json_format.Parse(json, message, True)
        #print text_format.MessageToString(message, True)
        model_train(message)

if __name__ == '__main__':
    conf = Config()
    conf.init('brain-sds-train.conf')
    serve()
