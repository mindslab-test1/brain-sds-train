#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys

import grpc
import time
from google.protobuf import json_format
from google.protobuf.empty_pb2 import Empty
from maum.brain.sds.train import trainer_pb2
from maum.brain.sds.train import trainer_pb2_grpc
from common.config import Config

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)


class SdstrainerClient:
    conf = Config()
    stub = None
    key = ""

    def init_channel(self):
        remote = '127.0.0.1:' + self.conf.get('brain-sds-trainer.listen.port')
        print(remote)
        channel = grpc.insecure_channel(remote)
        self.stub = trainer_pb2_grpc.SdsTrainerStub(channel)

    def open(self, train_model):
        print("********** open **********")
        key = self.stub.Open(train_model)
        self.key = key
        json_ret = json_format.MessageToJson(key, True)
        print(json_ret)

    def get_progress(self):
        print("********** get_progress **********")
        print(self.key)
        stat = self.stub.GetProgress(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        # print(json_ret)
        print('model '+stat.model)
        print('step  '+trainer_pb2.SdsTrainStep.Name(stat.step))
        print('result '+trainer_pb2.SdsTrainResult.Name(stat.result))
        print('action '+trainer_pb2.SdsTrainAction.Name(stat.action))
        return stat

    def get_all_progress(self):
        print("********** get_all_progress **********")
        stat_list = self.stub.GetAllProgress(Empty())
        json_list = json_format.MessageToJson(stat_list, True)
        # print json_list
        for stat in stat_list.sds_trains:
            print('model  : ' + stat.model)
            print('result : ' + trainer_pb2.SdsTrainResult.Name(stat.result) + '\n')
        return stat_list

    def close(self):
        print("********** close **********")
        stat = self.stub.Close(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print(json_ret)
        return stat

    def stop(self):
        print("********** stop **********")
        stat = self.stub.Stop(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print(json_ret)
        return stat

    def get_binary(self, model):
        print("********** get_binary **********")
        result = self.stub.GetBinary(model)

        i = 0;
        total_size = 0;
        for binary in result :
            i += 1
            total_size += len(binary.tar)
            print i, '` th total byte:', total_size

        return result

    def remove_binary(self, model):
        print("********** remove_binary **********")
        result = self.stub.RemoveBinary(model)
        print(result)
        return result

    def keyset2(self):
        key = trainer_pb2.SdsTrainKey()
        key.id = 'b70f8ec1-9fad-48ab-a1ef-a09d0c91e9f9'
        self.key = key


def test_sdstrainer(train_model):
    client = SdstrainerClient()

    client.init_channel()
    client.open(train_model)

    time.sleep(1)
    client.stop()

    while True:
        try:
            proc = client.get_progress()
            if proc.result == trainer_pb2.TRAINING:
                time.sleep(1)
            else:
                break
        except:
            print(sys.exc_info())
            break


    # # client.close()
    # client.get_binary(train_model.model)
    # client.remove_binary(train_model.model)

    client.get_all_progress()
#    time.sleep(5)
#client.remove_binary()

def get_binary(model):
    client = SdstrainerClient()
    tar = client.get_binary(model)
    print tar

def make_kor_model():
    model = trainer_pb2.SdsModel()
    model.name = 'test1'
    model.id = '123456789'
    # TODO, specify call back url
    # model.callback_url = 'http://127.0.0.1:9921/api/test/'

    entity_data = trainer_pb2.EntityData()
    entity_data.entity_value = '서울시'
    entity_data.synonyms.extend(['서울', '서울특별시'])

    entity1 = trainer_pb2.Entity()
    entity1.name = "city"
    entity1.entity_datas.extend([entity_data])
    entity1.source = trainer_pb2.KB
    entity1.type = trainer_pb2.STRING_TYPE

    entity2 = trainer_pb2.Entity()
    entity2.name = "date"
    entity2.source = trainer_pb2.KB
    entity2.type = trainer_pb2.STRING_TYPE

    class1 = trainer_pb2.EntityClass()
    class1.name = 'Weather'
    class1.source = trainer_pb2.SYSTEM
    class1.entities.extend([entity1, entity2])

    utter_data = trainer_pb2.IntentUtterData()
    utter_data.mapped_entity = "city"
    utter_data.mapped_value = "서울"

    utter = trainer_pb2.IntentUtter()
    utter.user_say = "서울 날씨 어때?"
    utter.mapped_user_say = "<city=서울> 날씨 어때?"
    utter.intent_utter_datas.extend([utter_data])

    utters1 = ["<city>의 날씨는 맑음입니다.", "<city>의 날씨는 맑음이다.", "<city>의 날씨는 맑아"]

    utter1 = trainer_pb2.UtteranceSet()
    utter1.system_intent = "inform"
    utter1.patterns.extend(utters1)

    utters2 = ["intention test1", "intention test2", "intention test3"]

    utter2 = trainer_pb2.UtteranceSet()
    utter2.system_intent = "inform"
    utter2.patterns.extend(utters2)

    system_action1 = trainer_pb2.SystemAction()
    system_action1.subtype = 'start'
    system_action1.condition = "IsDAType(\"user\", \"weather\") == true"
    system_action1.utterance_sets.extend([utter1, utter2])

    next_task = trainer_pb2.NextTask()
    next_task.name = "weather"
    next_task.condition = "IsDAType(\"user\", \"weather\") == true"
    next_task.controller = trainer_pb2.SYSTEM

    task1 = trainer_pb2.Task()
    task1.name = "Greet"
    task1.type = trainer_pb2.ESSENTIAL
    task1.goal = "IsDAType(\"user\", \"weather\") == true"
    task1.next_tasks.extend([next_task])

    task2 = trainer_pb2.Task()
    task2.name = "weather"
    task2.type = trainer_pb2.ESSENTIAL
    task2.goal = "1"
    task2.transactions.extend([system_action1])

    intent = trainer_pb2.Intent()
    intent.name = "weather"
    intent.intent_utters.extend([utter])

    model.intents.extend([intent])
    model.entities_classes.extend([class1])
    model.tasks.extend([task1, task2])

    return model


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-sds-train.conf')
    model = make_kor_model()
    train_model = trainer_pb2.SdsTrainModel()
    train_model.model.MergeFrom(model)
    train_model.action = trainer_pb2.ALL
    # train_model.action = trainer_pb2.SAVE_KNOWLEDGE_FILES
    test_sdstrainer(train_model)

