import pprint


class JsonPrinter(pprint.PrettyPrinter):
    def format(self, _object, context, maxlevels, level):
        if isinstance(_object, bool):
            tmp_object = _object
            tmp_object = str(tmp_object).lower()
            return "%s" % tmp_object, True, False
        if isinstance(_object, unicode):
            tmp_object = _object.encode('utf8')
            tmp_object = tmp_object.replace('"', '\\"')
            tmp_object = tmp_object.replace('\'', '\\\"')
            return "\"%s\"" % tmp_object, True, False
        elif isinstance(_object, str):
            _object = unicode(_object, 'utf8')
            tmp_object = _object.encode('utf8')
            tmp_object = tmp_object.replace('"', '\\"')
            tmp_object = tmp_object.replace('\'', '\\\"')
            return "\"%s\"" % tmp_object, True, False
        return pprint.PrettyPrinter.format(self, _object, context, maxlevels, level)