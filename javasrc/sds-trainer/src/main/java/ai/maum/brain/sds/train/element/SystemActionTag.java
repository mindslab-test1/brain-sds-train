package ai.maum.brain.sds.train.element;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import lombok.Getter;
import lombok.Setter;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlRootElement(name = "dialog_node")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder =
    {"talker", "dialogType", "task", "subtype", "relatedTask", "progressSlot", "condition",
        "requestUtterance", "utteranceSet"})
public class SystemActionTag {

  @XmlElement(name="talker")
  String talker;
  @XmlElement(name="dialog_type")
  String dialogType;
  @XmlElement(name="task")
  String task;
  @XmlElement(name="subtype")
  String subtype;
  @XmlElement(name="related_task")
  String relatedTask;
  @XmlElement(name="progress_slot")
  String progressSlot;
  @XmlElement(name="condition")
  String condition;
  @XmlElement(name="request_utterance")
  RequestUtteranceTag requestUtterance;//class
  @XmlElement(name="utterance_set")
  UtteranceSetTag utteranceSet;

  @Getter
  @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(propOrder = {"talker", "condition", "da"})
  public static class RequestUtteranceTag {

    @XmlElement(name="talker")
    String talker;
    @XmlElement(name="condition")
    String condition;
    @XmlElement(name="DA")
    String da;
  }

  @Getter
  @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  public static class UtteranceSetTag {

    List<UtteranceTag> utterance;

    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(propOrder = {"condition", "template", "action"})
    public static class UtteranceTag {

      @XmlElement(name="condition")
      String condition;
      @XmlElement(name="template")
      TemplateTag template;
      @XmlElement(name="action")
      String action;

      @Getter
      @Setter
      @XmlRootElement(name = "template")
      @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(propOrder = {"weight", "intention"})
      public static class TemplateTag {

        @XmlElement(name="weight")
        double weight;
        @XmlElement(name="intention")
        IntentionTag intention;

        @Getter
        @Setter
        @XmlRootElement(name = "Intention")
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class IntentionTag {

          @XmlElement(name="DA")
          String da;
          @XmlElement(name="pattern")
          List<String> pattern;
        }
      }
    }
  }

}
