package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

/**
 * Response of call of 'view/dialogslearningchk.go'.
 */

@Getter
public class LogResponse extends StatusResponse {

  @SerializedName("LOG")
  private String log;
}
