package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ToMap {

  static public Map<String, String> from(Object object) {
    Map<String, String> fieldMap = new HashMap<>();
    Field[] fields = object.getClass().getDeclaredFields();

    for (Field field : fields) {
      try {
        field.setAccessible(true);
        Object obj = field.get(object);
        String fieldName = field.getAnnotation(SerializedName.class).value();
        fieldMap.put(fieldName, obj != null ? obj.toString() : "");
      } catch (IllegalAccessException ignored) {
      }
    }

    return fieldMap;
  }


  /**
   * convert a instance to a PartMap.
   *
   * @return a converted PartMap
   */
  public Map<String, String> toMap() {
    return from(this);

//    Map<String, String> fieldMap = new HashMap<>();
//    Field[] fields = this.getClass().getDeclaredFields();
//
//    for (Field field : fields) {
//      try {
//        field.setAccessible(true);
//        Object obj = field.get(this);
//        if (obj != null) {
//          fieldMap.put(field.getAnnotation(SerializedName.class).value(), obj.toString());
//        }
//      } catch (IllegalAccessException ignored) {
//      }
//    }
//
//    return fieldMap;
  }
}
