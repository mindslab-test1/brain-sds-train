package ai.maum.brain.sds.train.interfaces;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import okhttp3.CookieJar;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RestClient {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private CookieManager cookieManager;
  private Retrofit retrofit;


  /**
   * setup
   *
   * @param baseUrl REST API base url
   * @param wantLog show http logs
   */
  public RestClient setup(String baseUrl, boolean wantLog) {
    cookieManager = new CookieManager(new CookieRepository(), CookiePolicy.ACCEPT_ALL);
    CookieJar cookieJar = new JavaNetCookieJar(cookieManager);
    OkHttpClient.Builder builder = new OkHttpClient.Builder().cookieJar(cookieJar);

    if (wantLog) {
      builder.addInterceptor(new HttpLoggingInterceptor().setLevel(Level.BODY));
    }

    retrofit = new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client(builder.build())
        .build();

    return this;
  }

  /**
   * create
   *
   * @param service REST API interface class
   * @return REST API interface
   */
  public <T> T create(final Class<T> service) {
    if (retrofit == null) {
      throw new IllegalStateException("Not ready to work. Do 'setup' first.");
    }

    return retrofit.create(service);
  }

  public List<HttpCookie> getCookies() {
    return cookieManager.getCookieStore().getCookies();
  }

  public void addCookies(Map<String, String> items) {
    List<String> cookies = new ArrayList<>();
    for (Entry<String, String> entry : items.entrySet()) {
      try {
        cookies.add(new StringBuilder()
            .append(entry.getKey())
            .append("=")
            .append(URLEncoder.encode(entry.getValue(), "UTF-8"))
            .append("; Path=/; HttpOnly")
            .toString());
      } catch (UnsupportedEncodingException ignore) {
      }
    }

    try {
      Map<String, List<String>> headers = new HashMap<>();
      headers.put("Set-Cookie", cookies);
      cookieManager.put(retrofit.baseUrl().uri(), headers);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void addCookie(String key, String value) {
    HashMap<String, String> cookie = new HashMap<>();
    cookie.put(key, value);
    addCookies(cookie);
  }

  public void removeCookie(String key) {
    URI uri = retrofit.baseUrl().uri();
    CookieRepository cookieStore = (CookieRepository) cookieManager.getCookieStore();
    List<HttpCookie> cookies = cookieStore.get(uri);
    for (HttpCookie cookie : cookies) {
      if (cookie.getName().equals(key)) {
        cookieStore.remove(uri, cookie);
        break;
      }
    }
  }

}

