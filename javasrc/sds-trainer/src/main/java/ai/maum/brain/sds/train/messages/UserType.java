package ai.maum.brain.sds.train.messages;


public enum UserType {
  SA, // 관리자
  GU, // 일반 사용자
  USER, // 특정 목적을 위한 사용자
  OA // Open API 사용자
}
