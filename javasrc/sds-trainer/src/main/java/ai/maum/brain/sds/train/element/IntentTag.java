package ai.maum.brain.sds.train.element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="define")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"daClass", "daType", "slot", "task"})
public class IntentTag {
  @XmlElement(name="DA_class")
  String daClass = "";
  @XmlElement(name="DA_type")
  String daType = "";
  @XmlElement(name="slot")
  String slot = "";
  @XmlElement(name="task")
  String task = "";
}


