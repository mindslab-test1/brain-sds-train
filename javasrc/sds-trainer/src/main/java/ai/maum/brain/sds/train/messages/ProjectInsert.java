package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * form-data class for 'view/insertproject.do' API
 */

@SuppressWarnings("SpellCheckingInspection")
@Getter
@Setter
@Accessors(chain = true)
public class ProjectInsert extends ToMap {

  @SerializedName("PROJECT_NAME")
  private String name;
  @SerializedName("DESCRIPTION")
  private String description;
  @SerializedName("SEQ_NO")
  private Integer seqNo;
  @SerializedName("CHATBOT_USE_FLAG")
  private Character useChatbotYn;
  @SerializedName("CHATBOT_DATA_FLAG")
  private Character hasChatbotDataYn;
  @SerializedName("FILTER_VALUE")
  private Float filteringReference;
  @SerializedName("LANG_SEQ_NO")
  private Integer language;
  @SerializedName("PROJECT_USER_SEQ")
  private Integer userSeq;
  @SerializedName("API_KEY")
  private String apiKey;
  @SerializedName("SELECT_API_KEY")
  private String selectApiKey;
  @SerializedName("DEL_SEQ_NO")
  private Integer delSeqNo;
  @SerializedName("OLD_NAME")
  private String oldName;
  @SerializedName("OLD_LANG_SEQ")
  private Integer oldLanguage;
}
