package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * form-data class for 'view/dialogslearnfilesave.do' API
 */

@Getter
@Setter
@Accessors(chain = true)
public class DialogKnowledgeFile extends ToMap {

  @SuppressWarnings("SpellCheckingInspection")
  @SerializedName("UNIQID")
  private String uniqueId;
  @SerializedName("PROJECT_SEQ")
  private int projectSeq;
  @SerializedName("PROJECT_NAME")
  private String projectName;
}
