package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * form-data class for 'view/view/logincheck.go' API
 */

@Getter
@Setter
@Accessors(chain = true)
public class Credentials extends ToMap {

  @SuppressWarnings("SpellCheckingInspection")
  @SerializedName("USER_ID")
  private String id;
  @SerializedName("USER_PWD")
  private String password;
}
