package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

/**
 * Response of call of 'view/userinsert.go'.
 */

@Getter
public class LoginResponse extends StatusResponse {

  @Override
  protected void init() {
    assignDescription("OK", "Login succeeded");
    assignDescription("FA", "Authentication failed");
  }

  @SerializedName("userinfo")
  private UserInfo userInfo;
}
