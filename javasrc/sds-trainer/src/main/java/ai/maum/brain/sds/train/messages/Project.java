package ai.maum.brain.sds.train.messages;


import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * form-data class for 'view/setprojectlist.do' API
 */

@Getter
@Setter
@Accessors(chain = true)
public class Project extends ToMap {

  @SerializedName("SEQ_NO")
  private Integer seqNo;
  @SerializedName("PROJECT_NAME")
  private String projectName;
  @SerializedName("LANG_SEQ_NO")
  private Integer language;
  @SerializedName("PROJECT_USER_SEQ")
  private Integer projectUserSeq;
  @SerializedName("API_KEY")
  private String apiKey;
}
