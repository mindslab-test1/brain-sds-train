package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * form-data class for 'view/userinsert.go' API
 */

@Getter
@Setter
@Accessors(chain = true)
public class UserInsert extends ToMap {

  /**
   * 사용자 번호 : 새로 추가 때는 0, 정보 업데이트 때는 해당 사용자 번호
   */
  @SerializedName("SEQ_NO")
  private Integer seqNo;
  @SerializedName("USER_ID")
  private String id;
  @SerializedName("USER_PWD")
  private String password;
  @SerializedName("USER_NAME")
  private String name;
  @SerializedName("USER_TYPE")
  private UserType type;
  @SerializedName("REMARKS")
  private String remarks;
  @SerializedName("API_KEY")
  private String apiKey;
}
