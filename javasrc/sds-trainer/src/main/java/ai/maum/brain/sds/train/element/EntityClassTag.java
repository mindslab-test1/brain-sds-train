package ai.maum.brain.sds.train.element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="class")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name", "source", "description", "task", "slots", "key"})
public class EntityClassTag {

  @XmlElement(name="name", required = true)
  String name;
  @XmlElement(name="source", required = true)
  String source;
  @XmlElement(name="description", required = true)
  String description = "";
  @XmlElement(name="task", required = true)
  String task = "";
  @XmlElement(name="slots", required = true)
  String slots = "";
  @XmlElement(name="key", required = true)
  String key = "";
}
