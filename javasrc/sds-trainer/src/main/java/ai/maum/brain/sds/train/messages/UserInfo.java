package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;


@Getter
public class UserInfo extends ToString {

  @SerializedName("SEQ_NO")
  private Integer seqNo;
  @SerializedName("USER_ID")
  private String id;
  @SerializedName("USER_NAME")
  private String name;
  @SerializedName("USER_TYPE")
  private UserType type;
  @SerializedName("API_KEY")
  private String apiKey;
}
