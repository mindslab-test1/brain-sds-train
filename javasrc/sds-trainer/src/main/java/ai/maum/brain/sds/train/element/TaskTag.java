package ai.maum.brain.sds.train.element;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@XmlRootElement(name="task")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder =
    {"taskName", "taskType", "taskGoal", "fillSlot", "relatedSlot", "nextTask", "resetSlot"})
public class TaskTag {
  @XmlElement(name="task_name")
  String taskName;
  @XmlElement(name="task_type")
  String taskType;
  @XmlElement(name="task_goal")
  String taskGoal;
  @XmlElement(name="fill_slot")
  FillSlot fillSlot;
  @XmlElement(name="related_slot")
  String relatedSlot;
  @XmlElement(name="next_task")
  NextTaskTag nextTask;
  @XmlElement(name="reset_slot")
  String resetSlot;

  @Getter @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  public static class FillSlot {
    @XmlElement(name="slot")
    List<String> slot;
  }

  @Getter @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  public static class NextTaskTag {
    @XmlElement(name="next_task_item")
    List<NextTaskItemTag> nextTaskItem;

    @Getter @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class NextTaskItemTag {
      @XmlElement(name="task_name")
      String taskName;
      @XmlElement(name="condition")
      String condition;
      @XmlElement(name="controller")
      String controller;
    }
  }

  // 빈값인 경우 tag가 부재되어버리는 현상 제거 위해.
  // todo : 다른 방법 찾기 & 없는 경우 실제로 문제가 되는지 확인하기
  public TaskTag() {
    taskName = "";
    taskType = "";
    taskGoal = "";
    fillSlot = new FillSlot();
    relatedSlot = "";
    nextTask = new NextTaskTag();
    resetSlot = "";
  }
}
