package ai.maum.brain.sds.train.messages;

/**
 * Response of call of 'view/userinsert.go'.
 */

public class UserInsertResponse extends StatusResponse {

  @Override
  protected void init() {
    assignDescription("S", "Success");
    assignDescription("UI", "The ID is already exist");
    assignDescription("UA", "The API Key is already exist");
  }
}
