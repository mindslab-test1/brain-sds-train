package ai.maum.brain.sds.train.trainer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TarUtil {

  static final Logger logger = LoggerFactory.getLogger(TarUtil.class);

  public static void compress(String name, File... files) throws IOException {
    try (TarArchiveOutputStream out = getTarArchiveOutputStream(name)) {
      for (File file : files) {
        addToArchiveCompression(out, file, ".");
      }
    }
  }

  public static void decompress(String in, File out) throws IOException {
    try (TarArchiveInputStream fin = new TarArchiveInputStream(new FileInputStream(in))) {
      TarArchiveEntry entry;
      while ((entry = fin.getNextTarEntry()) != null) {
        if (entry.isDirectory()) {
          continue;
        }
        File curFile = new File(out, entry.getName());
        File parent = curFile.getParentFile();
        if (!parent.exists()) {
          parent.mkdirs();
        }
        IOUtils.copy(fin, new FileOutputStream(curFile));
      }
    }
  }

  private static TarArchiveOutputStream getTarArchiveOutputStream(String name) throws IOException {
    TarArchiveOutputStream taos = new TarArchiveOutputStream(new FileOutputStream(name));
    // TAR has an 8 gig file limit by default, this gets around that
    taos.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
    // TAR originally didn't support long file names, so enable the support for it
    taos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
    taos.setAddPaxHeadersForNonAsciiNames(true);
    return taos;
  }

  private static void addToArchiveCompression(TarArchiveOutputStream out, File file, String dir)
      throws IOException {
    String entry = dir + File.separator + file.getName();
    if (file.isFile()) {
      out.putArchiveEntry(new TarArchiveEntry(file, entry));
      try (FileInputStream in = new FileInputStream(file)) {
        IOUtils.copy(in, out);
      }
      out.closeArchiveEntry();
    } else if (file.isDirectory()) {
      File[] children = file.listFiles();
      if (children != null) {
        for (File child : children) {
          addToArchiveCompression(out, child, entry);
        }
      }
    } else {
      logger.info(file.getName() + " is not supported");
    }
  }

  public enum Hash {

    MD5("MD5"),
    SHA1("SHA1"),
    SHA256("SHA-256"),
    SHA512("SHA-512");

    private String name;

    Hash(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public byte[] checksum(File input) {
      try (InputStream in = new FileInputStream(input)) {
        MessageDigest digest = MessageDigest.getInstance(getName());
        byte[] block = new byte[4096];
        int length;
        while ((length = in.read(block)) > 0) {
          digest.update(block, 0, length);
        }
        return digest.digest();
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    }

  }

}
