package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import java.lang.reflect.Field;
import java.util.Collection;

public class ToString {

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    Field[] fields = this.getClass().getDeclaredFields();

    for (Field field : fields) {
      try {
        field.setAccessible(true);
        Object obj = field.get(this);
        if (obj == null) {
          continue;
        }
        builder.append(field.getAnnotation(SerializedName.class).value()).append(": ");
        if (obj instanceof Collection<?>) {
          builder.append("[\n");
          for (Object element : (Collection<?>) obj) {
            builder.append("{\n");
            builder.append(element.toString());
            builder.append("}\n");
          }
          builder.append("]");
        } else {
          builder.append(obj.toString());
        }
        builder.append("\n");
      } catch (IllegalAccessException ignored) {
      }
    }

    return builder.toString();
  }

}
