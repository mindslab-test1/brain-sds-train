package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.Map;


public class StatusResponse {

  public final class Status {

    String value;
    Map<String, String> descriptionMap = new HashMap<>();

    public String getDescription() {
      String description = descriptionMap.get(value);
      return description != null ? description : value;
    }

    @Override
    public String toString() {
      return value;
    }
  }

  private Status status = new Status();

  @SerializedName("STATUS")
  private String rawText;


  StatusResponse() {
    init();
  }

  /**
   * Override하여 Status의 description을 등록한다.
   */
  protected void init() {
  }

  protected void assignDescription(String value, String description) {
    status.descriptionMap.put(value, description);
  }

  public Status getStatus() {
    status.value = rawText;
    return status;
  }

}
