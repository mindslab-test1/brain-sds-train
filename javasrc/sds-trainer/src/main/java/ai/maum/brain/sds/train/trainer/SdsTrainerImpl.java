package ai.maum.brain.sds.train.trainer;

import static com.google.protobuf.util.Timestamps.between;

import ai.maum.brain.sds.train.element.DomainControlFile;
import ai.maum.brain.sds.train.element.DomainControlFile.IntentControlTag;
import ai.maum.brain.sds.train.element.DomainControlFile.IntentControlTag.ControlTag;
import ai.maum.brain.sds.train.element.DomainControlFile.SlotInit.SlotInitSetWrapper;
import ai.maum.brain.sds.train.element.DomainControlFile.SlotInit.SlotInitSetWrapper.SlotInitSet;
import ai.maum.brain.sds.train.element.EntityClassTag;
import ai.maum.brain.sds.train.element.EntityTag;
import ai.maum.brain.sds.train.element.IntentTag;
import ai.maum.brain.sds.train.element.SystemActionTag;
import ai.maum.brain.sds.train.element.SystemActionTag.RequestUtteranceTag;
import ai.maum.brain.sds.train.element.SystemActionTag.UtteranceSetTag;
import ai.maum.brain.sds.train.element.SystemActionTag.UtteranceSetTag.UtteranceTag;
import ai.maum.brain.sds.train.element.SystemActionTag.UtteranceSetTag.UtteranceTag.TemplateTag;
import ai.maum.brain.sds.train.element.SystemActionTag.UtteranceSetTag.UtteranceTag.TemplateTag.IntentionTag;
import ai.maum.brain.sds.train.element.TaskTag;
import ai.maum.brain.sds.train.element.TaskTag.NextTaskTag;
import ai.maum.brain.sds.train.element.TaskTag.NextTaskTag.NextTaskItemTag;
import ai.maum.brain.sds.train.trainer.TarUtil.Hash;
import ai.maum.rpc.ResultStatus;
import ai.maum.rpc.ResultStatusListProto;
import ai.maum.util.PropertyManager;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Duration;
import com.google.protobuf.Empty;
import com.google.protobuf.Timestamp;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import maum.brain.sds.train.Entity;
import maum.brain.sds.train.EntityClass;
import maum.brain.sds.train.EntityData;
import maum.brain.sds.train.EntityInstance;
import maum.brain.sds.train.InstanceDataMap;
import maum.brain.sds.train.Intent;
import maum.brain.sds.train.IntentUtter;
import maum.brain.sds.train.IntentUtterData;
import maum.brain.sds.train.NextTask;
import maum.brain.sds.train.SdsModel;
import maum.brain.sds.train.SdsTrainAction;
import maum.brain.sds.train.SdsTrainBinary;
import maum.brain.sds.train.SdsTrainKey;
import maum.brain.sds.train.SdsTrainModel;
import maum.brain.sds.train.SdsTrainResult;
import maum.brain.sds.train.SdsTrainStatus;
import maum.brain.sds.train.SdsTrainStatusList;
import maum.brain.sds.train.SdsTrainStep;
import maum.brain.sds.train.SdsTrainerGrpc;
import maum.brain.sds.train.Source;
import maum.brain.sds.train.SystemAction;
import maum.brain.sds.train.Task;
import maum.brain.sds.train.UtteranceSet;
import maum.common.LangOuterClass.Lang;
import maum.rpc.Status.ExCode;
import maum.rpc.Status.Module;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SdsTrainerImpl extends SdsTrainerGrpc.SdsTrainerImplBase {

  static final Logger logger = LoggerFactory.getLogger(SdsTrainerImpl.class);
  // model id, train id 조합
  private Map<String, String> trainKeyMap = new HashMap<>();
  // train id, train task 조합
  private Map<String, TrainTask> trainTaskMap = new HashMap<>();
  // 학습중인 model id 목록
  private List<String> trainingModelIds = new ArrayList<>();

  public SdsTrainerImpl() {
    logger.debug("SDS Trainer Server Constructor");
    ResultStatus.setModule(Module.BRAIN_SDS_TRAIN);
    ResultStatus.setProcess(0);
  }
  /**
   * <pre>
   * 학습 모델과 수행 유형을 받은 후 모델 경로를 생성 한 후 지식파일을 저장한다.
   * 수행 유형이 ALL일 경우는 학습을 시작한다.
   * </pre>
   */
  public void open(SdsTrainModel request, StreamObserver<SdsTrainKey> responseObserver) {

    logger.info("SDS trainer Server: Open() , model : {}", request.getModel().getName());
    logger.trace("Open request : {}", request);

    SdsModel model = request.getModel();
    SdsTrainKey.Builder trainKeyBuilder = SdsTrainKey.newBuilder();

    // 이미 학습 중인 모델일 때
    if (trainingModelIds.contains(model.getId())) {

      logger.warn("Duplicate open() call of model : " + model.getName());

      ResultStatus status;

      try {
        status = new ResultStatus(ExCode.ABORTED, 500,
            "There's already ongoing training for the model " + model.getName());

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);

      } catch (Exception e1) {
        logger.error("{} => ", e1.getMessage(), e1);
        responseObserver.onError(new StatusRuntimeException(Status.ABORTED));
      }

    } else {

      // 해당 모델의 이전 학습 task 는 더 이상 접근할 수 없음.
      if (trainKeyMap.get(model.getId()) != null) {
        String oldTrainKey = trainKeyMap.get(model.getId());
        trainTaskMap.remove(oldTrainKey);
      }

      // 학습 thread 생성
      TrainTask trainTask = new TrainTask(request);
      Thread trainThread = new Thread(trainTask);

      String trainKey = UUID.randomUUID().toString();
      trainTaskMap.put(trainKey, trainTask);
      trainKeyMap.put(model.getId(), trainKey);
      trainingModelIds.add(model.getId());

      // 학습 thread 실행
      trainThread.setDaemon(true);
      trainThread.start();

      trainKeyBuilder.setId(trainKey);
      logger.trace("Open response : {}", trainKeyBuilder);
      responseObserver.onNext(trainKeyBuilder.build());
      responseObserver.onCompleted();
    }
  }

  /**
   * <pre>
   * Monitoring learning progress
   * </pre>
   */
  public void getProgress(SdsTrainKey request, StreamObserver<SdsTrainStatus> responseObserver) {

    logger.info("SDS trainer Server: GetProgress(), train key : {}", request.getId());
    logger.trace("GetProgress request : {}", request);

    String trainKey = request.getId();

    // trainKey 가 유효하지 않은 경우
    if (!trainTaskMap.containsKey(trainKey)) {

      logger.warn("No training task with train key " + trainKey);
      String clientErrMsg = "Invalid train key : " + trainKey;

      ResultStatus status;
      try {
        status = new ResultStatus(ExCode.MEM_KEY_NOT_FOUND, 500, clientErrMsg);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);

      } catch (Exception e1) {
        logger.error("{} => ", e1.getMessage(), e1);
        responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND));
      }
    } else {

      TrainTask trainThread = trainTaskMap.get(trainKey);
      SdsModel model = trainThread.getModel();
      Timestamp startTime = trainThread.getStartTime();

      SdsTrainStatus.Builder trainStatus = SdsTrainStatus.newBuilder();
      trainStatus.setId(trainKey)
          .setAction(trainThread.getTrainLevel())
          .setStep(trainThread.getTrainStep())
          .setModel(model.getName())
          .setLang(model.getLang())
          .setResult(trainThread.getTrainResult())
          .setStarted(startTime)
          .setElapsed(between(startTime, trainThread.getCurrentEndTime()));

      // train log
      File logFile;
      if (trainThread.getTrainLevel().equals(SdsTrainAction.ALL) &&
          !trainThread.getTrainResult().equals(SdsTrainResult.FAILED) &&
          (logFile = getTrainLogFile(model.getName(), model.getLang(), trainKey)) != null) {

        List<String> trainLog = getTrainLog(logFile);
        if (trainLog.size() != 0) {
          trainStatus.addAllLogs(trainLog);
        }
      }

      logger.trace("GetProgress response : {}", trainStatus);
      responseObserver.onNext(trainStatus.build());
      responseObserver.onCompleted();
    }
  }

  /**f
   * <pre>
   * View all learning status
   * </pre>
   */
  public void getAllProgress(Empty request, StreamObserver<SdsTrainStatusList> responseObserver) {
    // getAllProgress: 현재 학습 실행 중인 목록이 아니라 , 실행 + 완료 된 모든 목록을 돌려줌.
    // 단, 한 모델 당 한 status 만 가질 수 있음. 각 모델의 가장 최신 학습 status 만 돌려줌.

    logger.info("SDS trainer Server: GetAllProgress()");
    logger.trace("GetAllProgress request : {}", request);

    SdsTrainStatusList.Builder statusList = SdsTrainStatusList.newBuilder();

    for (Map.Entry<String, TrainTask> entry : trainTaskMap.entrySet()) {
      TrainTask task = entry.getValue();
      SdsModel model = task.getModel();
      SdsTrainStatus.Builder status = SdsTrainStatus.newBuilder();
      status.setId(entry.getKey())
          .setAction(task.getTrainLevel())
          .setStep(task.getTrainStep())
          .setModel(model.getName())
          .setLang(model.getLang())
          .setResult(task.getTrainResult())
          .setStarted(task.getStartTime())
          .setElapsed(task.getElapsedTime());

      // train log
      File logFile;
      if (status.getAction().equals(SdsTrainAction.ALL) &&
          !status.getResult().equals(SdsTrainResult.FAILED) &&
          (logFile = getTrainLogFile(model.getName(), model.getLang(), entry.getKey())) != null) {

        List<String> trainLog = getTrainLog(logFile);
        if (trainLog.size() != 0) {
          status.addAllLogs(trainLog);
        }
      }

      statusList.addSdsTrains(status);
    }

    logger.trace("GetAllProgress response : {}", statusList);
    responseObserver.onNext(statusList.build());
    responseObserver.onCompleted();
  }

  /**
   * <pre>
   * End of learning
   * </pre>
   */
  public void close(SdsTrainKey request, StreamObserver<SdsTrainStatus> responseObserver) {
  }

  /**
   * <pre>
   * Undo and force termination
   * </pre>
   */
  public void stop(SdsTrainKey request, StreamObserver<SdsTrainStatus> responseObserver) {

    logger.info("SDS trainer Server: Stop()");
    logger.trace("Stop request : {}", request);

    String trainKey = request.getId();
    // request가 null 일 경우
    if (trainKey.isEmpty()) {
      logger.warn("Train key is empty.");

      ResultStatus status;
      try {
        status = new ResultStatus(ExCode.MEM_KEY_NOT_FOUND, 500, "Train key is empty.");

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);

      } catch (Exception e1) {
        logger.error("{} => ", e1.getMessage(), e1);
        responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND));
      }
    } else {

      TrainTask trainTask = trainTaskMap.get(request.getId());
      trainTask.stopProcess();

      getProgress(request, responseObserver);

    }
  }

  /**
   * <pre>
   * Download learned images after completion of learning
   * </pre>
   */
  public void getBinary(SdsModel request, StreamObserver<SdsTrainBinary> responseObserver) {

    logger.info("SDS trainer Server: GetBinary()");
    logger.trace("GetBinary request : {}", request);

    String domainName = request.getName();
    String modelPath = String.format("%s/%s", getDialogDomainPath(request.getLang()), domainName);
    String tarPath = String.format("%s/%s.tar", getDialogDomainPath(request.getLang()), domainName);

    File modelDir = new File(modelPath);
    File tarOutput = new File(tarPath);
    if (tarOutput.exists()) {
      tarOutput.delete();
    }

    int i = 0, tmp, total = 0;
    int bufferSize = 512 * 1024; // 512k
    byte[] buffer = new byte[bufferSize];
    BufferedInputStream bi = null;

    try {
      // make tar
      TarUtil.compress(tarPath, modelDir);
      logger.debug("getBinary tar file SHA1  :  {}",
          DatatypeConverter.printHexBinary(Hash.SHA1.checksum(tarOutput)));

      bi = new BufferedInputStream(new FileInputStream(tarOutput));

      // read tar file
      while ((tmp = bi.read(buffer)) > 0) {
        i ++;
        total += tmp;
        logger.trace("{}`th total byte : {}", i, total);
        ByteString byteString = ByteString.copyFrom(buffer, 0 , tmp);
        SdsTrainBinary.Builder trainBinary = SdsTrainBinary.newBuilder();
        trainBinary.setTar(byteString);
        responseObserver.onNext(trainBinary.build());
      }

    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);

      try {
        ResultStatus status =
            new ResultStatus(maum.rpc.Status.ExCode.RUNTIME_ERROR, 500, e.getMessage());

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);

      } catch (Exception e1) {
        logger.error("{} => ", e1.getMessage(), e1);
        responseObserver.onError(new StatusRuntimeException(Status.FAILED_PRECONDITION));
      }

    } finally {
      try {
        bi.close();
      } catch (IOException e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }
    responseObserver.onCompleted();
  }

  /**
   * <pre>
   * Delete learning images kept on the server
   * </pre>
   */
  public void removeBinary(SdsModel request, StreamObserver<Empty> responseObserver) {

    logger.info("SDS trainer Server: RemoveBinary()");
    logger.trace("RemoveBinary request : {}", request);

    String domainName = request.getName();
    String modelPath = String.format("%s/%s", getDialogDomainPath(request.getLang()), domainName);
    String tarPath = String.format("%s/%s.tar", getDialogDomainPath(request.getLang()), domainName);
    File modelDir = new File(modelPath);
    File tarFile = new File(tarPath);

    if (tarFile.exists()) {
      if (tarFile.delete()) {
        logger.debug("Model tar [{}.tar] deleted", domainName);
      }
    }
    if (modelDir.exists()) {
      try {
        // delete
        if (deleteDirectory(modelDir)) {
          logger.debug("Model [{}] deleted", domainName);
        }
      } catch (Exception e) {
        // delete 실패시
        ResultStatus status;
        try {
          status = new ResultStatus(ExCode.RUNTIME_ERROR, 500, e.getMessage());
          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e1) {
          logger.error("{} => ", e1.getMessage(), e1);
          responseObserver.onError(new StatusRuntimeException(Status.FAILED_PRECONDITION));
        }
      }
    } else {
      logger.debug("Model [{}] not exist", domainName);
    }

    responseObserver.onNext(Empty.newBuilder().build());
    responseObserver.onCompleted();
  }

  private String getDialogDomainPath(Lang lang) {
    String enginePath = PropertyManager.getString("brain-sds-trainer.engine.dir");
    String langPath;
    if (lang.equals(Lang.ko_KR)) {
      langPath = "KOR";
    } else {
      langPath = "ENG";
    }
    String dialogDomainPath = String.format("%s/KB/%s/dialog_domain", enginePath, langPath);
    return dialogDomainPath;
  }

  private void saveKnowledgeFile(SdsModel model) throws Exception {

    String dialogDomainPath = getDialogDomainPath(model.getLang());
    String domainName = model.getName();
    char slotDelimiter;

    if (model.getLang().equals(Lang.ko_KR)) {
      slotDelimiter = '.';
    } else {
      slotDelimiter = '$';
    }

    String emptyPath = String.format("%s/empty", dialogDomainPath);
    String modelPath = String.format("%s/%s", dialogDomainPath, domainName);

    createModelStructure(emptyPath, modelPath, model.getName());

    modelPath = String.format("%s/%s", modelPath, model.getName());

    createTaskFile(model.getTasksList(), modelPath);
    createEntityFile(model.getEntitiesClassesList(), slotDelimiter, modelPath);
    createIntentFile(model.getIntentsList(), modelPath);
    createSystemActionFile(model.getTasksList(), model.getIntentsList(), modelPath);
    createUserUtterFile(model.getIntentsList(), slotDelimiter, modelPath);

    createSql(model.getEntitiesClassesList(), slotDelimiter, modelPath);
    CreateEtcFile(model, slotDelimiter, modelPath);
  }

  private void executeCmd(String[] cmd) throws Exception {
    Runtime rt = Runtime.getRuntime();
    Process proc;
    try {
      proc = rt.exec(cmd);
      proc.waitFor();

    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    } catch (InterruptedException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }

  private void createModelStructure(String emptyPath, String modelPath, String domainName) throws Exception {
    File s = new File(emptyPath);
    File t = new File(modelPath);
    if (!t.exists()) {
      t.mkdir();
      logger.debug("Model [{}] directory created.", domainName);

      copyDirectory(s, t);

      File oldsql = new File(modelPath + "/empty_DB.sqlite3");
      File renamesql = new File(modelPath + "/" + domainName + "_DB.sqlite3");
      oldsql.renameTo(renamesql);
      oldsql = new File(modelPath + "/empty.task.txt");
      renamesql = new File(modelPath + "/" + domainName + ".task.txt");
      oldsql.renameTo(renamesql);
      oldsql = new File(modelPath + "/empty.slot.txt");
      renamesql = new File(modelPath + "/" + domainName + ".slot.txt");
      oldsql.renameTo(renamesql);
      oldsql = new File(modelPath + "/empty.dialogLib.txt");
      renamesql = new File(modelPath + "/" + domainName + ".dialogLib.txt");
      oldsql.renameTo(renamesql);
      oldsql = new File(modelPath + "/empty.DAtype.txt");
      renamesql = new File(modelPath + "/" + domainName + ".DAtype.txt");
      oldsql.renameTo(renamesql);
      oldsql = new File(modelPath + "/empty.svm");
      renamesql = new File(modelPath + "/" + domainName + ".svm");
      oldsql.renameTo(renamesql);
    }

    String[] cmd = {"chmod", "-R", "777", modelPath};
    executeCmd(cmd);
  }

  private void copyDirectory(File sourceF, File targetF) throws Exception {
    if (sourceF.exists()) {
      File[] ff = sourceF.listFiles();
      for (File file : ff) {
        File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());

        if (file.isDirectory()) {
          temp.mkdir();
          copyDirectory(file, temp);
        } else {
          FileInputStream fis = null;
          FileOutputStream fos = null;
          try {
            fis = new FileInputStream(file);
            fos = new FileOutputStream(temp);
            byte[] b = new byte[4096];
            int cnt = 0;
            while ((cnt = fis.read(b)) != -1) {
              fos.write(b, 0, cnt);
            }
          } catch (Exception e) {
            logger.error("{} => ", e.getMessage(), e);
            fis.close();
            fos.close();
            throw e;
          } finally {
            fis.close();
            fos.close();
          }
        }
      }
    }
  }

  private boolean deleteDirectory(File dir) throws Exception {

    boolean result = false;
    try {
      if(dir.exists()){
        // get file list
        File[] fileList = dir.listFiles();

        for (int i = 0; i < fileList.length; i++) {
          if(fileList[i].isFile()) {
            fileList[i].delete();
          }else {
            deleteDirectory(fileList[i]);
          }
          fileList[i].delete();
        }
        dir.delete(); //폴더 삭제
        result = true;
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }

    return result;
  }

  // task.txt
  private void createTaskFile(List<Task> taskList, String modelPath) throws Exception {

    Marshaller xmlConverter = getXmlConverter(TaskTag.class);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    for (Task task : taskList) {
      TaskTag taskTag = new TaskTag();

      for (Map.Entry<FieldDescriptor, Object> field : task.getAllFields().entrySet()) {

        switch (field.getKey().getName()) {
          case "name":
            taskTag.setTaskName(field.getValue().toString());
            break;

          case "type":
            taskTag.setTaskType(field.getValue().toString());
            break;

          case "goal":
            taskTag.setTaskGoal(field.getValue().toString());
            break;

          case "progresses":
            // SystemAction 함수에서 처리
            break;

          case "related_slot":
            // todo
            taskTag.setRelatedSlot(field.getValue().toString());
            break;

          case "next_tasks":
            NextTaskTag nextTaskTag = new NextTaskTag();
            List<NextTaskItemTag> nextTaskItemTagList = new ArrayList<>();

            for (Object objTmp : (List<?>) field.getValue()) {

              NextTask nTask = (NextTask) objTmp;
              NextTaskItemTag nextTaskItemTag = new NextTaskItemTag();
              nextTaskItemTag.setTaskName(nTask.getName());
              nextTaskItemTag.setCondition(nTask.getCondition());
              String controller = nTask.getController().toString();
              if (nTask.getController() == Source.SYSTEM || nTask.getController() == Source.USER) {
                controller = controller.toLowerCase();
              }
              nextTaskItemTag.setController(controller);
              nextTaskItemTagList.add(nextTaskItemTag);

            }

            nextTaskTag.setNextTaskItem(nextTaskItemTagList);
            taskTag.setNextTask(nextTaskTag);
            break;

          case "reset_slots":
            taskTag.setResetSlot(field.getValue().toString());
            break;

          default:
            break;
        }
      }

      convertObjToXml(xmlConverter, taskTag, baos);
    }

    outStreamClose(baos);
    createTxtFile(modelPath + ".task.txt", baos.toString());
  }

  // slot.txt
  private void createEntityFile(List<EntityClass> entityClassList, char slotDelimiter,
      String modelPath) throws Exception {
    Marshaller entityClassXmlConverter = getXmlConverter(EntityClassTag.class);
    Marshaller entityXmlConverter = getXmlConverter(EntityTag.class);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    for (EntityClass entityClass : entityClassList) {
      EntityClassTag entityClassTag = new EntityClassTag();
      List<EntityTag> entityTagList = new ArrayList<>();

      for (Map.Entry<FieldDescriptor, Object> field : entityClass.getAllFields().entrySet()) {

        switch (field.getKey().getName()) {
          case "name":
            entityClassTag.setName(field.getValue().toString());
            break;

          case "source":
            entityClassTag.setSource(field.getValue().toString());
            break;

          case "description":
            entityClassTag.setDescription(field.getValue().toString());
            break;

          case "defined_task":
            entityClassTag.setTask(field.getValue().toString());
            break;

          case "entites":
            StringBuffer slotListString = new StringBuffer();
            slotListString.append("\n");

            if (field.getValue() instanceof List<?>) {

              for (Object tmpObj : (List<?>) field.getValue()) {

                Entity entity = (Entity) tmpObj;
                slotListString.append(entityClass.getName());
                slotListString.append(slotDelimiter);
                slotListString.append(entity.getName());
                slotListString.append("\n");

                EntityTag entityTag = new EntityTag();
                entityTag.setName(entity.getName());
                entityTag.setSource(entity.getSource().toString());
                entityTag.setType(entity.getType().toString().toLowerCase().split("_type")[0]);
                entityTag.setDescription(entity.getDescription());
                // task가 있을 지 확인 todo
                entityTag.setTask(entityClassTag.getTask());
                entityTag.setSense(entity.getSence());
                entityTag.setPrecedingSlots(
                    entity.getPrecedingSlotsList().toString()
                        .replace("[", "")
                        .replace("]", "").trim());

                entityTagList.add(entityTag);
              }
            }

            entityClassTag.setSlots(slotListString.toString());

            break;

          default:
            break;
        }
      }

      convertObjToXml(entityClassXmlConverter, entityClassTag, baos);

      for (EntityTag entityTag : entityTagList) {
        convertObjToXml(entityXmlConverter, entityTag, baos);
      }
    }

    outStreamClose(baos);
    createTxtFile(modelPath + ".slot.txt", baos.toString());
  }

  // DAtype.txt
  private void createIntentFile(List<Intent> intentList, String modelPath) throws Exception {

    List<String> defaultIntentList = new ArrayList<>();
    defaultIntentList.add("inform");
    defaultIntentList.add("request");
    defaultIntentList.add("reqalts");
    defaultIntentList.add("reqmore");
    defaultIntentList.add("search");
    defaultIntentList.add("confirm");
    defaultIntentList.add("confreq");
    defaultIntentList.add("select");
    defaultIntentList.add("affirm");
    defaultIntentList.add("negate");
    defaultIntentList.add("hello");
    defaultIntentList.add("greet");
    defaultIntentList.add("bye");
    defaultIntentList.add("command");
    defaultIntentList.add("silence");
    defaultIntentList.add("hangup");
    defaultIntentList.add("repeat");
    defaultIntentList.add("help");
    defaultIntentList.add("restart");
    defaultIntentList.add("unknown");
    defaultIntentList.add("misunderstanding");
    defaultIntentList.add("thankyou");
    defaultIntentList.add("welcome");
    defaultIntentList.add("ack");
    defaultIntentList.add("hereis");
    defaultIntentList.add("complain_misunderstanding");
    defaultIntentList.add("non_response");

    Marshaller xmlConverter = getXmlConverter(IntentTag.class);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    for (Intent intent : intentList) {

      Boolean defaultIntentYn = false;
      for (String defaultIntent : defaultIntentList) {
        if (defaultIntent.equals(intent.getName())) {
          defaultIntentYn = true;
          break;
        }
      }

      if (defaultIntentYn) {
        logger.debug("Exception : default intent " + intent.getName());
        break;
      }

      IntentTag intentTag = new IntentTag();
      List<EntityTag> entityTagList = new ArrayList<>();

      for (Map.Entry<FieldDescriptor, Object> field : intent.getAllFields().entrySet()) {

        switch (field.getKey().getName()) {
          case "name":
            intentTag.setDaType(field.getValue().toString());
            break;

          case "description":
            break;

          case "intent_class":
            intentTag.setDaClass(field.getValue().toString());
            break;

          case "defined_task":
            intentTag.setTask(field.getValue().toString());
            break;

          case "slot":
            intentTag.setSlot(field.getValue().toString());
            break;

          case "intent_utters":

            break;

          case "response":

            break;

          default:
            break;
        }
      }

      convertObjToXml(xmlConverter, intentTag, baos);

    }

    outStreamClose(baos);
    createTxtFile(modelPath + ".DAtype.txt", baos.toString());

  }

  // dialogLib.txt
  private void createSystemActionFile(List<Task> taskList, List<Intent> intentList,
      String modelPath) throws Exception {

    Marshaller xmlConverter = getXmlConverter(SystemActionTag.class);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    // response 시스템 발화 경우에 필요
    List<String> taskNameList = new ArrayList<>();

    for (Task task : taskList) {

      taskNameList.add(task.getName());

      // 1. TRANSACTION
      for (SystemAction transaction : task.getTransactionsList()) {
        SystemActionTag systemActionTag = new SystemActionTag();

        systemActionTag.setTalker("system");
        systemActionTag.setDialogType("transaction");
        systemActionTag.setTask(task.getName());
        systemActionTag.setSubtype(transaction.getSubtype());
        systemActionTag.setRelatedTask("");
        systemActionTag.setProgressSlot("");
        systemActionTag.setCondition("");

//          systemActionTag.setRequest_utterance();
        UtteranceSetTag utteranceSetTag = setUtteranceTag(transaction.getUtteranceSetsList());
        systemActionTag.setUtteranceSet(utteranceSetTag);

        convertObjToXml(xmlConverter, systemActionTag, baos);
      }

      // 2. PROGRESS

      for (SystemAction progress : task.getProgressesList()) {

        SystemActionTag systemActionTag = new SystemActionTag();

        systemActionTag.setTalker("system");
        systemActionTag.setDialogType("progress");
        systemActionTag.setTask("");
        systemActionTag.setSubtype("");
        systemActionTag.setRelatedTask(task.getName());
        systemActionTag.setProgressSlot(progress.getProgressSlot());
        systemActionTag.setCondition(progress.getCondition());

//          systemActionTag.setRequest_utterance();
        UtteranceSetTag utteranceSetTag = setUtteranceTag(progress.getUtteranceSetsList());
        systemActionTag.setUtteranceSet(utteranceSetTag);

        convertObjToXml(xmlConverter, systemActionTag, baos);

      }
    }

    for (Intent intent : intentList) {

      // 3. RESPONSE

      for (SystemAction response : intent.getResponsesList()) {

        SystemActionTag systemActionTag = new SystemActionTag();
        systemActionTag.setTalker("system");
        systemActionTag.setDialogType("response");
        systemActionTag.setTask("");
        systemActionTag.setSubtype("");
        if (intent.getDefinedTask().equals("All_tasks")) {
          systemActionTag.setRelatedTask(
              taskNameList.toString()
                  .replace("[", "")
                  .replace("]", "").trim());
        } else {
          // defined_task 비어 있는 경우는 처리 안하고 있음.
          systemActionTag.setRelatedTask(intent.getDefinedTask());
        }

        systemActionTag.setProgressSlot("");
        systemActionTag.setCondition("");

        RequestUtteranceTag requestUtteranceTag = new RequestUtteranceTag();
        requestUtteranceTag.setTalker("user");
        requestUtteranceTag.setCondition("1");
        requestUtteranceTag.setDa(response.getUserIntent());
        systemActionTag.setRequestUtterance(requestUtteranceTag);

        // utteranceSet
        UtteranceSetTag utteranceSetTag = setUtteranceTag(response.getUtteranceSetsList());
        systemActionTag.setUtteranceSet(utteranceSetTag);

        // converting systemActionTag to xml
        convertObjToXml(xmlConverter, systemActionTag, baos);

      }
    }

    String sbStr = baos.toString()
        .replaceAll("&lt;", "<")
        .replaceAll("&gt;", ">");

    outStreamClose(baos);
    createTxtFile(modelPath + ".dialogLib.txt", sbStr);
  }

  // .txt
  private void createUserUtterFile(List<Intent> intentList, char slotDelimiter, String modelPath)
      throws Exception {

    List<String> userUtter = new ArrayList<>();
    for (Intent intent : intentList) {
      for (IntentUtter intentUtter : intent.getIntentUttersList()) {
        int slotCnt = 0;
        StringBuffer userUtterDataRow = new StringBuffer();
        for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDatasList()) {
          slotCnt++;

          userUtterDataRow
              .append(intentUtterData.getMappedEntityClass())
              .append(slotDelimiter)
              .append(intentUtterData.getMappedEntity())
              .append("=")
              .append(intentUtterData.getMappedValue());

          if (slotCnt < intentUtter.getIntentUtterDatasList().size()) {
            userUtterDataRow.append(", ");
          }
        }
        String userUtterRow =
            String.format("%s\t%s\t%s(%s)@@%s",
                intentUtter.getUserSay(),
                intentUtter.getMappedUserSay(),
                intent.getName(),
                userUtterDataRow, intent.getDescription());
        userUtter.add(userUtterRow);
      }
    }

    createTxtFile(modelPath + ".txt", String.join("\n", userUtter));

  }

  // .domainControl.txt _prepattern.txt _replacement.txt
  private void CreateEtcFile(SdsModel model, char slotDelimiter, String modelPath) throws Exception {

    // 1. domainControl.txt
    Marshaller xmlConverter = getXmlConverter(DomainControlFile.class);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    DomainControlFile domain = new DomainControlFile();

    // chatbot
    String chatbotYn;

    if (model.getChatbotYn()) {
      chatbotYn = "Yes";
    } else {
      chatbotYn = "No";
    }
    domain.setChatbot(chatbotYn);

    // slot default value
    StringBuilder slotInitStr = new StringBuilder();
    for (EntityClass entityClass : model.getEntitiesClassesList()) {
      for (Entity entity : entityClass.getEntitiesList()) {

        if (!entity.getDefaultValue().isEmpty()) {
          Marshaller slotInitXmlConverter = getXmlConverter(SlotInitSetWrapper.class);
          ByteArrayOutputStream slotInitBaos = new ByteArrayOutputStream();

          SlotInitSetWrapper slotInitSetWrapper = new SlotInitSetWrapper();
          SlotInitSet slotInitSet = new SlotInitSet();
          slotInitSet.setCondition("true");
          slotInitSet.setValue(entity.getDefaultValue());
          slotInitSetWrapper.setSet(slotInitSet);

          convertObjToXml(slotInitXmlConverter, slotInitSetWrapper, slotInitBaos);
          String slotInitUnit = slotInitBaos.toString().replaceAll("temporarilySlotName",
              String.format("%s\\%c%s", entityClass.getName(), slotDelimiter, entity.getName()));

          slotInitStr.append(slotInitUnit);

          outStreamClose(slotInitBaos);
        }
      }
    }

    if (!slotInitStr.toString().isEmpty()) {
      domain.setSlotInitFake("\n" + slotInitStr.toString());
    }

    // intent control
    IntentControlTag intentControlTag = new IntentControlTag();
    List<ControlTag> controlTagList = new ArrayList<>();

    for (Task task : model.getTasksList()) {
      for (SystemAction progress : task.getProgressesList()) {
        for (UtteranceSet utteranceSet : progress.getUtteranceSetsList()) {
          String systemIntent = utteranceSet.getSystemIntent();
          String userResponse = utteranceSet.getForcedNextIntent();

          ControlTag controlTag = new ControlTag();
          controlTag.setSystemIntent(systemIntent);
          controlTag.setUserResponse(userResponse);
          controlTagList.add(controlTag);
        }
      }
    }
    intentControlTag.setControl(controlTagList);
    domain.setIntentControl(intentControlTag);

    convertObjToXml(xmlConverter, domain, baos);
    String sbStr = baos.toString()
        .replaceAll("&lt;", "<")
        .replaceAll("&gt;", ">")
        // domainControlFile Class는 XmlRootElement이지만 파일에서는 tag로 나타나면 안되므로 수동으로 삭제
        .replaceAll("<domainControlFile>\n", "")
        .replaceAll("</domainControlFile>\n", "");
    createTxtFile(modelPath + ".domainControl.txt", sbStr);

    // 2. _prepattern.txt
    double svmThreshold = 0.35;
    if (model.getSvmThreshold() != 0) {
      svmThreshold = model.getSvmThreshold();
    }
    createTxtFile(modelPath + "_prepattern.txt",
        "@SVM_Threshold=" + String.valueOf(svmThreshold));

    // 3. _replacement.txt
    createTxtFile(modelPath + "_replacement.txt", "");

    outStreamClose(baos);

  }

  private UtteranceSetTag setUtteranceTag(List<UtteranceSet> utteranceSetList) {

    UtteranceSetTag utteranceSetTag = new UtteranceSetTag();
    List<UtteranceTag> utteranceTagList = new ArrayList<>();

    // transaction의 진입/재진입 발화는 SystemAction이 여러개면 utterance_set 내 utterance가 반복

    // progress 경우, 여러개 등록되면 dialog_node 자체가 반복
    // 하여 utterranceSet 이 여러개인 경우는 없는 것으로 관찰되지만, 배열이므로 일단 for문으로 처리
    for (UtteranceSet utteranceSet : utteranceSetList) {

      UtteranceTag utteranceTag = new UtteranceTag();
      utteranceTag.setCondition(utteranceSet.getCondition());
      utteranceTag.setAction(utteranceSet.getAction());

      TemplateTag templateTag = new TemplateTag();
      templateTag.setWeight(1.0);

      IntentionTag intentionTag = new IntentionTag();
      intentionTag.setDa(utteranceSet.getSystemIntent());

      if (utteranceSet.getPatternsList() != null) {
        intentionTag.setPattern(utteranceSet.getPatternsList());
      }
      templateTag.setIntention(intentionTag);
      utteranceTag.setTemplate(templateTag);
      utteranceTagList.add(utteranceTag);
    }

    utteranceSetTag.setUtterance(utteranceTagList);

    return utteranceSetTag;

  }

  private void createSql(List<EntityClass> entityClassList, char slotDelimiter, String modelPath)
      throws Exception {
    StringBuilder sb = new StringBuilder();

    sb.append("PRAGMA foreign_keys=OFF;\n");
    sb.append("BEGIN TRANSACTION;\n");

    // CANONOCAL_FORM (동의어)
    sb.append("DROP TABLE canonical_form;\n");
    sb.append("CREATE TABLE canonical_form "
        + "(_idx TEXT DEFAULT '', "
        + "_key_from TEXT DEFAULT '', "
        + "_key_to TEXT DEFAULT '', "
        + "slot_from TEXT DEFAULT '', "
        + "slot_to TEXT DEFAULT '', "
        + "_norm_type TEXT DEFAULT '');\n");

    // task_information에 필요
    Map<String, String> entityIdNmMap = new HashMap<>();
    int iCntCF = 1;
    for (EntityClass entityClass : entityClassList) {

      for (Entity entity : entityClass.getEntitiesList()) {
        String entityFullName = String
            .format("%s%c%s", entityClass.getName(), slotDelimiter, entity.getName());

        entityIdNmMap.put(entity.getId(), entityFullName);

        for (EntityData entityData : entity.getEntityDatasList()) {

          // EX) INSERT INTO canonical_form (_idx, slot_from, slot_to) VALUES ('1', 'PizzaInfo$type="Cheeze"', 'PizzaInfo$type="Cheese"');
          for (String synonym : entityData.getSynonymsList()) {
            sb.append(String.format(
                "INSERT INTO canonical_form (_idx, slot_from, slot_to) "
                    + "VALUES ('%s', '%s=\"%s\"', '%s=\"%s\"');\n",
                iCntCF, entityFullName, entityData.getEntityValue(),
                entityFullName, synonym));
            iCntCF++;
          }
        }
      }
    }

    // TASK_INFORMATION (instances)
    sb.append("DROP TABLE task_information;\n");

    // EX ) CREATE TABLE task_information (_idx TEXT DEFAULT '', 'PizzaInfo$type' TEXT DEFAULT '', 'PizzaInfo$size' TEXT DEFAULT '', 'PizzaInfo$price' TEXT DEFAULT '',
    //'Weather$City' TEXT DEFAULT '', 'Weather$Date' TEXT DEFAULT '');
    StringBuilder taskInfoColumns = new StringBuilder();
    for (String key : entityIdNmMap.keySet()) {
      taskInfoColumns.append(", '");
      taskInfoColumns.append(entityIdNmMap.get(key));
      taskInfoColumns.append("' TEXT DEFAULT ''");
    }

    sb.append(String
        .format("CREATE TABLE task_information (_idx TEXT DEFAULT ''%s);\n",
            taskInfoColumns.toString()));

    int iCntTI = 1;
    for (EntityClass entityClass : entityClassList) {

      for (EntityInstance instance : entityClass.getInstancesList()) {

        StringBuilder insertColumnNm = new StringBuilder();
        StringBuilder insertValues = new StringBuilder();

        // INSERT INTO task_information (_idx, 'PizzaInfo$type', 'PizzaInfo$size', 'PizzaInfo$price') VALUES ('1', 'cheese', 'large', '15000');
        for (InstanceDataMap dataMap : instance.getInstanceDataMapsList()) {

          insertColumnNm.append(", '");
          insertColumnNm.append(entityIdNmMap.get(dataMap.getEntityId()));
          insertColumnNm.append("'");

          insertValues.append(", '");
          insertValues.append(dataMap.getValue());
          insertValues.append("'");

        }
        sb.append(String.format(
            "INSERT INTO task_information (_idx%s) VALUES ('%s'%s);\n", insertColumnNm, iCntTI,
            insertValues));
        iCntTI++;
      }
    }

    sb.append(
        "CREATE INDEX canonical_form_idx ON canonical_form ('_idx','_key_from','_key_to','slot_from','slot_to','_norm_type');\n");
    sb.append("CREATE INDEX task_information_idx ON task_information ('_idx');\n");

    // SLOT_MAPPING
    sb.append("DROP TABLE slot_mapping;\n");
    sb.append(
        "CREATE TABLE slot_mapping ("
            + "_idx TEXT DEFAULT '', "
            + "tagged_slot_key TEXT DEFAULT '', "
            + "tagged_slot_value TEXT DEFAULT '', "
            + "query_cond TEXT DEFAULT '', "
            + "db_slot_key TEXT DEFAULT '', "
            + "db_slot_value TEXT DEFAULT '');\n");

    // TODO: SLOT_MAPPING data 채우기

    sb.append("COMMIT;");

    createTxtFile(modelPath + ".sql", sb.toString());

    // sqlite3 DB 생성
    execSqlite(modelPath);

  }

  private void createTxtFile(String filePath, String content) throws Exception {

    try {
      FileWriterWithEncoding writer = new FileWriterWithEncoding(filePath, "UTF-8");
      writer.write(content);
      writer.close();
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }

  private void execSqlite(String modelPath) throws Exception {
    try {
      Runtime rt = Runtime.getRuntime();

      String[] cmd = new String[]{"sh", "-c",
          String.format("sqlite3 %s_DB.sqlite3 < %s.sql", modelPath, modelPath)};
      Process proc = rt.exec(cmd);
      InputStream is = proc.getInputStream();
      InputStreamReader isr = new InputStreamReader(is);
      BufferedReader br = new BufferedReader(isr);

      String line;
      while ((line = br.readLine()) != null) {
        logger.debug(line);
      }
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }

  private void outStreamClose(OutputStream baos) throws Exception {
    try {
      baos.close();
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }

  private Marshaller getXmlConverter(Class objClass) throws Exception {

    JAXBContext context;
    Marshaller marshaller = null;
    try {
      context = JAXBContext.newInstance(objClass);

      marshaller = context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      // xml 선언 header 사용하지 않기
      marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);

    } catch (JAXBException e) {

      logger.error("cannot make XML Converter");
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }

    return marshaller;
  }

  private void convertObjToXml(Marshaller marshaller, Object object, OutputStream out)
      throws Exception {
    try {
      marshaller.marshal(object, out);
      out.write('\n');
      out.write('\n');
    } catch (JAXBException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }

  private Process startTrain(String domainName, Lang lang, String trainKey) throws Exception {

    String enginePath = PropertyManager.getString("brain-sds-trainer.engine.dir");
    String dialogDomainPath = getDialogDomainPath(lang);

    String txtPath = String.format("%s/%s/%s.txt", dialogDomainPath, domainName, domainName);
    String logPath =
        String.format("%s/%s/log/%s.learn_slu.log", dialogDomainPath, domainName, trainKey);

    String cmd;

    if (lang.equals(Lang.ko_KR)) {

      cmd = String.format("%s/dial "
              + "-cmd LEARN_SLU "
              + "-dic %s/KB/KOR "
              + "-domain %s "
              + "-fn %s "
              + "-log_file %s "
              + "-FILE_ENC %s "
              + "-access_key %s",
          enginePath, enginePath, domainName, txtPath, logPath, "UTF-8", null);

    } else {

      cmd = String.format("%s/dial.english "
              + "-cmd LEARN_SLU "
              + "-dic %s/KB/ENG "
              + "-domain %s "
              + "-fn %s "
              + "> %s 2>&1",
          enginePath, enginePath, domainName, txtPath, logPath);
    }

    logger.debug(cmd);

    String[] totalCmd = {"/bin/sh", "-c", cmd};
    Runtime rt = Runtime.getRuntime();
    Process trainProc;
    try {
      trainProc = rt.exec(cmd);
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }

    return trainProc;
  }

  private File getTrainLogFile(String domainName, Lang lang, String trainKey) {
    String dialogDomainPath = getDialogDomainPath(lang);
    String logPath =
        String.format("%s/%s/log/%s.learn_slu.log", dialogDomainPath, domainName, trainKey);
    File logFile = new File(logPath);

    if (logFile.exists()) {
      return logFile;
    } else {
      return null;
    }
  }

  private List<String> getTrainLog(File logFile) {

    List<String> trainLog = new ArrayList<>();
    InputStream is;
    InputStreamReader isr;
    BufferedReader brr = null;
    try {
      is = new FileInputStream(logFile);
      isr = new InputStreamReader(is);
      brr = new BufferedReader(isr);

      String line = brr.readLine();

      // 현재 진행된 상태까지만 로그 전달
      while (line != null) {
        trainLog.add(line);
        line = brr.readLine();
      }

    } catch (FileNotFoundException e) {
      logger.error("{} => ", e.getMessage(), e);
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
    } finally {
      try {
        brr.close();
      } catch (IOException e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }

    return trainLog;
  }

  class TrainTask implements Runnable {

    private SdsModel model;
    private Timestamp startTime;
    private Timestamp endTime;
    private SdsTrainStep trainStep;
    private SdsTrainAction trainLevel;
    private SdsTrainResult trainResult;
    private Process trainProc;

    private Timestamp getCurrentTimeStamp() {
      Instant time = Instant.now();
      Timestamp timestamp = Timestamp.newBuilder().setSeconds(time.getEpochSecond())
          .setNanos(time.getNano()).build();

      return timestamp;
    }

    public TrainTask(SdsTrainModel request) {
      this.model = request.getModel();
      this.startTime = getCurrentTimeStamp();
      this.trainStep = SdsTrainStep.SDS_TRAIN_START;
      this.trainLevel = request.getAction();
      this.trainResult = SdsTrainResult.TRAINING;
    }

    public SdsModel getModel() {
      return model;
    }

    public synchronized SdsTrainStep getTrainStep() {
      return trainStep;
    }

    public Timestamp getStartTime() {
      return startTime;
    }

    public synchronized Timestamp getCurrentEndTime() {
      // 완료시 완료시각, 미완료시 현시각 리턴
      if (endTime != null) {
        return endTime;
      } else {
        return getCurrentTimeStamp();
      }
    }

    public Duration getElapsedTime() {
      return between(startTime, getCurrentEndTime());
    }

    public synchronized SdsTrainAction getTrainLevel() {
      return trainLevel;
    }

    public synchronized SdsTrainResult getTrainResult() {
      return trainResult;
    }

    private synchronized void setTrainStep(SdsTrainStep step) {
      trainStep = step;
    }

    private synchronized void setTrainResult(SdsTrainResult result) {
      trainResult = result;
    }

    private synchronized void setEndTime(Timestamp time) {
      this.endTime = time;
    }

    @Override
    public void run() {

      setTrainStep(SdsTrainStep.SDS_TRAIN_SAVE_KNOWLEDGE_FILES);
      try {
        saveKnowledgeFile(model);
        logger.debug("SDS knowledge files saved");
      } catch (Exception e) {
        logger.error("Fail to save knowledge file. \n{}", e.getMessage());
        setTrainResult(SdsTrainResult.FAILED);
        setEndTime(getCurrentTimeStamp());
      }

      if (trainLevel.equals(SdsTrainAction.ALL) && !trainResult.equals(SdsTrainResult.FAILED)) {

        setTrainStep(SdsTrainStep.SDS_TRAIN_LEARN_SLU);

        // 모델 학습
        String trainKey = trainKeyMap.get(model.getId());
        try {
          trainProc = startTrain(model.getName(), model.getLang(), trainKey);
          logger.debug("SLU training ...");
          trainProc.waitFor();

          // train log 생성 안 된 경우
          if (getTrainLogFile(model.getName(), model.getLang(), trainKey) == null) {
            throw new IOException("Log File is not created. Check engine dial.");
          }

          logger.debug("SLU train done.");

        } catch (Exception e) {
          logger.error("Fail to train SLU. \n{}", e.getMessage());
          setTrainResult(SdsTrainResult.FAILED);
          setEndTime(getCurrentTimeStamp());
        }
      }

      if (!trainResult.equals(SdsTrainResult.FAILED) && !trainResult.equals(SdsTrainResult.CANCELLED)) {
        setTrainStep(SdsTrainStep.SDS_TRAIN_DONE);
        setTrainResult(SdsTrainResult.SUCCESS);
        setEndTime(getCurrentTimeStamp());
      }

      // 학습 종료 시 map에서 지우기
      synchronized (this) {
        trainingModelIds.remove(model.getId());
      }

    }

    // synchronized 함수 내에서 사용되는 경우가 아니면 synchronized 붙여줘야.
    private long getPidOfProcess(Process p) {
      long pid = -1;

      try {
        if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
          Field f = p.getClass().getDeclaredField("pid");
          f.setAccessible(true);
          pid = f.getLong(p);
          f.setAccessible(false);
        }
      } catch (Exception e) {
        pid = -1;
      }
      return pid;
    }

    private boolean isAlive(Process p) {
      try {
        p.exitValue();
        return false;
      }
      catch (IllegalThreadStateException e) {
        return true;
      }
    }

    public synchronized void stopProcess() {

      Process proc = trainProc;

      if (proc == null || !isAlive(proc)) {
        logger.debug("Train process is not running.");
        return;
      }

      long pid = getPidOfProcess(proc);
      if (pid == -1) {
        logger.debug("PID is -1 . exit.");
        return;
      }

      trainProc.destroy();
      logger.debug("Train {} stopped.", pid);
    }
  }
}
