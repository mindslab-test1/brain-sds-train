package ai.maum.brain.sds.train.element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import lombok.Getter;
import lombok.Setter;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlRootElement(name="slot")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder =
    {"name", "source", "type", "description", "task", "sense", "precedingSlots", "valueDefine"})
public class EntityTag {

  @XmlElement(name="name")
  String name;
  @XmlElement(name="source")
  String source;
  @XmlElement(name="type")
  String type = "";
  @XmlElement(name="description")
  String description = "";
  @XmlElement(name="task")
  String task = "";
  @XmlElement(name="sense")
  String sense = "";
  @XmlElement(name="preceding_slots")
  String precedingSlots = "";
  @XmlElement(name="value_define")
  ValueDefineTag valueDefine;

  static class ValueDefineTag {

    String condition;
    String target_slot;
    String action;
  }

}

