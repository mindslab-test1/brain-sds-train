package ai.maum.brain.sds.train.interfaces;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CookieRepository implements CookieStore {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private Map<URI, Set<HttpCookie>> allCookies = new HashMap<>();


  private boolean checkDomainsMatch(String cookieHost, String requestHost) {
    return requestHost.equals(cookieHost) || requestHost.endsWith("." + cookieHost);
  }

  private boolean checkPathsMatch(String cookiePath, String requestPath) {
    return requestPath.equals(cookiePath) ||
        (requestPath.startsWith(cookiePath) && cookiePath.charAt(cookiePath.length() - 1) == '/') ||
        (requestPath.startsWith(cookiePath)
            && requestPath.substring(cookiePath.length()).charAt(0) == '/');
  }

  private URI cookieUri(URI uri, HttpCookie cookie) {
    URI cookieUri = uri;
    if (cookie.getDomain() != null) {
      String domain = cookie.getDomain();
      if (domain.charAt(0) == '.') {
        domain = domain.substring(1);
      }
      try {
        cookieUri = new URI(uri.getScheme() == null ? "http" : uri.getScheme(),
            domain,
            cookie.getPath() == null ? "/" : cookie.getPath(), null);
      } catch (URISyntaxException e) {
        logger.warn("", e);
      }
    }
    return cookieUri;
  }

  @Override
  public synchronized void add(URI uri, HttpCookie cookie) {
    uri = cookieUri(uri, cookie);
    Set<HttpCookie> targetCookies = allCookies.computeIfAbsent(uri, k -> new HashSet<>());
    targetCookies.remove(cookie);
    targetCookies.add(cookie);
  }

  @Override
  public synchronized List<HttpCookie> get(URI uri) {
    List<HttpCookie> targetCookies = new ArrayList<>();

    String host = uri.getHost();
    if (host != null && !host.contains(".")) {
      host += ".local";
    }

    for (URI storedUri : allCookies.keySet()) {
      if (checkDomainsMatch(storedUri.getHost(), host)) {
        if (checkPathsMatch(storedUri.getPath(), uri.getPath())) {
          targetCookies.addAll(allCookies.get(storedUri));
        }
      }
    }

    return targetCookies;
  }

  @Override
  public synchronized List<HttpCookie> getCookies() {
    List<HttpCookie> allValidCookies = new ArrayList<>();
    allCookies.keySet().forEach(k -> allValidCookies.addAll(allCookies.get(k)));
    return allValidCookies;
  }

  @Override
  public synchronized List<URI> getURIs() {
    return new ArrayList<>(allCookies.keySet());
  }

  @Override
  public synchronized boolean remove(URI uri, HttpCookie cookie) {
    String host = uri.getHost();
    if (host != null && !host.contains(".")) {
      host += ".local";
    }

    for (URI storedUri : allCookies.keySet()) {
      if (checkDomainsMatch(storedUri.getHost(), host)) {
        if (checkPathsMatch(storedUri.getPath(), uri.getPath())) {
          return allCookies.get(storedUri).remove(cookie);
        }
      }
    }

    return false;
  }

  @Override
  public synchronized boolean removeAll() {
    if (allCookies.isEmpty()) {
      return false;
    }
    allCookies.clear();
    return true;
  }
}
