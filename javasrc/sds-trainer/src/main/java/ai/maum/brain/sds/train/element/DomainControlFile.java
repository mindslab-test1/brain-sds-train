package ai.maum.brain.sds.train.element;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainControlFile {
  @XmlElement(name="Chatbot")
  String chatbot;
  @XmlElement(name="SlotInit")
  String slotInitFake;
  @XmlElement(name="IntentControl")
  IntentControlTag intentControl;

  @Getter
  @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  // SlotInit클래스는 틀만 있고 실제로 DoaminControl class의 attribute로는 SlotInit이 xml화된 string을 사용.
  public static class SlotInit {

    List<SlotInitSetWrapper> slotInitSetWrapper;

    @Getter
    @Setter
    @XmlRootElement(name="temporarilySlotName")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class SlotInitSetWrapper {
      @XmlElement(name="set")
      SlotInitSet set;

      @Getter
      @Setter
      @XmlAccessorType(XmlAccessType.FIELD)
      public static class SlotInitSet {
        @XmlElement(name="condition")
        String condition;

        @XmlElement(name="value")
        String value;
      }
    }
  }

  @Getter
  @Setter
  @XmlAccessorType(XmlAccessType.FIELD)
  public static class IntentControlTag {

    @XmlElement(name="control")
    List<ControlTag> control;

    @Getter
    @Setter
    @XmlType(propOrder = {"systemIntent", "userResponse"})
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class ControlTag {
      @XmlElement(name="system_response")
      String systemIntent;
      @XmlElement(name="user_response")
      String userResponse;
    }
  }
}
