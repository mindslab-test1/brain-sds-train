package ai.maum.brain.sds.train;

import ai.maum.brain.sds.train.trainer.SdsTrainerImpl;
import ai.maum.util.PropertyManager;
import ai.maum.util.ThreadPool;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 학습단계
 *
 * <pre>
 * 1. 로그인
 * 2. 도메인 생성
 * 3. 도메인 구성(flow, slot, intent)
 * 4. 지식저장
 * 5. 학습
 * </pre>
 */
public class SdsTrainer {

  public static final Logger logger = LoggerFactory.getLogger(SdsTrainer.class);
  private Server server;

  /**
   * logVersions
   */
  private static void logVersion(boolean console) {
    try {
      String version = "{Version information is not found.}";
      Enumeration<URL> resources = SdsTrainer.class.getClassLoader()
          .getResources("META-INF/MANIFEST.MF");
      while (resources.hasMoreElements()) {
        Manifest manifest = new Manifest(resources.nextElement().openStream());
        Attributes attrs = manifest.getMainAttributes();
        if (attrs.getValue("BuildTime") != null) {
          version = attrs.getValue("Title") + " Version " + attrs.getValue("Version") + " / "
              + attrs.getValue("BuildTime");
          break;
        }
      }
      if (console) {
        logger.info(version);
      } else {
        logger.info(version);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  private void start() throws IOException, IllegalStateException {
    /* The port on which the server should run */
    int port = PropertyManager.getInt("brain-sds-trainer.listen.port");
    int grpcTimeout = PropertyManager.getInt("brain-sds-trainer.grpc.timeout");

    ThreadPool.init(5, 20, true);
    SdsTrainerImpl sdsTrainer = new SdsTrainerImpl();
    ServerBuilder serverBuilder = NettyServerBuilder
        .forPort(port)
        .maxConnectionIdle(grpcTimeout, TimeUnit.MILLISECONDS)
        .maxConnectionAge(grpcTimeout, TimeUnit.MILLISECONDS);
    server = serverBuilder
        .executor(ThreadPool.getExecutor())
        .addService(sdsTrainer)
        .addService(ProtoReflectionService.newInstance())
        .build()
        .start();
    logger.info("Server started, listening on {}", port);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its JVM shutdown hook.
      logger.info("*** shutting down gRPC server since JVM is shutting down");
      SdsTrainer.this.stop();
      logger.info("*** server shut down");
    }));
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }

  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    logVersion(false);

    final SdsTrainer server = new SdsTrainer();
    server.start();
    server.blockUntilShutdown();
  }


}
