package ai.maum.brain.sds.train.messages;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.Getter;

@Getter
public class ProjectListResponse extends ToString {

  @Getter
  public class ProjectDetail extends ToString {

    @SerializedName("DELETE_FLAG")
    private Boolean isDeleted;
    @SerializedName("SEQ_NO")
    private Integer seqNo;
  }

  @SerializedName("list")
  private List<ProjectDetail> details;
  @SerializedName("COUNT_ERROR")
  private Boolean errro;
}
