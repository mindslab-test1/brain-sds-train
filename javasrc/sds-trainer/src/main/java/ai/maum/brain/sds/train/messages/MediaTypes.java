package ai.maum.brain.sds.train.messages;

import okhttp3.MediaType;

public final class MediaTypes {

  public static MediaType PLAIN_TEXT = MediaType.parse("text/plain");

}
