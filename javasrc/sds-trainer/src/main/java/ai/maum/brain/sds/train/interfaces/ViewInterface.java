package ai.maum.brain.sds.train.interfaces;

import ai.maum.brain.sds.train.messages.LogResponse;
import ai.maum.brain.sds.train.messages.LoginResponse;
import ai.maum.brain.sds.train.messages.ProjectListResponse;
import ai.maum.brain.sds.train.messages.StatusResponse;
import ai.maum.brain.sds.train.messages.UserInsertResponse;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;


/**
 * View API interface
 */
public interface ViewInterface {

  /**
   * 사용자 추가
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/userinsert.go")
  Call<UserInsertResponse> addUser(@FieldMap Map<String, String> user);

  /**
   * 로그인 및 사용자 정보 획득
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/logincheck.go")
  Call<LoginResponse> login(@FieldMap Map<String, String> credentials);


  /**
   * 프로젝트 조회
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/selectprojectname.do")
  Call<ProjectListResponse> queryProject(@Field(value = "PROJECT_NAME") String projectName,
      @Field(value = "USER_SEQ_NO") int userSeqNo);

  /**
   * 프로젝트 생성
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/insertproject.do")
  Call<Void> createProject(@FieldMap Map<String, String> project);


  /**
   * 프로젝트 선택
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/setprojectlist.do")
  Call<Void> selectProject(@FieldMap Map<String, String> project);


  /**
   * 지식 저장
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/dialogslearnfilesave.do")
  Call<StatusResponse> saveDialogKnowledgeFile(@FieldMap Map<String, String> knowledgeFile);

  /**
   * 학습 실행
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/dialogslearning.do")
  Call<StatusResponse> learn(@Field(value = "UNIQID") String uniqueId);

  /**
   * 학습 진행 확인
   */
  @FormUrlEncoded
  @Headers({"Content-Type: application/x-www-form-urlencoded; charset=UTF-8"})
  @POST("view/dialogslearningchk.do")
  Call<LogResponse> checkLearning(@Field(value = "UNIQID") String uniqueId);
}
