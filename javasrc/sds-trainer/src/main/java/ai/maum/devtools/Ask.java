package ai.maum.devtools;

import ai.maum.brain.sds.train.interfaces.RestClient;
import ai.maum.brain.sds.train.interfaces.ViewInterface;
import ai.maum.brain.sds.train.messages.Credentials;
import ai.maum.brain.sds.train.messages.DialogKnowledgeFile;
import ai.maum.brain.sds.train.messages.LogResponse;
import ai.maum.brain.sds.train.messages.LoginResponse;
import ai.maum.brain.sds.train.messages.Project;
import ai.maum.brain.sds.train.messages.ProjectInsert;
import ai.maum.brain.sds.train.messages.ProjectListResponse;
import ai.maum.brain.sds.train.messages.ProjectListResponse.ProjectDetail;
import ai.maum.brain.sds.train.messages.StatusResponse;
import ai.maum.brain.sds.train.messages.ToMap;
import ai.maum.brain.sds.train.messages.UserInsert;
import ai.maum.brain.sds.train.messages.UserInsertResponse;
import ai.maum.brain.sds.train.messages.UserType;
import ai.maum.util.Utils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;

@SuppressWarnings("Duplicates")
public class Ask {

  private static final Logger logger = LoggerFactory.getLogger(Ask.class);

  @SuppressWarnings("SpellCheckingInspection")
  private static final String API_KEY = "sfsdfdgijofehfsdfgsdf";
  private static final String PROJECT_NAME = "domain2";

  private static RestClient restClient;
  private static String uniqueId;
  private static int userSeqNo;
  private static Project project;

  private static void addUser(ViewInterface viewInterface) {
    @SuppressWarnings("SpellCheckingInspection")
    UserInsert user = new UserInsert()
        .setSeqNo(0)
        .setId("apeach")
        .setPassword("1234")
        .setName("어피치")
        .setType(UserType.GU)
        .setApiKey(API_KEY)
        .setRemarks("API 호출로 등록했음.");

    Call<UserInsertResponse> call = viewInterface.addUser(user.toMap());

    try {
      Response<UserInsertResponse> response = call.execute();
      if (response.isSuccessful()) {
        //noinspection ConstantConditions
        logger.debug("addUser response: {}", response.body().getStatus().getDescription());
      } else {
        logger.debug("addUser response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("addUser failed.", e);
    }

//    call.enqueue(new Callback<UserInsertResponse>() {
//      @Override
//      public void onResponse(Call<UserInsertResponse> call, Response<UserInsertResponse> response) {
//        logger.debug("response: Status = [{}]", response.body().getStatus().getDescription());
//        System.exit(0);
//      }
//
//      @Override
//      public void onFailure(Call<UserInsertResponse> call, Throwable t) {
//        logger.debug("user insert failed.", t);
//        call.cancel();
//        System.exit(0);
//      }
//    });
  }

  private static void login(ViewInterface viewInterface) {
    @SuppressWarnings("SpellCheckingInspection")
    Credentials credentials = new Credentials()
        .setId("apeach")
        .setPassword("1234");

    Call<LoginResponse> call = viewInterface.login(credentials.toMap());

    try {
      Response<LoginResponse> response = call.execute();
      if (response.isSuccessful()) {
        //noinspection ConstantConditions
        logger.debug("login response: {}", response.body().getStatus().getDescription());
        if (!"OK".equalsIgnoreCase(response.body().getStatus().toString())) {
          return;
        }
        logger.debug("userinfo: {}", response.body().getUserInfo());
        Map<String, String> userInfo = ToMap.from(response.body().getUserInfo());
        userInfo.put("USER_SEQ", userInfo.get("SEQ_NO"));
        userInfo.remove("SEQ_NO");
        userSeqNo = Integer.parseInt(userInfo.get("USER_SEQ"));
        restClient.addCookies(userInfo);
        uniqueId = Utils.getUniqueId();
        restClient.addCookie("UNIQID", uniqueId);
        restClient.addCookie("ProjectApiKey", userInfo.get("API_KEY"));
        restClient.addCookie("ProjectUserSeq", String.valueOf(userSeqNo));
      } else {
        logger.debug("login response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("login failed.", e);
    }
  }

  private static int queryProject(ViewInterface viewInterface) {
    Call<ProjectListResponse> call = viewInterface.queryProject(PROJECT_NAME, userSeqNo);

    try {
      Response<ProjectListResponse> response = call.execute();
      if (response.isSuccessful()) {
        logger.debug("queryProject response: succeeded");
        logger.debug("queryProject body: {}", response.body());
        ProjectListResponse projectList = response.body();
        if (projectList.getDetails().size() == 1) {
          ProjectDetail projectDetail = projectList.getDetails().get(0);

          Map<String, String> cookies = new HashMap<>();
          cookies.put("ProjectName", PROJECT_NAME);
          cookies.put("ProjectNo", String.valueOf(projectDetail.getSeqNo()));
          cookies.put("ProjectLangNo", "1");
          restClient.addCookies(cookies);

          project = new Project()
              .setSeqNo(projectDetail.getSeqNo())
              .setProjectName(PROJECT_NAME)
              .setLanguage(1)
              .setProjectUserSeq(userSeqNo)
              .setApiKey(API_KEY);
        }
        return projectList.getDetails().size();
      } else {
        logger.debug("queryProject response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("queryProject failed.", e);
    }

    return -1;
  }

  private static boolean createProject(ViewInterface viewInterface) {
    ProjectInsert newProject = new ProjectInsert()
        .setName("domain2")
        .setLanguage(1)
        .setUserSeq(userSeqNo)
        .setDelSeqNo(0)
        .setDescription("SDS-Trainer에서 생성한 것임.")
        .setFilteringReference(0.35f)
        .setUseChatbotYn('N')
        .setHasChatbotDataYn('N')
        .setApiKey(API_KEY)
        .setSelectApiKey(API_KEY);

    Call<Void> call = viewInterface.createProject(newProject.toMap());

    try {
      Response<Void> response = call.execute();
      if (response.isSuccessful()) {
        logger.debug("createProject response: succeeded");
        return true;
      } else {
        logger.debug("createProject response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("createProject failed.", e);
    }
    return false;
  }

  private static void selectProject(ViewInterface viewInterface) {
    Call<Void> call = viewInterface.selectProject(project.toMap());

    try {
      Response<Void> response = call.execute();
      if (response.isSuccessful()) {
        logger.debug("selectProject response: succeeded");
      } else {
        logger.debug("selectProject response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("selectProject failed.", e);
    }
  }

  /*
    LeanProjectName=scenario1;
    DialProjectName=scenario1;
  */
  private static void saveDialogKnowledgeFile(ViewInterface viewInterface) {
    DialogKnowledgeFile knowledgeFile = new DialogKnowledgeFile()
        .setProjectSeq(8)
        .setProjectName("scenario1")
        .setUniqueId(uniqueId);

    Call<StatusResponse> call = viewInterface.saveDialogKnowledgeFile(knowledgeFile.toMap());

    try {
      Response<StatusResponse> response = call.execute();
      if (response.isSuccessful()) {
        //noinspection ConstantConditions
        logger.debug("saveDialogKnowledgeFile response: {}",
            response.body().getStatus().getDescription());
      } else {
        logger.debug("saveDialogKnowledgeFile response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("saveDialogKnowledgeFile failed.", e);
    }
  }

  private static void learn(ViewInterface viewInterface) {
    Call<StatusResponse> call = viewInterface.learn(uniqueId);

    try {
      Response<StatusResponse> response = call.execute();
      if (response.isSuccessful()) {
        //noinspection ConstantConditions
        logger.debug("learn response: {}", response.body().getStatus().getDescription());
      } else {
        logger.debug("learn response: failed. [{}]", response);
      }
    } catch (IOException e) {
      logger.debug("learn failed.", e);
    }
  }

  private static void checkLearning(ViewInterface viewInterface) {
    int nullCount = 0;
    while (true) {
      Call<LogResponse> call = viewInterface.checkLearning(uniqueId);
      try {
        Response<LogResponse> response = call.execute();
        if (response.isSuccessful()) {
          //noinspection ConstantConditions
          logger.debug("checkLearning response: {}", response.body().getStatus().getDescription());
          String log = response.body().getLog();
          if (log.endsWith("<br/>")) {
            log = log.substring(0, log.length() - 5);
            int index = log.lastIndexOf("<br/>");
            log = log.substring(index < 0 ? 0 : index + 5);
            logger.debug("checkLearning log: {}", log);
          } else if (log.equals("")) {
            nullCount++;
            if (nullCount > 3) {
              break;
            }
          } else if (log.equals("FINISHED")) {
            break;
          }
        } else {
          logger.debug("checkLearning response: failed. [{}]", response);
          break;
        }
      } catch (IOException e) {
        logger.debug("checkLearning failed.", e);
        break;
      }

      try {
        TimeUnit.MILLISECONDS.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    restClient = new RestClient().setup("http://localhost:8100", false);
    ViewInterface viewInterface = restClient.create(ViewInterface.class);

//    addUser(viewInterface);

    login(viewInterface);

    int result = queryProject(viewInterface);

    if (result < 0) {
      return;
    }

    if (result == 0) {
      logger.info("Create a new Project.");
      if (!createProject(viewInterface)) {
        return;
      }
      queryProject(viewInterface);
    } else {
      logger.info("Work with the existing Project.");
    }

    selectProject(viewInterface);

    saveDialogKnowledgeFile(viewInterface);

    learn(viewInterface);

    checkLearning(viewInterface);
  }
}
