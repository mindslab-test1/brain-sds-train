package ai.maum.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;

public final class Utils {

  public static String getStackTrace(Throwable e) {
    StringWriter msg = new StringWriter();
    e.printStackTrace(new PrintWriter(msg));
    return msg.toString();
  }

  public static String getUniqueId() {
    return Long.toHexString(UUID.randomUUID().getLeastSignificantBits());
  }
}
