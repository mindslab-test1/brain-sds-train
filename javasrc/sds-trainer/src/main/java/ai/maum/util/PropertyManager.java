package ai.maum.util;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyManager {

  private static final Logger logger = LoggerFactory.getLogger(PropertyManager.class);

  private static final String maumRootEnv = "MAUM_ROOT";
  private static final String configPath = "/etc/";
  private static String configFilename = "brain-sds-train.conf";

  private static Configuration propertyConfig = null;

  private static void load() {
    if (propertyConfig == null) {
      logger.debug("### Load config file [{}].", configFilename);
      propertyConfig = getConfig();
    }
  }

  private static Configuration getConfig() {
    Configuration config = null;
    String confPath = getSystemConfPath();

    logger.debug("---------------------------------------");
    logger.debug("getSystemConfPath :: {}", confPath);
    logger.debug("getSystemConfFileName :: {}", configFilename);
    logger.debug("---------------------------------------");

    try {
      final File file = new File(confPath, configFilename);
      final Parameters params = new Parameters();
      final FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
          new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
              .configure(params.properties().setFile(file));

      config = builder.getConfiguration();
    } catch (ConfigurationException e) {
      logger.debug(e.getMessage());
      System.err.println("Cannot load config " + confPath + configFilename);
      System.exit(1);
    } catch (Exception e) {
      logger.debug(e.getMessage());
      e.printStackTrace();
      System.err.println("Cannot load config " + confPath + configFilename);
      System.exit(1);
    }
    return config;
  }

  private static String getSystemConfPath() {
    String path = System.getenv(maumRootEnv);
    if (path == null) {
      throw new IllegalStateException("MAUM_ROOT environment variable is not defined!");
    }
    path += configPath;
    return path;
  }

  public static String getString(String propertyName) {
    load();
    return propertyConfig.getString(propertyName);
  }

  public static String[] getStringArray(String propertyName) {
    load();
    return propertyConfig.getStringArray(propertyName);
  }

  public static int getInt(String propertyName) {
    load();
    return propertyConfig.getInt(propertyName);
  }

  public static double getDouble(String propertyName) {
    load();
    return propertyConfig.getDouble(propertyName);
  }

  public static long getLong(String propertyName) {
    load();
    return propertyConfig.getLong(propertyName);
  }

  public static float getFloat(String propertyName) {
    load();
    return propertyConfig.getFloat(propertyName);
  }

  public static BigDecimal getBigDecimal(String propertyName) {
    load();
    return propertyConfig.getBigDecimal(propertyName);
  }

  public static List<Object> getList(String propertyName) {
    load();
    return propertyConfig.getList(propertyName);
  }

  public static Boolean getBoolean(String propertyName) {
    load();
    return propertyConfig.getBoolean(propertyName);
  }

}
