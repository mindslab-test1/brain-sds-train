package ai.maum.util;

import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.MetadataUtils;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class GrpcClient<T extends AbstractStub> {

  private static final Logger logger = LoggerFactory.getLogger(GrpcClient.class);
  private static final long clientKeepAliveTime = 20 * 60 * 1000L; // 20 mins
  private static final long clientApplicableGap = 10 * 1000L; // 10 seconds
  protected T originalStub;
  protected T stub;
  private ManagedChannel originalChannel;
  private volatile long aliveTo;

  protected GrpcClient() {
  }

  protected abstract T newStub(final Channel underlyingChannel);

  protected void init(String host, int port, Executor executor) {
    originalChannel = NettyChannelBuilder
        .forAddress(host, port)
        .usePlaintext()
        .executor(executor)
        .directExecutor()
        .build();

    originalStub = newStub(ClientInterceptors.intercept(originalChannel));
    originalStub.withWaitForReady().withDeadlineAfter(1, TimeUnit.SECONDS);
    refreshLifetime();
  }

  /**
   * shutdown
   * <pre>
   * Shutdown GRPC Channel
   * </pre>
   */
  protected void shutdown() {
    originalChannel.shutdown();
    originalChannel = null;
    originalStub = null;
    stub = null;
  }

  /**
   * refreshLifetime
   */
  protected void refreshLifetime() {
    aliveTo = System.currentTimeMillis() + clientKeepAliveTime;
  }

  /**
   * isApplicable
   *
   * @return boolean value whether is applicable.
   */
  protected boolean isApplicable() {
    if (!originalChannel.isShutdown() && !originalChannel.isTerminated()) {
      if (aliveTo > System.currentTimeMillis()) {
        aliveTo += clientApplicableGap;
        return true;
      }
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  public void attachHeaders(Metadata headers) {
    stub = (T) MetadataUtils.attachHeaders(originalStub, headers);
  }

  public void clearHeaders() {
    stub = originalStub;
  }

}
