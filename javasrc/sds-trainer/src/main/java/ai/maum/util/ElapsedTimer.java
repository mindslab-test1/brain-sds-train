package ai.maum.util;

public class ElapsedTimer {

  private static final long NOT_STARTED = -1;

  long started = NOT_STARTED;

  public ElapsedTimer() {
  }

  public ElapsedTimer(long started) {
    start(started);
  }

  public void start() {
    this.started = System.currentTimeMillis();
  }

  public void start(long start) {
    this.started = System.currentTimeMillis();
  }

  public long getTookMillis() {
    return started > 0 ? System.currentTimeMillis() - started : NOT_STARTED;
  }

  public long getTookMillis(long finish) {
    return started > 0 ? finish - started : NOT_STARTED;
  }
}