<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js?ver=9'/>"></script>
	<script type="text/javascript">
		$(function () {					
			if (!gfn_isNull(window.frameElement)) {
				if ('${param.Type}' == 'session') {
					alert('장시간 사용하지 않아 로그아웃 되었습니다.\n로그인 후 다시 이용해 주세요.');
					if (gfn_isNull(self.opener)) {
						window.parent.fnlogout();	
					}
					else {
						window.opener.parent.fnlogout();
						window.close();
					}						
				}
				else {
					alert('작업 중 오류가 발생 하였습니다.\n오류가 계속되면 관리자에게 문의해 주세요.');
					if (gfn_isNull(self.opener)) {
						window.parent.location.reload();
					}
					else {
						window.opener.parent.location.reload();
						window.close();
					}						
				}							
			}
			else {
				if ('${param.Type}' == 'session') {
					alert('장시간 사용하지 않아 로그아웃 되었습니다.\n로그인 후 다시 이용해 주세요.');
					if (gfn_isNull(self.opener)) {
						window.fnlogout();	
					}
					else {
						if (!gfn_isNull(window.opener.frameElement))
							window.opener.parent.fnlogout();
						else
							window.opener.fnlogout();
						window.close();
					}
				}
				else {
					var comSubmit = new ComSubmit();			
					comSubmit.setUrl("<c:url value='/view/main.go' />");
					comSubmit.submit();	
				}				
			}								
		});
	</script>
</head>
<body>
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>
