<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
<script type="text/javascript">
		window.onload = function() {
			fnSetNaviTitle('Domain List');
		}
		
		function fnGoDetail() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/useruttrwrite.do' />");
			comSubmit.submit();
		}
		
		function fnOpenCreatDatype() {		
			fnGoDetail();
			fnpreventDefault(event);
		}				
		
		function fnProejctDelete(prjseq, prjname, delflag, langseq, userseq, apikey) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deletproject.do' />");		
				comSubmit.addParam("SEQ_NO", prjseq);
				comSubmit.addParam("PROJECT_NAME", prjname);
				comSubmit.addParam("DELETE_FLAG", delflag);
				comSubmit.addParam("LANG_SEQ_NO", langseq);
				comSubmit.addParam("PROJECT_USER_SEQ", userseq);
				comSubmit.addParam("API_KEY", apikey);
				comSubmit.submit();
			}
			fnstopPropagation(event);
		}
		
		function fnProjectUndo(prjseq) {
			if (confirm('해당 항목을 복원 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/updateprojectdelflag.do' />");		
				comSubmit.addParam("SEQ_NO", prjseq);				
				comSubmit.addParam("DELETE_FLAG", "N");
				comSubmit.submit();
			}
			fnstopPropagation(event);
		}
	</script>
</head>
<body>
<div id="wrap">
	<%@ include file="/WEB-INF/include/left.jspf"%>
	<div class="middleW entities domain">
		<span style="margin: 25px 0 0 5px; font-size: 13px; ">사용자당 ${item.DOMAIN_COUNT }개 도메인으로 제한하고 있습니다.</span>
		<div class="btnBox">
			<div id="divCreateDomain" class="createDomain">CREATE DOMAIN</div>
		</div>
		<c:set var="UserSeq" value=""></c:set>
		<c:set var="EndChk" value=""></c:set>
		<c:forEach items="${prjlist }" var="row">											
			<c:if test='${row.DELETE_FLAG == "N"}'>		
				<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
					<c:if test="${EndChk eq   'Y'}">										
						<c:set var="EndChk" value="N"></c:set>
							</ul>
							</div>
						</div>
					</c:if>									
					<c:set var="EndChk" value="Y"></c:set>					
					<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
					<div class="liBox liBox4">
						<c:if test='${USER_TYPE eq "SA"}'>
							<div class="title">${row.USER_ID} / ${row.USER_NAME }</div>
						</c:if>
						<div class="ulBox">
						<ul>
				</c:if>								
				<li id="divUserSays" onclick="fnSelectProject(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.SERVER_PORT }', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO }, '${row.API_KEY }');">					
					<div style="float:left; width:50%;">${row.PROJECT_NAME }</div>
					<div style="float:left;">${row.DESCRIPTION }</div>
					<span onclick="fnProejctModify(${row.SEQ_NO});" class="modify po_a_mt"></span>
					<span onclick="fnProejctDelete(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.DELETE_FLAG}', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO }, '${row.API_KEY }');" class="remove po_a_rt"></span>
					<%-- <span onclick="fnProjectUndo(${row.SEQ_NO });" class="undo_btn po_a_ut"></span> --%>
				</li>	
			</c:if>
		</c:forEach>
		<c:if test="${UserSeq ne ''}">
				</ul>
			</div>
			</div>
		</c:if>
		<c:if test='${USER_TYPE eq "SA"}'>
			<c:set var="UserSeq" value=""></c:set>
			<c:set var="EndChk" value=""></c:set>
			<h3 style="font-size: 20px; font-weight: bold; line-height: 24px; margin: 30px 45px 10px 2px;">삭제된 도메인</h3>
			<c:forEach items="${delprjlist }" var="row">				
				<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
					<c:if test="${EndChk eq 'Y'}">										
						<c:set var="EndChk" value="N"></c:set>
						</ul>
						</div>
						</div>
					</c:if>
					<c:set var="EndChk" value="Y"></c:set>
					<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
					<div class="liBox liBox4">
						<div class="title">${row.USER_ID} / ${row.USER_NAME }</div>
						<div class="ulBox">
						<ul>
				</c:if>				
				<li onclick="fnSelectProject(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.SERVER_PORT }', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO }, '${row.API_KEY }');">
					<div>${row.PROJECT_NAME }</div>
					<span onclick="fnProejctModify(${row.SEQ_NO});" class="modify po_a_mt"></span>
					<span onclick="fnProejctDelete(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.DELETE_FLAG}', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO }, '${row.API_KEY }');" class="remove po_a_rt"></span>
					<span onclick="fnProjectUndo(${row.SEQ_NO });" class="undo po_a_ut"></span>
				</li>
			</c:forEach>
			<c:if test="${UserSeq ne ''}">
					</ul>
				</div>
				</div>
			</c:if>
		</c:if>
	</div>
</div>	
</body>
</html>