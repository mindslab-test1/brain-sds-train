<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">	
		var bSaveChk = true;
		var reg = /[^a-zA-Z0-9_-]/; 
		
		window.onload = function() {
			fnSetNaviTitle('Domain Information');
			if(!gfn_isNull('${PROJECT_NAME}')) {
				$("#hidPrjSeq").val('${SEQ_NO}');
				$("#txtProjectName").val('${PROJECT_NAME}');
				$("#txtDescription").val('${fn:replace(DESCRIPTION, "'", "\\'")}');
				$('#txtFilter').val('${FILTER_VALUE}');
				$('#divOther, #divSample').hide();
			}	
			else {
				$('#divVersion').hide();
				if ('${USER_TYPE}' == 'OA') {
					$('#divOther').hide();
				}
				else {
					//$('#divSample').hide();
				}
			}
					
			$("#ddlSubDomain").change(function(){
				var seqno = $(this).val();				
			
				if (seqno != '') {
					if (seqno == '0') {
						$('#aLoadVersion').hide();
						$('#txtVersionName').val('');
					}
					else {
						$('#aLoadVersion').show();
						$('#txtVersionName').val($(this).find('option:selected').text());
					}
					$('#hidVersionSeq').val(seqno);					
					fnLayerPop('divLayer', 'o');
				}												
		    });
			
			if ('${SEQ_NO}' == '') {				
				$('#ddlSubDomain').prop('disabled', 'disabled');
				$('[id^=langChk]').eq(0).prop("checked", true);
				$('#chatbotDataUse').prop("disabled", true);
			}
			else {
				$('#ddlOtherDomain').prop('disabled', 'disabled');
				
				if ('${FROM_SEQ_NO}' != '0') {				
					$('#ddlOtherDomain').empty().append('<option value="${FROM_SEQ_NO}">${FROM_PROJECT_NAME}</option>').val('${FROM_SEQ_NO}');					
				}
				$('#langChk${LANG_SEQ_NO}').prop("checked", true);
				$('[id^=langChk]').prop("disabled", true);
				if ('${CHATBOT_USE_FLAG}' == 'Y') { 
					$('#chatbotUse').prop("checked", true);
					if ('${CHATBOT_DATA_FLAG}' == 'Y') 
						$('#chatbotDataUse').prop("checked", true);
				}
				else
					$('#chatbotDataUse').prop("disabled", true);				
			}		
			
			<%--if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {--%>
				<%--$('#divLanguage').hide();--%>
				<%--$('#divChatbot').hide();--%>
			<%--}					--%>

		  $('#divChatbot').hide();
		}
		
		function fnCheck(obj, type) {
			if (type == 'L') {
				$('[id^=langChk]').prop("checked", false);
				$(obj).prop("checked", true);
			}
			else if (type == 'S') {
				if ($(obj).prop("checked")) {
					$('[id^=sampleChk]').prop("checked", false);
					$(obj).prop("checked", true);	
				}				
				if ($('#txtProjectName').val().trim() == '') {
					$('#txtProjectName').val($('#' + $(obj).attr('id').replace('sampleChk', 'hidSamplePrjName')).val());
				}
			}
			else {
				if ($(obj).prop('checked') == true) {
					$(obj).prop("checked", true);
					if ($(obj).attr('id') == 'chatbotUse') {					
						$('#chatbotDataUse').prop("disabled", false);
					}
				}
				else {
					$(obj).prop("checked", false);
					if ($(obj).attr('id') == 'chatbotUse') {					
						$('#chatbotDataUse').prop("checked", false).prop("disabled", true);
					}
				}							
			}						
		}
		
		function fnGetLangSeq() {
			var langSeq = '';
			
			$('[id^=langChk]').each(function() {
				if ($(this).prop('checked') == true) {
					langSeq = $(this).val(); 
				}
			});
			
			if (langSeq == '') langSeq = '1';
						
			return langSeq;
		}
		function fnSave(){
			if (bSaveChk) {
				bSaveChk = false;
				
				var ProejctName = $('#txtProjectName').val().trim();
																
				if(ProejctName == '' || ProejctName == 'Domain name') {
					alert('도메인 명을 입력하세요.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;	
				}
				
				if (ProejctName.length > 50) {
					alert('도메인 명을 50자 이하로 입력해 주세요.');
					fnpreventDefault(event);
					return false;
				}
								
				if (reg.test(ProejctName)) {
					alert('도메인명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;
				}
				
				if ('${SEQ_NO}' == '') {
					if($('#txtDescription').val() == 'Describe your domain')
						$('#txtDescription').val('');
									
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/selectprojectname.do' />");
					comAjax.setCallback("fnSaveCallBack");
					comAjax.addParam("PROJECT_NAME", ProejctName);
					//comAjax.addParam("USER_SEQ_NO", getCookie("ProjectUserSeq"));
					comAjax.addParam("USER_SEQ_NO", '${USER_SEQ_NO}');
					comAjax.ajax();	
				}
				else							
					fnRealSave(0);
			}
			fnpreventDefault(event); 
		}
		
		function fnSaveCallBack(data) {
			if (data.COUNT_ERROR == 'N') {
				if (data.list.length == 0) {
					fnRealSave(0);	
				}
				else {
					if (data.list[0].DELETE_FLAG == 'N') {
						alert('이미 존재하는 도메인명 입니다.');
						bSaveChk = true;	
					}
					else
						fnRealSave(data.list[0].SEQ_NO);			
				}	
			}
			else {
				alert('생성 가능한 도메인 수를 초과 하였습니다.\n사용하지 않는 도메인을 제거 후 생성해 주세요.');
				bSaveChk = true;
			}
		}
		
		function fnRealSave(delseq) {
			fnLoading();
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertproject.do' />");
			comSubmit.addParam("PROJECT_NAME", $('#txtProjectName').val());
			comSubmit.addParam("DESCRIPTION", $('#txtDescription').val());
			comSubmit.addParam("SEQ_NO", $('#hidPrjSeq').val());
			comSubmit.addParam("OLD_NAME", $('#hidOldName').val());
			comSubmit.addParam("OLD_LANG_SEQ", $('#hidOldLang').val());
			if (!$('#ddlOtherDomain').prop('disabled') && $('#ddlOtherDomain').val() != '') {			
				comSubmit.addParam("PROJECT_SEQ", $('#ddlOtherDomain').val());
				comSubmit.addParam("FROM_SEQ_NO", $('#ddlOtherDomain').val());				
			}
			else {
				deleteCookie("ProjectUserSeq");
				setCookie("ProjectUserSeq", '${USER_SEQ_NO}');
			}
			
			$('[name=sampleChk]').each(function () {
				if ($(this).prop('checked')) {
					comSubmit.addParam("PROJECT_SEQ", $(this).val());
					comSubmit.addParam("FROM_SEQ_NO", $(this).val());	
				}
			});
			
			if ($('#chatbotUse').prop('checked') == true)
				comSubmit.addParam("CHATBOT_USE_FLAG", 'Y');	
			else
				comSubmit.addParam("CHATBOT_USE_FLAG", 'N');
			
			if ($('#chatbotDataUse').prop('checked') == true)
				comSubmit.addParam("CHATBOT_DATA_FLAG", 'Y');	
			else
				comSubmit.addParam("CHATBOT_DATA_FLAG", 'N');
			comSubmit.addParam("FILTER_VALUE", $('#txtFilter').val());
			comSubmit.addParam("LANG_SEQ_NO", fnGetLangSeq());
			comSubmit.addParam("PROJECT_USER_SEQ", '${USER_SEQ_NO}');
			comSubmit.addParam("DEL_SEQ_NO", delseq);
			comSubmit.addParam("API_KEY", getCookie('API_KEY'));
			comSubmit.addParam("SELECT_API_KEY", getCookie('ProjectApiKey'));
			comSubmit.submit();
		}
		
		function fnSaveVersion() {				
			var VersionName = $('#txtVersionName').val();
									
			if (VersionName.trim() == '') {
				alert('버전 이름을 입력해주세요.');
				return;
			}			
			
			if (reg.test(VersionName)) {
				alert('버전명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$('#txtVersionName').focus();
				fnpreventDefault(event);
				return;
			}
			
			if (bSaveChk) {
				bSaveChk = false;
				fnLoading();
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/saveversion.do' />");
				comSubmit.addParam("PROJECT_SEQ", '${SEQ_NO}');
				comSubmit.addParam("PROJECT_NAME", '${PROJECT_NAME}');
				comSubmit.addParam("DESCRIPTION", '${fn:replace(DESCRIPTION, "'", "\\'")}');
				comSubmit.addParam("USER_SEQ", '${USER_SEQ_NO}');
				comSubmit.addParam("VERSION_SEQ", $('#hidVersionSeq').val());
				comSubmit.addParam("VERSION_NAME", VersionName);			
				if ($('#chatbotUse').prop('checked') == true)
					comSubmit.addParam("CHATBOT_USE_FLAG", 'Y');	
				else
					comSubmit.addParam("CHATBOT_USE_FLAG", 'N');
				
				if ($('#chatbotDataUse').prop('checked') == true)
					comSubmit.addParam("CHATBOT_DATA_FLAG", 'Y');	
				else
					comSubmit.addParam("CHATBOT_DATA_FLAG", 'N');
				comSubmit.addParam("FILTER_VALUE", $('#txtFilter').val());
				comSubmit.addParam("LANG_SEQ_NO", fnGetLangSeq());
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnLoadVersion() {
			if (bSaveChk) {
				bSaveChk = false;
				fnLoading();
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/loadversion.do' />");
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
				comSubmit.addParam("PROJECT_SEQ", $('#hidVersionSeq').val());
				comSubmit.addParam("PROJECT_NAME", '${PROJECT_NAME}');
				comSubmit.addParam("DESCRIPTION", '${fn:replace(DESCRIPTION, "'", "\\'")}');
				comSubmit.addParam("USER_SEQ", '${USER_SEQ_NO}');
				if ($('#chatbotUse').prop('checked') == true)
					comSubmit.addParam("CHATBOT_USE_FLAG", 'Y');	
				else
					comSubmit.addParam("CHATBOT_USE_FLAG", 'N');
				
				if ($('#chatbotDataUse').prop('checked') == true)
					comSubmit.addParam("CHATBOT_DATA_FLAG", 'Y');	
				else
					comSubmit.addParam("CHATBOT_DATA_FLAG", 'N');
				comSubmit.addParam("FILTER_VALUE", $('#txtFilter').val());
				comSubmit.addParam("LANG_SEQ_NO", fnGetLangSeq());
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnCancelVersion() {
			fnLayerPop('divLayer', 'c');			
			$('#ddlSubDomain').val('');
		}		
		
		function fnGoChatbot() {
			parent.window.location = "<c:url value='/view/chatbotmain.do' />" + "?USER_SEQ=" + getCookie("USER_SEQ");	
		}
	</script>
</head>
<body>
	<div class="layer" id="layer">
		<div id="bg" class="bg"></div>
		<div id="divLayer" class="popup newSave"  style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
			<div class="title">Save & Load Domain version</div>
			<div class="body">
				<div class="popInput">
					<div class="title">Version Name</div>
					<div class="input">
						<input id="txtVersionName" type="text" value="" name="txtVersionName" />
					</div>
				</div>
			</div>
			<div class="btnBox">
				<div onclick="fnSaveVersion();" class="save" id="aSaveVersion">Save</div>
				<div onclick="fnLoadVersion();" class="load" id="aLoadVersion">Load</div>
				<div onclick="fnCancelVersion();" class="cancle">Cancel</div>
			</div>
			<input type="hidden" id="hidVersionSeq" />
		</div>
	</div>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		
		<div class="middleW createTask">						
			<div class="saveName">
				<div class="title">Domain Name</div>
				<div class="box"><input type="text" id="txtProjectName" name="PROJECT_NAME" /><button type="button" onclick="fnSave();">Save</button></div>
			</div>
			<div class="selectBox">
				<div class="title">Description</div>
				<div class="box"><input id="txtDescription" name="DESCRIPTION" type="text" placeholder="Description your domain" /></div>
			</div>
			
			<div class="selectBox" id="divChatbot">
				<div class="title">챗봇 <span>사용자의 발화 의도를 인식하지 못한 경우, 챗봇을 이용하여 시스템 발화를 할지 결정합니다.</span></div>
				<div class="inputBox">
					<div class="check" style="margin: 0px 0px 10px 0px; ">
						<div class="title" style="width:150px;">챗봇 사용하기</div>
						<div class="right" style="padding-left:150px;">
							<input type="checkbox" onclick="fnCheck(this, 'C');" id="chatbotUse" name="chatbotUse" value="" style="margin: 8px;"/><label for="chatbotUse" style="vertical-align: text-top; line-height:22px;"></label>
						</div>
					</div>
					<div class="check" style="margin: 0px 0px 10px 0px; display:none;">
						<div class="title" style="width:280px;">시스템 제공 학습 데이터 같이 사용하기</div>
						<div class="right" style="padding-left:280px;">
							<input type="checkbox" onclick="fnCheck(this, 'C');" id="chatbotDataUse" name="chatbotDataUse" value="" style="margin: 8px;"/><label for="chatbotDataUse" style="vertical-align: text-top; line-height:22px;"></label>
						</div>
					</div>
				</div>
			</div>
			
			<div class="selectBox">
				<div class="title">대화의도 필터링 <span>낮은 신뢰도로 인식되는 대화의도를 인식하지 못한 경우(unknown 의도)로 설정하세요.</span></div>
				<div class="inputBox">
					<table>
						<tr>
							<td><input type="text" id="txtFilter" name="FILTER_VALUE" value="0.35" /></td>
							<td style="padding-left: 10px;"><span>일반적으로 0.3 ~ 0.4 이하는 인식된 의도의 가능성이 아닐 가능성이 높고, <br />0.8 이상은 매우 높아서 정의한 문장에 매우 유사한 경우입니다.</span></td>
						</tr>
					</table>			
				</div>
			</div>
			<div id="divVersion" class="selectBox">
				<div class="title">버전 관리<span>3개 버전만 관리 가능합니다.</span></div>
				<div class="box">
					<select id="ddlSubDomain" name="ddlSubDomain">
						<c:set var="iCnt" value="0"></c:set>
						<option value="">선택하세요</option>
						<c:forEach items="${VERSION_LIST }" var="row">
							<option value="${row.SEQ_NO}">${row.PROJECT_NAME }</option>
							<c:set var="iCnt" value="${iCnt + 1}"></c:set>
						</c:forEach>
			        	<c:if test="${iCnt != 3}">
			        		<option value="0">새로 저장하기</option>
			        	</c:if>
					</select>
				</div>
			</div>
			
			<div id="divOther" class="selectBox">
				<div class="title">Create from other saved domains</div>
				<div class="box">
					<select id="ddlOtherDomain" name="ddlOtherDomain">
						<option value="">선택하세요</option>
						<c:forEach items="${VERSION_LIST }" var="row">
							<option value="${row.SEQ_NO}">${row.PROJECT_NAME }</option>
						</c:forEach>                           	
					</select>
				</div>
			</div>
			
			<div id="divSample" class="selectBox">
				<div class="title">Sample Domain으로 시작하기</div>
				<div class="checkbox">
					<c:forEach items="${SAMPLE_LIST }" var="row">
						<div class="check" style="margin: 0px 0px 10px 0px; ">
							<div class="right">
								<input type="checkbox" class="checkbox_st" onclick="fnCheck(this, 'S');" id="sampleChk${row.SEQ_NO }" name="sampleChk" value="${row.PROJECT_SEQ }"/><label for="sampleChk${row.SEQ_NO }" id="sampleLable${row.SEQ_NO }">${row.SAMPLE_NAME }</label>
								<input type="hidden" id="hidSamplePrjName${row.SEQ_NO }" value="${row.PROJECT_NAME }" />
							</div>						
						</div>
					</c:forEach>
				</div>
			</div>
			
			<div class="selectBox conBox" id="divLanguage">
				<div class="title">LANGUAGE</div>
				<div class="inputBox">
					<div class="check" style="margin: 0px;">
						<div class="title">한국어</div>
						<div class="right">
							<input type="checkbox" onclick="fnCheck(this, 'L');" id="langChk1" name="langChk1" value="1" style="margin: 8px;"/><label for="langChk1" ></label>
						</div>
					</div>
					<div class="check" style="margin: 0px 0px 10px 0px;">
						<div class="title">English</div>
						<div class="right">
							<input type="checkbox" onclick="fnCheck(this, 'L');" id="langChk2" name="langChk2" value="2" style="margin: 8px;"/><label for="langChk2" "></label>
						</div>
					</div>
				</div>
			</div>					
			
			
			<input type="hidden" id="hidPrjSeq" name="SEQ_NO" value="" />
	        <input type="hidden" id="hidOldName" name="OLD_NAME" value="${PROJECT_NAME}" />
	        <input type="hidden" id="hidOldLang" name="OLD_LANG_SEQ" value="${LANG_SEQ_NO}" />			
		</div>									
	</div>	
</body>
</html>