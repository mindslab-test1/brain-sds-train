<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {			
			fnSetNaviTitle('Logs');
			$('#divList').on({				
				'click': function(e) {											
					$(this).next().toggle();
					fnpreventDefault(e);
				}
			}, '[name=lilog]');
		}

		function fnProjectGetLogs(seqno, projectname, langseq, userseq, apikey) {
			var $dd = $('#dd' + seqno);
			if ($dd.find('ul').length == 0) {
				var comAjax = new ComAjax();
				comAjax.addParam("SEQ_NO", seqno);
				comAjax.addParam("PROJECT_NAME", projectname);
				comAjax.addParam("LANG_SEQ_NO", langseq);
				comAjax.addParam("USER_SEQ", userseq);
				comAjax.addParam("API_KEY", apikey);
				comAjax.setUrl("<c:url value='/view/getdomainlogs.do' />");			
				comAjax.setCallback("fnProjectGetLogsCallBack");
				comAjax.setAsync(false);
				comAjax.ajax();	
			}
			else 
				$dd.find('ul').eq(0).toggle();
			
			fnpreventDefault(event);
		}
		
		function fnProjectGetLogsCallBack(data) {			
			if (data.STATUS == 'OK') {
				$('#dd' + data.SEQ_NO).append(data.MSG);
			}
			else
				alert('Log 파일 목록 가져오기 중 오류가 발생하였습니다.');
		}
		
		function fnOpenFile(obj, projectname, langseq, userseq, parentname) {
			fnWinPop("<c:url value='/view/noitce.do' />" + '?Type=L&prjname=' + encodeURIComponent(projectname) + '&langseq=' + langseq + '&userseq=' + userseq + '&parentname=' + encodeURIComponent(parentname) + '&filename=' + encodeURIComponent($(obj).text()), "NoticePop", 713, 626, 0, 0);
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf"%>
		<div id="divList" class="middleW logs">			
				<c:set var="UserSeq" value=""></c:set>
				<c:forEach items="${prjlist }" var="row">															
					<c:if test='${row.DELETE_FLAG == "N"}'>
						<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
							<c:if test="${UserSeq.toString() ne ''}">
									</dl>
								</div>
							</div>
							</c:if>
							<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
							<div class="liBox liBox4">					
								<div class="title">${row.USER_ID} / ${row.USER_NAME }</div>
								<div class="ulBox">
									<dl class="class_name_dl">							
						</c:if>
						<dd id="dd${row.SEQ_NO}">
							<a onclick="fnProjectGetLogs(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO }, '${row.API_KEY }');" href="#">${row.PROJECT_NAME }</a>														
						</dd>					
					</c:if>
				</c:forEach>
				
				<c:if test="${UserSeq.toString() ne ''}">
						</dl>
					</div>
				</div>
				</c:if>
			</div>
				
			<!-- <div id="divList" class="liBox liBox4">
				<div class="title">carvatar/관리자</div>
				<div class="ulBox">
					<dl class="class_name_dl">
						<dd>
							<a href="#">jhtest2</a>
							<ul>
								<li>Run</li>
								<ul>
									<li><a href="#">180518-142920.txt</a></li>
								</ul>
							</ul>
						</dd>
						<dd>
							<a href="#">jhtest2</a>
							<ul>
								<li>Learn</li>
								<ul>
									<li><a href="#">180518-142920.txt</a></li>
									<li><a href="#">180518-142920.txt</a></li>
								</ul>
								<li>Run</li>
								<ul>
									<li><a href="#">180518-142920.txt</a></li>
								</ul>
							</ul>
						</dd>
						<dd><a href="#">aaaaa</a></dd>
					</dl>				
				</div>
			</div> -->			
		</div>
	</div>

	<%-- <%@ include file="/WEB-INF/include/left.jspf"%>
	<div class="logs_list">
		<div class="cont_title">
			<h2>Logs</h2>			
		</div>
		<div id="divList" class="user_utterance_contents" style="margin-top: 25px;">
			<dl class="class_name">
				<c:set var="UserSeq" value=""></c:set>
				<c:forEach items="${prjlist }" var="row">											
					<c:if test='${row.DELETE_FLAG == "N"}'>
						<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
							<h3 style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 10px 45px 10px 45px;">${row.USER_ID} / ${row.USER_NAME }</h3>							
							<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
						</c:if>
						<dd id="dd${row.SEQ_NO}" style="border-top: 1px solid #e1e1e1;">
							<a onclick="fnProjectGetLogs(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.LANG_SEQ_NO }', ${row.USER_SEQ_NO });"
								href="#" class="class_name_list">${row.PROJECT_NAME }</a>								
							<!-- <ul>
								<li name="lilog">Learn</li>								
								<li name="lilog">Run</li>							   	
								<ul>
									<li>180514-220517</li>
									<li>180514-230314</li>
								</ul>
							</ul> -->
						</dd>					
					</c:if>
				</c:forEach>				
			</dl>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf"%> --%>
</body>
</html>