<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var iSeqNo = '';
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var pop;
		
		window.onload = function() {
			$('#divrepliesT1, #divrepliesT2').on({
				focusout: function(e) {
					$this = $(this);
					var orgval;
					var $parentobj = $this.parent().parent();					
					
					if ($this.text().toLowerCase().indexOf('request') > -1) {											
						if($parentobj.find('[name=OBJECT_REQUEST]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
							$parentobj.find('[name=mainli]').html($('#divRequestAdd').html());
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').attr('name', 'OBJECT_REQUEST_PATTERN');							
							$parentobj.append('<div class="input input2_style" name="none_obj" style="display:block;"><div class="title">다음 사용자 발화 DA 제한</div><input class="input2_con_style" type="text" name="LIMIT_DA" /></div>');
						}
					}
					else {
						if($parentobj.find('[name=OBJECT_DETAIL_NAME]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
							$parentobj.find('[name=mainli]').html($('#divInformAdd').html());
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').attr('name', 'OBJECT_DETAIL_NAME');
							$parentobj.find('.input2_style').remove();
						}
					}
				}		
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {						
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false" class="wordBr"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
					//fnOpenPop(this, 'S');
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');					
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
														
					if ($this.text() != '') {
						fnAddSubObject($this, 'C', 'P');
					}
				},
				change: function(e) {
					bWorkChk = true;
				},
				keydown: function(e) {
					if(e.keyCode == 9) {
						$this = $(this);
						if ($this.text() != '') {
							fnAddSubObject($this, 'T', 'P');
						}						
						fnpreventDefault(e);
					}
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
					
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					if ($this.text() != '') {
						fnAddSubObject($this, 'C', 'T');
					}
				},
				focusout: function(e) {
					var str = $(this).text();
					var arrvalidation = fnSlotValidation(str, slotdata);
					if (!arrvalidation[0]) {
						var arrtempval = new Array();
						for (var i = 1; i < arrvalidation.length; i++) {
							arrtempval.push(arrvalidation[i]);
						}
						alert('존재 하지 않는 슬롯입니다.\n(' + arrtempval.join(', ') + ')');
					}
				},
				keydown: function(e) {
					if(e.keyCode == 38) {
						//$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						//$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {											
						$this = $(this);
						if ($this.text() != '') {
							fnAddSubObject($this, 'T', 'T');
							/* if($this.parent().parent().next().get(0).tagName == 'DIV') {
								$this.parent().parent().parent().append($('#divDetailSub').html());								
							}	
							fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0)); */
						}						
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
				
			$('#divreplies').on({				
				'click': function(e) {
					var $this = $(this);
					$this.parent().children().removeClass('on');
					$this.addClass('on');
					
					var idx = $this.index();
					$this.parent().parent().next().find('.fire').hide();
					$this.parent().parent().next().find('.fire').eq(idx).show();				
				}
			}, '.fireMenu ul li');
			
			$('#relatedSlot, #resetSlot').click(function() {
				pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />?Type=related&value=" + encodeURIComponent($(this).val()) + "&id=" + $(this).attr('id'), "SlotSearchpop", 430, 550, 0, 0);
			});
			
			$('#tbTaskMonitor').on({				
				keyup : function(e) {
					$(this).val($(this).val().replace(/[^0-9]/g, ""));
				}
			}, '[name=COUNT]');
			
			$('#divreplies').on('click', '.close_btn', function(e) {
				$(this).prev().val('');
				$(this).prev().prev().text('');
				bWorkChk = true;
				fnpreventDefault(e);
			});
			
			if ('${SEQ_NO}' != '') {
				$('#resetSlot').val('${Agent.RESET_SLOT}');				
				$('#selTaskType').val('${Agent.TASK_TYPE}');
				$('#relatedSlot').val('${Agent.RELATED_SLOTS}');
				$('#txtDescription').val('${Agent.DESCRIPTION}');
				fnSetNaviTitle('Task > ${Agent.AGENT_NAME}');
			}
			else {
				fnSetNaviTitle('Create Task');
			}
			
			$('#resetSlot, #selTaskType, #txtAgentName, #relatedSlot').change(function() {
				bWorkChk = true;
			});						
			
			if ('${fn:replace(Condition, "'", "\\'")}' == '')
				$('#divCondition').hide();
			else
				$('#divTaskGoal').hide();
			
			if ($('#lnb .sub .on').eq(0).text() != '${Agent.AGENT_NAME}') {
				$('#lnb .b2_btn_off').attr('class','b2_btn');
	    		$('#lnb .sub .on').removeClass('on'); 
	    		$('#lnb .sub .onsub2').removeClass('onsub2');
	    		$('#lnb .sub02').slideUp();
	    		var $spobj = $('#sp${Agent.AGENT_NAME}');
	    		$spobj.attr('class','b2_btn_off');
	    		$spobj.parent().attr('class', 'on');
	    		$spobj.parent().parent().children('.sub02').slideDown();
	    		$spobj.parent().parent().find('.sub02 li:last a').addClass('onsub2');
			}
			
			if ('${SEQ_NO}' == '') {
				$('[name=prop_obj]').hide();
				if (${fn:length(submenu)} == 1) {
					$('#initTaskChk').prop("checked", true);
				}
			}
			else {
				$('#divTaskCopy').hide();
				$('#divPropTitle').text('저장 된 내용');
				if ('${Agent.SORT_NO}' == '0')				
					$('#initTaskChk').prop("checked", true);
				$('#txtTaskGoal').val('${Agent.TASK_GOAL}');
			}
		}
		
		$(window).bind("beforeunload", function (e){
			if (!gfn_isNull(pop)) {
				pop.close();
			}
		});
		
		function fnAddSubObject(obj, type, cate) {
			var $this = obj;
			if (cate == 'T') {
				if ($this.parent().parent().attr('name') == 'mainli') {
					$ul = $this.parent().parent().next().find('ul');
					if ($ul.find('li').length == 0) {
						$ul.append($('#divDetailSub').html());	
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('ul').eq(0).find('div > div').get(0));
					}
				}
				else {
					if ($this.parent().parent().index() == $this.parent().parent().parent().find('li').length -1) {
						$this.parent().parent().parent().append($('#divDetailSub').html());
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0));
					}
				}
			}
			else {
				if ($this.parent().parent().parent().attr('name') == 'mainli') {
					$ul = $this.parent().parent().parent().next().find('ul');
					if ($ul.find('li').length == 0) {
						$ul.append($('#divDetailSubP').html());	
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().parent().next().find('ul').eq(0).find('div > div').get(0));
					}
				}
				else {
					if ($this.parent().parent().index() == $this.parent().parent().parent().find('li').length -1) {
						$this.parent().parent().parent().append($('#divDetailSubP').html());
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0));
					}
				}
			}
		}
		
		function fnColGroupChange(seqno) {
			$('#tbcolg' + seqno).html('<col width="120px" /><col width="*" /><col width="110px" />');			
		}
		
		function fnSave() {
			var bTaskSaveChk = false;
			var bRequestChk = false;
			var focusObj;
			var cnt = 0;
			var rcnt = 0;
			$txtAgentName = $('#txtAgentName');

		
			if ($txtAgentName.val() == '') {
				alert('Task 명을 입력 하세요.');
				fnpreventDefault(event);
				return false;
			}
			
			if ($txtAgentName.val().length > 50) {
				alert('Task 명을 50자 이하로 입력해 주세요.');
				fnpreventDefault(event);
				return false;
			}
			
			if ($txtAgentName.val().toUpperCase() == 'END') {
				alert('END: 해당 이름으로는 task를 만드실 수 없습니다.');
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_-]/; 
			if (reg.test($txtAgentName.val())) {
				alert('태스크명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$txtAgentName.focus();
				fnpreventDefault(event);
				return;
			}
			
			if('${SEQ_NO}' == '') {
				$('#lnb .taskBox div').each(function(e) {				
					if ($(this).text() == $txtAgentName.val()) {
						bTaskSaveChk = true;
						alert('이미 존재하는 Task 입니다.');
						return false;
					}
				});
			}
			
			if (bTaskSaveChk) {
				return false;
			}
						
			reg = /[^a-zA-Z0-9_\-=()."]/; 
			fnLoading();
			var arr = new Object();
			var mainarrobj = new Array();
			var typevalue;
			$('[id^=divrepliesT]').each(function(idx, e) {
				if (idx == 0) 
					typevalue = 1; 
				else 
					typevalue = 2;
				$(this).find('[name=wtastas]').each(function(idx, e) {					
					var mainitem = new Object();
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();
					mainitem.INTENTION_TYPE = typevalue;
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';

					var arrobj = new Array();
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						cnt = 0;
						rcnt = 0;
						var item = new Object();
						var arrobjsub = new Array();
						
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'inform()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bTaskSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text()) || mainitem.UTTR != 'true') {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);
									rcnt ++;
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt ++;
								}
							});
							
							if (cnt > 0 && rcnt == 0) {
								focusObj = $this.find('[name=OBJECT_REQUEST]');
								bTaskSaveChk = true;
								bRequestChk = true;
								return false;
							}
						}
						
						$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
							if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
								var subitem = new Object();
								subitem.TYPE = 'T';
								subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
								
								subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
								var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
								if (tempstr == '<br>')
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
								arrobjsub.push(subitem);
								cnt ++;
							}
						});										
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;
							
							arrobj.push(item);												
						}										
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);	
					}					
				});
			});
			
			arr.INTENTION_ITEM = mainarrobj;
						
			var arrslotitem = new Array();			
			
			$('#divrepliesP [name=wtastas]').each(function(idx, e) {				
				mainarrobj = new Array();
				var mainarrobjitem = new Object();
				var $txtSlotName = $(this).find('[name=txtSlotName]');
						
				if($txtSlotName.val().trim() != '') {
					mainarrobjitem.SLOT_SEQ = $txtSlotName.next().val();
					mainarrobjitem.SLOT_NAME = $txtSlotName.val();										
					
					if (mainarrobjitem.SLOT_NAME.indexOf(',') < 0)
						mainarrobjitem.CONDITION = 'ExistValue("' + mainarrobjitem.SLOT_NAME + '")==false';	
					else {
						var arrtmp = mainarrobjitem.SLOT_NAME.split(', ');
						var str = '';
						for (var i = 0; i < arrtmp.length; i++) {
							if (i == 0)
								str += 'ExistValue("' + arrtmp[i] + '")==false';
							else
								str += ' and ExistValue("' + arrtmp[i] + '")==false';
						}
						mainarrobjitem.CONDITION = str;
					}					
					mainarrobjitem.PROGRESS_SLOT = mainarrobjitem.SLOT_NAME;
								
					var mainitem = new Object();
					var arrobj = new Array();
					
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();
					mainitem.INTENTION_TYPE = 3;
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';
						
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						var item = new Object();
						var arrobjsub = new Array();
						cnt = 0;
						
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'request()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bTaskSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}										
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text()) || mainitem.UTTR != 'true') {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);									
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});
						}
						
						$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
							if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
								var subitem = new Object();
								subitem.TYPE = 'T';
								subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
								
								subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
								var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
								if (tempstr == '<br>')
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
								arrobjsub.push(subitem);
								cnt++;
							}
						});
																	
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;							
							arrobj.push(item);
						}
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
						
						mainarrobjitem.INTENTION_ITEM = mainarrobj;
						arrslotitem.push(mainarrobjitem);	
					}
				}
			});
			
			if (bTaskSaveChk) {
				fnLoading();
				if (bRequestChk)
					alert('질문 할 slot을 선택해 주세요.');
				else
					alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				focusObj.focus();
				return false;
			}
			
			var SortNo = ${fn:length(submenu)};
			if ('${Agent.SORT_NO}' != '') {
				SortNo = '${Agent.SORT_NO}';
			}
			if ($('#initTaskChk').prop('checked') == true)
				SortNo = 0;
			
			if (${fn:length(submenu)} == 1) {
				SortNo = 0;
			}
			
			arr.SLOT_FILLING_ITEM = arrslotitem;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertagent.do' />");			
			comSubmit.addParam("AGENT_NAME", $txtAgentName.val());
			comSubmit.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comSubmit.addParam("SORT_NO", SortNo);				
			comSubmit.addParam("TASK_GOAL", $('#txtTaskGoal').val());
			comSubmit.addParam("RESET_SLOT", $('#resetSlot').val());
			comSubmit.addParam("TASK_TYPE", $('#selTaskType').val());
			comSubmit.addParam("RELATED_SLOTS", $('#relatedSlot').val());
			comSubmit.addParam("DESCRIPTION", $('#txtDescription').val());
			comSubmit.addParam("OBJECT_ITEM", jsoobj);			
			
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '')
					comSubmit.addParam("SEQ_NO", '');
				else
					comSubmit.addParam("SEQ_NO", iSeqNo);
			}
			else
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.submit();
			/* var comAjax = new ComAjax();				
			comAjax.setUrl("<c:url value='/view/insertagent.do' />");
			comAjax.setCallback("fnSaveCallBack");
			comAjax.addParam("AGENT_NAME", $txtAgentName.val());
			//comAjax.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comAjax.addParam("SORT_NO", SortNo);
			comAjax.addParam("RESET_SLOT", $('#resetSlot').val());
			comAjax.addParam("TASK_TYPE", $('#selTaskType').val());
			comAjax.addParam("RELATED_SLOTS", $('#relatedSlot').val());
			comAjax.addParam("DESCRIPTION", $('#txtDescription').val());
			comAjax.addParam("OBJECT_ITEM", jsoobj);			
			
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '')
					comAjax.addParam("SEQ_NO", '');
				else
					comAjax.addParam("SEQ_NO", iSeqNo);
			}
			else
				comAjax.addParam("SEQ_NO", '${SEQ_NO}');
			comAjax.addParam("depth1", '${depth1}');
			comAjax.addParam("depth2", '${depth2}');
			comAjax.addParam("depth3", '${depth3}');
			comAjax.ajax();			 */
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {
			alert('저장 되었습니다.');
			fnLoading();
			if (iSeqNo == '' && '${SEQ_NO}' == '') {
				iSeqNo = data.SgEQ_NO;
			
				var str = '';			
				str = '<li>';
				str += '<a href="#">' + $('#txtAgentName').val() + '<span class="b2_btn"></span></a>';
				str += '<input type="hidden" value="' + iSeqNo + '">';
				str += '<ul class="sub02" style="display:none;">';											
				str += '<li><a href="#">Entities</a></li>';
				str += '<li><a href="#">Intents</a></li>';
				str += '<li><a href="#">Property</a></li>';																
				str += '</ul>';
				str += '</li>';
				$('#menu1').next().append(str);	
			}			
			bWorkChk = false;
		}
		
		function fnTransactionAdd(type) {
			if (type == 'P') {
				if ($('#divrepliesP').find('[name=wtastas]').length == 15) {					
					fnpreventDefault(event);
					return false;
				}
				$('#divrepliesP').append($('#divrepliesAddP').html().replace(/§/g, type + '_' + $('#divreplies' + type + ' [name=divSysIntent]').length));
			}
			else {
				if ($('#divreplies' + type).find('[name=wtastas]').length == 15) {
					fnpreventDefault(event);
					return false;
				}
				$('#divreplies' + type).append($('#divrepliesAddT').html().replace(/§/g, type + '_' + $('#divreplies' + type + ' [name=divSysIntent]').length));
			}
						
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj, Type) {
			$obj = $(obj);
			if (SeqNo == 1) {
				$obj.parent().parent().remove();
			}
			else if (SeqNo == 2) {
				if (Type == 'T') {
					if ($obj.parent().attr('name') == 'mainli') {
						if ($obj.parent().next().find('li').length > 0) {
							$obj.parent().find('div > div').text($obj.parent().next().find('li').eq(0).find('div > div').text());
							$obj.parent().next().find('li').eq(0).remove();
						}
						else {
							if ($(obj).parent().parent().parent().parent().parent().find('[name=divSysIntent]').length > 1) {
								$(obj).parent().parent().parent().parent().remove();
							}
							else
								$obj.parent().find('div > div').text('');
						}
					}
					else
						$obj.parent().remove();
				}
				else {
					if ($obj.parent().parent().attr('name') == 'mainli') {
						if ($obj.parent().parent().next().find('li').length > 0) {
							$obj.parent().find('div > div').text($obj.parent().parent().next().find('li').eq(0).find('div > div').text());
							$obj.parent().parent().next().find('li').eq(0).remove();
						}
						else {
							if ($(obj).parent().parent().parent().parent().parent().parent().find('[name=divSysIntent]').length > 1) {
								$(obj).parent().parent().parent().parent().parent().remove();
							}
							else
								$obj.parent().find('div > div').text('');
						}
					}
					else
						$obj.parent().remove();
				}
			}
			
			bWorkChk = true;
			fnpreventDefault(event);
		}
		
		function fnTransactionSave() {			
			fnpreventDefault(event);
		}
		
		function fnAddCalcel() {
			$.modal.close();
			//$('#divmodal').hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj, type, flag) {
			var width = '713';
			var heigth = '626';
			if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
				width = '613';
				heigth = '400';	
			}
			if (type == 'F') {
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id') + '&flag=' + flag, "AgentFunctionpop", width, heigth, 0, 0);	
			}
			else if (type == 'A') {
				pop = fnWinPop("<c:url value='/view/openactionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id'), "AgentActionpop", 570, 555, 0, 0);	
			}
			else if (type == 'E') {
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id') + "&flag=A&Type=CC&Title=", "AgentFunctionpop", width, heigth, 0, 0);				
			}
			else if (type == 'S') {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id') + "&Type=TS", "AgentSearchpop", 430, 550, 0, 0);
			}
			else {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id'), "AgentSearchpop", 430, 550, 0, 0);
			}
			
			fnpreventDefault(event);
		}		
		
		function fnCheck(obj, type) {
			if ($(obj).prop("checked") == false) {
				
				if (type == 'I' && '${Agent.SORT_NO}' == '0') {
					alert('반드시 하나의 task는 initial task로 설정되어야 합니다.');
					$(obj).prop('checked', true);
					return;
				}
			}
			else {
				if (type == 'E' && '${Agent.SORT_NO}' == '0') {
					alert('반드시 하나의 task는 initial task로 설정되어야 합니다.');
					$(obj).prop('checked', true);
					return;
				}
			}
		}
		
		function fnObjectView(obj, type) {
			var $parentobj = $(obj).parent().parent();
			if (type == 1) {																			
				if ($parentobj.attr('class') != 'fireBox') {
					$parentobj.find('.delete').css('height', '47px');
					$parentobj = $parentobj.parent().find('.fireMenu');
				}
				else {
					$parentobj.find('.fireCont>.delete').css('height', '47px');
					$parentobj = $parentobj.find('.fireMenu');
				}
				
				$parentobj.parent().find('[name=none_obj]').show();
				$(obj).hide();
			}
			else {
				$parentobj.find('.fireCont>.delete').css('height', '60px');
				$parentobj.find('.fireCont>.open').show();
				$parentobj.find('.fireCont>div').hide();
				$parentobj.find('.fireCont>div').eq(0).show();				
				$parentobj.find('[name=none_obj]').hide();
				$parentobj.find('.modify').show();				
			}
							
			fnpreventDefault(event);
		}
		
		function fnCopyTask() {
			pop = fnWinPop("<c:url value='/view/copytask.do' />", "copyTaskPop", 700, 550, 0, 0);
		}
		
		function fnCopyMove(seqno) {
			var comSubmit = new ComSubmit();			
			comSubmit.setUrl("<c:url value='/view/agentsub.do' />");
			comSubmit.addParam("AGENT_SEQ", seqno);
			comSubmit.addParam("COPY_CHK", "Y");
			comSubmit.addParam("depth1", 'menu1');
			comSubmit.addParam("depth2", '0');
			comSubmit.addParam("depth3", '3');
			comSubmit.submit();
		}
		
		function fnSysIntentAdd(obj, type) {
			if (type == 'T')
				$(obj).parent().find('.fireCont > .systemfire').append($('#divSysintentAdd').html());			
			else
				$(obj).parent().find('.fireCont > .systemfire').append($('#divSysintentRequestAdd').html());
						
			fnpreventDefault(event);
		}
		
		function fnSysIntentDel(obj) {
			$parentobj = $(obj).parent().parent().parent().parent().parent().parent();
			//if ($parentobj.parent().find('[name=divSysIntent]').length > 1) {
				$parentobj.remove();	
			//}
				
			fnpreventDefault(event);
		}
		
		function fnShowMonitoring() {
			if ('${SEQ_NO}' != '') {				
				fnLayerPop('divMonitoring', 'o');
				fnpreventDefault(event);	
			}
			else
				alert('태스크 저장 후 작성 할 수 있습니다.');
		}
		
		function fnCancel() {			
			fnLayerPop('divMonitoring', 'c');
		}
		
		function fnAddTaskAction() {
			var cnt = $('#divTaskAction').find('[name=TaskAction]').length; 
			$('#divTaskAction').append($('#divAddTaskAction').html().replace(/§/g, cnt.toString()));						
			fnpreventDefault(event);
		}
		
		function fnMonitoringSave() {							
			var arrMonitoring = new Array();
			var cnt = 0;
			$('#tbTaskMonitor').find('[name=COUNT]').each(function() {
				if ($(this).val() != '') {
					arrMonitoring[cnt] = $(this).val();
					cnt++;
				}
			});
				
			if (cnt != 0 && cnt != 6) {
				alert('task monitoring에 있는 모든 칸을 채워 주세요.');
				return;
			}
			
			var arrobj = new Object();
			var arr = new Array();
			$('#divTaskAction').find('[name=TaskAction]').each(function() {
				var subobj = new Object();
				var $this = $(this);
				subobj.TYPE = $this.find('[id^=ddlType]').val();
				subobj.CON = $this.find('[id^=txtMonitorCon]').val();
				subobj.ACT = $this.find('[id^=txtMonitorAct]').val();
				
				if (subobj.CON != '') {
					arr.push(subobj);
				}				
			});	
			arrobj.ACTION_ITEM = arr;
			var jsoobj = JSON.stringify(arrobj);
			
			var comSubmit = new ComSubmit();			
			comSubmit.setUrl("<c:url value='/view/insertagentmotitor.do' />");
			comSubmit.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comSubmit.addParam("MONITORIN_TASK_0", arrMonitoring[0]);
			comSubmit.addParam("MONITORIN_TASK_1", arrMonitoring[1]);
			comSubmit.addParam("MONITORIN_TASK_2", arrMonitoring[2]);
			comSubmit.addParam("MONITORIN_USER_0", arrMonitoring[3]);
			comSubmit.addParam("MONITORIN_USER_1", arrMonitoring[4]);
			comSubmit.addParam("MONITORIN_USER_2", arrMonitoring[5]);
			comSubmit.addParam("TASK_ACTIONS_ITEM", jsoobj);
			comSubmit.addParam("depth1", 'menu1');
			comSubmit.addParam("depth2", '0');
			comSubmit.addParam("depth3", '3');
			comSubmit.submit();
		}
		
		function fnMonitoringDelete(obj) {
			$(obj).parent().parent().remove();
			fnpreventDefault(event);
		}
		
		function fnObjectUpDown(obj, type) {
			var $fireBox = $(obj).closest('.fireBox');
			if (type == 'U') {
				if ($fireBox.prev().attr('class') != 'title') {
					$fireBox.prev().before($fireBox);
				}
			}
			else {
				if ($fireBox.next().length != 0) {
					$fireBox.next().after($fireBox);
				}
			}
		}
	</script>
</head>
<body>
	<div class="layer" id="layer">
		<div id="bg" class="bg"></div>
		<div id="divMonitoring" class="popup view" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
			<div class="title">Define actions and monitoring information</div>
			<div class="body">
				<div class="title"><div class="squre"></div>Task Monitoring</div>
				<div class="liBox table">
					<table id="tbTaskMonitor" class="table">
						<caption class="hide" style="display:none;">task monitoring</caption>
						<colgroup>
							<col width="25%">
							<col width="25%">
							<col width="25%">
							<col width="25%">
						</colgroup>
						<thead>
							<tr>
								<th></th>
								<th>관심</th>
								<th>경고</th>
								<th>위험</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${fn:length(Mornitoringlist) > 0}">                  	
			                    	<c:forEach items="${Mornitoringlist}" var="row">	                        
		                        		<tr>
		                        			<c:choose>
			                        		<c:when test="${row._TYPE eq 'T'}">		                        			                        	
			                       				<td>태스크 반복 수</td>
			                        		</c:when>
			                        		<c:otherwise>
			                        			<td>최대 사용자 발화 수</td>	
			                        		</c:otherwise>
				                        	</c:choose>
				                        	<td><input type="text" value="${row.INTEREST }" name="COUNT" /></td>
				                            <td><input type="text" value="${row.WARNING }" name="COUNT" /></td>
				                            <td><input type="text" value="${row.DANGER }" name="COUNT" /></td>
				                        </tr>
			                        </c:forEach>
		                        </c:when>
		                        <c:otherwise>
		                        	<tr>
	                        			<td>태스크 반복 수</td>
			                        	<td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                        </tr>
			                        <tr>
	                        			<td>최대 사용자 발화 수</td>
			                        	<td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                        </tr>
		                        </c:otherwise>
	                        </c:choose>						
						</tbody>
					</table>
				</div>
				<div class="title ta"><div class="squre"></div>Task Actions<div class="btn" onclick="fnAddTaskAction();">Add</div></div>
				<div id="divTaskAction" style="width:100%;">
					<c:set var="Cnt" value="0" />
					<c:forEach items="${Actionslist}" var="row">
						<div name="TaskAction" class="body taskAction">
							<div class="popInput">
								<div class="title">적용시점</div>
								<div class="input">
			                   		<select id="ddlType${Cnt }">					                   
										<option value="0" <c:if test="${row._TYPE eq 0}">selected</c:if>>사용자 발화 처리 전</option>
										<option value="1" <c:if test="${row._TYPE eq 1}">selected</c:if>>사용자 발화 처리 후</option>
				                    </select>
								</div>
							</div>
							<div class="popInput">
							<div class="title">조건</div>
								<div class="input">
									<input type="text" id="txtMonitorCon${Cnt }" name="txtMonitorCon" value="${fn:replace(row._CONDITION, '\"', '&quot;')}"  />
									<button type="button" onclick="fnOpenPop(this, 'E', 'T');" title="Script Guide" class="scriptGuide"></button>
								</div>
							</div>
							<div class="popInput">
								<div class="title">행동</div>
								<div class="input">
									<input type="text" id="txtMonitorAct${Cnt }" name="txtMonitorAct" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
									<button type="button" onclick="fnOpenPop(this, 'E', 'T');" title="Script Guide" class="scriptGuide"></button>
								</div>
							</div>
							<div class="taskDeleteBtn"><a href="#" onclick="fnMonitoringDelete(this);"><img src="../images/delete_btn.png" alt="deleteBtn" /></a></div>
						</div>						
						<c:set var="Cnt" value="${Cnt + 1 }" />
					</c:forEach>				
	            </div>
			</div>
			<div class="btnBox">
				<div onclick="fnMonitoringSave();" class="save">Save</div>
				<div onclick="fnCancel();" class="cancle">Cancel</div>
			</div>
		</div>
	</div>
	<div id="wrap">	
	<%@ include file="/WEB-INF/include/left.jspf" %>

		<div id="divreplies" class="middleW createTask">
			<div class="btnBox">			
				<div onclick="fnSave();" class="save" style="float:right !important;">Save</div>
			</div>
			<div class="searchBox" id="divTaskCopy">
				<input type="hidden" id="hidurl" value="<c:url value='/view/openfunctionwrite.do' />" />
				<div class="title">다른 Task 복사해오기</div>
				<div class="box">
					<c:choose>
						<c:when test="${SEQ_NO != null}">
							<c:choose>
								<c:when test="${Agent.FROM_COPY == null }">								
									<input type="text" id="CopyTask" value="" readonly="readonly" />									
								</c:when>
								<c:otherwise>									
									<input type="text" id="CopyTask" value="${Agent.FROM_COPY}" readonly="readonly" style="font-size: 16px; color: #444;"/>
								</c:otherwise>								
							</c:choose>
						</c:when>
						<c:otherwise>							
							<input type="text" id="CopyTask" value="" readonly="readonly" /><button onclick="fnCopyTask();" type="button"><img src="../images/icon_searchW.png" alt="search" title="Search"></button>
						</c:otherwise>
					</c:choose>					
				</div>
			</div>			
			<div class="conBox">
				<div class="title" id="divPropTitle">새로 만들기</div>
				<div class="con">
					<div class="inputBox">
						<div class="input">
							<div class="title">Task Name</div>
							<div class="right">
								<c:choose>
			                		<c:when test="${null eq Agent}">	                			
			                			<input type="text" id="txtAgentName" name="txtAgentName" />
			                		</c:when>
			                		<c:otherwise>
			                			<c:choose>	                	
			                				<c:when test='${Agent.AGENT_NAME != ""}'>
			                					<input type="text" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
			               					</c:when>
			               					<c:otherwise>		                						
			               						<input type="hidden" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
			               					</c:otherwise>
			              					</c:choose>
			                		</c:otherwise>
			                	</c:choose>
							</div>
						</div>
						<div class="input">
							<div class="title">Description</div>
							<div class="right">
								<input type="text" value="" id="txtDescription" name="txtDescription" />
							</div>
						</div>
						<div class="check">
							<div class="title">Initial</div>
							<div class="right">								
								<input type="checkbox" onclick="fnCheck(this, 'I');" id="initTaskChk" name="initTaskChk" value="" style="margin: 8px;"/><label for="initTaskChk" style="vertical-align: text-top; line-height:22px;"></label>
							</div>
						</div>
						<div class="inputBtn" name="prop_obj">
							<div class="title">Task Goal</div>
							<div class="right" id="divCondition">
								<textarea id="txtCondition" style="border:1px solid #ddd; width: 100%; border-radius:4px; padding-left:5px; color:#000000;" readonly="readonly">${Condition}</textarea>								
							</div>
							<div class="right" id="divTaskGoal">
								<input type="text" id="txtTaskGoal" name="txtTaskGoal" style="padding:5px 0px; /*width:65%;*/" />
								<button type="button" onclick="fnOpenPop(this, 'E', 'T');" title="Script Guide" class="scriptGuide"></button>
							</div>
						</div>
						<div class="input relatedSlot" name="prop_obj">
							<div class="title">Related Slots</div>
							<div class="right">
								<input type="text" value="" id="relatedSlot" name="relatedSlot" />
							</div>
						</div>
						<div class="input relatedSlot" name="prop_obj">
							<div class="title">Reset Slots</div>
							<div class="right">
								<input type="text" value="" id="resetSlot" name="resetSlot" />
							</div>
						</div>										
					</div>
				</div>
			</div>			
			<div id="divrepliesT1" class="liBox liBox1">
				<div class="title">Task 시작 시, 시스템 발화 정의<div onclick="fnTransactionAdd('T1');" class="btn">추가</div></div>
				<c:choose>
					<c:when test="${fn:length(Intentionlist_1) > 0}">
						<c:forEach items="${Intentionlist_1}" var="row">
							<div class="fireBox" name="wtastas">
								<div class="fireMenu" name="none_obj" style="display:none;">
									<ul>
										<li class="on">시스템 발화</li>										
										<li>시스템 발화 후 행동</li>
									</ul>
								</div>	
								<div class="fireCont" style="margin-bottom: 20px;">
									<div class="fire systemfire on">										
										<c:choose>
										<c:when test="${fn:length(Objectlist_1) > 0}">											
											<c:forEach items="${Objectlist_1}" var="orow">																																																						
												<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
													<div name="divSysIntent">
														<div class="inputBtn" name="none_obj" style="display:none;">
															<div class="title">시스템 발화 조건</div>
															<input type="text" id="txtEUttrT1_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
															<button type="button" onclick="fnOpenPop(this, 'F', 'T');" title="Script Guide" class="scriptGuide"></button>
														</div>
														<div class="input" name="none_obj" style="display:none;">
															<div class="title">시스템 intent</div>
															<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">${orow.OBJECT_VALUE}</div>
														</div>
														<div class="liBox liBox_style">
															<ul>
																<c:if test="${fn:length(ObjectDtllist_1) > 0}">
																	<c:set var="Cnt" value="0" />
																	<c:set var="Request" value="" />
																	<c:forEach items="${ObjectDtllist_1}" var="drow">																																																						
																		<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">
																			<c:choose>
																			<c:when test="${Cnt == 0}">
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<c:set var="Cnt" value="1" />
																					<c:set var="Request" value="${drow.OBJECT_VALUE }" />																			
																				</c:when>
																				<c:otherwise>
																					<c:set var="Cnt" value="2" />
																					<li name="mainli">								
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>									
																					</li>
																					<div>
																						<ul>
																				</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:when test="${Cnt == 1}">
																				<c:set var="Cnt" value="2" />																		
																				<li name="mainli">																			
																					<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false">${Request }</div>
																					<input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>
																					<div class="pattern pattern_style">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</div>
																				</li>
																				<div>
																					<ul>
																			</c:when>
																			<c:otherwise>																
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>			
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</li>																			
																				</c:when>
																				<c:otherwise>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
																					</li>
																				</c:otherwise>
																				</c:choose>																																					
																			</c:otherwise>
																			</c:choose>
																		</c:if>
																	</c:forEach>
																				</ul>
																			</div>
																</c:if>																															
															</ul>
														</div>
														<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
															<div class="input input2_style" name="none_obj" style="display:none;">
																<div class="title">다음 사용자 발화 DA 제한</div>
																<input class="input2_con_style" type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" />
															</div>
														</c:if>	
													</div>
												</c:if>
											</c:forEach>
										</c:when>
										</c:choose>										
									</div>
									<div class="fire inputBtn">
										<div class="right">											
											<input type="text" id="txtEActionT1_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
											<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
										</div>
									</div>
									<span onclick="fnObjectView(this, 1);" class="open"></span>
									<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
									<span onclick="fnUserObjectDelete(1, this, 'T');" class="delete" style="display:block;"></span>
								</div>
								<c:if test="${USER_TYPE ne 'OA'}">
									<div class="fireAdd" name="none_obj" style="display:none;" onclick="fnSysIntentAdd(this, 'T');">+ Add</div>
								</c:if>	
							</div>														
						</c:forEach>							
					</c:when>					
				</c:choose>
			</div>			
			<div id="divrepliesP" class="liBox liBox2" name="prop_obj">
				<div class="title">사용자에게 Slot 값 질문하기<div onclick="fnTransactionAdd('P');" class="btn">추가</div></div>
				<c:choose>
				<c:when test="${fn:length(SlotFillinglist) > 0}">
					<c:forEach items="${SlotFillinglist}" var="srow">
						<c:choose>
							<c:when test="${fn:length(Intentionlist_3) > 0}">
								<c:forEach items="${Intentionlist_3}" var="row">
									<c:if test="${srow.SEQ_NO eq row.FILLING_SEQ}">
										<div class="fireBox" name="wtastas">
											<div class="fireMenu" name="none_obj" style="display:none;">
												<ul>
													<li class="on">대상 슬롯 &amp; 시스템 발화</li>													
													<li>시스템 발화 후 행동</li>
												</ul>
											</div>
											<div class="fireCont" style="margin-bottom: 20px;">
												<div class="fire systemfire on">
													<div class="inputBtn" name="none_obj" style="padding-bottom:0; border-bottom:0; display:none;">
														<div class="title">시스템 발화 조건</div>
														<input type="text" id="txtEUttrP_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
														<button type="button" onclick="fnOpenPop(this, 'F', 'P');" title="Script Guide" class="scriptGuide"></button>
													</div>	
													<div class="input slotSearch" style="padding-bottom:0; border-bottom:0;">
														<div class="title">대상슬롯</div>
														<input readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtESlotName_${srow.SEQ_NO}" name="txtSlotName" value="${srow.SLOT_NAME}" />
														<input type="hidden" name="hidSlotSeq" value="${srow.SLOT_SEQ}" />
													</div>
													<c:choose>
													<c:when test="${fn:length(Objectlist_3) > 0}">
														<c:forEach items="${Objectlist_3}" var="orow">																																																						
															<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
																<div name="divSysIntent">
																	<div class="input" name="none_obj" style="display:none;">
																		<div class="title">시스템 intent</div>
																		<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">${orow.OBJECT_VALUE}</div>
																	</div>
																	<div class="box">
																		<ul>
																			<c:if test="${fn:length(ObjectDtllist_3) > 0}">
																				<c:set var="Cnt" value="0" />
																				<c:set var="Request" value="" />
																				<c:forEach items="${ObjectDtllist_3}" var="drow">																											
																					<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">
																						<c:choose>
																						<c:when test="${Cnt == 0}">
																							<c:set var="Cnt" value="1" />
																							<c:set var="Request" value="${drow.OBJECT_VALUE }" />
																						</c:when>
																						<c:when test="${Cnt == 1}">
																							<c:set var="Cnt" value="2" />																		
																							<li name="mainli">																			
																								<div class="slot"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false">${Request }</div>
																								<input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>
																								<div class="pattern pattern_style">
																									<div>
																										<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																									</div>																									
																									<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																								</div>
																							</li>
																							<div>
																								<ul>
																						</c:when>
																						<c:otherwise>																
																							<li name="subli">
																								<div>
																									<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																								</div>			
																								<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																							</li>																																				
																						</c:otherwise>
																						</c:choose>
																					</c:if>
																				</c:forEach>
																						</ul>
																					</div>
																			</c:if>																															
																		</ul>
																	</div>
																	<div class="input input2" name="none_obj" style="display:none;">
																		<div class="title">다음 사용자 발화 DA 제한</div>
																		<input type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" />
																	</div>
																</div>
															</c:if>
														</c:forEach>
													</c:when>
													</c:choose>										
												</div>
												<div class="fire inputBtn">
													<div class="right">											
														<input type="text" id="txtEActionP_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
														<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
													</div>
												</div>
												<span onclick="fnObjectUpDown(this, 'U');" class="up"></span>
												<span onclick="fnObjectView(this, 1);" class="open"></span>
												<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
												<span onclick="fnUserObjectDelete(1, this, 'P');" class="delete" style="display:block;"></span>
												<span onclick="fnObjectUpDown(this, 'D');" class="down"></span>
											</div>
											<c:if test="${USER_TYPE ne 'OA'}">
												<div class="fireAdd"  name="none_obj" style="display:none;"  onclick="fnSysIntentAdd(this, 'P');">+ Add</div>
											</c:if>	
										</div>
									</c:if>														
								</c:forEach>							
							</c:when>					
						</c:choose>
					</c:forEach>							
				</c:when>					
				</c:choose>
			</div>
			<div id="divrepliesT2" class="liBox liBox1" name="prop_obj" <c:if test="${USER_TYPE eq 'OA'}">style="display:none;"</c:if>>
				<div class="title">Task 다시 시작할 경우, 시스템 발화 정의<div onclick="fnTransactionAdd('T2');" class="btn">추가</div></div>
				<c:choose>
					<c:when test="${fn:length(Intentionlist_2) > 0}">
						<c:forEach items="${Intentionlist_2}" var="row">
							<div class="fireBox" name="wtastas">
								<div class="fireMenu" name="none_obj" style="display:none;">
									<ul>
										<li class="on">시스템 발화</li>										
										<li>시스템 발화 후 행동</li>
									</ul>
								</div>
								<div class="fireCont" style="margin-bottom: 20px;">
									<div class="fire systemfire on">										
										<c:choose>
										<c:when test="${fn:length(Objectlist_2) > 0}">
											<c:forEach items="${Objectlist_2}" var="orow">																																																						
												<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
													<div name="divSysIntent">
														<div class="inputBtn" name="none_obj" style="display:none;">
															<div class="title">시스템 발화 조건</div>
															<input type="text" id="txtEUttrT2_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
															<button type="button" onclick="fnOpenPop(this, 'F', 'T');" title="Script Guide" class="scriptGuide"></button>
														</div>
														<div class="input" name="none_obj" style="display:none;">
															<div class="title">시스템 intent</div>
															<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">${orow.OBJECT_VALUE}</div>
														</div>
														<div class="liBox liBox_style">
															<ul>
																<c:if test="${fn:length(ObjectDtllist_2) > 0}">
																	<c:set var="Cnt" value="0" />
																	<c:set var="Request" value="" />
																	<c:forEach items="${ObjectDtllist_2}" var="drow">																											
																		<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">
																			<c:choose>
																			<c:when test="${Cnt == 0}">
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<c:set var="Cnt" value="1" />
																					<c:set var="Request" value="${drow.OBJECT_VALUE }" />																			
																				</c:when>
																				<c:otherwise>
																					<c:set var="Cnt" value="2" />
																					<li name="mainli">								
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>									
																					</li>
																					<div>
																						<ul>
																				</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:when test="${Cnt == 1}">
																				<c:set var="Cnt" value="2" />																		
																				<li name="mainli">																			
																					<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false">${Request }</div>
																					<input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>
																					<div class="pattern pattern_style">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</div>
																				</li>
																				<div>
																					<ul>
																			</c:when>
																			<c:otherwise>																
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>			
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</li>																			
																				</c:when>
																				<c:otherwise>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
																					</li>
																				</c:otherwise>
																				</c:choose>																																					
																			</c:otherwise>
																			</c:choose>
																		</c:if>
																	</c:forEach>
																				</ul>
																			</div>
																</c:if>																															
															</ul>
														</div>
														<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
															<div class="input input2_style" name="none_obj" style="display:none;">
																<div class="title">다음 사용자 발화 DA 제한</div>
																<input class="input2_con_style" type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" />
															</div>
														</c:if>	
													</div>
												</c:if>
											</c:forEach>
										</c:when>
										</c:choose>										
									</div>
									<div class="fire inputBtn">
										<div class="right">											
											<input type="text" id="txtEActionT2_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
											<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
										</div>
									</div>
									<span onclick="fnObjectView(this, 1);" class="open"></span>
									<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
									<span onclick="fnUserObjectDelete(1, this, 'T');" class="delete" style="display:block;"></span>
								</div>
								<c:if test="${USER_TYPE ne 'OA'}">
									<div class="fireAdd" name="none_obj" style="display:none;" onclick="fnSysIntentAdd(this, 'T');">+ Add</div>
								</c:if>	
							</div>														
						</c:forEach>							
					</c:when>					
				</c:choose>
			</div>
			<c:if test="${USER_TYPE ne 'OA'}">
				<div class="liBox" name="prop_obj">
					<div class="title">Task action 및 모니터링 정보 정의				
						<div onclick="fnShowMonitoring();" class="btn view">보기</div>				
					</div>
				</div>
			</c:if>
		</div>
		</div>
	</div>
	
	
	
	
	<div id="divrepliesAddT" style="display:none;">
		<div class="fireBox" name="wtastas" style="margin-bottom:0px;">
			<div class="fireMenu" name="none_obj" style="display:none;">
				<ul>
					<li class="on">시스템 발화</li>
					<li>시스템 발화 후 행동</li>
				</ul>
			</div>
			<div class="fireCont" style="margin-bottom:20px;">
				<div class="fire systemfire on">
					<div name="divSysIntent">
						<div class="inputBtn" name="none_obj" style="display:none;">
							<div class="title">시스템 발화 조건</div>
							<input type="text" id="txtUttr§" name="txtUttr" value="true" />
							<button type="button" onclick="fnOpenPop(this, 'F', 'T');" title="Script Guide" class="scriptGuide"></button>
						</div>
						<div class="input" name="none_obj" style="display:none;">
							<div class="title">시스템 intent</div>
							<!-- <input type="text" /> -->
							<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">inform()</div>
						</div>
						<div class="liBox liBox_style">
							<ul>
								<li name="mainli"><!-- <input type="text"/> -->								
									<div>
										<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
									</div>																	
									<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>									
								</li>
								<div>
									<ul></ul>
								</div>
							</ul>
						</div>
					</div>
				</div>
				<div class="fire inputBtn">
					<div class="right">
						<input type="text" id="txtAction§" name="txtAction">
						<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
					</div>
				</div>				
				<span onclick="fnObjectView(this, 1);" class="open"></span>
				<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
				<span onclick="fnUserObjectDelete(1, this, 'T');" class="delete" style="display:block;"></span>
			</div>
			<c:if test="${USER_TYPE ne 'OA'}">
				<div class="fireAdd" name="none_obj" style="display:none;" onclick="fnSysIntentAdd(this, 'T');">+ Add</div>
			</c:if>			
		</div>		
	</div>
	
	<div id="divDetailSub" style="display:none;">
		<li name="subli">
			<div>
				<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
			</div>
			<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
		</li>
	</div>
	
	<div id="divSysintentAdd" style="display:none;">
		<div name="divSysIntent">
			<div class="input" name="none_obj" style="display:block;">
				<div class="title">시스템 intent</div>
				<!-- <input type="text" /> -->
				<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">inform()</div>
			</div>
			<div class="liBox">
				<ul>
					<li name="mainli">
						<div>
							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
						</div>
						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
						<!-- <span onclick="fnObjectView(this, 1);" class="modify"></span> -->
					</li>
					<div>
						<ul></ul>
					</div>
				</ul>
			</div>
		</div>
	</div>
	
	<div id="divSysintentRequestAdd" style="display:none;">
		<div name="divSysIntent">
			<div class="input" name="none_obj" style="display:block;">
				<div class="title">시스템 intent</div>
				<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">request()</div>
				<!-- <input type="text" name="OBJECT_NAME" value=""> -->
			</div>
			<div class="box">
				<ul>
					<li name="mainli">
						<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false" class="wordBr"></div><input type="hidden" name="SLOT_SEQ_NO" />
						<span class="remove"></span></div>
						<div class="pattern"><!-- <input type="text" name="OBJECT_REQUEST_PATTERN" /> -->
							<div>
								<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div>
							</div>
							<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
							<!-- <span onclick="fnObjectView(this, 1);" class="modify"></span> -->
						</div>
					</li>
					<div>
						<ul></ul>
					</div>
				</ul>
			</div>
			<div class="input input2" name="none_obj" style="display:block;">
				<div class="title">다음 사용자 발화 DA 제한</div>
				<input type="text" name="LIMIT_DA" />
			</div>
		</div>
	</div>
	
	<div id="divInformAdd" style="display:none;">
		<div>
			<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
		</div>		
		<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
	</div>
	
	<div id="divRequestAdd" style="display:none;">
		<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false" class="wordBr"></div><input type="hidden" name="SLOT_SEQ_NO" />
		<span class="remove"></span></div>
		<div class="pattern pattern_style">
			<div>
				<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div>
			</div>
			<span onclick="fnObjectView(this, 1);" class="modify" style="display:none;"></span>
			<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
		</div>
	</div>
	
	<div id="divrepliesAddP" style="display:none;">
		<div class="fireBox" name="wtastas">
			<div class="fireMenu" name="none_obj" style="display:none;">
				<ul>
					<li class="on">대상 슬롯 &amp; 시스템 발화</li>
					<li>시스템 발화 후 행동</li>
				</ul>
			</div>
			<div class="fireCont" style="margin-bottom: 20px;">
				<div class="fire systemfire on">						
					<div class="inputBtn" name="none_obj" style="padding-bottom:0; border-bottom:0; display:none;">
						<div class="title">시스템 발화 조건</div>
						<input type="text" id="txtUttr§" name="txtUttr" value="true">
						<button type="button" onclick="fnOpenPop(this, 'F', 'P');" title="Script Guide" class="scriptGuide"></button>
					</div>	
					<div class="input slotSearch" style="padding-bottom:0; border-bottom:0;">
						<div class="title">대상슬롯</div>
						<input type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtSlotName§" name="txtSlotName" />
						<input type="hidden" name="hidSlotSeq" />
					</div>
					<div name="divSysIntent">
						<div class="input" name="none_obj" style="display:none;">
							<div class="title">시스템 intent</div>
							<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">request()</div>
							<!-- <input type="text" name="OBJECT_NAME" value=""> -->
						</div>
						<div class="box">
							<ul>
								<li name="mainli">
									<div class="slot"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false" class="wordBr"></div><input type="hidden" name="SLOT_SEQ_NO" />
									<span class="remove"></span></div>
									<div class="pattern"><!-- <input type="text" name="OBJECT_REQUEST_PATTERN" /> -->
										<div>
											<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div>
										</div>										
										<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
									</div>
								</li>	
								<div>
									<ul></ul>
								</div>							
							</ul>
						</div>
						<div class="input input2" name="none_obj" style="display:none;">
							<div class="title">다음 사용자 발화 DA 제한</div>
							<input type="text" name="LIMIT_DA" />
						</div>
					</div>
				</div>
				<div class="fire inputBtn">
					<div class="right">
						<input type="text" id="txtAction§" name="txtAction">
						<button type="button" onclick="fnOpenPop(this, 'A');" title="Script Guide" class="scriptGuide"></button>
					</div>
				</div>
				<span onclick="fnObjectUpDown(this, 'U');" class="up"></span>
				<span onclick="fnObjectView(this, 1);" class="open"></span>
				<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
				<span onclick="fnUserObjectDelete(1, this, 'P');" class="delete" style="display:block;"></span>
				<span onclick="fnObjectUpDown(this, 'D');" class="down"></span>
			</div>
			<c:if test="${USER_TYPE ne 'OA'}">
				<div class="fireAdd"  name="none_obj" style="display:none;"  onclick="fnSysIntentAdd(this, 'P');">+ Add</div>
			</c:if>	
		</div>		
	</div>
	
	<div id="divDetailSubP" style="display:none;">
		<li name="subli">
			<div><div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div></div>			
			<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
		</li>
	</div>
	
	<div id="divAddTaskAction" style="display:none;">
		<div name="TaskAction" class="body taskAction">
			<div class="popInput">
				<div class="title">적용시점</div>
				<div class="input">
					<select id="ddlType§">
                       <option value="0">사용자 발화 처리 전</option>
                       <option value="1">사용자 발화 처리 후</option>
                   </select>
				</div>
			</div>
			<div class="popInput">
			<div class="title">조건</div>
				<div class="input">
					<input type="text" id="txtMonitorCon§" name="txtMonitorCon" />
					<button type="button" onclick="fnOpenPop(this, 'E', 'T');" title="Script Guide" class="scriptGuide"></button>
				</div>
			</div>
			<div class="popInput">
				<div class="title">행동</div>
				<div class="input">
					<input type="text" id="txtMonitorAct§" name="txtMonitorAct" />
					<button type="button" onclick="fnOpenPop(this, 'E', 'T');" title="Script Guide" class="scriptGuide"></button>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>