<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var iSeqNo = '';
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var pop;
		
		window.onload = function() {
			$('#divreplies').on({
				focusout: function(e) {
					$this = $(this);
					var orgval;
					var $parentobj = $this.parent().parent().parent().parent().parent().parent();					
					
					if ($this.text().toLowerCase().indexOf('request') > -1) {											
						if($parentobj.find('[name=OBJECT_REQUEST]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
							$parentobj.find('.wtastas01').html($('#divRequestAdd').html());
							$parentobj.find('.wtastas02').css('padding-left', '110px');
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').attr('name', 'OBJECT_REQUEST_PATTERN');
							$parentobj.append('<div name="divDA" style="padding-left: 64px;"><span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;<input type="text" name="LIMIT_DA" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;"></div>');
						}
					}
					else {
						if($parentobj.find('[name=OBJECT_DETAIL_NAME]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
							$parentobj.find('.wtastas01').html($('#divInformAdd').html());
							$parentobj.find('.wtastas02').css('padding-left', '');
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').attr('name', 'OBJECT_DETAIL_NAME');
							$parentobj.find('[name=divDA]').remove();
						}
					}
				}		
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {						
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
					//fnOpenPop(this, 'S');
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');					
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
										
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					
					/* if($this.parent().parent().parent().parent().parent().next().length == 0 ) {
						$this.parent().parent().parent().parent().parent().parent().append($('#divDetailSubP').html());
					} */
					
					if ($this.text() != '') {
						if($this.parent().parent().parent().parent().parent().next().attr('name') == 'divDA') {
							$this.parent().parent().parent().parent().parent().after($('#divDetailSubP').html());
						}			
					}
				},
				change: function(e) {
					bWorkChk = true;
				},
				keydown: function(e) {
					if(e.keyCode == 9) {
						$this = $(this);
						if ($this.text() != '') {
							if($this.parent().parent().parent().parent().parent().next().attr('name') == 'divDA') {
								$this.parent().parent().parent().parent().parent().after($('#divDetailSubP').html());
							}
							fnCursorEnd($this.parent().parent().parent().parent().parent().next().find('td').eq(1).find('div').eq(0).get(0));
						}						
						fnpreventDefault(e);
					}
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
					
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					if ($this.text() != '') {
						if($this.parent().parent().parent().parent().parent().next().length == 0 || $this.parent().parent().parent().parent().parent().next().attr('class') == 'wtastas01 btn_hover') {					
							$this.parent().parent().parent().parent().parent().after($('#divDetailSub').html());						
						}
					}
				},
				focusout: function(e) {
					var str = $(this).text();
					var arrvalidation = fnSlotValidation(str, slotdata);
					if (!arrvalidation[0]) {
						var arrtempval = new Array();
						for (var i = 1; i < arrvalidation.length; i++) {
							arrtempval.push(arrvalidation[i]);
						}
						alert('존재 하지 않는 슬롯입니다.\n(' + arrtempval.join(', ') + ')');
					}
				},
				keydown: function(e) {
					if(e.keyCode == 38) {
						//$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						//$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {											
						$this = $(this);
						if ($this.text() != '') {
							if($this.parent().parent().parent().parent().parent().next().length == 0 || $this.parent().parent().parent().parent().parent().next().attr('class') == 'wtastas01 btn_hover') {
								$this.parent().parent().parent().parent().parent().after($('#divDetailSub').html());
							}
							fnCursorEnd($this.parent().parent().parent().parent().parent().next().find('div').eq(0).get(0));
						}						
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
				
			$('#relatedSlot').click(function() {
				pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />?Type=related&value=" + encodeURIComponent($(this).val()), "SlotSearchpop", 430, 550, 0, 0);
			});
			
			$('#tbTaskMonitor').on({				
				keyup : function(e) {
					$(this).val($(this).val().replace(/[^0-9]/g, ""));
				}
			}, '[name=COUNT]');
			
			$('#divreplies').on('click', '.close_btn', function(e) {
				$(this).prev().val('');
				$(this).prev().prev().text('');
				bWorkChk = true;
				fnpreventDefault(e);
			});
			
			if ('${SEQ_NO}' != '') {
				$('#selResetSlot').val('${Agent.RESET_SLOT}');
				$('#selResetSlot').trigger("chosen:updated");
				$('#selTaskType').val('${Agent.TASK_TYPE}');
				$('#selTaskType').trigger("chosen:updated");
				$('#relatedSlot').val('${Agent.RELATED_SLOTS}');
				$('#txtDescription').val('${Agent.DESCRIPTION}');
			}					
			
			$('#selResetSlot, #selTaskType, #txtAgentName, #relatedSlot').change(function() {
				bWorkChk = true;
			});
			
			//checkbox
			$('#initTaskChk').picker({
				customClass: 'list_check'
			});
			//checkbox_end
			
			if ('${Agent.SORT_NO}' == '0')
				$('#initTaskChk').picker('check');
						
			$('#txtTaskGoal').val('${Agent.TASK_GOAL}');
			
			if ('${fn:replace(Condition, "'", "\\'")}' == '')
				$('#divCondition').hide();
			else
				$('#divTaskGoal').hide();
			
			if ($('#lnb .sub .on').eq(0).text() != '${Agent.AGENT_NAME}') {
				$('#lnb .b2_btn_off').attr('class','b2_btn');
	    		$('#lnb .sub .on').removeClass('on'); 
	    		$('#lnb .sub .onsub2').removeClass('onsub2');
	    		$('#lnb .sub02').slideUp();
	    		var $spobj = $('#sp${Agent.AGENT_NAME}');
	    		$spobj.attr('class','b2_btn_off');
	    		$spobj.parent().attr('class', 'on');
	    		$spobj.parent().parent().children('.sub02').slideDown();
	    		$spobj.parent().parent().find('.sub02 li:last a').addClass('onsub2');
			}
		}
		
		$(window).bind("beforeunload", function (e){
			if (!gfn_isNull(pop)) {
				pop.close();
			}
		});
		
		function fnColGroupChange(seqno) {
			$('#tbcolg' + seqno).html('<col width="120px" /><col width="*" /><col width="110px" />');			
		}
		
		function fnSave() {
			var bTaskSaveChk = false;
			var focusObj;
			var cnt = 0;
			$txtAgentName = $('#txtAgentName');

		
			if ($txtAgentName.val() == 'Task name') {
				alert('Task명을 입력 하세요.');
				fnpreventDefault(event);
				return false;
			}
			
			if ($txtAgentName.val().toUpperCase() == 'END') {
				alert('END: 해당 이름으로는 task를 만드실 수 없습니다.');
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_-]/; 
			if (reg.test($txtAgentName.val())) {
				alert('태스크명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$txtAgentName.focus();
				fnpreventDefault(event);
				return;
			}
			
			if('${SEQ_NO}' == '') {
				$('#lnb .lnb01 .sub').children().each(function(e) {				
					if ($(this).children('a').text() == $txtAgentName.val()) {
						bTaskSaveChk = true;
						alert('이미 존재하는 Task 입니다.');
						return false;
					}
				});
			}
			
			if (bTaskSaveChk) {
				return false;
			}
						
			reg = /[^a-zA-Z0-9_\-=()."]/; 
			fnLoading();
			var arr = new Object();
			var mainarrobj = new Array();
			var typevalue;
			$('[id^=divrepliesT]').each(function(idx, e) {
				if (idx == 0) 
					typevalue = 1; 
				else 
					typevalue = 2;
				$(this).find('[name=wtastas]').each(function(idx, e) {					
					var mainitem = new Object();
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();
					mainitem.INTENTION_TYPE = typevalue;
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';

					var arrobj = new Array();
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						cnt = 0;
						var item = new Object();
						var arrobjsub = new Array();
						
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'inform()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bTaskSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt ++;
								}
							});
						}
						
						$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
							if(!gfn_isNull($(this).text())) {
								var subitem = new Object();
								subitem.TYPE = 'T';
								subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
								
								subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
								var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
								if (tempstr == '<br>')
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
								arrobjsub.push(subitem);
								cnt ++;
							}
						});										
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;
							
							arrobj.push(item);												
						}										
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);	
					}					
				});
			});
			
			arr.INTENTION_ITEM = mainarrobj;
						
			var arrslotitem = new Array();			
			
			$('#divrepliesP [name=wtastas]').each(function(idx, e) {				
				mainarrobj = new Array();
				var mainarrobjitem = new Object();
				var $txtSlotName = $(this).find('[name=txtSlotName]');
						
				if($txtSlotName.val().trim() != '') {
					mainarrobjitem.SLOT_SEQ = $txtSlotName.next().val();
					mainarrobjitem.SLOT_NAME = $txtSlotName.val();										
					
					if (mainarrobjitem.SLOT_NAME.indexOf(',') < 0)
						mainarrobjitem.CONDITION = 'ExistValue("' + mainarrobjitem.SLOT_NAME + '")==false';	
					else {
						var arrtmp = mainarrobjitem.SLOT_NAME.split(', ');
						var str = '';
						for (var i = 0; i < arrtmp.length; i++) {
							if (i == 0)
								str += 'ExistValue("' + arrtmp[i] + '")==false';
							else
								str += ' and ExistValue("' + arrtmp[i] + '")==false';
						}
						mainarrobjitem.CONDITION = str;
					}					
					mainarrobjitem.PROGRESS_SLOT = mainarrobjitem.SLOT_NAME;
								
					var mainitem = new Object();
					var arrobj = new Array();
					
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();
					mainitem.INTENTION_TYPE = 3;
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';
						
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						var item = new Object();
						var arrobjsub = new Array();
						cnt = 0;
						
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'request()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bTaskSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}										
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);									
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});
						}
						
						$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
							if(!gfn_isNull($(this).text())) {
								var subitem = new Object();
								subitem.TYPE = 'T';
								subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
								
								subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
								var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
								if (tempstr == '<br>')
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
								arrobjsub.push(subitem);
								cnt++;
							}
						});
																	
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;							
							arrobj.push(item);
						}
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
						
						mainarrobjitem.INTENTION_ITEM = mainarrobj;
						arrslotitem.push(mainarrobjitem);	
					}
				}
			});
			
			if (bTaskSaveChk) {
				fnLoading();
				alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				focusObj.focus();
				return false;
			}
			
			var SortNo = ${fn:length(submenu)};
			if ('${Agent.SORT_NO}' != '') {
				SortNo = '${Agent.SORT_NO}';
			}
			if ($('#initTaskChk').attr('checked') == 'checked')
				SortNo = 0;
			
			if (${fn:length(submenu)} == 1) {
				SortNo = 0;
			}
			
			arr.SLOT_FILLING_ITEM = arrslotitem;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertagent.do' />");			
			comSubmit.addParam("AGENT_NAME", $txtAgentName.val());
			comSubmit.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comSubmit.addParam("SORT_NO", SortNo);				
			comSubmit.addParam("TASK_GOAL", $('#txtTaskGoal').val());
			comSubmit.addParam("RESET_SLOT", $('#selResetSlot').val());
			comSubmit.addParam("TASK_TYPE", $('#selTaskType').val());
			comSubmit.addParam("RELATED_SLOTS", $('#relatedSlot').val());
			comSubmit.addParam("DESCRIPTION", $('#txtDescription').val());
			comSubmit.addParam("OBJECT_ITEM", jsoobj);			
			
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '')
					comSubmit.addParam("SEQ_NO", '');
				else
					comSubmit.addParam("SEQ_NO", iSeqNo);
			}
			else
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.submit();
			/* var comAjax = new ComAjax();				
			comAjax.setUrl("<c:url value='/view/insertagent.do' />");
			comAjax.setCallback("fnSaveCallBack");
			comAjax.addParam("AGENT_NAME", $txtAgentName.val());
			//comAjax.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comAjax.addParam("SORT_NO", SortNo);
			comAjax.addParam("RESET_SLOT", $('#selResetSlot').val());
			comAjax.addParam("TASK_TYPE", $('#selTaskType').val());
			comAjax.addParam("RELATED_SLOTS", $('#relatedSlot').val());
			comAjax.addParam("DESCRIPTION", $('#txtDescription').val());
			comAjax.addParam("OBJECT_ITEM", jsoobj);			
			
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '')
					comAjax.addParam("SEQ_NO", '');
				else
					comAjax.addParam("SEQ_NO", iSeqNo);
			}
			else
				comAjax.addParam("SEQ_NO", '${SEQ_NO}');
			comAjax.addParam("depth1", '${depth1}');
			comAjax.addParam("depth2", '${depth2}');
			comAjax.addParam("depth3", '${depth3}');
			comAjax.ajax();			 */
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {
			alert('저장 되었습니다.');
			fnLoading();
			if (iSeqNo == '' && '${SEQ_NO}' == '') {
				iSeqNo = data.SgEQ_NO;
			
				var str = '';			
				str = '<li>';
				str += '<a href="#">' + $('#txtAgentName').val() + '<span class="b2_btn"></span></a>';
				str += '<input type="hidden" value="' + iSeqNo + '">';
				str += '<ul class="sub02" style="display:none;">';											
				str += '<li><a href="#">Entities</a></li>';
				str += '<li><a href="#">Intents</a></li>';
				str += '<li><a href="#">Property</a></li>';																
				str += '</ul>';
				str += '</li>';
				$('#menu1').next().append(str);	
			}			
			bWorkChk = false;
		}
		
		function fnTransactionAdd(type) {
			if (type == 'P') {
				if ($('#divrepliesP').find('[name=wtastas]').length == 15) {					
					fnpreventDefault(event);
					return false;
				}
				$('#divrepliesP').append($('#divrepliesAddP').html().replace(/§/g, type + '_' + $('#divreplies' + type + ' [name=divSysIntent]').length));
			}
			else {
				if ($('#divreplies' + type).find('[name=wtastas]').length == 15) {
					fnpreventDefault(event);
					return false;
				}
				$('#divreplies' + type).append($('#divrepliesAddT').html().replace(/§/g, type + '_' + $('#divreplies' + type + ' [name=divSysIntent]').length));
			}
						
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj, Type) {
			if (SeqNo == 1) {							
				if ($(obj).parent().attr('name') == undefined) {					
					var $parentobj = $(obj).parent().parent().parent().parent().parent().parent();
					if ($parentobj.find('.wtastas01, .wtastas02').length > 1) {						
						$parentobj.find('.wtastas01').remove();			
						
						if ($parentobj.find('.wtastas02').length > 0) {									
							if ($parentobj.find('.sys_intent [name=OBJECT_NAME]').text().toLowerCase().indexOf('request') > -1) {				
								var objval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
								$parentobj.find('.wtastas02').eq(0).attr('class', 'wtastas01 btn_hover').html($('#divRequestAdd').html()).css('padding-left', '');
								$parentobj.find('.wtastas01').eq(0).find('[name=OBJECT_REQUEST_PATTERN]').text(objval);
							}
							else {					
								var objval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
								$parentobj.find('.wtastas02').eq(0).attr('class', 'wtastas01 btn_hover').html($('#divInformAdd').html());
								$parentobj.find('.wtastas01').eq(0).find('[name=OBJECT_DETAIL_NAME]').text(objval);
							}
							
							$parentobj.parent().find('.wta_tb_mod').hide();
							
							if ($parentobj.parent().prev().css('display') == 'none') {
								$parentobj.parent().find('[name=divSysIntent]').eq(0).find('.wtastas01').eq(0).find('.wta_tb_mod').show();								
							}
						}
						else {
							if ($(obj).parent().attr('name') == undefined)
								$parentobj.parent().remove();						
						}	
					}
					else {
						if (Type == 'T') {
							if ($parentobj.parent().prev().css('display') == 'none') {							
								if ($parentobj.parent().find('[name=divSysIntent]').length == 1)
									$parentobj.parent().parent().remove();
								else {
									$parentobj.parent().find('[name=divSysIntent]').eq(1).find('.wtastas01').eq(0).find('.wta_tb_mod').show();
									$parentobj.remove();								
								}
							}
							else
								$(obj).parent().parent().find('[name^=OBJECT_]').text('');
						}
						else {
							if ($parentobj.parent().prev().prev().css('display') == 'none') {								
								if ($parentobj.parent().find('[name=divSysIntent]').length == 1)
									$parentobj.parent().parent().remove();
								else {
									$parentobj.parent().find('[name=divSysIntent]').eq(1).find('.wtastas01').eq(0).find('.wta_tb_mod').show();
									$parentobj.remove();								
								}
							}
							else
								$(obj).parent().parent().find('[name^=OBJECT_]').text('');
						}
					}
				}
				else
					$(obj).parent().parent().parent().remove();
			}
			else if (SeqNo == 2)
				$(obj).parent().parent().parent().parent().parent().remove();				 
			else
				$(obj).parent().remove();
			
			bWorkChk = true;
			fnpreventDefault(event);
		}
		
		function fnTransactionSave() {			
			fnpreventDefault(event);
		}
		
		function fnAddCalcel() {
			$.modal.close();
			//$('#divmodal').hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj, type, flag) {
			var width = '713';
			var heigth = '626';
			if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
				width = '613';
				heigth = '400';	
			}
			if (type == 'F') {
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().prev().find('input').attr('id') + '&flag=' + flag, "AgentFunctionpop", width, heigth, 0, 0);	
			}
			else if (type == 'A') {
				pop = fnWinPop("<c:url value='/view/openactionwrite.do' />" + "?obj=" + $(obj).parent().prev().find('input').attr('id'), "AgentActionpop", 570, 470, 0, 0);	
			}
			else if (type == 'E') {
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id') + "&flag=A&Type=CC&Title=", "AgentFunctionpop", width, heigth, 0, 0);				
			}
			else if (type == 'S') {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id') + "&Type=TS", "AgentSearchpop", 430, 550, 0, 0);
			}
			else {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id'), "AgentSearchpop", 430, 550, 0, 0);
			}
			
			fnpreventDefault(event);
		}
		
		function fnChkAll() {
			$('[id^=schk]').picker("update");
		}
		
		function fnformfillingChk(obj) {
			if($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
				$('#divform').hide()	
			}
			else {
				$(obj).picker('check');
				$('#divform').show();				
			}
		}
		
		function fnClassChk(seq, obj) {
			if($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
				$('#divClass' + seq).hide()	
			}
			else {
				$(obj).picker('check');
				$('#divClass' + seq).show();
			}
		}
		
		function fnCheck(obj, type) {
			if ($(obj).attr('checked') == 'checked') {
				
				if (type == 'I' && '${Agent.SORT_NO}' == '0') {
					alert('반드시 하나의 task는 initial task로 설정되어야 합니다.');
					return;
				}
				$(obj).picker("uncheck");
			}
			else {
				if (type == 'E' && '${Agent.SORT_NO}' == '0') {
					alert('반드시 하나의 task는 initial task로 설정되어야 합니다.');
					return;
				}
				
				$('#initTaskChk').picker("uncheck");			
				$(obj).picker('check');
			}
		}
		
		function fnObjectView(obj, type) {
			if (type == 1) {							
				var $parentobj = $(obj).parent().parent().parent().parent().parent().parent().parent().find('.wtastas_cutn');
				if ($parentobj.css('display') == 'none') {
					//$parentobj.parent().parent().css('border', '1px solid #c5c5c5').find('[name=none_obj], [name=divDA]').show();
					$parentobj.parent().parent().find('[name=none_obj], [name=divDA]').show();					
					$(obj).parent().parent().parent().parent().parent().parent().parent().find('.wta_tb_mod').hide();
				}
				else {
					//$parentobj.parent().parent().css('border', 'none').find('[name=none_obj], [name=divDA]').hide();
					$parentobj.parent().parent().find('[name=none_obj], [name=divDA]').hide();
				}
			}
			else {
				var $parentobj = $(obj).parent().parent();
				if ($parentobj.css('display') == 'none') {
					//$parentobj.parent().css('border', '1px solid #c5c5c5').find('[name=none_obj], [name=divDA]').show();
					$parentobj.parent().find('[name=none_obj], [name=divDA]').show();										
				}
				
				else {
					//$parentobj.parent().css('border', 'none').find('[name=none_obj], [name=divDA]').hide();
					$parentobj.parent().find('[name=none_obj], [name=divDA]').hide();
					$parentobj.find('.wtastas01').eq(0).find('.wta_tb_mod').show();
				}
			}
							
			fnpreventDefault(event);
		}
		
		function fnCopyTask() {
			pop = fnWinPop("<c:url value='/view/copytask.do' />", "copyTaskPop", 700, 550, 0, 0);
		}
		
		function fnCopyMove(seqno) {
			var comSubmit = new ComSubmit();			
			comSubmit.setUrl("<c:url value='/view/agentsub.do' />");
			comSubmit.addParam("AGENT_SEQ", seqno);
			comSubmit.addParam("COPY_CHK", "Y");
			comSubmit.addParam("depth1", 'menu1');
			comSubmit.addParam("depth2", '0');
			comSubmit.addParam("depth3", '3');
			comSubmit.submit();
		}
		
		function fnSysIntentAdd(obj, type) {
			if (type == 'T')
				$(obj).parent().parent().append($('#divSysintentAdd').html());			
			else
				$(obj).parent().parent().append($('#divSysintentRequestAdd').html());
						
			fnpreventDefault(event);
		}
		
		function fnSysIntentDel(obj) {
			$parentobj = $(obj).parent().parent().parent().parent().parent().parent();
			//if ($parentobj.parent().find('[name=divSysIntent]').length > 1) {
				$parentobj.remove();	
			//}
				
			fnpreventDefault(event);
		}
		
		function fnShowMonitoring() {
			if ('${SEQ_NO}' != '') {
				$("#divMonitoring").show();
				fnLayerPop('divMonitoring');
				fnpreventDefault(event);	
			}
			else
				alert('태스크 저장 후 작성 할 수 있습니다.');
		}
		
		function fnCancel() {
			$("#divMonitoring").hide();
			$('#layer').fadeOut();
		}
		
		function fnAddTaskAction() {
			var cnt = $('#divTaskAction').find('[name=TaskAction]').length; 
			$('#divTaskAction').append($('#divAddTaskAction').html().replace(/§/g, cnt.toString()));
			$('#ddlType' + cnt).chosen({ disable_search_threshold: 10, allow_single_deselect: true });			
			fnpreventDefault(event);
		}
		
		function fnMonitoringSave() {							
			var arrMonitoring = new Array();
			var cnt = 0;
			$('#tbTaskMonitor').find('[name=COUNT]').each(function() {
				if ($(this).val() != '') {
					arrMonitoring[cnt] = $(this).val();
					cnt++;
				}
			});
				
			if (cnt != 0 && cnt != 6) {
				alert('task monitoring에 있는 모든 칸을 채워 주세요.');
				return;
			}
			
			var arrobj = new Object();
			var arr = new Array();
			$('#divTaskAction').find('[name=TaskAction]').each(function() {
				var subobj = new Object();
				var $this = $(this);
				subobj.TYPE = $this.find('[id^=ddlType]').val();
				subobj.CON = $this.find('[id^=txtMonitorCon]').val();
				subobj.ACT = $this.find('[id^=txtMonitorAct]').val();
				
				if (subobj.CON != '') {
					arr.push(subobj);
				}				
			});	
			arrobj.ACTION_ITEM = arr;
			var jsoobj = JSON.stringify(arrobj);
			
			var comSubmit = new ComSubmit();			
			comSubmit.setUrl("<c:url value='/view/insertagentmotitor.do' />");
			comSubmit.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comSubmit.addParam("MONITORIN_TASK_0", arrMonitoring[0]);
			comSubmit.addParam("MONITORIN_TASK_1", arrMonitoring[1]);
			comSubmit.addParam("MONITORIN_TASK_2", arrMonitoring[2]);
			comSubmit.addParam("MONITORIN_USER_0", arrMonitoring[3]);
			comSubmit.addParam("MONITORIN_USER_1", arrMonitoring[4]);
			comSubmit.addParam("MONITORIN_USER_2", arrMonitoring[5]);
			comSubmit.addParam("TASK_ACTIONS_ITEM", jsoobj);
			comSubmit.addParam("depth1", 'menu1');
			comSubmit.addParam("depth2", '0');
			comSubmit.addParam("depth3", '3');
			comSubmit.submit();
		}
		
		function fnMonitoringDelete(obj) {
			$(obj).parent().parent().remove();
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="layer" id="layer">
		<div class="bg"></div>
		<!-- 모달창 -->
		<div id="divMonitoring" class="layer_box create_class_box"
			style="width:70%; height:465px; overflow-y:auto; z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display: none;">
			<div class="anp_contents" style="margin-bottom:25px;">				
				<h3 class="property_icon" style="background:url(../images/h3_bul10.png) no-repeat 0px center !important;">task monitoring</h3>
	            <div style="margin: 30px 45px 30px 65px; border-top: 1px solid rgb(225, 225, 225); border-right: 1px solid rgb(225, 225, 225); border-left: 1px solid rgb(225, 225, 225); border-image: initial; border-bottom: none; overflow: auto; display: block;">
	                <table id="tbTaskMonitor" class="slot_instan_title">
	                    <caption class="hide">task monitoring</caption>
	                    <thead>
	                        <tr>
	                            <th></th>
	                            <th>관심</th>
	                            <th>경고</th>
	                            <th>위험</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<c:choose>
								<c:when test="${fn:length(Mornitoringlist) > 0}">                  	
			                    	<c:forEach items="${Mornitoringlist}" var="row">	                        
		                        		<tr>
		                        			<c:choose>
			                        		<c:when test="${row._TYPE eq 'T'}">		                        			                        	
			                       				<td>태스크 반복 수</td>
			                        		</c:when>
			                        		<c:otherwise>
			                        			<td>최대 사용자 발화 수</td>	
			                        		</c:otherwise>
				                        	</c:choose>
				                        	<td><input type="text" value="${row.INTEREST }" name="COUNT" /></td>
				                            <td><input type="text" value="${row.WARNING }" name="COUNT" /></td>
				                            <td><input type="text" value="${row.DANGER }" name="COUNT" /></td>
				                        </tr>
			                        </c:forEach>
		                        </c:when>
		                        <c:otherwise>
		                        	<tr>
	                        			<td>태스크 반복 수</td>
			                        	<td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                        </tr>
			                        <tr>
	                        			<td>최대 사용자 발화 수</td>
			                        	<td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                            <td><input type="text" value="" name="COUNT" /></td>
			                        </tr>
		                        </c:otherwise>
	                        </c:choose>
	                    </tbody>
	                </table>
	            </div>
					
	            <h3 class="property_icon" style="background:url(../images/h3_bul11.png) no-repeat 0px center !important;">task actions<a onclick="fnAddTaskAction();" href="#">+ Add</a></h3>
				
				<div id="divTaskAction">
					<c:set var="Cnt" value="0" />
					<c:forEach items="${Actionslist}" var="row">
						<div name="TaskAction" style="margin-bottom:30px;">
							<dl class="reset_slot">
								<dt>적용시점</dt>					
					               <dd class="anp_contents_dd02 task_select_padding" style="width:75%; padding-right:0; box-sizing:border-box;">
					                   <select class="f_right u_group" id="ddlType${Cnt }">					                   
					                       <option value="0" <c:if test="${row._TYPE eq 0}">selected</c:if>>사용자 발화 처리 전</option>
					                       <option value="1" <c:if test="${row._TYPE eq 1}">selected</c:if>>사용자 발화 처리 후</option>
					                   </select>
					               </dd>
							</dl>
							<dl class="reset_slot">
							    <dt>조건</dt>
							    <dd style="width:82%; padding-right:0; box-sizing:border-box;">
							        <div>
							            <input type="text" id="txtMonitorCon${Cnt }" name="txtMonitorCon" value="${fn:replace(row._CONDITION, '\"', '&quot;')}" style="text-indent:5px; padding:5px 0px; width:65%;" />
							            <a class="class_name_instances" href="#" onclick="fnOpenPop(this, 'E', 'T');" style="padding:8px 7px; float:none;">Script Guide</a>
							        </div>
							    </dd>
							</dl>
							<dl class="reset_slot">
							    <dt>행동</dt>
							    <dd style="width:82%; padding-right:0; box-sizing:border-box;">
							        <div>
							            <input type="text" id="txtMonitorAct${Cnt }" name="txtMonitorAct" value="${fn:replace(row.ACT, '\"', '&quot;')}" style="text-indent:5px; padding:5px 0px; width:65%;" />
							            <a class="class_name_instances" href="#" onclick="fnOpenPop(this, 'E', 'T');" style="padding:8px 7px; float:none;">Script Guide</a>
							        </div>
							    </dd>
							</dl>
							<div class="taskDeleteBtn"><a href="#" onclick="fnMonitoringDelete(this);"><img src="../images/delete_btn.png" alt="deleteBtn" /></a></div>
						</div>
						<c:set var="Cnt" value="${Cnt + 1 }" />
					</c:forEach>				
	            </div>
			</div>
			<div class="layer_box_btn">
				<a onclick="fnMonitoringSave();" href="#">Save</a> <a
					onclick="fnCancel();" href="#">Cancel</a>
			</div>
		</div>
		<!-- 모달창 -->
	</div>
	<div class="anp">
		<div class="cont_title">
			<div class="Agent_name_box">
				<div class="Agent_name_box_word">
		            <div class="s_word">
		                <label for="search">	                	
		                	<c:choose>
		                		<c:when test="${null eq Agent}">	                			
		                			<input type="text" id="txtAgentName" name="txtAgentName" value="Task name" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
		                		</c:when>
		                		<c:otherwise>
		                			<c:choose>	                	
		                				<c:when test='${Agent.AGENT_NAME != ""}'>
		                					<input type="text" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
	                					</c:when>
	                					<c:otherwise>		                						
	                						<input type="hidden" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
	                					</c:otherwise>
	               					</c:choose>
		                		</c:otherwise>
		                	</c:choose>
							
		                    <a href="#" id="saveIntents" onclick="fnSave();" title="저장"><span class="project_save">Save</span></a>
		                </label>
		            </div>
	            </div>
	        </div>
        </div>
		<div id="divreplies" class="anp_contents" style="margin-top:25px;">
			<input type="hidden" id="hidurl" value="<c:url value='/view/openfunctionwrite.do' />" />

			<h3 class="property_icon" style="background:url(../images/h3_bul05.png) no-repeat 0px center !important;">다른 task 복사해오기</h3>
			<div class="search_box" style="border-bottom:none; height:40px; width:75%;">				
				<div class="s_word" style="margin:0 65px 0;">
					<c:choose>
						<c:when test="${SEQ_NO != null}">
							<c:choose>
								<c:when test="${Agent.FROM_COPY == null }">								
									<input type="text" id="CopyTask" value="" readonly="readonly" />									
								</c:when>
								<c:otherwise>									
									<input type="text" id="CopyTask" value="${Agent.FROM_COPY}" readonly="readonly" style="font-size: 16px; color: #444;"/>
								</c:otherwise>								
							</c:choose>
						</c:when>
						<c:otherwise>
							<label for="slot_search"> 
								<input type="text" id="CopyTask" value="" readonly="readonly" />
								<a onclick="fnCopyTask();" href="#" title="검색" class="solt_search" style="background:url(../images/btn_slot_search.png) no-repeat center center;"><span class="hide">search</span></a>
							</label>
						</c:otherwise>
					</c:choose>					
				</div>
			</div>
				
			<h3 class="property_icon">Property</h3>
			<dl class="reset_slot">
				<dt>task goal</dt>
				<dd style="width:75%;">
					<div id="divCondition">
						<textarea id="txtCondition" style="border:1px solid #c5c5c5; width: 99%; padding-left:5px; color:#000000;" readonly="readonly">${Condition}</textarea>
					</div>
					<div id="divTaskGoal">											
						<input type="text" id="txtTaskGoal" name="txtTaskGoal" style="text-indent:5px; padding:5px 0px; width:65%;" />
						<a class="class_name_instances" href="#" onclick="fnOpenPop(this, 'E', 'T');" style="padding:8px 7px; float:none;">Script Guide</a>											
					</div>
				</dd>  
			</dl>
			
			<dl class="reset_slot" style="display:none;">
				<dt>task type</dt>
				<!-- <dd class="anp_contents_dd01">
					<label for="reset_slot01">
						<input type="text" value="" onblur="" onclick="" id="reset_slot01" name="reset_slot01" />
					</label>
				</dd> -->
				<dd class="anp_contents_dd02">
					<select class="f_right u_group" id="selTaskType" name="selTaskType">
						<option value="essential">essential</option>
						<option value="optional">optional</option>
					</select>
				</dd>
			</dl>
			
			<dl class="reset_slot">
				<dt>related slots</dt>
				<dd class="anp_contents_dd02">
					<label for="reset_slot01">
						<input type="text" value="" id="relatedSlot" name="relatedSlot" />
					</label>
				</dd>
			</dl>
			
			<dl class="reset_slot">
				<dt>reset slot</dt>
				<!-- <dd class="anp_contents_dd01">
					<label for="reset_slot01">
						<input type="text" value="" onblur="" onclick="" id="reset_slot01" name="reset_slot01" />
					</label>
				</dd> -->
				<dd class="anp_contents_dd02">
					<select class="f_right u_group" id="selResetSlot" name="selResetSlot">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</dd>
			</dl>
			
			<dl class="reset_slot">
				<dt>initial</dt>
				<dd>
			        <span>                        	
						<input type="checkbox" onclick="fnCheck(this, 'I');" id="initTaskChk" name="initTaskChk" value="" style="margin: 8px;"/><label for="initTaskChk" style="vertical-align: text-top; line-height:22px;"></label>
					</span>
				</dd>
			</dl>
						
			<dl class="reset_slot">
				<dt>description</dt>
				<dd class="anp_contents_dd02">
					<label for="reset_slot01">
						<input type="text" value="" id="txtDescription" name="txtDescription" />
					</label>
				</dd>
			</dl>
						
			<c:if test="${USER_TYPE ne 'OA'}">
			<h3 class="property_icon">define actions and monitoring information
				<a onclick="fnShowMonitoring();" href="#">View</a>
			</h3>
			</c:if>
			
			<div id="divrepliesT1" class="sortable">
				<h3 class="speech_transaction_icon">When the agent starts, the agent says : <a onclick="fnTransactionAdd('T1');" href="#">+ Add</a></h3>
				<c:choose>
					<c:when test="${fn:length(Intentionlist_1) > 0}">
						<c:forEach items="${Intentionlist_1}" var="row">
							<div class="wtastas" name="wtastas">
								<div class="wtastas_add mt20" name="none_obj" style="display:none;">
									<div class="wtastas01">
										<table>
											<colgroup>
												<col width="150px" />
												<col width="*" />
												<col width="110px" />
											</colgroup>
											<tbody>
												<tr>
													<td>
														<h3 style="margin:0px;">시스템 발화 조건</h3>
													</td>
													<td>
														<input class="wtastas_txt01" type="text" id="txtEUttrT1_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
													</td>
													<td>
														<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'F', 'T');">Script Guide</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>					
															
								
								<div class="wtastas_add">
									<div class="wtastas_cutn" name="none_obj" style="display:none;">
										<h3>시스템 발화</h3> 
										<c:if test="${USER_TYPE ne 'OA'}">
										<a onclick="fnSysIntentAdd(this, 'T');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
										</c:if>
										<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
										<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
									</div>									
									<c:choose>
										<c:when test="${fn:length(Objectlist_1) > 0}">
											<c:set var="EditChk" value="0" />
											<c:forEach items="${Objectlist_1}" var="orow">																																																						
												<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
													<div name="divSysIntent" class="intentBox">										
														<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
															<table>
																<colgroup>
																	<col width="100px" />
																	<col width="*" />
																	<col width="110px" />
																</colgroup>
																<tbody>
																	<tr>
																		<td>
																			<h4>시스템 intent</h4>
																		</td>
																		<td>
																			<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																		</td>
																		<td>
																			<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														
														<c:choose>
															<c:when test="${fn:length(ObjectDtllist_1) > 0}">
																<c:set var="Cnt" value="0" />
																<c:set var="Request" value="" />
																<c:forEach items="${ObjectDtllist_1}" var="drow">																											
																	<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">															
																		<c:choose>
																			<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																				<c:choose>
																					<c:when test="${Cnt eq 0}">
																						<c:set var="Cnt" value="1" />
																						<c:set var="Request" value="${drow.OBJECT_VALUE }" />																										
																					</c:when>
																					<c:otherwise>
																						
																					</c:otherwise>
																				</c:choose>															
																			</c:when>
																			<c:otherwise>
																				<c:choose>
																					<c:when test="${Cnt eq 0}">
																						<c:set var="Cnt" value="1" />
																						<div class="wtastas01 btn_hover">
																							<table>
																								<colgroup>
																									<col width="*" />
																									<col width="110px" />
																								</colgroup>
																								<tbody>
																									<tr>
																										<td>																											
																											<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																										</td>
																										<td>
																											<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
																											<c:choose>
																												<c:when test='${EditChk == 0}'>
																													<c:set var="EditChk" value="1" />
																													<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																												</c:when>
																												<c:otherwise>
																													<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																												</c:otherwise>
																											</c:choose>
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</div>
																					</c:when>
																					<c:otherwise>
																						<c:choose>
																							<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																								<div class="wtastas02 btn_hover">
																									<table>
																										<colgroup>
																										    <col width="42px" />
																											<col width="*" />
																											<col width="110px" />
																										</colgroup>
																										<tbody>
																											<tr>
																											    <td class="liImg">
																												   
																												</td>
																												<td>
																													<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																												</td>
																												<td>
																													<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'T');"></a>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</div>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${Cnt eq 1}">
																										<c:set var="Cnt" value="2" />
																										<div class="wtastas01 btn_hover">
																											<table>
																												<colgroup>
																													<col width="120px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																														<td>
																															<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true">${Request }</div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
																														</td>
																														<td>									
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
																															<c:choose>
																																<c:when test='${EditChk == 0}'>
																																	<c:set var="EditChk" value="1" />
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																																</c:when>
																																<c:otherwise>
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																																</c:otherwise>
																															</c:choose>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:when>
																									<c:otherwise>
																										<div class="wtastas02 btn_hover" style="padding-left:110px;">
																											<table>
																												<colgroup>
																												    <col width="42px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																													    <td class="liImg">
																														   
																														</td>
																														<td>																														
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>																												
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'T');"></a>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:otherwise>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:otherwise>
																				</c:choose>
																			</c:otherwise>
																		</c:choose>
																	</c:if>
																</c:forEach>
															</c:when>
															<c:otherwise>
																													
															</c:otherwise>
														</c:choose>
														
														<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
															<div name="divDA" style="padding-left: 64px; display:none;">
																<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
																<input type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;">
															</div>
														</c:if>	
													</div>													
												</c:if>
											</c:forEach>
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>																
								</div>										
											
								<div class="wtastas_add" name="none_obj" style="display:none;">
									<div class="wtastas01">
										<table>
											<colgroup>
												<col width="150px" />
												<col width="*" />
												<col width="110px" />
											</colgroup>
											<tbody>
												<tr>
													<td>	
														<h3 style="margin:0px;">시스템 발화 후 행동</h3>
													</td>
													<td>
														<input class="wtastas_txt01" type="text" id="txtEActionT1_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
													</td>
													<td>
														<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'A');">Script Guide</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</c:forEach>							
					</c:when>					
				</c:choose>
			</div>
			
			
			<div id="divrepliesT2" class="sortable" <c:if test="${USER_TYPE eq 'OA'}">style="display:none;"</c:if>>
				<h3 class="speech_transaction_icon">When the agent restarts, the agent says : <a onclick="fnTransactionAdd('T2');" href="#">+ Add</a></h3>
				<c:choose>
					<c:when test="${fn:length(Intentionlist_2) > 0}">
						<c:forEach items="${Intentionlist_2}" var="row">
							<div class="wtastas" name="wtastas">
								<div class="wtastas_add mt20" name="none_obj" style="display:none;">
									<div class="wtastas01">
										<table>
											<colgroup>
												<col width="150px" />
												<col width="*" />
												<col width="110px" />
											</colgroup>
											<tbody>
												<tr>
													<td>
														<h3 style="margin:0px;">시스템 발화 조건</h3>
													</td>
													<td>
														<input class="wtastas_txt01" type="text" id="txtEUttrT2_${row.SEQ_NO}" name="txtUttr" 	value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
													</td>
													<td>
														<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'F', 'T');">Script Guide</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>					
								
								<div class="wtastas_add">
									<div class="wtastas_cutn" name="none_obj" style="display:none;">
										<h3>시스템 발화</h3> 
										<c:if test="${USER_TYPE ne 'OA'}">
										<a onclick="fnSysIntentAdd(this, 'T');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
										</c:if>
										<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
										<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
									</div>									
									<c:choose>
										<c:when test="${fn:length(Objectlist_2) > 0}">
											<c:set var="EditChk" value="0" />
											<c:forEach items="${Objectlist_2}" var="orow">																																																						
												<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
													<div name="divSysIntent" class="intentBox">										
														<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
															<table>
																<colgroup>
																	<col width="100px" />
																	<col width="*" />
																	<col width="110px" />
																</colgroup>
																<tbody>
																	<tr>
																		<td>
																			<h4>시스템 intent</h4>
																		</td>
																		<td>
																			<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																		</td>
																		<td>
																			<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
																		</td>
																	</tr>
																</tbody>
															</table>			
														</div>
														
														<c:choose>
															<c:when test="${fn:length(ObjectDtllist_2) > 0}">
																<c:set var="Cnt" value="0" />
																<c:set var="Request" value="" />
																<c:forEach items="${ObjectDtllist_2}" var="drow">																											
																	<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">															
																		<c:choose>
																			<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																				<c:choose>
																					<c:when test="${Cnt eq 0}">
																						<c:set var="Cnt" value="1" />
																						<c:set var="Request" value="${drow.OBJECT_VALUE }" />																										
																					</c:when>
																					<c:otherwise>
																						
																					</c:otherwise>
																				</c:choose>															
																			</c:when>
																			<c:otherwise>
																				<c:choose>
																					<c:when test="${Cnt eq 0}">
																						<c:set var="Cnt" value="1" />
																						<div class="wtastas01 btn_hover">
																							<table>
																								<colgroup>
																									<col width="*" />
																									<col width="110px" />
																								</colgroup>
																								<tbody>
																									<tr>
																										<td>																											
																											<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																										</td>
																										<td>
																											<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
																											<c:choose>
																												<c:when test='${EditChk == 0}'>
																													<c:set var="EditChk" value="1" />
																													<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																												</c:when>
																												<c:otherwise>
																													<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																												</c:otherwise>
																											</c:choose>
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</div>
																					</c:when>
																					<c:otherwise>
																						<c:choose>
																							<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																								<div class="wtastas02 btn_hover">
																									<table>
																										<colgroup>
																										    <col width="42px" />
																											<col width="*" />
																											<col width="110px" />
																										</colgroup>
																										<tbody>
																											<tr>
																											    <td class="liImg">
																												   
																												</td>
																												<td>
																													<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																												</td>
																												<td>
																													<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'T');"></a>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</div>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${Cnt eq 1}">
																										<c:set var="Cnt" value="2" />
																										<div class="wtastas01 btn_hover">
																											<table>
																												<colgroup>
																													<col width="120px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																														<td>
																															<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true">${Request }</div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
																														</td>
																														<td>									
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
																															<c:choose>
																																<c:when test='${EditChk == 0}'>
																																	<c:set var="EditChk" value="1" />
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																																</c:when>
																																<c:otherwise>
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																																</c:otherwise>
																															</c:choose>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:when>
																									<c:otherwise>
																										<div class="wtastas02 btn_hover" style="padding-left:110px;">
																											<table>
																												<colgroup>
																												    <col width="42px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																													    <td class="liImg">
																														   
																														</td>
																														<td>																														
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>																												
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'T');"></a>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:otherwise>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:otherwise>
																				</c:choose>
																			</c:otherwise>
																		</c:choose>
																	</c:if>
																</c:forEach>
															</c:when>
															<c:otherwise>
																													
															</c:otherwise>
														</c:choose>
														
														<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
															<div name="divDA" style="padding-left: 64px; display:none;">
																<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
																<input type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;">
															</div>
														</c:if>
													</div>																								
												</c:if>
											</c:forEach>
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>																
								</div>
											
								<div class="wtastas_add" name="none_obj" style="display:none;">
									<div class="wtastas01">
										<table>
											<colgroup>
												<col width="150px" />
												<col width="*" />
												<col width="110px" />
											</colgroup>
											<tbody>
												<tr>
													<td>	
														<h3 style="margin:0px;">시스템 발화 후 행동</h3>
													</td>
													<td>
														<input class="wtastas_txt01" type="text" id="txtEActionT2_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
													</td>
													<td>
														<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'A');">Script Guide</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</c:forEach>							
					</c:when>					
				</c:choose>
			</div>
			
			
			<div id="divrepliesP" class="sortable">
				<h3 class="speech_transaction_icon">The Agent gathers information from user by filling slots : <a onclick="fnTransactionAdd('P');" href="#">+ Add</a></h3>
				
				<c:choose>						
					<c:when test="${fn:length(SlotFillinglist) > 0}">
						<c:forEach items="${SlotFillinglist}" var="srow">														
							<c:choose>
								<c:when test="${fn:length(Intentionlist_3) > 0}">
									<c:forEach items="${Intentionlist_3}" var="row">
										<c:if test="${srow.SEQ_NO eq row.FILLING_SEQ}">
											<div class="wtastas" name="wtastas">
												<div class="wtastas_add mt20" name="none_obj" style="display:none;">
													<div class="wtastas01" <c:if test="${USER_TYPE eq 'OA'}">style="display:none;"</c:if>>
														<table>
															<colgroup>
																<col width="150px" />
																<col width="*" />
																<col width="110px" />
															</colgroup>
															<tbody>
																<tr>
																	<td>
																		<h3 style="margin:0px;">시스템 발화 조건</h3>
																	</td>
																	<td>
																		<input class="wtastas_txt01" type="text" id="txtEUttrT3_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
																	</td>
																	<td>
																		<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'F', 'P');">Script Guide</a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>					
												
												<div class="wtastas_add">
													<div class="wtastas01">
														<table>
															<colgroup>
																<col width="150px" />
																<col width="*" />
																<col width="110px" />
															</colgroup>
															<tbody>
																<tr>
																	<td>	
																		<h3 style="margin:0px;">대상슬롯</h3>
																	</td>
																	<td>
																		<input class="wtastas_txt01" type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtESlotName_${srow.SEQ_NO}" name="txtSlotName" value="${srow.SLOT_NAME}" />
																		<input type="hidden" name="hidSlotSeq" value="${srow.SLOT_SEQ}" />
																	</td>
																	<td>
																		
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												
												<div class="wtastas_add">
													<div class="wtastas_cutn" name="none_obj" style="display:none;">
														<h3>시스템 발화</h3>														
														<c:if test="${USER_TYPE ne 'OA'}"> 
														<a onclick="fnSysIntentAdd(this, 'P');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
														</c:if>
														<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
														<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
													</div>									
													<c:choose>
														<c:when test="${fn:length(Objectlist_3) > 0}">
															<c:set var="EditChk" value="0" />
															<c:forEach items="${Objectlist_3}" var="orow">																																																						
																<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
																	<div name="divSysIntent" class="intentBox">										
																		<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
																			<table>
																				<colgroup>
																					<col width="100px" />
																					<col width="*" />
																					<col width="110px" />
																				</colgroup>
																				<tbody>
																					<tr>
																						<td>
																							<h4>시스템 intent</h4>
																						</td>
																						<td>
																							<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																						</td>
																						<td>
																							<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
																						</td>
																					</tr>
																				</tbody>
																			</table>			
																		</div>
																		
																		<c:choose>
																			<c:when test="${fn:length(ObjectDtllist_3) > 0}">
																				<c:set var="Cnt" value="0" />
																				<c:set var="Request" value="" />
																				<c:forEach items="${ObjectDtllist_3}" var="drow">																											
																					<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">															
																						<c:choose>
																							<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																								<c:choose>
																									<c:when test="${Cnt eq 0}">
																										<c:set var="Cnt" value="1" />
																										<c:set var="Request" value="${drow.OBJECT_VALUE }" />																										
																									</c:when>
																									<c:otherwise>
																										
																									</c:otherwise>
																								</c:choose>																
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${Cnt eq 1}">
																										<c:set var="Cnt" value="2" />
																										<div class="wtastas01 btn_hover">
																											<table>
																												<colgroup>
																													<col width="120px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																														<td>
																															<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true">${Request }</div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
																														</td>
																														<td>									
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
																															<c:choose>
																																<c:when test='${EditChk == 0}'>
																																	<c:set var="EditChk" value="1" />
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																																</c:when>
																																<c:otherwise>
																																	<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																																</c:otherwise>
																															</c:choose>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:when>
																									<c:otherwise>
																										<div class="wtastas02 btn_hover" style="padding-left:110px;">
																											<table>
																												<colgroup>
																												    <col width="42px" />
																													<col width="*" />
																													<col width="110px" />
																												</colgroup>
																												<tbody>
																													<tr>
																													    <td class="liImg">
																														   
																														</td>
																														<td>																														
																															<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>																												
																														</td>
																														<td>
																															<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'P');"></a>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</c:otherwise>
																								</c:choose>																								
																							</c:otherwise>
																						</c:choose>
																					</c:if>
																				</c:forEach>
																			</c:when>
																			<c:otherwise>
																																	
																			</c:otherwise>
																		</c:choose>
																																			
																		<div name="divDA" style="padding-left: 64px; display:none;">
																			<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
																			<input type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;">
																		</div>
																	</div>																								
																</c:if>
															</c:forEach>
														</c:when>
														<c:otherwise>
															
														</c:otherwise>
													</c:choose>																
												</div>																																		
															
												<div class="wtastas_add" name="none_obj" style="display:none;">
													<div class="wtastas01">
														<table>
															<colgroup>
																<col width="150px" />
																<col width="*" />
																<col width="110px" />
															</colgroup>
															<tbody>
																<tr>
																	<td>	
																		<h3 style="margin:0px;">시스템 발화 후 행동</h3>
																	</td>
																	<td>
																		<input class="wtastas_txt01" type="text" id="txtEActionT3_${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
																	</td>
																	<td>
																		<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'A');">Script Guide</a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>															
										</c:if>
									</c:forEach>							
								</c:when>
							</c:choose>
						</c:forEach>
					</c:when>					
				</c:choose>										
			</div>
		</div>
	</div>
	
	<div id="divrepliesAddT" style="display:none;">					
		<div class="wtastas" name="wtastas">
			<div class="wtastas_add mt20" name="none_obj" style="display:none;">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<h3 style="margin:0px;">시스템 발화 조건</h3>
								</td>
								<td>
									<input class="wtastas_txt01" type="text" id="txtUttr§" name="txtUttr" value="true">
								</td>
								<td>
									<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'F', 'T');">Script Guide</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="wtastas_add">
				<div class="wtastas_cutn" name="none_obj" style="display:none;">
					<h3>시스템 발화</h3> 
					<c:if test="${USER_TYPE ne 'OA'}">
					<a onclick="fnSysIntentAdd(this, 'T');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
					</c:if>
					<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
					<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
				</div>

				<div name="divSysIntent" class="intentBox">
					<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
						<table>
							<colgroup>
								<col width="100px" />
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<h4>시스템 intent</h4>
									</td>
									<td>
										<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">inform()</div>
									</td>
									<td>
										<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
						
					<div class="wtastas01 btn_hover">
						<table>
							<colgroup>
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
										<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
									</td>
									<td>
										<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
										<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
						
			<div class="wtastas_add" name="none_obj" style="display:none;">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>	
									<h3 style="margin:0px;">시스템 발화 후 행동</h3>
								</td>
								<td>
									<input class="wtastas_txt01" type="text" id="txtAction§" name="txtAction">
								</td>
								<td>
									<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'A');">Script Guide</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="divDetailSub" style="display:none;">
		<div class="wtastas02 btn_hover">
			<table>
				<colgroup>
				    <col width="42px" />
					<col width="*" />
					<col width="110px" />
				</colgroup>
				<tbody>
					<tr>
					    <td class="liImg">
						   
						</td>
						<td>
							<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
						</td>
						<td>
							<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'T');"></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="divSysintentAdd" style="display:none;">
		<div name="divSysIntent" class="intentBox">
			<div class="sys_intent" name="none_obj" style="height:auto;">						
				<table>
					<colgroup>
						<col width="100px" />
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<h4>시스템 intent</h4>
							</td>
							<td>
								<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">inform()</div>
							</td>
							<td>
								<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
							</td>
						</tr>
					</tbody>
				</table>			
			</div>
				
			<div class="wtastas01 btn_hover">
				<table>
					<colgroup>
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
								<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
							</td>
							<td>
								<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
								<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="divSysintentRequestAdd" style="display:none;">
		<div name="divSysIntent" class="intentBox">
			<div class="sys_intent" name="none_obj" style="height:auto;">						
				<table>
					<colgroup>
						<col width="100px" />
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<h4>시스템 intent</h4>
							</td>
							<td>
								<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">request()</div>
							</td>
							<td>
								<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
							</td>
						</tr>
					</tbody>
				</table>			
			</div>
				
			<div class="wtastas01 btn_hover">
				<table>
					<colgroup>
						<col width="120px" />
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
							</td>
							<td>									
								<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
							</td>
							<td>
								<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
								<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div name="divDA" style="padding-left: 64px;">
				<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
				<input type="text" name="LIMIT_DA" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;">
			</div>
		</div>
	</div>
	
	<div id="divInformAdd" style="display:none;">
		<table>
			<colgroup>
				<col width="*" />
				<col width="110px" />
			</colgroup>
			<tbody>
				<tr>
					<td>
						<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
						<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
					</td>
					<td>
						<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'T');"></a>
						<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="divRequestAdd" style="display:none;">
		<table>
			<colgroup>
				<col width="120px" />
				<col width="*" />
				<col width="110px" />
			</colgroup>
			<tbody>
				<tr>
					<td>
						<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
					</td>
					<td>									
						<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
					</td>
					<td>
						<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
						<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="divrepliesAddP" style="display:none;">		
		<div class="wtastas" name="wtastas">
			<div class="wtastas_add mt20" name="none_obj" style="display:none;">
				<div class="wtastas01" <c:if test="${USER_TYPE eq 'OA'}">style="display:none;"</c:if>>
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<h3 style="margin:0px;">시스템 발화 조건</h3>
								</td>
								<td>
									<input class="wtastas_txt01" type="text" id="txtUttr§" name="txtUttr" value="true">
								</td>
								<td>
									<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'F', 'P');">Script Guide</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>	
			
			<div class="wtastas_add">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>	
									<h3 style="margin:0px;">대상슬롯</h3>
								</td>
								<td>
									<input class="wtastas_txt01" type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtSlotName§" name="txtSlotName" />
									<input type="hidden" name="hidSlotSeq" />
								</td>
								<td>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
				
			<div class="wtastas_add">
				<div class="wtastas_cutn" name="none_obj" style="display:none;">
					<h3>시스템 발화</h3> 
					<c:if test="${USER_TYPE ne 'OA'}">
					<a onclick="fnSysIntentAdd(this, 'P');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
					</c:if>
					<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
					<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
				</div>

				<div name="divSysIntent" class="intentBox">
					<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
						<table>
							<colgroup>
								<col width="100px" />
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<h4>시스템 intent</h4>
									</td>
									<td>
										<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_NAME" contenteditable="true">request()</div>
									</td>
									<td>
										<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
						
					<div class="wtastas01 btn_hover">
						<table>
							<colgroup>
								<col width="120px" />
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
									</td>
									<td>									
										<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
									</td>
									<td>
										<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this, 'P');"></a>
										<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div name="divDA" style="padding-left: 64px; display:none;">
						<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
						<input type="text" name="LIMIT_DA" class="wtastas_text_div dp_ib wtastas_margin" style="width: 59%;">
					</div>
				</div>
			</div>
			
			<div class="wtastas_add" name="none_obj" style="display:none;">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>	
									<h3 style="margin:0px;">시스템 발화 후 행동</h3>
								</td>
								<td>
									<input class="wtastas_txt01" type="text" id="txtAction§" name="txtAction">
								</td>
								<td>
									<a class="wtastas_btn" href="#" onclick="fnOpenPop(this, 'A');">Script Guide</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>		
	</div>
	
	<div id="divDetailSubP" style="display:none;">
		<div class="wtastas02 btn_hover" style="padding-left:110px;">
			<table>
				<colgroup>
				   <col width="42px" />
					<col width="*" />
					<col width="110px" />
				</colgroup>
				<tbody>
					<tr>
					    <td class="liImg">
						   
						</td>
						<td>
							<div class="wtastas_text_div dp_ib wtastas_margin" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
						</td>
						<td>
							<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this, 'P');"></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>		
	</div>
	
	<div id="divAddTaskAction" style="display:none;">
		<div name="TaskAction" style="margin-bottom:30px;">
			<dl class="reset_slot">
				<dt>적용시점</dt>					
	               <dd class="anp_contents_dd02 task_select_padding" style="width:75%; padding-right:0; box-sizing:border-box;">
	                   <select class="f_right" id="ddlType§">
	                       <option value="0">사용자 발화 처리 전</option>
	                       <option value="1">사용자 발화 처리 후</option>
	                   </select>
	               </dd>
			</dl>
			<dl class="reset_slot">
			    <dt>조건</dt>
			    <dd style="width:82%; padding-right:0; box-sizing:border-box;">
			        <div>
			            <input type="text" id="txtMonitorCon§" name="txtMonitorCon" style="text-indent:5px; padding:5px 0px; width:65%;" />
			            <a class="class_name_instances" href="#" onclick="fnOpenPop(this, 'E', 'T');" style="padding:8px 7px; float:none;">Script Guide</a>
			        </div>
			    </dd>
			</dl>
			<dl class="reset_slot">
			    <dt>행동</dt>
			    <dd style="width:82%; padding-right:0; box-sizing:border-box;">
			        <div>
			            <input type="text" id="txtMonitorAct§" name="txtMonitorAct" style="text-indent:5px; padding:5px 0px; width:65%;" />
			            <a class="class_name_instances" href="#" onclick="fnOpenPop(this, 'E', 'T');" style="padding:8px 7px; float:none;">Script Guide</a>
			        </div>
			    </dd>
			</dl>
			<div class="taskDeleteBtn"><a href="#" onclick="fnMonitoringDelete(this);"><img src="../images/delete_btn.png" alt="deleteBtn" /></a></div>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>	
</body>
</html>