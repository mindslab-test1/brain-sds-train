<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var iSeqNo = '';
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		
		window.onload = function() {	
			$('#divreplies').on({
				'click focusin': function(e) {
					if($(this).parent().parent().next().length == 0) {
						/* if ($(this).parents().eq(7).attr('id') == 'divrepliesP') {
							AddObjRequest($(this).parent().parent().parent());
						}
						else {
							AddObj($(this).parent().parent().parent());	
						} */
						AddObj($(this).parent().parent().parent());
					}
				},
				focusout: function(e) {
					$this = $(this);
					if ($this.text().toLowerCase().indexOf('request') > -1) {
						if ($this.parent().next().find('[name=OBJECT_DETAIL_NAME]').length > 0) {
							$this.parent().next().empty();
						}
						if ($this.parent().next().find('[name=OBJECT_REQUEST]').length == 0) {
							$this.parent().next().prepend('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
							$this.parent().next().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
						}						
					}
					else {
						if ($this.parent().next().find('[name=OBJECT_REQUEST]').length > 0) {
							$this.parent().next().empty();
							$this.parent().next().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().parent().prev().find('th').find('div').eq(0).focus();
					} else if(e.keyCode == 40) {
						$(this).parent().parent().next().find('th').find('div').eq(0).focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						bWorkChk = true;
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {						
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					if($(this).parent().next().length == 0) {
						$(this).parent().after('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					}
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {						
					var $this = $(this);
					
					if($this.parent().next().length == 0) {
						$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					}
				},
				focusout: function(e) {
					var str = $(this).text();
					var arrvalidation = fnSlotValidation(str, slotdata);					
					if (!arrvalidation[0]) {
						var arrtempval = new Array();
						for (var i = 1; i < arrvalidation.length; i++) {
							arrtempval.push(arrvalidation[i]);
						}
						alert('존재 하지 않는 슬롯입니다.\n(' + arrtempval.join(', ') + ')');
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {						
						//$(this).next().focus();
						$(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
			
			$('#divreplies').on('click', '[name=imgAction]', function(e) {
				$txtAction = $(this).parent().next().find('input');
				$aw_btn = $(this).parent().next().next().find('a');
				$imgActionDel = $(this).parent().next().next().next().find('img');
				if($txtAction.css('display') == 'none') {
					$txtAction.show();
					$aw_btn.show();
					$imgActionDel.show();
				}				
				else {
					$txtAction.hide();
					$aw_btn.hide();
					$imgActionDel.hide();
				}
				fnpreventDefault(e);
			});
			
			$('#divreplies').on('click', '[name=imgActionDel]', function(e) {
				$(this).parent().prev().find('input').val('');
				fnpreventDefault(e);
			});
						
			$('#divreplies').on('click', '.close_btn', function(e) {				
				if($(this).parent().siblings().length > 0)				
					$(this).parent().remove();
				else {
					if($(this).parent().parent().parent().siblings().length > 0)
						$(this).parent().parent().parent().remove();					
				}
				bWorkChk = true;
				fnpreventDefault(e);
			});
			
			if ('${SEQ_NO}' != '') {
				$('#selResetSlot').val('${Agent.RESET_SLOT}');
				$('#selResetSlot').trigger("chosen:updated");
				$('#selTaskType').val('${Agent.TASK_TYPE}');
				$('#selTaskType').trigger("chosen:updated");
				$('#relatedSlot').val('${Agent.RELATED_SLOTS}');
			}
			
			$('[id^=divrepliesT] [name=useruttrw]').each(function() {
				if ($(this).find('.uuw_tab').length == 0) {
					$(this).append($('#divtbAdd').html());
				}	
			});
			
			$('#divrepliesP [name=useruttrwsub]').each(function() {
				if ($(this).find('.uuw_tab').length == 0) {
					$(this).append($('#divtbPAdd').html());
				}	
			});
			
			$('[name=OBJECT_REQUEST]').each(function() {
				if($(this).parent().next().length == 0) {
					$(this).parent().after('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
				}
			});
			
			$('#selResetSlot, #selTaskType, #txtAgentName, #relatedSlot').change(function() {
				bWorkChk = true;
			});
			
			//checkbox
			$('#initTaskChk, #endTaskChk').picker({
				customClass: 'list_check'
			});
			//checkbox_end
			
			if ('${Agent.SORT_NO}' == '0')
				$('#initTaskChk').picker('check');				
			else if ('${Agent.SORT_NO}' == '2')
				$('#endTaskChk').picker('check');
		}
		
		function AddObj(obj) {
			var str = '<tr><th>';
			str += '<div name="OBJECT_NAME" contenteditable="true">inform()</div></th>';
			str += '<td class="slot_modify01">';
			str += '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>';
			str += '</td></tr>';
			obj.append(str);
		}
		
		function AddObjRequest(obj) {
			var str = '<tr><th>';
			str += '<div name="OBJECT_NAME" contenteditable="true">request()</div></th>';
			str += '<td class="slot_modify01">';
			str += '<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true">' + $(obj).parent().parent().parent().parent().find('[name=txtSlotName]').val() + '</div><a class="close_btn f_right" href="#"></a></div>';
			str += '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>';
			str += '</td></tr>';
			obj.append(str);
		}
		
		function fnSave() {
			var bTaskSaveChk = false;
			var focusObj;
			$txtAgentName = $('#txtAgentName');
		
			if ($txtAgentName.val() == 'Intent name') {
				alert('Intent명을 입력 하세요.');
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_-]/; 
			if (reg.test($txtAgentName.val())) {
				alert('태스크명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$txtAgentName.focus();
				fnpreventDefault(event);
				return;
			}
			
			if('${SEQ_NO}' == '') {
				$('#lnb .lnb01 .sub').children().each(function(e) {				
					if ($(this).children('a').text() == $txtAgentName.val()) {
						bTaskSaveChk = true;
						alert('이미 존재하는 Task 입니다.');
						return false;
					}
				});
			}
			
			if (bTaskSaveChk) {
				return false;
			}
						
			reg = /[^a-zA-Z0-9_\-=()."]/; 
			fnLoading();
			var arr = new Object();
			var mainarrobj = new Array();
			var typevalue;
			$('[id^=divrepliesT]').each(function(idx, e) {
				if (idx == 0) 
					typevalue = 1; 
				else 
					typevalue = 2;
				$(this).find('[name=useruttrw]').each(function(idx, e) {							
					var $this = $(this);
					var mainitem = new Object();
					var arrobj = new Array();
					
					mainitem.UTTR = $this.find('[name=txtUttr]').val();
					mainitem.ACT = $this.find('[name=txtAction]').val().replace(/\+/g, "%2B");
					mainitem.INTENTION_TYPE = typevalue;
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';
					
					$this.find('[name=tbOBJ_LIST] tr').each(function(idx, e) {
						var $obj = $(this);
						var item = new Object();
						var arrobjsub = new Array();
						
						item.OBJECT_VALUE = $obj.find('[name=OBJECT_NAME]').text();
						
						if (reg.test(item.OBJECT_VALUE)) {
							bTaskSaveChk = true;
							focusObj = $obj.find('[name=OBJECT_NAME]');
							return false;							
						}
						
						if(!gfn_isNull(item.OBJECT_VALUE)) {
							if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
								$obj.find('[name=OBJECT_REQUEST]').each(function() {
									if(!gfn_isNull($(this).text())) {
										var subitem = new Object();
										subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
										subitem.TYPE = 'R';
										arrobjsub.push(subitem);
									}
								});
								
								$obj.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
									if(!gfn_isNull($(this).text())) {
										var subitem = new Object();
										subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
										subitem.TYPE = 'P';
										arrobjsub.push(subitem);
									}
								});
							}
							
							$obj.find('[name=OBJECT_DETAIL_NAME]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.TYPE = 'T';
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());						
									arrobjsub.push(subitem);
								}
							});						
						}
						
						if (arrobjsub.length > 0) {
							item.OBJECT_DETAIL = arrobjsub;
							
							arrobj.push(item);	
						}						
					});
					mainitem.OBJECT = arrobj;
					mainarrobj.push(mainitem);
				});					
			});
			
			arr.INTENTION_ITEM = mainarrobj;
			
			
			var arrslotitem = new Array();			
			
			$('#divrepliesP [name=useruttrw]').each(function(idx, e) {
				mainarrobj = new Array();
				var mainarrobjitem = new Object();
				var $txtSlotName = $(this).find('[name=txtSlotName]');
				
				if (!gfn_isNull($txtSlotName.val())) {				
					mainarrobjitem.SLOT_SEQ = $txtSlotName.next().val();
					mainarrobjitem.SLOT_NAME = $txtSlotName.val();
					mainarrobjitem.CONDITION = 'ExistValue("' + $txtSlotName.val() + '")==false';
					mainarrobjitem.PROGRESS_SLOT = $txtSlotName.val();
					
					$(this).find('[name=useruttrwsub]').each(function(idx, e) {
						var $this = $(this);
						var mainitem = new Object();
						var arrobj = new Array();
						
						mainitem.UTTR = $this.find('[name=txtUttr]').val();
						mainitem.ACT = $this.find('[name=txtAction]').val().replace(/\+/g, "%2B");
						mainitem.INTENTION_TYPE = 3;
						
						if(mainitem.UTTR.trim() == '')
							mainitem.UTTR = 'true';
						
						$this.find('[name=tbOBJ_LIST] tr').each(function(idx, e) {
							var $obj = $(this);
							var item = new Object();
							var arrobjsub = new Array();
							
							item.OBJECT_VALUE = $obj.find('[name=OBJECT_NAME]').text();
							
							if (reg.test(item.OBJECT_VALUE)) {
								bTaskSaveChk = true;
								focusObj = $obj.find('[name=OBJECT_NAME]');
								return false;
							}
							
							if(!gfn_isNull(item.OBJECT_VALUE)) {
								if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
									$obj.find('[name=OBJECT_REQUEST]').each(function() {
										if(!gfn_isNull($(this).text())) {
											var subitem = new Object();
											subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
											subitem.TYPE = 'R';
											arrobjsub.push(subitem);
										}
									});
									
									$obj.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
										if(!gfn_isNull($(this).text())) {
											var subitem = new Object();
											subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
											subitem.TYPE = 'P';
											arrobjsub.push(subitem);
										}
									});
								}
								
								$obj.find('[name=OBJECT_DETAIL_NAME]').each(function() {
									if(!gfn_isNull($(this).text())) {
										var subitem = new Object();
										subitem.TYPE = 'T';
										subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());						
										arrobjsub.push(subitem);
									}
								});						
							}											
							
							if (arrobjsub.length > 0) {
								item.OBJECT_DETAIL = arrobjsub;
								
								arrobj.push(item);	
							}						
						});
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
					});
					mainarrobjitem.INTENTION_ITEM = mainarrobj;
					arrslotitem.push(mainarrobjitem);
				}
			});
			
			if (bTaskSaveChk) {
				fnLoading();
				alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				focusObj.focus();
				return false;
			}
			
			var SortNo = 1;
			if ($('#initTaskChk').attr('checked') == 'checked')
				SortNo = 0;
			else if ($('#endTaskChk').attr('checked') == 'checked')
				SortNo = 2;
			
			if (${fn:length(submenu)} == 0) {
				SortNo = 0;
			}
			
			arr.SLOT_FILLING_ITEM = arrslotitem;
			var jsoobj = JSON.stringify(arr);
			
			var comAjax = new ComAjax();				
			comAjax.setUrl("<c:url value='/view/insertagent.do' />");
			comAjax.setCallback("fnSaveCallBack");
			comAjax.addParam("AGENT_NAME", $txtAgentName.val());
			//comAjax.addParam("AGENT_SEQ", '${param.AGENT_SEQ}');
			comAjax.addParam("SORT_NO", SortNo);
			comAjax.addParam("RESET_SLOT", $('#selResetSlot').val());
			comAjax.addParam("TASK_TYPE", $('#selTaskType').val());
			comAjax.addParam("RELATED_SLOTS", $('#relatedSlot').val());
			comAjax.addParam("OBJECT_ITEM", jsoobj);			
			
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '')
					comAjax.addParam("SEQ_NO", '');
				else
					comAjax.addParam("SEQ_NO", iSeqNo);
			}
			else
				comAjax.addParam("SEQ_NO", '${SEQ_NO}');
			comAjax.addParam("depth1", '${depth1}');
			comAjax.addParam("depth2", '${depth2}');
			comAjax.addParam("depth3", '${depth3}');
			comAjax.ajax();			
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {
			alert('저장 되었습니다.');
			fnLoading();
			if (iSeqNo == '' && '${SEQ_NO}' == '') {
				iSeqNo = data.SEQ_NO;
			
				var str = '';			
				str = '<li>';
				str += '<a href="#">' + $('#txtAgentName').val() + '<span class="b2_btn"></span></a>';
				str += '<input type="hidden" value="' + iSeqNo + '">';
				str += '<ul class="sub02" style="display:none;">';											
				str += '<li><a href="#">Entities</a></li>';
				str += '<li><a href="#">Intents</a></li>';
				str += '<li><a href="#">Property</a></li>';																
				str += '</ul>';
				str += '</li>';
				$('#menu1').next().append(str);	
			}			
			bWorkChk = false;
		}
		function fnTransactionAdd(type) {
			if (type == 'P') {
				$('#divrepliesP').append($('#divrepliesAddP').html().replace(/§/g, 'P_' + $('#divrepliesP [name=useruttrwsub]').length));	
			}
			else {
				$('#divreplies' + type).append($('#divrepliesAddT').html().replace(/§/g, type + '_' + $('#divreplies' + type + ' [name=useruttrw]').length));
			}
						
			fnpreventDefault(event);
		}

		function fnPsubAdd(obj) {
			$(obj).parent().parent().append($('#divrepliesAddPSub').html().replace(/§/g, 'P_' + $('#divrepliesP [name=useruttrwsub]').length));
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj) {					
			$(obj).parent().remove();
			bWorkChk = true;
			fnpreventDefault(event);
		}
		
		function fnTransactionSave() {			
			alert('저장');
			fnpreventDefault(event);
		}
		
		function fnAddCalcel() {
			$.modal.close();
			//$('#divmodal').hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj, type) {
			if (type == 'F') {
				fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().prev().find('input').attr('id'), "Agentpop", 571, 388, 0, 0);	
			}
			else {
				fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id'), "AgentSearchpop", 430, 550, 0, 0);
			}
		}
		
		function fnChkAll() {
			$('[id^=schk]').picker("update");
		}
		
		function fnformfillingChk(obj) {
			if($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
				$('#divform').hide()	
			}
			else {
				$(obj).picker('check');
				$('#divform').show();				
			}
		}
		
		function fnClassChk(seq, obj) {
			if($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
				$('#divClass' + seq).hide()	
			}
			else {
				$(obj).picker('check');
				$('#divClass' + seq).show();
			}
		}
		
		function fnCheck(obj) {
			if ($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
			}
			else {
				$('#initTaskChk, #endTaskChk').picker("uncheck");			
				$(obj).picker('check');
			}
		}
		
		function fnObjectView(obj, type) {
			if (type == 1) {							
				var $parentobj = $(obj).parent().parent().parent().parent().parent().parent().find('.wtastas_cutn');
				if ($parentobj.css('display') == 'none')
					$parentobj.parent().parent().css('border', '1px solid #c5c5c5').find('[name=none_obj]').show();				
				else
					$parentobj.parent().parent().css('border', 'none').find('[name=none_obj]').hide();
			}
			else {
				var $parentobj = $(obj).parent().parent();
				if ($parentobj.css('display') == 'none')
					$parentobj.parent().css('border', '1px solid #c5c5c5').find('[name=none_obj]').show();				
				else
					$parentobj.parent().css('border', 'none').find('[name=none_obj]').hide();	
			}
							
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="anp">
		<div class="Agent_name_box">
			<div class="Agent_name_box_word">
	            <div class="s_word">
	                <label for="search">	                	
	                	<c:choose>
	                		<c:when test="${null eq Agent}">	                			
	                			<input type="text" id="txtAgentName" name="txtAgentName" value="Task name" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
	                		</c:when>
	                		<c:otherwise>
	                			<c:choose>	                	
	                				<c:when test='${Agent.AGENT_NAME != ""}'>
	                					<input type="text" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
                					</c:when>
                					<c:otherwise>		                						
                						<input type="hidden" id="txtAgentName" name="txtIntentsName" value="${Agent.AGENT_NAME}" />
                					</c:otherwise>
               					</c:choose>
	                		</c:otherwise>
	                	</c:choose>
						
	                    <a href="#" id="saveIntents" onclick="fnSave();" title="저장"><span class="project_save">Save</span></a>
	                </label>
	            </div>
            </div>
        </div>
		<div id="divreplies" class="anp_contents" style="margin-top:25px;">			
			<input type="hidden" id="hidurl" value="<c:url value='/view/openfunctionwrite.do' />" />			
			
			<dl class="reset_slot agent_cbox">
		        <span>                        	
					<input type="checkbox" onclick="fnCheck(this);" id="initTaskChk" name="initTaskChk" value="" style="margin: 8px;"/><label for="initTaskChk" style="vertical-align: text-top; line-height:22px;">Set as the initial task</label>
				</span>
			</dl>
			<dl class="reset_slot agent_cbox">
				<span>                        	
					<input type="checkbox" onclick="fnCheck(this);" id="endTaskChk" name="endTaskChk" value="" style="margin: 8px;"/><label for="endTaskChk" style="vertical-align: text-top; line-height:22px;">Set as the end task</label>
				</span>  
			</dl>
					
			<h3 class="property_icon">Property</h3>
			<dl class="reset_slot">
				<dt>task type</dt>
				<!-- <dd class="anp_contents_dd01">
					<label for="reset_slot01">
						<input type="text" value="" onblur="" onclick="" id="reset_slot01" name="reset_slot01" />
					</label>
				</dd> -->
				<dd class="anp_contents_dd02">
					<select class="f_right u_group" id="selTaskType" name="selTaskType">
						<option value="essential">essential</option>
						<option value="optional">optional</option>
					</select>
				</dd>
			</dl>
			
			<dl class="reset_slot">
				<dt>related slots</dt>
				<dd class="anp_contents_dd02">
					<label for="reset_slot01">
						<input type="text" value="" id="relatedSlot" name="relatedSlot" />
					</label>
				</dd>
			</dl>
			
			<dl class="reset_slot">
				<dt>reset slot</dt>
				<!-- <dd class="anp_contents_dd01">
					<label for="reset_slot01">
						<input type="text" value="" onblur="" onclick="" id="reset_slot01" name="reset_slot01" />
					</label>
				</dd> -->
				<dd class="anp_contents_dd02">
					<select class="f_right u_group" id="selResetSlot" name="selResetSlot">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</dd>
			</dl>					
						
			<div id="divrepliesT1" class="sortable">
				<h3 class="speech_transaction_icon">When the agent starts, the agent says : <a onclick="fnTransactionAdd('T1');" href="#">+ Add</a></h3>
				<c:choose>						
					<c:when test="${fn:length(Intentionlist_1) > 0}">
						<c:forEach items="${Intentionlist_1}" var="row">
							<div class="useruttrw" name="useruttrw">
								<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
								<div class="useruttrw01">
									<table>
										<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
										<tr>								
											<td><input type="text" id="txtEUttrT1_${row.SEQ_NO}" name="txtUttr" value='${row.UTTR}' /></td>
											<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
											<td></td>
										</tr>
									</table>									
									
									<div class="useruttrw02">
										<table>
											<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
											<tr>
												<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
												<td><input type="text" <c:if test="${empty row.ACT}">style="display:none;"</c:if> id="txtEActionT1_${row.SEQ_NO}" name="txtAction" value='${row.ACT}' /></td>
												<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
												<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="uuw_tab">
									<table name="tbOBJ_LIST" class="slot_modify">
										<c:choose>						
											<c:when test="${fn:length(Objectlist_1) > 0}">
												<c:forEach items="${Objectlist_1}" var="orow">
													<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">												
														<tr>
															<th>
																<div name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
															</th>
															<td class="slot_modify01">
																<c:forEach items="${ObjectDtllist_1}" var="drow">
																	<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">													
																		<c:choose>						
																			<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																				<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:when>
																			<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																				<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:when>
																			<c:otherwise>
																				<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:otherwise>
																		</c:choose>			
																	</c:if>											
																</c:forEach>																																							
															</td>
														</tr>													
													</c:if>
												</c:forEach>											
											</c:when>
											<c:otherwise>												
												<tr>
													<th>
														<div name="OBJECT_NAME" contenteditable="true">inform()</div>
													</th>
													<td class="slot_modify01">																			
														<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
													</td>
												</tr>
											</c:otherwise>
										</c:choose>	
									</table>
								</div>
							</div>
						</c:forEach>							
					</c:when>
					<%-- <c:otherwise>
						<div class="useruttrw" name="useruttrw">
							<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
							<div class="useruttrw01">								
								<input type="text" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtUttrT1_0" name="txtUttr" />
								
								<div class="useruttrw02">					
									<table>
										<colgroup><col width="27px" /><col width="*" /></colgroup>
										<tr>
											<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
											<td><input type="text" style="display:none;" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtActionT1_0" name="txtAction" /></td>
										</tr>
									</table>									
								</div>
							</div>
							<div class="uuw_tab">
								<table name="tbOBJ_LIST" class="slot_modify">
									<tr>
										<th>
											<div name="OBJECT_NAME" contenteditable="true">inform()</div>
										</th>
										<td class="slot_modify01">																			
											<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
										</td>
									</tr>
								</table>
							</div>
						</div>
					</c:otherwise> --%>
				</c:choose>
			</div>
			
			<div id="divrepliesT2" class="sortable">
				<h3 class="speech_transaction_icon">When the agent restarts, the agent says : <a onclick="fnTransactionAdd('T2');" href="#">+ Add</a></h3>
				<c:choose>						
					<c:when test="${fn:length(Intentionlist_2) > 0}">
						<c:forEach items="${Intentionlist_2}" var="row">
							<div class="useruttrw" name="useruttrw">
								<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
								<div class="useruttrw01">								
									<table>
										<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
										<tr>								
											<td><input type="text" id="txtEUttrT2_${row.SEQ_NO}" name="txtUttr" value='${row.UTTR}' /></td>
											<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
											<td></td>
										</tr>
									</table>									
									
									<div class="useruttrw02">
										<table>
											<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
											<tr>
												<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
												<td><input type="text" <c:if test="${empty row.ACT}">style="display:none;"</c:if> id="txtEActionT2_${row.SEQ_NO}" name="txtAction" value='${row.ACT}' /></td>
												<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
												<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="uuw_tab">
									<table name="tbOBJ_LIST" class="slot_modify">
										<c:choose>						
											<c:when test="${fn:length(Objectlist_2) > 0}">
												<c:forEach items="${Objectlist_2}" var="orow">
													<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">												
														<tr>
															<th>
																<div name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
															</th>
															<td class="slot_modify01">
																<c:forEach items="${ObjectDtllist_2}" var="drow">
																	<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">													
																		<c:choose>						
																			<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																				<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:when>
																			<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																				<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:when>
																			<c:otherwise>
																				<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																			</c:otherwise>
																		</c:choose>			
																	</c:if>											
																</c:forEach>																																							
															</td>
														</tr>
													</c:if>
												</c:forEach>											
											</c:when>
											<c:otherwise>												
												<tr>
													<th>
														<div name="OBJECT_NAME" contenteditable="true">inform()</div>
													</th>
													<td class="slot_modify01">																			
														<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
													</td>
												</tr>
											</c:otherwise>
										</c:choose>	
									</table>
								</div>
							</div>
						</c:forEach>							
					</c:when>
					<%-- <c:otherwise>
						<div class="useruttrw" name="useruttrw">
							<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
							<div class="useruttrw01">								
								<input type="text" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtUttrT2_0" name="txtUttr" />
								
								<div class="useruttrw02">
									<table>
										<colgroup><col width="27px" /><col width="*" /></colgroup>
										<tr>
											<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
											<td><input type="text" style="display:none;" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtActionT2_0" name="txtAction" /></td>
										</tr>
									</table>
								</div>
							</div>
							<div class="uuw_tab">
								<table name="tbOBJ_LIST" class="slot_modify">
									<tr>
										<th>
											<div name="OBJECT_NAME" contenteditable="true">inform()</div>
										</th>
										<td class="slot_modify01">																			
											<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
										</td>
									</tr>
								</table>
							</div>
						</div>
					</c:otherwise> --%>
				</c:choose>								
			</div>
			
			<div id="divrepliesP" class="sortable">
				<h3 class="speech_transaction_icon">The Agent gathers information from user by filling slots : <a onclick="fnTransactionAdd('P');" href="#">+ Add</a></h3>
				
				<c:choose>						
					<c:when test="${fn:length(SlotFillinglist) > 0}">
						<c:forEach items="${SlotFillinglist}" var="srow">
							<div class="useruttrw" name="useruttrw">
								<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
								<div class="useruttrw_add">
									<input class="useruttrw_s1" type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtESlotName_${srow.SEQ_NO}" name="txtSlotName" value="${srow.SLOT_NAME}" />
									<input type="hidden" name="hidSlotSeq" value="${srow.SLOT_SEQ}" />
									<a onclick="fnPsubAdd(this);" href="#">+ Add</a>
								</div>
								<c:choose>						
									<c:when test="${fn:length(Intentionlist_3) > 0}">
										<c:forEach items="${Intentionlist_3}" var="row">
											<c:if test="${srow.SEQ_NO eq row.FILLING_SEQ}">											
												<div class="useruttrwsub" name="useruttrwsub">
													<div class="useruttrw01">
														<table>
															<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
															<tr>								
																<td><input type="text" id="txtEUttrT3_${row.SEQ_NO}" name="txtUttr" value='${row.UTTR}' /></td>
																<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
																<td></td>
															</tr>
														</table>														
														
														<div class="useruttrw02">
															<table>
																<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
																<tr>
																	<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
																	<td><input type="text" <c:if test="${empty row.ACT}">style="display:none;"</c:if> id="txtEActionT3_${row.SEQ_NO}" name="txtAction" value='${row.ACT}' /></td>
																	<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
																	<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
																</tr>
															</table>
														</div>
													</div>
													<div class="uuw_tab">
														<table name="tbOBJ_LIST" class="slot_modify">
															<c:choose>						
																<c:when test="${fn:length(Objectlist_3) > 0}">
																	<c:forEach items="${Objectlist_3}" var="orow">
																		<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">																	
																			<tr>
																				<th>
																					<div name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																				</th>
																				<td class="slot_modify01">
																					<c:forEach items="${ObjectDtllist_3}" var="drow">
																						<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">													
																							<c:choose>						
																								<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																									<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																								</c:when>
																								<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																									<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																								</c:when>
																								<c:otherwise>
																									<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																								</c:otherwise>
																							</c:choose>			
																						</c:if>											
																					</c:forEach>																																							
																				</td>
																			</tr>																		
																		</c:if>
																	</c:forEach>											
																</c:when>
																<c:otherwise>
																	<tr>
																		<th>
																			<div name="OBJECT_NAME" contenteditable="true">request()</div>
																		</th>
																		<td class="slot_modify01">									
																			<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
																			<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>																											
																		</td>
																	</tr>
																</c:otherwise>
															</c:choose>
														</table>
													</div>
												</div>
											</c:if>
										</c:forEach>							
									</c:when>
									<c:otherwise>
										<div class="useruttrwsub" name="useruttrwsub">
											<div class="useruttrw01">								
												<table>
													<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
													<tr>								
														<td><input type="text" id="txtUttrP_0" name="txtUttr" /></td>
														<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
														<td></td>
													</tr>
												</table>
												
												
												<div class="useruttrw02">
													<table>
														<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
														<tr>
															<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
															<td><input type="text" style="display:none;" id="txtActionP_0" name="txtAction" /></td>
															<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
															<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
														</tr>
													</table>
												</div>
											</div>
											<div class="uuw_tab">
												<table name="tbOBJ_LIST" class="slot_modify">
													<tr>
														<th>
															<div name="OBJECT_NAME" contenteditable="true">request()</div>
														</th>
														<td class="slot_modify01">																			
															<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
															<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
														</td>
													</tr>
												</table>
											</div>
										</div>	
									</c:otherwise>
								</c:choose>
								
							</div>	
						</c:forEach>
					</c:when>
					<%-- <c:otherwise>	
						<div class="useruttrw" name="useruttrw">
							<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
							<div class="useruttrw_add">
								<input class="useruttrw_s1" type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtSlotName_0" name="txtSlotName" />
								<input type="hidden" name="hidSlotSeq" />
								<a onclick="fnPsubAdd(this);" href="#">+ Add</a>
							</div>
							<div class="useruttrwsub" name="useruttrwsub">
								<div class="useruttrw01">								
									<input type="text" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtUttrP_0" name="txtUttr" />
									
									<div class="useruttrw02">
										<table>
											<colgroup><col width="27px" /><col width="*" /></colgroup>
											<tr>
												<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
												<td><input type="text" style="display:none;" readonly="readonly" onclick="fnOpenPop(this, 'F');" id="txtActionP_0" name="txtAction" /></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="uuw_tab">
									<table name="tbOBJ_LIST" class="slot_modify">
										<tr>
											<th>
												<div name="OBJECT_NAME" contenteditable="true">request()</div>
											</th>
											<td class="slot_modify01">																			
												<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
												<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
											</td>
										</tr>
									</table>
								</div>
							</div>	
						</div>
					</c:otherwise> --%>
				</c:choose>										
			</div>
		</div>
	</div>
	
	<div id="divrepliesAddT" style="display:none;">		
		<div class="useruttrw" name="useruttrw">
			<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>			
			<div class="useruttrw01">
				<table>
					<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
					<tr>								
						<td><input type="text" id="txtUttr§" name="txtUttr" value="true" /></td>
						<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
						<td></td>
					</tr>
				</table>
				<div class="useruttrw02">
					<table>
						<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
						<tr>
							<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
							<td><input type="text" style="display:none;" id="txtAction§" name="txtAction" /></td>
							<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
							<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="uuw_tab">
				<table name="tbOBJ_LIST" class="slot_modify">
					<tr>
						<th>
							<div name="OBJECT_NAME" contenteditable="true">inform()</div>
						</th>
						<td class="slot_modify01">																			
							<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>	
	
	<div id="divrepliesAddP" style="display:none;">
		<div class="useruttrw" name="useruttrw">
			<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
			<div class="useruttrw_add">
				<input class="useruttrw_s1" type="text" readonly="readonly" onclick="fnOpenPop(this, 'P');" id="txtSlotName§" name="txtSlotName" />
				<input type="hidden" name="hidSlotSeq" />
				<a onclick="fnPsubAdd(this);" href="#">+ Add</a>
			</div>			
			<div class="useruttrwsub" name="useruttrwsub">
				<div class="useruttrw01">
					<table>
						<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
						<tr>								
							<td><input type="text" id="txtUttr§" name="txtUttr" value="true" /></td>
							<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
							<td></td>
						</tr>
					</table>
					
					<div class="useruttrw02">
						<table>
							<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
							<tr>
								<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
								<td><input type="text" style="display:none;" id="txtAction§" name="txtAction" /></td>
								<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
								<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="uuw_tab">
					<table name="tbOBJ_LIST" class="slot_modify">
						<tr>
							<th>
								<div name="OBJECT_NAME" contenteditable="true">request()</div>
							</th>
							<td class="slot_modify01">																			
								<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
								<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>								
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="divrepliesAddPSub" style="display:none;">
		<div class="useruttrwsub" name="useruttrwsub">
			<div class="useruttrw01">								
				<table>
					<colgroup><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
					<tr>								
						<td><input type="text" id="txtUttr§" name="txtUttr" /></td>
						<td><a class="aw_btn" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
						<td></td>
					</tr>
				</table>
				
				<div class="useruttrw02">
					<table>
						<colgroup><col width="27px" /><col width="*" /><col width="120px" /><col width="27px" /></colgroup>
						<tr>
							<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
							<td><input type="text" style="display:none;" id="txtAction§" name="txtAction" /></td>
							<td><a class="aw_btn" style="display:none;" onclick="fnOpenPop(this, 'F');" href="#">Script Guide</a></td>
							<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="uuw_tab">
				<table name="tbOBJ_LIST" class="slot_modify">
					<tr>
						<th>
							<div name="OBJECT_NAME" contenteditable="true">request()</div>
						</th>
						<td class="slot_modify01">																			
							<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
							<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	<div id="divtbAdd" style="display:none;">
		<div class="uuw_tab">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tr>
					<th>
						<div name="OBJECT_NAME" contenteditable="true">inform()</div>
					</th>
					<td class="slot_modify01">																			
						<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
					</td>
				</tr>
			</table>
		</div>
	</div>
	
	<div id="divtbPAdd" style="display:none;">
		<div class="uuw_tab">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tr>
					<th>
						<div name="OBJECT_NAME" contenteditable="true">request()</div>
					</th>
					<td class="slot_modify01">
						<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
						<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>							
					</td>
				</tr>
			</table>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>	
</body>
</html>