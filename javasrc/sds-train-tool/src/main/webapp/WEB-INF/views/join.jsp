<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js?ver=9'/>"></script>
	<script type="text/javascript">
		$(function () {									
			var reg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&\-_+=])[A-Za-z\d$@$!%*#?&\-_+=]{8,16}$/;
			
			$('#txtPwd1').bind({
				keyup : function() {
					var text = $(this).val();
					if (reg.test(text)) {
						$('#spPw1').attr('class', 'cu_ok').text('사용 가능합니다.');
					}				
					else {
						$('#spPw1').attr('class', 'cu_no').text('비밀번호는 8~16자리 영문, 특수문자, 숫자를 조합하여 작성해주세요.');
						return false;
					}				
				}
			});
			
			$('#txtPwd2').keyup(function() {
				if ($(this).val() == $('#txtPwd1').val()) {
					$('#spPw2').attr('class', 'cu_ok').text('비밀번호가 일치 합니다.');					
				}				
				else {
					$('#spPw2').attr('class', 'cu_no').text('비밀번호가 일치하지 않습니다.');
				}				
			});
			
			$('#btnApi').click(function() {
				fnApiConfirm();
			});
						
			if ('${param.TYPE}' == 'I') {
				$('#sptitle').text('아이디 찾기');
				$('#divId, #divPw1, #divPw2, #spPw1, #spPw2, #spPolicy, #divPolicy, #divChk').hide();				
				$('#divBg').css('height', '210px');
				$('#btnReg').text('로그인 하기').click(function() {
					var comSubmit = new ComSubmit();
					comSubmit.setUrl("<c:url value='/view/login.go' />");					
					comSubmit.submit();
				});
			}
			else if ('${param.TYPE}' == 'P') {
				$('#sptitle').text('비밀번호 찾기');
				$('#divPw1, #divPw2, #spPw1, #spPw2, #spPolicy, #divPolicy, #divChk').hide();				
				$('#divBg').css('height', '280px');			
				$('#btnReg').text('비밀번호 변경 하기').click(function() {
					if ($('#spApi').text() != '정상적으로 확인 되었습니다.') {
						alert('API Key를 확인해주세요.');
						return;
					}
					
					if ($('#spPw1').text() == '사용 가능합니다.' && $('#spPw2').text() == '비밀번호가 일치 합니다.') {
						var comAjax = new ComAjax();
						comAjax.setUrl("<c:url value='/view/updateuserpassword.go' />");
						comAjax.setCallback("fnUpdatePasswordCallBack");						
						comAjax.addParam("USER_ID", $("#txtid").val());
						comAjax.addParam("USER_PWD", $("#txtPwd1").val());
						
						comAjax.ajax();
					}
					else {
						alert('비밀번호를 바르게 입력해주세요.');
					}					
				});
			}
			else {				
				$('#sptitle').text('사용자 등록');
				$('#btnReg').text('등록').click(function() {
					if ($('#chkPolicy').is(":checked") == false) {
						alert('서비스 약관에 동의해 주세요.');
						return false;
					}
					
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/userinsert.go' />");
					comAjax.setCallback("fnSaveCallBack");
					comAjax.addParam("SEQ_NO", '0');
					comAjax.addParam("USER_ID", $("#txtid").val());
					comAjax.addParam("USER_PWD", $("#txtPwd1").val());
					comAjax.addParam("USER_NAME", $("#txtid").val());
					comAjax.addParam("USER_TYPE", "OA");
					comAjax.addParam("REMARKS", "Open API 사용자");
					comAjax.addParam("API_KEY", $("#txtKey").val().trim());
					comAjax.ajax();
					fnpreventDefault(event);
				});				
				$('#divBg').css('height', '580px');
				$('#txtid').prop('readonly', true);
				$('#spApi').html('ETRI Open API Key가 없으시면, <a href="#" onclick="fnEtriApi();"><span style="font-size:12px; text-decoration:underline;">여기</span></a>에서 먼저 신청하시기 바랍니다.');
			}
		});		
		
		function fnSaveCallBack(data) {
			if (data.STATUS == 'S') {
				alert('정상적으로 등록 되었습니다. 로그인 후 이용해 주세요.')
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/login.go' />");				
				comSubmit.submit();
			}
			else if (data.STATUS == 'UI') {
				alert('이미 존재하는 ID 입니다.');
			}
			else if (data.STATUS == 'UA') {
				alert('이미 존재하는 API Key 입니다.');
			}
			else
				alert('작업 중 오류가 발생하였습니다.\n관리자에게 문의하여 주십시오.');			
		}
		
		function fnUpdatePasswordCallBack(data) {
			if (data.STATUS == 'S') {
				alert('정상적으로 변경 되었습니다. 로그인 후 이용해 주세요.')
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/login.go' />");				
				comSubmit.submit();
			}
			else
				alert('작업 중 오류가 발생하였습니다.\n관리자에게 문의하여 주십시오.');
		}
		
		function fnFindId() {
			if($("#txtid").val() != '' && $("#txtPwd").val() != '') {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/logincheck.go' />");
				comAjax.setCallback("fnLoginCallBack");
				comAjax.addParam("USER_ID",$("#txtid").val());
				comAjax.addParam("USER_PWD", $("#txtPwd").val());				
				comAjax.ajax();
				fnpreventDefault(event);							
			} else {
				fnAlert();
				fnpreventDefault(event);
			}				
		}
		
		function fnLoginCallBack(data) {
			if (data.STATUS == 'OK') {
				if ($('#chkidsave').is(":checked") == true)
					setCookie("UserId", $("#txtid").val(), 30);
				else 
					deleteCookie("UserId");
				
				setCookie("USER_TYPE", data.userinfo.USER_TYPE);
				setCookie("USER_SEQ", data.userinfo.SEQ_NO);
				setCookie("USER_ID", data.userinfo.USER_ID);
				setCookie("USER_NAME", data.userinfo.USER_NAME);
				setCookie("UNIQID", uniqid());
				
				fnMain();
			}
			else
				fnAlert();
		} 
		
		function fnApiConfirm() {
			if($('#txtKey').val().trim() == '') {
				alert('API Key를 입력해주세요.');
				$('#txtKey').focus();
				return false;
			}
			
			if ('${param.TYPE}' == 'P' && $('#txtid').val().trim() == '') {								
				alert('ID를 입력해주세요.');
				$('#txtid').focus();
				return false;			
			}
							
			var obj = new Object();
			var objarg = new Object();
			objarg.api_id = 'conversation';
			objarg.comment = '대화처리 ACCESS KEY 인증';
			objarg.accessKey = $('#txtKey').val().trim();
			obj.request_id = 'reserved field';
			obj.argument = objarg;
			
			
			obj.argument = 
			$.ajax({
			    url: 'http://aiopen.etri.re.kr:8000/accessKeyCheck',
			    async: true,			    
			    type: 'POST',
			    data: JSON.stringify(obj),
			    dataType: 'json',		    
			    success: function(data) {
			    	fnApiConfirmCallBack(data);
				}, 
			    error: function(e) {alert('API 인증 중 오류가 발생 하였습니다.');}
			});
		}
		
		function fnApiConfirmCallBack(data) {			
			if (data.result == '0') {
				$('#txtKey').prop('readonly', true);
				if ('${param.TYPE}' == 'I') {
					$('#spApi').html('귀하의 ID는 <b>' + data.return_object.email + '</b> 입니다.').attr('class', 'cu_ok');
					$('#divBg').css('height', '230px')
				}
				else {
					$('#spApi').text('정상적으로 확인 되었습니다.').attr('class', 'cu_ok');
					$('#txtid').show().val(data.return_object.email);
					$('#divId, #spId, #divPw1, #divPw2, #spPw1, #spPw2').show();					
					$('#txtid').prop('readonly', true);

					if ('${param.TYPE}' == 'P') {
						$('#divBg').css('height', '470px');
						$('#divPw1').css('margin-top', '0px');
						$('#spId').show().text('새로운 비밀번호를 입력 하세요.');						
					}
					else {
						$('#divBg').css('height', '620px');
						$('#btnApi').text('다시입력').unbind('click').click(function() {
							$('#txtKey').prop('readonly', false).val('');
							$('#txtid').val('');
							$('#spApi').text('');
							$('#btnApi').text('확인').unbind('click').click(function() {
								fnApiConfirm();
							});
							fnpreventDefault(event);
						});
					}
				}	
			}						
			else {
				$('#spApi').text('올바르지 않은 API Key 입니다. (메세지 : ' + data.reason + ')').attr('class', 'cu_no');
			}						
		}
		
		function fnEtriApi() {
			window.location = 'http://aiopen.etri.re.kr/key_main.php';
		}
	</script>
</head>
<body>
	<div id="wrap" class="login">
		<div id="divBg">
			<div class="logo"><a href="#" onclick="fnMain();">GENIE<span>DIALOG</span></a> <span id="sptitle" class="h1_join">사용자 등록</span></div>
			<div class="input id"><input type="text" id="txtKey" name="txtKey" class="tf_login tf_key" maxlength="50" tabindex="2" placeholder="ETRI Open API Key " /><span id="btnApi" class="key_btn">확인</span></div>
			<span id="spApi" class="cu_ok"></span>
			<div id="divId" class="input id"><input type="text" id="txtid" name="txtid" class="tf_login" maxlength="50" tabindex="2" placeholder="ID" /></div>
			<span id="spId" class="cu_ok" style="display:none; padding:13px 0px 5px 21px !important;"></span>			
			<div id="divPw1" class="input pw"><input type="password" id="txtPwd1" name="txtPwd1" class="tf_login tf_pw" maxlength="16" tabindex="3" placeholder="password" /></div>
			<span id="spPw1" class="cu_no">비밀번호는 8~16자리 영문, 특수문자, 숫자를 조합하여 작성해주세요.</span>
			<div id="divPw2" class="input pw"><input type="password" id="txtPwd2" name="txtPwd1" class="tf_login tf_pw" maxlength="16" tabindex="3" placeholder="password 확인" /></div>
			<span id="spPw2" class="cu_no"></span>
			<span id="spPolicy" class="cu_ok" style="padding:12px 0px 5px 21px !important;">※ 서비스 약관</span>
			<div id="divPolicy" class="input po" style="margin-top:5px;">
				<div class="PolicyContent">
					<p>본 약관은 ETRI(한국전자통신연구원)가 제공하는 GenieDialog 서비스를 이용함에 있어 ETRI와 사용자의 권리, 의무 및 책임 사항을 규정함을 목적으로 합니다.</p>
					<ul class="dash">
						<li>본 서비스를 통해 구축한 도메인은 본 서비스 및 ETRI 인공지능 Open API 서비스에만 사용이 제한됩니다.</li>
						<li>본 서비스를 통해 구축한 도메인의 사용 및 관리에 관한 권리 및 책임은 도메인을 소유한 사용자에게 있습니다.</li>
						<li>본 서비스를 통해 구축한 도메인 및 사용 내역은 ETRI가 기술 연구 목적으로 활용할 수 있습니다.</li>
						<li>사용자는 서비스 이용을 위한 ID, 비밀번호의 관리에 대한 책임, 본인 ID의 제3자에 의한 부정사용 등의 고의 및 과실로 인해 발생하는 모든 불이익에 대한 책임을 부담합니다.</li>
						<li>본 서비스 내용이 변경하거나 종료되는 경우, 사용자 등록한 전자우편 주소로 이메일을 통하여 서비스 내용의 변경 사항 또는 종료를 통지할 수 있습니다.</li>
						<li>본 서비스는 ETRI의 정책에 따라 종료될 수 있습니다.</li>
					</ul>
				</div>
				
			</div>
			<div id="divChk" class="remember" style="height:22px; width:235px; margin:0px 0px 10px; ">
				<div><input type="checkbox" id="chkPolicy"/><label for="chkPolicy" style="margin-top:1px; font-size:12px;"></label></div>
				<div><label for="reId" style="font-size:12px; cursor:auto;">상기 약관에 동의합니다.</label></div>
			</div>	
			<button type="button" id="btnReg">등록</button>
			<!-- <div class="find_info">
				<a>아이디찾기</a>
				<span>|</span>
				<a>비밀번호찾기</a>
				<span>|</span>
				<a>회원가입</a>
			</div> -->
		</div>
	</div>

	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>
