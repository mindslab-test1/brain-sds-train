<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			$('#txtSearch').keydown(function(e) {
				if (e.keyCode == 13) {
					fnSelectList();	
				}				
			});
			fnSetNaviTitle('Users');
			fnSelectList(1);
		}
		
		function fnSelectList(pageNo){
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectUserList.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			if ($('#txtSearch').val().trim() != '') {
				comAjax.addParam("WHERE", " WHERE USER_ID like '%" + $('#txtSearch').val() + "%' or  USER_NAME like '%" + $('#txtSearch').val() + "%' ");
			}
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbusers > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				var iRowColor = true;
				var str = '';
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
											
					str += '<td><a href="#" onclick="fnWrite('+value.SEQ_NO+');" >'+value.USER_ID+'</a></td>';						
					str += '<td>'+value.USER_NAME+'</td>';
					str += '<td>'+value.CREATE_DATE+'</td>';
					str += '<td>'+value.LAST_LOGIN_TIME+'</td>';
					str += '<td>'+value.DOMAIN_COUNT+'<span class="remove"onclick="fnDelete('+value.SEQ_NO+');"></span></td>';
					str +='</tr>'; 
				});
			
				body.append(str);
			}
		}
			
		function fnWrite(seqno) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/userwrite.do' />");
			comSubmit.addParam("SEQ_NO", seqno);			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDelete(seqno) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/userdelete.do' />");
				comSubmit.addParam("SEQ_NO", seqno);			
				comSubmit.submit();			
				fnpreventDefault(event);	
			}
		}
		
		function fnOpenLua() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/lualist.do' />");					
			comSubmit.submit();			
			fnpreventDefault(event);	
		}
		
		function fnOpenLanguage() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/languagewrite.do' />");					
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnOpenEncoding() {
			fnWinPop("<c:url value='/view/setencoding.do' />", "EncodingPop", 345, 216, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnOpenCountList() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/selectcountmng.do' />");					
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDownLoadUser() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getuesersexcel.do' />");			
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW users">
			<div class="btnBox">
				<div onclick="fnOpenCountList();" class="setEncoding">데이터 생성 관리</div>
				<div onclick="fnOpenEncoding();" class="setEncoding">Set Encoding</div>
				<div onclick="fnOpenLua();" class="editLua">Edit Lua</div>
				<div onclick="fnDownLoadUser();" class="createUser">Download User</div>
				<div onclick="fnWrite(0);" class="createUser">Create User</div>				
			</div>			
			<div class="searchBox searchBox_flow" style="width:100%;">
				<div class="box"><input type="text" id="txtSearch" placeholder="search user..."><button type="button" onclick="fnSelectList('');"><img src="../images/icon_searchW.png" alt="search"></button></div>
			</div>
			<div class="liBox table">				
				<table id="tbusers" class="table">
                   <caption class="hide" style="display:none;">USER</caption>
                   <colgroup>
                       <col width="">
                       <col width="20%">
                       <col width="20%">
                       <col width="20%">
                       <col width="15%">
                   </colgroup>
                   <thead>
					<tr>
						<th>ID</th>
						<th>이름</th>
						<th>등록일자</th>
						<th>최근 로그인</th>
						<th>도메인 수</th>
					</tr>
                   </thead>
                   <tbody>
                   </tbody>
               </table>
               
               <!--board paging start-->
				<div id="PAGE_NAVI" class="board_paging">								
				</div>
				<!--board paging end-->
				<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			</div>			
		</div>
	</div>	
</body>
</html>