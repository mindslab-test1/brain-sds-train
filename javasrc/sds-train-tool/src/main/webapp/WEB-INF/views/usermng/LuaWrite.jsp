<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			if ('${SEQ_NO}' == '')
				fnSetNaviTitle('Create Lua');	
			else
				fnSetNaviTitle('Lua > ${luainfo.FUNCTION_NAME}');	
			
			$('#txtParamCount').keyup(function () {
	            $(this).val($(this).val().replace(/[^0-9]/gi, ""));
	        });
		}
		
		function fnSave() {
			$txtFuntionName = $('#txtFuntionName');
			$txtDescription = $('#txtDescription');
			$txtParamCount = $('#txtParamCount');
			$txtRemarks = $('#txtRemarks');
			
			if ($txtFuntionName.val().trim() == '') {
				alert('함수명을 입력하세요.');
				$txtFuntionName.focus();
				return;
			}
			
			if ($txtDescription.val().trim() == '') {
				alert('인자 값을 입력하세요.');
				$txtDescription.focus();
				return;
			}
			
			if ($txtParamCount.val().trim() == '') {
				alert('인자 수를 입력하세요.');
				$txtParamCount.focus();
				return;
			}					
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/luainsert.do' />");
			comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("FUNCTION_NAME", $txtFuntionName.val());
			comSubmit.addParam("DESCRIPTION", $txtDescription.val());
			comSubmit.addParam("PARAM_COUNT", $txtParamCount.val());
			comSubmit.addParam("REMARKS", $txtRemarks.val());
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW users usersDetail">
			<div class="btnBox">
				<div class="save" onclick="fnSave();">Save</div>
			</div>
			<div class="liBox table">
				<table class="table">
					<caption class="hide" style="display:none;">USER</caption>
						<colgroup>
							<col width="30%">
							<col width="70%">
						</colgroup>
					<tbody>
						<tr>	
							<th>함수명</th>
							<td><input type="text" id="txtFuntionName" name="FUNCTION_NAME" value="${luainfo.FUNCTION_NAME}" placeholder="함수명을 입력해주세요." /></td>
						</tr>
						<tr>
							<th>인자값</th>
							<td><input type="text" id="txtDescription" name="DESCRIPTION" value='${luainfo.DESCRIPTION}' placeholder="인자값을 입력해주세요." /></td>
						</tr>
						<tr>
							<th>인자수</th>
							<td><input type="text" id="txtParamCount" name="PARAM_COUNT" value="${luainfo.PARAM_COUNT}" placeholder="인자수를 입력해주세요." /></td>
						</tr>
						<tr>
							<th>설명</th>
							<td>
								<div class="registration_area">
									<textarea id="txtRemarks" name="REMARKS">${luainfo.REMARKS}</textarea>
								</div>
							</td>
						</tr>						
					</tbody>
              </table>
			</div>
		</div>
	</div>
</body>
</html>