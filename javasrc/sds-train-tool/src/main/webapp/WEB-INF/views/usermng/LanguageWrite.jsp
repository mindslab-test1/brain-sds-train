<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();
			
			$('#tLang tbody tr').each(function(idx, e) {
				var item = new Object();
				
				$tr = $(this);
				
				if ($tr.find('[name=SEQ_NO]').length != 0) {
					item.SEQ_NO = $tr.find('[name=SEQ_NO]').val();
					item.LANG_NAME = $tr.find('td').eq(0).text();
				}
				else {
					item.SEQ_NO = 0;
					item.LANG_NAME = $tr.find('[name=LANG_NAME]').val();
				}
				
				
				item.ENGINE_NAME = $tr.find('[name=ENGINE_NAME]').val();
				item.DIC_NAME = $tr.find('[name=DIC_NAME]').val();
				
				if (item.ENGINE_NAME.trim() != '' || item.DIC_NAME.trim() != '') {
					arrobj.push(item);	
				}
			});
			
			arr.LANGUAGE_ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertlanguage.do' />");			
			comSubmit.addParam("LANGUAGE_ITEM", jsoobj);			
			comSubmit.submit(); 
		}
		
		function fnLangAdd() {
			var str = '';
		
			str += '<tr><td class="admin_user_list_bg02"><input type="text" name="LANG_NAME" value="" /></td>';
			str += '<td class="admin_user_list_bg01"><input type="text" name="ENGINE_NAME" value="" /></td>';
			str += '<td class="admin_user_list_bg01"><input type="text" name="DIC_NAME" value="" /></td>';
			str += '<td class="admin_user_list_bg01"><a href="#" onclick="fnDelete(0, this);" class="delete_btn"><span class="hide">삭제</span></a></td></tr>';
			$('#tLang tbody').append(str);
		}
		
		function fnDelete(seqno, obj) {
			if (seqno != 0) {
				if(confirm('해당열을 삭제 하시겠습니까?')) {
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deletelanguage.do' />");
					comAjax.setCallback("fnDeleteCallBack");
					comAjax.addParam("SEQ_NO", seqno);
					comAjax.ajax();							
				}
			}
			$(obj).parent().parent().remove();
		}
		
		function fnDeleteCallBack(data) {
			if(data.status != 'OK')
				alert('저장 중 오류가 발생했습니다.');
		}		
		
		function fnBack() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/userlist.do' />");			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div id="contents">
		<div class="admin_user">
			<div class="cont_title">
				<h2>Users</h2>				
				<a href="#" onclick="fnSave();">save</a>
				<a href="#" onclick="fnBack();" style="background:#ff772e;">Back</a>
			</div>
			<div class="admin_user_con">
				<div class="admin_user_title">
					<table id="tLang" class="admin_user_list language_list" summary="표시">
						<caption class="hide">정보 표</caption>
						<colgroup>
							<col width="*" />
							<col width="40%" />
							<col width="40%" />
							<col width="5%" />
						</colgroup>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>엔진</th>
								<th colspan="2">사전</th>
							</tr>
						</thead>	
						<tbody>	
							<c:forEach items="${LANG_LIST }" var="row">
								<tr>
									<td class="admin_user_list_bg02">${row.LANG_NAME}<input type="hidden" name="SEQ_NO" value="${row.SEQ_NO}" /></td>
									<td class="admin_user_list_bg01"><input type="text" name="ENGINE_NAME" value="${row.ENGINE_NAME}" /></td>
									<td class="admin_user_list_bg01"><input type="text" name="DIC_NAME" value="${row.DIC_NAME}" /></td>
									<td class="admin_user_list_bg01"><a href="#" onclick="fnDelete(${row.SEQ_NO}, this);" class="delete_btn"><span class="hide">삭제</span></a></td>
								</tr>
							</c:forEach>	
						</tbody>
					</table>
				</div>
				<div class="layer_box_btn" style="margin:10px 42px 0px 0px; text-align:right;">
					<!-- <a href="#" onclick="fnApply();">적용</a> -->
					<a href="#" onclick="fnLangAdd();">추가하기</a>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>