<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			fnSetNaviTitle('Edit Lua');
			fnSelectList(1);
		}
		
		function fnSelectList(pageNo){
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectlualist.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbusers > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				var iRowColor = true;
				var str = '';
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
											
					str += '<td><a href="#" onclick="fnWrite('+value.SEQ_NO+');" >'+value.FUNCTION_NAME+'</a></td>';
					str += '<td>'+value.DESCRIPTION+'</td>';
					str += '<td>'+value.PARAM_COUNT+'</td>';
					/* str += '<td>'+value.REMARKS+'</td>'; */
					str += '<td><span class="remove"onclick="fnDelete('+value.SEQ_NO+');"></span></td>';
					str +='</tr>'; 
				});
				
				body.append(str);
			}
		}
			
		function fnWrite(seqno) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/luawrite.do' />");
			comSubmit.addParam("SEQ_NO", seqno);			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDelete(seqno) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/luadelete.do' />");
				comSubmit.addParam("SEQ_NO", seqno);			
				comSubmit.submit();			
				fnpreventDefault(event);	
			}
		}		
		
		function fnBack() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/userlist.do' />");			
			comSubmit.submit();			
			fnpreventDefault(event);	
		}
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW users">
			<div class="btnBox">
				<div onclick="fnWrite(0);" class="setEncoding">Create Lua</div>
				<div onclick="fnBack();" class="editLua">Back</div>
			</div>
			<div class="liBox table">				
				<table id="tbusers" class="table">
                   <caption class="hide" style="display:none;">USER</caption>
                   <colgroup>
						<col width="33%" />
						<col width="*" />
						<col width="10%" />
						<!-- <col width="*" /> -->
						<col width="3%" />
					</colgroup>
					<thead>
						<tr>
							<th>함수명</th>
							<th>인자값</th>
							<th>인자수</th>
							<!-- <th>설명</th> -->								
							<th>&nbsp;</th>
						</tr>
					</thead>
                   <tbody>
                   </tbody>
               </table>
               
               <!--board paging start-->
				<div id="PAGE_NAVI" class="board_paging">								
				</div>
				<!--board paging end-->
				<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			</div>			
		</div>
	</div>
</body>
</html>