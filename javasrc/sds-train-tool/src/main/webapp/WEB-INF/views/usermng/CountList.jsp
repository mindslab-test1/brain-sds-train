<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">		
		window.onload = function() {			
			$('input:text').keyup(function() {
				$(this).val($(this).val().replace(/[^Z0-9]/g, ''));
			});
		}
		
		function fnSave() {			
			if($('#txtDomainCnt').val().trim() == '') {
				alert('도메인 수를 입력해 주세요.');
				return;
			}
			else if($('#txtUserSayCnt').val().trim() == '') {
				alert('사용자 발화 수를 입력해 주세요.');
				return;
			}
			else if($('#txtSlotObjCnt').val().trim() == '') {
				alert('슬롯 단어 수를 입력해 주세요.');
				return;
			}
			else if($('#txtInstanceCnt').val().trim() == '') {
				alert('Instance 수를 입력해 주세요.');
				return;					
			}
			fnLoading();
			var comSubmit = new ComSubmit('countform');
			comSubmit.setUrl("<c:url value='/view/updatecountmng.do' />");					
			comSubmit.submit();			
			fnpreventDefault(event);
		}
	</script>	
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<form id="countform" name="countform" style="width:100%; padding-top:0px;">		
			<div class="middleW instances" style="padding:10px 20px 20px 260px;">
				<div class="btnBox">
					<div class="save" onclick="fnSave();">SAVE</div>
				</div>			
				<div class="liBox liBox4 liBox5 table" style="margin-top:20px;">
					<h3 class="table_h3" style="cursor:default !important;">데이터 생성 관리</h3>
					<div class="admin_user_con entities_ex_con">
						<div class="admin_user_title">
							<table id="tbslot" style="" class="slot_instan_title">							
								<colgroup>
									<col width="60%">
									<col width="">
								</colgroup>
								<thead>
									<tr>
										<th>명칭</th>
										<th>제한 개수</th>
									</tr>
								</thead>
								<tbody>	
									<tr>
										<td>생성 가능한 도메인 수</td>
										<td><input type="text" class="none_focus" id="txtDomainCnt" value="${item.DOMAIN_COUNT }" name="DOMAIN_COUNT" /></td>
									</tr>
									<tr>
										<td>생성 가능한 사용자 발화 수</td>
										<td><input type="text" class="none_focus" id="txtUserSayCnt" value="${item.USERSAY_COUNT }" name="USERSAY_COUNT" /></td>
									</tr>
									<tr>
										<td>생성 가능한 슬롯 단어 수</td>
										<td><input type="text" class="none_focus" id="txtSlotObjCnt" value="${item.SLOTOBJ_COUNT }" name="SLOTOBJ_COUNT" /></td>
									</tr>
									<tr>
										<td>생성 가능한 Instance 수</td>
										<td><input type="text" class="none_focus" id="txtInstanceCnt" value="${item.INSTANCE_COUNT }" name="INSTANCE_COUNT" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>	
</body>
</html>