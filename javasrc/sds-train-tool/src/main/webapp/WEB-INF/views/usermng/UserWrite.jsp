<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			fnSetNaviTitle('Create User');
			
			if ('${USING}' == 'Y') {
				$('#txtUserId').val('${USER_ID}');				
				$('#txtUserPwd').val('${USER_PWD}');
				$('#txtUserName').val('${USER_NAME}');
				$('#selUserType').val('${USER_TYPE}');
				alert('이미 존재하는 아아디 입니다.');
			}
			else if ('${SEQ_NO}' != '0') {
				fnSetNaviTitle('Users > ${userinfo.USER_ID}');
				$('#txtUserId').attr('readonly', 'readonly');
				$('#selUserType').val('${userinfo.USER_TYPE}');				
			}
		}
		
		function fnDomainList(userno, userid, apikey, type) {
			fnWinPop("<c:url value='/view/userdomain.do' />" + "?userno=" + userno + "&userid=" + userid + "&userapi=" + apikey + "&type=" + type, "DomainPop", 550, 408, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnSave() {
			$txtUserId = $('#txtUserId');
			$txtUserPwd = $('#txtUserPwd');
			$txtUserName = $('#txtUserName');
			
			if ($txtUserId.val().trim() == '') {
				alert('아이디를 입력하세요.');
				$txtUserId.focus();
				return;
			}
			
			if ($txtUserPwd.val().trim() == '') {
				alert('비밀번호를 입력하세요.');
				$txtUserPwd.focus();
				return;
			}
			
			if ($txtUserName.val().trim() == '') {
				alert('이름을 입력하세요.');
				$txtUserName.focus();
				return;
			}
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/userinsert.go' />");
			comAjax.setCallback("fnSaveCallBack");
			comAjax.addParam("SEQ_NO", '${SEQ_NO}');
			comAjax.addParam("USER_ID", $txtUserId.val());
			comAjax.addParam("USER_PWD", $txtUserPwd.val());
			comAjax.addParam("USER_NAME", $txtUserName.val());
			comAjax.addParam("USER_TYPE", $('#selUserType').val());
			comAjax.addParam("REMARKS", $('#txtRemarks').val());
			comAjax.addParam("API_KEY", "");
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {
			if (data.STATUS == 'S') {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/userlist.do' />");				
				comSubmit.submit();
			}
			else if (data.STATUS == 'UI') {
				alert('이미 존재하는 ID 입니다.');
			}
			else
				alert('작업 중 오류가 발생하였습니다.\n관리자에게 문의하여 주십시오.');			
		}
	</script>
</head>
<body>	
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW users usersDetail">
			<div class="btnBox">
				<div class="save" onclick="fnSave();">Save</div>
			</div>
			<div class="liBox table">
				<table class="table">
					<caption class="hide" style="display:none;">USER</caption>
						<colgroup>
							<col width="30%">
							<col width="70%">
						</colgroup>
					<tbody>
						<tr>	
							<th>아이디</th>
							<td><input type="text" id="txtUserId" name="USER_ID" value="${userinfo.USER_ID}" placeholder="아이디를 입력해주세요." /></td>
						</tr>
						<tr>
							<th>비밀번호</th>
							<td><input type="password" id="txtUserPwd" name="USER_PWD" value="${userinfo.USER_PWD}" placeholder="비밀번호를 입력해주세요."></td>
						</tr>
						<tr>
							<th>이름</th>
							<td><input type="text" id="txtUserName" name="USER_NAME" value="${userinfo.USER_NAME}" placeholder="이름을 입력해주세요."></td>
						</tr>
						<tr>
							<th>권한</th>
							<td>
								<select id="selUserType" name="USER_TYPE">
									<option value="GU">GU</option>
									<option value="USER">USER</option>
									<option value="SA">SA</option>
									<option value="OA">OPEN API</option>
								</select>
							</td>
						</tr>
						<c:if test="${SEQ_NO ne 0 && userinfo.USER_TYPE ne 'SA'}">
						<tr>
							<th>도메인리스트</th>
							<td>
								<button type="button" onclick="fnDomainList(${SEQ_NO}, '${userinfo.USER_ID}', '${userinfo.API_KEY}', 'v');">보기</button>
								<button type="button" onclick="fnDomainList(${SEQ_NO}, '${userinfo.USER_ID}', '${userinfo.API_KEY}', 'c');">복사</button>							
							</td>
						</tr>
						</c:if>
						<tr class="memo">
							<th>메모</th>
							<td><textarea id="txtRemarks" name="REMARKS">${REMARKS}${userinfo.REMARKS}</textarea></td>
						</tr>
					</tbody>
              </table>
			</div>
		</div>
	</div>
</body>
</html>