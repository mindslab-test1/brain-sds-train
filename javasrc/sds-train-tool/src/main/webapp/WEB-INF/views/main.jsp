<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>대화모델링구축도구</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/main-style.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
	<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ui.all.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout.min.js'/>"></script> --%>
	<script type="text/javascript" src="<c:url value='/js/jquery-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout-latest.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">
		var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method
		var enginedata = '';
		var idxcal = 0;
		var enginestate = false;
		var engineEndchk = false;
		var leanInterval;
		//var enginedata = JSON.parse('{"SYS_UTTER":"어떤 피자 주문할꺼냐고","TURN":"9","DIALOG_END":"FALSE","LOG_HIST":[{"USER":{"INPUT":"","INTENTION":[]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할래?","PATTERN":"어떤 피자 주문할래?","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"흠","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"생각좀하자","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"ㅡㅡ","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"야","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]}]}');
		$(document).ready(function () {	
			var width = $(document).width() * 0.25;
			
			myLayout = $('body').layout({
				east__size:					width
			,	east__minSize:				360
			,	east__spacing_closed:		20
			,	east__togglerLength_closed:	130
			,	east__togglerAlign_closed:	"top"
			,	east__togglerContent_closed:"D<BR>i<BR>a<BR>l<BR>o<BR>g<BR>"
			,	east__togglerTip_closed:	"Open Dialog"
			,	east__sliderTip:			"Slide Open Dialog"
			,	east__slideTrigger_open:	"mouseover"
			,	east__initClosed:			true
			,	north__spacing_open:	0
			,	center__maskContents:		true // IMPORTANT - enable iframe masking
			,	resizable:					true			
			});
			
			//기본 세팅			
			<%-- var iframe2 = document.getElementById("iframe2");
			if (!gfn_isNull(iframe2))
				iframe2.src = encodeURI('<%= session.getServletContext().getInitParameter("engineUrl")%>/api.start.php?prjname=' + getCookie("ProjectName") + '&port=<%= session.getAttribute("ServerPort")%>&type=dynamic&uniqid=<%= session.getAttribute("UNIQID")%>' + '&engine=' + getCookie("ProjectEngine") + '&dic=' + getCookie("ProjectDic")); --%> 	
							
			$('#areInput').keypress(function(e) {
				if(e.keyCode == 13) {
					if($(this).val().trim() != '') {
						$('#divResult').append('user: ' + $(this).val() + '<br />');
						fnInput('I');						
					}
					
					fnpreventDefault(e);
				}					
			});


			if('<%= session.getAttribute("BIZ_CHATBOT")%>' != 'null' &&
					'<%= session.getAttribute("USER_NAME")%>' == 'null') {
				// UNIQIT set
				setCookie("UNIQID", uniqid());

				if (getCookie("USER_NAME") == null) {
					alert('마음 챗봇 사용자가 등록되어 있지 않습니다.')
				} else {

					setCookie("USER_NAME", "TMP");
					var comSubmit = new ComSubmit();
					comSubmit.setUrl("<c:url value='/view/main.go' />");
					comSubmit.submit();
				}
			}

			//mainlogo
			$('#mainlogo').click(function(e) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/main.go' />");					
				comSubmit.submit();
				fnpreventDefault(e);
			});
			
			if (getCookie("DialProjectName") == '')
				$('#spDomain').text(getCookie("ProjectName"));	
			else
				$('#spDomain').text(getCookie("DialProjectName"));
			
			if (getCookie("LeanProjectName") == '')
				$('#spLeanDomain').text(getCookie("ProjectName"));	
			else
				$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
				$('#aTutorMap').hide();
			}			
			
			if ('<%= session.getAttribute("USER_NAME")%>' == 'null') {

				$('#spNim').hide();
				$('#spUserName').text('로그인 해주세요.');
				$('#splogout').text('로그인');
				$('#spChangePw').hide();

			}
			else {
				$('#splogout').text('로그아웃');				
				$('#spUserJoin').hide();				
			}
		});					
		
		function fnLearning() {
			if (!fnLoginChk()) {
				return;
			}
			
			$('#divlean').show();
			$('#divleanbox').html('학습 중 입니다.<br />잠시만 기다려 주세요.');
			$('#divStart').hide();
			$('#divDialtitle').text('Learning Log..');				
			setCookie("LeanProjectName", getCookie("ProjectName"));
			$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearning.do' />");
			comAjax.setCallback("fnLearningCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnLearningCallBack(data) {				
			if (data.STATUS == 'OK') {
				leanInterval = setInterval(fnLearningCompliteChk, 1000);	
			}
			else
				alert('학습 중 오류가 발생하였습니다.');						
		}
		
		function fnLearningCompliteChk() {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearningchk.do' />");
			comAjax.setCallback("fnLearningCompliteChkCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
		}

		function fnLearningCompliteChkCallBack(data) {
			$('#divleanbox').html(data.LOG).scrollTop($("#divleanbox")[0].scrollHeight);;
			if (data.LOG.indexOf('FINISHED') > -1 || data.LOG.indexOf('Finished') > -1) {
				clearInterval(leanInterval);
			}
		}
		
		function fnStart() {
			if (!fnLoginChk()) {
				return;
			}
			
			$('#divStart').show();
			$('#divlean').hide();
			$('#divDialtitle').text('Dialog');
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogstart.do' />");			
			comAjax.setCallback("fnStartCallBack");
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			
			/* engineEndchk = true;
			setTimeout(function() {
				fnErrorChk();				
			}, 5000); */
			
			//var iframe2 = document.getElementById("iframe2");
			//iframe2.src = encodeURI('<%= session.getServletContext().getInitParameter("engineUrl")%>/api.start.php?prjname=' + getCookie("ProjectName") + '&port=<%= session.getAttribute("ServerPort")%>&type=dynamic&uniqid=<%= session.getAttribute("UNIQID")%>' + '&engine=' + getCookie("ProjectEngine") + '&dic=' + getCookie("ProjectDic"));
			//$('#divLog').hide();			
			fnpreventDefault(event);			
		}
		
		function fnStartCallBack(data) {
			engineEndchk = false;
			if (data.STATUS == 'OK') {
				$('#divResult').html('');
				$('#areInput').removeAttr('disabled');
				//$('#divLog').hide();				
				setCookie("DialProjectName", getCookie("ProjectName"));
				$('#spDomain').text(getCookie("DialProjectName"));
				$('#btnStart, #btnStop, #btnClear').show();				
				alert('엔진이 구동 되었습니다.');
			}
			else if (data.STATUS == 'NL') {
				alert('학습을 먼저 해 주세요.');
			}
			else
				alert('엔진 구동 중 오류가 발생하였습니다.\n' + data.STATUS);
		}
		
		function fnInput(type) {
			if (!fnLoginChk()) {
				return;
			}
			
			engineEndchk = true;
			/* setTimeout(function() {
				fnErrorChk();
			}, 5000); */
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialoginput.do' />");
			comAjax.setCallback("fnInputCallBack");
			if (type != 'S') { 
				comAjax.addParam("MSG", $('#areInput').val().replace(/\s/g, '|@').replace(/\\/g, '\\\\').replace(/\"/g, '\\"'));
				$('#areInput').val('');
				$('#areInput').attr('disabled', 'disabled');
			}
			else {
				$('#divResult').html('');
			}
			
			comAjax.addParam("TYPE", type);
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			
			fnpreventDefault(event);
		}

		function fnInputCallBack(data) {
			engineEndchk = false;
			if (data.LOG != '') {
				enginestate = true;
				try {
					enginedata = JSON.parse(data.LOG);
					$('#areInput').removeAttr('disabled').focus();
					//if($('#divLog').css('display') == 'block')
					if ('<%= session.getAttribute("ProjectLangNo")%>' != 2) {
						fnLog();
					}
					/* if($('#divDevLog').css('display') == 'block')
						fnDevLog(); */
					if (enginedata.DIALOG_END == 'TRUE') {					
						if (enginedata.SYS_UTTER != '_EMPTY_STRING_')
							$('#divResult').append('system: ' + enginedata.SYS_UTTER + '<br />').scrollTop($("#divResult")[0].scrollHeight);
						fnStop('M');
						return;
					}
					else
						$('#divResult').append('system: ' + enginedata.SYS_UTTER + '<br />').scrollTop($("#divResult")[0].scrollHeight);					
				} catch (e) {
					// TODO: handle exception					
					alert('구동 중에 문제가 발생하였습니다. 구축한 대화 모델을 한 번 더 확인해주세요.');
					fnEngineStop();
					return;
				}											
			}			
			else {
				if (data.STATUS != '') {
					alert(data.STATUS.replace(/<BR>/g, '\n'));
				}
				else
					alert('엔진을 구동시켜주세요.');
			}
						
		}
		
		function fnStop(type) {
			if (!fnLoginChk()) {
				return;
			}
			
			if (!enginestate) {
				alert('종료시킬 대화가 없습니다.');
				return;
			}
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogstop.do' />");
			comAjax.setCallback("fnStopCallBack");			
			comAjax.addParam("TYPE", type);
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(false);
			comAjax.ajax();
			
			if (type == 'M')
				fnpreventDefault(event);
		}
		
		function fnStopCallBack(data) {
			//var stopdata = JSON.parse(data.LOG);
			if (data.LOG == 'OK') {						
				if (data.TYPE == 'M')
					$('#divResult').append('-대화가 종료되었습니다.-<br />');									
				else
					$('#divResult').html('');
				$('#areInput').attr('disabled', 'disabled');
				enginestate = false;
			}
			else {
				alert('엔진 중지 중 문제가 발생하였습니다.');
			}
		}
		
		function fnErrorChk() {
			if (engineEndchk) {
				engineEndchk = false;
				alert('구동 중에 문제가 발생하였습니다. 구축한 대화 모델을 한 번 더 확인해주세요.');
				fnEngineStop();
				$('#divResult').append('-대화가 종료되었습니다.-<br />');
			}
		}
		
		function fnEngineStop() {
			if (!fnLoginChk()) {
				return;
			}
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/enginestop.do' />");
			comAjax.setCallback("");
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
		}
		
		function fnClear() {
			$('#divResult').html('');
			if ($('#areInput').attr('disabled') == 'disabled') {
				enginedata = '';
				//$('#txtDevLog').val('');
				$('#divLog, #divDevLog').hide();	
			}			
			fnpreventDefault(event);
		}
		
		function fnLog() {
			if (!gfn_isNull(enginedata)) {				
				var idx = Math.ceil(enginedata.TURN/2) - 1 + idxcal;
											
				if (idx == 0) {		
					idx = 0;
					idxcal = (enginedata.LOG_HIST.length + 1) - (enginedata.LOG_HIST.length * 2);
					$('#spTurn').text('1');										
				}
				else {
					if (enginedata.LOG_HIST[idx] == undefined) {
						idxcal --;
						return;
					}
					var titleidx = idx * 2 + 1;
					$('#spTurn').text((titleidx - 1).toString() + ', ' + titleidx.toString()); 	
				}
				$('#hidTurn').val(idx);
				
				var str = '';				
				for (var i = 0; i < enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS.length; i++) {
					if (i == 0)
						str += enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].END_TURN + ')'; 
					else	
						str += ', ' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].END_TURN + ')';										 
				}
				$('#tdPrevTask').text(str);
				str = '';
				str = enginedata.LOG_HIST[idx].TASK_HIST.CURRENT_TASKS.TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.CURRENT_TASKS.START_TURN + ')';
				$('#tdCurrentTask').text(str);				
				
				str = '';
				for (var i = 0; i < enginedata.LOG_HIST[idx].ENTITY_HIST.length; i++) {
					str += '<tr>';
					str += '<th>' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].SLOT_NAME + '&nbsp;=&nbsp;</th>';
					str += '<td>' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].SLOT_VAL + '(' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].CHANGED_TURN + ')' + '</td>';
					str += '</tr>';
				}
				$('#tbEntities').html(str);			
							
				str = '';
				str += '<tr><th colspan="2">User</th></tr>';
				if (enginedata.LOG_HIST[idx].USER.INTENTION.length == 0) 
					str += '<tr><th class="th_bg02">utterance<span>&nbsp;:&nbsp;</span></th>';
				else
						str += '<tr><th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>';				
				str += '<td>' + enginedata.LOG_HIST[idx].USER.INPUT + '</td></tr>';
				
				var classnm = 'th_bg01';
				for (var i = 0; i < enginedata.LOG_HIST[idx].USER.INTENTION.length; i++) {
					if ((i + 1) == enginedata.LOG_HIST[idx].USER.INTENTION.length)
						classnm = 'th_bg02';
					
					str += '<tr><th class="' + classnm + '">intent (' + (i + 1).toString() + ')<span>&nbsp;:&nbsp;</span></th>';
					str += '<td>' + enginedata.LOG_HIST[idx].USER.INTENTION[i].DA + ' (' + enginedata.LOG_HIST[idx].USER.INTENTION[i].CONFIDENCE + ')</td></tr>';	
				}
				
				str += '<tr><th colspan="2">System</th></tr>';
				str += '<tr><th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.OUTPUT + '</td></tr>';
				str += '<tr><th class="th_bg01">intent<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.INTENT + '</td></tr>';
				str += '<tr><th class="th_bg02">pattern<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.PATTERN.replace(/</g, '&lt;').replace(/>/g, '&gt;') + '</td></tr>';				
				$('#tbIntents').html(str);
				
				$('#divLog').show();
				//$('#divDevLog').hide();
			}
			else {
				alert('엔진을 시작해주세요.');	
			}
			//fnWinPop("<c:url value='/view/logview.do' />", "LogPop", 713, 626, 0, 0);	
		}
		
		function fnLogPrevAndNext(type) {
			if (type == 'P') {
				if(Math.ceil(enginedata.TURN/2) - 1 + idxcal != 0)
					idxcal --;
			}
			else 
				idxcal ++;
			fnLog();
		}
		
		function fnDataDownload() {
			if (!fnLoginChk()) {
				return;
			}
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/common/downloadzipFile.do' />");
			comSubmit.addParam("PROJECT_NAME", getCookie("ProjectName"));		
			comSubmit.submit();
			fnpreventDefault(event);	
		}
		
		function fnLeanFile() {
			if (!fnLoginChk()) {
				return;
			}
			
			$('#divlean').show();
			$('#divleanbox').html('지식 저장을 위해 지식 데이터를 변환하고 있습니다.<br />잠시만 기다려 주세요.');
			$('#divStart').hide();
			setCookie("LeanProjectName", getCookie("ProjectName"));
			$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearnfilesave.do' />");
			comAjax.setCallback("fnLeanFileCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnLeanFileCallBack(data) {				
			if (data.STATUS == 'OK') {
				$('#divleanbox').text('지식 저장이 완료 되었습니다.');					
			}
			else
				alert('지식 저장 중 오류가 발생하였습니다.');						
		}
		
		function fnDevLog() {
			$('#divLog').hide();
			if (!gfn_isNull(enginedata)) {
				$('#divDevLog').show();
				$('#txtDevLog').val(enginedata.DEV_LOG).scrollTop($("#txtDevLog")[0].scrollHeight);
			}
		}
		
		function fnTutor() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/tutordomainlist.do' />");					
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnRecursiveReplace(str, srep, erep, idx) {			
			var s_idx = str.indexOf(srep, idx);
			var e_idx = str.indexOf(erep, s_idx);
			var reg;
			if(s_idx > -1 && e_idx > -1) {
				var strtemp = str.substring(s_idx + srep.length, e_idx);
				if (strtemp.indexOf('<') > -1) {
					reg = new RegExp(srep + strtemp + erep,'g');
					str = str.replace(reg, srep + strtemp.replace(/</, '&lt;').replace(/>/, '&gt;') + erep);	
				}			
				fnRecursiveReplace(str, srep, erep, e_idx);
			}	
			else
				return str;
		}
		
		
		/* function fnOpenXmlView() {
			var str = $('#txtDevLog').val();
			var strtemp = '';
			var arrReg = new Array();
			arrReg.push('sentence');
			arrReg.push('SLU1');
			arrReg.push('pattern');			
			
			for (var i = 0; i < arrReg.length; i++) {
				str = fnRecursiveReplace(str, '<' + arrReg[i] + '>', '</' + arrReg[i] + '>', 0);
			}
			
			$('#hidDevLog').val('<dev_log>' + str + '</dev_log>');
			var pop_title = "NoticePop" ;
	         	        
	        fnWinPop("", pop_title, 713, 626, 0, 0);
	        
	        var frmData = document.frmData ;
	        frmData.target = pop_title ;
	        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
	        frmData.submit();
	        fnpreventDefault(event);
		}
		
		function fnOpenXmlView() {
			var str = $('#txtDevLog').val();
			var strtemp = '';
			var arrReg = new Array();
			arrReg[0] = 'sentence';
			arrReg[1] = 'SLU1';
			arrReg[2] = 'pattern';			
			var arrRegExec;
			
			var reg;
			var subreg;
			
			for (var i = 0; i < arrReg.length; i++) {
				reg = new RegExp('<' + arrReg[i] + '>[\\W0-9a-zA-Z]+<\/' + arrReg[i] + '>','g');
				arrRegExec = str.match(reg);
				
				if (arrRegExec != null) {
					for (var j = 0; j < arrRegExec.length; j++) {						
						strtemp = '<' + arrReg[i] + '>' + arrRegExec[j].replace(new RegExp('<' + arrReg[i] + '>|<\/' + arrReg[i] + '>','g'), '').replace('<', '&lt;').replace('>', '&gt;') + '</' + arrReg[i] + '>';
						str.replace(arrRegExec[j], strtemp);						
					}
				}
			}
			
			$('#hidDevLog').val('<dev_log>' + str + '</dev_log>');
			var pop_title = "NoticePop" ;
	         	        
	        fnWinPop("", pop_title, 713, 626, 0, 0);
	        
	        var frmData = document.frmData ;
	        frmData.target = pop_title ;
	        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
	        frmData.submit();
	        fnpreventDefault(event);
		} */
		
		function fnOpenXmlView() {
			/*
			if (!gfn_isNull(enginedata.DEV_LOG)) {
				$('#hidDevLog').val('<dev_log>' + enginedata.DEV_LOG + '</dev_log>');
				var pop_title = "XmlPop" ;
		         	        
		        fnWinPop("", pop_title, 713, 626, 0, 0);
		        
		        var frmData = document.frmData ;
		        frmData.target = pop_title ;
		        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
		        frmData.submit();
		        fnpreventDefault(event);	
			} 
			else {
				alert('로그가 없습니다.');
			}
			*/

			if (!gfn_isNull(enginedata.DEV_LOG)) {
				console.log('dev_log->');
				console.log(enginedata.DEV_LOG);
				$("#txtDevLog").val(enginedata.DEV_LOG);
				fnSetXmlViewAndShow();
			} else {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/xmlviewerEnglish.do' />");
				comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
				comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
				comAjax.setCallback("fnXmlViewEnglishCallBack");
				comAjax.setAsync(true);
				comAjax.ajax();
				fnpreventDefault(event);
			}
		}

		function fnXmlViewEnglishCallBack(data) {
			console.log(data.STR);
			enginedata.DEV_LOG = data.STR;
			fnSetXmlViewAndShow();
		}

		function fnSetXmlViewAndShow() {
			console.log(enginedata.DEV_LOG);
			$("#txtDevLog").val(enginedata.DEV_LOG);
			$('#hidDevLog').val('<dev_log>' + $("#txtDevLog").val() + '</dev_log>');
			var pop_title = "XmlPop" ;
			fnWinPop("", pop_title, 713, 626, 0, 0);

			var frmData = document.frmData ;
			frmData.target = pop_title ;
			frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;
			frmData.submit();
			fnpreventDefault(event);
		}

		function fnOpenXmlViewText() {
			$('#divDevLog').show();
			$('#txtDevLog').val(enginedata.DEV_LOG).scrollTop($("#txtDevLog")[0].scrollHeight);
		}
		
		function fnNotice() {			
			fnWinPop("<c:url value='/view/noitce.do' />" + "?Type=N", "NoticePop", 713, 626, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnlogout() {
			if ($('#splogout').text() == '로그인') {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/login.go' />");	
				comSubmit.submit();
			}
			else {
				deleteCookie("USER_TYPE");
				deleteCookie("USER_ID");
				deleteCookie("USER_NAME");
				deleteCookie("USER_SEQ");
				deleteCookie("UNIQID");
				
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/logout.go' />");	
				comSubmit.submit();
			}
			fnpreventDefault(event);
		}
		
		function fnJoin() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/join.go' />");
			comSubmit.addParam("TYPE", "J");
			comSubmit.submit();
		}
		
		function fnLoginChk() {
			if ('<%= session.getAttribute("USER_NAME")%>' == 'null') {
				alert('로그인 후 이용해주세요.');
				return false;
			}
			else 
				return true;
		}
		
		function fnChangePw() {
			fnWinPop("<c:url value='/view/openchangepassword.do' />?usertype=${USER_TYPE}", "PwPop", 510, 410, 0, 0);			
		}
		
		function fnMigration() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/migration.go' />");
			comSubmit.addParam("USER_SEQ", getCookie('USER_SEQ'));
			comSubmit.addParam("USER_TYPE", getCookie('USER_TYPE'));
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<c:set var="bizChatbot" value="<%= session.getAttribute(\"BIZ_CHATBOT\")%>" scope="session"/>
	<c:if test="${bizChatbot eq null}">
		<div class="header ui-layout-north ui-layout-pane ui-layout-pane-north">
			<div class="logo"><a id="mainlogo" href="#">GENIE<span>DIALOG</span></a></div>
			<div class="title"><span id="spNavi">Overview</span></div>
			<div class="userLog">
				<span id="spUserName" class="name"><%= session.getAttribute("USER_NAME")%></span>
				<span id="spNim">님</span>
				<c:if test="${USER_TYPE eq 'GU' || USER_TYPE eq 'OA'}">
					<span id="spChangePw" onclick="fnChangePw();" class="changepw">비밀번호 변경</span>
				</c:if>
				<span id="splogout" onclick="fnlogout();" class="logout">로그아웃</span>
				<span id="spUserJoin" class="join_st" onclick="fnJoin();">사용자 등록</span>
			</div>
		</div>
	</c:if>

	<iframe id="mainFrame" name="mainFrame" class="ui-layout-center"
		width="100%" height="100%" frameborder="0" scrolling="auto"
		src="<c:url value='/view/mainIframe.go' />">
	</iframe>	
	
	<div class="ui-layout-east ui-layout-pane ui-layout-pane-east">
		<div id="right">
			<div class="rightW" style="right:-450px">
				<div class="body">
					<div class="top">
						<div class="notice" onclick="fnNotice();">공지사항</div>
					</div>
					<div class="dialogT">
						<div class="title">DIALOG</div>
						<div class="btn">												
							<!-- <div onclick="fnMigration();">Migration</div> -->	
							<div onclick="fnLeanFile();">지식저장</div>
							<div onclick="fnLearning();">학습</div>
							<div class="on" onclick="fnStart();">구동</div>
							<c:if test="${USER_TYPE ne 'GU' && USER_TYPE ne 'OA'}">
							<div onclick="fnDataDownload();">data 다운</div>
							</c:if>
						</div>
					</div>
					<div class="dialogBox">
						<div id="divlean">
							<div class="chat chat2">
								<div class="top">
									<div class="title">현재 도메인</div>
									<div class="domain"><span id="spLeanDomain"></span></div>
								</div>
								<div class="chatBox" id="divleanbox">지식 저장을 위해 데이터를 변환하고 있습니다.<br />잠시만 기다려 주세요.</div>
							</div>
						</div>
						<div id="divStart" class="on">
							<div class="chat">
								<div class="top">
									<div class="title">현재 도메인</div>
									<div class="domain"><span id="spDomain"></span></div>
								</div>
								<div class="btnBox">
									<div id="btnStart" onclick="fnInput('S');" style="display:none;">Start</div>
									<div id="btnStop" onclick="fnStop('M');" style="display:none;">Stop</div>
									<div id="btnClear" onclick="fnClear();" style="display:none;">Clear</div>
									<!-- <div onclick="fnLog();">Log</div> -->
									<div onclick="fnOpenXmlView();">DevLog</div>
									<div onclick="fnOpenXmlViewText();">DevTest용</div>
								</div>
								<div class="chatBox" id="divResult" style="overflow-y:auto;"></div>
								<div class="chatBox chatInput">
									<textarea id="areInput" class="inputarea" disabled='disabled' placeholder="여기에 입력해주세요."></textarea>
								</div>
							</div>
							
							<div id="divLog" style="margin-top:15px; display:none;">
								<!-- Title h1 -->
								<p class="logtitle">Turn <span id="spTurn"></span><input type="hidden" id="hidTurn" /></p>					
								<div class="logtitle_btn">
									<a onclick="fnLogPrevAndNext('P');" href="#"><img src="../images/btn_prev.png" alt="이전 보기"></a>
									<a onclick="fnLogPrevAndNext('N');" href="#"><img src="../images/btn_next.png" alt="다음 보기"></a>
								</div>
								<!-- Title h1// -->
								
								<hr />
								<div class="turn_chapter01">
									<span class="t_title_buu t_title_but" style="float:none;">Task</span>
									<div class="turn_table_u">
										<table class="turn_table" summary="task">
											<caption class="hide">정보 표</caption>
											<colgroup>
												<col width="38%" />
												<col width="*" />
											</colgroup>
											<tbody>
												<tr>
													<th>Previous task&nbsp;:&nbsp;</th>
													<td id="tdPrevTask">&nbsp;</td>
												</tr>
												<tr>
													<th>Current task&nbsp;:&nbsp;</th>
													<td id="tdCurrentTask">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="turn_chapter02">
									<span class="t_title_buu t_title_but" style="float:none;">Entities</span>
									<div class="turn_table_u">
										<table class="turn_table" summary="Entities">
											<caption class="hide">정보 표</caption>
											<colgroup>
												<col width="38%" />
												<col width="*" />
											</colgroup>
											<tbody id="tbEntities">
												
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="turn_chapter03">
									<span class="t_title_buu t_title_but" style="float:none;">Intents</span>
									<div class="turn_table_u">
										<table class="turn_table" summary="Intents">
											<caption class="hide">정보 표</caption>
											<colgroup>
												<col width="38%" />
												<col width="*" />
											</colgroup>
											<tbody id="tbIntents">
												<tr>
													<th colspan="2">User</th>
												</tr>
												<tr>
													<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<th class="th_bg02">intent<span>&nbsp;:&nbsp;</span></th>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<th colspan="2">System</th>
												</tr>
												<tr>
													<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<th class="th_bg01">intent<span>&nbsp;:&nbsp;</span></th>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<th class="th_bg02">pattern<span>&nbsp;:&nbsp;</span></th>
													<td>&nbsp;</td>
												</tr>												
											</tbody>
										</table>
									</div>
								</div>
							</div>					
							
							<div id="divDevLog" style="display:none;">
								<textarea id="txtDevLog" style="margin:0; margin-top:15px; padding:0px; width:100%; min-height:150px; max-height:250px;" class="leanbox">
								
								</textarea>
								<a href="#" onclick="fnOpenXmlView();" style="float:right; color:#ea5404;">xml viewer</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="divlean" style="display:none;">
				<div style="font-size:14px; color:#666666; margin:16px;">
					<span>현재 도메인 : <b><span id="spLeanDomain"></span></b></span>
				</div>				
				<div id="divleanbox" class="leanbox">
					<!-- <iframe id="iframe1" style="width:100%;height: 580px;"></iframe> -->
				</div>				
			</div>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
		<input type="hidden" id="depth1" name="depth1" value="1" />
		<input type="hidden" id="depth2" name="depth2" value="1" />
		<input type="hidden" id="depth3" name="depth3" value="1" />
	</form>
	<!-- 팝업창으로 전송하는 정보 -->
	<form name="frmData" id="frmData" method="post">
	    <input type="hidden" id="hidDevLog" name="STR" />
	</form>
</body>
</html>