<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			fnSetNaviTitle('Intents');
			if ('${param.UPLOAD_STATUS}' == 'F') {
				if ('${param.STATUS}' == 'NS')
					alert('Entities에서 작성하지 않은 슬롯이 포함되어 있습니다.');				
				else
					alert('파일 형식이 잘못되었습니다.');				
			}
		}

		function fnSearch() {
			var comSubmit = new ComSubmit();
			if ($('#slot_search').val() == 'Search intents…') 
				comSubmit.addParam("INTENT_NAME", "");				
			else
				comSubmit.addParam("INTENT_NAME", $('#slot_search').val());
			
			comSubmit.setUrl("<c:url value='/view/useruttrlist.do' />");
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnOpenCreatDatype() {		
			fnGoDetail(0);
			fnpreventDefault(event);
		}
		
		function fnGoDetail(Seq, AgentSeq) {
			var Name = $('#hid' + Seq).val();
			var comSubmit = new ComSubmit();
			if (Seq == 0) {
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.setUrl("<c:url value='/view/useruttrwrite.do' />");	
			}
			else {
				comSubmit.setUrl("<c:url value='/view/useruttredit.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", AgentSeq);
				comSubmit.addParam("INTENT_SEQ", Seq);
				comSubmit.addParam("INTENT_NAME", Name);
				comSubmit.addParam("SLOT_SEQ_NO", "");
				comSubmit.addParam("INTENTION", "");
			}			
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();
			fnpreventDefault(event);
		}		
		
		function fnDelete(Seq) {
			if (confirm('해당 항목을 삭제 하시겠습니까?\r\n[주의] 해당 항목이 포함된 지식이 해당 항목의 삭제로 인하여 오류가 발생할 수 있습니다.')) {
				fnLoading();
				var comSubmit = new ComSubmit();
				comSubmit.addParam("SEQ_NO", Seq);			
				comSubmit.setUrl("<c:url value='/view/deleteintent.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
			fnstopPropagation(event);
		} 	
		
		function fnUpLoadIntentSubmit() {
			fnLoading();
			var agentSeq;
			var type; 
			if ('${AGENT_SEQ}' == '' || '${AGENT_SEQ}' == '0') {
				type = 'AL';
				agentSeq = '0';
			}
			else {
				type = 'TL';
				agentSeq = '${AGENT_SEQ}';
			}
			
			var comSubmit = new ComSubmit("UploadForm");
			comSubmit.setUrl('<c:url value='/view/insertusersayfile.do' />');
			comSubmit.addParam("TYPE", type);		
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", agentSeq);			
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();	
		}		
		
		function fnOpenCopyIntent() {
			fnWinPop("<c:url value='/view/openreferencesearch.do' />" + '?Type=I&AGENT_SEQ=${AGENT_SEQ}', "Sensepop", 600, 550, 0, 0);
		}
		
		function fnOpenIntention() {
			fnWinPop("<c:url value='/view/selectintention.do' />" , "IntentionSelect", 900, 400, 0, 0);	
		}
		
		function fnGoREIntent(type) {
			var comSubmit = new ComSubmit();			
			if (type == 'R')
				comSubmit.setUrl("<c:url value='/view/replaceintent.do' />");	
			else
				comSubmit.setUrl("<c:url value='/view/editintent.do' />");
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnIntentDownload() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/downloadintentfile.do' />");			
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');			
			comSubmit.submit();
			fnpreventDefault(event);
		}			
	</script>
</head>
<body>	
	<form id="UploadForm" name="UploadForm" enctype="multipart/form-data" method="post">
		<div class="layer" id="layer">
			<div id="bg" class="bg"></div>		
			<div id="divUpload" class="popup uploadIntents" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
				<div class="title">Upload Entry</div>
				<div class="body">
					<div class="popFile">
						<div class="ex">
							<ul>
								<li>*UTF-8 형식의 텍스트 파일로 업로드 요망.</li>
								<li>*작성하지 않은 Entities가 포함되어 있으면 저장되지 않습니다.</li>
							</ul>
						</div>
						<div><input type="file" id="fupload" name="fupload" /></div>
					</div>
				</div>
				<div class="btnBox">
					<div onclick="fnUpLoadIntent();" class="upload">Upload</div>
					<div onclick="fnLayerPop('divUpload', 'c');" class="cancle">Cancel</div>
				</div>
			</div>
		</div>
	</form>
	
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW entities intents">
			<div class="btnBox">
				<c:choose>
					<c:when test="${USER_TYPE ne 'OA'}">
						<div onclick="fnIntentDownload();"  class="downloadIn">Download Intent</div>					
						<c:choose>
							<c:when test="${AGENT_SEQ != null && AGENT_SEQ != 0}">							
								<div onclick="fnLayerPop('divUpload', 'o');" class="uploadIn">Upload Intent</div>
								<div onclick="fnOpenCopyIntent();"  class="downloadIn">Copy Intent</div>											
							</c:when>
							<c:otherwise>
							<div onclick="fnLayerPop('divUpload', 'o');" class="uploadIn">Upload Intent</div>
								<div onclick="fnGoREIntent('R');" class="replaceIn">Replace Intent</div>
								<div onclick="fnGoREIntent('E');" class="editIn">Edit Intent</div>																
							</c:otherwise>				
						</c:choose>	
					</c:when>
					<c:otherwise>
						<div onclick="fnIntentDownload();"  class="downloadIn">Download Intent</div>					
						<div onclick="fnLayerPop('divUpload', 'o');" class="uploadIn">Upload Intent</div>												
					</c:otherwise>
				</c:choose>
				<div onclick="fnOpenIntention();" class="createIn">의도 Slot 등록</div>				
				<div onclick="fnOpenCreatDatype();" class="createIn">Create Intent</div>										
			</div>
	
			<c:set var="AgentName" value=""></c:set>
			<c:set var="CopyCheck" value="0"></c:set>
			<c:forEach items="${list }" var="row">
				<c:choose>
					<c:when test="${null ne param.AGENT_SEQ && row.COPY_FLAG == 'Y' && CopyCheck == '0' }">
						<c:if test="${AgentName ne ''}">
								</ul>
							</div>
						</div>							
						</c:if>	
						<div class="liBox liBox4">
						<div class="title">Copied</div>
							<div class="ulBox">
								<ul>
						<c:set var="CopyCheck" value="1"></c:set>
					</c:when>
					<c:when test="${AgentName ne row.AGENT_NAME && row.COPY_FLAG == 'N'}">
						<c:if test="${AgentName ne '' or CopyCheck ne '0'}">
								</ul>
							</div>
						</div>							
						</c:if>					
						<div class="liBox liBox4">
						<div class="title">${row.AGENT_NAME}</div>
							<div class="ulBox">
								<ul>
						<c:set var="AgentName" value="${row.AGENT_NAME}"></c:set>
					</c:when>									
				</c:choose>	
				<li onclick="fnGoDetail(${row.SEQ_NO}, ${row.AGENT_SEQ});"><div>${row.INTENT_NAME }</div>
						<span onclick="fnDelete(${row.SEQ_NO});" class="remove po_a_rt"></span>
						<input type="hidden" id="hid${row.SEQ_NO}" value='${row.INTENT_NAME }' />
					</li>
			</c:forEach>
			<c:if test="${AgentName ne '' or CopyCheck ne '0'}">
					</ul>
				</div>
			</div>							
			</c:if>
		</div>
	</div>	
</body>
</html>