<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
<script type="text/javascript">
		var $WorkhidObj;
		var blockvalue = '';
		var UserSayRange;
		var arrSlot = new Array();
		var arrSlotList = new Array();
		var arrIntentionNamelist = new Array();
		var arrIntentionSlotlist = new Array();
		var iSeqNo = '';
		var jsondata = jQuery.parseJSON('${INTENTS_LIST}'.replace(/=\"/g, '=').replace(/\"\)/g, ')'));
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var slotdata2 = jQuery.parseJSON('${SLOT_LIST2}');
		var slotstr = '';
		var $autoMapobj;		
		var CheckCnt;
		var CheckCntMax;
		var pop;
		
		window.onload = function() {
			if('${SEQ_NO}' == '') 
				fnSetNaviTitle('Create Intent');
			else {
				fnSetNaviTitle('Intents > ${INTENT_NAME}');
				
				if ('${SLOT_SEQ_NO}' != '' || '${INTENTION}' != '') {
					$('#divreplies').hide();
				}
				else {
					if ('${INTENT_NAME}'.indexOf('request') == 0) {
						fnRequestView();
						
						var temp = '${INTENT_NAME}'.split('(');
						if (temp.length > 1) {
							$('#txtIntentsName').val(temp[0]);
							$('#txtIntentsRequest').val(temp[1].substring(0, temp[1].length -1));	
						}
						else
							$('#txtIntentsName').val(temp[0]);
					}
					else {
						$('#txtIntentsName').val('${INTENT_NAME}');	
					}
				}
			}
			
			$('#divUserSays').on({				
				'click focusin': function(e) {
					if($(this).parent().next().length == 0 && $(this).text() != '') {
						AddRow();
					}
					
					fnstopPropagation(e);
				},
				mouseup: function(e) {
					blockvalue = window.getSelection().toString();
					if(!gfn_isNull(blockvalue)) {												
						UserSayRange = window.getSelection().getRangeAt(0);
						var calpos = UserSayRange.endOffset - UserSayRange.startOffset;					
						if (calpos != blockvalue.length) {
							alert('이미 태깅 된 텍스트는 다시 태깅할 수 없습니다.');
							return;
						}
						$WorkhidObj = $(this).next();					
						var comAjax = new ComAjax();
						comAjax.setUrl("<c:url value='/view/getslotobjectlist.do' />");
						comAjax.setCallback("fnSlotSearchCallBack");
						comAjax.addParam("OBJECT_NAME", blockvalue);
						comAjax.setAsync(false);
						comAjax.ajax();
					}
					fnpreventDefault(e);
				},
				focusout: function(e) {
																							
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().prev().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 40) {
						$(this).parent().next().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {

					} else if(e.keyCode == 13) {
						$this = $(this);
						if($this.parent().next().length == 0 && $this.text() != '') {
							AddRow();
						}
						$this.parent().next().children().eq(0).focus();
						return false;
					} else {
						bWorkChk = true;
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=USER_SAYS]');		
						
			$('#divreplies').on({
				focusout: function(e) {
					$this = $(this);
					var orgval;
					var $parentobj = $this.parent().parent();					
					
					if ($this.text().toLowerCase().indexOf('request') > -1) {											
						if($parentobj.find('[name=OBJECT_REQUEST]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
							$parentobj.find('[name=mainli]').html($('#divRequestAdd').html());
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').attr('name', 'OBJECT_REQUEST_PATTERN');							
							$parentobj.append('<div class="input input2_style" name="none_obj" style="display:block;"><div class="title">다음 사용자 발화 DA 제한</div><input class="input2_con_style" type="text" name="LIMIT_DA" /></div>');
						}
					}
					else {
						if($parentobj.find('[name=OBJECT_DETAIL_NAME]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
							$parentobj.find('[name=mainli]').html($('#divInformAdd').html());
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').attr('name', 'OBJECT_DETAIL_NAME');
							$parentobj.find('.input2_style').remove();
						}
					}
				}		
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {						
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
					//fnOpenPop(this, 'S');
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
														
					if ($this.text() != '') {
						fnAddSubObject($this, 'C', 'P');
					}
				},
				change: function(e) {
					bWorkChk = true;
				},
				keydown: function(e) {
					if(e.keyCode == 9) {
						$this = $(this);
						if ($this.text() != '') {
							fnAddSubObject($this, 'T', 'P');
						}						
						fnpreventDefault(e);
					}
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
					
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					if ($this.text() != '') {
						fnAddSubObject($this, 'C', 'T');
					}
				},
				focusout: function(e) {
					var str = $(this).text();
					var arrvalidation = fnSlotValidation(str, slotdata);
					if (!arrvalidation[0]) {
						var arrtempval = new Array();
						for (var i = 1; i < arrvalidation.length; i++) {
							arrtempval.push(arrvalidation[i]);
						}
						alert('존재 하지 않는 슬롯입니다.\n(' + arrtempval.join(', ') + ')');
					}
				},
				keydown: function(e) {
					if(e.keyCode == 38) {
						//$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						//$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {											
						$this = $(this);
						if ($this.text() != '') {
							fnAddSubObject($this, 'T', 'T');
							/* if($this.parent().parent().next().get(0).tagName == 'DIV') {
								$this.parent().parent().parent().append($('#divDetailSub').html());								
							}	
							fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0)); */
						}						
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {
					var $this = $(this);
					$this.parent().children().removeClass('on');
					$this.addClass('on');
					
					var idx = $this.index();
					$this.parent().parent().next().find('.fire').hide();
					$this.parent().parent().next().find('.fire').eq(idx).show();				
				}
			}, '.fireMenu ul li');
			
			$('#divreplies').on('click', '.close_btn', function(e) {				
				$(this).prev().val('');
				$(this).prev().prev().text('');
				bWorkChk = true;
				fnpreventDefault(e);
			});
				
			$('#txtIntentsName').bind({
				'change' : function() {
					bWorkChk = true;					
				},
				'keyup' : function(e) {
					$this = $(this);
					if ($this.val().indexOf('request') == 0) {
						$this.val($this.val().replace(/[\(\)\sㄱ-힣]/g, ''));						
						fnRequestView();
					}
					else {
						$this.css('width', '100%');
						$('#txtIntentsRequest').val('').hide();	
					}
					
					if ('${SEQ_NO}' != '') {
						$('#spWarning').show();
					}
				} 				
			});
			
			$('#txtIntentsRequest').click(function() {
				pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />?Type=IR&id=txtIntentsRequest", "SlotSearchpop", 430, 550, 0, 0);
				
				if ('${SEQ_NO}' != '') {
					$('#spWarning').show();
				}
			});
			
			<c:forEach items="${SlotList}" var="row">
				var objSlot = new Object();
				objSlot.seqno = '${row.SEQ_NO}';
				objSlot.slotname = '${row.SLOT_NAME}';
				objSlot.color = '${row.COLOR}';
				
				arrSlotList['${row.SLOT_NAME}'] = objSlot;					
			</c:forEach>
			
			<c:forEach items="${intlist}" var="row">
				var Islotvalue = '${row.INTENTION_SLOT}' + '="' + '${row.INTENTION_SLOT_VALUE}' + '"';
				var objIname = new Object();
				objIname.slotvalue = Islotvalue;
				objIname.slotname = '${row.INTENTION_SLOT}';
				arrIntentionNamelist['${row.INTENTION_NAME}'] = objIname;
				
				var objISlot = new Object();
				objISlot.intentionname = '${row.INTENTION_NAME}';
				arrIntentionSlotlist[Islotvalue] = objISlot;					
			</c:forEach>			
			
			if ('${param.UPLOAD_STATUS}' == 'F') {
				if ('${param.STATUS}' == 'NS')
					alert('Entities에서 작성하지 않은 슬롯이 포함되어 있습니다.');
				else if ('${param.STATUS}' == 'UC')
					alert('한번에 업로드 할 수 있는 사용자 발화 수는 100개 입니다.');
				else if ('${param.STATUS}' == 'TC')
					alert('저장 가능한 사용자 발화 수를 초과 했습니다.');
				else
					alert('파일 형식이 잘못되었습니다.');				
			}
			
			fnSelectList();			
		}		
		
		$(window).bind("beforeunload", function (e){
			if (!gfn_isNull(pop)) {
				pop.close();
			}
		});
		
		function fnRequestView() {
			$('#txtIntentsName').css('width', '70%');
			$('#txtIntentsRequest').show();
		}
		
		function fnSelectList() {
			var wheresql = '';			
		
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getusersaylist.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX", $("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);			
			comAjax.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
			comAjax.addParam("INTENTION", '${INTENTION}');
			comAjax.addParam("INTENT_SEQ", '${SEQ_NO}');
			
			if ('${SLOT_SEQ_NO}' != '' && '${INTENTION}' != '') {
				wheresql = 'AND SLOT_SEQ_NO = \'${SLOT_SEQ_NO}\' AND INTENTION = \'${INTENTION}\' ';
			}
			else if ('${SLOT_SEQ_NO}' != '') {
				wheresql = 'AND SLOT_SEQ_NO = \'${SLOT_SEQ_NO}\' AND INTENTION = \'\' ';
			}
			else if ('${INTENTION}' != '') {
				wheresql = 'AND INTENTION = \'${INTENTION}\' ';
			}	
			
			if ($('#txtSearch').val() != '') {
				wheresql = 'AND SLOT_TAG like \'%' + $('#txtSearch').val() + '%\''
			}
			
			comAjax.addParam("WHERE", wheresql);					
			comAjax.ajax();
			fnpreventDefault(event);			
		}
		
		function fnSelectListCallBack(data) {
			var iCnt = 0;
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			$("#divUserSays > li").remove();;
			$("#TOTAL_COUNT").val(total);
			
			if(total != 0){
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var cnt = 0;
				var str = "";
				$.each(data.list, function(key, value){									
					str = "";
					str += '<li>';					
					if ('${SLOT_SEQ_NO}' == '' && '${INTENTION}' == '') {
						str += '<div class="user_says_add_con_inp" id="' + value.SEQ_NO + '" name="EDIT_USER_SAYS">' + value.SLOT_TAG + '</div>';
						str += '<span onclick="fnUserSayDelete(\'' + value.INTENTION + '\', \'' + value.SLOT_SEQ_NO + '\', ' + value.SEQ_NO + ', this);" class="remove"></span>';
						if (value.SLOT_SEQ_NO != '' || value.INTENTION != '') {
							str += '<span onclick="fnUserSayDetailList(\'' + value.SLOT_SEQ_NO + '\', ${SEQ_NO }, \'' + value.INTENTION + '\');" class="list_btn"></span>';
							str += '<input type="hidden" name="SlotSeqNo" value="' + value.SLOT_SEQ_NO + '" />';
						}
						else {
							str += '<span onclick="fnUserSayEdit(' + value.SEQ_NO  + ', this);" class="modify"></span>';
							str += '<span onclick="fnSlotMapping(this);" class="mapping_btn" style="display:none;"></span>';
							str += '<span onclick="fnUserSayClear(this);" class="undo_btn"></span>';
						}
						str += '<span onclick="fnIntentionPop(this);" class="intention_btn"></span>';
						str += '<div class="intent_title">' + value.INTENTION + '</div>';
					}
					else {
						str += '<div class="user_says_add_con_inp" id="' + value.SEQ_NO + '" name="EDIT_USER_SAYS">' + value.SAY_TAG + '</div>';
						str += '<input type="hidden" name="hidUserSayEdit" value=\'' + value.SLOT_MAPDA + '\' />';
						str += '<span onclick="fnUserSayEdit(' + value.SEQ_NO + ', this);" class="modify"></span>';						
						str += '<span onclick="fnSlotMapping(this);" class="mapping_btn" style="display:none;"></span>';												
						str += '<span onclick="fnUserSayClear(this);" class="undo_btn"></span>';
						str += '<span onclick="fnUserSayDelete(\'\', \'\', ' + value.SEQ_NO + ', this);" class="remove"></span>';
						str += '<input type="hidden" name="hidUserSayEdit" value=\'' + value.SLOT_MAPDA + '\' />';
						if (cnt == 0) {
							fnSetIntentsName('${INTENT_NAME}', value.SLOT_NAME);
							cnt++;
						}
						str += '<span onclick="fnIntentionPop(this);" class="intention_btn"></span>';
						str += '<div class="intent_title">' + value.INTENTION + '</div>';
					}
					
					str +='</li>';									
					$('#divUserSays').append(str);
				});						
			}
			AddRow();
		}
		
		function fnIntentionPop(obj) {
			$this = $(obj).parent();
			if ($this.find('.list_btn').length == 0) {						
				if ($this.find('.USER_SAYS').length == 1) {
					$('#divUserSays .USER_SAYS').removeAttr('id');
					$this.find('.USER_SAYS').attr('id', 'temp_usersayid');
					fnOpenPop($this.get(0), 'IN1');
				}
				else							 
					fnOpenPop($this.get(0), 'IN2');
			}
			else
				fnOpenPop($this.get(0), 'IN3');
		}
		
		function fnAddSubObject(obj, type, cate) {
			var $this = obj;
			if (cate == 'T') {
				if ($this.parent().parent().attr('name') == 'mainli') {
					$ul = $this.parent().parent().next().find('ul');
					if ($ul.find('li').length == 0) {
						$ul.append($('#divDetailSub').html());	
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('ul').eq(0).find('div > div').get(0));
					}
				}
				else {
					if ($this.parent().parent().index() == $this.parent().parent().parent().find('li').length -1) {
						$this.parent().parent().parent().append($('#divDetailSub').html());
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0));
					}
				}
			}
			else {
				if ($this.parent().parent().parent().attr('name') == 'mainli') {
					$ul = $this.parent().parent().parent().next().find('ul');
					if ($ul.find('li').length == 0) {
						$ul.append($('#divDetailSubP').html());	
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().parent().next().find('ul').eq(0).find('div > div').get(0));
					}
				}
				else {
					if ($this.parent().parent().index() == $this.parent().parent().parent().find('li').length -1) {
						$this.parent().parent().parent().append($('#divDetailSubP').html());
					}
					
					if (type == 'T') {
						fnCursorEnd($this.parent().parent().next().find('div > div').eq(0).get(0));
					}
				}
			}
		}
		
		function fnColGroupChange(seqno) {
			$('#tbcolg' + seqno).html('<col width="120px" /><col width="*" /><col width="110px" />');			
		}
		
		function AddRow() {
			$('#divUserSays').append($('#divAdd').html());
		}
				
		function fnSlotSearchCallBack(data) {
			var divTop;
			if ($('#wrap').scrollTop() == 0) 				
				divTop = event.pageY + 10; //상단 좌표 위치 안맞을시 e.pageY
			else
				divTop = event.pageY + $('#wrap').scrollTop(); //상단 좌표 위치 안맞을시 e.pageY
				
			var divLeft = event.pageX; //좌측 좌표 위치 안맞을시 e.pageX
			
			slotstr = '';
			if(data.list.length > 0) {
				$.each(data.list, function(key, value){					
					slotstr += '<a href="#" onclick="fnSetSlot(' + value.SEQ_NO + ', \'' + value.CLASS_NAME + '.' + value.SLOT_NAME + '\', \'' + value.COLOR + '\', true);">' + value.CLASS_NAME + '.' + value.SLOT_NAME + '</a><br/>';
					
					if (value.PARENT_CLASS_SEQ_NO != 0) {
						fnGetNotUsingClass(value.CLASS_SEQ_NO, value.SEQ_NO, value.SLOT_NAME, value.COLOR);
					}
				});
			}
			else
				slotstr = '일치하는 Slot이 없습니다.';
			
			slotstr += '<br /><a href="#" onclick="fnOpenSlot();">&lt;직접선택&gt;</a>';
			
			$('#divSlotLayerCon').empty().append(slotstr);
			$('#divSlotLayer').css({
			     "top": divTop
			     ,"left": divLeft
			     ,"width": "auto"
			     ,"background": "#ffffff"
			     ,"z-index": "1"
			     , "position": "absolute"
			}).show();
		}
		
		function fnGetNotUsingClass(classSeq, slotSeq, slotName, slotColor) {
			$('#hidSlotAdd').val(classSeq + '|' + slotSeq + '|' + slotName + '|' + slotColor)
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", classSeq);			
			comAjax.setUrl("<c:url value='/view/getparentclass.do' />");			
			comAjax.setCallback("fnGetNotUsingClassCallBack");
			comAjax.setAsync(false);
			comAjax.ajax();
		}
		
		function fnGetNotUsingClassCallBack(data) {
			try {
				var arrobj = $('#hidSlotAdd').val().split('|');
				var slotname = '';
				var slotseq = '';
				$.each(data.list, function(key, value1){					
					$.each(slotdata2[value1.CLASS_SEQ_NO], function(key, value){
						if (value1.SLOT_SEQ_NO == value.SEQ_NO) {
							slotname = value1.SLOT_NAME + '.' + arrobj[2];
							slotseq = value1.SUB_SLOT_SEQ + '_' + arrobj[0] + '_' + arrobj[1];
							fnGetSlotColor(value.CLASS_SEQ_NO, slotname, "<c:url value='/view/getslotcolor.do' />");
							slotstr += '<a href="#" onclick="fnSetSlot(\'' + value.CLASS_SEQ_NO + '_' + slotseq + '\', \'' + value.CLASS_NAME + '.' + slotname + '\', \'' + $('#hidSlotColor').val() + '\', true);">' + value.CLASS_NAME + '.' + slotname + '</a><br />';
							$('#hidSlotColor').val('');
						}
					});
				});
				$('#hidSlotAdd').val('');
			} catch (e) {
				// TODO: handle exception				
			}
		}				
		
		function fnSetSlot(seqno, slotname, color, bevent) {			
			var spos = UserSayRange.startOffset;
			var epos = UserSayRange.endOffset;
			var content = UserSayRange.startContainer.textContent;
			var text = UserSayRange.toString();
						
			if (text[text.length - 1] == ' ') {
				epos --;
				text = text.substring(0, text.length -1);
			}
			
			var $WorkdivView = $WorkhidObj.prev(); 
			
			var replaceViewHtml = fnReplaceSpace($WorkdivView.html());			
			var sindex = replaceViewHtml.indexOf(content);
			if (sindex + spos > 0) {
				if (replaceViewHtml.substring(sindex + spos - 1, sindex + spos) == '>' && replaceViewHtml.substring(sindex + epos, sindex + epos + 1) == '<') {
					alert('이미 태깅된 단어를 다시 태깅할 수 없습니다.\n우측 되돌리기 버튼을 이용하여 초기화 후 태깅 해 주세요.');
					fnSlotClose();
					fnCursorEnd($WorkdivView.get(0));
					return;
				}
			}
			$WorkdivView.html(replaceViewHtml.substring(0, sindex + spos) + '<span style="color:' + color + '">' + text + '</span>' + replaceViewHtml.substring(sindex + epos));
			var objSlot = new Object();
			objSlot.seqno = seqno.toString();			
			objSlot.slotname = slotname;
			objSlot.color = color;
			objSlot.text = text;
			
			arrSlot[text + '_' + color] = objSlot;
			bWorkChk = true;
		
			fnSlotClose();
			if (bevent) {
				fnpreventDefault(event);	
			}
			
			fnCursorEnd($WorkdivView.get(0));
		}
		
		function fnTagDeep(obj) {
			var returnval = ''
			$(obj).each(function() {
				if($(this).find('span').length == 0) {
					returnval = $(this).text(); 
				}
				else 
					returnval = fnTagDeep(this);
			});
			return returnval;
		}
		
		function fnSlotClose() {
			$('#divSlotLayer').hide();
		}
	
		function fnResponseAdd() {
			if ($('#divreplies').find('[name=wtastas]').length == 15) {
				fnpreventDefault(event);
				return false;
			}
			
			$('#divreplies').append($('#divrepliesAdd').html().replace(/§/g, $('#divreplies [name=wtastas]').length));			
			fnpreventDefault(event);
		}
		
		function fnResponseCancel(type) {
			$.modal.close();
			$("#divResponse").hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj, type) {
			if (type == 'F') {
				var width = '713';
				var heigth = '626';
				if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
					width = '613';
					heigth = '400';	
				}
				
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id') + '&flag=R', "IntentsPop", width, heigth, 0, 0);	
			}
			else if (type == 'A') {
				pop = fnWinPop("<c:url value='/view/openactionwrite.do' />" + "?obj=" + $(obj).parent().find('input').attr('id'), "IntentsPop", 570, 555, 0, 0);	
			}		
			else if (type == 'S') {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id') + "&Type=TS", "AgentSearchpop", 430, 550, 0, 0);
			}
			else if (type == 'IN1') {
				pop = fnWinPop("<c:url value='/view/openintention.do' />" + "?obj=" + $(obj).find('.USER_SAYS').attr('id') + "&intention=" + encodeURI(encodeURIComponent($(obj).find('.intent_title').text())), "IntentionSearchpop", 430, 550, 0, 0);
			}
			else if (type == 'IN2') {
				pop = fnWinPop("<c:url value='/view/openintention.do' />" + "?obj=" + $(obj).find('.user_says_add_con_inp').attr('id') + "&intention=" + encodeURI(encodeURIComponent($(obj).find('.intent_title').text())), "IntentionSearchpop", 430, 550, 0, 0);
			}
			else if (type == 'IN3') {
				pop = fnWinPop("<c:url value='/view/openintention.do' />" + "?obj=" + $(obj).find('.user_says_add_con_inp').attr('id') + "&intention=" + encodeURI(encodeURIComponent($(obj).find('.intent_title').text())) + "&Type=G&SlotSeq=" + $(obj).find('input:hidden').val() + '&IntentSeq=${SEQ_NO}' , "IntentionSearchpop", 430, 550, 0, 0);
			}
			else if (type == 'IT') {
				pop = fnWinPop("<c:url value='/view/selectintention.do' />?Type=W" , "IntentionSelect", 900, 400, 0, 0);
			}
		}
		
		function fnOpenSlot() {
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=&Type=U", "AgentSearchpop", 430, 550, 0, 0);
		}
		
		function fnSave() {			
			if ($('#divLoading').css('display') == 'block') {
				alert('저장 중 입니다.');
				return;
			}			
		
			var bNameChk = false;
			var bIntentSaveChk = false;
			var focusObj;
			
			$txtIntentsName = $('#txtIntentsName');
		
			if ('${SLOT_SEQ_NO}' == '') {							
				if ($txtIntentsName.val() == 'Intent name' || $txtIntentsName.val().trim() == '') {
					alert('Intent명을 입력 하세요.');
					$txtIntentsName.focus();
					fnpreventDefault(event);
					return false;
				}
				
				if ($txtIntentsName.val().length > 50) {
					alert('Intent 명을 50자 이하로 입력해 주세요.');
					$txtIntentsName.focus();
					fnpreventDefault(event);
					return false;
				}
				//var reg = /[a-zA-Z0-9_\-]+\([a-zA-Z0-9,._\-\s]+="[a-zA-Z0-9ㄱ-힣]+"\)|[^a-zA-Z0-9_\-=()."]/;
				var reg = /[^a-zA-Z0-9_\-=()."]/;
				if (reg.test($txtIntentsName.val())) {
					alert('Intent명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$txtIntentsName.focus();
					fnpreventDefault(event);
					return false;
				}			
			
				if ($('#txtIntentsRequest').val().trim() != '') {
					var tempintentnm = $txtIntentsName.val() + '(' +  $('#txtIntentsRequest').val() + ')'								
					$txtIntentsName.val(tempintentnm);
				}			
			}
			
			if (jsondata[${AGENT_SEQ}] != undefined) {
				var str = '';		
				$.each(jsondata[${AGENT_SEQ}], function(key, value){
					if('${SEQ_NO}' == '' && iSeqNo == '') {
						if (value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else if (iSeqNo != ''){
						if (value.SEQ_NO.toString() != iSeqNo && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else {
						if (value.SEQ_NO.toString() != '${SEQ_NO}' && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
				});							
			}
			
			if (bNameChk) {
				alert('이미 존재하는 Intent명 입니다.');
				fnpreventDefault(event);
				return false;
			}
					
			fnLoading();
		
			var objusersay = new Object();			
			var arrusersay = new Array();
			var bSaveChk = true;
			
			$('#divUserSays [name=USER_SAYS]').each(function(idx, e) {
				$ObjUserSay = $(this);
				if(!gfn_isNull($ObjUserSay.text())) {
					var usitem = new Object();
					usitem.SAY = $ObjUserSay.text();
					usitem.SAY_TAG = fnReplaceSpace($ObjUserSay.html());
					if ('${INTENTION}' == '')
						usitem.INTENTION = $ObjUserSay.parent().find('.intent_title').text();
					else {
						if ($ObjUserSay.parent().find('.intent_title').text() == '')
							usitem.INTENTION = '${INTENTION}';
						else
							usitem.INTENTION = $ObjUserSay.parent().find('.intent_title').text();						
					}
						
					if (usitem.SAY_TAG.indexOf('<span style="color: rgb') == 0) {
						
						var tempsaytag = usitem.SAY_TAG.replace(/<span [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>/g, '');
						if (tempsaytag.substring(tempsaytag.length -7) == '</span>') {
							tempsaytag = tempsaytag.substring(0, tempsaytag.length -7);
							usitem.SAY_TAG = tempsaytag; 
						}
					}
									
					var slotarray = new Array();					
					var arrslotseq = new Array();
					var arrslotseqno = new Array();
					var arrslotname = new Array();
					var arrslotmapda = new Array();
					var slottag = usitem.SAY_TAG;
					var slotmap = usitem.SAY_TAG;					
					
					try {
						$(this).find('span').each(function() {
							var objslotseq = new Object();
							var text = $(this).text();//fnTagDeep(this);					
							var arr = arrSlot[text + '_' + fnHexc($(this).css('color'))];
							/* if (arr.seqno.toString().indexOf('_') > -1)
								arrslotseq.push(Number(arr.seqno.toString().split('_')[1]));	
							else
								arrslotseq.push(arr.seqno); */
							if (arr == undefined) {
								//slottag = usitem.SAY;
								//slotmap = usitem.SAY;
								return;
							}
								
							if (slottag.indexOf($(this).css('color')) > -1) {
								slottag = slottag.replace($(this).css('color'), fnHexc($(this).css('color')));
								slotmap = slotmap.replace($(this).css('color'), fnHexc($(this).css('color')));
								usitem.SAY_TAG = usitem.SAY_TAG.replace($(this).css('color'), fnHexc($(this).css('color')));
							}
							
							arr.seqno = arr.seqno.toString();
							objslotseq.SLOT_SEQ_NO = arr.seqno;
							if (arr.seqno.indexOf('_') > -1)
								objslotseq.SEQ_NO = arr.seqno.substring(arr.seqno.lastIndexOf('_') + 1);
							else
								objslotseq.SEQ_NO = arr.seqno;
							
							arrslotseqno.push(objslotseq);
							arrslotseq.push(arr.seqno);							
							arrslotname.push(arr.slotname + '=value');
							arrslotmapda.push(arr.slotname + '="' + text.trim() + '"');
							slottag = slottag.replace('>' + text + '</', '>@' + arr.slotname + '</');
							var reg = new RegExp('<span style=\"color:\\s?#\\w+;?\">' + text.replace('+', '\\+').replace('|', '\\|').replace('(', '\\(').replace(')', '\\)').replace('[', '\\[').replace(']', '\\]') + '<\/span>','g');
							slotmap = slotmap.replace(reg, '<' + arr.slotname + '=' + text.trim() + '>');
						});	
					} catch (e) {
						bSaveChk = false;
						fnLoading();
						alert('사용자 발화 저장 중 [' + usitem.SAY + '] 문장에서 문제를 발견 하였습니다. 문장을 다시 작성해주세요.');
						return;
					}
					
					if (arrslotseqno.length > 0) {
						arrslotseqno.sort(function(a, b) { // 오름차순
						    return a.SEQ_NO - b.SEQ_NO;
						});
						
						arrslotseq = new Array();
						
						for (var i = 0; i < arrslotseqno.length; i++) {
							arrslotseq.push(arrslotseqno[i].SLOT_SEQ_NO);
						}
						
						if ($ObjUserSay.parent().find('.intent_title').text() != '') {
							var arrilist = $ObjUserSay.parent().find('.intent_title').text().split(' ');
						
							for (var i = 0; i < arrilist.length; i++) {
								if (arrIntentionNamelist[arrilist[i]] != undefined) {
									for (var j = 0; j < arrslotname.length; j++) {
										if (arrslotname[j].indexOf(arrIntentionNamelist[arrilist[i]].slotname) == 0) {											
											bSaveChk = false;
											fnLoading();
											alert('사용자 발화 저장 중 [' + usitem.SAY + '] 문장에서 문제를 발견 하였습니다.\n의도 Slot과 태깅된 Slot이 중복 됩니다.');
											return;
										}
									}
								}
							}
						}						
					}
					else
						arrslotseq.sort();					
					
					usitem.SLOT_SEQ_NO = arrslotseq.toString();
					usitem.SLOT_TAG = slottag;
					usitem.SLOT_NAME = arrslotname.toString();
					usitem.SLOT_MAP = slotmap;
					
					if (usitem.SLOT_MAP == '') {
						usitem.SLOT_MAP = usitem.SAY; 
					}
					
					if ($ObjUserSay.attr('id') == 'temp_usersayid') {
						$ObjUserSay.removeAttr('id');
					}
					
					if(gfn_isNull($ObjUserSay.attr('id')))
						usitem.SEQ_NO = '0';
					else
						usitem.SEQ_NO = $ObjUserSay.attr('id');
					
					usitem.SLOT_MAPDA = arrslotmapda.toString().replace(/,/g, ', ');
					arrusersay.push(usitem);					
				}				
			});
			
			if (arrusersay.length > 0) {
				var comAjax = new ComAjax();
				comAjax.addParam("TABLE", "intents_user_says");
				comAjax.addParam("WHERE", "INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = " + getCookie('ProjectNo') + ")");
				comAjax.addParam("COUNT_NAME", "USERSAY_COUNT");
				comAjax.setUrl("<c:url value='/view/getcountcheck.do' />");			
				comAjax.setCallback("fnCountCheckCallBack");
				comAjax.setAsync(false);
				comAjax.ajax();				
							
				if ((CheckCnt + arrusersay.length) > CheckCntMax) {				
					alert('저장 가능한 사용자 발화 수를 초과 했습니다.');
					fnLoading();
					bSaveChk = false;
					return;
				}				
			}
		
			if (bSaveChk) {
				objusersay.USER_SAYS = arrusersay;
				var usersayobj = JSON.stringify(objusersay);
				var bRequestChk = false;
				var arr = new Object();
				var mainarrobj = new Array();
				var cnt = 0;
				var rcnt = 0;
				
				$('#divreplies [name=wtastas]').each(function(idx, e) {							
					var mainitem = new Object();
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();				
					mainitem.SLOT_SEQ_NO = '${SLOT_SEQ_NO}';
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';

					var arrobj = new Array();
					
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						cnt = 0;
						rcnt = 0;
						var item = new Object();
						var arrobjsub = new Array();
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();										
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'inform()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bIntentSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}					
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text()) || mainitem.UTTR != 'true') {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);
									rcnt ++;
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});
							
							if (cnt > 0 && rcnt == 0) {
								focusObj = $this.find('[name=OBJECT_REQUEST]');
								bIntentSaveChk = true;
								bRequestChk = true;
								return false;
							}
						}
						else {
							$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
								if(!gfn_isNull($(this).text()) || (mainitem.UTTR != 'true' && cnt == 0)) {
									var subitem = new Object();
									subitem.TYPE = 'T';
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/(<\/div>\s*<div>|<\/p>)/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>|<p>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});		
						}
																
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;
							
							arrobj.push(item);							
						}	
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
					}
				});
				
				if (bIntentSaveChk) {
					fnLoading();
					if (bRequestChk)
						alert('질문 할 slot을 선택해 주세요.');
					else
						alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					focusObj.focus();
					return false;
				}
				
				arr.OBJECT_ITEM = mainarrobj;
				var jsoobj = JSON.stringify(arr);
				
				/* var comAjax = new ComAjax();				
				comAjax.setUrl("<c:url value='/view/insertintent.do' />");
				comAjax.setCallback("fnSaveComplete");
				comAjax.addParam("INTENT_NAME", $txtIntentsName.val());
				comAjax.addParam("USER_SAYS", usersayobj);
				comAjax.addParam("OBJECT_ITEM", jsoobj);
				comAjax.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				if('${SEQ_NO}' == '') {
					if (iSeqNo == '')
						comAjax.addParam("SEQ_NO", '');
					else
						comAjax.addParam("SEQ_NO", iSeqNo);
				}
				else
					comAjax.addParam("SEQ_NO", '${SEQ_NO}');
				comAjax.addParam("depth1", '${depth1}');
				comAjax.addParam("depth2", '${depth2}');
				comAjax.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comAjax.addParam("AGENT_SEQ", '0');
				else
					comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comAjax.ajax(); */
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertintent.do' />");
				comSubmit.addParam("INTENT_NAME", $txtIntentsName.val());
				comSubmit.addParam("USER_SAYS", usersayobj);
				comSubmit.addParam("OBJECT_ITEM", jsoobj);
				comSubmit.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				comSubmit.addParam("INTENTION", '${INTENTION}');
				if('${SEQ_NO}' == '') {
					if (iSeqNo == '')
						comSubmit.addParam("SEQ_NO", '');
					else
						comSubmit.addParam("SEQ_NO", iSeqNo);
				}
				else
					comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comSubmit.addParam("AGENT_SEQ", '0');
				else
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
				comSubmit.submit();			
				fnpreventDefault(event);
			}
		}				
		
		function fnUserSayDelete(Intention, SlotSeqNo, Seqno, obj) {			
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				if (Seqno != '') {
					var comAjax = new ComAjax();
					if (SlotSeqNo == '' && Intention == '') { 
						comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");
						comAjax.addParam("SEQ_NO", Seqno);
					}
					else {
						comAjax.setUrl("<c:url value='/view/deleteintentusersay.do' />");
						comAjax.addParam("INTENT_SEQ", '${SEQ_NO}');
						if (SlotSeqNo != '' && Intention != '') {
							wheresql = 'SLOT_SEQ_NO = \'' + SlotSeqNo + '\' AND INTENTION = \'' + Intention + '\'';
						}
						else if (SlotSeqNo != '') {
							wheresql = 'SLOT_SEQ_NO = \'' + SlotSeqNo + '\' AND INTENTION = \'\' ';
						}
						else if (Intention != '') {
							wheresql = 'INTENTION = \'' + Intention + '\' ';
						}
						comAjax.addParam("WHERE", wheresql);
					}
					
					comAjax.setAsync(false);
					comAjax.setCallback("");							
					comAjax.ajax();	
				}
				
				if ($(obj).parent().parent().find('li').length > 2) {					
					$(obj).parent().remove();
				}
				else {
					if($("#PAGE_INDEX").val() != '' && $("#PAGE_INDEX").val() != '1')						
						$("#PAGE_INDEX").val(Number($("#PAGE_INDEX").val()) - 1);
					fnSelectList();
				}				
			}
			fnpreventDefault(event);			
		}
		
		function fnUserSayEdit(Seqno, obj) {			
			if ('${SLOT_SEQ_NO}' != '') {
				var arrMapList = $(obj).prev().val().split(',');
				var sname;
				var tempidx;
				var text;
				
				for (var i = 0; i < arrMapList.length; i++) {
					arrMapList[i] = arrMapList[i].trim();
					sname = arrMapList[i].substr(0, arrMapList[i].indexOf("="));
					tempidx = arrMapList[i].indexOf("=") + 2;
					text = arrMapList[i].substr(tempidx, arrMapList[i].length - tempidx - 1);
					
					var objSlot = new Object();
					objSlot.seqno = arrSlotList[sname].seqno;
					objSlot.slotname = arrSlotList[sname].slotname;
					objSlot.color = arrSlotList[sname].color;
					objSlot.text = text;
					
					arrSlot[text + '_' + objSlot.color] = objSlot;
				}				
			}
			$(obj).hide();
			$(obj).next().show();
			$('#' + Seqno).attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($('#' + Seqno).get(0));
			
			if (!gfn_isNull(event)) {
				fnpreventDefault(event);			
			}
		}
		
		function fnUserSayClear(obj) {
			$(obj).parent().find('.mapping_btn').show();
			$(obj).parent().find('.intent_title').text('');
			$UsreSays = $(obj).parent().find('[name=USER_SAYS], [name=EDIT_USER_SAYS]');
			$UsreSays.html($UsreSays.text()).attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($UsreSays.get(0));	
			fnpreventDefault(event);			
		}
		
		/* function fnUserSayUndo(Seqno, obj) {
			$UsreSays = $('#' + Seqno);
			if ('${SLOT_SEQ_NO}' != '') {
				$UsreSays.html($UsreSays.text());					
			}
			$UsreSays.attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($UsreSays.get(0));
			
			fnpreventDefault(event);
		} */
		
		function fnUserSayDetailDelete(SeqNo, obj) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");
				comAjax.setCallback("");
				comAjax.addParam("SEQ_NO", SeqNo);
				comAjax.ajax();
				$(obj).parent().remove();
			}
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj, Type) {
			$obj = $(obj);
			if (SeqNo == 1) {
				$obj.parent().parent().remove();
			}
			else if (SeqNo == 2) {
				if (Type == 'T') {
					if ($obj.parent().attr('name') == 'mainli') {
						if ($obj.parent().next().find('li').length > 0) {
							$obj.parent().find('div > div').text($obj.parent().next().find('li').eq(0).find('div > div').text());
							$obj.parent().next().find('li').eq(0).remove();
						}
						else {
							if ($(obj).parent().parent().parent().parent().parent().find('[name=divSysIntent]').length > 1) {
								$(obj).parent().parent().parent().parent().remove();
							}
							else
								$obj.parent().find('div > div').text('');
						}
					}
					else
						$obj.parent().remove();
				}
				else {
					if ($obj.parent().parent().attr('name') == 'mainli') {
						if ($obj.parent().parent().next().find('li').length > 0) {
							$obj.parent().find('div > div').text($obj.parent().parent().next().find('li').eq(0).find('div > div').text());
							$obj.parent().parent().next().find('li').eq(0).remove();
						}
						else {
							if ($(obj).parent().parent().parent().parent().parent().parent().find('[name=divSysIntent]').length > 1) {
								$(obj).parent().parent().parent().parent().parent().remove();
							}
							else
								$obj.parent().find('div > div').text('');
						}
					}
					else
						$obj.parent().remove();
				}
			}
			
			bWorkChk = true;
			fnpreventDefault(event);
		}
		
		function fnObjectView(obj, type) {
			var $parentobj = $(obj).parent().parent();
			
			if (type == 1) {
				if ($parentobj.attr('class') != 'fireBox') {
					$parentobj.find('.delete').css('height', '47px');
					$parentobj = $parentobj.parent().find('.fireMenu');
				}
				else {
					$parentobj.find('.fireCont>.delete').css('height', '47px');
					$parentobj = $parentobj.find('.fireMenu');
				}
				
				$parentobj.parent().find('[name=none_obj]').show();
				$(obj).hide();
			}
			else {				
				$parentobj.find('.fireCont>.delete').css('height', '60px');
				$parentobj.find('.fireCont>.open').show();
				$parentobj.find('.fireCont>div').hide();
				$parentobj.find('.fireCont>div').eq(0).show();				
				$parentobj.find('[name=none_obj]').hide();
				$parentobj.find('.modify').show();
			}
							
			fnpreventDefault(event);
		}
		
		function fnUserSayDetailList(SlotNo, IntentSeq, Intention) {
			if (bWorkChk) {
				if (!confirm('작업된 내용이 있습니다. 저장하지 않고 이동 하시겠습니까?')) {
					return;
				}	
			}
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/useruttredit.do' />");			
			comSubmit.addParam("SLOT_SEQ_NO", SlotNo);
			comSubmit.addParam("INTENT_SEQ", IntentSeq);
			comSubmit.addParam("INTENT_NAME", '${INTENT_NAME}');
			comSubmit.addParam("INTENTION", Intention);
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();							
			/* var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getintentusersays.do' />");
			comAjax.setCallback("fnUserSayDetailListCallBack");
			comAjax.addParam("SLOT_SEQ_NO", SlotNo);			
			comAjax.ajax(); */	
		}
		
		/* function fnUserSayDetailListCallBack(data) {			
			if(data.list.length > 0) {
				var str = '<h3 class="user_says_icon">사용자 발화<a onclick="fnList();" class="class_name_instances" href="#">List</a></h3>';
				$.each(data.list, function(key, value){						
					str += '<div class="user_says_add_con">';																
					str += '<div class="user_says_add_con_inp" name="EDIT_USER_SAYS">' + value.SAY_TAG + '</div>';
					str += '<a href="#" onclick="fnUserSayDetailDelete(' + value.SEQ_NO + ', this);" class="delete_btn"><span class="hide">삭제</span></a>';					
					str += '</div>';
				});
				$('#divUserSaysDetail').empty().append(str).show();
				$('#divUserSays').hide();
			}
		} */
		
		function fnList() {
			$('#divUserSaysDetail').hide();
			$('#divUserSays').show();
		}
		
		function fnSetIntentsName(intentName, slotName) {
			var viewintentName = '';
		
			if (intentName.indexOf("(") > -1) {
				if (intentName.indexOf("(") + 1 == intentName.indexOf(")")) {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					if ('${INTENTION_SLOT}' == '')
						viewintentName = tempName + '(' + slotName + ')';
					else
						viewintentName = tempName + '(${INTENTION_SLOT}, ' + slotName + ')';
					
				}
				else {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					var tempMapda = intentName.substring(intentName.indexOf("(") + 1, intentName.indexOf(")"));
					
					if (slotName == '') {
						if ('${INTENTION_SLOT}' == '')
							viewintentName = intentName;
						else
							viewintentName = tempName + '(' + tempMapda + ', ' + '${INTENTION_SLOT})';
						
					}
					else {
						if ('${INTENTION_SLOT}' == '')
							viewintentName = tempName + '(' + tempMapda + ', ' + slotName + ')';
						else
							viewintentName = tempName + '(' + tempMapda + ', ${INTENTION_SLOT}, ' + slotName + ')';					
					}
				}
			}
			else {
				if ('${INTENTION_SLOT}' == '')
					viewintentName = intentName + '(' + slotName + ')';
				else {
					if (slotName == '') 
						viewintentName = intentName + '(${INTENTION_SLOT})';					
					else
						viewintentName = intentName + '(${INTENTION_SLOT}, ' + slotName + ')';
				}
				
			}
			$('#viewIntentsName').val(viewintentName);
		}
		
		function fnSlotMapping(obj) {							
			 $autoMapobj = $(obj).parent().find('[name=USER_SAYS]');
			 
			if ($autoMapobj.text().trim() == '') {
				alert('사용자 발화를 입력하세요.');
				fnCursorEnd($autoMapobj.get(0));
				return;
			}
			else
				$autoMapobj.html($autoMapobj.text());
			
			fnLoading();
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslotmapping.do' />");
			comAjax.setCallback("fnSlotMappingCallBack");			
			comAjax.addParam("UNIQID", '<%=session.getAttribute("UNIQID")%>');
			comAjax.addParam("UTTR", $autoMapobj.text().trim().replace(/\s/g, '|@'));
			comAjax.ajax();
			/* var data = new Object();
			data.status = "OK";
			data.slotmapping = '나는 치즈 피자 라지 사이즈|나는<Aa_class.aa_slot2=치즈> 피자 <Aa_class.aa_slot1=라지> 사이즈|test(Aa_class.aa_slot3="min", Aa_class.aa_slot2="치즈", Aa_class.aa_slot1="라지")|FINISHED|'; 
			fnSlotMappingCallBack(data);
			fnpreventDefault(event);	 */		
		}
		
		function fnSlotMappingCallBack(data) {
			fnLoading();
			if (data.status == 'OK') {	
				data.slotmapping = data.slotmapping.replace(/&LT;/g, '<').replace(/&GT;/g, '>').replace(/&LB;/g, '(').replace(/&RB;/g, ')').replace(/&EQ;/g, '=').replace(/&DQ;/g, '"').replace(/&AT;/g, '@').replace(/&SL;/g, '/') .replace(/&SH;/g, '#');				
				var arr = data.slotmapping.split('|');
				var intentnm = arr[2].substring(0, arr[2].indexOf('('));
				var intentslotlist = arr[2].substring(arr[2].indexOf('(') + 1, arr[2].indexOf(')'));
				var arrslot = arr[1].match(/<[a-zA-Z가-힣0-9_\-]+.[a-zA-Z가-힣0-9_\-]+=[\"`',%\+\/\\?~!@#$%^&*·;:=|\(\)\[\]{}a-zA-Z가-힣0-9.\s_\-]+>/g);
				
				if (arrslot == null || arrslot.length == 0) {
					if (intentslotlist == '') {
						alert('일치하는 slot이 없습니다.');	
					}
					else {
						var intentionlist = '';
						var arrilist = intentslotlist.replace(/\s/g, '').split(',');
						for (var j = 0; j < arrilist.length; j++) {							
							if (arrIntentionSlotlist[arrilist[j]] != undefined) {
								intentionlist += arrIntentionSlotlist[arrilist[j]].intentionname + ' ';
							}														
						}
						$autoMapobj.parent().find('.intent_title').text(intentionlist.trim());
						
						if($autoMapobj.parent().next().length == 0) {
							AddRow();
						}
					}
				}						
				else {			
					var bintentchk = true;				
					var alertmsg = '';
					var realintentname = ''; 
					var arritem;
					var arritemsub;
					
					realintentname = $('#txtIntentsName').val().trim();
					if (realintentname == 'Intent name' || realintentname == '')
						bintentchk = false;								
								
					if (bintentchk && realintentname.indexOf('()') > -1) {
						realintentname = realintentname.substring(0, realintentname.indexOf('()'));
					}
					
					arr[1] = arr[1].substring(0, arr[1].length);
					for (var i = 0; i < arrslot.length; i++) {
						arritem = arrslot[i].substring(1, arrslot[i].length - 1).split('=');
						arritemsub = arritem[0].split('.');
						
						$.each(slotdata[arritemsub[0]], function(key, value){
							if (value.SLOT_NAME == arritemsub[1]) {
								arr[1] = arr[1].replace(arrslot[i], '<span style="color:' + value.COLOR + '">' + arritem[1] + '</span>');
								
								var objSlot = new Object();
								objSlot.seqno = value.SEQ_NO;
								objSlot.slotname = arritem[0];
								objSlot.color = value.COLOR;
								objSlot.text = arritem[1];
								
								arrSlot[objSlot.text + '_' + objSlot.color] = objSlot;
								return false;
							}
						});
												
						var intentionlist = '';
						var arrilist = intentslotlist.replace(/\s/g, '').split(',');
						for (var j = 0; j < arrilist.length; j++) {							
							if (arrIntentionSlotlist[arrilist[j]] != undefined) {
								intentionlist += arrIntentionSlotlist[arrilist[j]].intentionname + ' ';
							}														
						}
						$autoMapobj.parent().find('.intent_title').text(intentionlist.trim());						
					}
					
					$autoMapobj.html(arr[1]);
					
					if($autoMapobj.parent().next().length == 0) {
						AddRow();
					}
					
					/* if (bintentchk) {
						if (realintentname.indexOf('(') > -1) {
							if (intentnm != realintentname.substring(0, realintentname.indexOf('('))) {
								alert('현재 intent로 인식되지 않습니다. 수동으로 작업하시기 바랍니다.');
							}	
						}
						else {
							if (intentnm != realintentname) {
								alert('현재 intent로 인식되지 않습니다. 수동으로 작업하시기 바랍니다.');
							}	
						}						
					}	 */			
				}
			}
		}
		
		function fnUpLoadIntentSubmit() {
			var agentSeq;
			var type;
			if ('${AGENT_SEQ}' == '' || '${AGENT_SEQ}' == '0') {
				type = 'AW';
				agentSeq = '0';
			}
			else
				type = 'TW';
			
			var comSubmit = new ComSubmit("UploadForm");
			comSubmit.setUrl('<c:url value='/view/insertusersayfile.do' />');
			comSubmit.addParam("TYPE", type);		
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", agentSeq);;		
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '') {
					comSubmit.addParam("SEQ_NO", '');
					alert('intent 저장 후 업로드 해주세요.');
					return;
				}
				else
					comSubmit.addParam("SEQ_NO", iSeqNo);
			}
			else
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			
			if ($('#txtIntentsRequest').val().trim() != '') {
				var tempintentnm = $('#txtIntentsName').val() + '(' +  $('#txtIntentsRequest').val() + ')'								
				comSubmit.addParam("INTENT_NAME", tempintentnm);
			}
			else {
				comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val());	
			}			
								
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();	
		}
		
		function fnSysIntentAdd(obj, type) {
			$(obj).parent().find('.fireCont > .systemfire').append($('#divSysintentAdd').html());
			
			fnpreventDefault(event);
		}
		
		function fnSysIntentDel(obj) {
			$parentobj = $(obj).parent().parent().parent().parent().parent().parent();
			//if ($parentobj.parent().find('[name=divSysIntent]').length > 1) {
				$parentobj.remove();	
			//}
				
			fnpreventDefault(event);
		}
		
		function fnIntentDownload() {
			if ('${SEQ_NO}' != '') {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/downloadintentfile.do' />");			
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
				if ($('#txtIntentsName').val().indexOf('(') > -1)
					comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val().substring(0, $('#txtIntentsName').val().indexOf('(')));
				else 
					comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val());
				comSubmit.submit();	
			}
			else {
				alert('Intent 저장 후 사용할 수 있습니다.');
			}
			
			fnpreventDefault(event);
		}		
	</script>
</head>
<body>
	<form id="UploadForm" name="UploadForm" enctype="multipart/form-data" method="post">
		<div class="layer" id="layer">
			<div id="bg" class="bg"></div>		
			<div id="divUpload" class="popup uploadIntents" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
				<div class="title">Upload Entry</div>
				<div class="body">
					<div class="popFile">
						<div class="ex">
							<ul>
								<li>*UTF-8 형식의 텍스트 파일로 업로드 요망.</li>
								<li>*이름이 같지 않은 intents는 저장되지 않습니다.</li>
								<li>*작성하지 않은 Entities가 포함되어 있으면 저장되지 않습니다.</li>
							</ul>
						</div>
						<div><input type="file" id="fupload" name="fupload" /></div>
					</div>
				</div>
				<div class="btnBox">
					<div onclick="fnUpLoadIntent();" class="upload">Upload</div>
					<div onclick="fnLayerPop('divUpload', 'c');" class="cancle">Cancel</div>
				</div>
			</div>
		</div>
	</form>	
	
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf"%>
		<div class="middleW createTask intentsDetail">
			<div class="btnBox">
				<div onclick="fnSave();" class="save" style="float:right !important;">Save</div>										
				<div onclick="fnOpenPop('', 'IT');" class="createIn" style="float:right !important; margin-right: 10px !important;">의도 Slot 등록</div>
				<div onclick="fnLayerPop('divUpload', 'o');" class="uploadIn" style="float:right !important; margin-right: 10px !important;">Upload Intent</div>
				<div onclick="fnIntentDownload();" id="aIntentDown" class="downloadIn" style="float:right !important; margin-right: 10px !important;">Download Intent</div>
				<c:if test="${not empty SLOT_SEQ_NO || not empty INTENTION}">
					<div onclick="fnUserSayDetailList('', '${SEQ_NO }', '');" id="aIntentDown" class="downloadIn" style="float:right !important; margin-right: 10px !important;">상위 Intent로</div>
				</c:if>															
			</div>
			<div class="saveName">				
				<c:choose>
					<c:when test="${empty SLOT_SEQ_NO && empty INTENTION}">
						<div class="title">Intent Type</div>
						<div class="box" style="border:none;">
							<input type="text" id="txtIntentsName" name="txtIntentsName" placeholder="Intent name" value="" style="width:100%; border:1px solid #ddd; border-radius:4px;" />
							<input type="text" id="txtIntentsRequest" name="txtIntentsRequest" value="" style="width:29%; border:1px solid #ddd; border-radius:4px; margin-left:5px; background-color:#c5eae3; display:none;" readonly="readonly" />
							<span id="spWarning" class="Warning" style="display:none;"> [주의] 이름 변경 주의 : 기존 이름으로 저장한 다른 지식들은 변경되지 않습니다.</span>
						</div>						
					</c:when>
					<c:otherwise>
						<div class="title">Intent</div>
						<div class="box">
							<input type="text" id="viewIntentsName" readonly="readonly" value='' style="width:100%;" />
							<input type="hidden" id="txtIntentsName" name="txtIntentsName" value='${INTENT_NAME}' />
							<input type="hidden" id="txtIntentsRequest" name="txtIntentsRequest" value='' />
						</div>
					</c:otherwise>
				</c:choose>	
			</div>
			
			<div id="divSlotLayer" class="slot_layer" style="display: none;">
				<a href="#" class="hide divSlotLayer_c" onclick="fnSlotClose();">닫기</a>
				<div id="divSlotLayerCon"></div>
			</div>
			<div id="divUserSayBox" class="liBox liBox4 liBox5">
				<div class="title">
					<div class="title">사용자 발화</div>
					<div class="btnBox">
						<input type="text" id="txtSearch" style="width:200px; height:25px; float:left; border:1px solid #ddd; border-radius:4px;" />
						<div onclick="fnSelectList();" class="btn" style="margin:0px 5px; background-color: #87A;">검색</div>						
					</div>
				</div>
				<div class="ulBox">
					<ul id="divUserSays">
						<%-- <c:forEach items="${UserSaylist }" var="row">
						<li>
							<c:choose>
								<c:when test="${empty SLOT_SEQ_NO}">
									
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }" name="EDIT_USER_SAYS">${row.SLOT_TAG}</div>
											<span onclick="fnUserSayDelete('${row.SLOT_SEQ_NO}', ${row.SEQ_NO }, this);" class="remove"></span> <!-- 이게 엑스 -->																			
									<c:choose>
										<c:when test='${row.SLOT_SEQ_NO ne ""}'>
											<span onclick="fnUserSayDetailList('${row.SLOT_SEQ_NO}', ${SEQ_NO });" class="list_btn"></span>		<!-- 이게 리스트 -->										
										</c:when>
										<c:otherwise>		
											<span onclick="fnUserSayEdit(${row.SEQ_NO }, this);" class="modify"></span> <!-- 이게 연필 -->									
											<span onclick="fnUserSayClear(this);" class="undo_btn"></span> <!-- 이게 되돌리기 -->
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>								
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }" name="EDIT_USER_SAYS">${row.SAY_TAG }</div>
									<input type="hidden" name="hidUserSayEdit" value='${row.SLOT_MAPDA }' />
									<span onclick="fnUserSayEdit(${row.SEQ_NO }, this);" class="modify"></span> <!-- 연필 -->
									<span onclick="fnUserSayClear(this);" class="undo_btn"></span> <!-- 이게 되돌리기 -->
									<span onclick="fnUserSayDelete('', ${row.SEQ_NO }, this);" class="remove"></span> <!-- 엑스 -->									 																		
									<input type="hidden" name="hidUserSayEdit" value='${row.SLOT_MAPDA }' />
								</c:otherwise>
							</c:choose>
						</li>
						</c:forEach> --%>
					</ul>
					<div id="PAGE_NAVI" class="board_paging" style="margin-bottom:0px;">
					</div>
					<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX" />
					<input type="hidden" id="TOTAL_COUNT" name="TOTAL_COUNT" />
				</div>
				
				<div id="divUserSaysDetail" class="ulBox" style="display: none;">
				</div>
			</div>
			
			<div id="divreplies" class="liBox liBox1">
				<div class="title">시스템 응답 정의<div onclick="fnResponseAdd();" class="btn">추가</div></div>
				<c:choose>
					<c:when test="${fn:length(Intentionlist) > 0}">
						<c:forEach items="${Intentionlist}" var="row">
							<div class="fireBox" name="wtastas">
								<div class="fireMenu" name="none_obj" style="display:none;">
									<ul>
										<li class="on">시스템 발화</li>										
										<li>시스템 발화 후 행동</li>
									</ul>
								</div>	
								<div class="fireCont" style="margin-bottom: 20px;">
									<div class="fire systemfire on">										
										<c:choose>
										<c:when test="${fn:length(Objectlist) > 0}">											
											<c:forEach items="${Objectlist}" var="orow">																																																						
												<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">	
													<div name="divSysIntent">
														<div class="inputBtn" name="none_obj" style="display:none;">
															<div class="title">시스템 발화 조건</div>
															<input type="text" id="txtEUttrT1_${row.SEQ_NO}" name="txtUttr" value="${fn:replace(row.UTTR, '\"', '&quot;')}" />
															<button type="button" onclick="fnOpenPop(this, 'F', 'T');" title="Script Guide" class="scriptGuide"></button>
														</div>
														<div class="input" name="none_obj" style="display:none;">
															<div class="title">시스템 intent</div>
															<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">${orow.OBJECT_VALUE}</div>
														</div>
														<div class="liBox liBox_style">
															<ul>
																<c:if test="${fn:length(ObjectDtllist) > 0}">
																	<c:set var="Cnt" value="0" />
																	<c:set var="Request" value="" />
																	<c:forEach items="${ObjectDtllist}" var="drow">																																																						
																		<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">
																			<c:choose>
																			<c:when test="${Cnt == 0}">
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<c:set var="Cnt" value="1" />
																					<c:set var="Request" value="${drow.OBJECT_VALUE }" />																			
																				</c:when>
																				<c:otherwise>
																					<c:set var="Cnt" value="2" />
																					<li name="mainli">								
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>									
																					</li>
																					<div>
																						<ul>
																				</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:when test="${Cnt == 1}">
																				<c:set var="Cnt" value="2" />																		
																				<li name="mainli">																			
																					<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false">${Request }</div>
																					<input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>
																					<div class="pattern pattern_style">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>																						
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</div>
																				</li>
																				<div>
																					<ul>
																			</c:when>
																			<c:otherwise>																
																				<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>			
																						<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
																					</li>																			
																				</c:when>
																				<c:otherwise>
																					<li name="subli">
																						<div>
																							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"><c:choose><c:when test="${drow.OBJECT_VALUE == ' '}">&nbsp;</c:when><c:otherwise>${drow.OBJECT_VALUE }</c:otherwise></c:choose></div>
																						</div>
																						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
																					</li>
																				</c:otherwise>
																				</c:choose>																																					
																			</c:otherwise>
																			</c:choose>
																		</c:if>
																	</c:forEach>
																				</ul>
																			</div>
																</c:if>																															
															</ul>
														</div>
														<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
															<div class="input input2_style" name="none_obj" style="display:none;">
																<div class="title">다음 사용자 발화 DA 제한</div>
																<input class="input2_con_style" type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" />
															</div>
														</c:if>	
													</div>
												</c:if>
											</c:forEach>
										</c:when>
										</c:choose>										
									</div>
									<div class="fire inputBtn">
										<div class="right">											
											<input type="text" id="txtEAction${row.SEQ_NO}" name="txtAction" value="${fn:replace(row.ACT, '\"', '&quot;')}" />
											<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
										</div>
									</div>
									<span onclick="fnObjectView(this, 1);" class="open"></span>
									<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
									<span onclick="fnUserObjectDelete(1, this, 'T');" class="delete" style="display:block;"></span>
								</div>
								<c:if test="${USER_TYPE ne 'OA'}">
									<div class="fireAdd" name="none_obj" style="display:none;" onclick="fnSysIntentAdd(this, 'T');">+ Add</div>
								</c:if>	
							</div>														
						</c:forEach>							
					</c:when>					
				</c:choose>
			</div>
		</div>
	</div>
	
	<input type="hidden" id="hidSlotAdd" />
	<input type="hidden" id="hidSlotColor" />
	
	
	<div id="divAdd" style="display: none;">
		<li>	
			<div class="USER_SAYS" name="USER_SAYS" contenteditable="true"></div>
			<span onclick="fnUserSayDelete('', '', '', this);" class="remove"></span>
			<span onclick="fnSlotMapping(this);" class="mapping_btn"></span>  <!-- 이게 A -->
			<span onclick="fnUserSayClear(this);" class="undo_btn"></span> <!-- 이게 되돌리기 -->
			<span onclick="fnIntentionPop(this);" class="intention_btn"></span>			
			<div class="intent_title">${INTENTION}</div>
		</li>		
	</div>
	
	<div id="divrepliesAdd" style="display: none;">
		<div class="fireBox" name="wtastas" style="margin-bottom:0px;">
			<div class="fireMenu" name="none_obj" style="display:none;">
				<ul>
					<li class="on">시스템 발화</li>					
					<li>시스템 발화 후 행동</li>
				</ul>
			</div>
			<div class="fireCont" style="margin-bottom:20px;">
				<div class="fire systemfire on">
					<div name="divSysIntent">
						<div class="inputBtn" name="none_obj" style="display:none;">
							<div class="title">시스템 발화 조건</div>
							<input type="text" id="txtUttr§" name="txtUttr" value="true" />
							<button type="button" onclick="fnOpenPop(this, 'F', 'T');" title="Script Guide" class="scriptGuide"></button>
						</div>
						<div class="input" name="none_obj" style="display:none;">
							<div class="title">시스템 intent</div>
							<!-- <input type="text" /> -->
							<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">inform()</div>
						</div>
						<div class="liBox liBox_style">
							<ul>
								<li name="mainli"><!-- <input type="text"/> -->								
									<div>
										<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
									</div>									
									<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>									
								</li>
								<div>
									<ul></ul>
								</div>
							</ul>
						</div>
					</div>
				</div>				
				<div class="fire inputBtn">
					<div class="right">
						<input type="text" id="txtAction§" name="txtAction">
						<button onclick="fnOpenPop(this, 'A');" type="button" title="Script Guide" class="scriptGuide"></button>
					</div>						
				</div>
				<span onclick="fnObjectView(this, 1);" class="open"></span>
				<span onclick="fnObjectView(this, 2);" class="close" name="none_obj" style="display:none;"></span>
				<span onclick="fnUserObjectDelete(1, this, 'T');" class="delete" style="display:block;"></span>
			</div>
			<c:if test="${USER_TYPE ne 'OA'}">
				<div class="fireAdd" name="none_obj" style="display:none;" onclick="fnSysIntentAdd(this, 'T');">+ Add</div>
			</c:if>			
		</div>
	</div>
	
	<div id="divDetailSub" style="display: none;">
		<li name="subli">
			<div>
				<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
			</div>
			<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
		</li>
	</div>
		
	<div id="divSysintentAdd" style="display:none;">
		<div name="divSysIntent">
			<div class="input" name="none_obj" style="display:block;">
				<div class="title">시스템 intent</div>
				<!-- <input type="text" /> -->
				<div class="systemfileDiv" name="OBJECT_NAME" contenteditable="true" class="wordBr">inform()</div>
			</div>
			<div class="liBox">
				<ul>
					<li name="mainli">
						<div>
							<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
						</div>
						<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
						<!-- <span onclick="fnObjectView(this, 1);" class="modify"></span> -->
					</li>
					<div>
						<ul></ul>
					</div>
				</ul>
			</div>
		</div>
	</div>
	
	<div id="divInformAdd" style="display:none;">
		<div>
			<div name="OBJECT_DETAIL_NAME" contenteditable="true" class="wordBr"></div>
		</div>		
		<span onclick="fnUserObjectDelete(2, this, 'T');" class="remove"></span>
	</div>
	
	<div id="divRequestAdd" style="display:none;">
		<div class="slot slot_style"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="false" class="wordBr"></div><input type="hidden" name="SLOT_SEQ_NO" />
		<span class="remove"></span></div>
		<div class="pattern pattern_style">
			<div>
				<div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div>
			</div>
			<span onclick="fnObjectView(this, 1);" class="modify" style="display:none;"></span>
			<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
		</div>
	</div>
	
	<div id="divDetailSubP" style="display:none;">
		<li name="subli">
			<div><div name="OBJECT_REQUEST_PATTERN" contenteditable="true" class="wordBr"></div></div>			
			<span onclick="fnUserObjectDelete(2, this, 'P');" class="remove"></span>
		</li>
	</div>
</body>
</html>