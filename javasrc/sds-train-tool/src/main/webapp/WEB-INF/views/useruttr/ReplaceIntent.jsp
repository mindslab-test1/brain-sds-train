<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		window.onload = function() {
			$('#tbslot').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddRow($("#tbslot > tbody"));
					}
				},
				focusout: function(e) {
					var $this = $(this);	
					var id = $this.attr('id');					
					
					if(!gfn_isNull(id)) {
						var seq = id.replace('txtObj', '');
						if($('#hidSaveChk' + seq).val() == 'Y') {
							var strupdate = '';													
							strupdate += 'R_BEFORE = \'' + $(this).parent().parent().find('input:text').eq(0).val().replace(/\\/, '\\\\').replace(/'/, '\\\'') + '\'';
							strupdate += ', R_AFTER = \'' + $(this).parent().parent().find('input:text').eq(1).val().replace(/\\/, '\\\\').replace(/'/, '\\\'') + '\'';
							
							var comAjax = new ComAjax();		
							comAjax.setCallback("fnAjaxCallBack");
							comAjax.addParam("SEQ_NO", '${SEQ_NO}');
							comAjax.addParam("ITEM_SEQ_NO", seq);
							
							if(strupdate != '') {								
								comAjax.setUrl("<c:url value='/view/updatereplaceintent.do' />");
								comAjax.addParam("SET_ITEM", strupdate);
							}
							else {
								comAjax.setUrl("<c:url value='/view/deletereplaceintent.do' />");
								$(this).parent().parent().remove();
							}
							comAjax.ajax();
							$('#hidSaveChk' + seq).val('N');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						/* $(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e); */
					} else if(e.keyCode == 40) {
						
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txtObj', '')).val('Y');
					}
				}			
			}, '[name=OBJECT]');
						
			$('#tbslot').on('click', '[name="imgdel"]', function(e) {
				var $tr = $(this).parent().parent();
				if($tr.find('[id^=hidSeqno]').length > 0) {					
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deletereplaceintent.do' />");
					comAjax.setCallback("fnAjaxCallBack");
					comAjax.addParam("SEQ_NO", '${SEQ_NO}');
					comAjax.addParam("ITEM_SEQ_NO", $tr.find('[id^=hidSeqno]').val());
					comAjax.ajax();
				}
				$tr.remove();
				fnpreventDefault(e);
			});
			
			$('#txtSearch').bind('keydown', function(e) {
				if(e.keyCode == 13) { 
					fnSelectList(1);
				}
			});
						
			fnSelectList(1);
		}
		
		function fnAjaxCallBack(data) {
			if (data.status != "OK") {
				alert('작업 중 오류가 발생 하였습니다.');
			}
		}
		
		function fnShowSearchBtn(type) {
			if (type == 0) {
				$("#txtSearch").val('');
				$('#tbSearchCol tr input').val('');
				fnSelectList(1);
			}
			else {
				$("#divSearchBox").show();
				fnLayerPop('divSearchBox');
			}	
		}				
		
		function fnCancelSearchBtn() {			
			$("#divSearchBox").hide();
			$('#layer').fadeOut();
			fnpreventDefault(event);
		}
		
		function fnSelectList(pageNo) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectreplaceintent.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.addParam("PROJECT_SEQ", '${PROJECT_SEQ}');			
			$txtSearch = $("#txtSearch");
			var strsearch = '';
			var searchval = '';
			
			if($txtSearch.val().trim() != '' && $txtSearch.val() != 'search…') {			
				strsearch = 'AND (R_BEFORE LIKE "§'+ $txtSearch.val() + '§" OR R_AFTER LIKE "§'+ $txtSearch.val() + '§")';				
			}
				
			comAjax.addParam("WHERE_SQL", strsearch);
			comAjax.ajax();
			fnpreventDefault(event);
		}
						
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbslot > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var str = "";
				var iRowColor = true;											
				
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
						
					str += '<input type="hidden" id="hidSaveChk'+value.SEQ_NO+'" value="N" />' +
						'<input type="hidden" id="hidSeqno'+value.SEQ_NO+'" value="'+value.SEQ_NO+'" />';
					
					str += '<td><input type="text" class="none_focus" value="'+value.R_BEFORE.replace(/\"/g,"&quot;")+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';
					str += '<td><input type="text" class="none_focus" value="'+value.R_AFTER.replace(/\"/g,"&quot;")+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';					
					str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
					str +='</tr>'; 
				});
				
				body.append(str);
				AddRow(body);
				
				$("a[name='title']").on("click", function(e){ //제목 
					e.preventDefault();
					fn_openBoardDetail($(this));
				});
			}
		}
		
		function AddRow(el) {
			var str = "<tr name='trIn'>";
			
			for (var int = 0; int < 2; int++) {
				str += "<td><input type='text' value='' name='OBJECT' /></td>";
			}		
			str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
			str += "</tr>";				
			el.append(str);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();									
			
			$('#tbslot tbody [name=trIn]').each(function(idx, e) {								
				var item = new Object();				
				var arrobjsub = new Array();				
				var bInserChk = false;							
				var strvalues = 'VALUES(';
				
				$(this).find('input:text').each(function() {
					if($(this).val().trim() != '') {
						bInserChk = true;
					}					
					strvalues += '"' + $(this).val().replace(/'/g, "et§").replace(/\\/g, '\\\\').replace(/"/g, '\\"') + '", ';								
				});
				
				if(bInserChk) {
					strvalues = strvalues + '${PROJECT_SEQ})';					
					item.ITEM_DETAIL = strvalues;
					arrobj.push(item);
				}
			});
			arr.ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertreplaceintent.do' />");
			comSubmit.addParam("PROJECT_SEQ", '${PROJECT_SEQ}');		
			comSubmit.addParam("VALUES_ITEM", jsoobj);
			comSubmit.submit();			
			fnpreventDefault(event);
		}		
	</script>
</head>
<body>
	<div id="wrap">
	<%@ include file="/WEB-INF/include/left.jspf" %>	
	<div class="middleW entities entitiesDetail intents">
		<div class="btnBox">
			<div class="save" onclick="fnSave();">SAVE</div>
		</div>
		<div class="searchBox">
			<div class="box"><input type="text" id="txtSearch" placeholder="search slot..."><button type="button"><img src="../images/icon_searchW.png" alt="search"></button></div>
		</div>
		<div class="liBox liBox3">
			<div class="title slotCond">Replace Intents</div>
			<div class="conBox tableBox slotCondBox">
				<table id="tbslot" class="table">
					<caption class="hide" style="display:none;">USER</caption>
					<colgroup>
						<col width="50%">
						<col width="50%">
					</colgroup>
					<thead>
						<tr>
							<th name="thTitle">치환 전</th>
							<th name="thTitle">치환 후</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text"></td>
							<td><input type="text"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--board paging start-->
			<div id="PAGE_NAVI" class="board_paging">
			</div>
			<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
		</div>
	</div>
</div>
</body>
</html>