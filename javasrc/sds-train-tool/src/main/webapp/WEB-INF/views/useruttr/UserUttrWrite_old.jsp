<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
<script type="text/javascript">
		var $WorkhidObj;
		var blockvalue = '';
		var UserSayRange;
		var arrSlot = new Array();
		var arrSlotList = new Array();
		var iSeqNo = '';
		var jsondata = jQuery.parseJSON('${INTENTS_LIST}'.replace(/=\"/g, '=').replace(/\"\)/g, ')'));
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var slotdata2 = jQuery.parseJSON('${SLOT_LIST2}');
		var slotstr = '';
		var $autoMapobj;
		var pop;
		
		window.onload = function() {
			AddRow();
			
			$('#divUserSays').on({				
				'click focusin': function(e) {
					if($(this).parent().next().length == 0 && $(this).text() != '') {
						AddRow();
					}
				},
				mouseup: function(e) {
					blockvalue = window.getSelection().toString();
					if(!gfn_isNull(blockvalue)) {												
						UserSayRange = window.getSelection().getRangeAt(0);
						$WorkhidObj = $(this).next();					
						var comAjax = new ComAjax();
						comAjax.setUrl("<c:url value='/view/getslotobjectlist.do' />");
						comAjax.setCallback("fnSlotSearchCallBack");
						comAjax.addParam("OBJECT_NAME", blockvalue);
						comAjax.setAsync(false);
						comAjax.ajax();
					}
					fnpreventDefault(e);
				},
				focusout: function(e) {
																							
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().prev().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 40) {
						$(this).parent().next().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {

					} else if(e.keyCode == 13) {
						$this = $(this);
						if($this.parent().next().length == 0 && $this.text() != '') {
							AddRow();
						}
						$this.parent().next().children().eq(0).focus();
						return false;
					} else {
						bWorkChk = true;
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=USER_SAYS]');		
						
			$('#divreplies').on({
				focusout: function(e) {
					$this = $(this);
					var orgval;
					var $parentobj = $this.parent().parent().parent().parent().parent().parent();
					
					if ($this.text().toLowerCase().indexOf('request') > -1) {											
						if($parentobj.find('[name=OBJECT_REQUEST]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
							$parentobj.find('.wtastas01').html($('#divRequestAdd').html());
							$parentobj.find('.wtastas02').css('padding-left', '110px');
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').attr('name', 'OBJECT_REQUEST_PATTERN');
							$parentobj.append('<div name="divDA" style="padding-left: 64px;"><span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;<input type="text" name="LIMIT_DA" class="wtastas_text_div dp_ib" style="width: 59%;"></div>');
						}
					}
					else {
						if($parentobj.find('[name=OBJECT_DETAIL_NAME]').length == 0) {							
							orgval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
							$parentobj.find('.wtastas01').html($('#divInformAdd').html());
							$parentobj.find('.wtastas02').css('padding-left', '');
							$parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text(orgval);
							$parentobj.find('[name=OBJECT_REQUEST_PATTERN]').attr('name', 'OBJECT_DETAIL_NAME');
							$parentobj.find('[name=divDA]').remove();
						}
					}
				}		
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {						
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
					//fnOpenPop(this, 'S');
				},
				change: function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					var $this = $(this);
										
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					
					/* if($this.parent().parent().parent().parent().parent().next().length == 0) {
						$this.parent().parent().parent().parent().parent().parent().append($('#divDetailSubP').html());
					} */
					
					if ($this.text() != '') {
						if($this.parent().parent().parent().parent().parent().next().attr('name') == 'divDA') {
							$this.parent().parent().parent().parent().parent().after($('#divDetailSubP').html());
						}	
					}					
				},
				change: function(e) {
					bWorkChk = true;
				},
				keydown: function(e) {
					if(e.keyCode == 9) {
						$this = $(this);
						if ($this.text() != '') {
							if($this.parent().parent().parent().parent().parent().next().attr('name') == 'divDA') {
								$this.parent().parent().parent().parent().parent().after($('#divDetailSubP').html());
							}
							fnCursorEnd($this.parent().parent().parent().parent().parent().next().find('td').eq(1).find('div').eq(0).get(0));
						}						
						fnpreventDefault(e);
					}
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {						
					var $this = $(this);
					
					/* if ($this.parent().parent().parent().parent().parent().parent().find('div.wtastas02.btn_hover').length > 14)				
						return; */
					
					if ($this.text() != '') {
						if($this.parent().parent().parent().parent().parent().next().length == 0 || $this.parent().parent().parent().parent().parent().next().attr('class') == 'wtastas01 btn_hover') {					
							$this.parent().parent().parent().parent().parent().after($('#divDetailSub').html());						
						}
					}
				},
				focusout: function(e) {
					var str = $(this).text();
					var arrvalidation = fnSlotValidation(str, slotdata);					
					if (!arrvalidation[0]) {
						var arrtempval = new Array();
						for (var i = 1; i < arrvalidation.length; i++) {
							arrtempval.push(arrvalidation[i]);
						}
						alert('존재 하지 않는 슬롯입니다.\n(' + arrtempval.join(', ') + ')');
					}																
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						//$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						//$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {
						$this = $(this);
						if ($this.text() != '') {
							if($this.parent().parent().parent().parent().parent().next().length == 0 || $this.parent().parent().parent().parent().parent().next().attr('class') == 'wtastas01 btn_hover') {
								$this.parent().parent().parent().parent().parent().after($('#divDetailSub').html());
							}
							fnCursorEnd($this.parent().parent().parent().parent().parent().next().find('div').eq(0).get(0));
						}						
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;	
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');			
			
			$('#divreplies').on('click', '.close_btn', function(e) {				
				$(this).prev().val('');
				$(this).prev().prev().text('');
				bWorkChk = true;
				fnpreventDefault(e);
			});
						
			$('#txtIntentsName').change(function() {
				bWorkChk = true;
			});
			
			<c:forEach items="${SlotList}" var="row">
				var objSlot = new Object();
				objSlot.seqno = ${row.SEQ_NO};
				objSlot.slotname = '${row.SLOT_NAME}';
				objSlot.color = '${row.COLOR}';
				
				arrSlotList['${row.SLOT_NAME}'] = objSlot;					
			</c:forEach>
			
			if ('${param.UPLOAD_STATUS}' == 'F') {
				if ('${param.STATUS}' == 'NS')
					alert('Entities에서 작성하지 않은 슬롯이 포함되어 있습니다.');				
				else
					alert('파일 형식이 잘못되었습니다.');				
			}
		}			
		
		$(window).bind("beforeunload", function (e){
			if (!gfn_isNull(pop)) {
				pop.close();
			}
		});
		
		function fnColGroupChange(seqno) {
			$('#tbcolg' + seqno).html('<col width="120px" /><col width="*" /><col width="110px" />');			
		}
		
		function AddRow() {
			$('#divUserSays').append($('#divAdd').html());
		}
				
		function fnSlotSearchCallBack(data) {
			var divTop = event.pageY + 10; //상단 좌표 위치 안맞을시 e.pageY
			var divLeft = event.pageX; //좌측 좌표 위치 안맞을시 e.pageX
			
			slotstr = '';
			if(data.list.length > 0) {
				$.each(data.list, function(key, value){					
					slotstr += '<a href="#" onclick="fnSetSlot(' + value.SEQ_NO + ', \'' + value.CLASS_NAME + '.' + value.SLOT_NAME + '\', \'' + value.COLOR + '\', true);">' + value.CLASS_NAME + '.' + value.SLOT_NAME + '</a>';
					
					if (value.PARENT_CLASS_SEQ_NO != 0) {
						fnGetNotUsingClass(value.CLASS_SEQ_NO, value.SEQ_NO, value.SLOT_NAME, value.COLOR);
					}
				});
			}
			else
				slotstr = '일치하는 Slot이 없습니다.';
			
			slotstr += '<a href="#" onclick="fnOpenSlot();">&lt;직접선택&gt;</a>';
			
			$('#divSlotLayerCon').empty().append(slotstr);
			$('#divSlotLayer').css({
			     "top": divTop
			     ,"left": divLeft
			     , "position": "absolute"
			}).show();
		}
		
		function fnGetNotUsingClass(classSeq, slotSeq, slotName, slotColor) {
			$('#hidSlotAdd').val(classSeq + '|' + slotSeq + '|' + slotName + '|' + slotColor)
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", classSeq);			
			comAjax.setUrl("<c:url value='/view/getparentclass.do' />");			
			comAjax.setCallback("fnGetNotUsingClassCallBack");
			comAjax.setAsync(false);
			comAjax.ajax();
		}
		
		function fnGetNotUsingClassCallBack(data) {
			try {
				var arrobj = $('#hidSlotAdd').val().split('|');
				var slotname = '';
				$.each(data.list, function(key, value){
					var arrsubobj = value;
					$.each(slotdata2[value.CLASS_SEQ_NO], function(key, value){
						if (arrsubobj.SLOT_SEQ_NO == value.SEQ_NO) {
							slotname = value.SLOT_NAME + '.' + arrobj[2];							
							fnGetSlotColor(value.CLASS_SEQ_NO, slotname);
							slotstr += '<a href="#" onclick="fnSetSlot(\'' + value.CLASS_SEQ_NO + '_' + value.SEQ_NO + '\', \'' + value.CLASS_NAME + '.' + slotname + '\', \'' + $('#hidSlotColor').val() + '\', true);">' + value.CLASS_NAME + '.' + slotname + '</a>';
							$('#hidSlotColor').val('');
						}
						/* if (value.TYPE_SEQ == arrobj[0]) {
							arrobj[0] = value.CLASS_SEQ_NO;
							arrobj[2] = value.SLOT_NAME + '.' + arrobj[2];
							fnGetSlotColor(arrobj[0], arrobj[2]);
							slotstr += '<a href="#" onclick="fnSetSlot(\'' + value.CLASS_SEQ_NO + '_' + arrobj[1] + '\', \'' + value.CLASS_NAME + '.' + arrobj[2] + '\', \'' + $('#hidSlotColor').val() + '\', true);">' + value.CLASS_NAME + '.' + arrobj[2] + '</a>';
							$('#hidSlotColor').val('');
						} */
					});
				});
				$('#hidSlotAdd').val('');
			} catch (e) {
				// TODO: handle exception				
			}
		}
		
		function fnGetSlotColor(classSeq, slotName) {			
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", classSeq);
			comAjax.addParam("SLOT_NAME", slotName);
			comAjax.setUrl("<c:url value='/view/getslotcolor.do' />");			
			comAjax.setCallback("fnGetSlotColorCallBack");
			comAjax.setAsync(false);
			comAjax.ajax();
		}
		
		function fnGetSlotColorCallBack(data) {
			try {
				$('#hidSlotColor').val(data.COLOR);
			} catch (e) {
				// TODO: handle exception
			}	
		}
		
		function fnSetSlot(seqno, slotname, color, bevent) {			
			var spos = UserSayRange.startOffset;
			var epos = UserSayRange.endOffset;
			var content = UserSayRange.startContainer.textContent;
			var text = UserSayRange.toString();
			
			var $WorkdivView = $WorkhidObj.prev(); 
			
						
			var sindex = fnReplaceSpace($WorkdivView.html()).indexOf(content);
			$WorkdivView.html(fnReplaceSpace($WorkdivView.html()).substring(0, sindex + spos) + '<span style="color:' + color + '">' + text + '</span>' + fnReplaceSpace($WorkdivView.html()).substring(sindex + epos));
			var objSlot = new Object();
			objSlot.seqno = seqno;
			objSlot.slotname = slotname;
			objSlot.color = color;
			objSlot.text = text;
			
			arrSlot[text + '_' + color] = objSlot;
			bWorkChk = true;
		
			fnSlotClose();
			if (bevent) {
				fnpreventDefault(event);	
			}
		}
		
		function fnTagDeep(obj) {
			var returnval = ''
			$(obj).each(function() {
				if($(this).find('span').length == 0) {
					returnval = $(this).text(); 
				}
				else 
					returnval = fnTagDeep(this);
			});
			return returnval;
		}
		
		function fnSlotClose() {
			$('#divSlotLayer').hide();
		}
	
		function fnResponseAdd() {
			if ($('#divreplies').find('[name=wtastas]').length == 15) {
				fnpreventDefault(event);
				return false;
			}
			
			$('#divreplies').append($('#divrepliesAdd').html().replace(/§/g, $('#divreplies [name=wtastas]').length));			
			fnpreventDefault(event);
		}			
		
		function fnResponseCancel(type) {
			$.modal.close();
			$("#divResponse").hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj, type) {
			if (type == 'F') {
				var width = '713';
				var heigth = '626';
				if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
					width = '613';
					heigth = '400';	
				}
				
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).parent().prev().find('input').attr('id') + '&flag=R', "IntentsPop", width, heigth, 0, 0);	
			}
			else if (type == 'A') {
				pop = fnWinPop("<c:url value='/view/openactionwrite.do' />" + "?obj=" + $(obj).parent().prev().find('input').attr('id'), "IntentsPop", 570, 470, 0, 0);	
			}		
			else if (type == 'S') {
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id') + "&Type=TS", "AgentSearchpop", 430, 550, 0, 0);
			}
		}
		
		function fnOpenSlot() {
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=&Type=U", "AgentSearchpop", 430, 550, 0, 0);
		}
		
		function fnSave() {			
			if ($('#divLoading').css('display') == 'block') {
				alert('저장 중 입니다.');
				return;
			}			
		
			var bNameChk = false;
			var bIntentSaveChk = false;
			var focusObj;
			
			$txtIntentsName = $('#txtIntentsName');
		
			if ($txtIntentsName.val() == 'Intent name' || $txtIntentsName.val().trim() == '') {
				alert('Intent명을 입력 하세요.');
				$txtIntentsName.focus();
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_\-=()."]/;
			if (reg.test($txtIntentsName.val())) {
				alert('Intent명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$txtIntentsName.focus();
				fnpreventDefault(event);
				return false;
			}
				
			if (jsondata[${AGENT_SEQ}] != undefined) {
				var str = '';		
				$.each(jsondata[${AGENT_SEQ}], function(key, value){
					if('${SEQ_NO}' == '' && iSeqNo == '') {
						if (value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else if (iSeqNo != ''){
						if (value.SEQ_NO.toString() != iSeqNo && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else {
						if (value.SEQ_NO.toString() != '${SEQ_NO}' && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
				});							
			}
			
			if (bNameChk) {
				alert('이미 존재하는 Intent명 입니다.');
				fnpreventDefault(event);
				return false;
			}
			
			fnLoading();
		
			var objusersay = new Object();			
			var arrusersay = new Array();
			var bSaveChk = true;
									
			$('#divUserSays [name=USER_SAYS]').each(function(idx, e) {
				$ObjUserSay = $(this);
				if(!gfn_isNull($ObjUserSay.text())) {
					var usitem = new Object();
					usitem.SAY = $ObjUserSay.text();
					usitem.SAY_TAG = fnReplaceSpace($ObjUserSay.html());
					
					if (usitem.SAY_TAG.indexOf('<span style="color: rgb') == 0) {						
						usitem.SAY_TAG = usitem.SAY_TAG.replace(/<span [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>/g, '');
						if (usitem.SAY_TAG.substring(usitem.SAY_TAG.length -7) == '</span>') {
							usitem.SAY_TAG = usitem.SAY_TAG.substring(0, usitem.SAY_TAG.length -7);
						}
					}
									
					var slotarray = new Array();					
					var arrslotseq = new Array();
					var arrslotname = new Array();
					var arrslotmapda = new Array();
					var slottag = usitem.SAY_TAG;
					var slotmap = usitem.SAY_TAG;
							
					try {
						$(this).find('span').each(function() {
							var text = $(this).text();//fnTagDeep(this);					
							var arr = arrSlot[text + '_' + fnHexc($(this).css('color'))];
							/* if (arr.seqno.toString().indexOf('_') > -1)
								arrslotseq.push(Number(arr.seqno.toString().split('_')[1]));	
							else
								arrslotseq.push(arr.seqno); */
							if (arr == undefined) {
								//slottag = usitem.SAY;
								//slotmap = usitem.SAY;
								return;
							}
							arrslotseq.push(arr.seqno);
							arrslotname.push(arr.slotname + '=value');
							arrslotmapda.push(arr.slotname + '="' + text.trim() + '"');
							slottag = slottag.replace('>' + text + '</', '>@' + arr.slotname + '</');
							var reg = new RegExp('<span style=\"color:#\\w+\">' + text.replace('+', '\\+').replace('|', '\\|').replace('(', '\\(').replace(')', '\\)').replace('[', '\\[').replace(']', '\\]') + '<\/span>','g');
							slotmap = slotmap.replace(reg, '<' + arr.slotname + '=' + text.trim() + '>');
						});	
					} catch (e) {
						bSaveChk = false;
						fnLoading();
						alert('User says 저장 중 [' + usitem.SAY + '] 문장에서 문제를 발견 하였습니다. 문장을 다시 작성해주세요.');
						return;
					}
					
					
					arrslotseq.sort();
					
					usitem.SLOT_SEQ_NO = arrslotseq.toString();
					usitem.SLOT_TAG = slottag;
					usitem.SLOT_NAME = arrslotname.toString();
					usitem.SLOT_MAP = slotmap;
					
					if (usitem.SLOT_MAP == '') {
						usitem.SLOT_MAP = usitem.SAY; 
					}
					
					if(gfn_isNull($ObjUserSay.attr('id')))
						usitem.SEQ_NO = '0';
					else
						usitem.SEQ_NO = $ObjUserSay.attr('id');
					
					usitem.SLOT_MAPDA = arrslotmapda.toString().replace(/,/g, ', ');
					arrusersay.push(usitem);					
				}				
			});
			
			if (bSaveChk) {						
				objusersay.USER_SAYS = arrusersay;
				var usersayobj = JSON.stringify(objusersay);
				
				var arr = new Object();
				var mainarrobj = new Array();
				var cnt = 0;
				
				$('#divreplies [name=wtastas]').each(function(idx, e) {							
					var mainitem = new Object();
					mainitem.UTTR = $(this).find('[name=txtUttr]').val();
					mainitem.ACT = $(this).find('[name=txtAction]').val();				
					mainitem.SLOT_SEQ_NO = '${SLOT_SEQ_NO}';
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';

					var arrobj = new Array();
					
					$(this).find('[name=divSysIntent]').each(function(idx, e) {
						var $this = $(this);
						cnt = 0;
						var item = new Object();
						var arrobjsub = new Array();
						item.OBJECT_VALUE = $this.find('[name=OBJECT_NAME]').text();										
						item.LIMIT_DA = '';
						
						if ($this.find('[name=LIMIT_DA]').length > 0)
							item.LIMIT_DA = $this.find('[name=LIMIT_DA]').val();
						
						if(item.OBJECT_VALUE.trim() == '')
							item.OBJECT_VALUE = 'inform()';
						else {
							if (item.OBJECT_VALUE.indexOf('(') == -1) {
								item.OBJECT_VALUE += '()';
							}
						}
						
						if (reg.test(item.OBJECT_VALUE)) {
							bIntentSaveChk = true;
							focusObj = $this.find('[name=OBJECT_NAME]');
							return false;							
						}					
											
						if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
							$this.find('[name=OBJECT_REQUEST]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'R';
									arrobjsub.push(subitem);
								}
							});
							
							$this.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									subitem.TYPE = 'P';
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});
						}
						else {
							$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
								if(!gfn_isNull($(this).text())) {
									var subitem = new Object();
									subitem.TYPE = 'T';
									subitem.OBJECT_VALUE = fnReplaceSpace($(this).html());
									
									subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.replace(/<\/div>\s*<div>/g, '<br>').replace(/<(span|p|div|br) [a-zA-Z=":\s()0-9,;&%\-ㄱ-힣]+>|<\/(span|p|div)>|<div>/g, '');
									var tempstr = subitem.OBJECT_VALUE.substr(subitem.OBJECT_VALUE.length - 4, subitem.OBJECT_VALUE.length);
									if (tempstr == '<br>')
										subitem.OBJECT_VALUE = subitem.OBJECT_VALUE.substr(0, subitem.OBJECT_VALUE.length - 4);
									arrobjsub.push(subitem);
									cnt++;
								}
							});		
						}
																
						
						if (cnt > 0) {
							item.OBJECT_DETAIL = arrobjsub;
							
							arrobj.push(item);							
						}	
					});
					
					if (arrobj.length > 0) {
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
					}
				});
				
				if (bIntentSaveChk) {
					fnLoading();
					alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					focusObj.focus();
					return false;
				}
				
				arr.OBJECT_ITEM = mainarrobj;
				var jsoobj = JSON.stringify(arr);
				
				/* var comAjax = new ComAjax();				
				comAjax.setUrl("<c:url value='/view/insertintent.do' />");
				comAjax.setCallback("fnSaveComplete");
				comAjax.addParam("INTENT_NAME", $txtIntentsName.val());
				comAjax.addParam("USER_SAYS", usersayobj);
				comAjax.addParam("OBJECT_ITEM", jsoobj);
				comAjax.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				if('${SEQ_NO}' == '') {
					if (iSeqNo == '')
						comAjax.addParam("SEQ_NO", '');
					else
						comAjax.addParam("SEQ_NO", iSeqNo);
				}
				else
					comAjax.addParam("SEQ_NO", '${SEQ_NO}');
				comAjax.addParam("depth1", '${depth1}');
				comAjax.addParam("depth2", '${depth2}');
				comAjax.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comAjax.addParam("AGENT_SEQ", '0');
				else
					comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comAjax.ajax(); */
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertintent.do' />");
				comSubmit.addParam("INTENT_NAME", $txtIntentsName.val());
				comSubmit.addParam("USER_SAYS", usersayobj);
				comSubmit.addParam("OBJECT_ITEM", jsoobj);
				comSubmit.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				if('${SEQ_NO}' == '') {
					if (iSeqNo == '')
						comSubmit.addParam("SEQ_NO", '');
					else
						comSubmit.addParam("SEQ_NO", iSeqNo);
				}
				else
					comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comSubmit.addParam("AGENT_SEQ", '0');
				else
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
				comSubmit.submit();			
				fnpreventDefault(event);
			}
		}
		
		function fnUserSayDelete(SlotSeqNo, Seqno, obj) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comAjax = new ComAjax();
				if (SlotSeqNo == '') 
					comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");	
				else
					comAjax.setUrl("<c:url value='/view/deleteintentusersay.do' />");
				comAjax.setCallback("");
				comAjax.addParam("SLOT_SEQ_NO", SlotSeqNo);
				comAjax.addParam("SEQ_NO", Seqno);
				comAjax.ajax();	
				$(obj).parent().remove();
			}
			fnpreventDefault(event);
		}
		
		function fnUserSayEdit(Seqno, obj) {			
			if ('${SLOT_SEQ_NO}' != '') {
				var arrMapList = $(obj).prev().val().split(',');
				var sname;
				var tempidx;
				var text;
				
				for (var i = 0; i < arrMapList.length; i++) {
					arrMapList[i] = arrMapList[i].trim();
					sname = arrMapList[i].substr(0, arrMapList[i].indexOf("="));
					tempidx = arrMapList[i].indexOf("=") + 2;
					text = arrMapList[i].substr(tempidx, arrMapList[i].length - tempidx - 1);
					
					var objSlot = new Object();
					objSlot.seqno = arrSlotList[sname].seqno;
					objSlot.slotname = arrSlotList[sname].slotname;
					objSlot.color = arrSlotList[sname].color;
					objSlot.text = text;
					
					arrSlot[text + '_' + objSlot.color] = objSlot;
				}				
			}
			$('#' + Seqno).attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($('#' + Seqno).get(0));
			
			fnpreventDefault(event);
		}
		
		function fnUserSayClear(obj) {
			$UsreSays = $(obj).parent().find('[name=USER_SAYS]');
			$UsreSays.html($UsreSays.text());
			fnCursorEnd($UsreSays.get(0));			
			fnpreventDefault(event);
		}
		
		/* function fnUserSayUndo(Seqno, obj) {
			$UsreSays = $('#' + Seqno);
			if ('${SLOT_SEQ_NO}' != '') {
				$UsreSays.html($UsreSays.text());					
			}
			$UsreSays.attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($UsreSays.get(0));
			
			fnpreventDefault(event);
		} */
		
		function fnUserSayDetailDelete(SeqNo, obj) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");
				comAjax.setCallback("");
				comAjax.addParam("SEQ_NO", SeqNo);
				comAjax.ajax();
				$(obj).parent().remove();
			}
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj) {
			if (SeqNo == 1) {							
				if ($(obj).parent().attr('name') == undefined) {					
					var $parentobj = $(obj).parent().parent().parent().parent().parent().parent();
					if ($parentobj.find('.wtastas01, .wtastas02').length > 1) {
						$parentobj.find('.wtastas01').remove();			
						
						if ($parentobj.find('.wtastas02').length > 0) {									
							if ($parentobj.find('.sys_intent [name=OBJECT_NAME]').text().toLowerCase().indexOf('request') > -1) {				
								var objval = $parentobj.find('[name=OBJECT_REQUEST_PATTERN]').eq(0).text();
								$parentobj.find('.wtastas02').eq(0).attr('class', 'wtastas01 btn_hover').html($('#divRequestAdd').html()).css('padding-left', '');
								$parentobj.find('.wtastas01').eq(0).find('[name=OBJECT_REQUEST_PATTERN]').text(objval);
							}
							else {					
								var objval = $parentobj.find('[name=OBJECT_DETAIL_NAME]').eq(0).text();
								$parentobj.find('.wtastas02').eq(0).attr('class', 'wtastas01 btn_hover').html($('#divInformAdd').html());
								$parentobj.find('.wtastas01').eq(0).find('[name=OBJECT_DETAIL_NAME]').text(objval);
							}
							
							//$parentobj.find('.wtastas01').eq(0).find('.wta_tb_mod').hide();
							$parentobj.parent().find('.wta_tb_mod').hide();
							
							if ($parentobj.parent().prev().css('display') == 'none') {
								$parentobj.parent().find('[name=divSysIntent]').eq(0).find('.wtastas01').eq(0).find('.wta_tb_mod').show();								
							}
						}
						else {
							if ($(obj).parent().attr('name') == undefined)
								$parentobj.parent().remove();						
						}
					}
					else {
						if ($parentobj.parent().prev().css('display') == 'none') {
							if ($parentobj.parent().find('[name=divSysIntent]').length == 1)
								$parentobj.parent().parent().remove();
							else {
								$parentobj.parent().find('[name=divSysIntent]').eq(1).find('.wtastas01').eq(0).find('.wta_tb_mod').show();
								$parentobj.remove();								
							}
						}
						else
							$(obj).parent().parent().find('[name^=OBJECT_]').text('');
					}
				}
				else
					$(obj).parent().parent().parent().remove();
			}
			else if (SeqNo == 2)
				$(obj).parent().parent().parent().parent().parent().remove();				 
			else {				
				$(obj).parent().remove();
			}
			
			bWorkChk = true;
			fnpreventDefault(event);
		}
		
		function fnObjectView(obj, type) {
			if (type == 1) {							
				var $parentobj = $(obj).parent().parent().parent().parent().parent().parent().parent().find('.wtastas_cutn');
				if ($parentobj.css('display') == 'none') {
					//$parentobj.parent().parent().css('border', '1px solid #c5c5c5').find('[name=none_obj], [name=divDA]').show();
					$parentobj.parent().parent().find('[name=none_obj], [name=divDA]').show();
					$(obj).parent().parent().parent().parent().parent().parent().parent().find('.wta_tb_mod').hide();
				}
				else {
					//$parentobj.parent().parent().css('border', 'none').find('[name=none_obj], [name=divDA]').hide();
					$parentobj.parent().parent().find('[name=none_obj], [name=divDA]').hide();
				}
			}
			else {
				var $parentobj = $(obj).parent().parent();
				if ($parentobj.css('display') == 'none')
					$parentobj.parent().find('[name=none_obj], [name=divDA]').show();
					//$parentobj.parent().css('border', '1px solid #c5c5c5').find('[name=none_obj], [name=divDA]').show();
				
				else {
					//$parentobj.parent().css('border', 'none').find('[name=none_obj], [name=divDA]').hide();
					$parentobj.parent().find('[name=none_obj], [name=divDA]').hide();
					$parentobj.find('.wtastas01').eq(0).find('.wta_tb_mod').show();
				}
			}
			
			fnpreventDefault(event);
		}
		
		function fnUserSayDetailList(SlotNo, IntentSeq) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/useruttredit.do' />");			
			comSubmit.addParam("SLOT_SEQ_NO", SlotNo);
			comSubmit.addParam("INTENT_SEQ", IntentSeq);
			comSubmit.addParam("INTENT_NAME", '${INTENT_NAME}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();			
			
			/* var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getintentusersays.do' />");
			comAjax.setCallback("fnUserSayDetailListCallBack");
			comAjax.addParam("SLOT_SEQ_NO", SlotNo);			
			comAjax.ajax(); */	
		}
		
		function fnUserSayDetailListCallBack(data) {			
			if(data.list.length > 0) {
				var str = '<h3 class="user_says_icon">User says<a onclick="fnList();" class="class_name_instances" href="#">List</a></h3>';
				$.each(data.list, function(key, value){						
					str += '<div class="user_says_add_con">';																
					str += '<div class="user_says_add_con_inp" name="EDIT_USER_SAYS">' + value.SAY_TAG + '</div>';
					str += '<a href="#" onclick="fnUserSayDetailDelete(' + value.SEQ_NO + ', this);" class="delete_btn"><span class="hide">삭제</span></a>';					
					str += '</div>';
				});
				$('#divUserSaysDetail').empty().append(str).show();
				$('#divUserSays').hide();
			}
		}
		
		function fnList() {
			$('#divUserSaysDetail').hide();
			$('#divUserSays').show();
		}
		
		function fnSetIntentsName(intentName, slotName) {
			var viewintentName = '';
		
			if (intentName.indexOf("(") > -1) {
				if (intentName.indexOf("(") + 1 == intentName.indexOf(")")) {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					viewintentName = tempName + '(' + slotName + ')';
				}
				else {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					var tempMapda = intentName.substring(intentName.indexOf("(") + 1, intentName.indexOf(")"));
					
					if (slotName == '') {
						viewintentName = intentName;
					}
					else {
						viewintentName = tempName + '(' + tempMapda + ', ' + slotName + ')';
					}
				}
			}
			else {
				viewintentName = intentName + '(' + slotName + ')';	
			}
			$('#viewIntentsName').val(viewintentName);
		}
		
		function fnSlotMapping(obj) {	
			//var str = '"나 지금 테스트 중"|"<Test2C1.c1s1=나> 지금 <TestC1.c1s1=테스트> 중"|test(Test2C1.c1s1="나", TestC1.c1s1="테스트")|FINISHED|';				
			$autoMapobj = $(obj).prev();
			
			if ($autoMapobj.text().trim() == '') {
				alert('사용자 발화를 입력하세요.');
				fnCursorEnd($autoMapobj.get(0));
				return;
			}
			
			fnLoading();
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslotmapping.do' />");
			comAjax.setCallback("fnSlotMappingCallBack");			
			comAjax.addParam("UNIQID", '<%=session.getAttribute("UNIQID")%>');
			comAjax.addParam("UTTR", $autoMapobj.text().trim().replace(/\s/g, '|@'));
			comAjax.ajax();
				
			fnpreventDefault(event);
		}
		
		function fnSlotMappingCallBack(data) {
			fnLoading();
			if (data.status == 'OK') {	
				data.slotmapping = data.slotmapping.replace(/&LT;/g, '<').replace(/&GT;/g, '>').replace(/&LB;/g, '(').replace(/&RB;/g, ')').replace(/&EQ;/g, '=').replace(/&DQ;/g, '"').replace(/&AT;/g, '@').replace(/&SL;/g, '/') .replace(/&SH;/g, '#');				
				var arr = data.slotmapping.split('|');
				var intentnm = arr[2].substring(0, arr[2].indexOf('('));			
				var arrslot = arr[1].match(/<[a-zA-Z가-힣0-9_\-]+.[a-zA-Z가-힣0-9_\-]+=[\"`',%\+\/\\?~!@#$%^&*·;:=|\(\)\[\]{}a-zA-Z가-힣0-9.\s_\-]+>/g);
				
				if (arrslot == null || arrslot.length == 0)
					alert('일치하는 slot이 없습니다.');		
				else {				
					var bintentchk = true;				
					var alertmsg = '';
					var realintentname = ''; 
					var arritem;
					var arritemsub;
					
					realintentname = $('#txtIntentsName').val().trim();
					if (realintentname == 'Intent name' || realintentname == '')
						bintentchk = false;								
								
					if (bintentchk && realintentname.indexOf('()') > -1) {
						realintentname = realintentname.substring(0, realintentname.indexOf('()'));
					}
					
					arr[1] = arr[1].substring(0, arr[1].length);
					for (var i = 0; i < arrslot.length; i++) {
						arritem = arrslot[i].substring(1, arrslot[i].length - 1).split('=');
						arritemsub = arritem[0].split('.');
						
						$.each(slotdata[arritemsub[0]], function(key, value){
							if (value.SLOT_NAME == arritemsub[1]) {
								arr[1] = arr[1].replace(arrslot[i], '<span style="color:' + value.COLOR + '">' + arritem[1] + '</span>');
								
								var objSlot = new Object();
								objSlot.seqno = value.SEQ_NO;
								objSlot.slotname = arritem[0];
								objSlot.color = value.COLOR;
								objSlot.text = arritem[1];
								
								arrSlot[objSlot.text + '_' + objSlot.color] = objSlot;
																					
								return false;
							}						
						});
					}
					
					$autoMapobj.html(arr[1]);
					if (bintentchk) {
						if (realintentname.indexOf('(') > -1) {
							if (intentnm != realintentname.substring(0, realintentname.indexOf('('))) {
								alert('현재 intent로 인식되지 않습니다. 수동으로 작업하시기 바랍니다.');
							}	
						}
						else {
							if (intentnm != realintentname) {
								alert('현재 intent로 인식되지 않습니다. 수동으로 작업하시기 바랍니다.');
							}	
						}						
					}				
				}
			}
		}
		
		function fnUpLoadIntentSubmit() {
			var agentSeq;
			var type;
			if ('${AGENT_SEQ}' == '' || '${AGENT_SEQ}' == '0') {
				type = 'AW';
				agentSeq = '0';
			}
			else
				type = 'TW';
			
			var comSubmit = new ComSubmit("UploadForm");
			comSubmit.setUrl('<c:url value='/view/insertusersayfile.do' />');
			comSubmit.addParam("TYPE", type);		
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", agentSeq);;		
			if('${SEQ_NO}' == '') {
				if (iSeqNo == '') {
					comSubmit.addParam("SEQ_NO", '');
					alert('intent 저장 후 업로드 해주세요.');
					return;
				}
				else
					comSubmit.addParam("SEQ_NO", iSeqNo);
			}
			else
				comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val());					
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();	
		}
		
		function fnSysIntentAdd(obj, type) {
			$(obj).parent().parent().append($('#divSysintentAdd').html());
			
			fnpreventDefault(event);
		}
		
		function fnSysIntentDel(obj) {
			$parentobj = $(obj).parent().parent().parent().parent().parent().parent();
			//if ($parentobj.parent().find('[name=divSysIntent]').length > 1) {
				$parentobj.remove();	
			//}
				
			fnpreventDefault(event);
		}
		
		function fnIntentDownload() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/downloadintentfile.do' />");			
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			if ($('#txtIntentsName').val().indexOf('(') > -1)
				comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val().substring(0, $('#txtIntentsName').val().indexOf('(')));
			else 
				comSubmit.addParam("INTENT_NAME", $('#txtIntentsName').val());
			comSubmit.submit();
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf"%>
	<div class="user_utterance">
		<div class="cont_title">
			<div class="Agent_name_box">
				<div class="Agent_name_box_word">
					<div class="s_word">
						<label for="search"> <c:choose>
								<c:when test="${null eq INTENT_NAME}">
									<input type="text" id="txtIntentsName" name="txtIntentsName"
										value="Intent name"
										onblur="if (this.value=='') this.value=this.defaultValue"
										onclick="if (this.defaultValue == this.value) this.value = ''" />
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${empty SLOT_SEQ_NO}">
											<input type="text" id="txtIntentsName" name="txtIntentsName"
												value='${INTENT_NAME}' />
										</c:when>
										<c:otherwise>
											<input type="text" id="viewIntentsName" readonly="readonly"
												value='' />
											<input type="hidden" id="txtIntentsName" name="txtIntentsName"
												value='${INTENT_NAME}' />
											<script>fnSetIntentsName('${INTENT_NAME}', '${UserSaylist[0].SLOT_NAME }');</script>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose> <a href="#" id="saveIntents" onclick="fnSave();" title="저장"><span
								class="project_save">Save</span></a>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="user_utterance_contents">
			<div id="divSlotLayer" class="slot_layer" style="display: none;">
				<a href="#" class="hide divSlotLayer_c" onclick="fnSlotClose();">닫기</a>
				<div id="divSlotLayerCon"></div>
			</div>
			<!-- step01 -->
			<div class="user_utterance_step01">
				<div id="divUserSays" class="sortable01">
					<h3 class="user_says_icon" style="display: inline-block;">User says</h3>
					<c:if test="${USER_TYPE ne 'OA'}">
					<a style="margin: 30px 45px 0px 0px; line-height: 26px; height: 26px;"
						onclick="fnShowUpload();" class="class_name_createslot" href="#">Upload Intent</a>
					<a style="margin: 30px 10px 0px 0px; line-height: 26px; height: 26px; background:#005ca2;"
						onclick="fnIntentDownload();" id="aIntentDown" class="class_name_createslot" href="#">Download Intent</a>
					</c:if>
					<c:forEach items="${UserSaylist }" var="row">
						<c:choose>
							<c:when test="${empty SLOT_SEQ_NO}">
								<div class="user_says_add_con">
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }"
										name="EDIT_USER_SAYS">${row.SLOT_TAG}</div>
									<a href="#"
										onclick="fnUserSayDelete('${row.SLOT_SEQ_NO}', ${row.SEQ_NO }, this);"
										class="delete_btn"><span class="hide">삭제</span></a>
									<c:choose>
										<c:when test='${row.SLOT_SEQ_NO ne ""}'>
											<a href="#"
												onclick="fnUserSayDetailList('${row.SLOT_SEQ_NO}', ${SEQ_NO });"
												class="list_btn"><span class="hide">리스트</span></a>
										</c:when>
										<c:otherwise>
											<a href="#" onclick="fnUserSayEdit(${row.SEQ_NO }, this);"
												class="modify_btn"><span class="hide">리스트</span></a>
											<a href="#" onclick="fnUserSayClear(this);" class="undo_btn"><span class="hide">리스트</span></a>
										</c:otherwise>
									</c:choose>
								</div>
							</c:when>
							<c:otherwise>
								<div class="user_says_add_con">
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }"
										name="EDIT_USER_SAYS">${row.SAY_TAG }</div>
									<a href="#"
										onclick="fnUserSayDetailDelete(${row.SEQ_NO}, this);"
										class="delete_btn"><span class="hide">삭제</span></a> <input
										type="hidden" name="hidUserSayEdit" value='${row.SLOT_MAPDA }' />
									<a href="#" onclick="fnUserSayEdit(${row.SEQ_NO }, this);"
										class="modify_btn"><span class="hide">리스트</span></a>
									<a href="#" onclick="fnUserSayClear(this);" class="undo_btn"><span class="hide">리스트</span></a>
								</div>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</div>

				<div id="divUserSaysDetail" class="sortable01"
					style="display: none;"></div>
			</div>

			<!-- step04 -->
			<div class="user_utterance_step04">
				<div id="divreplies" class="sortable04">
					<h3 class="speech_response_icon">
						System replies : <a onclick="fnResponseAdd();" href="#">+ Add</a>
					</h3>
					<c:choose>
						<c:when test="${fn:length(Intentionlist) > 0}">
							<c:forEach items="${Intentionlist}" var="row">
								<div class="wtastas" name="wtastas">
									<div class="wtastas_add mt20" name="none_obj"
										style="display: none;">
										<div class="wtastas01">
											<table>
												<colgroup>
													<col width="150px" />
													<col width="*" />
													<col width="110px" />
												</colgroup>
												<tbody>
													<tr>
														<td>
															<h3 style="margin: 0px;">시스템 발화 조건</h3>
														</td>
														<td><input class="wtastas_txt01" type="text"
															id="txtEUttr${row.SEQ_NO}" name="txtUttr"
															value="${fn:replace(row.UTTR, '\"', '&quot;')}" /></td>
														<td><a class="wtastas_btn" href="#"
															onclick="fnOpenPop(this, 'F');">Script Guide</a></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>

									<div class="wtastas_add">
										<div class="wtastas_cutn" name="none_obj" style="display:none;">
											<h3>시스템 발화</h3> 
											<c:if test="${USER_TYPE ne 'OA'}">
											<a onclick="fnSysIntentAdd(this, 'T');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
											</c:if>
											<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this);"></a>
											<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
										</div>								
										<c:choose>
											<c:when test="${fn:length(Objectlist) > 0}">
												<c:set var="EditChk" value="0" />
												<c:forEach items="${Objectlist}" var="orow">
													<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">
														<div name="divSysIntent" class="intentBox">
															<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
																<table>
																	<colgroup>
																		<col width="100px" />
																		<col width="*" />
																		<col width="110px" />
																	</colgroup>
																	<tbody>
																		<tr>
																			<td>
																				<h4>시스템 intent</h4>
																			</td>
																			<td>
																				<div class="wtastas_text_div dp_ib" name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																			</td>
																			<td>
																				<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
																			</td>
																		</tr>
																	</tbody>
																</table>			
															</div>
																														
															<c:choose>
																<c:when test="${fn:length(ObjectDtllist) > 0}">
																	<c:set var="Cnt" value="0" />
																	<c:set var="Request" value="" />																	
																	<c:forEach items="${ObjectDtllist}" var="drow">
																		<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">
																			<c:choose>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<c:choose>
																						<c:when test="${Cnt eq 0}">
																							<c:set var="Cnt" value="1" />
																							<c:set var="Request" value="${drow.OBJECT_VALUE }" />																										
																						</c:when>
																						<c:otherwise>
																							
																						</c:otherwise>
																					</c:choose>															
																				</c:when>
																				<c:otherwise>
																					<c:choose>
																						<c:when test="${Cnt eq 0}">
																							<c:set var="Cnt" value="1" />
																							<div class="wtastas01 btn_hover">
																								<table>
																									<colgroup>
																										<col width="*" />
																										<col width="110px" />
																									</colgroup>
																									<tbody>
																										<tr>
																											<td>																											
																												<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																											</td>
																											<td>																																																								
																												<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>																												
																												<c:choose>
																													<c:when test='${EditChk == 0}'>
																														<c:set var="EditChk" value="1" />
																														<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																													</c:when>
																													<c:otherwise>
																														<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																													</c:otherwise>
																												</c:choose>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</div>
																						</c:when>
																						<c:otherwise>
																							<c:choose>
																								<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																									<div class="wtastas02 btn_hover">
																										<table>
																											<colgroup>
																											    <col width="42px" />
																												<col width="*" />
																												<col width="110px" />
																											</colgroup>
																											<tbody>
																												<tr>
																												    <td class="liImg">
																													   
																													</td>
																													<td>
																														<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true">${drow.OBJECT_VALUE }</div>
																													</td>
																													<td>
																														<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this);"></a>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</div>
																								</c:when>
																								<c:otherwise>
																									<c:choose>
																										<c:when test="${Cnt eq 1}">
																											<c:set var="Cnt" value="2" />
																											<div class="wtastas01 btn_hover">
																												<table>
																													<colgroup>
																														<col width="120px" />
																														<col width="*" />
																														<col width="110px" />
																													</colgroup>
																													<tbody>
																														<tr>
																															<td>
																																<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true">${Request }</div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
																															</td>
																															<td>									
																																<div class="wtastas_text_div dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>
																															</td>
																															<td>
																																<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>
																																<c:choose>
																																	<c:when test='${EditChk == 0}'>
																																		<c:set var="EditChk" value="1" />
																																		<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>	
																																	</c:when>
																																	<c:otherwise>
																																		<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
																																	</c:otherwise>
																																</c:choose>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																											</div>
																										</c:when>
																										<c:otherwise>
																											<div class="wtastas02 btn_hover" style="padding-left:110px;">
																												<table>
																													<colgroup>
																													    <col width="42px" />
																														<col width="*" />
																														<col width="110px" />
																													</colgroup>
																													<tbody>
																														<tr>
																														    <td class="liImg">
																															   
																															</td>
																															<td>																														
																																<div class="wtastas_text_div dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true">${drow.OBJECT_VALUE }</div>																												
																															</td>
																															<td>
																																<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this);"></a>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																											</div>
																										</c:otherwise>
																									</c:choose>
																								</c:otherwise>
																							</c:choose>
																						</c:otherwise>
																					</c:choose>
																				</c:otherwise>
																			</c:choose>
																		</c:if>
																	</c:forEach>
																</c:when>
																<c:otherwise>
																														
																</c:otherwise>
															</c:choose>
															
															<c:if test="${fn:indexOf(orow.OBJECT_VALUE, 'request') > -1}">
																<div name="divDA" style="padding-left: 64px; display:none;">
																	<span style="font-size: 14px;">- 다음 사용자 발화 DA 제한</span>&nbsp;
																	<input type="text" name="LIMIT_DA" value="${orow.LIMIT_DA}" class="wtastas_text_div dp_ib" style="width: 59%;">
																</div>
															</c:if>
														</div>																								
													</c:if>
												</c:forEach>
											</c:when>
											<c:otherwise>
												
											</c:otherwise>
										</c:choose>																
									</div>

									<div class="wtastas_add" name="none_obj" style="display: none;">
										<div class="wtastas01">
											<table>
												<colgroup>
													<col width="150px" />
													<col width="*" />
													<col width="110px" />
												</colgroup>
												<tbody>
													<tr>
														<td>
															<h3 style="margin: 0px;">시스템 발화 후 행동</h3>
														</td>
														<td><input class="wtastas_txt01" type="text"
															id="txtEAction${row.SEQ_NO}" name="txtAction"
															value="${fn:replace(row.ACT, "\"", "&quot;")}" /></td>
														<td><a class="wtastas_btn" href="#"
															onclick="fnOpenPop(this, 'A');">Script Guide</a></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:when>
					</c:choose>
				</div>
			</div>
		</div>
	</div>

	<div id="divAdd" style="display: none;">
		<div class="user_says_add_con">
			<div class="user_says_add_con_inp" name="USER_SAYS"
				contenteditable="true"></div>
			<a href="#" onclick="fnSlotMapping(this);" class="mapping_btn"><span
				class="hide">슬롯매핑</span></a> <input type="hidden" name="hidUserSays" />
				<a href="#" onclick="fnUserSayClear(this);" class="undo_btn"><span class="hide">리스트</span></a>
			<!-- <input type="hidden" name="hidSlotTag" />
			<input type="hidden" name="hidSlotName" />
			<input type="hidden" name="hidSlotLeng" />
			<input type="hidden" name="hidSlotMap" />
			<input type="hidden" name="hidSlotMapDa" /> -->
			<!-- <a href="#" class="delete_btn"><span class="hide">삭제</span></a>
			<a href="#" class="list_btn"><span class="hide">리스트</span></a> -->
		</div>
	</div>

	<div id="divrepliesAdd" style="display: none;">
		<div class="wtastas" name="wtastas">
			<div class="wtastas_add mt20" name="none_obj" style="display: none;">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<h3 style="margin: 0px;">시스템 발화 조건</h3>
								</td>
								<td><input class="wtastas_txt01" type="text" id="txtUttr§"
									name="txtUttr" value="true"></td>
								<td><a class="wtastas_btn" href="#"
									onclick="fnOpenPop(this, 'F');">Script Guide</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="wtastas_add">
				<div class="wtastas_cutn" name="none_obj" style="display:none;">
					<h3>시스템 발화</h3> 
					<c:if test="${USER_TYPE ne 'OA'}">
					<a onclick="fnSysIntentAdd(this, 'T');" href="#" style="color:red; font-size:12px;">&nbsp;+add</a>
					</c:if>
					<a href="#" class="wta_del" onclick="fnUserObjectDelete(1, this);"></a>
					<a href="#" class="wta_mod" onclick="fnObjectView(this, 2);"></a>
				</div>

				<div name="divSysIntent" class="intentBox">
					<div class="sys_intent" name="none_obj" style="height:auto; display:none;">						
						<table>
							<colgroup>
								<col width="100px" />
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<h4>시스템 intent</h4>
									</td>
									<td>
										<div class="wtastas_text_div dp_ib" name="OBJECT_NAME" contenteditable="true">inform()</div>
									</td>
									<td>
										<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
						
					<div class="wtastas01 btn_hover">
						<table>
							<colgroup>
								<col width="*" />
								<col width="110px" />
							</colgroup>
							<tbody>
								<tr>
									<td>
										<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
										<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
									</td>
									<td>
										<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>
										<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="wtastas_add" name="none_obj" style="display: none;">
				<div class="wtastas01">
					<table>
						<colgroup>
							<col width="150px" />
							<col width="*" />
							<col width="110px" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<h3 style="margin: 0px;">시스템 발화 후 행동</h3>
								</td>
								<td><input class="wtastas_txt01" type="text"
									id="txtAction§" name="txtAction"></td>
								<td><a class="wtastas_btn" href="#"
									onclick="fnOpenPop(this, 'A');">Script Guide</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="divDetailSub" style="display: none;">
		<div class="wtastas02 btn_hover">
			<table>
				<colgroup>
					<col width="42px" />
					<col width="*" />
					<col width="110px" />
				</colgroup>
				<tbody>
					<tr>
						<td class="liImg"></td>
						<td>
							<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME"
								contenteditable="true"></div>
						</td>
						<td><a href="#" class="wta_tb_del"
							onclick="fnUserObjectDelete(2, this);"></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="divSysintentAdd" style="display:none;">
		<div name="divSysIntent" class="intentBox">
			<div class="sys_intent" name="none_obj" style="height:auto;">
				<table>
					<colgroup>
						<col width="100px" />
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<h4>시스템 intent</h4>
							</td>
							<td>
								<div class="wtastas_text_div dp_ib" name="OBJECT_NAME" contenteditable="true">inform()</div>
							</td>
							<td>
								<a href="#" style="padding:6px;" onclick="fnSysIntentDel(this);">delete</a>	
							</td>
						</tr>
					</tbody>
				</table>			
			</div>
				
			<div class="wtastas01 btn_hover">
				<table>
					<colgroup>
						<col width="*" />
						<col width="110px" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
								<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
							</td>
							<td>
								<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>
								<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);" style="display:none;"></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div id="divInformAdd" style="display:none;">
		<table>
			<colgroup>
				<col width="*" />
				<col width="110px" />
			</colgroup>
			<tbody>
				<tr>
					<td>
						<!-- <input class="wtastas_txt01" type="text" id="txtUttrT1_0" name="txtUttr" value="true"> -->
						<div class="wtastas_text_div dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div>
					</td>
					<td>
						<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>
						<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="divRequestAdd" style="display:none;">
		<table>
			<colgroup>
				<col width="120px" />
				<col width="*" />
				<col width="110px" />
			</colgroup>
			<tbody>
				<tr>
					<td>
						<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><input type="hidden" name="SLOT_SEQ_NO" /><a class="close_btn f_right" href="#"></a></div>									
					</td>
					<td>									
						<div class="wtastas_text_div dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
					</td>
					<td>
						<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(1, this);"></a>
						<a href="#" class="wta_tb_mod" onclick="fnObjectView(this, 1);"></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="divDetailSubP" style="display:none;">
		<div class="wtastas02 btn_hover" style="padding-left:110px;">
			<table>
				<colgroup>
				    <col width="42px" />
					<col width="*" />
					<col width="110px" />
				</colgroup>
				<tbody>
					<tr>
					    <td class="liImg">
						   
						</td>
						<td>
							<div class="wtastas_text_div dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div>
						</td>
						<td>
							<a href="#" class="wta_tb_del" onclick="fnUserObjectDelete(2, this);"></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<input type="hidden" id="hidSlotAdd" />
	<input type="hidden" id="hidSlotColor" />

	<form id="UploadForm" name="UploadForm" enctype="multipart/form-data"
		method="post">
		<div class="layer" id="layer">
			<div class="bg"></div>
			<div id="divUpload" class="layer_box w400" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display: none;">
				<p style="font-weight:bold; font-size:12px; padding-bottom: 10px;">
					*UTF-8 형식의 텍스트 파일로 업로드 요망.					
				</p>
				<p style="font-weight:bold; font-size:12px; padding-bottom: 10px;">
					*이름이 같지 않은 intents는 저장되지 않습니다.					
				</p>
				<p style="font-weight:bold; font-size:12px; padding-bottom: 10px;">
					*작성하지 않은 Entities가 포함되어 있으면 저장되지 않습니다.					
				</p>
				<p style="padding-bottom: 10px;">
					<input type="file" id="fupload" name="fupload" />
				</p>
				<div class="layer_box_btn">
					<a onclick="fnUpLoadIntent();" href="#">Upload</a> <a
						onclick="fnShowUploadClose();" href="#">Cancel</a>
				</div>
			</div>
		</div>
	</form>
	<%@ include file="/WEB-INF/include/footer.jspf"%>
</body>
</html>