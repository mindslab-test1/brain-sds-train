<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	
	<script type="text/javascript">
		window.onload = function() {
			
		}
		
		function fnSave() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/inserteditintent.do' />");
			
			if ('${editintent.SEQ_NO}' != '') {
				comSubmit.addParam("SEQ_NO", '${editintent.SEQ_NO}');	
			}
			comSubmit.addParam("EDIT_VALUE", $('#txtEdit').val());
			comSubmit.submit();			
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW entities entitiesDetail intents">
			<div class="btnBox">
				<div class="save" onclick="fnSave();">SAVE</div>
			</div>
			<div class="liBox liBox3">
				<div class="title slotCond">Edit Intents</div>
				<div class="conBox tableBox slotCondBox">
					<textarea id="txtEdit">${editintent.EDIT_VALUE }</textarea>
				</div>
			</div>
		</div>
	</div>			
</body>
</html>