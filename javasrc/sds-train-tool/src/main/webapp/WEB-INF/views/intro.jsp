<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>대화모델링구축도구</title>	
	<%@ include file="/WEB-INF/include/header.jsp" %>
		
	<script type="text/javascript">		
		$(function() {
			$('#videoPlayer').hide();
		});
		
		function fnPlayVideo(idx) {
			$('#play').hide();
			$('#videoPlayer').show();
			fnPlay();
		}
		
		function fnSelectSource(obj) {
			$('#source1').attr("src", $(obj).val());
			fnPlay();
		}
		
		function fnPlay() {
			$("#videoPlayer").load();
			document.getElementById("videoPlayer").play();
		}
		
	</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div class="middleW index">
			<div class="liBox">
				<div class="title">한글 대화로 가능한 인터페이스를 만들어 보세요.</div>
				<div class="exBox ex">
					<ul>
						<li>먼저 Domain을 생성하세요.</li>
						<li>Domain에서 하는 일을 Tasks로 나누어 보세요.</li>
						<li>Domain에서 대화할 의미를 Entities에서 정의해 보세요.</li>
						<li>내가 말할 때 응답할 내용을 Intents에서 작성하세요.</li>
						<li>학습하시고 내가 만든 Domain에서 대화해 보세요.</li>
					</ul>					
				</div>				
				<div style="text-align: center; margin-top: 10px;">
					<span>Quick Start 동영상</span>
					<select id="ddlSource" onchange="fnSelectSource(this);" style="border:1px solid #ddd; border-radius:5px;">
						<option value="../video/01.mp4">Quick Guide #1 : 간단한 대화 만들기</option>
						<option value="../video/02.mp4">Quick Guide #2 :  피자주문 대화 만들기</option>
					</select>
				</div>
				<div class="exBox movie">
					<div id="play" class="play" onclick="fnPlayVideo(1);"></div>
					<video id="videoPlayer" poster="movie.jpg" width="100%" height="100%" controls="controls">
					        <source id="source1" src="../video/01.mp4" type="video/mp4;" />					       
					        <p>지원하지 않는 브라우저입니다.</p>
					</video>
				</div>
			</div>			
			<div class="liBox liBox6">
				<div class="title">새로운 도메인 대화를 만들어 보세요.<div id="divCreateDomain" class="btn domain">Domain</div></div>
			</div>
			<div class="liBox liBox6">
				<div class="title">사용법을 익혀 보세요.<div class="btn docs" onclick="fnManual();">Manual</div></div>
			</div>
			<span style="margin-top:50px; font-size:11px;">본 사이트는 Chrome 브라우저에 최적화 되어 있습니다. (Microsoft Edge, Explorer 11 버전 이상에서 이용 가능)</span>
		</div>
	</div>
</body>
</html>