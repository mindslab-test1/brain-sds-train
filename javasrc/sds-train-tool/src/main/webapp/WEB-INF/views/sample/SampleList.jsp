<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		window.onload = function() {
			fnSetNaviTitle('Samples');			
		}		
				
		function fnDomainList(obj) {
			fnWinPop("<c:url value='/view/userdomain.do' />" + "?&userid=" + $(obj).attr('id') + "&type=sa", "DomainPop", 550, 408, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnAddRow() {
			var cnt = $('#tbSample').find('[name=trAdd]').length;		
			var str = '<tr class="knowledge_th01" name="trAdd">';			
			str += '<td>&nbsp;</td>';
			str += '<td><input type="text" name="txtName" /></td>';
			str += '<td><input type="text" id="txtDomain' + cnt + '" name="txtDomain" readonly="readonly" onclick="fnDomainList(this);" /><input type="hidden" name="hidPrjSeq" /><input type="hidden" name="hidPrjUserSeq" /></td>';
			str += '<td>&nbsp;</td>';
			str += '</tr>';
			
			$('#tbSample tbody').append(str);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();
			var msg = '';			
			var bInserChk = false;
			
			$('#tbSample tbody [name=trAdd]').each(function(idx, e) {
				var item = new Object();				
				var strvalues = 'VALUES(';
				var $input;
				var cnt = 0;
				
				$(this).find('input').each(function() {
					var itemsub = new Object();
					$input = $(this);
					if($input.val().trim() != '') {
						if (strvalues == 'VALUES(')
							strvalues += '"' + $input.val() + '"';	
						else
							strvalues += ', "' + $input.val() + '"';
						
						cnt ++;
					}
					else {
						if ($input.attr('name') == 'txtName') {
							msg = '이름을 입력해주세요.';
						}
						else if ($input.attr('name') == 'txtDomain') {
							mst = '도메인을 입력해주세요.';
						}
					}					
				});
				
				if (cnt == 4) {
					strvalues += ')';
					item.ITEM_DETAIL = strvalues;
					arrobj.push(item);
				}
				else if (cnt != 0) {
					bInserChk = true;
				}
			});
			
			if(bInserChk) {
				alert(msg);
				return;
			}
			
			if (arrobj.length > 0) {
				arr.ITEM = arrobj;
				var jsoobj = JSON.stringify(arr);
				
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertsampledomain.do' />");			
				comSubmit.addParam("VALUES_ITEM", jsoobj);
				comSubmit.submit();	
			}			
			fnpreventDefault(event);
		}
		
		function fnDelete() {
			var arrseq = new Array();
			$('[name=sampleChk]').each(function() {
				if ($(this).prop('checked') == true) {
					arrseq.push($(this).val())
				}
			});
			
			if (arrseq.length > 0) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deletesampledomain.do' />");
				comSubmit.addParam("ARR_SEQ_NO", arrseq.toString());
				comSubmit.submit();
				fnpreventDefault(event);
			}
			else
				alert('삭제할 샘플을 선택해주세요.');			
		}		
	</script>	
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>		
		<div class="middleW instances" style="padding:10px 20px 20px 260px;">
			<div class="btnBox">
				<div class="save" onclick="fnSave();">Save</div>
				<div class="save" onclick="fnAddRow();">Add</div>
				<div class="save" onclick="fnDelete();">Delete</div>
			</div>			
			<div class="liBox liBox4 liBox5 table">
				<h3 class="table_h3" style="cursor:default !important;">Sample 리스트</h3>
				<div class="admin_user_con entities_ex_con">
					<div class="admin_user_title">
						<table id="tbSample" class="slot_instan_title">							
							<colgroup>
								<col width="50px">
								<col width="30%">								
								<col width="">								
								<col width="20%">
							</colgroup>
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>이름</th>
									<th>도메인</th>
									<th>작성자</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="Chk" value="1"></c:set>
								<c:forEach items="${list}" var="row">
									<c:choose>
										<c:when test="${Chk == 1}">
											<tr class="knowledge_th01">
											<c:set var="Chk" value="2"></c:set>
										</c:when>
										<c:otherwise>
											<tr class="knowledge_th02">
											<c:set var="Chk" value="1"></c:set>
										</c:otherwise>
									</c:choose>										
											<td><input type="checkbox" id="sampleChk${row.SEQ_NO }" name="sampleChk" value="${row.SEQ_NO }"></td>
											<td>${row.SAMPLE_NAME }</td>
											<td>${row.PROJECT_NAME }</td>
											<td>${row.USER_NAME }</td>
										</tr>													
								</c:forEach>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>