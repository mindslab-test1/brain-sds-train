<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var str = '';
		var total = 0;
		var rowcount = 10;
		var $txtObj;
		
		window.onload = function() {
			fnCreateHead(${SEQ_NO});
			$("#search_option").trigger("chosen:updated");
			$('#trHead').append(str);
			
			var iwidth = '100%';
			var iheadLeng = $('#trHead th').length;
			
			if(iheadLeng > 5)
				iwidth = iheadLeng * 120;
			
			$('#tbslot').css('min-width', iwidth);
			
			$('#tbslot').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddRow($("#tbslot > tbody"));
					}
				},
				focusout: function(e) {
					var $this = $(this);	
					var id = $this.attr('id');					
					
					var comAjax = new ComAjax();
					comAjax.addParam("INSTANCE_VAL", $this.val());
					
					if(!gfn_isNull(id)) {
						comAjax.addParam("SEQ_NO", $this.attr('id').replace('txt', ''));
						if($this.val().trim() != '') {						
							comAjax.setUrl("<c:url value='/view/updateslotinstansces.do' />");
						}
						else {
							comAjax.setUrl("<c:url value='/view/deleteslotinstansces.do' />");
							$this.removeAttr('id');
						}
							
						comAjax.ajax();
					}
					else {
						if($this.val().trim() != '') {
							$txtObj = $this;
							comAjax.addParam("SLOT_SEQ_NO", $this.parent().attr('name').replace('td', ''));
							comAjax.addParam("CLASS_SEQ_NO", ${SEQ_NO});
							comAjax.setCallback("fnSaveCallBack");
							comAjax.setUrl("<c:url value='/view/insertslotinstansces.do' />");
							comAjax.ajax();							
						}
					}																				
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().parent().prev().find('td').eq($(this).parent().index()).find('input:text').focus();
					} else if(e.keyCode == 40) {
						$(this).parent().parent().next().find('td').eq($(this).parent().index()).find('input:text').focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=OBJECT]');
			
			fnSelectList(1);						
		}
		
		function fnCreateHead(seqno) {
			$.each(slotdata[seqno], function(key, value){
				if (value.SLOT_TYPE != 'C') {
					if(value.CLASS_SEQ_NO == ${SEQ_NO})
						str += '<th id="th' + value.SEQ_NO + '">' + value.SLOT_NAME + '</th>';
					else
						str += '<th id="th' + value.SEQ_NO + '">' + value.CLASS_NAME + '.' + value.SLOT_NAME + '</th>';
					$('#search_option').append('<option value="' + value.SEQ_NO + '">' + value.SLOT_NAME + '</option>');					
				}
				else {
					fnCreateHead(value.TYPE_SEQ);
				}
			});
		}
		
		function fnCreateBody() {
			str = '';
			var iRowColor = true;											
			
			for (var i = 0; i < rowcount; i++) {				
				if (iRowColor) {
					str += '<tr class="knowledge_th01">';
					iRowColor = false;
				}
				else {
					str += '<tr class="knowledge_th02">';
					iRowColor = true;
				}
				$('#trHead th').each(function() {
					str += '<td name="td' + $(this).attr('id').replace('th','') + '"><input type="text" class="none_focus" name="OBJECT" /></td>'; 						
				});
				
				str +='</tr>'; 
			}
			$('#tbslot tbody').append(str);
			str = '';
		}
		
		function fnRemoveBody(page) {
			var iCnt = (rowcount * page) - total;
			iCnt = rowcount - iCnt;
			
			for (var i = iCnt; i < rowcount; i++) {
				$('#tbslot tbody tr').eq(iCnt).remove();
			}
		}
		
		
		function fnSelectList(pageNo){
			total = 0;
			$("#tbslot > tbody").empty();
			fnCreateBody();			
			$('#trHead th').each(function() {
				var slotid = $(this).attr('id').replace('th', '');
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/selectslotinstansces.do' />");
				comAjax.setCallback("fnSelectListCallBack");
				comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
				comAjax.addParam("PAGE_ROW", rowcount);				
				comAjax.addParam("SLOT_SEQ_NO", slotid);
				comAjax.addParam("CLASS_SEQ_NO", ${SEQ_NO});
				$txtSearch = $("#txtSearch");
				var strsearch = '';
				if($txtSearch.val().trim() != '' && $txtSearch.val() != 'search…') {
					if ($("#search_option").val() == "0") {
						strsearch = 'AND INSTANCE_VAL = "' + $txtSearch.val() + '"';	
					}
					else {
						if ($("#search_option").val() == slotid) {
							strsearch = 'AND INSTANCE_VAL = "' + $txtSearch.val() + '"';	
						}
					}					
				}
					
				comAjax.addParam("WHERE_SQL", strsearch);
				comAjax.ajax();
				fnpreventDefault(event);
			});
			fnRemoveBody(pageNo);
			AddRow($("#tbslot > tbody"));
			var params = {
				divId : "PAGE_NAVI",
				pageIndex : "PAGE_INDEX",
				totalCount : total,
				recordCount : rowcount,
				eventName : "fnSelectList"					
			};
			gfn_renderPaging(params);
		}
						
		function fnSelectListCallBack(data) {
			if (data.TOTAL > total)
				total = data.TOTAL;	
			
			var body = $("#tbslot > tbody");
			//body.empty();
			if(total > 0){											
				iCnt = 0;
				$.each(data.list, function(key, value){		
					$input = $('#tbslot tbody tr').eq(iCnt).find('td[name=td' + value.SLOT_SEQ_NO + ']').find('input:text');
					$input.val(value.INSTANCE_VAL);
					$input.attr('id', 'txt' + value.SEQ_NO);
					
					iCnt ++;
				});
			}
		}
		
		function AddRow(el) {
			var strtag = "<tr name='trIn'>";
			
			$('#trHead th').each(function() {
				strtag += "<td name='td" + $(this).attr('id').replace('th', '') + "'><input type='text' value='' name='OBJECT' /></td>";
			});	
			strtag += "</tr>";				
			el.append(strtag);
		}				
		
		function fnSaveCallBack(data) {
			$txtObj.attr('id', 'txt' + data.SEQ_NO);
			$txtObj = null;
		}
		
		function fnUpLoadInstances(Type) {
			if(Type == 'O') {
				$("#divExcelUpload").show();
			}
			else {
				var comSubmit = new ComSubmit("ExcelForm");
				comSubmit.setUrl("<c:url value='/view/setslotinstanscesexcel.do' />");
				comSubmit.addParam("SEQ_NO", ${SEQ_NO});
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
				comSubmit.addParam("HEADER_LIST", arrThead.toString());
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnExportInstances() {
			var arrThead = new Array();
			$('#trHead th').each(function() {
				arrThead.push($(this).text() + '§' + $(this).attr('id').replace('th', '')); 						
			});
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getslotinstanscesexcel.do' />");
			comSubmit.addParam("HEADER_LIST", arrThead.toString());			
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
			comSubmit.addParam("SEQ_NO", ${SEQ_NO});
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="slot">
		<div class="cont_title">
			<h2>Entities</h2>			
			<div style="font-size: 14px; position: absolute; top: 70px; left: 65px; color: #666; line-height: 14px;">
				<!-- <span style="padding:0px 5px;">aaaaa</span><span style="padding:0px 5px;">&gt;</span> -->
				
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">
				<img style="width: 16px; vertical-align: text-top; margin-right: 3px;" src="../images/icon_c_img.png" />${CLASS_NAME }</span><span style="padding:0px 5px;">&gt;</span>
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">Instances</span>
			</div>
			<!-- <a href="#" onclick="fnSave();">Save</a> -->									
		</div>
		
		<div class="solt_contents">	
			<div class="sel_sea">	
			<!--search box start-->
				<div style="width:24%; margin-right:1%; display: inline-block;">
					<select class="f_right u_group" id="search_option" name="search_option" data-placeholder="검색조건을 선택하세요">>
						<option value="0">All</option>					
					</select>
				</div>
				<div class="search_box" style="height:auto; width: 75%; float: right; display: inline-block;">
					<div class="s_word">					
						<label for="slot_search">
							<input style="width:85% !important;" type="text" id="txtSearch" value="search…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" /><a onclick="fnSelectList(1);" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>						
						</label>
					</div>
				</div>
				<!--search box end-->
			</div>
			
			<dl class="class_name">
				<dt style="margin-right: 17px; margin-left: 17px;">
					<span>${CLASS_NAME}</span>
					<a onclick="fnUpLoadInstances('O');" class="class_name_del" style="display:none;" href="#">Upload instances</a>					
					<a onclick="fnExportInstances();" class="class_name_instances" href="#">Export instances</a>
											
				</dt>
			</dl>
			
			<form id="ExcelForm" name="ExcelForm" enctype="multipart/form-data" method="post">
				<div id="divExcelUpload" class="layer_box w400" style="display:none;">
					<p style="padding-bottom: 10px;"><input type="file" name="excelFile"/></p>
					<div class="layer_box_btn">
						<a onclick="fnUpLoadInstances('S');" href="#">Upload</a>
						<a onclick="javascript:$('#divExcelUpload').hide();" href="#">Cancel</a>			
					</div>		
				</div>
			</form>
			
			<div style="margin:15px; border: 1px solid #e1e1e1; border-bottom: none;overflow: auto;">							
				<table id="tbslot" style="" class="slot_instan_title" summary="TASK_INFORMATION 내용을 보여줍니다.">
					<caption class="hide">Instances 표</caption>					
					<thead>
						<tr id="trHead">							
						</tr>						
					</thead>
					<tbody>
						<!-- <tr>
							<td>								
								<input type="text" value="한국전자통신 연구원" id="knowledge_input07" name="knowledge_input01" />								
							</td>
							<td>								
								<input type="text" value="에버트란" id="knowledge_input07" name="knowledge_input02" />								
							</td>
							<td>								
								<input type="text" value="영화표발행" id="knowledge_input08" name="OBJECT" />
							</td>							
						</tr>	 -->					
					</tbody>
					<!-- <tfoot>
						<tr>
							<td colspan="12"><a href="#">Click here to edit row</a></td>
						</tr>
					</tfoot> -->
				</table>			
			</div>
			<!--board paging start-->
			<div id="PAGE_NAVI" class="board_paging">
				<!-- <span class="left">
					<a href="#" class="front"><img src="../images/btn_front.png" /></a>
					<a href="#" class="prev"><img src="../images/btn_prev.png" /></a>
				</span>
				<span class="num_group">
					<a class="on" href="#" title="1번째 페이지 목록 보기">1</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">2</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">3</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">4</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">5</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">6</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">7</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">8</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">9</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">10</a>&nbsp;
				</span>
				<span class="right">
					<a href="#" class="next"><img src="../images/btn_next.png" /></a>
					<a href="#" class="back"><img src="../images/btn_back.png"  /></a>
				</span> -->
			</div>			
			<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			<!--board paging end-->
		</div>
	</div>		
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>