<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var iSeqNo = '';
		var bsaving = false;
		var bupdatechk = false;	
		var CheckCnt;
		var CheckCntMax;
		
		window.onload = function() {			
			fnSetNaviTitle('Entities > 슬롯 단어 정의');
			$('#ObjectList').on({ 
				'focusout': function(e){			
					if(gfn_isNull($(this).text()))
						$(this).text('입력 하세요...');
				},
				focus: function(e) {
					if($(this).text() == '입력 하세요...')
						$(this).text('');
				},
				click: function(e){			
					
				},
				keydown: function(e) {									
					if(e.keyCode == 13) {
						fnpreventDefault(e);
						fnAddRow();
						return false;
					}
					else {
						bWorkChk = true;	
						if(!gfn_isNull($(this).attr('id'))) {
							bupdatechk =  true;	
						}
					}
				}
			}, '[name=OBJECT_NAME]');
						
			$('#solt_contents').on({			
				'click focusin': function(e) {
					var $this = $(this);
					if ($this.text() != '') {
						if($this.parent().next().length == 0) {
							$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#" onclick="fnObjectDeleteDetail(0, this);"></a></div>');
						}						
					}								
				},
				keydown: function(e) {
					if(e.keyCode == 9) {
						$this = $(this);
						//$(this).next().focus();
						if ($this.text().trim() != '') {
							if ($this.parent().next().length == 0) {
								$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#" onclick="fnObjectDeleteDetail(0, this);"></a></div>');									
							}
							$this.parent().next().children().eq(0).focus();
							fnCursorEnd($this.parent().next().children().eq(0).get(0));										
						}						
						else {
							var $obj = $(this).parent().parent().parent().parent().parent().parent().next().find('tbody tr td div').children().eq(0);
							if ($obj.length > 0) {
								$obj.focus();
								fnCursorEnd($obj.get(0));
							}
							else
								return false;
						}							
						
						fnpreventDefault(e);
					} else if(e.keyCode == 40) {
						var $obj = $(this).parent().parent().parent().parent().parent().parent().next().find('tbody tr td div').children().eq(0);
						//if ($obj.attr('name') == 'tbOBJ_LIST') {
							$obj.focus();
							fnCursorEnd($obj.get(0));
						//}
						fnpreventDefault(e);
					} else if(e.keyCode == 38) {
						var $obj = $(this).parent().parent().parent().parent().parent().parent().prev().find('tbody tr td div').children().eq(0);
						//if ($obj.attr('name') == 'tbOBJ_LIST') {
							$obj.focus();							
						//}
						fnpreventDefault(e);
					} else if(e.keyCode == 13) {
						fnpreventDefault(e);
						fnAddRow();
						return false;
					}
					else {
						bWorkChk = true;						
						if(!gfn_isNull($(this).attr('id'))) {
							bupdatechk =  true;	
						}						
					}
				}
			}, '[name=OBJECT_DETAIL_NAME]');
								
			$('#txtSearch').bind('keydown', function(e) {
				if(e.keyCode == 13) { 
					fnSelectList();
				}
			});
						
			fnSelectList();
			
			if ('${STATUS}' == 'F') {
				alert('엑셀 업로드 중 중복된 값을 발견 했습니다. 제거 후 다시 시도해주세요.');
			}
			else if ('${STATUS}' == 'MC') {
				alert('한번에 업로드 할 수 있는 대표값 수는 30개 입니다.');
			}
			else if ('${STATUS}' == 'SC') {
				alert('한번에 업로드 할 수 있는 동의어 수는 20개 입니다.');
			}
			else if ('${STATUS}' == 'TC') {
				alert('저장 가능한 슬롯 단어 수를 초과 했습니다.');				
			}
		}
				
		function fnSelectList(pageNo) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslotobject.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX", $("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 20);			
			comAjax.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});
			$txtSearch = $("#txtSearch");
			var strsearch = '';
			var searchval = '';
			
			if($txtSearch.val().trim() != '' && $txtSearch.val() != 'search…') {
				strsearch = 'AND (OBJECT_NAME LIKE "§'+ $txtSearch.val() + '§"#EVR)';
			}
			comAjax.addParam("OBJECT_NAME", $txtSearch.val());
			comAjax.addParam("WHERE_SQL", strsearch);
			comAjax.ajax();
			fnpreventDefault(event);			
		}
						
		function fnSelectListCallBack(data) {
			var iCnt = 0;
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			$("#ObjectList > dd").remove();;
			$("#TOTAL_COUNT").val(total);
			
			if(total == 0){
				fnAddRow();
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var str = "";
				$.each(data.list, function(key, value){
					str = "";
					str += '<dd class="solt_dd">';
					str += '<table name="tbOBJ_LIST" class="slot_modify">';
					str += '<tr><th>';
					str += '<div id="OBJECT_NAME_' + value.SEQ_NO + '" onfocusout="fnObjectUpdate(' + value.SEQ_NO + ', this, \'O\');" name="OBJECT_NAME" contenteditable="true">' + value.OBJECT_NAME + '</div>';
					str += '<input type="hidden" id="hid_OBJECT_NAME_' + value.SEQ_NO + '" value="' + value.OBJECT_NAME + '" />';
					str += '</th>';
					str += '<td class="slot_modify01" id="td_detail_' + value.SEQ_NO + '"-">';
					str += '</td><td class="slot_modify02">';
					str += '<a href="#" class="delete_btn" onclick="fnObjectDelete(' + value.SEQ_NO + ', this);"><span class="hide">삭제</span></a>';					
					str +='</td></tr>';
					str +='</table></dd>';
					$('#ObjectList').append(str);
					fnGetObjDetail(value.SEQ_NO);
				});						
			}
		}						
						
		function fnGetObjDetail(seqno) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslotobjectdetail.do' />");
			comAjax.setCallback("fnGetObjDetailCallBack");
			comAjax.addParam("PARENT_SEQ_NO", seqno);			
			comAjax.ajax();				
		}			
		
		function fnGetObjDetailCallBack(data) {
			var iCnt = 0;
			var str = "";			
			$.each(data.DETAIL_LIST, function(key, value){
				str += '<div class="slot_modify_set02"><div class="dp_ib" id="' + value.SEQ_NO + '" " onfocusout="fnObjectUpdate(' + value.SEQ_NO + ', this, \'OD\');" name="OBJECT_DETAIL_NAME" contenteditable="true">' + value.OBJECT_NAME + '</div><a class="close_btn f_right" href="#" onclick="fnObjectDeleteDetail(' + value.SEQ_NO + ', this);"></a>';
				str += '<input type="hidden" id="hid_' + value.SEQ_NO + '" value="' + value.OBJECT_NAME + '" />';
				str += '</div>';
				iCnt++;
			});
			
			if(iCnt == 0)
				str = '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#" onclick="fnObjectDeleteDetail(0, this);"></a></div>';
			
			$('#td_detail_' + data.PARENT_SEQ_NO).append(str);
		}	
		
		function fnSlotSave() {
			if (bsaving == true) {
				fnLoading();
				return false;
			}
			else 				
				bsaving = true;
	
			fnLoading();
			
			var arr = new Object();
			var arrobj = new Array();
			var saveChk = true;
			var arrobjChk = new Array();
			Array.prototype.contains = function(elem) {
				for (var i in this) {
					if (this[i] == elem) return true;
				}
				return false;
			}
			
			var iobjCount = 0;
			
			$('#ObjectList [name=tbOBJ_LIST]').each(function(idx, e) {
				var $this = $(this);
				var item = new Object();
				var arrobjsub = new Array();
				var id = $this.find('[name=OBJECT_NAME]').attr("id");
				var seqno;
				var bObjName = true;
				var iObjDtl = 0;
				
				if(gfn_isNull(id)) {
					item.SEQ_NO = "0";
				} else {
					item.SEQ_NO = id.replace('OBJECT_NAME_', '');					
				}
				
				item.OBJECT_NAME = $this.find('[name=OBJECT_NAME]').text().trim();
				if (item.OBJECT_NAME == '입력 하세요...' || item.OBJECT_NAME.trim() == '') {
					bObjName = false;
				}
				else {
					if (item.SEQ_NO == '0') {
						iobjCount ++;
					}
				}
								
				if (bObjName) {					
					var $dtlObj;
					var arrDtlChk = new Array();
					$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
						$dtlObj = $(this);
						if (item.OBJECT_NAME != $dtlObj.text() && !arrDtlChk.contains($dtlObj.text())) {
							arrDtlChk.push($dtlObj.text().trim());
							if(!gfn_isNull($dtlObj.text().trim()) && gfn_isNull($dtlObj.attr('id'))) {
								var subitem = new Object();
								subitem.OBJECT_NAME = $dtlObj.text().trim();
								//subitem.PARENT_SEQ_NO = item.SEQ_NO;
								arrobjsub.push(subitem);
								iObjDtl ++;
								iobjCount ++;
							}
						}
					});
									
					arrobjChk.push(item.OBJECT_NAME);
					
					
					//if (iObjDtl > 0) {			
						item.OBJECT_DETAIL = arrobjsub;
						
						arrobj.push(item);
					//}
				}
			});
			
			
			if (arrobj.length > 0) {
				var comAjax = new ComAjax();
				comAjax.addParam("TABLE", "v_slot_object");
				comAjax.addParam("WHERE", "SLOT_SEQ_NO in (select SEQ_NO from slot where PROJECT_SEQ = " + getCookie('ProjectNo') + ")");
				comAjax.addParam("COUNT_NAME", "SLOTOBJ_COUNT");
				comAjax.setUrl("<c:url value='/view/getcountcheck.do' />");			
				comAjax.setCallback("fnCountCheckCallBack");
				comAjax.setAsync(false);
				comAjax.ajax();				
							
				if ((CheckCnt + iobjCount) > CheckCntMax) {				
					alert('저장 가능한 사용자 발화 수를 초과 했습니다.');
					fnLoading();
					bsaving = false;
					return;
				}				
			}
			
			if (saveChk) {
				bsaving = false;
				arr.OBJECT_ITEM = arrobj;
				var jsoobj = JSON.stringify(arr);
				
				/* var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertslotobject.do' />");
				comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});
				comSubmit.addParam("OBJECT_ITEM", jsoobj);
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
				comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
				comSubmit.submit(); */
				var comAjax = new ComAjax();			
				comAjax.setUrl("<c:url value='/view/insertslotobject.do' />");	
				comAjax.setCallback("fnSlotSaveCallBack");
				comAjax.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});
				comAjax.addParam("OBJECT_ITEM", jsoobj);
				comAjax.addParam("depth1", '${depth1}');
				comAjax.addParam("depth2", '${depth2}');
				comAjax.addParam("depth3", '${depth3}');
				comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comAjax.addParam("CLASS_NAME", '${CLASS_NAME }');
				comAjax.addParam("SLOT_NAME", '${SLOT_NAME }');
				comAjax.ajax();
			}
			fnpreventDefault(event);
		}
		
		function fnSlotSaveCallBack(data) {
			fnLoading();
			if (data.STATUS == 'S') {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/slotwrite.do' />");
				comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});			
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
				comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
				comSubmit.submit();
			}
			else {
				alert('저장 중 중복된 값을 발견 했습니다. 제거 후 다시 시도해주세요.\n' + data.STATUS);
			}
		}
		
		function fnLoadEntry() { 
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getslotobjectexcel.do' />");
			comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});			
			comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnUpLoadEntry(Type) {
			if(Type == 'O') {
				fnLayerPop('divExcelUpload', 'o');
			}
			else {
				var comSubmit = new ComSubmit("ExcelForm");
				comSubmit.setUrl("<c:url value='/view/setslotobjectexcel.do' />");				
				comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});				
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
				comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
				if ($('#chkDel').attr('checked') == 'checked')					
					comSubmit.addParam("DEL_FLAG", 'Y');
				else					
					comSubmit.addParam("DEL_FLAG", 'N');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnProgressCancel(type) {
			$.modal.close();
			//$("#divProgress").hide();
			fnpreventDefault(event);
		}
		
		function fnAddRow() {
			$("#ObjectList").append($("#EmptyRow").html());			
			fnCursorEnd($("#ObjectList").find('dd').last().find('[name=OBJECT_NAME]').get(0));
			fnpreventDefault(event);
		}
		
		function fnObjectUpdate(seqno, obj, type) {
			if (bupdatechk) {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/updateslotobject.do' />");
				comAjax.setCallback("fnObjectUpdateCallBack");
				comAjax.addParam("TYPE", type);
				comAjax.addParam("SEQ_NO", seqno);
				comAjax.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});				
				comAjax.addParam("OBJECT_NAME", $(obj).text());	
				if (type == 'O') {
					comAjax.addParam("OLD_OBJECT_NAME", $('#hid_OBJECT_NAME_' + seqno).val());	
				}
				else {
					comAjax.addParam("OLD_OBJECT_NAME", $('#hid_' + seqno).val());	
				}
				comAjax.addParam("CLASS_NAME", '${CLASS_NAME }');
				comAjax.addParam("SLOT_NAME", '${SLOT_NAME }');				
				comAjax.ajax();	
				bupdatechk = false;
			}				
		}
		
		function fnObjectUpdateCallBack(data) {
			if (data.SAVE_CHK != "OK") {
				alert('이미 존재하는 데이터 입니다.');
			}
			else {
				if (data.TYPE == "O")
					$('#hid_OBJECT_NAME_' + data.SEQ_NO).val($('#OBJECT_NAME_' + data.SEQ_NO).text());
				else
					$('#hid_' + data.SEQ_NO).val($('#' + data.SEQ_NO).text());
			}
		}
				
		function fnObjectDelete(seqno, obj) {			
			if(seqno != 0) {
				if (confirm('삭제 하시겠습니까?')) {					
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deleteslotobject.do' />");
					comAjax.setCallback("");
					comAjax.setAsync(false);
					comAjax.addParam("SEQ_NO", seqno);
					comAjax.addParam("PARENT_SEQ_NO", seqno);
					comAjax.addParam("OLD_OBJECT_NAME", $('#hid_OBJECT_NAME_' + seqno).val());
					comAjax.addParam("CLASS_NAME", '${CLASS_NAME }');
					comAjax.addParam("SLOT_NAME", '${SLOT_NAME }');
					comAjax.ajax();	
					
					if ($(obj).parent().parent().parent().parent().parent().parent().find('dd').length > 1) {					
						$(obj).parent().parent().parent().parent().parent().remove();
					}
					else {
						if($("#PAGE_INDEX").val() != '' && $("#PAGE_INDEX").val() != '1')
							$("#PAGE_INDEX").val(Number($("#PAGE_INDEX").val()) - 1);						
						fnSelectList();
					}					
				}
			}
			else
				$(obj).parent().parent().parent().parent().parent().remove();
			
			fnpreventDefault(event);
		}	
		
		function fnObjectDeleteDetail(seqno, obj) {
			if(seqno != 0) {
				if (confirm('삭제 하시겠습니까?')) {
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deleteslotobjectdetail.do' />");
					comAjax.setCallback("");
					comAjax.addParam("SEQ_NO", seqno);	
					comAjax.addParam("OLD_OBJECT_NAME", $('#hid_' + seqno).val());
					comAjax.addParam("CLASS_NAME", '${CLASS_NAME }');
					comAjax.addParam("SLOT_NAME", '${SLOT_NAME }');
					comAjax.ajax();	
					$(obj).parent().remove();
				}
			}
			else
				$(obj).parent().remove();
			
			fnpreventDefault(event);
		}
		
		function fnCheck(obj) {
			if ($(obj).prop('checked') == false) {
				$(obj).prop("checked", false);
			}
			else {
				$(obj).prop("checked", true);
			}
		}
	
	</script>	
</head>
<body>
	<form id="ExcelForm" name="ExcelForm" enctype="multipart/form-data" method="post">
		<div class="layer" id="layer">
			<div id="bg" class="bg"></div>		
			<div id="divExcelUpload" class="popup uploadIntents" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
				<div class="title">Upload Entry</div>
				<div class="body">
					<div class="popFile">
						<div class="ex">
							<ul>
								<li>엑셀의 "표시 형식"을 "텍스트"로 설정 후 작성 및 업로드 요망.</li>
							</ul>
						</div>
						<div><input type="file" name="excelFile" /></div>
						<div class="check">
							<div class="right">
								<input type="checkbox" onclick="fnCheck(this);" id="chkDel" name="chkDel" value="" class="checkbox_st"/>
								<label for="chkDel" style="vertical-align: text-top; line-height:22px;">기존에 작성된 항목들 삭제</label>
							</div>
						</div>						
					</div>
				</div>
				<div class="btnBox">
					<div onclick="fnUpLoadEntry('S');" class="upload">Upload</div>
					<div onclick="fnLayerPop('divExcelUpload', 'c');" class="cancle">Cancel</div>
				</div>
			</div>
		</div>
	</form>
	
	<div id="wrap">	
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div id="solt_contents" class="middleW entities">
			<div class="middleW entities" style="width:100%;">
				<div class="btnBox">
					<div class="save" onclick="fnSlotSave();">SAVE</div>
				</div>
				<div class="searchBox searchBox_flow">
					<div class="box"><input type="text" id="txtSearch" placeholder="search slot..."><button type="button" onclick="fnSelectList(1, '');"><img src="../images/icon_searchW.png" alt="search"></button></div>
				</div>
			</div>
			<div class="liBox liBox4 liBox5 table" style="margin-top:20px;">			
				<div class="title">
					<div class="title">${CLASS_NAME }.${SLOT_NAME }</div>
					<div class="btnBox">
						<div onclick="fnLoadEntry();" class="btn downloadIn">Download slot</div>
						<div onclick="fnUpLoadEntry('O');" class="btn uploadIn">Upload slot</div>
					</div>
				</div>
	            <div id="solt_contents" class="class_name_div">
	                <dl id="ObjectList" class="class_name mt30">
	                    <dt class="hide_style">고구마</dt>            
	                    <!-- <dd class="solt_dd">
	                        <table name="tbOBJ_LIST" class="slot_modify">
	                            <tbody>
	                                <tr>
	                                    <th>
	                                        <div id="OBJECT_NAME_723" onfocusout="fnObjectUpdate(723, this, 'O');" name="OBJECT_NAME" contenteditable="true">겨울왕국</div>
	                                        <input type="hidden" id="hid_OBJECT_NAME_723" value="겨울왕국">
	                                    </th>
	                                    <td class="slot_modify01">
	                                        <div class="slot_modify_set02">
	                                            <div class="dp_ib" onfocusout="fnObjectUpdate(5704, this, 'OD');" name="OBJECT_DETAIL_NAME" contenteditable="true">프론즈</div>
	                                            <a class="close_btn" href="#"></a>
	                                            <input type="hidden" value="프론즈">
	                                        </div>
	                                        <div class="slot_modify_set02">
	                                            <div class="dp_ib" onfocusout="fnObjectUpdate(5704, this, 'OD');" name="OBJECT_DETAIL_NAME" contenteditable="true">프론즈</div>
	                                            <a class="close_btn" href="#"></a>
	                                            <input type="hidden" value="프론즈">
	                                        </div>
	                                    </td>
	                                    <td class="slot_modify02">
	                                        <a href="#" class="delete_btn">
	                                            <span class="hide">삭제</span>
	                                        </a>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </dd>
	                    <dd class="solt_dd">
	                        <table name="tbOBJ_LIST" class="slot_modify">
	                            <tbody>
	                                <tr>
	                                    <th>
	                                        <div id="OBJECT_NAME_723" onfocusout="fnObjectUpdate(723, this, 'O');" name="OBJECT_NAME" contenteditable="true">겨울왕국</div>
	                                        <input type="hidden" id="hid_OBJECT_NAME_723" value="겨울왕국">
	                                    </th>
	                                    <td class="slot_modify01" id="td_detail_723">
	                                        <div class="slot_modify_set02">
	                                            <div class="dp_ib" onfocusout="fnObjectUpdate(5704, this, 'OD');" name="OBJECT_DETAIL_NAME" contenteditable="true">프론즈</div>
	                                            <a class="close_btn" href="#"></a>
	                                            <input type="hidden" value="프론즈">
	                                        </div>
	                                    </td>
	                                    <td class="slot_modify02">
	                                        <a href="#" class="delete_btn">
	                                            <span class="hide">삭제</span>
	                                        </a>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </dd> -->
	                </dl>
					<div class="addrow">
						<a href="#" onclick="fnAddRow();">+Add row</a>
					</div>
					
					<div id="PAGE_NAVI" class="board_paging">
					</div>
					<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX" />
					<input type="hidden" id="TOTAL_COUNT" name="TOTAL_COUNT" />
	            </div>
            </div>
		</div>
	</div>
	
	<div id="EmptyRow" style="display:none;">
		<dd class="solt_dd">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tbody>
					<tr>
						<th>
							<div name="OBJECT_NAME" contenteditable="true">입력 하세요...</div>
						</th>
						<td class="slot_modify01">																
							<div class="slot_modify_set02 f_left"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#" onclick="fnObjectDeleteDetail(0, this);"></a></div>										
						</td>
						<td class="slot_modify02">
							<a href="#" class="delete_btn" onclick="fnObjectDelete(0, this);"><span class="hide">삭제</span></a>
						</td>
					</tr>
				</tbody>
			</table>
		</dd>
	</div>
</body>
</html>