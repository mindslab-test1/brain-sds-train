<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">		
		var arrChead = new Array();;
		var strCol = '';
	
		window.onload = function() {			
			arrChead.push('TAGGED_SLOT_KEY');
			arrChead.push('TAGGED_SLOT_VALUE');
			arrChead.push('QUERY_COND');
			arrChead.push('DB_SLOT_KEY');
			arrChead.push('DB_SLOT_VALUE');
			strCol = arrChead.toString(); 
			
			$('[name=h3none]').click(function() {
				$(this).next().toggle();
			});
			
			$('#tbslot').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddRow($("#tbslot > tbody"));
					}
				},
				focusout: function(e) {
					var $this = $(this);	
					var id = $this.attr('id');					
					
					if(!gfn_isNull(id)) {
						var seq = id.replace('txtObj', '');
						if($('#hidSaveChk' + seq).val() == 'Y') {
							var strupdate = '';
							
							$(this).parent().parent().find('input:text').each(function(idx) {												
								strupdate += arrChead[idx] + ' = \'' + $(this).val().replace(/\\/, '\\\\').replace(/'/, '\\\'') + '\', ';								
							});							
							
							var comAjax = new ComAjax();		
							comAjax.setCallback("fnAjaxCallBack");
							comAjax.addParam("SEQ_NO", '${SEQ_NO}');
							comAjax.addParam("ITEM_SEQ_NO", seq);
							
							if(strupdate != '') {
								strupdate = strupdate.substr(0, strupdate.length - 2);
								comAjax.setUrl("<c:url value='/view/updateslotmapping.do' />");
								comAjax.addParam("SET_ITEM", strupdate);
							}
							else {
								comAjax.setUrl("<c:url value='/view/deleteslotmapping.do' />");
								$(this).parent().parent().remove();
							}
							comAjax.ajax();
							$('#hidSaveChk' + seq).val('N');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						/* $(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e); */
					} else if(e.keyCode == 40) {
						
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txtObj', '')).val('Y');
					}
				}			
			}, '[name=OBJECT]');
						
			$('#tbslot').on('click', '[name="imgdel"]', function(e) {
				var $tr = $(this).parent().parent();
				if($tr.find('[id^=hidSeqno]').length > 0) {					
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deleteslotmapping.do' />");
					comAjax.setCallback("fnAjaxCallBack");
					comAjax.addParam("ITEM_SEQ_NO", $tr.find('[id^=hidSeqno]').val());
					comAjax.ajax();
				}
				$tr.remove();
				fnpreventDefault(e);
			});			
			
			fnSelectList(1, '');
		}
		
		function fnAjaxCallBack(data) {
			if (data.status != "OK") {
				alert('작업 중 오류가 발생 하였습니다.');
			}
		}
		
		function fnSelectList(pageNo, searchval) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectslotmapping.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.addParam("COL", strCol);
			
			comAjax.ajax();
			fnpreventDefault(event);
		}
						
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbslot > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var str = "";
				var iRowColor = true;											
				
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
						
					str += '<input type="hidden" id="hidSaveChk'+value.SEQ_NO+'" value="N" />' +
						'<input type="hidden" id="hidSeqno'+value.SEQ_NO+'" value="'+value.SEQ_NO+'" />';
						
					for (var i = 0; i < arrChead.length; i++) {
						if (value[arrChead[i]] != undefined) {
							if (i == 0)
								str += '<td><input type="text" class="none_focus" value="'+value[arrChead[i]].replace(/\"/g,"&quot;")+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" onclick="fnSlotSearch(this);" /></td>';
							else
								str += '<td><input type="text" class="none_focus" value="'+value[arrChead[i]].replace(/\"/g,"&quot;")+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';	
						}
						else {
							if (i == 0)
								str += '<td><input type="text" class="none_focus" value="" id="txtObj'+value.SEQ_NO+'" name="OBJECT" onclick="fnSlotSearch(this);" /></td>';
							else
								str += '<td><input type="text" class="none_focus" value="" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';
						}
					}
					str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
					str +='</tr>'; 
				});
				
				body.append(str);
				AddRow(body);
				
				$("a[name='title']").on("click", function(e){ //제목 
					e.preventDefault();
					fn_openBoardDetail($(this));
				});
			}
		}
		
		function AddRow(el) {
			var str = "<tr name='trIn'>";
			
			for (var int = 0; int < 5; int++) {
				if (int == 0)						
					str += "<td><input type='text' value='' name='OBJECT' onclick='fnSlotSearch(this);' /></td>";
				else
					str += "<td><input type='text' value='' name='OBJECT' /></td>";	
			}		
			str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
			str += "</tr>";				
			el.append(str);
		}
		
		function fnSlotSearch(obj) {
			$(obj).attr('id', 'tempid');
			fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(obj).attr('id') + "&Type=TS", "AgentSearchpop", 430, 550, 0, 0);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();
			
			var strinsert = 'INSERT INTO slot_mapping (';
			
			/* for (var i in arrThead) {
				strinsert += arrThead[i] + ', ';
			} */
			strinsert += strCol + ', PROJECT_SEQ_NO)';
			//strinsert = strinsert.substr(0, strinsert.length - 2) + ')';
			
			$('#tbslot tbody [name=trIn]').each(function(idx, e) {
				var item = new Object();
				var arrobjsub = new Array();
				var bInserChk = false;
				var strvalues = 'VALUES(';
				
				$(this).find('input:text').each(function() {
					if($(this).val().trim() != '') {
						bInserChk = true;
					}					
					strvalues += '"' + $(this).val().replace(/'/g, "et§").replace(/\\/g, '\\\\').replace(/"/g, '\\"') + '", ';								
				});
				
				if(bInserChk) {
					strvalues = strvalues + '${PROJECT_SEQ})';					
					item.ITEM_DETAIL = strvalues;
					arrobj.push(item);
				}
			});
			arr.ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertslotmapping.do' />");
			comSubmit.addParam("INSERT_ITEM", strinsert);
			comSubmit.addParam("VALUES_ITEM", jsoobj);
			comSubmit.submit();
			fnpreventDefault(event);
		}
	</script>	
</head>
<body>
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>		
		<div class="middleW instances" style="padding:10px 20px 20px 260px;">
			<div class="btnBox">
				<div class="save" onclick="fnSave();">SAVE</div>
			</div>			
			<div class="liBox liBox4 liBox5 table" style="margin-top:20px;">
				<h3 class="table_h3" name="h3none">예시</h3>			
				<div class="contentBox" style="display:none;">
					<div class="admin_user_con entities_ex_con">
						<div class="admin_user_title">
							<table id="tbusers" class="admin_user_list" summary="표시">								
								<colgroup>
									<col width="15%">
									<col width="15%">
									<col width="15%">
									<col width="25%">
									<col width="30%">
								</colgroup>
								<thead>
									<tr>
										<th>tagged slot key</th>
										<th>tagged slot value</th>
										<th>query cond</th>
										<th>db slot key</th>
										<th>db slot value</th>
									</tr>
								</thead>
								<tbody>
									<tr class="admin_user_list_bg01">
										<td>Weather.city</td>
										<td>*</td>
										<td>~=</td>
										<td>Weather.city</td>
										<td>_SLOT_VALUE_</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<h4>Instance에서 Weather.city의 슬롯 값을 LIKE로 검색</h4>
					<div class="admin_user_con entities_ex_con">
						<div class="admin_user_title">
							<table id="tbusers" class="admin_user_list" summary="표시">								
								<colgroup>
									<col width="15%">
									<col width="15%">
									<col width="15%">
									<col width="25%">
									<col width="30%">
								</colgroup>
								<thead>
									<tr>
										<th>tagged slot key</th>
										<th>tagged slot value</th>
										<th>query cond</th>
										<th>db slot key</th>
										<th>db slot value</th>
									</tr>
								</thead>
								<tbody>
									<tr class="admin_user_list_bg01">
										<td>POI.distance</td>
										<td>min</td>
										<td>*</td>
										<td>_WHERE_EXTRA_</td>
										<td>ORDER BY “POI.distance” LIMIT 1</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<h4>Instance에서 POI.distance의 슬롯 값이 "min"일 때, POI.distance의 슬롯 값들 중 최솟값 리턴</h4>
				</div>
			</div>
			<div class="liBox liBox4 liBox5 table" style="margin-top:20px;">
				<h3 class="table_h3" name="h3none">예약어</h3>
				<div class="entities_ex_con"  style="display:none;">
					<ul class="wordLi">
						<li>_SLOT_VALUE_</li>
						<li>대화 엔진의 슬롯 값 그대로 사용</li>
						<li>*</li>
						<li>don't care</li>
						<li>_WHERE_EXTRA_</li>
						<li>SQL query 생성 시, where 문 추가</li>
					</ul>
				</div>
			</div>
			<div class="liBox liBox4 liBox5 table" style="margin-top:20px;">
				<h3 class="table_h3" style="cursor:default !important;">Slot mapping table 상태</h3>
				<div class="admin_user_con entities_ex_con">
					<div class="admin_user_title">
						<table id="tbslot" style="" class="slot_instan_title">							
							<colgroup>
								<col width="15%">
								<col width="15%">
								<col width="15%">
								<col width="25%">
								<col width="30%">
								<col width="30px">
							</colgroup>
							<thead>
								<tr>
									<th>tagged slot key</th>
									<th>tagged slot value</th>
									<th>query cond</th>
									<th>db slot key</th>
									<th>db slot value</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>								
							</tbody>
						</table>
					</div>
					
					<div id="PAGE_NAVI" class="board_paging">
					</div>
					<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>