<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var arrThead;
		var arrChead;
		var strCol = '';
		
		window.onload = function() {
			fnSetNaviTitle('Entities > Instances');
			var iwidth = '100%';						
			if (${fn:length(slotlist)} == 0) {
				
			}
			else {				 
				if(${fn:length(slotlist)} > 5)					
					iwidth = ${fn:length(slotlist)} * 120;										
			}						
			//$('#tbslot').css('width', iwidth);
			
			arrThead = new Array();
			$('#tbslot thead tr [name=thTitle]').each(function() {
				arrThead.push($(this).text());
				var str = '';
				str += '<div class="popInput">';
				str += '<div class="title">' + $(this).text() + '</div>';
				str += '<div class="input">';
				str += '<input type="text" name="SEARCH_COL" /></div></div>';
				
				$('#divSearchCol').append(str);
			});
			
			$('#tbslot').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddRow($("#tbslot > tbody"));
					}
				},
				focusout: function(e) {
					var $this = $(this);	
					var id = $this.attr('id');					
					
					if(!gfn_isNull(id)) {
						var seq = id.replace('txtObj', '');
						if($('#hidSaveChk' + seq).val() == 'Y') {
							var strupdate = '';
							
							$(this).parent().parent().find('input:text').each(function(idx) {																						
								strupdate += arrChead[idx] + ' = \'' + $(this).val().replace(/\\/, '\\\\').replace(/'/, '\\\'') + '\', ';
							});							
							
							var comAjax = new ComAjax();		
							comAjax.setCallback("fnAjaxCallBack");
							comAjax.addParam("SEQ_NO", ${SEQ_NO});
							comAjax.addParam("ITEM_SEQ_NO", seq);
							
							if(strupdate != '') {
								strupdate = strupdate.substr(0, strupdate.length - 2);
								comAjax.setUrl("<c:url value='/view/updateslotinstansces.do' />");
								comAjax.addParam("SET_ITEM", strupdate);
								comAjax.ajax();
							}
							else {
								comAjax.setUrl("<c:url value='/view/deleteslotinstansces.do' />");
								comAjax.setAsync(false);
								comAjax.ajax();
								
								if ($(this).parent().parent().parent().find('tr').length > 2) {					
									$(this).parent().parent().remove();
								}
								else {
									if($("#PAGE_INDEX").val() != '' && $("#PAGE_INDEX").val() != '1')
										$("#PAGE_INDEX").val(Number($("#PAGE_INDEX").val()) - 1);						
								}
								fnSelectList('');
							}							
							$('#hidSaveChk' + seq).val('N');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						/* $(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e); */
					} else if(e.keyCode == 40) {
						
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txtObj', '')).val('Y');
					}
				}			
			}, '[name=OBJECT]');
						
			$('#tbslot').on('click', '[name="imgdel"]', function(e) {
				var $tr = $(this).parent().parent();				
				if($tr.find('[id^=hidSeqno]').length > 0) {
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deleteslotinstansces.do' />");
					comAjax.setCallback("fnAjaxCallBack");
					comAjax.setAsync(false);
					comAjax.addParam("SEQ_NO", ${SEQ_NO});
					comAjax.addParam("ITEM_SEQ_NO", $tr.find('[id^=hidSeqno]').val());
					comAjax.ajax();
				}
				
				if ($tr.parent().find('tr').length > 2) {					
					$tr.remove();
				}
				else {
					if($("#PAGE_INDEX").val() != '' && $("#PAGE_INDEX").val() != '1')
						$("#PAGE_INDEX").val(Number($("#PAGE_INDEX").val()) - 1);
					fnSelectList('');
				}				
				fnpreventDefault(e);
			});
			
			$('#txtSearch').bind('keydown', function(e) {
				if(e.keyCode == 13) { 
					fnSelectList('');
				}
			});
			
			strCol = strCol.substring(0, strCol.length - 2);
			arrChead = strCol.split(', ');
			fnSelectList('');			
			
			if ('${UPLOAD_STATUS}' == 'F') {
				alert('업로드 하려는 엑셀 파일 형식에 문제가 있습니다.\n확인 후 다시 시도 해주세요.');				
			}
			else if ('${UPLOAD_STATUS}' == 'UC') {
				alert('한번에 업로드 할 수 있는 instance 수는 200개 입니다.');
			}
			else if ('${UPLOAD_STATUS}' == 'TC') {
				alert('저장 가능한 Instance 수를 초과 했습니다.');
			}
		}
		
		function fnDelete() {
			if (confirm('모든 Instance를 삭제 하시겠습니까?')) {						
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deleteslotinstansall.do' />");
				comSubmit.addParam("SEQ_NO", ${SEQ_NO});
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');								
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				comSubmit.submit();
			}
		}
		
		function fnAjaxCallBack(data) {
			if (data.status != "OK") {
				alert('작업 중 오류가 발생 하였습니다.');
			}
		}
		
		function fnShowSearchBtn(type) {
			if (type == 0) {
				$("#txtSearch").val('');
				$('#divSearchCol input').val('');
				fnSelectList('');
			}
			else {				
				fnLayerPop('divSearchBox', 'o');
			}	
		}				
		
		function fnCancelSearchBtn() {						
			fnLayerPop('divSearchBox', 'c');
			fnpreventDefault(event);
		}
		
		function fnSelectList(searchval) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectslotinstansces.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.addParam("SEQ_NO", ${SEQ_NO});
			comAjax.addParam("COL", strCol);
			$txtSearch = $("#txtSearch");
			var strsearch = '';
			var searchval = '';
			
			$('#divSearchCol input').each(function(idx) {
				if ($(this).val() != '') {
					searchval += arrChead[idx] + ' LIKE "§'+ $(this).val() + '§" AND ';
				}
			});
			
			if (searchval == '') {
				if($txtSearch.val().trim() != '' && $txtSearch.val() != 'search…') {
					strsearch = 'WHERE ';
					for (var i = 0; i < arrThead.length; i++) {
						strsearch += arrChead[i] + ' LIKE "§'+ $txtSearch.val() + '§" OR ';
					}
					strsearch = strsearch.substr(0, strsearch.length - 4);
				}	
			}
			else {
				strsearch = 'WHERE ' + searchval;
				strsearch = strsearch.substr(0, strsearch.length - 4);
				fnCancelSearchBtn();
			}
				
			comAjax.addParam("WHERE_SQL", strsearch);
			comAjax.ajax();
			fnpreventDefault(event);
		}
						
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbslot > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var str = "";
				var iRowColor = true;											
				
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
						
					str += '<input type="hidden" id="hidSaveChk'+value.SEQ_NO+'" value="N" />' +
						'<input type="hidden" id="hidSeqno'+value.SEQ_NO+'" value="'+value.SEQ_NO+'" />';
						
					for (var i = 0; i < arrChead.length; i++) {
						if (value[arrChead[i]] != undefined) {
							str += '<td><input type="text" class="none_focus" value="'+value[arrChead[i]].replace(/\"/g,"&quot;")+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';	
						}
						else {
							str += '<td><input type="text" class="none_focus" value="" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';
						}
					}
					str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
					str +='</tr>'; 
				});
				
				body.append(str);
				AddRow(body);
				
				$("a[name='title']").on("click", function(e){ //제목 
					e.preventDefault();
					fn_openBoardDetail($(this));
				});
			}
		}
		
		function AddRow(el) {
			var str = "<tr name='trIn'>";
			
			for (var int = 0; int < ${fn:length(slotlist)}; int++) {
				str += "<td><input type='text' value='' name='OBJECT' /></td>";
			}		
			str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
			str += "</tr>";				
			el.append(str);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();
			
			var strinsert = 'INSERT INTO slot_class_${SEQ_NO} (';
			
			/* for (var i in arrThead) {
				strinsert += arrThead[i] + ', ';
			} */
			strinsert += strCol + ')';
			//strinsert = strinsert.substr(0, strinsert.length - 2) + ')';
			
			$('#tbslot tbody [name=trIn]').each(function(idx, e) {
				var item = new Object();				
				var arrobjsub = new Array();				
				var bInserChk = false;							
				var strvalues = 'VALUES(';
				
				$(this).find('input:text').each(function() {
					if($(this).val().trim() != '') {
						bInserChk = true;
					}					
					strvalues += '"' + $(this).val().replace(/'/g, "et§").replace(/\\/g, '\\\\').replace(/"/g, '\\"') + '", ';								
				});
				
				if(bInserChk) {
					strvalues = strvalues.substr(0, strvalues.length - 2) + ')';					
					item.ITEM_DETAIL = strvalues;
					arrobj.push(item);
				}
			});
			
			if (arrobj.length > 0) {
				var comAjax = new ComAjax();
				comAjax.addParam("TABLE", "slot_class_${SEQ_NO}");
				comAjax.addParam("WHERE", "1=1");
				comAjax.addParam("COUNT_NAME", "INSTANCE_COUNT");
				comAjax.setUrl("<c:url value='/view/getcountcheck.do' />");			
				comAjax.setCallback("fnCountCheckCallBack");
				comAjax.setAsync(false);
				comAjax.ajax();				
							
				if ((CheckCnt + arrobj.length) > CheckCntMax) {				
					alert('저장 가능한 Instance 수를 초과 했습니다.');				
					return;
				}				
			}
			
			arr.ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertslotinstansces.do' />");
			comSubmit.addParam("SEQ_NO", ${SEQ_NO});
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
			comSubmit.addParam("INSERT_ITEM", strinsert);
			comSubmit.addParam("VALUES_ITEM", jsoobj);
			comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnUpLoadInstances(Type) {
			if(Type == 'O') {
				fnLayerPop('divExcelUpload', 'o');				
			}
			else {
				var comSubmit = new ComSubmit("ExcelForm");
				comSubmit.setUrl("<c:url value='/view/setslotinstanscesexcel.do' />");
				comSubmit.addParam("SEQ_NO", ${SEQ_NO});
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
				comSubmit.addParam("HEADER_LIST", arrThead.toString());
				comSubmit.addParam("COL_LIST", arrChead.toString());
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				if ($('#chkDel').prop('checked') == true)					
					comSubmit.addParam("DEL_FLAG", 'Y');
				else					
					comSubmit.addParam("DEL_FLAG", 'N');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnLoadInstances() {
			var strChead = '';
			for (var i = 0; i < arrChead.length; i++) {
				if (i == 0)
					strChead = 'IFNULL(' + arrChead[i] + ', "") ' + arrChead[i];
				else
					strChead += ', IFNULL(' + arrChead[i] + ', "") ' + arrChead[i];
			}
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getslotinstanscesexcel.do' />");
			comSubmit.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comSubmit.addParam("PAGE_ROW", 10);
			comSubmit.addParam("HEADER_LIST", arrThead.toString());
			comSubmit.addParam("COL_LIST", arrChead.toString());
			comSubmit.addParam("SEL_COL_LIST", strChead);
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
			comSubmit.addParam("SEQ_NO", ${SEQ_NO});
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnCheck(obj) {
			if ($(obj).prop('checked') == true) {
				$(obj).prop("checked", true);
			}
			else {
				$(obj).prop("checked", false);		
			}
		}
	</script>
</head>
<body>
	<form id="ExcelForm" name="ExcelForm" enctype="multipart/form-data" method="post">
		<div class="layer" id="layer">
			<div id="bg" class="bg"></div>		
			<div id="divExcelUpload" class="popup uploadIntents" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
				<div class="title">Upload Intents</div>
				<div class="body">
					<div class="popFile">
						<div class="ex">
							<ul>
								<li>엑셀의 "표시 형식"을 "텍스트"로 설정 후 작성 및 업로드 요망.</li>
							</ul>
						</div>
						<div><input type="file" name="excelFile" /></div>
						<div class="check">
							<div class="right">
								<input type="checkbox" onclick="fnCheck(this);" id="chkDel" name="chkDel" value="" class="checkbox_st"/>
								<label for="chkDel" style="vertical-align: text-top; line-height:22px;">기존에 작성된 항목들 삭제</label>
							</div>
						</div>						
					</div>
				</div>
				<div class="btnBox">
					<div onclick="fnUpLoadInstances('S');" class="upload">Upload</div>
					<div onclick="fnLayerPop('divExcelUpload', 'c');" class="cancle">Cancel</div>
				</div>
			</div>
						
			<div id="divSearchBox" class="popup uploadIntents"
				style="z-index: 1; display: block; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display: none;">
				<div class="title">고급 검색</div>
				<div id="divSearchCol" class="body">				
				</div>
				<div class="btnBox">
					<div onclick="fnSelectList('C');" class="upload">Search</div>
					<div onclick="fnCancelSearchBtn();" class="cancle">Cancel</div>
				</div>
			</div>
		</div>
	</form>
		
	
	<div id="wrap">
		<%@ include file="/WEB-INF/include/left.jspf" %>		
		<div class="middleW instances">
			<div class="middleW entities" style="width:100%;">
				<div class="btnBox">
					<div class="save" onclick="fnSave();">SAVE</div>
				</div>
				<div class="searchBox searchBox_flow">
					<div class="box"><input type="text" id="txtSearch" placeholder="search slot..."><button type="button" onclick="fnSelectList('');"><img src="../images/icon_searchW.png" alt="search"></button></div>
				</div>
				<div class="btnBox btnBox_flow">
					<div class="btn_s" onclick="fnShowSearchBtn(1);">고급검색</div>
					<div class="btn_s" onclick="fnShowSearchBtn(0);">원래대로</div>
				</div>
			</div>
			<div class="liBox liBox4 liBox5 table">
				<div class="title">
					<div class="title">${CLASS_NAME }</div>
					<div class="btnBox">
						<div onclick="fnDelete();" class="btn downloadIn">Delete All</div>
						<div onclick="fnLoadInstances();" class="btn uploadIn">Download Instance</div>
						<div onclick="fnUpLoadInstances('O');" class="btn uploadIn">Upload Instance</div>
					</div>
				</div>
				<div class="tableIn">
					<table id="tbslot" class="table">
						<caption class="hide" style="display:none;">USER</caption>						
						<thead>
							<tr>
								<c:forEach items="${slotlist }" var="row">
									<th name="thTitle" style="word-wrap: break-word;">${row.VIEW_COL }</th>
									<script>strCol += '${row.QUERY_COL }' + ', ';</script>
								</c:forEach>
								<th style="width:30px;">&nbsp;</th>	
							</tr>						
						</thead>
						<tbody>
						<!-- <tr class="knowledge_th01">
							<td>김김김</td>
							<td>김김김<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th02">
							<td>이름이름</td>
							<td>이름이름<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th01">
							<td>김김김</td>
							<td>김김김<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th02">
							<td>찰스</td>
							<td>찰스<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th01">
							<td>김김김</td>
							<td>김김김<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th02">
							<td>에버트란</td>
							<td>에버트란<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th01">
							<td>김김김</td>
							<td>김김김<span class="remove"></span></td>
						</tr>
						<tr class="knowledge_th02">
							<td><input type="text"></td>
							<td><input type="text"></td>
						</tr> -->
						</tbody>
					</table>
				</div>
				
				<div id="PAGE_NAVI" class="board_paging"><!-- <span class="left"><a href="#" class="front" onclick="_movePage(1);"><img src="../images/btn_front.png"></a><a href="#" class="prev"><img src="../images/btn_prev.png" onclick="_movePage(1)"></a></span><span class="num_group"><a class="on" href="#" onclick="_movePage(1)">1</a>&nbsp;<a href="#" onclick="_movePage(2)">2</a>&nbsp;<a href="#" onclick="_movePage(3)">3</a>&nbsp;</span><span class="right"><a href="#" class="next"><img src="../images/btn_next.png" onclick="_movePage(3);" ;=""></a><a href="#" class="back"><img src="../images/btn_back.png" onclick="_movePage(3);"></a></span> -->
				</div>
				<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			</div>
		</div>
	</div>	
</body>
</html>