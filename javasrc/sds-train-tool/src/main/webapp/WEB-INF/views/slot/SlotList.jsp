<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
</head>
<body>
	<div class="layer" id="layer">
		<div id="bg" class="bg"></div>
		<div id="divCreatClass" class="popup entitiesCreate" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
			<div class="title">Create Class</div>
			<div class="body">
				<div class="popInput">
					<div class="title">Class Name</div>
					<div class="input">
						<input id="CLASS_NAME" type="text" />
					</div>
				</div>
				<div id="divClassWarning" class="popInput" style="display:none;">
					<b><span class="Warning">
						[주의] Class 이름 변경 주의 :
					</span></b><br>
					<span class="Warning">
						기존 이름으로 저장한 다른 지식들은 변경되지 않습니다.
					</span>
				</div>
				<div id="divDefinedTask" class="popInput">
					<div class="title">Defined task</div>
					<div class="input" style="border:1px solid #ddd; border-radius:4px;">
						<input type="text" id="txtClassAgent" readonly="readonly" style="border:none; width:calc(100% - 30px); float:left;" /> 
						<a id="aTaskSearch" href="#" title="검색" class="solt_search search_hide_btn_link" onclick="fnOpenTask();"><span class="search_hide_btn">search</span></a>
						<input type="hidden" id="hidClassAgent" />
					</div>
				</div>
				<div class="popInput">
					<div class="title">Description</div>
					<div class="input">
						<input id="DESCRIPTION" type="text" value="" />
					</div>
				</div>
			</div>
			<div class="btnBox">
				<div class="save" onclick="fnSaveClass();">Save</div>
				<div class="cancle" onclick="fnCancelClass();">Cancel</div>
			</div>
		</div>
		
		<div id="divCreatSlot" class="popup createSlot" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display:none;">
			<div class="title">Create Slot</div>
			<div class="body">
				<div class="divCreatSlot_group">
					<span>Slot essential information</span>
					<div class="popInput">
						<div class="title">Class</div>
						<div class="input"><input id="SLOT_CLASS_NAME" type="text" disabled="disabled" /></div>
					</div>
					<div class="popInput">
						<div class="title">Slot Name</div>
						<div class="input"><input id="SLOT_NAME" type="text" /></div>
					</div>
					<div id="divSlotWarning" class="popInput" style="display:none;">
						<b><span class="Warning">
							[주의] Slot 이름 변경 주의 :
						</span></b><br>
						<span class="Warning">
							기존 이름으로 저장한 다른 지식들은 변경되지 않습니다.
						</span>
					</div>
					<div class="popInput">
						<div class="title">Slot Type</div>
						<div class="input" style="border:1px solid #ddd; border-radius:4px;">
							<input type="text" id="TYPE_NAME" readonly="readonly" style="border:none; width:calc(100% - 30px); float:left;" /> 
							<!-- <a id="aTypeSearch" href="#" title="검색" class="solt_search search_hide_btn_link"><span class="search_hide_btn">search</span></a> -->
							<input type="hidden" id="hidslottype" /> 
							<input type="hidden" id="hidtypeseq" />
							<input type="hidden" id="hidtypesubseq" />
						</div>
					</div>
					<!-- display none이지만 실제로 사용함 지우면 안 됨. -->
					<div class="popInput" style="display:none;">
						<div class="title">작성한 슬롯 값만 사용</div>
						<div class="input"><input type="checkbox" id="chkSet" name="chkSet" style="width:7%;" onclick="fnCheck(this);"/>
								<label for="chkSet" style="vertical-align: text-top; line-height:22px;"></label></div>
					</div>
					
					<div class="popInput"  id="tr2">
						<div class="title">Instance와 연동</div>
						<div class="input" style="margin-top:5px;"><input type="checkbox" id="chkInstance" name="chkInstance" style="width:7%;" onclick="fnCheck(this);" class="checkbox_st"/>
								<label for="chkInstance" style="vertical-align: text-top; line-height:22px;"></label></div>
					</div>
					<span id="tr3">Slot additional information</span>
					<div class="popInput">
						<div class="title">Default Value</div>
						<div class="input"><input id="Default_value" type="text" /></div>
					</div>
					<div class="popInput">
						<div class="title">Slot description</div>
						<div class="input"><input id="SLOT_DESCRIPTION" type="text" /></div>
					</div>
					<!-- display none이지만 실제로 사용함 지우면 안 됨. -->
					<div class="popInput" style="display:none;">
						<div class="title">Instance와 연동</div>
						<div class="input">
							<select class="f_right u_group"
								id="search_option" name="search_option"
								data-placeholder="검색조건을 선택하세요">
									<option value="">select class</option>
									<c:forEach items="${CLASS_LIST }" var="row">
										<option value="${row.SEQ_NO}">${row.CLASS_NAME }</option>
									</c:forEach>
									<c:forEach items="${CLASS_LIST_REFER }" var="row">
										<option value="${row.SEQ_NO}">${row.CLASS_NAME }</option>
									</c:forEach>
							</select>						
						</div>
					</div>
					<div class="popInput" id="tr5">
						<div class="title">Preceding slots</div>
						<div class="input" style="border:1px solid #ddd; border-radius:4px;">
							<input type="text" id="PRCE_SLOT_NAME" readonly="readonly" style="border:none; width:calc(100% - 30px); float:left;" /> 
							<!-- <a id="aPrceSearch" href="#" title="검색" class="solt_search search_hide_btn_link"><span class="search_hide_btn">search</span></a> -->
							<input type="hidden" id="hidPreceSlotSeq" />
							<input type="hidden" id="hidPreceSlotSubSeq" />
						</div>
					</div>
					<!-- display none이지만 실제로 사용함 지우면 안 됨. -->
					<div class="popInput" style="display:none;">
						<div class="title">min, max 설정</div>
						<div class="input">
							<label for="chkMin" style="vertical-align: text-top; line-height:22px; margin-right:5px; display:inline-block;">min</label>
							<input type="checkbox" id="chkMin" name="chkMin" style="width:7%; margin-right:20px; display:inline-block;" onclick="fnCheck(this);"/>
							<label for="chkMax" style="vertical-align: text-top; line-height:22px; margin-right:5px; display:inline-block;">max</label>
							<input type="checkbox" id="chkMax" name="chkMax" style="width:7%; display:inline-block;" onclick="fnCheck(this);"/>						
						</div>
					</div>
				</div>
				<div class="divCreatSlot_group" id="tr7">
					<span>Slot value relationship&nbsp;<a href="#" onclick="fnAddDefine('', 0, 'true', '', '');">+add</a></span>
					<div id="divDefine">
					</div>
				</div>
			</div>
			<div class="btnBox">
				<div class="save" onclick="fnSaveSlot();">Save</div>
				<div class="cancle" onclick="fnCancelSlot();">Cancel</div>
			</div>
		</div>
	</div>
	<div id="wrap">	
		<%@ include file="/WEB-INF/include/left.jspf" %>
		<div id="solt_contents" class="middleW entities">
			<div class="btnBox">
				<div id="divCreateClass" onclick="fnOpenCreateClass();" class="createC">Create Class</div>
				<c:if test="${USER_TYPE ne 'OA'}">
					<div onclick="fnSlotMapping();" class="slotM">Slot Mapping</div>
				</c:if>							
				<c:if test="${AGENT_SEQ ne null}">
					<div onclick="fnOpenReferenceClass();" class="slotM">Reference Class</div>
				</c:if>				
			</div>
			<div class="searchBox">
				<div class="box"><input type="text" id="txtSearch" placeholder="search slot..."><button type="button" onclick="fnSearch();"><img src="../images/icon_searchW.png" alt="search"></button></div>
			</div>						
			
			<c:set var="AgentName" value=""></c:set>
			<c:forEach items="${CLASS_LIST }" var="crow">			
				<c:if test="${AgentName ne crow.AGENT_NAME}">
					<c:if test="${AgentName ne ''}">
						</div>						
					</c:if>	
					<c:set var="AgentName" value="${crow.AGENT_NAME}"></c:set>
					<div <c:if test="${crow.AGENT_NAME eq 'All tasks'}">id="hAllTask"</c:if> class="liBox liBox3">
						<div class="title">${crow.AGENT_NAME}</div>
				</c:if>
				
				<div class="liBox classBox">
					<div class="topBox">
						<div class="title" onclick="fnClassSlide(this);" id="divclass${crow.SEQ_NO}">
							<div class="className">${crow.CLASS_NAME}</div>
							<div class="icon open"></div>
							<div class="icon close"></div>
							<input type="hidden" id="hidClassName${crow.SEQ_NO}" value="${crow.CLASS_NAME}" />
						</div>
						<div class="decription"<c:if test='${crow.NEW_CHECK eq "Y"}'> style="display:none;"</c:if>>${crow.DESCRIPTION}<div class="open"></div></div>						
						<div class="btnBox"<c:if test='${crow.NEW_CHECK eq "Y"}'> style="display:block;"</c:if>>
							<div onclick="fnOpenCreateSlot(${crow.SEQ_NO}, '${crow.CLASS_NAME}');" class="btn createSlot">Create Slot</div>
							<div onclick="fnClassInstans(${crow.SEQ_NO}, '${crow.CLASS_NAME}', this);" class="btn instances">Instances</div>
							<div onclick="fnClassEdit(${crow.SEQ_NO}, '${crow.CLASS_NAME}', '${crow.DESCRIPTION}', '${crow.AGENT_SEQ }', '${crow.AGENT_NAME }');" class="btn edit">Edit</div>
							<div onclick="fnClassDelete(${crow.SEQ_NO});" class="btn delete">Delete</div>
						</div>
					</div>
					<div id="dl${crow.SEQ_NO}" class="slotBox" style="border-bottom:1px solid #ddd;">
					
					</div>
				</div>			
			</c:forEach>
			
			<c:if test="${AgentName ne ''}">
					</div>
				</div>
			</c:if>	
			<input type="hidden" id="hidClassSeq" /> <input type="hidden" id="hidSlotSeq" />
			<input type="hidden" id="hidSlotDelChk" />
		</div>
	</div>	
	
	<script type="text/javascript">
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var savechk = true;
		var addslotSeq = 0;
		var pop;
		var typesearchChk = false;
		window.onload = function() {
			$('#CLASS_NAME').keydown(function(e) {
				if ($('#hidClassSeq').val() != '') {
					$('#divClassWarning').show();
				}
			});
		
			$('#SLOT_NAME').keydown(function(e) {
				if ($('#hidSlotSeq').val() != '') {
					$('#divSlotWarning').show();
				}
			});
			
			if ('${AGENT_SEQ}' != '') {
				var agentname = '';
				<c:forEach items="${submenu }" var="row">					
					<c:if test="${row.SEQ_NO eq AGENT_SEQ}">
						agentname = '${row.AGENT_NAME}';
					</c:if>
				</c:forEach>
				fnSetNaviTitle('Entities of ' + agentname + ' Task');				
			}
			else
				fnSetNaviTitle('Entities of <%= session.getAttribute("ProjectName")%> domain');
			
			var str = '';
			var iCnt = 0;
			var arrRefClass = new Array();
			
			<c:if test="${AGENT_SEQ ne null}">
				<c:forEach items="${CLASS_LIST_REFER }" var="crow">
					if (iCnt == 0) {	
						str += '<div class="liBox liBox3">';
						str += '<div class="title">Referenced</div>';
						iCnt++;
					}
					str += '<div class="liBox classBox">';
					str += '<div class="topBox">';
					str += '<div class="title" onclick="fnClassSlide(this);" id="divclass${crow.SEQ_NO}">';
					str += '<div class="className">${crow.CLASS_NAME}</div>';
					str += '<div class="icon open"></div>';
					str += '<div class="icon close"></div>';
					str += '</div>';
					str += '<div class="decription">${crow.DESCRIPTION}<div class="open"></div></div>';
					str += '<div class="btnBox">';
					str += '<div onclick="fnReferCancel(${crow.SEQ_NO});" class="btn createSlot">Ref. cancel</div>';
					str += '<div onclick="fnOpenCreateSlot(${crow.SEQ_NO}, \'${crow.CLASS_NAME}\');" class="btn createSlot">Create Slot</div>';
					str += '<div onclick="fnClassInstans(${crow.SEQ_NO}, \'${crow.CLASS_NAME}\', this);" class="btn instances">Instances</div>';
					str += '<div onclick="fnClassEdit(${crow.SEQ_NO}, \'${crow.CLASS_NAME}\', \'${crow.DESCRIPTION}\', \'${crow.AGENT_SEQ }\', \'${crow.AGENT_NAME }\');" class="btn edit">Edit</div>';
					str += '<div onclick="fnClassDelete(${crow.SEQ_NO});" class="btn delete">Delete</div>';
					str += '</div>';
					str += '</div>';
					str += '<div id="dl${crow.SEQ_NO}" class="slotBox">';
					str += '</div>';
					str += '</div>';
					arrRefClass.push('${crow.SEQ_NO}');
				</c:forEach>
			</c:if>
			
			if (str.trim() != '') {
				str += '</div>';
				
				if ($('#hAllTask').length == 0) {
					if ($('[id^=dl]').length == 0)
						$('#solt_contents').append(str);
					else
						$('[id^=dl]').last().parent().parent().after(str);					
				}
				else {
					$('#hAllTask').before(str);
				}	
			}			
			
			var dlid;
			$('[id^=dl]').each(function() {
				dlid = $(this).attr('id').replace('dl', '');
				
				if (arrRefClass.indexOf(dlid) > -1)
					fnCreateSlotList(dlid, false);
				else
					fnCreateSlotList(dlid, true);
			});
			
			fnNewClass();
			/* $('#chkSet').change(function() {
				if ($(this).attr('checked') == 'checked') {
					$("#hidslottype").val('S');
					$("#hidtypeseq").val('2');					
					$("#hidslotname").val('Sys.string');
					$("#TYPE_NAME").val('Sys.string');
				}
			}); */
		}
		
		function fnNewClass() {
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/updateslotclassnewcheck.do' />");	
			comAjax.setCallback("");
			comAjax.ajax();
		}
		
		$('#CLASS_NAME').keyup(function() {
			var tempval = $(this).val().replace(/\s/g, '');
			tempval = tempval[0].toLocaleUpperCase() + tempval.substr(1);
			$(this).val(tempval);
		});
		
		$('#txtSortNo').keyup(function () {
            $(this).val($(this).val().replace(/[^0-9]/gi, ""));
        });
		
		$('#DESCRIPTION, #SLOT_DESCRIPTION, #Default_value, #txtSearch').keyup(function() {
			var tempval = $(this).val().replace(/['"]/g, '');
			$(this).val(tempval);
		});
		
		function fnCreateSlotList(classSeq, referchk) {
			if (slotdata[classSeq] != undefined) {
				var str = '';		
				$.each(slotdata[classSeq], function(key, value){
					str += '<li id="dd' + classSeq + '_' + value.SEQ_NO + '" >';					
					if (value.SLOT_TYPE != 'C') {
						var tempseq = value.SEQ_NO;
						if (value.SLOT_TYPE == 'O')
							tempseq = value.TYPE_SEQ;
						str += '<div id="tdslot' + classSeq + '_' + value.SEQ_NO + '" class="slotName" onclick="fnSlotDetail(' + tempseq + ', \'\', \'' + value.SLOT_NAME + '\', this);">' + value.SLOT_NAME + '</div>';
						str += '<div class="slotType" onclick="fnSlotDetail(' + tempseq + ', \'\', \'' + value.SLOT_NAME + '\', this);">' + value.TYPE_NAME + '</div>';	
					}
					else {
						str += '<div id="tdslot' + classSeq + '_' + value.SEQ_NO + '" class="slotName arrow_dot" style="background: 10px -1px url(\'../images/icon_squre.png\') no-repeat;" onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', this);"><p>' + value.SLOT_NAME + '</p><b></b></div>';
						str += '<div class="slotType" onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', this);">' + value.TYPE_NAME + '</div>';
					}										
					str += '<div class="slotBtn">';
					str += '<input type="hidden" id="hidslotName' + value.SEQ_NO + '" value="' + value.SLOT_NAME + '" />';
					str += '<input type="hidden" id="hidOrgType' + value.SEQ_NO + '" value="' + value.SLOT_TYPE + '" />';
					str += '<input type="hidden" id="hidOrgTypeSeq' + value.SEQ_NO + '" value="' + value.TYPE_SEQ + '" />';
					str += '<input type="hidden" id="hidOrgTypeName' + value.SEQ_NO + '" value="' + value.TYPE_NAME + '" />';
					str += '<span onclick="fnSlotEdit(' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', \'' + value.CLASS_NAME + '\', \'' + value.SLOT_NAME + '\', \'' + value.SLOT_DESCRIPTION + '\', \'' + value.SLOT_TYPE + '\', ' + value.TYPE_SEQ + ', \'' + value.TYPE_NAME + '\', \'' + value.PRCE_SLOT_SEQ + '\', \'' + value.PRCE_SLOT_NAME + '\', \'' + value.SET_FLAG + '\', \'' + value.INSTANCE_FLAG + '\', \'' + value.TYPE_SUB_SEQ + '\', \'' + value.PRCE_SLOT_SUB_SEQ + '\', ' + value.MIN_MAX_VALUE + ', \'' + value.DEFAULT_VALUE + '\');" class="modify"></span>';
					str += '<span onclick="fnSlotDelete(' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', \'' + value.CLASS_NAME + '\', \'' + value.SLOT_TYPE + '\', ' + value.TYPE_SEQ + ');" class="remove"></span>';
					str += '<div class="updown">';
					str += '<span onclick="fnSlotUpandDown(' + value.SEQ_NO + ', \'U\', this);" class="up"></span>';
					str += '<span onclick="fnSlotUpandDown(' + value.SEQ_NO + ', \'D\', this);" class="down"></span>';
					str += '</div>';
					str += '</div>';
					str += '</li>';
				});
				
				if (str != '') {
					str = '<ul>' + str + '</ul>';
				}
				$('#dl' + classSeq).append(str);
			}
		}
		
		function fnCreateSubSlotList(classSeq, slotSeq, parentclassSeq, obj) {			
			var $li = $(obj).parent();
			var $divSubSlot = $li.next();
			var Title = $li.find('[id^=tdslot]').text();
			var ArrCSeq = new Array();
			var paddingleft = Number($li.find('[id^=tdslot]').css('padding-left').replace('px', '')) + 20;
			var bgpaddingleft = paddingleft - 25;
			
			if($divSubSlot[0] == undefined || $divSubSlot[0].tagName != 'DIV') {
				var str = '<div name="divSubSlot" class="SubSlot_dat02" style="display:block;">';
				$.each(slotdata[classSeq], function(key, value){
					var id = parentclassSeq + '_' + slotSeq + '_' + classSeq + '_' + value.SEQ_NO;
					
					str += '<li id="dd' + id + '">';					
					if (value.SLOT_TYPE != 'C') {
						var tempseq = value.SEQ_NO;
						if (value.SLOT_TYPE == 'O')
							tempseq = value.TYPE_SEQ;
						str += '<div id="tdslot' + id + '" class="slotName" style="padding-left:' + paddingleft + 'px; background:' + bgpaddingleft + 'px -1px url(\'../images/slot_icon.png\') no-repeat; word-wrap: break-word;" onclick="fnSlotDetail(' + tempseq + ', \'' + Title  + '\', \'' + value.SLOT_NAME + '\', this);">' +  Title  + '.' + value.SLOT_NAME + '</div>';
						str += '<div class="slotType" onclick="fnSlotDetail(' + tempseq + ', \'' + Title  + '\', \'' + value.SLOT_NAME + '\', this);" >' + value.TYPE_NAME + '</div>';	
					}
					else {
						str += '<div id="tdslot' + id + '" class="slotName arrow_dot" style="padding-left:' + paddingleft + 'px; background:' + bgpaddingleft + 'px -1px url(\'../images/icon_squre.png\') no-repeat; word-wrap: break-word;" onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', \'' + parentclassSeq + '_' + slotSeq + '\', this);"><p>' +  Title  + '.' + value.SLOT_NAME + '</P><b></b></div>';
						str += '<div class="slotType" onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', \'' + parentclassSeq + '_' + slotSeq + '\', this);">' + value.TYPE_NAME + '</div>';
					}										
					str += '<div class="slotBtn">';
					if (value.SLOT_TYPE != 'C') {	
						str += '<span onclick="fnSlotEdit(\'' + id + '\', ' + value.CLASS_SEQ_NO + ', \'' + value.CLASS_NAME + '\', \'' + value.SLOT_NAME + '\', \'' + value.SLOT_DESCRIPTION + '\', \'' + value.SLOT_TYPE + '\', ' + value.TYPE_SEQ + ', \'' + value.TYPE_NAME + '\', \'' + value.PRCE_SLOT_SEQ + '\', \'' + value.PRCE_SLOT_NAME + '\', \'' + value.SET_FLAG + '\', \'' + value.INSTANCE_FLAG + '\', \'' + value.TYPE_SUB_SEQ + '\', \'' + value.PRCE_SLOT_SUB_SEQ + '\', ' + value.MIN_MAX_VALUE + ', \'' + value.DEFAULT_VALUE + '\');" class="modify"></span>';
					}
					str += '</div>';
					str += '</li>';
					
					ArrCSeq.push(id);
				});	
				str += '</div>';
				$li.after(str);
				
				for (var i = 0; i < ArrCSeq.length; i++) {
					var $tdslot = $('#tdslot' + ArrCSeq[i]);
				}
				
			}
			else {
				if($divSubSlot.css('display') == 'none')
					$divSubSlot.css('display', 'block');
				else 
					$divSubSlot.css('display', 'none');
			}													
		}
		
		function fnAddSpace(count) {
			var retrunValue = 20 * (count + 1);
			
			/* for (var int = 0; int < count + 1; int++) {
				retrunValue += '';
			} */
			return retrunValue+= 'px';
		}
		
		function fnClassSlide(obj) {
			if ($(obj).parent().next().css('display') == 'none') {				
				$(obj).parent().next().show();
			}
			else {
				$(obj).parent().next().hide();
			}
			
			$(obj).find('.open').toggle();
			$(obj).find('.close').toggle();
			
			fnpreventDefault(event);
		}
		
		function fnSlotDetail(seqno, classname, slotname, obj) {
			if(classname != '') classname = '.' + classname;
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotwrite.do' />");
			comSubmit.addParam("SLOT_SEQ_NO", seqno);			
			comSubmit.addParam("CLASS_NAME", $('#divclass' + $(obj).closest('ul').parent().attr('id').replace('dl', '')).text().trim() + classname);
			comSubmit.addParam("SLOT_NAME", slotname);
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnOpenReferenceClass() {
			pop = fnWinPop("<c:url value='/view/openreferencesearch.do' />" + '?Type=E&AGENT_SEQ=${AGENT_SEQ}', "Sensepop", 600, 550, 0, 0);
		}
		
		function fnOpenAction(type, idx) {
			if (type == 'C')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=Action" + idx + "&Type=F" + "&SubType=CO", "AgentSearchpop", 850, 550, 0, 0);
			else if (type == 'V')				
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=Action" + idx + "&Type=FNC&SubType=V", "AgentSearchpop", 430, 550, 0, 0);
			else if (type == 'PD')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=Action" + idx + "&Type=F" + "&SubType=PD", "AgentSearchpop", 850, 550, 0, 0);
			else if (type == 'PT')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=Action" + idx + "&Type=F" + "&SubType=PT", "AgentSearchpop", 850, 550, 0, 0);
		}
		
		function fnOpenCreateClass() {
			$('#CLASS_NAME').val('');
			$('#hidClassAgent').val('');
			$('#txtClassAgent').val('');			
			$('#DESCRIPTION').val('');
			$('#divDefinedTask').hide();
			fnLayerPop('divCreatClass', 'o');
			fnpreventDefault(event);
		}
		
		function fnOpenCreateSlot(classSeq, classname) {
			$('#SLOT_CLASS_NAME').val(classname);
			$("#search_option").val(classSeq);		
			$("#hidslottype").val('S');
			$("#hidtypeseq").val('2');
			$('#hidtypesubseq').val('');
			$("#hidslotname").val('Sys.string');
			$("#TYPE_NAME").val('Sys.string');			
			//$("#txtSortNo").val('');
			$('#Default_value').val('');
			$('#SLOT_DESCRIPTION').val('');
			$('#PRCE_SLOT_NAME').val('');
			$('#hidPreceSlotSeq').val('');
			$('#hidPreceSlotSubSeq').val('');
			//$('#SENSE_NAME').val('None');
			//$('#hidSenseSeq').val('');
			$('#chkSet').prop("checked", false);
			$('#chkInstance').prop("checked", true);
			
			$("#divCreatSlot").show();
			$('[id^=tr], #divDefine').hide();
			//fnAddDefine('', 0, '' ,'');			
			fnTypeEventBinding(classSeq);
			fnLayerPop('divCreatSlot', 'o');
			//$("#modalback").show();
			//$("#divCreatSlot").modal();			
			fnpreventDefault(event);
			$("#SLOT_NAME").val('').focus();
		}
		
		function fnTypeEventBinding(classSeq) {
			$("#TYPE_NAME").unbind('click');
			$("#TYPE_NAME").click(function() {
				if (typesearchChk) {
					alert('해당 클래스가 다른 클래스의 슬롯에서 사용중이면 타입을 수정할 수 없습니다.');
				}
				else {
					if ($('#chkSet').prop('checked') == true)
						fnOpenPop(classSeq, 'T', '', 'Y');
					else
						fnOpenPop(classSeq, 'T', '', 'N');
				}
			});
			$("#PRCE_SLOT_NAME").click(function() {
				fnOpenPop(classSeq, 'P', $(this).val(), $(this).parent().parent().parent().find('#SLOT_NAME').val());
			});
			$("#aSenseSearch").click(function() {
				fnOpenPop(classSeq, 'S', '', '');
			});
			$("#divDefine").on({
				click: function(e) {
					var hidSeq = $(this).attr('id').replace('aTargetSeq', '');
					fnOpenPop(classSeq, 'D', hidSeq, '');
				}
			}, '[id^=aTargetSeq]');
		}
		
		function fnSaveClass() {		
			if (!gfn_isNull(pop)) {
				pop.close();
			}
			
			var className = $('#CLASS_NAME').val().trim();
			if (className == '') {
				alert('클래스 명을 입력하세요.');
				fnpreventDefault(event);
				$('#CLASS_NAME').focus();
				return false;
			}
			
			if ($('#CLASS_NAME').val().length > 50) {
				alert('클래스명 명을 50자 이하로 입력해 주세요.');
				$('#CLASS_NAME').focus();
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_]/;
			if (reg.test(className)) {
				alert('클래스명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				fnpreventDefault(event);
				$('#CLASS_NAME').focus();
				return false;
			}
			
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/classnamecheck.do' />");	
			comAjax.setCallback("fnSaveClassCallBack");
			comAjax.addParam("CLASS_NAME", className);
			comAjax.ajax();
		}
		
		function fnSaveClassCallBack(data) {
			if (data.CHECK || $('#hidClassSeq').val() != '') {
				fnLoading();
				if(savechk) {
					savechk = false;
					var comSubmit = new ComSubmit();
					comSubmit.setUrl("<c:url value='/view/insertslotclass.do' />");
					comSubmit.addParam("depth1", '${depth1}');
					comSubmit.addParam("depth2", '${depth2}');
					comSubmit.addParam("depth3", '${depth3}');
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');					
					if ($('#hidClassAgent').val() == '')
						comSubmit.addParam("UPDATE_TASK", ', AGENT_SEQ = NULL');					
					else
						comSubmit.addParam("UPDATE_TASK", ', AGENT_SEQ = ' + $('#hidClassAgent').val());
					comSubmit.addParam("UPDATE_TASK_SEQ", $('#hidClassAgent').val());
					comSubmit.addParam("CLASS_NAME", $('#CLASS_NAME').val());
					comSubmit.addParam("DESCRIPTION", $('#DESCRIPTION').val());
					comSubmit.addParam("SEQ_NO", $('#hidClassSeq').val());										
					comSubmit.addParam("OLD_CLASS_NAME", $('#hidClassName' + $('#hidClassSeq').val()).val());
					comSubmit.submit();
					fnpreventDefault(event);
				}
				else {
					alert('저장중 입니다.');
					fnpreventDefault(event);
					return false;
				}
			}
			else {
				alert('이미 존재하는 클래스명 입니다.');
				$('#CLASS_NAME').focus();
			}
		}
		
		function fnSaveSlot() {
			if (!gfn_isNull(pop)) {
				pop.close();
			}
			
			var bSlotSave = false;
			if(savechk) {
				if ($('#search_option').val() == '') {
					alert('클래스를 선택하세요.');					
					fnpreventDefault(event);
					return false;
				}
				
				if ($('#SLOT_NAME').val().trim() == '') {
					alert('슬롯 이름을 입력하세요.');
					$('#SLOT_NAME').focus();
					fnpreventDefault(event);
					return false;
				}
						
				if ($('#SLOT_NAME').val().length > 50) {
					alert('슬롯 명을 50자 이하로 입력해 주세요.');
					$('#SLOT_NAME').focus();
					fnpreventDefault(event);
					return false;
				}
				
				var reg = /[^a-zA-Z0-9_]/;
				if (reg.test($('#SLOT_NAME').val())) {
					alert('슬롯명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$('#SLOT_NAME').focus();
					fnpreventDefault(event);
					return false;
				}
				
				if (slotdata[$('#search_option').val()] != undefined && $('#hidSlotSeq').val() == '') {
					$.each(slotdata[$('#search_option').val()], function(key, value){
						if (value.SLOT_NAME == $('#SLOT_NAME').val().trim()) {
							alert('이미 존재하는 슬롯 입니다.');
							$('#SLOT_NAME').focus();
							bSlotSave = true;
							return false;
						}
					});
				}
				
				if (bSlotSave) {
					fnpreventDefault(event);
					return false;
				}
				
				savechk = false;
				
				fnLoading();
				
				var arr = new Object();
				var arrobj = new Array();  
				
				$('#divDefine>div').each(function(idx) {
					var item = new Object();
					$table = $(this);
					item.TARGET_NAME = $table.find('[name=TARGET_NAME]').val();									
					item.TARGET_SEQ = $table.find('[name=hidTargetSeq]').val();
					if (item.TARGET_SEQ.trim() == '') {
						item.TARGET_SEQ = 0;
					}
					item.CON = $table.find('[name=CONDITION]').val();
					item.ACT = $table.find('[name=ACTION]').val();
					item.TARGET_SUB_SEQ = $table.find('[name=hidTargetSubSeq]').val();
					if (item.CON.trim() != '') {
						arrobj.push(item);	
					}										
				});
				
				arr.DEFINE_ITEM = arrobj;
				var jsoobj = JSON.stringify(arr);				
				
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertslot.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				if ('${AGENT_SEQ}' != '')
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				else
					comSubmit.addParam("AGENT_SEQ", 0);
				comSubmit.addParam("SLOT_NAME", $('#SLOT_NAME').val());
				comSubmit.addParam("SLOT_DESCRIPTION", $('#SLOT_DESCRIPTION').val());
				comSubmit.addParam("CLASS_SEQ_NO", $('#search_option').val());
				comSubmit.addParam("CLASS_NAME", $('#SLOT_CLASS_NAME').val());
				comSubmit.addParam("SLOT_TYPE", $('#hidslottype').val());
				comSubmit.addParam("TYPE_SEQ", $('#hidtypeseq').val());
				comSubmit.addParam("TYPE_NAME", $('#TYPE_NAME').val());
				comSubmit.addParam("TYPE_SUB_SEQ", $('#hidtypesubseq').val());
				comSubmit.addParam("PRCE_SLOT_SEQ", ($('#hidPreceSlotSeq').val().trim() == '') ? 0 : $('#hidPreceSlotSeq').val());
				comSubmit.addParam("PRCE_SLOT_NAME", $('#PRCE_SLOT_NAME').val());
				comSubmit.addParam("PRCE_SLOT_SUB_SEQ", $('#hidPreceSlotSubSeq').val());
				//comSubmit.addParam("SENSE_SEQ", ($('#hidSenseSeq').val().trim() == '') ? 0 : $('#hidSenseSeq').val());				
				//comSubmit.addParam("SENSE_NAME", $('#SENSE_NAME').val());				
				if ($('#chkSet').prop("checked") == true)					
					comSubmit.addParam("SET_FLAG", 'Y');
				else					
					comSubmit.addParam("SET_FLAG", 'N');
				
				if ($('#chkInstance').prop('checked') == true)					
					comSubmit.addParam("INSTANCE_FLAG", 'Y');
				else					
					comSubmit.addParam("INSTANCE_FLAG", 'N');
				
				if ($('#chkMin').prop('checked') == true && $('#chkMax').prop('checked') == true)
					comSubmit.addParam("MIN_MAX_VALUE", 3);
				else if ($('#chkMin').prop('checked') == true)
					comSubmit.addParam("MIN_MAX_VALUE", 1);
				else if ($('#chkMax').prop('checked') == true)
					comSubmit.addParam("MIN_MAX_VALUE", 2);
				else					
					comSubmit.addParam("MIN_MAX_VALUE", 0);
				
				var tmparr = new Array();
				if ($('#hidSlotSeq').val().indexOf('_') == -1) {
					comSubmit.addParam("SEQ_NO", $('#hidSlotSeq').val());
				}
				else {
					var tmparr = $('#hidSlotSeq').val().split('_');				
					comSubmit.addParam("SEQ_NO", tmparr[tmparr.length - 1]);
					comSubmit.addParam("SUB_SLOT_SEQ", $('#hidSlotSeq').val());
				}
				
				comSubmit.addParam("DEFAULT_VALUE", $('#Default_value').val());
				comSubmit.addParam("DEFINE_ITEM", jsoobj);			
				comSubmit.addParam("SORT_NO", $('#dl' + $('#search_option').val()).children().length);
				//comSubmit.addParam("SORT_NO", ($('#txtSortNo').val().trim() == 'sort no…') ? 0 : $('#txtSortNo').val());
				comSubmit.addParam("OLD_SLOT_TYPE", $('#hidOrgType' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("OLD_TYPE_SEQ", $('#hidOrgTypeSeq' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("OLD_TYPE_NAME", $('#hidOrgTypeName' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("OLD_SLOT_NAME", $('#hidslotName' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				comSubmit.submit();			
				fnpreventDefault(event);
			}
			else {
				alert('저장중 입니다.');
				fnpreventDefault(event);
				return false;
			}
		}
				
		function fnSlotDelete(seqno, cseqno, classname, slottype, typeseq) {			
			var comAjax = new ComAjax();				
			comAjax.setUrl("<c:url value='/view/deleteslotcheck.do' />");
			comAjax.setCallback("fnSlotDeleteCallBack");
			comAjax.setAsync(false);				
			comAjax.addParam("TYPE_SEQ", seqno);
			comAjax.addParam("WHERE_SQL", " AND SLOT_TYPE = \'O\'");
			comAjax.ajax();	
			
			if ($('#hidSlotDelChk').val() == 'Y') {
				if (confirm('해당 항목을 삭제 하시겠습니까?\r\n[주의] 해당 항목이 포함된 지식이 해당 항목의 삭제로 인하여 오류가 발생할 수 있습니다.')) {
					var comSubmit = new ComSubmit();
					comSubmit.setUrl("<c:url value='/view/deleteslot.do' />");					
					comSubmit.addParam("SEQ_NO", seqno);
					comSubmit.addParam("CLASS_SEQ_NO", cseqno);
					comSubmit.addParam("CLASS_NAME", classname);
					comSubmit.addParam("SLOT_TYPE", slottype);
					comSubmit.addParam("TYPE_SEQ", typeseq);
					comSubmit.addParam("OLD_SLOT_NAME", $('#hidslotName' + seqno).val());
					comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
					comSubmit.addParam("depth1", '${depth1}');
					comSubmit.addParam("depth2", '${depth2}');
					comSubmit.addParam("depth3", '${depth3}');
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
					comSubmit.submit();
				}
			}		
		}
		
		function fnSlotDeleteCallBack(data) {
			if (data.STATUS == 'F') {
				$('#hidSlotDelChk').val('N');
				alert('다른 슬롯에서 타입으로 사용되고 있는 슬롯 입니다.');
			}
			else
				$('#hidSlotDelChk').val('Y');
		}
		
		function fnSlotEdit(seqno, classseq, classname, slotname, slotdescript, slottype, typeseq, typename, prceseq, prceslotname, setflag, instanceflag, typesubseq, prcesubseq, minmaxvalue, defaultvalue) {
			//$(window).scrollTop(0);
			$('#hidSlotSeq').val(seqno);
			$("#search_option").val(classseq);
			$("#search_option").trigger("chosen:updated");
			$("#SLOT_CLASS_NAME").val(classname);
			$("#SLOT_NAME").val(slotname);
			$("#hidslottype").val(slottype);
			$("#hidtypeseq").val(typeseq);
			$('#hidtypesubseq').val(typesubseq);
			$("#TYPE_NAME").val(typename);
			//$("#txtSortNo").val(sortno);
			$('#Default_value').val(defaultvalue);
			$('#SLOT_DESCRIPTION').val(slotdescript);			
			$('#hidPreceSlotSeq').val(prceseq);
			$('#PRCE_SLOT_NAME').val(prceslotname);
			$('#hidPreceSlotSubSeq').val(prcesubseq);
			//$('#hidSenseSeq').val(senseseq);
			/* (sensename == '') ? $('#SENSE_NAME').val('None') : $('#SENSE_NAME').val(sensename);
			if (setflag == 'Y') {
				$('#chkSet').picker('check');
				$('#SENSE_NAME').val('None');
			}
			else
				$('#chkSet').picker("uncheck"); */
				
			if (setflag == 'Y')
				$('#chkSet').prop("checked", true);
			else
				$('#chkSet').prop("checked", false);
			
			if (instanceflag == 'Y')
				$('#chkInstance').prop("checked", true);
			else
				$('#chkInstance').prop("checked", false);
				
			if (minmaxvalue == 3) {
				$('#chkMin').prop("checked", true);
				$('#chkMax').prop("checked", true);
			}
			else if (minmaxvalue == 1) {
				$('#chkMin').prop("checked", true);
			}
			else if (minmaxvalue == 2) {
				$('#chkMax').prop("checked", true);
			}
			defaultvalue
			fnGetDefine(seqno, classseq);					
			fnTypeEventBinding(classseq);
			$('[id^=tr], #divDefine').show();
			$("#divCreatSlot").show();
			fnLayerPop('divCreatSlot', 'o');
			//$("#modalback").show();
			//$("#divCreatSlot").modal();
			fnpreventDefault(event);
		}
		
		function fnGetDefine(seqno, classseq) {
			$('#divDefine').empty();
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/getslotdefine.do' />");	
			comAjax.setCallback("fnGetDefineComplete");
			comAjax.addParam("SLOT_SEQ_NO", seqno);
			comAjax.addParam("TYPE_SEQ", classseq);
			comAjax.addParam("WHERE_SQL", " AND SLOT_TYPE = 'C'");
			comAjax.ajax();	
		}
		
		function fnGetDefineComplete(data) {
			if (data.list.length != 0) {
				$.each(data.list,  function(key, value) {
					fnAddDefine(value.TARGET_NAME, value.TARGET_SEQ, value.CON, value.ACT, value.TARGET_SUB_SEQ);
				});
			}
			
			if (data.list2.length != 0) {
				typesearchChk = true;
			}
			
			if (data.SLOT_SEQ_NO.indexOf('_') > -1) {
				
				/* if (!gfn_isNull(data.addition)) {
					$('#Default_value').val(data.addition.DEFAULT_VALUE);
					$('#SLOT_DESCRIPTION').val(data.addition.SLOT_DESCRIPTION);
					$('#hidPreceSlotSeq').val(data.addition.PRCE_SLOT_SEQ);
					$('#PRCE_SLOT_NAME').val(data.addition.PRCE_SLOT_NAME);
					$('#hidPreceSlotSubSeq').val(data.addition.PRCE_SLOT_SUB_SEQ);
				}
				else {
					$('#Default_value').val('');
					$('#SLOT_DESCRIPTION').val('');
					$('#hidPreceSlotSeq').val('');
					$('#PRCE_SLOT_NAME').val('');
					$('#hidPreceSlotSubSeq').val('');
				} */
			}
		}
		
		function fnClassDelete(seqno) {
			$('#hidClassSeq').val(seqno);
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslottypecheck.do' />");	
			comAjax.setCallback("fnClassDeleteCallBack");
			comAjax.addParam("TYPE_SEQ", seqno);
			comAjax.addParam("SEQ_NO", seqno);
			comAjax.addParam("WHERE_SQL", " AND SLOT_TYPE = 'C'");
			comAjax.ajax();
		}
		
		function fnClassDeleteCallBack(data) {
			if (data.STATUS == 'S') {
				if (confirm('해당 항목을 삭제 하시겠습니까?\r\n[주의] 해당 항목이 포함된 지식이 해당 항목의 삭제로 인하여 오류가 발생할 수 있습니다.')) {
					var comSubmit = new ComSubmit();
					comSubmit.setUrl("<c:url value='/view/deleteslotclass.do' />");
					comSubmit.addParam("SEQ_NO", $('#hidClassSeq').val());
					comSubmit.addParam("depth1", '${depth1}');
					comSubmit.addParam("depth2", '${depth2}');
					comSubmit.addParam("depth3", '${depth3}');
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
					comSubmit.submit();
					fnpreventDefault(event);
				}
			}
			else if (data.STATUS == 'UC') {
				alert('다른 슬롯에서 타입으로 사용되고 있는 클래스 입니다.')
			}
			else if (data.STATUS == 'US') {
				alert('다른 슬롯에서 타입으로 사용되고 있는 슬롯이 포함된 클래스 입니다.')
			}
			$('#hidClassSeq').val();
		}
		
		
		function fnClassEdit(seqno, name, descript, agentseq, agentname) {
			$(window).scrollTop(0);
			$('#hidClassSeq').val(seqno);
			$('#CLASS_NAME').val(name);
			$('#hidClassAgent').val(agentseq);
			$('#txtClassAgent').val(agentname);
			$('#divDefinedTask').show();
			$('#DESCRIPTION').val(descript);
			fnLayerPop('divCreatClass', 'o');	
			fnpreventDefault(event);
		}
		
		function fnSearch() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotlist.do' />");
			comSubmit.addParam("SLOT_NAME", ($('#txtSearch').val() == 'search slots…') ? '' : $('#txtSearch').val());
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnCancelClass() {
			fnLayerPop('divCreatClass', 'c');			
			$('#hidClassSeq').val('');
			$('#divClassWarning').hide();
			if (!gfn_isNull(pop)) {
				pop.close();
			}
			//$("#modalback").hide();
			//$.modal.close();
			fnpreventDefault(event);
		}

		function fnCancelSlot() {
			fnLayerPop('divCreatSlot', 'c')
			$('#hidSlotSeq').val('');
			$('#divSlotWarning').hide();
			typesearchChk = false;
			if (!gfn_isNull(pop)) {
				pop.close();
			}
			//$("#modalback").hide();			
			//$.modal.close();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(classSeq, pType, hidSeq, nchk) {			
			/* if($('#search_option').val() == '') {
				alert('클래스를 선택하세요.');
				return false;
			} */
			var Title = 'Slot type';
			if (pType == 'P')
				Title = 'Preceding slots';
			else if (pType == 'D')
				Title = 'Target  slots';	

			if (pType == 'S') {
				/* var Sense = ($('#SENSE_NAME').val() == '') ? 'None' : $('#SENSE_NAME').val();
				fnWinPop("<c:url value='/view/opensensesearch.do' />" + '?Sense=' + Sense, "Sensepop", 430, 550, 0, 0); */
			}
			else if (pType == 'P') {
				pop = fnWinPop("<c:url value='/view/requestslotsearch.do' />?Type=P&Cname=" + hidSeq + "&Sname=" + nchk + "&Chklist=" + $('#PRCE_SLOT_NAME').val().replace(/\s/g, ''), "SlotSearchpop", 430, 550, 0, 0);
			}
			else
				pop = fnWinPop("<c:url value='/view/opentypesearch.do' />" + "?pType=" + pType + "&Type=" + $("#hidslottype").val() + "&Seq=" + $("#hidtypeseq").val() + "&SubSeq=" + $('#hidtypesubseq').val() + "&ClassSeq=" + classSeq + "&hidSeq=" + hidSeq + "&nchk=" + nchk + "&Title=" + encodeURIComponent(Title), "Typepop", 430, 550, 0, 0);			
			
			fnpreventDefault(event);
		}
		
		function fnOpenLuaPop(obj) {
			var width = '713';
			var heigth = '626';
			if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
				width = '613';
				heigth = '400';	
			}
			
			var pType = 'SC';
			
			if ($(obj).attr('id').indexOf('Action') > -1) {				
				pop = fnWinPop("<c:url value='/view/openactionwrite.do' />" + "?obj=" + $(obj).attr('id') + "&Type=S", "AgentActionpop", 570, 470, 0, 0);
			}
			else			
				pop = fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).attr('id') + "&flag=A&Type=" + pType, "IntentsPop", width, heigth, 0, 0);
		}
		
		function fnOpenTask() {		
			pop = fnWinPop("<c:url value='/view/opentasksearch.do' />" + "?Type=S", "TaskPop", 430, 550, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnClassInstans(seqno, classname, obj) {
			if ($(obj).parent().parent().next().find('ul>li').length == 0) {
				alert('slot을 생성 후 instance를 작성하세요.');
				return false;
			}
			
			var iCnt = 0;
			if (slotdata[seqno] != undefined) {				
				$.each(slotdata[seqno], function(key, value){
					if (value.INSTANCE_FLAG == 'Y') {
						iCnt++;
					}					
				});
			}
			
			if (iCnt == 0) {
				alert('Instance와 연동된 slot이 없습니다.\n연동 후 사용할 수 있습니다.');
				return false;
			}
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotinstansces.do' />");
			comSubmit.addParam("SEQ_NO", seqno);
			comSubmit.addParam("CLASS_NAME", classname);
			comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();			
			fnpreventDefault(event);	
		}			
		
		function fnAddDefine(targetname, targetseq, con, act, targetsubseq) {
			var str = '';
			var $divDefine = $('#divDefine');
			var defineCnt = $divDefine.find('.divCreatSlot_del').length;
							
			str += '<div id="divDef' + defineCnt + '" class="divCreatSlot_del">';
			str += '<span>define<a onclick="fnDefineDelete(' + defineCnt + ');" href="#">delete</a></span>';
			str += '<div class="popInput" style="display:none;">';
			str += '<div class="title">Target slot</div>';
			str += '<div class="input"><input type="text" id="TARGET_NAME' + defineCnt + '" name="TARGET_NAME" value="' + targetname + '" style="border:none; width:calc(100% - 30px); float:left; border:1px solid #ddd;" />';
			str += '<a id="aTargetSeq' + defineCnt + '" href="#" title="검색" class="solt_search search_hide_btn_link"><span class="search_hide_btn">search</span></a>';
			str += '<input type="hidden" id="hidTargetSeq' + defineCnt + '" name="hidTargetSeq" value="' + targetseq + '" />';
			str += '<input type="hidden" id="hidTargetSubSeq' + defineCnt + '" name="hidTargetSubSeq" value="' + targetsubseq + '" />';
			str += '</div>';
			str += '</div>';	
			str += '<div class="popInput">';
			str += '<div class="title">Condition</div>';
			str += '<div class="input"><input id="CONDITION' + defineCnt + '" readonly="readonly" onclick="fnOpenLuaPop(this);" name="CONDITION" type="text" value=\'' + con + '\' maxlength="2000" /></div>';
			str += '</div>';
			str += '<div class="popInput">';
			str += '<div class="title">Action</div>';
			str += '<div class="input"><input id="Action' + defineCnt + '" readonly="readonly" onclick="fnOpenLuaPop(this);" name="ACTION" type="text" value=\'' + act + '\' maxlength="2000" /></div>';
			str += '</div>';			
			str += '</div>';
			
			$divDefine.append(str);
		}
		
		function fnDefineDelete(seq) {
			$('#divDef' + seq).remove();
		}
		
		function fnSlotUpandDown(seqno, type, obj) {
			$dl = $(obj).parent().parent().parent();
			var sindex = $dl.index();
			var eindex;
			var eseqno;
			var bSaveChk = false;
			
			if (type == 'D') {
				eindex = $dl.next().index();
				eseqno = $dl.next().attr('id').substr($dl.attr('id').indexOf('_') + 1);
				$dl.next().after($dl);
				bSaveChk = true;
			}
			else {
				if ($dl.prev().get(0).tagName != 'UL') {
					eindex = $dl.prev().index();
					eseqno = $dl.prev().attr('id').substr($dl.attr('id').indexOf('_') + 1);
					$dl.prev().before($dl);
					bSaveChk = true;
				}				
			}
			
			if (bSaveChk) {
				var comAjax = new ComAjax();			
				comAjax.setUrl("<c:url value='/view/updateslotsort.do' />");			
				comAjax.addParam("S_SEQ_NO", seqno);
				comAjax.addParam("E_SEQ_NO", eseqno);
				comAjax.addParam("SINDEX", sindex);
				comAjax.addParam("EINDEX", eindex);
				comAjax.ajax();
			}
		}
		
		function fnCheck(obj) {
			if ($(obj).prop('checked') == false) {				
				$(obj).prop("checked", false);
			}
			else {
				$(obj).prop("checked", true);
				if($(obj).attr('id') == 'chkSet' && $("#TYPE_NAME").val() != 'Sys.number' && $("#TYPE_NAME").val() != 'Sys.string') {					
					$("#hidslottype").val('S');
					$("#hidtypeseq").val('2');
					$('#hidtypesubseq').val('');
					$("#hidslotname").val('Sys.string');
					$("#TYPE_NAME").val('Sys.string');
					//$('#SENSE_NAME').val('None');	
				}
			}
		}
		
		function fnReferCancel(seqno) {
			if (confirm('해당 Class 참조를 해제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deletereferslotclass.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_SEQ", seqno);			
				comSubmit.submit();
				fnpreventDefault(event);	
			}				
		}
		
		function fnSlotMapping() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotmapping.do' />");			
			//comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);	
		}
	</script>
</body>
</html>