<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>챗봇</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">
	var bsaving = false;

	$(function() {
		$('#txtTalk').bind({
			click: function() {				
				if ($(this).val() == 'Say to GenieTalkTalk ...') $(this).val('');					
			},
			keypress: function() {
				if (event.keyCode == 13) {
					$this = $(this);
					if ($this.val().trim() != '') {
						if ($('#divTalk p').length > 1) {
							$('#ptemp').text($('#sptemp').text()).removeAttr('id');
						}
						
						var str = '';				
						str += '<p class="chatbot_talk01">' + $this.val() + '</p>';
						str += '<p class="chatbot_talk02" id="ptemp"><span id="sptemp">loading...</span><a href="#" onclick="fnGetSysTalk(\'R\');"><img src="../images/chatbot_icon01.png" /></a><a href="#" onclick="fnEditTalk(this);"><img src="../images/chatbot_icon02.png" /></a></p>';
											
						$('#divTalk').append(str);
						fnSaveTalk('U', $this.val());
						$this.val('').prop('disabled', true);						
						fnEditTalkClear();
						fnGetSysTalk('N');
						
					}
					else {
						alert('값을 입력하세요');
					}
					$this.focus();
				}
			}
		});
		
		$('#ObjectList').on({ 
			'focusout': function(e){			
				if(gfn_isNull($(this).text()))
					$(this).text('입력 하세요...');
			},
			click: function(e){			
				if($(this).text() == '입력 하세요...')
					$(this).text('');
			}
		}, '[name=OBJECT_NAME]');
					
		$('#solt_contents').on({				
			'click focusin': function(e) {
				var $this = $(this);
									
				if($this.parent().next().length == 0) {
					$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
				}
			},
			keydown: function(e) {
				if(e.keyCode == 9) {						
					//$(this).next().focus();
					$(this).parent().next().children().eq(0).focus();
					fnCursorEnd($(this).parent().next().children().eq(0).get(0));
					fnpreventDefault(e);
				}
			}			
		}, '[name=OBJECT_DETAIL_NAME]');
		
		$('#solt_contents').on('click', '.close_btn', function(e) {				
			if($(this).parent().siblings().length > 0)				
				$(this).parent().remove();
		});
	});
	
	function fnGetSysTalk(type) {
		if (type == 'N') {
			$('#sptemp').text('시스템 발화');
			fnSaveTalk('S', $('#sptemp').text());
		}
		else {
			$('#sptemp').text('시스템 재발화');
			fnSaveTalk('SP', $('#sptemp').text());
			fnpreventDefault(event);	
		}
		
		$('#divTalk').scrollTop($("#divTalk")[0].scrollHeight);
		$('#txtTalk').prop('disabled', false);
	}
	
	function fnEditTalk(obj) {
		if ($(obj).parent().find('input').length == 0) {
			$(obj).parent().append('<input id="txtEditTalk" type="text" value="Say to GenieTalkTalk ..." onclick="if (this.defaultValue == this.value) this.value = \'\'" class="Clear_search_chatbot_con" /><a id="aEditTalk" href="#" onclick="fnEditTalkSave(this);" title="Edit" class="Clear_search_btn_p">Save</a>');	
		}	
		fnpreventDefault(event);
	}
	
	function fnEditTalkSave(obj) {
		$('#sptemp').text($(obj).prev().val());
		fnSaveTalk('SM', $('#sptemp').text());
		fnEditTalkClear();
		fnpreventDefault(event);
	}
	
	function fnEditTalkClear() {
		$('#txtEditTalk, #aEditTalk').remove();
	}
	
	function fnSaveTalk(type, say) {		
		var comAjax = new ComAjax();
		comAjax.setUrl("<c:url value='/view/savechatbottalk.do' />");
		comAjax.setCallback("fnSaveTalkCallBack");
		comAjax.addParam("TYPE", type);
		comAjax.addParam("SAY", say);
		comAjax.addParam("USER_ID", getCookie("USER_ID"));
		comAjax.ajax();	
	}
	
	function fnSaveTalkCallBack(data) {
		if (data.STATUS != true) {
			alert('로그 저장 중 오류가 발생 하였습니다.');
		}		
	}
	
	function fnAddRow() {
		$("#ObjectList").append($("#EmptyRow").html());
		fnpreventDefault(event);
	}
	
	function fnObjectDelete(seqno, obj) {
		//if($('#ObjectList [name=tbOBJ_LIST]').length > 1) {
			$(obj).parent().parent().parent().parent().parent().remove();					
		//}
		
		if(seqno != 0) {								
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/deletechatbotobject.do' />");				
			comAjax.addParam("SEQ_NO", seqno);			
			comAjax.ajax();	
		}						
		fnpreventDefault(event);
	}
	
	function fnOpenPop(type) {
		if (type == 'L') 					
			fnWinPop("<c:url value='/view/popchatbottalk.do' />" + "?USER_ID=" +  encodeURIComponent(getCookie("USER_ID")), "AgentPop", 713, 626, 0, 0);
		else
			fnWinPop("<c:url value='/view/popchatbotlean.do' />" + "?USER_SEQ=" +  encodeURIComponent(getCookie("USER_SEQ")), "AgentPop", 585, 590, 0, 0);
		fnpreventDefault(event);
	}
	
	function fnGetObjDetail(seqno) {
		var comAjax = new ComAjax();
		comAjax.setUrl("<c:url value='/view/getchatbotobjectdetail.do' />");
		comAjax.setCallback("fnCreateObjDetail");
		comAjax.addParam("PARENT_SEQ_NO", seqno);			
		comAjax.ajax();				
	}
	
	function fnCreateObjDetail(data) {
		var iCnt = 0;
		var str = "";			
		$.each(data.DETAIL_LIST, function(key, value){
			str += '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true">' + value.OBJECT_NAME + '</div><a class="close_btn f_right" href="#"></a></div>';
			iCnt++;
		});
		
		if(iCnt == 0)
			str = '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>';
		
		$('#OBJECT_NAME_' + data.PARENT_SEQ_NO).parent().next().append(str);
	}
	
	function fnObjectSave() {
		if (bsaving == true) {
			fnLoading();
			return false;
		}
		else 				
			bsaving = true;

		fnLoading();
		
		var arr = new Object();
		var arrobj = new Array();
		var saveChk = true;
		var arrobjChk = new Array();
		Array.prototype.contains = function(elem) {
			for (var i in this) {
				if (this[i] == elem) return true;
			}
			return false;
		}
					
		$('#ObjectList [name=tbOBJ_LIST]').each(function(idx, e) {
			var $this = $(this);
			var item = new Object();
			var arrobjsub = new Array();
			var id = $this.find('[name=OBJECT_NAME]').attr("id");
			var seqno;
			var bObjName = true;
			var iObjDtl = 0;
			
			if(gfn_isNull(id)) {
				item.SEQ_NO = "0";
			} else {
				item.SEQ_NO = id.replace('OBJECT_NAME_', '');					
			}
			
			item.OBJECT_NAME = $this.find('[name=OBJECT_NAME]').text();
			if (item.OBJECT_NAME == '입력 하세요...' || item.OBJECT_NAME.trim() == '') {
				bObjName = false;
			}
			
			if (arrobjChk.contains(item.OBJECT_NAME)) {
				alert('이미 존재하는 값 입니다.');
				$this.find('[name=OBJECT_NAME]').focus();
				saveChk = false;					
				fnLoading();	
				return false;
			}
			
			if (bObjName) {
				$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
					if(!gfn_isNull($(this).text())) {
						var subitem = new Object();
						subitem.OBJECT_NAME = $(this).text();
						//subitem.PARENT_SEQ_NO = item.SEQ_NO;
						arrobjsub.push(subitem);
						iObjDtl ++;
					}
				});							
								
				arrobjChk.push(item.OBJECT_NAME);
				
								
				item.OBJECT_DETAIL = arrobjsub;				
				arrobj.push(item);
			}
		});
		if (saveChk) {
			bsaving = false;
			arr.OBJECT_ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			 
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/insertchatbotobject.do' />");	
			comAjax.setCallback("fnObjectSaveCallBack");
			comAjax.addParam("USER_SEQ", getCookie("USER_SEQ"));
			comAjax.addParam("USER_ID", getCookie("USER_ID"));
			comAjax.addParam("OBJECT_ITEM", jsoobj);
			comAjax.ajax();
		}
		fnpreventDefault(event);	
	}
	
	function fnObjectSaveCallBack(data) {
		if (data.STATUS)
			alert('저장 되었습니다.');
		else
			alert('저장 중 오류가 발생 하였습니다.');
	}
	
	function fnBack() {
		window.location = "<c:url value='/view/main.go' />";
	}
</script>

</head>
<body>
	<div class="chatbot"> 
        <div class="chatbot_top">
            <div class="chatbot_h1">
                <h1>챗봇</h1>
            </div>
            <div class="chatbot_t_btn">
                <a href="#" onclick="fnOpenPop('C');">챗봇 학습 코퍼스 작성</a>
                <a href="#" onclick="fnBack();">이전 페이지로 이동</a>
            </div>
        </div>
        <div class="chatbot_con">
        	<div id="divTalk" class="chatbot_con01">
            	<!-- <p class="chatbot_talk01">Hi</p>
                <p class="chatbot_talk02">Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.</p>
                <p class="chatbot_talk01">Hi</p>
                <p class="chatbot_talk02">Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.</p>
                <p class="chatbot_talk01">Hi</p>
                <p class="chatbot_talk02">Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.</p>
                <p class="chatbot_talk01">How are you?</p>
                <p class="chatbot_talk02">Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.Hello.
                    <label for="Clear_search">
                        <input type="text" value="Say to GenieTalkTalk ..." onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="Clear_search" name="Clear_search" class="Clear_search_chatbot_con" /><a href="#" title="Clear" class="Clear_search_btn_p">Save</a>
                    </label>
                </p>
                <p class="chatbot_talk01">How are you?</p>
                <p class="chatbot_talk02">I'm fine <a href="#"><img src="../images/chatbot_icon01.png" /></a><a href="#"><img src="../images/chatbot_icon02.png" /></a></p> -->
            </div>
            <div class="chatbot_con02">
                <div class="search_box Clear_search_box" style="height:50px !important; padding-top:10px;">
                    <label for="Clear_search">
                        <input id="txtTalk" value="Say to GenieTalkTalk ..." style="margin-left:30px;" type="text" class="Clear_search" /><a href="#" onclick="fnOpenPop('L');" title="Talk" class="Clear_search_btn">Log</a>
                    </label>
                </div>            	
            </div>
        </div>
        <hr />
        <div class="chatbot_bottom" style="padding-top:35px;">
        	<div style="text-align: center;">
        		<span style="font-size: 18px; color: #555555; font-weight: bold;">의미 추가/삭제</span>
        	</div>
        	<div style="text-align: right; margin: 0px 145px;">
        		<a href="#" onclick="fnObjectSave();" style="background: #0488da; color: #ffffff; border-radius: 3px; padding: 5px 9px 6px; vertical-align: middle;">저장</a>
        	</div>
            <div id="solt_contents" class="solt_contents">				
				<dl id="ObjectList" class="class_name mt20">					
					<c:choose>						
						<c:when test="${fn:length(OBJ_LIST) > 0}">
							<c:forEach items="${OBJ_LIST}" var="row">
								<dd class="solt_dd" style="margin:-1px 145px 0px 145px;">
									<table name="tbOBJ_LIST" class="slot_modify">
										<tr>								
											<th>
												<div id="OBJECT_NAME_${row.SEQ_NO}" name="OBJECT_NAME" contenteditable="true">${row.OBJECT_NAME}</div>
											</th>
											<td class="slot_modify01">										
												<script>fnGetObjDetail(${row.SEQ_NO});</script>							
											</td>
											<td class="slot_modify02">
												<a href="#" class="delete_btn" onclick="fnObjectDelete(${row.SEQ_NO}, this);"><span class="hide">삭제</span></a>
											</td>																	
										</tr>
									</table>
								</dd>
							</c:forEach>
						</c:when>					
						<c:otherwise>
							<dd class="solt_dd" style="margin:-1px 145px 0px 145px;">
								<table name="tbOBJ_LIST" class="slot_modify">
									<tr>
										<th>
											<div name="OBJECT_NAME" contenteditable="true">입력 하세요...</div>
										</th>
										<td class="slot_modify01">
											<!-- <div name="OBJECT_DETAIL_NAME" class="slot_modify_set01" contenteditable="true">아수라</div>&nbsp; -->										
											<div class="slot_modify_set02 f_left"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>										
										</td>
										<td class="slot_modify02">
											<a href="#" class="delete_btn" onclick="fnObjectDelete(0, this);"><span class="hide">삭제</span></a>
										</td>
									</tr>
								</table>
							</dd>						
						</c:otherwise>
					</c:choose>				
				</dl>
				
				<div class="addrow" style="margin:10px 145px 10px 145px;">
					<a href="#" onclick="fnAddRow();">+Add row</a>
				</div>
			</div>
        </div>        
    </div>
    
    <div id="EmptyRow" style="display:none;">
		<dd class="solt_dd" style="margin:-1px 145px 0px 145px;">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tr>
					<th>
						<div name="OBJECT_NAME" contenteditable="true">입력 하세요...</div>
					</th>
					<td class="slot_modify01">
						<!-- <div name="OBJECT_DETAIL_NAME" class="slot_modify_set01" contenteditable="true">아수라</div>&nbsp; -->										
						<div class="slot_modify_set02 f_left"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>										
					</td>
					<td class="slot_modify02">
						<a href="#" class="delete_btn" onclick="fnObjectDelete(0, this);"><span class="hide">삭제</span></a>
					</td>
				</tr>
			</table>
		</dd>
	</div>
</body>
</html>