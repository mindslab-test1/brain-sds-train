<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>챗봇</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">
	$(function() {
		$('#txtU').click(function() {
			if($('#divU').find('[name=txtUP]').length == 0) {
				$('#divU').append(fnAddEdit('UP'));
			}
		});
		
		$('#txtS').click(function() {
			if($('#divS').find('[name=txtSP]').length == 0) {
				$('#divS').append(fnAddEdit('SP'));
			}
		});
		
		$('#divU').on({				
			'click focusin': function(e) {						
				if($(this).parent().next().length == 0) {
					$('#divU').append(fnAddEdit('UP'));
				}
			}	
		}, '[name=txtUP]');
		
		$('#divS').on({				
			'click focusin': function(e) {						
				if($(this).parent().next().length == 0) {
					$('#divS').append(fnAddEdit('SP'));
				}
			}	
		}, '[name=txtSP]');
		
		function fnAddEdit(name) {
			var str = '<div style="margin-left:20px;">';
			str += '<span class="t_title_buu">' + name +':</span>';
			str += '<input type="text" name="txt' + name + '" class="Clear_search" style="width:350px;" />';
			str += '</div>';
			return str;
		}
		
		$('#divData').scrollTop($("#divData")[0].scrollHeight);
	});
	
	function fnSaveEdit() {			
		if ($('#txtU').val().trim() != '' && $('#txtS').val().trim() != '') {		
			var arr = new Object();
			var arrobjsub = new Array();
			
			$('#divU [name=txtUP]').each(function(idx, e) {								
				var item = new Object();
				if ($(this).val().trim() != '') {
					item.SAY = $(this).val();
					arrobjsub.push(item);				
				}			
			});
			
			arr.UP_ITEM = arrobjsub;				
			
			arrobjsub = new Array();
			$('#divS [name=txtSP]').each(function(idx, e) {
				var item = new Object();
				if ($(this).val().trim() != '') {
					item.SAY = $(this).val();
					arrobjsub.push(item);				
				}			
			});
			
			arr.SP_ITEM = arrobjsub;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertchatbotlean.do' />");
			comSubmit.addParam("U_SAY", $('#txtU').val());
			comSubmit.addParam("S_SAY", $('#txtS').val());
			comSubmit.addParam("TRAIN_ITEM", jsoobj);
			comSubmit.addParam("USER_SEQ", getCookie("USER_SEQ"));
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		else
			alert('U, S를 모두 입력해주세요.');
	}
	
	function fnSayDelete(seqno, type, parentseq, obj) {
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/view/deletechatbotlean.do' />");		
		comSubmit.addParam("USER_SEQ", getCookie("USER_SEQ"));		
		if (type == 'UP' || type == 'SP') {
			comSubmit.addParam("SEQ_NO", seqno);
			comSubmit.addParam("DEL_TYPE", 'S');
		}
		else if (type == 'U') {
			if($(obj).parent().parent().find('[name=UP]').length == 0) {
				comSubmit.addParam("DEL_TYPE", 'A');
				comSubmit.addParam("SEQ_NO", parentseq);	
			}
			else {
				comSubmit.addParam("DEL_TYPE", 'U');
				comSubmit.addParam("SEQ_NO", seqno);				
				comSubmit.addParam("UPDATE_SEQ_NO", $(obj).parent().next().find('input:hidden').val());
				comSubmit.addParam("TRAIN_TYPE", 'U');
			}
		}
		else if (type == 'S') {
			if($(obj).parent().parent().find('[name=SP]').length == 0) {
				comSubmit.addParam("DEL_TYPE", 'A');
				comSubmit.addParam("SEQ_NO", parentseq);	
			}
			else {
				comSubmit.addParam("DEL_TYPE", 'U');
				comSubmit.addParam("SEQ_NO", seqno);
				comSubmit.addParam("UPDATE_SEQ_NO", $(obj).parent().next().find('input:hidden').val());
				comSubmit.addParam("TRAIN_TYPE", 'S');
			}
		}
		comSubmit.submit();			
		fnpreventDefault(event);
	}
	
	function fnSayTextFile() {
		var str = '';
		$('#divData div').each(function() {
			$(this).find('span').each(function() {				
				str += $(this).text() + '\n';	
			});
			str += '\n';
		});
		var comAjax = new ComAjax();			
		comAjax.setUrl("<c:url value='/view/savechatbotlean.do' />");	
		comAjax.setCallback("fnSayTextFileCallBack");		
		comAjax.addParam("USER_ID", getCookie("USER_ID"));
		comAjax.addParam("DATA", str);
		comAjax.ajax();
		fnpreventDefault(event);
	}
	
	function fnSayTextFileCallBack(data) {
		if (data.STATUS)
			alert('저장 되었습니다.');
		else
			alert('저장 중 오류가 발생 하였습니다.');	
	}
</script>

</head>
<body style="min-width:0px;">
	<div class="popup_function" style="height:540px !important;">
		
		<div id="divData" class="chatbot_popup">
			<c:set var="ParentSeq" value=""></c:set>
			<c:set var="iCnt" value="0"></c:set>
			<c:forEach items="${TRAIN_LIST }" var="row">
				<c:if test='${ParentSeq != row.PARENT_SEQ_NO}'>
					<c:if test='${iCnt > 0}'>
						</div>
					</c:if>
					<div class="chatbot_text">
				</c:if>
					<p><span name="${row.TRAIN_TYPE }">${row.TRAIN_TYPE } : ${row.TRAIN_SAY }</span><a href="#" class="delete_btn" onclick="fnSayDelete(${row.SEQ_NO}, '${row.TRAIN_TYPE }', ${row.PARENT_SEQ_NO }, this);"></a><input type="hidden" value="${row.SEQ_NO }"/></p>									
				<c:set var="ParentSeq" value="${row.PARENT_SEQ_NO }"></c:set>
				<c:set var="iCnt" value="${iCnt + 1}"></c:set>				
			</c:forEach>
			<c:if test='${iCnt > 0}'>
				</div>
			</c:if>
		</div>
		<div class="tran">
			<!-- Entity 검사 -->				
				<div id="divU" class="chatbot_lean_edit">
					<div style="padding-top:30px;">
						<span class="t_title_buu">U:</span>
						<input type="text" id="txtU" class="Clear_search" />
					</div>
				</div>
				<div id="divS" class="chatbot_lean_edit">
					<div>
						<span class="t_title_buu">S:</span>
						<input type="text" id="txtS" class="Clear_search" />
					</div>					
				</div>
				<div style="text-align:right; width:467px; padding:10px 0px 10px 0px;">
					<a href="#" onclick="fnSaveEdit();" title="Clear" class="Clear_search_btn" style="background:#0488da;">저장</a>
				</div>
			<!-- Entity 검사 -->
		</div>
		<hr />
		<div class="layer_box_btn" style="margin-top:19px;">
			<a href="#" onclick="fnSayTextFile();">Save</a>
			<a href="#" onclick="javascript:window.close();">Cancel</a>
		</div>
		<br/>
	</div>
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>