<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/tutor_header.jsp"%>
<script type="text/javascript">
		window.onload = function() {
					
		}
				
		function fnProejctDelete(prjseq, prjname, delflag, langseq) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/delettutorproject.do' />");		
				comSubmit.addParam("SEQ_NO", prjseq);
				comSubmit.addParam("PROJECT_NAME", prjname);
				comSubmit.addParam("DELETE_FLAG", delflag);
				comSubmit.addParam("LANG_SEQ_NO", langseq);
				comSubmit.submit();
			}
		}
		
		function fnProjectUndo(prjseq) {
			if (confirm('해당 항목을 복원 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/updatetutorprojectdelflag.do' />");		
				comSubmit.addParam("SEQ_NO", prjseq);				
				comSubmit.addParam("DELETE_FLAG", "N");
				comSubmit.submit();
			}
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/tutor_left.jspf"%>
	<div class="project_list">
		<div class="cont_title">
			<h2>Domains</h2>
			<a id="aCreateDomain" href="#" style="background:#2F5597 !important;">Create Domain</a>
		</div>
		<div class="user_utterance_contents" style="margin-top: 25px;">
			<dl class="class_name">
				<c:set var="UserSeq" value=""></c:set>
				<c:forEach items="${prjlist }" var="row">											
					<c:if test='${row.DELETE_FLAG == "N"}'>
						<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
							<h3 style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 10px 45px 10px 45px;">${row.USER_ID} / ${row.USER_NAME }</h3>							
							<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
						</c:if>
						<dd style="border-top: 1px solid #e1e1e1;">
							<a onclick="fnSelectProject(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.SERVER_PORT }', '${row.LANG_SEQ_NO }', '${row.USER_SEQ_NO }');"
								href="#" class="class_name_list">${row.PROJECT_NAME }</a> 
							<a onclick="fnProejctModify(${row.SEQ_NO});" href="#" class="modify_btn2"><span class="hide">수정</span></a> 
							<a onclick="fnProejctDelete(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.DELETE_FLAG}', '${row.LANG_SEQ_NO }');" href="#" class="delete_btn2"><span class="hide">삭제</span></a>				
						</dd>					
					</c:if>
				</c:forEach>
				<c:if test='${USER_TYPE eq "SA"}'>
					<c:if test="${fn:length(delprjlist) > 0}">
						<h3 style="font-size: 20px; font-weight: bold; line-height: 24px; margin: 30px 45px 10px 45px;">삭제된 도메인</h3>
					</c:if>					
					<c:forEach items="${delprjlist }" var="row">						
						<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
							<h3 style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 10px 45px 10px 45px;">${row.USER_ID} / ${row.USER_NAME }</h3>							
							<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
						</c:if>
						<dd style="border-top: 1px solid #e1e1e1;">
							<a onclick="fnSelectProject(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.SERVER_PORT }', '${row.LANG_SEQ_NO }', '${row.USER_SEQ_NO }');"
								href="#" class="class_name_list">${row.PROJECT_NAME }</a> 							
							<a onclick="fnProejctModify(${row.SEQ_NO});" href="#" class="modify_btn"><span class="hide">수정</span></a> 
							<a onclick="fnProejctDelete(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.DELETE_FLAG}', '${row.LANG_SEQ_NO }');" href="#" class="delete_btn"><span class="hide">삭제</span></a>
							<a onclick="fnProjectUndo(${row.SEQ_NO });" href="#" class="undo_btn"><span class="hide">복원</span></a>							
						</dd>
					</c:forEach>
				</c:if>
			</dl>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/tutor_footer.jspf"%>
</body>
</html>