<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>공지사항</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/tutor-common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">	
	$(function() {
		
	});		
</script>
<style>
	table tr td {border: 1px solid #e1e1e1; font-size:14px; color:#444444; padding:3px 5px; line-height:18px; table-layout:fixed; width:200px;}
	.file_view_tatle {font-size:24px; font-weight:bold; color:#2F5597; margin:25px 45px 15px 15px;}
</style>

</head>
<body>
	<div><h3 class="file_view_tatle">내용보기</h3></div>
    <div style="margin:10px 10px 10px 10px;">
    	${CON }
    </div>
</body>
</html>