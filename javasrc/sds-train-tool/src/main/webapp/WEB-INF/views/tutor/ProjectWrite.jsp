<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
	<%@ include file="/WEB-INF/include/tutor_header.jsp" %>
	<script type="text/javascript">	
		var bSaveChk = true;
		var reg = /[^a-zA-Z0-9_-]/; 
		
		window.onload = function() {
			if(!gfn_isNull('${PROJECT_NAME}')) {
				$("#hidPrjSeq").val('${SEQ_NO}');
				$("#txtProjectName").val('${PROJECT_NAME}');
				$("#txtDescription").val('${DESCRIPTION}');
			}		
			
			//checkbox
			$('[id^=langChk]').picker({
				customClass: 'list_check'
			});			
			//checkbox_end			
		    		   	
			
			if ('${SEQ_NO}' == '') {								
				$('[id^=langChk]').eq(0).picker('check');				
			}
			else {
				$('#langChk${LANG_SEQ_NO}').picker('check');
				$('[id^=langChk]').picker("disable");
			}					
		}
		
		function fnCheck(obj) {			
			$('[id^=langChk]').picker("uncheck");
			$(obj).picker('check');
		}
		
		function fnGetLangSeq() {
			var langSeq = '';
			
			$('[id^=langChk]').each(function() {
				if ($(this).attr('checked') == 'checked') {
					langSeq = $(this).val(); 
				}
			});
			
			if (langSeq == '') langSeq = '1';
						
			return langSeq;
		}
		
		function fnSave() {
			if (bSaveChk) {
				bSaveChk = false;
				
				var ProejctName = $('#txtProjectName').val().trim();
																
				if(ProejctName == '' || ProejctName == 'Domain name') {
					alert('도메인명을 입력하세요.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;	
				}
								
				if (reg.test(ProejctName)) {
					alert('도메인명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;
				}
				
				if ('${SEQ_NO}' == '') {
					if($('#txtDescription').val() == 'Describe your domain')
						$('#txtDescription').val('');
									
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/selecttutorprojectname.do' />");
					comAjax.setCallback("fnSaveCallBack");
					comAjax.addParam("PROJECT_NAME", ProejctName);
					comAjax.ajax();	
				}
				else							
					fnRealSave();
			}
			fnpreventDefault(event); 
		}
		
		function fnSaveCallBack(data) {
			if (data.list.length == 0) {
				fnRealSave();
			}
			else {
				alert('이미 존재하는 도메인명 입니다.');
				bSaveChk = true;
			}
		}
		
		function fnRealSave() {
			fnLoading();
			
			var comSubmit = new ComSubmit("frm");
			comSubmit.setUrl("<c:url value='/view/inserttutorproject.do' />");			
			
			comSubmit.addParam("LANG_SEQ_NO", fnGetLangSeq());
			comSubmit.addParam("PROJECT_USER_SEQ", '${USER_SEQ_NO}');
			comSubmit.submit();
		}				
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/tutor_left.jspf" %>
	<form id="frm" name="frm">					
		<!--Agent_name_box start-->
        <div class="Agent_name_box">
            <div class="s_word">
                <label for="search">
                    <input type="text" id="txtProjectName" name="PROJECT_NAME" value="Domain name" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
                    <a href="#" id="saveproject" onclick="fnSave();" title="저장"><span class="project_save">Save</span></a>
                </label>
            </div>
        </div>
        <!--Agent_name_box end-->
        <div class="description">
            <dl class="active-result">
                <dt>DESCRIPTION</dt>
                <dd>
                    <!--DESCRIPTION_box start-->
                    <div class="description_box">
                        <div class="s_word">
                            <label for="search">
                                <input id="txtDescription" name="DESCRIPTION" type="text" value="Describe your domain" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
                            </label>
                        </div>
                    </div>
                    <!--DESCRIPTION_box end-->
                </dd>
            </dl>
        </div>
        
        <div class="description">
            <dl class="active-result">
                <dt>LANGUAGE</dt>
                <dd>
                    <!--DESCRIPTION_box start-->
                    <div class="description_box box_he">
                        <div style="width:40%; padding-top:5px; margin:0px 0px 40px 5px;">
                        	<dl>
                        		<dd style="margin-left:0px;">
                        			<span class="check popupck_d1">                        	
										<input type="checkbox" onclick="fnCheck(this, 'L');" id="langChk1" name="langChk1" value="1" style="margin: 8px;"/><label for="langChk1" style="vertical-align: text-top; line-height:22px;">한국어</label>
									</span>
                        		</dd>
                        		<dd style="margin-left:0px;">
                        			<span class="check popupck_d1">                        	
										<input type="checkbox" onclick="fnCheck(this, 'L');" id="langChk2" name="langChk2" value="2" style="margin: 8px;"/><label for="langChk2" style="vertical-align: text-top; line-height:22px;">English</label>
									</span>
                        		</dd>
                        		<%-- <c:forEach items="${LANG_LIST }" var="row">
                        			<dd style="margin-left:0px;">
	                        			<span class="check popupck_d1">                        	
											<input type="checkbox" onclick="fnCheck(this);" id="langChk${row.SEQ_NO}" name="langChk${row.SEQ_NO}" value="${row.SEQ_NO}" style="margin: 8px;"/><label for="langChk${row.SEQ_NO}" style="vertical-align: text-top; line-height:22px;">${row.LANG_NAME}</label>
										</span>
	                        		</dd>
                        		</c:forEach>   --%>                      		
                        	</dl>															
                        </div>
                    </div>
                    <!--DESCRIPTION_box end-->
                </dd>
            </dl>
        </div>
        
        <input type="hidden" id="hidPrjSeq" name="SEQ_NO" value="" />
        <input type="hidden" id="hidOldName" name="OLD_NAME" value="${PROJECT_NAME}" />
        <input type="hidden" id="hidOldLang" name="OLD_LANG_SEQ" value="${LANG_SEQ_NO}" />               
	</form>
	<%@ include file="/WEB-INF/include/tutor_footer.jspf" %>
</body>
</html>