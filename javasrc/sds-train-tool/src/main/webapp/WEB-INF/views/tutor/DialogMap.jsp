<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
	<%@ include file="/WEB-INF/include/tutor_header.jsp" %>
	<script type="text/javascript">
		$(function () {
			//checkbox
			$('[id^=chkFile]').picker({
				customClass: 'list_check',
			});
			//checkbox_end
						
			$('#txtInput').keypress(function(e) {
				if(e.keyCode == 13) {
					if($(this).val().trim() != '') {
						$('#divResult').append('user: ' + $(this).val() + '<br />');
						fnInput('I');						
					}
					
					fnpreventDefault(e);
				}
					
			});
		});
		
		function fnUpLoadMapFile() {
			var fupload = document.getElementById('fupload');
			var filetype = '';
			if (fupload.value == '') {
				alert('파일을 선택해주세요.');
				return;
			}
			if (!(fupload.files[0].type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || fupload.files[0].type == 'application/vnd.ms-excel')) {
				alert('엑셀 파일만 가능합니다.');
				return;
			}	
			
			var comSubmit = new ComSubmit("UploadForm");
			comSubmit.setUrl("<c:url value='/view/insertdialogmapfile.do' />");						
			comSubmit.addParam("LANG_SEQ_NO", '${LANG_SEQ_NO}');
			comSubmit.addParam("FILE_NAME", fupload.files[0].name);			
			comSubmit.submit();
		}
		
		function fnCheck(obj) {			
			$('[id^=chkFile]').picker("uncheck");			
			$(obj).picker('check');											
		}
		
		function fnViewFile(seqno, filename) {						
			
			fnWinPop("<c:url value='/view/opendialogfile.do' />" + '?FILE_NAME=' + encodeURIComponent(filename), "NoticePop", 1200, 1000, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnDeleteFile(seqno, filename) {
			if (confirm('해당 파일을 삭제 하시겠습니까?')) {							
				var comSubmit = new ComSubmit();
							
				comSubmit.setUrl("<c:url value='/view/deletedialogmapfile.do' />");
				comSubmit.addParam("SEQ_NO", seqno);
				comSubmit.addParam("LANG_SEQ_NO", '${LANG_SEQ_NO}');				
				comSubmit.addParam("FILE_NAME", filename);
				comSubmit.submit();
			}
			fnpreventDefault(event);
		}		
		
		function fnTrain(type) {
			var arrfile;
			$('[id^=chkFile]').each(function() {
				if ($(this).attr('checked') == 'checked') {
					arrfile = $(this).val().split('|'); 	
					return;
				}					
			});	
			
			if (arrfile == undefined) {
				alert('파일을 선택해주세요.');
				return;
			}
			fnLoading();
			$('#divRun').hide();
			$('#divTutor').show();
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogtrain.do' />");			
			comAjax.setCallback("fnTrainCallBack");			
			comAjax.addParam("SEQ_NO", arrfile[0]);
			comAjax.addParam("FILE_NAME", arrfile[1]);
			comAjax.addParam("TYPE", type);
			comAjax.setAsync(true);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnTrainCallBack(data) {
			fnLoading();
			if (data.STATUS == 'S') {
				$('#divTrain').html(data.MSG);
			}
			else
				alert('Train 실행 중 오류가 발생하였습니다.');
		}		
		
		function fnRun() {						
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/gettutordialogmission.do' />");			
			comAjax.setCallback("fnRunCallBack");
			comAjax.setAsync(false);
			comAjax.ajax();
			
			$('#divRun').show();
			$('#divTutor').hide();
			fnpreventDefault(event);
		}
		
		function fnRunCallBack(data) {
			if (data.STATUS == 'S') {
				if (data.FILE_EXISTS == 'Y') {
					$('#ddlMission').empty();
					var arrMission = data.STR.split('§');
					var arrMissionSub;
					var str = '';
					
					for (var i = 0; i < arrMission.length; i++) {
						arrMissionSub = arrMission[i].split('|');
						str += '<option value="' + arrMissionSub[0].split("\t")[1] + '">' + arrMissionSub[1].split("\t")[2] + '</option>';
					}
					$('#ddlMission').append(str).trigger("chosen:updated");					
				}
				else 
					alert('먼저 Train 버튼을 눌러서 학습을 진행해주세요.');
			}
			else
				alert('Run 실행 중 오류가 발생하였습니다.');
		}
		
		function fnRunStart() {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/tutorrunstart.do' />");			
			comAjax.setCallback("fnRunStartCallBack");			
			comAjax.addParam("ID", $('#ddlMission').val());
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(false);
			comAjax.ajax();
			
			fnpreventDefault(event);
		}
		
		function fnRunStartCallBack(data) {
			if (data.STATUS == 'S') {
				$('#txtInput').removeAttr('disabled');				
				alert('엔진이 구동 되었습니다.');	
				fnInput('S');
			}
			else
				alert('엔진이 구동 중 오류가 발생하였습니다.');
		}
		
		function fnInput(type) {			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/tutorruninput.do' />");
			comAjax.setCallback("fnInputCallBack");
			if (type != 'S') { 
				comAjax.addParam("MSG", $('#txtInput').val().replace(/\s/g, '|@'));
				$('#txtInput').val('');
				$('#txtInput').attr('disabled', 'disabled');
			}
			
			comAjax.addParam("TYPE", type);
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(false);
			comAjax.ajax();
			
			fnpreventDefault(event);
		}
		
		function fnInputCallBack(data) {
			if (data.STATUS == 'OK') {
				if (data.LOG != '') {
					enginedata = JSON.parse(data.LOG);
					$('#divResult').append('<span>system: ' + enginedata.SYS_UTTER + '</span><br />').scrollTop($("#divResult")[0].scrollHeight);				
					$('#txtInput').removeAttr('disabled').focus();
					$('#divXml').append('<span>' + enginedata.LOG + '</span>')				
				}
			}			
			else
				alert('엔진을 구동시켜주세요.');			
		}
		
		function fnStop() {					
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/tutordialogstop.do' />");
			comAjax.setCallback("fnStopCallBack");						
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');	
			comAjax.setAsync(false);
			comAjax.ajax();
						
			fnpreventDefault(event);
		}
		
		function fnStopCallBack(data) {			
			if (data.STATUS == 'S') {										
				$('#divResult').append('Tutor End. Thank.<br />');
				$('#txtInput').attr('disabled', 'disabled');
			}
			else {
				alert('엔진 중지 중 문제가 발생하였습니다.');
			}
		}
		
		function fnClear() {
			$('#divResult').html('');
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/tutor_left.jspf" %>	
							
	<div class="user_utterance">
		<div class="cont_title">
			<h2><%= session.getAttribute("TutorProjectName")%></h2>
		</div>
		<div class="user_utterance_contents">
			
			<!-- step01 -->
			<div class="user_utterance_step02">
				<div class="sortable02">
					<h3 class="slot_icon">Dialog Map<a href="#" onclick="fnShowUpload();" >Load</a></h3>
					<form id="UploadForm" name="UploadForm" enctype="multipart/form-data" method="post">
						<div class="layer" id="layer">
							<div class="bg"></div>
							<div id="divUpload" class="layer_box w400" style="z-index: 1; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display: none;">
								<p style="padding-bottom: 10px;"><input type="file" id="fupload" name="fupload"/></p>
								<div class="layer_box_btn">
									<a onclick="fnUpLoadMapFile();" href="#">Upload</a>
									<a onclick="fnShowUploadClose();" href="#">Cancel</a>			
								</div>
							</div>		
						</div>
					</form>					
					<ul class="ordering_food_li">
						<c:forEach items="${filelist}" var="frow">
					<c:choose>
						<c:when test="${frow.TUTOR_FLAG == 'Y'}">
							<li class="ui-state-default of_on">
						</c:when>
						<c:otherwise>
							<li class="ui-state-default of_off">
						</c:otherwise>			
					</c:choose>
								<span class="check sttable02_title_01 ordering_food_ck">
									<input type="checkbox" id="chkFile${frow.SEQ_NO }" name="chkFile${frow.SEQ_NO }" onclick="fnCheck(this);" value="${frow.SEQ_NO }|${frow.FILE_NAME }" /><label for="chkFile${frow.SEQ_NO }" class="hide">메뉴명 선택/해제</label>
								</span>
								<div class="ordering_food_list">
									<span class="ordering_food_list01">${frow.FILE_NAME }</span>
									<span class="ordering_food_list02">${frow.LOAD_DATE }</span>
								</div>
								<a href="#" onclick="fnViewFile(${frow.SEQ_NO }, '${frow.FILE_NAME }');" class="of_view_btn"><span class="hide">보기</span></a>
								<a href="#" onclick="fnDeleteFile(${frow.SEQ_NO }, '${frow.FILE_NAME }');" class="delete_btn"><span class="hide">삭제</span></a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="layer_box_btn" style="margin-top:19px; text-align:right; margin:19px 45px 0px 55px;">
				<a href="#" onclick="fnTrain('C');">Train_convert</a>
				<a href="#" onclick="fnTrain('L');">Train_learn_slu</a>
				<a href="#" onclick="fnRun();">Run</a>
			</div>
			<!-- step01 -->
			
			<!-- step02 -->
			<div id="divTutor" class="ordering_food_02" style="background: #e1e1e1; overflow-y:hidden;">
				<div id="divTrain" class="chatbot_popup of_box_txt">
					
				</div>
			</div>
			<div id="divRun" class="ordering_food_02" style="display:none;">
				<div class="layer_box_btn" style="text-align:right; margin:0px 0px 0px 0px; text-align:left !important;">
					<a onclick="fnRunStart();" href="#">Start</a>
					<a onclick="fnStop();" href="#">Stop</a>
					<a onclick="fnClear();" href="#">Clear</a>
				</div>	
				<div class="tran" style="height:25px; margin:10px 0px;">
					<span class="t_title_buu">Input</span>
					<div class="s_option" style="width:80%; float:left; height:22px;">
					   <label for="user_group">
						  <select class="f_right u_group" id="ddlMission" name="ddlMission" data-placeholder="검색조건을 선택하세요">							 
						  </select>
					   </label>
					</div>
				</div>					
				<div id="divResult" class="chatbot_popup">
					
				</div>
				<div class="tran">
						<div>
							<div style="text-align:left; padding:10px 0px 10px 0px;">
								<span class="t_title_buu">Input</span>
								<input type="text" id="txtInput" class="Clear_search" style="height:23px; line-height:23px; display:inline-block; padding:0px !important;" disabled='disabled' />
							</div>
						</div>
				</div>
				<div id="divXml" class="xml_ordering_food">
				
				</div>
			</div>
			<!-- step02 -->
		</div>
	</div>
	<%@ include file="/WEB-INF/include/tutor_footer.jspf" %>
</body>
</html>