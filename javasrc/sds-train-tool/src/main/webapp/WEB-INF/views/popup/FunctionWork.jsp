<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>함수생성</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/chosen.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-latest.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/chosen.jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>


<script type="text/javascript">
	var pop;
	$(function() {
		if('${param.Type}' == 'G') {
			$('#pTitle').text('${param.Title}'.replace('ã¡', 'ㅡ') + ' 태스크 이동 조건');
		}
		else if('${param.Type}' == 'CC') {
			$('#pTitle').text('태스크 완료 조건');
		}
		else if('${param.Type}' == 'SC') {
			$('#pTitle').text('Slot Condition 조건');
		}
		else if('${param.Type}' == 'SA') {
			$('#pTitle').text('Slot Action 조건');
		}
		
		$('#divSimbol li').click(function(e) {
			var cursorpos = $("#txtResult").getCursorPosition();
			$txtResult = $('#txtResult');
			
			if (cursorpos == $("#txtResult").val().length) {
				if($(this).text() == 'space')
					$txtResult.val($txtResult.val() + ' ');
				else if($(this).text() == 'and' || $(this).text() == 'or')
					$txtResult.val($txtResult.val() + ' ' + $(this).text() + ' ');
				else
					$txtResult.val($txtResult.val() + $(this).text() + ' ');
				
				$txtResult.focus();
			}
			else {
				var beforeStr = $txtResult.val().substring(0, cursorpos);
				var afterStr = $txtResult.val().substring(cursorpos);
				
				if($(this).text() == 'space')
					$txtResult.val(beforeStr + ' ' + afterStr);
				else if($(this).text() == 'and' || $(this).text() == 'or')
					$txtResult.val(beforeStr + ' ' + $(this).text() + ' ' + afterStr);
				else if($(this).text() == '(')
					$txtResult.val(beforeStr + ' ' + $(this).text() + afterStr);
				else if($(this).text() == ')')
					$txtResult.val(beforeStr + $(this).text() + ' ' + afterStr);
				
				$txtResult.focus();
				$txtResult.get(0).setSelectionRange(cursorpos, cursorpos);				
			}
			
			fnpreventDefault(e);	
		});
		
		$('[name="txtSlot"]').click(function(e) {
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(this).attr('id') + "&Type=UI", "AgentSearchpop", 430, 550, 0, 0);
			fnpreventDefault(e);
		});
		
		$('[name="txtSlotNoClass"]').click(function(e) {
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(this).attr('id') + "&Type=FNC", "AgentSearchpop", 430, 550, 0, 0);
			fnpreventDefault(e);
		});
		
		$('[name="txtTask"]').click(function(e) {
			pop = fnWinPop("<c:url value='/view/opentasksearch.do' />" + "?obj=" + $(this).attr('id') + "&Type=FW", "TaskPop", 430, 550, 0, 0);
			fnpreventDefault(e);
		});
		
		$('#btnClear').click(function(e) {
			$('#txtResult').val('');
			fnpreventDefault(e);
		});	
		
		if ('${param.Type}' == 'G') {
			$('#divIntentbtn').on('click', 'li', function(e) {				
				var $this = $(this);
				$('#IntentType').val($this.text());
				
				$this.parent().find('li').removeClass('on');
				$this.addClass('on');
				
				fnIntentClear();
							
				fnpreventDefault(e);
			});	
		}
		else 
			$('#divIntentbtn').hide();
				
		$('#divIntent, #divSlot, #divTask').on('click', '[name=divIntentBit] button', function(e) {
			var $this = $(this);
			$this.parent().find('input:hidden').val($this.text());
			
			$this.parent().find('button').removeClass('on');
			$this.addClass('on');
						
			fnpreventDefault(e);
		});
		
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			$('#txtResult').val(opener.$('#${obj}').text());
		else
			$('#txtResult').val(opener.$('#${obj}').val());
		
		if ('${param.flag}' == 'R')
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '2턴 전 시스템 DA Type'));
		else if ('${param.flag}' == 'P' || '${param.flag}' == 'T') {
			$('#hidIntentSeq').val('2');
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
			$('#divIntent').append(fnGetIntentAddHtml());
			$('#divIntent').find('[name=Intent_OBJECT]').hide();			
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '0턴 전 시스템 DA Type'));
		}
		else if ('${param.flag}' == 'A') {
			$('#hidIntentSeq').val('2');
			$('#divIntent').append(fnGetIntentAddHtml());
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
			$('#divIntent').find('[name=Intent_OBJECT]').hide();			
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '0턴 전 사용자 DA Type'));
		}
		else
			$('#divIntent').append(fnGetIntentAddHtml());
		
		$('#divIntent [name=Intent_OBJECT]').find('.left').eq(0).hide();
		
		//셀렉트 박스 레이아웃
		//$('.u_group').chosen({ disable_search_threshold: 10, allow_single_deselect: true });
		
		if ($('#txtResult').val() == 'false' || $('#txtResult').val() == 'true') {
			$('#txtResult').val('');
		}
		
		$.fn.getCursorPosition = function() {
	        var input = this.get(0);
	        if (!input) return; // No (input) element found
	        if ('selectionStart' in input) {
	            // Standard-compliant browsers
	            return input.selectionStart;
	        } else if (document.selection) {
	            // IE
	            input.focus();
	            var sel = document.selection.createRange();
	            var selLen = document.selection.createRange().text.length;
	            sel.moveStart('character', -input.value.length);
	            return sel.text.length - selLen;
	        }
	    }
	});	
	
	$(window).bind("beforeunload", function (e){
		if (!gfn_isNull(pop)) {
			pop.close();
		}
	});
	
	function fnGetIntentAddHtml() {		
		return $('#divIntentAdd').html().replace(/#/g, $('#divIntent').find('[name=txtIntent]').length);
	}
	
	function fnIntentClear() {
		$('#divIntent').empty();
		var str = '';
		if ('${param.flag}' == 'R')
			str = '2턴 전 시스템 DA Type';
		else if ('${param.flag}' == 'S') {
			$('#hidIntentSeq').val('2');
			$('#divIntent').append(fnGetIntentAddHtml());
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
			$('#divIntent').find('[name=Intent_OBJECT]').hide();			
			str = '0턴 전 사용자 DA Type';
		}
		else if ('${param.flag}' == 'P' || '${param.flag}' == 'T') {
			$('#hidIntentSeq').val('2');
			$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
			$('#divIntent').append(fnGetIntentAddHtml());
			$('#divIntent').find('[name=Intent_OBJECT]').hide();			
			str = '0턴 전 시스템 DA Type';
		}
		else {
			if ($('#IntentType').val() == '사용자') {
				$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 사용자 DA Type'));
				$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
				str = '0턴 전 사용자 DA Type';
			}
			else {
				$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 시스템 DA Type'));
				$('#divIntent').append(fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, '모든 턴 사용자 DA Type'));
				str = '0턴 전 시스템 DA Type';
			}
			$('#divIntent').find('[name=Intent_OBJECT]').hide();	
			$('#hidIntentSeq').val('2');
		}
		
		var html = fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, str); 						
		$('#divIntent').append(html);
		$('#divIntent [name=Intent_OBJECT]').find('.left').eq(0).hide();
	}
	
	function fnIntentPrev(obj) {
		var idx = $(obj).parent().parent().parent().index();
		
		if (idx != 0) {
			var $arrObj = $('#divIntent').find('[name=Intent_OBJECT]');							
			$arrObj.hide();
			$arrObj.eq(idx - 1).show();
			$('#hidIntentSeq').val(idx - 1);
		}
		fnpreventDefault(event);
	}
	
	function fnIntentNext(obj) {
		var idx = $(obj).parent().parent().parent().index();
		var $arrObj = $('#divIntent').find('[name=Intent_OBJECT]');
		var cnt = $arrObj.length - 1;
		
		$arrObj.hide();		
		
		if (idx == cnt) {
			var str = '';
			
			if ('${param.flag}' == 'R')
				str = ($arrObj.length + 2).toString() + '턴 전 ';
			else {
				if (idx == 0) 
					str = '모든 턴  ';
				else if (idx == 1) 
					str = '0턴 전  ';				
				else 
					str = ($arrObj.length - 2).toString() + '턴 전 ';												
			}			
			if ($('#IntentType').val() == '사용자') {
				if ($arrObj.length % 2 == 0)
					str += '사용자';					
				else 
					str += '시스템';
			}
			else {
				if ($arrObj.length % 2 == 0)
					str += '시스템';					
				else 
					str += '사용자';
			}
			str += ' DA Type';
			
			var html = fnGetIntentAddHtml().replace(/모든 턴 사용자 DA Type/, str); 
			$('#divIntent').append(html);
			$('#hidIntentSeq').val($arrObj.length);
		}
		else {
			$arrObj.eq(idx + 1).show();
			$('#hidIntentSeq').val(idx + 1);
		}
		
		fnpreventDefault(event);
	}
	
	function fnSlotPrevNext(type) {
		var title = $('#spSlotTitle').text();
		if (type == 0) {
			if (title == '시스템 응답 가능 내용') {
				title = '사용자 발화 내용';
				$('#aPrev2').hide();
			}
			/* else if (title == '둘 다에서') {
				title = 'Instance에서';				
			} */
			$('#aNext2').show();
		}
		else {
			if (title == '사용자 발화 내용') {
				title = '시스템 응답 가능 내용';
				$('#aNext2').hide();
			}
			/* else if (title == 'Instance에서') {
				title = '둘 다에서';
				$('#aNext2').hide();
			} */
			$('#aPrev2').show();
		}
		$('#spSlotTitle').text(title);
		fnpreventDefault(event);
	}	
	
	function fnResultAdd(type) {
		var str = '';
		var str2 = '';
		if (type == 1) {
			var idx = Number($('#hidIntentSeq').val());		
			var $obj = $('#divIntent').find('[name=Intent_OBJECT]').eq(idx);
			var title = $obj.find('[name=divTitle]').text();
			var value = $obj.find('[name=txtIntent]').val();
			var bit = $obj.find('[name=IntentBit]').val();
			
			if (title.indexOf('사용자') > -1)
				title = 'user';
			else
				title = 'system';
			
			if ('${param.flag}' != 'R') {				
				if (idx < 2) 
					str = 'Intent("' + title + '", "' + value + '", "*", "' + bit + '") ';				
				else													
					str = 'Intent("' + title + '", "' + value + '", "' + (idx + -2).toString() + '", "' + bit + '") ';
			}
			else
				str = 'Intent("' + title + '", "' + value + '", "' + (idx + 2).toString() + '", "' + bit + '") ';
						
		}
		else if (type == 2) {
			var $txtSlotExists = $('#txtSlotExists');
			var $txtSlotCountName = $('#txtSlotCountName');
			var $txtSlotCountValue = $('#txtSlotCountValue');
			var $txtSlotValueName = $('#txtSlotValueName');
			var $txtSlotValueValue = $('#txtSlotValueValue');
			var $txtSlotInstance = $('#txtSlotInstance');
			var bChk = false;
			
			if (($txtSlotCountName.val().trim() == '' && $txtSlotCountValue.val().trim() != '') || ($txtSlotCountName.val().trim() != '' && $txtSlotCountValue.val().trim() == '')) {
				alert('[슬롯 개수 확인] 칸을 모두 채워 주세요.');
				return;
			}
			
			if (($txtSlotValueName.val().trim() == '' && $txtSlotValueValue.val().trim() != '') || ($txtSlotValueName.val().trim() != '' && $txtSlotValueValue.val().trim() == '')) {
				alert('[슬롯 값 확인] 칸을 모두 채워 주세요.');
				return;
			}
			
			var title = $('#spSlotTitle').text();
			var subtitle = '';
			if (title.indexOf('사용자') > -1) {
				subtitle = 'user_history';				
			}			
			else {
				subtitle = 'system_response';
			}
			
			if ($txtSlotExists.val().trim() != '') {
				str += 'Entity("' + $txtSlotExists.val() + '", "' + subtitle + '", "' + $('#SlotExists').val() + '")';
				bChk = true;
			}
			
			if ($txtSlotCountName.val().trim() != '') {
				if (bChk) {
					str += ' or ';	
				}
				str += 'Count("' + $txtSlotCountName.val() + '", "' + subtitle + '") ' + $('#SlotCount').val() + ' ' + $txtSlotCountValue.val();
				bChk = true;
			}
			
			if ($txtSlotValueName.val().trim() != '') {
				if (bChk) {
					str += ' or ';	
				}
				if ($('#hidtxtSlotValueName').val() == 'Sys.number')				
					str += 'Value("' + $txtSlotValueName.val() + '", "' + subtitle + '") ' + $('#SlotValue').val() + ' ' + $txtSlotValueValue.val();
				else
					str += 'Value("' + $txtSlotValueName.val() + '", "' + subtitle + '") ' + $('#SlotValue').val() + ' "' + $txtSlotValueValue.val() + '"';
			}
			
			if ($txtSlotInstance.val().trim() != '') {
				if (bChk) {
					str += ' or ';	
				}
				str += 'Instance("' + $txtSlotInstance.val() + '", "' + $('#SlotInstance').val() + '")';
			}
			
			if (str.indexOf(' or ') > -1) {
				str = '(' + str + ')';
			}
			
			/* else if (title.indexOf('Instance') > -1) {
				if ($txtSlotExists.val().trim() != '') {
					str += 'Entity(' + $txtSlotExists.val() + ', instance, ' + $('#SlotExists').val() + ')';
					bChk = true;
				}
				
				if ($txtSlotCountName.val().trim() != '') {
					if (bChk) {
						str += ' or ';	
					}
					str += 'Count(' + $txtSlotCountName.val() + ', instance) ' + $('#SlotCount').val() + ' ' + $txtSlotCountValue.val();
					bChk = true;
				}
			} */
			fnEntiryClear(type);
		}
		else {
			$txtTaskComp = $('#txtTaskComp');
			$txtTaskHisotryTask = $('#txtTaskHisotryTask');
			var bChk = false;
						
			if ($txtTaskComp.val().trim() != '') {
				bChk = true;
				str = 'FinishedTask("' + $txtTaskComp.val() + '", "' + $('#TaskComp').val() + '")';
			}
			//PreviousTask(“1”, “태스크이름”, “true or false”)

			if ($txtTaskHisotryTask.val().trim() != '') {
				if (bChk)
				 str += ' or ';				
				str += 'PreviousTask("' + $('#hidTaskHistory').val() + '", "' + $txtTaskHisotryTask.val() + '", "' + $('#TaskHisotryTask').val()  + '")';
			}
			
			if (str.indexOf(' or ') > -1) {
				str = '(' + str + ')';
			}
			fnEntiryClear(type);
		}
		
		if (str == '') {
			alert('입력값을 확인해주세요.');
			return;
		}
		
		$('#txtResult').val($('#txtResult').val() + str);
		fnCursorEnd($('#txtResult').get(0));
		fnpreventDefault(event);
	}
	
	function fnEntiryClear(type) {
		if (type == 1) {
			fnIntentClear();
		}
		else if (type == 2) {
			$('#divSlot input').val('');
			$('#divSlot [name=divIntentBit] button').removeClass('on');
			$('#divSlot [name=divIntentBit] button').eq(0).addClass('on');
			$('#divSlot [name=divIntentBit] button').eq(2).addClass('on');
			$('#SlotExists, #SlotInstance').val('true');
		}
		else {
			$('#txtTaskComp, #txtTaskHisotryTask').val('');
			$('#divTask [name=divIntentBit] button').removeClass('on');
			$('#divTask [name=divIntentBit] button').eq(0).addClass('on');
			$('#divTask [name=divIntentBit] button').eq(2).addClass('on');
			$('#TaskComp').val('true');
			$('#spTaskHisIndex').text('바로');
			$('#hidTaskHistory').val('1');
			$('#TaskHistory').val('==');
			$('#imgTaskPrev').hide();
		}
		fnpreventDefault(event);
	}
	
	function fnSetParent() {		
		if($('#txtResult').val().length > 5000) { 
			alert('5000자 이상 저장할 수 없습니다.');
			return false;
		}
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			opener.$('#${obj}').text($('#txtResult').val().trim());
		else
			opener.$('#${obj}').val($('#txtResult').val().trim());
		opener.bWorkChk = true;
		
		if ('${param.Type}' == 'G') {
			opener.bConWork = true;
			opener.$('#${obj}').focus();
		}
		
		fnWinClose();
	}
	
	function fnOpenPop(obj) {
		var type = 'system';
		
		if ($(obj).parent().parent().find('[name=divTitle]').text().indexOf('사용자') > -1) {
			type = 'user';
		}
		pop = fnWinPop("<c:url value='/view/opendatypesearch.do' />" + "?obj=" + $(obj).attr('id') + '&type=' + type, "DASearchpop", 430, 550, 0, 0);
	}
	
	function fnChange(type, obj) {
		$(obj).parent().find('li').removeClass('on');
		$(obj).addClass('on');		
		
		if (type == 'I') {
			$('#divIntentTab').show();	
			$('#divSlot, #divTask').hide();
		}
		else if (type == 'E') {
			$('#divIntentTab, #divTask').hide();
			$('#divSlot').show();
		}
		else {
			$('#divIntentTab, #divSlot').hide();
			$('#divTask').show();
		}
	}
	
	function fnTaskHisMove(type) {
		var $hidTaskHistory = $('#hidTaskHistory');
		if (type == 1) {
			if ($hidTaskHistory.val() == '1') {			
				$hidTaskHistory.val('0');
				$('#spTaskHisIndex').text('현재');
				$('#imgTaskPrev, #spTaskHisSub').hide();		
			}
			else if ($hidTaskHistory.val() == '2') {			
				$hidTaskHistory.val('1');
				$('#spTaskHisIndex').text('바로');
			}
			else {				
				$hidTaskHistory.val(Number($hidTaskHistory.val()) - 1);
				$('#spTaskHisIndex').text($hidTaskHistory.val() + '번째');
			}
		}
		else {
			$hidTaskHistory.val(Number($hidTaskHistory.val()) + 1);
			if ($hidTaskHistory.val() == '1') {
				$('#spTaskHisIndex').text('바로');
				$('#spTaskHisSub').show();
			}
			else
				$('#spTaskHisIndex').text($hidTaskHistory.val() + '번째');			
			
			$('#imgTaskPrev').show();
		}
		
	}
</script>

</head>
<body>	
	<div id="popWrap" class="popWrap">
		<div style="margin:20px; border:1px solid #ddd; display: inline-block;">
			<div class="middleW createTask fnSG" style="padding:0px;">
				<div class="searchBox" style="border:none;">
					<div id="pTitle" class="title">시스템 발화 조건</div>
					<!-- <div class="box"><input type="text" id="txtResult"><button type="button" id="btnClear">Clear</button></div> -->
					<div class="box box_textarea"><textarea id="txtResult"></textarea><button type="button" id="btnClear">Clear</button></div>
					<div class="box box2">
						<ul id="divSimbol">
							<!-- <li><div>true</div></li>
							<li><div>false</div></li> -->
							<li><div>and</div></li>
							<li><div>or</div></li>
							<li><div>(</div></li>
							<li><div>)</div></li>
							<li><div>space</div></li>
						</ul>
					</div>
				</div>
				
				<input type="hidden" id="hidIntentSeq" class="funct_txt_obx_style" value="0" />
				
				<ul class="tab_st">
					<li class="on" onclick="fnChange('I', this);">Intent 검사</li>
					<li onclick="fnChange('E', this);">Entity 검사</li>
					<li onclick="fnChange('T', this);">Task 검사</li>
				</ul>
				<div id="divIntentTab" class="searchBox" style="width:calc(100% - 40px); margin: 0px 20px 20px;">
					<div class="title">
						<div class="title">Intent 검사</div>
						<div class="btnBox">
							<div class="btn downloadIn" onclick="fnResultAdd(1);">Add</div>
							<div class="btn uploadIn" onclick="fnEntiryClear(1);">Clear</div>
						</div>
					</div>
					<div id="divIntentbtn" class="box box2">
						<ul>
							<c:choose>
								<c:when test='${param.flag != "R" && param.flag != "P" && param.flag != "T"}'>
									<li class="on"><div>사용자</div></li>
									<li><div>시스템</div></li>
									<input type="hidden" id="IntentType" value="사용자" />										
								</c:when>
								<c:otherwise>
									<li><div>사용자</div></li>
									<li class="on"><div>시스템</div></li>
									<input type="hidden" id="IntentType" value="시스템" />
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
					<div id="divIntent">
						<!-- <div class="box box3">
							<div class="title user">
								<div class="title">모든 턴 사용자 DA Type</div>
								<div class="btnBox">
									<div class="left"></div>
									<div class="right"></div>
								</div>
							</div>
							<div class="inputBox"><input type="text"><button type="submit" class="on">true</button><button type="submit">false</button></div>
						</div> -->
					</div>
				</div>
				<div id="divSlot" class="searchBox" style="width:calc(100% - 40px); margin: 0px 20px 20px; display:none;">
					<div class="title">
						<div class="title">Entity 검사</div>
						<div class="btnBox">
							<div onclick="fnResultAdd(2);" class="btn downloadIn">Add</div>
							<div onclick="fnEntiryClear(2);" class="btn uploadIn">Clear</div>
						</div>
					</div>
					<div class="box box3">
						<div class="title user">
							<div class="title" id="spSlotTitle">사용자 발화 내용</div>
							<div class="btnBox">
								<div id="aPrev2" onclick="fnSlotPrevNext(0);" class="left" style="display:none;"></div>
								<div id="aNext2" onclick="fnSlotPrevNext(1);" class="right"></div>
							</div>
						</div>
						<div class="title2">슬롯 값 존재 확인</div>
						<div name="divIntentBit" class="inputBox">
							<input type="text" id="txtSlotExists" name="txtSlotNoClass"  />
							<button type="submit" class="on">true</button>
							<button type="submit">false</button>
							<input type="hidden" id="SlotExists" value="true" />
						</div>
						<div class="title2">슬롯 값 개수 확인</div>
						<div class="inputBox inputBox2">
							<input type="text" id="txtSlotCountName" name="txtSlotNoClass" />
							<form>
								<select id="SlotCount" name="SlotCount" data-placeholder="조건을 선택하세요.">
									<option value="==">==</option>
									<option value="~=">~=</option>
									<option value="&lt;">&lt;</option>
									<option value="&gt;">&gt;</option>
									<option value="&lt;=">&lt;=</option>
									<option value="&gt;=">&gt;=</option>
								</select>
							</form>
							<input type="text" id="txtSlotCountValue" />
						</div>
						<div class="title2">슬롯 값 정보 확인</div>
						<div class="inputBox inputBox2">
							<input type="text" id="txtSlotValueName" name="txtSlotNoClass" />
							<input type="hidden" id="hidtxtSlotValueName" />
							<form>
								<select id="SlotValue" name="SlotValue" data-placeholder="조건을 선택하세요.">
									<option value="==">==</option>
									<option value="~=">~=</option>
									<option value="&lt;">&lt;</option>
									<option value="&gt;">&gt;</option>
									<option value="&lt;=">&lt;=</option>
									<option value="&gt;=">&gt;=</option>
								</select>
							</form>
							<input type="text" id="txtSlotValueValue" />						
						</div>					
					</div>
					<div class="box box3">					
						<div class="title user">
							<div class="title">사용자 발화 슬롯 값 Instance 존재 확인</div>
							<div class="btnBox">						
							</div>
						</div>
						<div name="divIntentBit" class="inputBox">
							<input type="text" id="txtSlotInstance" name="txtSlot"  />
							<button type="submit" class="on">true</button>
							<button type="submit">false</button>
							<input type="hidden" id="SlotInstance" value="true" />
						</div>
					</div>
				</div>
				
				<div id="divTask" class="searchBox" style="width:calc(100% - 40px); margin: 0px 20px 20px; display:none;">
					<div class="title">
						<div class="title">Task 검사</div>
						<div class="btnBox">
							<div onclick="fnResultAdd(3);" class="btn downloadIn">Add</div>
							<div onclick="fnEntiryClear(3);" class="btn uploadIn">Clear</div>
						</div>
					</div>
					<div class="box box3">
						<div class="title2">완료 Task 여부</div>
						<div name="divIntentBit" class="inputBox">
							<input type="text" id="txtTaskComp" name="txtTask"  />
							<button type="submit" class="on">true</button>
							<button type="submit">false</button>
							<input type="hidden" id="TaskComp" value="true" />
						</div>
					</div>
					<%-- <div class="box box3">
						<div class="title2" style="float:none;">Task 이력 확인</div>
						<table>								
							<colgroup>
								<col style="width:10%;" />
								<col />
								<col style="width:10%;" />
							</colgroup>
							<tbody>
								<tr>
									<td><img id="imgTaskPrev" src="../images/icon_open1.png" style="display:none;" onclick="fnTaskHisMove(1);" /></td>
									<td><span id="spTaskHisIndex">바로</span> 이전 태스크</td>
									<td><img id="imgTaskNext" src="../images/icon_close1.png" onclick="fnTaskHisMove(2);"/></td>
								</tr>
							</tbody>
						</table>
						<input type="hidden" id="hidTaskHistory" name="txtTask" value="1" />
						<div name="divIntentBit" class="inputBox">							
							<input type="text" id="txtTaskHisotryTask" name="txtTask" style="width:calc(60% - 140px);" />
							<button type="submit" class="on">true</button>
							<button type="submit">false</button>
							<input type="hidden" id="TaskHisotryTask" value="true" />
						</div>
					</div> --%>
					<div class="box box3">
						<div class="title2">Task 이력 확인</div>
						<div class="inputBox inputBox2">
							<table>								
								<colgroup>
									<col style="width:10%;" />
									<col />
									<col style="width:10%;" />
								</colgroup>
								<tbody>
									<tr>
										<td><img id="imgTaskPrev" src="../images/icon_open1.png" onclick="fnTaskHisMove(1);" /></td>
										<td><span id="spTaskHisIndex">바로</span><span id="spTaskHisSub"> 이전</span> 태스크</td>
										<td><img id="imgTaskNext" src="../images/icon_close1.png" onclick="fnTaskHisMove(2);"/></td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" id="hidTaskHistory" name="txtTask" value="1" />							
							<div name="divIntentBit" style="border:none; width:60%; height:28px; margin:0; padding:0;">
								<input type="text" id="txtTaskHisotryTask" name="txtTask" style="width:calc(100% - 140px); border:1px solid #ddd; border-radius:4px; " />
								<button type="submit" class="on" style="margin-left:9px;">true</button>
								<button type="submit" style="margin-left:9px;">false</button>
								<input type="hidden" id="TaskHisotryTask" value="true" />
							</div>
						</div>						
					</div>
				</div>				
			</div>
		</div>
		
		<div class="btnBox btn_style_01">
			<div class="ok" onclick="fnSetParent();">Ok</div>
			<div class="cancle" onclick="fnWinClose();" style="margin-right:0px;">Cancel</div>
		</div>
	</div>
	
	<div id="divIntentAdd" style="display:none;">
		<div name="Intent_OBJECT" class="box box3 box_style">
			<div class="title user">
				<div class="title" name="divTitle">모든 턴 사용자 DA Type</div>
				<div class="btnBox">
					<div class="left" onclick="fnIntentPrev(this);"></div>
					<div class="right" onclick="fnIntentNext(this);"></div>
				</div>
			</div>			
			<div name="divIntentBit" class="inputBox">
				<input type="text" id="txtIntent#" name="txtIntent" onclick="fnOpenPop(this);" />
				<button type="submit" class="on">true</button>
				<button type="submit">false</button>
				<input type="hidden" name="IntentBit" value="true" />
			</div>
		</div>
	</div>
</body>
</html>