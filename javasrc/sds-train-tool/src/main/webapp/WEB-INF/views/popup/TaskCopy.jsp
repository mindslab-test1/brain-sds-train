<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Task 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery-ui-1.10.4.custom.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {					
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}
				
				fnpreventDefault(e);
			});			
						
			$('#popupck${param.Sense}').prop("checked", true);					
		}
		
		
		function fnCheck(obj, seqno, agentname, prjseqno) {
			$('[id^=popupck]').prop("checked", false);			
			$(obj).prop("checked", true);
			
			$('#hidSeqNo').val(seqno);
			$('#hidPrjSeqNo').val(prjseqno);
			$('#txtAgentName').val(agentname + '_copy');
			$('#hidAgentName').val(agentname);
		}
		
		function fnCopy() {
			if ($('#hidSeqNo').val() == '') {
				alert('복사할 task를 선택해주세요.');
				fnpreventDefault(event);
				return;
			}
			$txtAgentName = $('#txtAgentName'); 
			if ($txtAgentName.val() == '') {
				alert('태스크 명을 입력해주세요.');
				fnpreventDefault(event);
				return;
			}			
			
			var bTaskSaveChk = false;
			
			/* opener.$('#lnb .taskBox div').each(function(e) {				
				if ($(this).text() == $txtAgentName.val()) {
					bTaskSaveChk = true;
					alert('이미 존재하는 Task 입니다.');					
					return false;
				}
			}); */
			
			if (bTaskSaveChk) {				
				return;
			}
			
			fnLoading();
			fnLayerPop('divLayer');
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/taskcopy.do' />");	
			comAjax.setCallback("fnCopyCallBack");
			comAjax.addParam("AGENT_SEQ", $('#hidSeqNo').val());
			comAjax.addParam("AGENT_NAME", $txtAgentName.val());
			comAjax.addParam("FROM_AGENT_NAME", $('#hidAgentName').val());
			comAjax.addParam("TARGET_PROJECT_SEQ", $('#hidPrjSeqNo').val());			
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnCopyCallBack(data) {
			fnLoading();
			$('#layer').fadeOut();			
			if (data.STATUS == 'OK') {				
				if (data.COPY_TYPE == 1) {
					alert('이미 같은 이름의 Class가 다른 Slot 구성으로 존재하여 복사가 불가능합니다.');
					retrun;
				}			
				else if (data.COPY_TYPE == 2) {
					alert('복사중 오류가 발생하였습니다.');
					retrun;
				} 
				
				opener.fnCopyMove(data.SEQ_NO);
				fnWinClose();	
			}
			else {				
				alert('복사 중 오류가 발생하였습니다.');
			}	
			fnpreventDefault(event);
		}
	</script>
</head>
<body style="min-width:550px;">
	<div id="divLoading" style="display:none; box-shadow: 3px 3px 10px #999999;position: absolute;left: 50%;top: 50%;width: 180px;height: 180px;margin-top:-90px;margin-left:-90px;min-width: 170px !important;z-index: 1001;background: url(../images/loading.gif) no-repeat center center #ffffff !important;"></div>
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title">Task 검색</div>
				<div class="box box3">
					<c:set var="ProjectName" value=""></c:set>
					<c:forEach items="${agentlist }" var="row">			
					<c:choose>
						<c:when test="${row.PROJECT_NAME == ProjectName}">
							<div class="check">
								<div class="right" style="width:98%;">									
									<table style="width:98%;">
										<tr>
											<td style="width:40%; vertical-align:middle; word-break:break-all;">							
												<input class="checkbox_st" type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" onclick="fnCheck(this, ${row.SEQ_NO}, '${row.AGENT_NAME}', ${row.PROJECT_SEQ});" />
												<label for="popupck${row.SEQ_NO}">${row.AGENT_NAME}</label>
											</td>
											<td style="vertical-align:middle; word-break:break-all;">
												<span>
													${row.DESCRIPTION}
												</span>
											</td>
										</tr>
									</table>								
								</div>								
							</div>						
						</c:when>
						<c:otherwise>
						<c:if test="${ProjectName != ''}">
							</div>
						</div>
						</c:if>
						<c:set var="ProjectName" value="${row.PROJECT_NAME}"></c:set>
						<div class="entitiesBox">
							<div class="title"><div>${row.PROJECT_NAME}</div><span class="open"></span></div>
							<div class="checkbox">	
							
							<div class="check">
								<div class="right" style="width:98%;">									
									<table style="width:98%;">
										<tr>
											<td style="width:40%; vertical-align:middle; word-break:break-all;">							
												<input class="checkbox_st" type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" onclick="fnCheck(this, ${row.SEQ_NO}, '${row.AGENT_NAME}', ${row.PROJECT_SEQ});" />
												<label for="popupck${row.SEQ_NO}">${row.AGENT_NAME}</label>
											</td>
											<td style="vertical-align:middle; word-break:break-all;">
												<span>
													${row.DESCRIPTION}
												</span>
											</td>
										</tr>
									</table>								
								</div>								
							</div>																
						</c:otherwise>
					</c:choose>					
					</c:forEach>
					<c:if test="${ProjectName != ''}">
						</div>
					</div>
					</c:if>	
				</div>
			</div>
			
			<div class="searchBox">
				<div class="box box3">
					<div class="entitiesBox">
						<div style="background-color:#eee; font-weight: bold;" class="title"><div>복사 될 이름</div><span class="open"></span></div>
						<div class="checkbox">
							<input style="border:1px solid #ddd" type="text" id="txtAgentName" />
						</div>
					</div>							
				</div>						
			</div>
			
			<div class="btnBox" style="background-color:#ffffff !important;">				
				<span>
					<div id="divLoading" style="display:none; box-shadow: 3px 3px 10px #999999;position: fixed;left: 50%;top: 50%;width: 180px;height: 180px;margin-top:-90px;margin-left:-90px;min-width: 170px !important;border:1px solid #999999;z-index: 999;background: url(../images/loading.gif) no-repeat center center #ffffff !important;"></div>
					<div class="ok" onclick="fnCopy();">Copy</div>
					<div class="cancle" onclick="fnWinClose();">Cancel</div>					
				</span>				
			</div>
		</div>
		<input type="hidden" id="hidSeqNo" />		
		<input type="hidden" id="hidPrjSeqNo" />
		<input type="hidden" id="hidAgentName" />
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>