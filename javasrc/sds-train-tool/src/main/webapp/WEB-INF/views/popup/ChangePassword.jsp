<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>비밀번호 변경</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">	
	$(function() {
		$('#spId').text(getCookie('USER_ID'));
		$('#spApi').text(getCookie('API_KEY'));
		
		var reg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&\-_+=])[A-Za-z\d$@$!%*#?&\-_+=]{8,16}$/;
		
		$('#txtNewPassword').bind({
			keyup : function() {
				var text = $(this).val();
				if (reg.test(text)) {
					$('#spPw1').attr('class', 'cu_ok').text('사용 가능합니다.');
				}				
				else {
					$('#spPw1').attr('class', 'cu_no').text('비밀번호는 8~16자리 영문, 특수문자, 숫자를 조합하여 작성해주세요.');
					return false;
				}				
			}
		});
		
		$('#txtNewPasswordChk').keyup(function() {
			if ($(this).val() == $('#txtNewPassword').val()) {
				$('#spPw2').attr('class', 'cu_ok').text('비밀번호가 일치 합니다.');					
			}				
			else {
				$('#spPw2').attr('class', 'cu_no').text('비밀번호가 일치하지 않습니다.');
			}				
		});
	});
		
	function fnChange() {
		$txtPassword = $('#txtPassword');
		
		if ($txtPassword.val().trim() == '') {
			alert('현재 비밀번호를 입력해 주세요.');
			return;
		}
		
		if ($('#spPw1').text() == '사용 가능합니다.' && $('#spPw2').text() == '비밀번호가 일치 합니다.') {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/changepassword.do' />");
			comAjax.setCallback("fnChangeCallBack");						
			comAjax.addParam("USER_ID", $('#spId').text());
			comAjax.addParam("USER_PWD", $txtPassword.val());
			comAjax.addParam("NEW_USER_PWD", $("#txtNewPassword").val());
			
			comAjax.ajax();
		}
		else {
			alert('변경할 비밀번호를 바르게 입력해주세요.');
		}
	}
	
	function fnChangeCallBack(data) {
		if (data.STATUS == '1') {
			alert('현재 비밀번호가 일치하지 않습니다.');
			$('#txtPassword').focus();
		}
		else if (data.STATUS == '2') {
			alert('비밀번호 변경 중 오류가 발생 하였습니다. \n관리자에게 문의해 주세요.');
		}
		else {
			alert('비밀번호가 변경 되었습니다.');
			fnWinClose();
		}
	}
	
	
</script>

</head>
<body>
	<div id="popWrap" class="popWrap">
		<div class="middleW createTask fnSG">
			<div class="searchBox">
				<div class="box box2 title_con">				
					<div class="zoom_style">
						<div>ID : <span id="spId"></span></div>
					</div>
					<c:if test="${param.usertype eq 'OA'}">
						<div class="zoom_style" style="margin-bottom:10px;">
							<div>ETRI Open API Key : <span id="spApi"></span></div>						
						</div>				
					</c:if>
				</div>				
			</div>
			<div class="searchBox">
				<div class="title">비밀번호 변경하기</div>
				<div class="box box_input"><input type="password" id="txtPassword" maxlength="16" placeholder="현재 비밀번호" /></div>
				<div class="box box_input" style="margin: 15px 15px 0px 15px;"><input type="password" id="txtNewPassword" maxlength="16" placeholder="새 비밀번호" /></div>
				<span id="spPw1" class="cu_no" style="font-size:12px;">비밀번호는 8~16자리 영문, 특수문자, 숫자를 조합하여 작성해주세요.</span>
				<div class="box box_input" style="margin: 5px 15px 0px 15px;"><input type="password" id="txtNewPasswordChk" maxlength="16" placeholder="새 비밀번호 확인" /></div>
				<span id="spPw2" class="cu_no" style="font-size:12px; margin-bottom:5px;"></span>
			</div>
			<div class="btnBox" style="margin-top:50px; background:none !important;">
				<div class="ok" onclick="fnChange();">Ok</div>
				<div class="cancle" onclick="fnWinClose();">Cancel</div>
			</div>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>