<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Encoding 설정</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">		
		$(function () {
			$('[id^=popupck]').click(function() {
				$('[id^=popupck]').prop('checked', false);
				this.checked = true;
			});
			
			$('#popupck${encodingdata.ENCODING_NAME}').prop('checked', true);			
		});
					
		function fnSave() {
			var encodingval = 'EUC-KR';
			if ($('#popupckUTF-8').is(":checked") == true)
				encodingval = 'UTF-8';				
		
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/updateencoding.do' />");
			comAjax.setCallback("fnSaveCallBack");			
			comAjax.addParam("ENCODING_NAME", encodingval);							
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {			
			if(data.state == 'OK') {				
				fnWinClose();			
			}
			else {
				alert('인코딩 설정 중 오류가 발생 되었습니다.');
			}
		}
	</script>
</head>
<body style="min-width:280px;">
<div class="popup setEncoding" style="width:100%; height:100%; margin:0; border:none; border-radius:0; overflow:none;">
	<div class="title">Set Encoding</div>
	<div class="body" style="height:138px;">
		<div class="title"><div class="squre"></div>도메인</div>
		<div class="popRadio">
			<div><input type="checkbox" id="popupckEUC-KR" name="popupckEUC-KR" value="EUC-KR" class="checkbox_st"/><label for="popupckEUC-KR">EUC-KR</label></div>
			<div><input type="checkbox" id="popupckUTF-8" name="popupckUTF-8" value="UTF-8" class="checkbox_st"/><label for="popupckUTF-8">UTF-8</label></div>
		</div>
	</div>
	<div class="btnBox">
		<div onclick="fnSave();" class="save">Save</div>
		<div onclick="javascript:window.close();" class="cancle">Cancel</div>
	</div>
</div>
	<!-- <div class="popup_function" style="width:235px !important; min-width:235px !important; height:220px !important;">
    	<p>Encoding 설정</p>
        <dl class="function_list">
        	<dt>도메인</dt>
        	<dd class="function_list_bg01 ck_box">
            	<span class="check popupck_d1">
                    <input type="checkbox" id="popupckEUC-KR" name="popupckEUC-KR" value="EUC-KR" style="margin: 8px;"/><label for="popupckEUC-KR" style="vertical-align: text-top;">EUC-KR</label>
                </span>
            </dd>    
            <dd class="function_list_bg01 ck_box">
            	<span class="check popupck_d1">
                    <input type="checkbox" id="popupckUTF-8" name="popupckUTF-8" value="UTF-8" style="margin: 8px;"/><label for="popupckUTF-8" style="vertical-align: text-top;">UTF-8</label>
                </span>
            </dd>    
        </dl>
        <div class="layer_box_btn mt30">
        	<a id="aCopy" href="#" onclick="fnSave();">저장</a>            
            <a href="#" onclick="javascript:window.close();">닫기</a>
        </div>
    </div>
    
	<form id="commonForm" name="commonForm">
	</form>-->
</body>
</html>