<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>Log</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/chosen.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-latest.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/chosen.jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>


<script type="text/javascript">	
	$(function() {
		
	});		
</script>

</head>
<body style="min-width:0px; overflow:auto;">
	<div class="popup_function" style="height:410px !important;">
		<!-- Title h1 -->
		<p style="float: left; height: 24px; line-height: 24px; margin-bottom:0px; margin-right:13px;">Turn 2, 3</p>					
		<div>
			<a style="margin:0px 2px;" href="#"><img src="../images/btn_prev.png" alt="이전 보기"></a>
			<a style="margin:0px 2px;" href="#"><img src="../images/btn_next.png" alt="다음 보기"></a>
		</div>
		<!-- Title h1// -->
		
		<hr />
		<div class="turn_chapter01">
			<span class="t_title_buu t_title_but" style="float:none;">Task</span>
			<div class="turn_table_u">
				<table class="turn_table" summary="task">
					<caption class="hide">정보 표</caption>
					<colgroup>
						<col width="35%" />
						<col width="65%" />
					</colgroup>
					<tbody>
						<tr>
							<th>Previous task<span>&nbsp;:&nbsp;</span></th>
							<td>Weather Info</td>
						</tr>
						<tr>
							<th>Current task<span>&nbsp;:&nbsp;</span></th>
							<td>Weather Info</td>
						</tr>
						<!-- <tr>
							<th>Triggered condition<span>&nbsp;:&nbsp;</span></th>
							<td>aasadasdasdsd</td>
						</tr> -->
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="turn_chapter02">
			<span class="t_title_buu t_title_but" style="float:none;">Entities</span>
			<div class="turn_table_u">
				<table class="turn_table" summary="Entities">
					<caption class="hide">정보 표</caption>
					<colgroup>
						<col width="20%" />
						<col width="80%" />
					</colgroup>
					<tbody>
						<tr>
							<th colspan="2">Weather</th>
						</tr>
						<tr>
							<th class="th_bg01">date<span>&nbsp;:&nbsp;</span></th>
							<td>내일</td>
						</tr>
						<tr>
							<th class="th_bg01">date<span>&nbsp;:&nbsp;</span></th>
							<td>내일</td>
						</tr>
						<tr>
							<th class="th_bg02">city</span><span>&nbsp;:&nbsp;</span></th>
							<td>대구</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="turn_chapter03">
			<span class="t_title_buu t_title_but" style="float:none;">Intents</span>
			<div class="turn_table_u">
				<table class="turn_table" summary="Intents">
					<caption class="hide">정보 표</caption>
					<colgroup>
						<col width="20%" />
						<col width="80%" />
					</colgroup>
					<tbody>
						<tr>
							<th colspan="2">User</th>
						</tr>
						<tr>
							<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
							<td>“대구 내일 날씨는“</td>
						</tr>
						<tr>
							<th class="th_bg02">DA</span><span>&nbsp;:&nbsp;</span></th>
							<td>request(Weather.info, Weather.city="대구", Weather.date="내일")</td>
						</tr>
						<tr>
							<th colspan="2">System</th>
						</tr>
						<tr>
							<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
							<td>“&lt;Weather.city&gt;의 &lt;Weather.date&gt; 날씨는 &lt;Weather.info&gt;입니다.”</td>
						</tr>
						<tr>
							<th class="th_bg02">DA</span><span>&nbsp;:&nbsp;</span></th>
							<td>inform()</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- 				
		<div class="turn_chapter04">
			<div class="turn_chapter04_left">
				<span class="t_title_buu t_title_but" style="width:80px; height:30px; line-height:30px;">Script Test</span>
				<a class="orange_btn" href="#">Script guide</a>
				<input style="height:20px; line-height:20px; width:180px; margin:10px 0px 0px 20px" type="text" value="search slots…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="Clear_search" name="Clear_search" class="Clear_search" />
			</div>
			<div class="turn_chapter04_right">
				<a href="#">사용자 발화 ▶ 시스템 발화 찾기 까지</a>
				<a href="#">시스템 발화 태스크 이동</a>
				<a href="#">true</a>
				<a href="#">false</a>
			</div>
		</div>		 -->
	</div>
</body>
</html>