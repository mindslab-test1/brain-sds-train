<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Task 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery-ui-1.10.4.custom.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}
				
				fnpreventDefault(e);
			});
									
			$('#popupck${param.Sense}').prop("checked", true);					
		}
		
		
		function fnCheck(obj, seqno, agentname) {
			$('[id^=popupck]').prop("checked", false);			
			$(obj).prop("checked", true);
			
			if ('${param.Type}' == 'S') {
				opener.$('#hidClassAgent').val(seqno);
				opener.$('#txtClassAgent').val(agentname);	
			}
			else {				
				opener.$('#${param.obj}').val(agentname);
			}
			
			fnWinClose();
		}		
	</script>    
</head>
<body style="min-width:413px;">
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title">
					<c:choose>
						<c:when test="${param.Type == 'S'}">Defined Task 설정</c:when>
						<c:otherwise>Task 검색</c:otherwise>
					</c:choose>				
				</div>
				<div class="box box3">					
					<div class="entitiesBox">						
						<div class="checkbox">		
							<c:if test="${param.Type == 'S'}">															
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupck0" name="popupck0" onclick="fnCheck(this, '', 'All tasks');" />
										<label for="popupck0">All tasks</label>								
									</div>								
								</div>
							</c:if>
							<c:forEach items="${submenu }" var="row">
								<c:if test="${row.AGENT_NAME != 'END'}">
									<div class="check">
										<div class="right">
											<input type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" onclick="fnCheck(this, ${row.SEQ_NO}, '${row.AGENT_NAME}');" />
											<label for="popupck${row.SEQ_NO}">${row.AGENT_NAME}</label>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</div>
					</div>					
				</div>
			</div>
			<div class="btnBox" style="background:none !important">
				<span>
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
				</span>				
			</div>
		</div>
	</div>
		
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>