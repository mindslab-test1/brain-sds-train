<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>GenieDialog 매뉴얼</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/manual.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
	<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ui.all.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout.min.js'/>"></script> --%>
	<script type="text/javascript" src="<c:url value='/js/jquery-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout-latest.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">
		$(function() {
			if ('${param.spos}' != '0') {
				$(window).document.scrollTop(${param.spos});
			}
			else if ('${param.id}' != '') {
				fnMove('${param.id}'.replace('%23', '#'), 1);
			}
		});
		function fnMove(id, type) {
			if (type != 1) {
				window.parent.backType = 'append';
				window.parent.backScrollPos = $(window).scrollTop();
				window.parent.$('#spBack').show();	
			}			
			
			if (id.indexOf('${param.type}') > -1) {
				window.parent.fnMove(id);
			}
			else
				window.parent.fnSelect('menu', id);
			
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<div id="content_wrap">
		<div id="content">
			<div class="content_body">
				<!-- 부록1 start -->
				<h4 id="0append">샘플 도메인 설명</h4>
				<div>					
					<p>다음은 샘플 도메인으로 주어진 5개 도메인 각각에 대한 간략한 설명입니다. 샘플 도메인을 직접 수정하고 추가해 봐서 사용법을 익히기 바랍니다. 각 샘플은 <a href="#" onclick="fnMove('#2_1menu');">도메인 생성(Create Domain)</a>에서 <a href="#" onclick="fnMove('#2_1_2menu');">Sample Domain으로 시작하기</a>로 다음과 같이 사용할 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image007.png" /></p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><b>[주의] Sample Domain을 선택하여 새로 만드신 경우에, 도메인을 처음부터 만든 경우와 마찬가지로 <a href="#" onclick="fnMove('#7_1menu');">학습과 지식저장</a>을 수행해야 대화시스템을 구동할 수 있습니다.</b></td>
							</tr>
						</tbody>
					</table>					
				</div>
				
				<!-- 1장 start -->
				<h4 id="1append">날씨 검색 도메인</h4>
				<div>					
					<p>날씨 검색 도메인은 날씨 정보를 지역과 날짜로 검색하는 도메인입니다. 날씨 검색 도메인에서 제공하는 날씨 정보는 내부 DB에서 검색하기에 예로 검색되고 있습니다. 지역은 특별시와 광역시, 도 수준으로 정의하였고, 날짜는 오늘, 내일, 모레만 가능합니다. 제공하는 정보는 날씨 정보와 강수확률, 미세먼지 농도, 황사 수지합니다. 제공되는 샘플 도메인을 확장하여 최저/최고 온도, 평균 온도, 현재 온도 등과 추가적인 정보를 제공할 수 있도록 확장해 보기 바랍니다.</p>					
				</div>		
				<h5 id="1_1append">대표 대화시나리오</h5>
				<div>														
					<p>날씨 검색 도메인을 샘플로 했으면, 먼저 아래 화면에서 “학습” 버튼을 눌러서 대화지식들을 학습하게 해야 합니다. 학습이 완료되면 “구동” 버튼으로 학습한 대화지식으로 대화시스템이 로딩하도록 합니다. 그 이후에 하나의 대화 섹션을 “Start” 버튼으로 시작합니다. 각 사용자 발화를 입력하면됩니다.</p>
					<p class="center"><img src="../manualfiles/image114.png" /></p>
					<p>날씨 검색 샘플 도메인에는 다음과 같은 대화가 가능합니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<p>system: 궁금하신 날씨를 문의해 보세요. (제가 지금 제공하는 날씨 정보는 예제입니다.)</p>
									<p>user: 서울 날씨는</p>
									<p>system: 오늘, 서울 날씨는 맑음입니다.</p>
									<p>user: 내일은</p>
									<p>system: 서울의 내일 날씨는 흐림입니다.</p>
									<p>user: 비는 오나</p>
									<p>system: 내일 11월 28일 수요일, 서울의 비 올 가능성은 40.0%이고 날씨는 흐림입니다.</p>
									<p>user: 미세먼지는 심한가</p>
									<p>system: 내일 서울의 미세먼지는 130 ㎍/m³로 나쁨 상태입니다.</p>
									<p>user: 황사는 와</p>
									<p>system: 내일 서울의 황사지수는 233로 약간 나쁨입니다.</p>
									<p>user: 부산의 대기 상태는</p>
									<p>system: 내일 부산의 공기 상태는 미세먼지 좋음이고 황사는 보통입니다.</p>
									<p>user: 우산은 가지고 가야 해</p>
									<p>system: 내일 11월 28일 수요일, 부산의 비 올 가능성은 30.0%이고 날씨는 구름 많음입니다.</p>
									<p>user: 그만하자</p>
									<p>system: 다음에 뵙겠습니다. 오늘도 즐거운 하루되세요.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="1_2append">Task Graph</h5>
				<div>														
					<p>날씨 검색 도메인의 Task Graph를 보면다음과 같이 간단하게 2개 Task로 구성되어 있습니다.</p>
					<p class="center"><img src="../manualfiles/image115.png" /></p>
					<p>종료 Task END를 제외하면 실제적인 Task는 Weather_Info 하나입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="17%" />
							<col width="*" />
							<col width="17%" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>Task 이름</th>
								<th>수행 내용</th>
								<th>다음 Task</th>
								<th>이동 조건</th>
							</tr>
							<tr>
								<td class="center_td">Weather_Info</td>
								<td>사용자가 요청하는 날씨 종류(날씨, 강수확률, 미세먼지, 황사)를 파악하고 사용자가 요청하는 지역과 날짜로 그 정보를 제공합니다.</td>
								<td class="center_td">END</td>
								<td>사용자 bye()의도에 시스템이 bye()로 답변한 이후 END로 이동</td>
							</tr>
						</tbody>
					</table>
					<p>상기 표에서 설명되었듯이, Weather_Info Task에서 사용자 검색 요청을 답변을 하다가 사용자가 bye(끝인사) 의도를 발화하면 그에 대한 응답을 하고 종료합니다.</p>
					<p>모든 Task에서 나타날 수 있는 사용자 Intent는 다음과 같습니다. 아래에서 bye만 새로 정의하였고 나머지는 도메인 생성 시에 자동 생성된 것입니다. 필요 없기에 지우셔도 좋지만 교육 관점에서 그대로 두었습니다.</p>
					<p class="center"><img src="../manualfiles/image116.png" /></p>
					<p>-	bye</p>
					<p>사용자가 끝인사를 하는 발화입니다. 예로 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image117.png" /></p>
					<p>시스템 응답으로는 bye DA type으로 다음처럼 응답하도록 했습니다. 아래 시스템 Intent를 기본적인 inform()에서 bye()로 변경하여 시스템이 끝인사를 하였는지 판단할 수 있게 하였습니다. 이 정보를 이용하여 Weather_Info Task에서 END Task로 이동하게 됩니다.</p>
					<p class="center"><img src="../manualfiles/image118.png" /></p>
					<p>Weather_Info Task가 하나만 있기에 그 Task 내부의 Local Intent로 정의할 수 있지만, 추후 확장 및 Task 업무 범위를 명확하게 구분하기 위해 Global Intent에 정의하였습니다.</p>
				</div>
				<h5 id="1_3append">Weather_Info Task</h5>
				<div>														
					<p>날씨 검색 도메인에서 Weather_Info Task만 정의되어 있습니다. 따라서, Weather_Info Task가 하나의 도메인 대화를 모두 수행하도록 구성되어 있습니다.</p>
				</div>
				<div>
					<h6 id="1_3_1append">Entity</h6>										
					<p>날씨 검색 도메인에서 사용할 Entity는 다음과 같이 정의하였습니다. 하나의 Class Weather과 아래처럼 9개의 Slot으로 구성하였습니다.</p>
					<p class="center"><img src="../manualfiles/image119.png" /></p>
					<p>다음은 Weather Class의 Slot 설명입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="9">Weather</td>
								<td class="center_td">city</td>								
								<td>사용자 요청 지역, Type은 place로 설정</td>
							</tr>
							<tr>								
								<td class="center_td">date</td>								
								<td>사용자 요청 날짜를 String으로 변경한 정보로 r_date Slot의 값을 문자열로 가진 형태, Default 값으로 “서울” 정의</td>
							</tr>
							<tr>								
								<td class="center_td">r_date</td>								
								<td>사용자 요청 날짜, Type은 date으로 설정, Instance와 연동하지 않음</td>
							</tr>
							<tr>								
								<td class="center_td">info</td>								
								<td>지역과 날짜에 대한 날씨 정보</td>
							</tr>
							<tr>								
								<td class="center_td">rain</td>								
								<td>지역과 날짜에 대한 강수 확률</td>
							</tr>
							<tr>								
								<td class="center_td">fine_dust</td>								
								<td>지역과 날짜에 대한 미세먼지 농도</td>
							</tr>
							<tr>								
								<td class="center_td">fine_dust_info</td>								
								<td>지역과 날짜의 미세먼지 상태 정보(좋음, 보통, 나쁨, 매우 나쁨)</td>
							</tr>
							<tr>								
								<td class="center_td">yellow_dust</td>								
								<td>지역과 날짜에 대한 황사지수</td>
							</tr>
							<tr>								
								<td class="center_td">yellow_dust_info</td>								
								<td>지역과 날짜의 황사 상태(보통, 약간 나쁨, 나쁨, 매우 나쁨)</td>
							</tr>
						</tbody>
					</table>
					<p>Weather Class Instance DB는 다음과 같이 들어가 있습니다.</p>
					<p class="center"><img src="../manualfiles/image120.png" /></p>
					<p>Weather Class에서 살펴봐야 할 Slot은 r_date과 date Slot입니다. Class의 Instance DB에는 r_date이 없습니다. 이유는 Instance 연동을 하지 않았기 때문입니다.</p>
					<p>Weather.r_date Slot은 사용자 발화에서 태깅할 때 사용할 Slot 값으로 date Type으로 정의했습니다. date Type이기에 학습문장에 태깅되지 않은 날짜 단어라도 Weather.r_date으로 태깅될 가능성이 높아지는 장점이 있기 때문입니다. Weather.r_date의 Type이 date이기에, 대화시스템 내부적으로 그 값이 “2019-01-01” 형태로 저장됩니다. Weather.date Slot은 이러한 Weather.r_date 값 형태를 “오늘, 내일, 모레, 1월 4일, ...”과 같은 형식으로 갖습니다. Weather.date Slot이 이와 같은 값을 가지는 것은 Weather.r_date의 Slot 설정에서 Slot value relationship에서 다음 define이 있기 때문입니다.</p>
					<p class="center"><img src="../manualfiles/image121.png" /></p>
					<p>상기 define은 Condition이 true이기에, 사용자 발화에서 Weather.r_date이 인식되면, Action을 수행합니다. Action은 “Set("Weather.date", PrintDate("Weather.r_date", "오늘/내일 형식"));”이기에 Weather.date Slot에 Weather.r_date 값을 “오늘/내일 형식”으로 저장합니다. </p>
					<p>따라서, 사용자가 발화하여 인식한 Weather.r_date 값을 자동으로 Weather.date 값으로 변경하게 되어 Weather Class의 Instance DB에서 Weather.date으로 검색이 되도록 했습니다.</p>
				</div>
				<div>
					<h6 id="1_3_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image122.png" /></p>
							<p>Weather_Info Task 시작 시, 첫 시스템 대화입니다.</p>	
						</li>
						<li>진행 대화
							<p class="center"><img src="../manualfiles/image123.png" /></p>
							<p>날씨 정보 검색에 필요한 조건 2개 지역(Weather.city)와 날짜(Weather.date or Weather.r_date)를 문의합니다. 진행 대화이기에 사용자가 발화하지 않은 경우에만 출력합니다. Entity에서 Weather.date의 Default 값으로 “오늘”을 설정하였기에, 2번째 진행 대화를 시스템이 발화할 경우는 없습니다.</p>	
						</li>
						<li>응답 대화
							<p>Weather_Info Task에 정의된 사용자 발화 가능한 Intent 목록입니다.</p>
							<p class="center"><img src="../manualfiles/image124.png" /></p>
							<ol>
								<li>request(Weather.info)
									<p>날씨 정보를 문의하는 사용자 발화입니다. 사용자 발화에 2가지 조건 Slot을 넣어서 발화하거나 없이 발화할 수 있습니다.</p>
									<p class="center"><img src="../manualfiles/image125.png" /></p>
									<p>이에 대한 시스템 응답은 날짜 Weather.date에 Default로 “오늘”로 설정했기에 검사하지 않고 사용자가 지역을 발화한 경우이고 대답할 Weather.info 개수가 1개 이상이면 아래 패턴으로 대답합니다. 이 조건에 맞지 않으면 사용자의 이 의도에 대해 시스템의 응답 대화는 없습니다. “{ }”로 시스템 응답 패턴을 감싼 것은 검색된 수만큼 반복하기 위한 것입니다. 자세한 것은 <a href="#" onclick="fnMove('#5_5_2menu');">시스템 발화 패턴 작성 방법</a>을 참조하세요. </p>
									<p class="center"><img src="../manualfiles/image126.png" /></p>
								</li>
								<li>non_response
									<p>예약된 사용자 발화 DA Type으로 시스템이 발화할 내용이 없는 경우에 여기 정의된 응답 대화로 대답을 합니다. 따라서 실제 사용자 발화는 없습니다.</p>
									<p>Weather_Info Task 내부에 정의되었기에 Weather_Info Task 상황에서 응답할 수 없을 때, 검사하여 응답합니다. 응답할 수 없는 상황은 사용자가 지역과 날짜를 발화했지만, 그 정보가 Instance DB에 없는 경우와 사용자 발화가 unknown 또는 응답이 정의되지 않은 Intent(affirm, negate)인 경우입니다. 2개 응답은 Instance DB에 사용자 발화 조건이 없는 경우에 대한 것입니다. </p>
									<p class="center"><img src="../manualfiles/image127.png" /></p>
								</li>
								<li>request(Weather.rain)
									<p>강수확률에 대한 질문 의도로 사용자는 다음과 같은 발화 종류들이 가능합니다.</p>
									<p class="center"><img src="../manualfiles/image128.png" /></p>
									<p>시스템 응답은 request(Weather.info)와 유사한 조건 상황에서 강수확률을 알려주는 응답입니다. </p>
									<p class="center"><img src="../manualfiles/image129.png" /></p>
								</li>
								<li>request(Weather.yellow_dust)
									<p>황사지수에 대한 질문 의도로 사용자는 다음과 같은 발화 종류들이 가능합니다.</p>
									<p class="center"><img src="../manualfiles/image130.png" /></p>
									<p>이에 대한 시스템 응답은 다음과 같습니다.</p>
									<p class="center"><img src="../manualfiles/image131.png" /></p>
								</li>
								<li>request(Weather.fine_dust)
									<p>미세먼지 농도에 대한 질문 의도로 사용자는 다음과 같은 발화 종류들이 가능합니다.</p>
									<p class="center"><img src="../manualfiles/image132.png" /></p>
									<p>이에 대한 시스템 응답은 다음과 같습니다.</p>
									<p class="center"><img src="../manualfiles/image133.png" /></p>
								</li>
								<li>request(Weather.fine_dust, Weather.yellow_dust)
									<p>대기 상태에 대한 질문 의도로 사용자는 다음과 같은 발화 종류들이 가능합니다. 미세먼지나 황사에 대한 명확한 정보가 없이 전체적인 대기 상태를 문의하는 것으로 정의했습니다.</p>
									<p class="center"><img src="../manualfiles/image134.png" /></p>
									<p>이에 대한 시스템 응답은 다음과 같습니다.</p>
									<p class="center"><img src="../manualfiles/image135.png" /></p>
								</li>
								<li>request(unknown)
									<p>사용자가 정확한 정보를 문의하지 않은 발화입니다. 단지 조건들을 발화하는 것입니다. <a href="#" onclick="fnMove('#5_4_3menu');">request(unknown) Intent</a>에서 설명한 내용을 반영한 것입니다. unknown 질문 Slot은 추후 대화시스템에서 가장 최근에 기록된 질문 Slot으로 변경하여 답변을 합니다.</p>
									<p class="center"><img src="../manualfiles/image134.png" /></p>
									<p>request(unknown)에 대한 시스템 응답은 unknown이 최근 질문 Slot으로 변경되지 못한 경우에만 적용됩니다. 따라서, 대화에서 어떠한 명확한 질문을 하지 않은 상태이기에 다음처럼 답변하도록 정의하였습니다.</p>
									<p class="center"><img src="../manualfiles/image136.png" /></p>
								</li>
								<li>request(Weather.city)
									<p>사용자가 지역을 언급하였는데, Instance DB에 없는 지명이어서 답변할 수 없는 지명이라고 응답할 때, 사용자가 시스템이 어떤 장소를 알고 있는지에 대해 문의할 수 있는 상황의 의도입니다. 아래와 같이 사용자가 발화할 수 있습니다.</p>
									<p class="center"><img src="../manualfiles/image137.png" /></p>
									<p>시스템 응답은 다음과 같습니다. “{Weather.city}”로 응답하지 않는 것은 대화이력에 사용자가 발화한 지역명이 들어가 있고 다른 조건들로 Weather.city 결과가 제한되기 때문입니다.</p>
									<p class="center"><img src="../manualfiles/image138.png" /></p>
								</li>
							</ol>						
						</li>
					</ol>
				</div>
				
				<!-- 2장 start -->
				<h4 id="2append">피자 주문 도메인</h4>
				<div>					
					<p>피자 주문 상황을 시뮬레이션한 도메인입니다. 고객으로부터 주문 받을 피자 정보를 입력 받고, 음료 주문 여부를 물어본 다음 그에 따라 주문 받을 음료 정보를 입력 받고, 결제 수단을 물어본 다음 주문을 접수함으로써 대화가 종료됩니다.</p>					
				</div>		
				<h5 id="2_1append">대표 대화시나리오</h5>
				<div>
					<p>대표적인 대화시나리오는 아래와 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<p>system: 안녕하세요, 함께 즐겨요 피자훗입니다. 주문하실 피자 종류, 크기, 도우를 말씀해주세요.</p>
									<p>user: 페퍼로니 피자 라지 사이즈로 주세요</p>
									<p>system: 어떤 도우의 피자를 주문하시겠습니까? </p>
									<p>(오리지널, 나폴리, 씬)</p>
									<p>user: 보통 도우로요</p>
									<p>system: 페퍼로니 피자 라지 사이즈 오리지널 도우의 가격은 22900원 입니다.음료도 주문하시겠습니까?</p>
									<p>user: 네</p>
									<p>system: 주문하실 음료를 말씀해주세요. </p>
									<p>(콜라, 사이다)</p>
									<p>user: 사이다 얼마에요</p>
									<p>system: 사이다의 가격은 1800원 입니다. 주문하실 음료를 말씀해주세요. </p>
									<p>(콜라, 사이다)</p>
									<p>user: 사이다로 주세요</p>
									<p>system: 음료 가격 1800원을 포함하여 총 가격은 24700원 입니다. 결제는 현금으로 하시겠어요, 카드로 하시겠어요?</p>
									<p>user: 카드로 할께요</p>
									<p>system: 30분 안에 배달됩니다. 카드를 미리 준비해주세요. 이용해 주셔서 감사합니다.</p>
								</td>
							</tr>
						</tbody>
					</table>
					<p>위의 대화시나리오는 크게 아래의 4단계로 구성되어 있습니다.</p>
					<ol class="decimal_ol">
						<li>우선 주문 받을 피자 정보를 입력 받습니다. 여기서 사용자는 피자 종류와 가격 등도 물어볼 수 있습니다.</li>
						<li>그런 후에 음료 주문 여부에 따라 주문 받을 음료 정보를 입력 받습니다. 여기서도 사용자는 음료수의 종류나 가격 등을 물어볼 수 있습니다.</li>
						<li>그 다음 사용자에게 총 결제 금액을 알려주고 결제 수단을 물어봅니다.</li>
						<li>입력 받은 결제 수단을 알려주고 대화를 종료합니다.</li>
					</ol>
				</div>
				<h5 id="2_2append">Task Graph 와 Entity</h5>
				<div>														
					<p>피자 주문 도메인의 Task Graph는 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image139.png" /></p>
					<p>피자 주문 도메인의 Task들은 다음과 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="17%" />
							<col width="*" />
							<col width="17%" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>Task 이름</th>
								<th>수행 내용</th>
								<th>다음 Task</th>
								<th>이동 조건</th>
							</tr>
							<tr>
								<td class="center_td">Greet</td>
								<td>첫인사를 수햄함.</td>
								<td class="center_td">Get_pizza_info</td>
								<td>항상</td>
							</tr>
							<tr>
								<td class="center_td">Get_pizza_info</td>
								<td>주문 받을 피자 정보(피자 종류, 크기, 도우)를 입력 받음. 추가적으로, 사용자의 발화에 따라 피자 종류와 가격을 알려줌.</td>
								<td class="center_td">Ask_drink</td>
								<td>사용자가 DB에 있는 피자 종류, 크기, 가격을 모두 말했을 때</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">Ask_drink</td>
								<td rowspan="2">음료 주문 여부를 물어봄</td>
								<td class="center_td">Get_drink_info</td>
								<td>사용자가 음료 주문 물음에 대해 긍정적으로 대답했을 때</td>
							</tr>
							<tr>								
								<td class="center_td">Get_payment_info</td>
								<td>사용자가 음료 주문 물음에 대해 부정적으로 대답했을 때</td>
							</tr>
							<tr>
								<td class="center_td">Get_drink_info</td>
								<td>주문 받을 음료 정보(음료 종류)를 입력 받음. 추가적으로, 사용자의 발화에 따라 음료 종류와 가격을 알려줌.</td>
								<td class="center_td">Get_payment_info</td>
								<td>사용자가 DB에 있는 음료 종류를 말하거나, 음료 주문을 취소할 때</td>
							</tr>
							<tr>
								<td class="center_td">Get_payment_info</td>
								<td>총 주문 금액을 알려주고 결제 수단을 물어봄.</td>
								<td class="center_td">Bye</td>
								<td>사용자가 결제 수단을 말했을 때</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>결제 수단을 확인하고 대화 종료.</td>
								<td class="center_td">END</td>
								<td>시스템이 끝인사를 수행했을 때</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="2_3append">Greet Task</h5>
				<div>
					<h6 id="2_3_1append">시스템 대화</h6>					
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image140.png" /></p>			
							<p>Task 진입 시, 위와 같은 첫인사를 수행합니다. 이후 바로 다음 Task로 넘어갑니다.</p>
						</li>
					</ol>
				</div>
				<h5 id="2_4append">Get_pizza_info Task</h5>
				<div>
					<h6 id="2_4_1append">Entity</h6>
					<p>Pizza Class: 피자 정의입니다.</p>					
					<p class="center"><img src="../manualfiles/image141.png" /></p>
					<p>- Pizza Class는 다음 Slot들로 구성되어 있습니다.</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="4">Pizza</td>
								<td class="center_td">type</td>								
								<td>피자 종류, Sys.string<br>대표값: 리얼바비큐, 포테이토, 슈퍼디럭스, 슈퍼슈프림, 베이컨체더치즈, 불고기, 페퍼로니, 치즈</td>
							</tr>
							<tr>								
								<td class="center_td">size</td>								
								<td>피자 크기, Sys.string<br>대표값: 미디엄, 라지</td>
							</tr>
							<tr>								
								<td class="center_td">dough</td>								
								<td>피자 도우, Sys.string<br>대표값: 오리지널, 나폴리, 씬</td>
							</tr>
							<tr>								
								<td class="center_td">price</td>								
								<td>피자 가격, Sys.number</td>
							</tr>
						</tbody>
					</table>
					<p>- Pizza Class의 Instance DB는 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image142.png" /></p>
					<p>Pizza Class Instance에 작성된 피자들만 정보 제공이 가능합니다. 더 많은 피자 정보를 제공하기 위해서는 관련된 정보를 추가적으로 작성해주어야 합니다.</p>
				</div>
				<div>
					<h6 id="2_4_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image143.png" /></p>
							<p>Task 진입 시, 피자 종류, 크기, 도우를 물어봅니다.</p>	
						</li>
						<li>진행 대화
							<p class="center"><img src="../manualfiles/image144.png" /></p>
							<p>사용자가 피자 종류, 크기, 도우를 말하지 않았다면 순서대로 해당 Slot 값을 물어봅니다.</p>
						</li>
						<li>응답 대화
							<ol>
								<li>inform_pizza
									<p>사용자가 주문할 피자 정보를 말하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>페퍼로니 피자요</p>
													<p>치즈 피자 라지 사이즈요</p>
													<p>페퍼로니 피자 라지 사이즈 오리지널 도우요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image145.png" /></p>
									<p class="center"><img src="../manualfiles/image146.png" /></p>
									<p>각각 사용자가 말한 피자 종류, 크기, 도우가 DB에 없을 경우 응답되는 발화들입니다. 해당 시스템 응답 후, 관련된 Slot을 초기화합니다.</p>
								</li>
								<li>ask_pizza_type
									<p>판매 중인 피자 종류를 물어보는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>피자 뭐뭐 있어요</p>
													<p>어떤 피자 있어요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>									
									<p class="center"><img src="../manualfiles/image147.png" /></p>
									<p>판매 중인 모든 피자 정보를 알려주는 발화입니다.</p>
								</li>
								<li>ask_pizza_price
									<p>피자 가격을 물어보는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>치즈 피자 가격 얼마에요</p>
													<p>페퍼로니 피자 얼마야</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>									
									<p class="center"><img src="../manualfiles/image148.png" /></p>
									<p class="center"><img src="../manualfiles/image149.png" /></p>
									<p>사용자가 물어본 피자가 instance에 없다면 판매하고 있지 않다고 알립니다. Instance에 있다면 피자 가격을 알려줍니다. 그리고 관련 Slot 값을 초기화합니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="2_5append">Ask_drink Task</h5>
				<div>
					<h6 id="2_5_1append">Entity</h6>									
					<h7>Ask_drink Class</h7>
					<p class="center"><img src="../manualfiles/image150.png" /></p>
					<p>- Ask_drink Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td">Ask_drink</td>
								<td class="center_td">answer</td>								
								<td>음료 주문 여부, Sys.string, Instance와 연동 체크 해제</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<h6 id="2_5_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image151.png" /></p>
							<p>Task 진입 시, 주문 받은 피자 정보를 알려주고 음료 주문 여부를 물어봅니다.</p>	
						</li>
						<li>진행 대화
							<p class="center"><img src="../manualfiles/image152.png" /></p>
							<p>사용자가 음료 주문 여부를 대답하지 않은 경우, 반복적으로 물어봅니다.</p>
						</li>
						<li>응답 대화
							<ol>
								<li>affirm
									<p>음료 주문 여부 물음에 대해 사용자가 긍정적으로 대답한 경우입니다.</p>									
									<p class="center"><img src="../manualfiles/image153.png" /></p>
									<p class="center"><img src="../manualfiles/image154.png" /></p>
									<p>이전 시스템 발화가 음료 주문 여부를 물어보는 것이고 (-> Intent("system", "request_ask_drink", "2", "true")), 사용자가 긍정적으로 대답했다면 (-> affirm), Ask_drink.answer의 Slot 값을 “yes”로 설정합니다. 여기서 시스템 응답이 비어있는 이유는, 이 응답 대화에서는 별다른 발화는 하지 않고 Ask_drink.answer의 Slot 값만 변화시키기 위함입니다.</p>
								</li>
								<li>negate
									<p>음료 주문 여부 물음에 대해 사용자가 부정적으로 대답한 경우입니다.</p>													
									<p class="center"><img src="../manualfiles/image155.png" /></p>
									<p class="center"><img src="../manualfiles/image156.png" /></p>
									<p>이전 시스템 발화가 음료 주문 여부를 물어보는 것이고 (-> Intent("system", "request_ask_drink", "2", "true")), 사용자가 부정적으로 대답했다면 (-> negate), Ask_drink.answer의 Slot 값을 “no”로 설정합니다. 여기서 시스템 응답이 비어있는 이유는, 위에서와 마찬가지로, 이 응답 대화에서는 별다른 발화는 하지 않고 Ask_drink.answer의 슬롯 값만 변화시키기 위함입니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="2_6append">Get_drink_info Task</h5>
				<div>
					<h6 id="2_6_1append">Entity</h6>
					<h7>Drink Class</h7>									
					<p class="center"><img src="../manualfiles/image157.png" /></p>
					<p>- Drink Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">Drink</td>
								<td class="center_td">type</td>								
								<td>음료 종류, Sys.string<br>대표값: 콜라, 사이다</td>
							</tr>
							<tr>								
								<td class="center_td">price</td>								
								<td>음료 가격, Sys.number</td>
							</tr>
						</tbody>
					</table>
					<p>- Drink Class Instance DB</p>
					<p class="center"><img src="../manualfiles/image158.png" /></p>
					<p>Drink Class Instance에 작성된 음료들만 정보 제공이 가능합니다. 더 많은 음료 정보를 제공하기 위해서는 관련된 정보를 추가적으로 작성해주어야 합니다.</p>
				</div>
				<div>
					<h6 id="2_6_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image159.png" /></p>
							<p>Task 진입 시, 주문 받을 음료 정보를 물어봅니다.</p>	
						</li>
						<li>진행 대화
							<p class="center"><img src="../manualfiles/image160.png" /></p>
							<p>사용자가 음료 종류를 물어보지 않았다면, 이 Slot 값을 물어봅니다.</p>
						</li>
						<li>응답 대화
							<ol>
								<li>inform_drink
									<p>사용자가 주문할 음료 정보를 말하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>콜라요</p>
													<p>사이다 주세요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답은 아래와 같습니다.</p>					
									<p class="center"><img src="../manualfiles/image161.png" /></p>									
									<p>사용자가 말한 음료 종류가 DB에 없을 경우 응답되는 발화입니다.</p>
								</li>
								<li>ask_drink_type
									<p>판매 중인 음료 종류를 물어보는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>음료 뭐뭐 있어요</p>
													<p>음료수 뭐뭐 팔아요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image162.png" /></p>
									<p>판매 중인 모든 음료 정보를 알려주는 발화입니다.</p>
								</li>
								<li>ask_drink_price
									<p>음료 가격을 물어보는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>콜라 얼마야</p>
													<p>음료수 가격이 어떻게 돼요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image163.png" /></p>
									<p class="center"><img src="../manualfiles/image164.png" /></p>
									<p>사용자가 물어본 음료가 instance에 없다면 판매하고 있지 않다고 알립니다. Instance에 있다면 음료 가격을 알려줍니다. 그리고 관련 Slot 값을 초기화합니다.</p>
									<p class="center"><img src="../manualfiles/image165.png" /></p>
									<p>추가적으로, 사용자가 특정 음료를 말하지 않았다면, 판매 중인 모든 음료의 정보를 알려줍니다.</p>
								</li>
								<li>inform_no_drink
									<p>사용자가 음료 주문을 취소하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>음료 그냥 안 살래요</p>
													<p>음료 주문 취소</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 경우 별다른 시스템 응답을 하지 않고 바로 다음 Task로 넘어갑니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="2_7append">Get_payment_info Task</h5>
				<div>
					<h6 id="2_7_1append">Entity</h6>					
					<h7>Order Class</h7>
					<p class="center"><img src="../manualfiles/image166.png" /></p>
					<p>- Order Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td">Order</td>
								<td class="center_td">payment_type</td>								
								<td>결제 수단, Sys.string, Instance와 연동 체크 해제</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<h6 id="2_7_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image167.png" /></p>
							<p>Task 진입 시, 사용자가 음료를 주문했는지 여부에 따라 서로 다른 시스템 응답이 나옵니다. 우선 사용자가 주문할 음료를 말하지 않거나, 음료를 주문하지 않겠다고 하면 피자 금액만 안내가 됩니다. 반면 사용자가 주문할 음료를 말한 경우 피자 가격과 음료 가격이 더해진 총 금액이 안내가 됩니다. 그런 후에 결제 수단을 물어봅니다.</p>	
						</li>
						<li>진행 대화
							<p class="center"><img src="../manualfiles/image168.png" /></p>
							<p>사용자가 결제 수단을 말하지 않은 경우 결제 수단을 반복적으로 물어봅니다.</p>
						</li>
						<li>응답 대화
							<ol>
								<li>inform_payment_type
									<p>결제 수단을 말하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>현금이요</p>
													<p>신용카드로 할께요</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때는 별다른 시스템 응답을 하지는 않고, 바로 다음 Task로 넘어갑니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="2_8append">Bye Task</h5>
				<div>
					<h6 id="2_8_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image169.png" /></p>
							<p>결제 수단을 확인하고 대화를 종료합니다. 이때 시스템 Intent를 bye로 설정하여 시스템 Intent가 bye일 때 대화가 종료되도록 합니다.</p>	
						</li>
					</ol>					
				</div>
				
				<!-- 3장 start -->
				<h4 id="3append">차량용 내비게이션 도메인</h4>
				<div>					
					<p>차량에서 내비게이션을 이용하는 상황을 시뮬레이션한 도메인입니다. 특정 지점 등록/삭제나 길찾기를 수행할 수 있습니다. 사용자가 종료를 말하면 대화가 종료됩니다. 시뮬레이션하기 위한 길찾기 DB는 샘플로 제한되어 있습니다.</p>					
				</div>		
				<h5 id="3_1append">대표 대화시나리오</h5>
				<div>
					<p>대표적인 대화시나리오는 아래와 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<p>system: 안녕하세요, 차량용 내비게이션입니다. 길찾기 또는 지점 등록/삭제가 가능합니다.</p>
									<p>user: 버거킹으로 가자</p>
									<p>system: 동작구 노량진동에 있는 버거킹 노량진점과 용산구 한강로2가에 있는 버거킹 국제점중 어디로 가시겠습니까?</p>
									<p>user: 노량진에 있는 곳으로</p>
									<p>system: 버거킹 노량진점으로 길안내를 시작합니다. 버거킹 노량진점까지의 거리는 3.01km이고, 약 8분정도 걸릴 예정입니다. 길찾기 또는 지점 등록/삭제가 가능합니다.</p>
									<p>user: 경로 취소</p>
									<p>system: 버거킹 노량진점으로의 경로를 취소합니다. 길찾기 또는 지점 등록/삭제가 가능합니다.</p>
									<p>user: 여의도역 근처에 있는 맥도날드로 가자</p>
									<p>system: 맥도날드 여의도IFC몰점으로 길안내를 시작합니다. 맥도날드 여의도IFC몰점까지의 거리는 6.17km이고, 약 15분정도 걸릴 예정입니다. 길찾기 또는 지점 등록/삭제가 가능합니다.</p>
									<p>user: 지점을 등록해줘</p>
									<p>system: 여기를 어떤 이름으로 등록할까요?</p>
									<p>user: 회사로</p>
									<p>system: 여기를 회사로 등록합니다. 길찾기 또는 지점 등록/삭제가 가능합니다.</p>
									<p>user: 내비게이션 종료</p>
									<p>system: 내비게이션을 종료합니다. 이용해 주셔서 감사합니다.</p>
								</td>
							</tr>
						</tbody>
					</table>
					<p>차량용 내비게이션 도메인에서는 크게 다음의 두 가지 기능이 제공됩니다.</p>
					<ol class="decimal_ol">
						<li>지점 등록/삭제: 현재 위치를 즐겨 부르는 이름으로 등록하거나, 등록되어 있는 지점을 삭제합니다.</li>
						<li>길찾기: 길찾기를 수행합니다. 검색 결과가 여러 개일 때 추가적인 요청을 통해 검색 결과내 재검색을 진행할 수 있습니다.</li>
					</ol>
				</div>
				<h5 id="3_2append">Task Graph 와 Entity</h5>
				<div>														
					<p>내비게이션 도메인의 Task Graph입니다.</p>
					<p class="center"><img src="../manualfiles/image170.png" /></p>
					<p>도메인에서 사용한 Task 정보입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="17%" />
							<col width="*" />
							<col width="17%" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>Task 이름</th>
								<th>수행 내용</th>
								<th>다음 Task</th>
								<th>이동 조건</th>
							</tr>
							<tr>
								<td class="center_td">Greet</td>
								<td>첫인사를 수햄함.</td>
								<td class="center_td">WaitOrder</td>
								<td>항상</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="3">WaitOrder</td>
								<td rowspan="3">대기 상태에 있음. 사용자의 요청에 따라 지점 등록/삭제 또는 길찾기 Task로 이동함.</td>
								<td class="center_td">EditLocation</td>
								<td>사용자가 지점 등록/삭제를 요청할 때</td>
							</tr>
							<tr>
								<td class="center_td">ManageRoute</td>
								<td>사용자가 길찾기를 요청할 때</td>
							</tr>
							<tr>								
								<td class="center_td">Bye</td>
								<td>사용자가 종료 관련 발화를 할 때</td>
							</tr>
							<tr>
								<td class="center_td">EditLocation</td>
								<td>지점 등록/삭제를 수행함.</td>
								<td class="center_td">WaitOrder</td>
								<td>지점 등록/삭제를 끝냈을 때</td>
							</tr>
							<tr>
								<td class="center_td">ManageRoute</td>
								<td>길찾기를 수행함.</td>
								<td class="center_td">WaitOrder</td>
								<td>길찾기를 끝냈을 때</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>끝인사 후 대화 종료.</td>
								<td class="center_td">END</td>
								<td>시스템이 끝인사를 수행했을 때</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="3_3append">Greet Task</h5>
				<div>
					<h6 id="3_3_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image171.png" /></p>
							<p>Task 진입 시, 첫인사를 수행 후 다음 Task로 넘어갑니다.</p>	
						</li>
					</ol>					
				</div>
				<h5 id="3_4append">WaitOrder Task</h5>
				<div>
					<h6 id="3_4_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image172.png" /></p>
							<p>Task 진입 시, 도메인에서 수행 가능한 기능에 대해 말합니다.</p>	
						</li>
						<li>응답 대화
							<ol>
								<li>add_my_loc
									<p>지점 등록을 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>지점을 등록해줘</p>
													<p>여기를 회사로 등록해줘</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 Task에서는 지점 등록 작업을 수행하지 않고, EditLocation Task로 이동 후 지점 등록 작업을 수행합니다.</p>
								</li>
								<li>del_my_loc
									<p>지점 삭제를 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>등록 지점 삭제</p>
													<p>우리집을 지점에서 삭제해줘</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 Task에서는 지점 삭제 작업을 수행하지 않고, EditLocation Task로 이동 후 지점 삭제 작업을 수행합니다.</p>
								</li>
								<li>inform_route
									<p>길찾기를 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>버거킹으로 가자</p>
													<p>여의도역 근처에 있는 맥도날드로 가자</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 Task에서는 길찾기 작업을 수행하지 않고, ManageRoute Task로 이동 후 길찾기 작업을 수행합니다.</p>
								</li>
								<li>cancel_route
									<p>찾은 경로를 취소하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>경로 취소</p>
													<p>안내 중지</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 Task에서는 경로 취소 작업을 수행하지 않고, ManageRoute Task로 이동 후 경로 취소 작업을 수행합니다.</p>
								</li>
								<li>bye
									<p>내비게이션을 종료하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>잘 가</p>
													<p>내비게이션 종료</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이 경우에는 별다른 시스템 응답을 하지 않고 Bye Task로 이동합니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="3_5append">EditLocation Task</h5>
				<div>
					<h6 id="3_5_1append">Entity</h6>					
					<h7>MyLocation Class</h7>
					<p class="center"><img src="../manualfiles/image173.png" /></p>
					<p>- MyLocation Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td">MyLocation</td>
								<td class="center_td">name</td>								
								<td>등록/삭제할 지점의 이름, Sys.number</td>
							</tr>
						</tbody>
					</table>
					<p>- MyLocation Class Instance DB</p>
					<p class="center"><img src="../manualfiles/image174.png" /></p>
					<p>“우리집”이라는 이름의 지점이 등록되어 있다고 가정한 상황입니다. 지점 등록 발화 시, “우리집”으로 등록하고자 하면 이미 등록되어 있다는 응답이 나옵니다. 다만, 이는 내부 DB를 사용해서 동작하는 것을 보여주기 위한 도메인이므로 사용자의 발화에 따라 항목이 추가/삭제되지는 않습니다.</p>
				</div>
				<div>
					<h6 id="3_5_2append">시스템 대화</h6>
					<ol>
						<li>응답 대화
							<ol>
								<li>add_my_loc
									<p>지점 등록을 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다. 여기서의 사용자 발화는 WaitOrder Task에서 지점 등록 발화를 했으나, 등록할 지점 이름을 말하지 않아서 사용자가 추가적으로 이를 말하는 경우입니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>우리집으로</p>
													<p>회사로</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image175.png" /></p>
									<p>첫번째 응답은 사용자가 등록할 지점의 이름을 말했고, 그 이름이 instance에 없는 경우입니다. 두번째 응답은 사용자가 등록할 지점의 이름을 말했으나, 이미 그 이름이 instance에 있는 경우입니다. 마지막 응답은 사용자가 지점 등록 발화를 하였으나 등록할 지점의 이름을 말하지 않은 경우이고, 이때는 시스템이 등록할 지점 이름을 물어봅니다.</p>
									<p class="center"><img src="../manualfiles/image176.png" /></p>
									<p>첫번째와 두번째 응답 후에는 관련된 Slot을 초기화합니다.</p>
								</li>
								<li>del_my_loc
									<p>지점 삭제를 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다. 여기서의 사용자 발화는 WaitOrder Task에서 지점 삭제 발화를 했으나, 삭제할 지점 이름을 말하지 않아서 사용자가 추가적으로 이를 말하는 경우입니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>우리집</p>
													<p>회사를 삭제해줘</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image177.png" /></p>
									<p>첫번째 응답은 사용자가 삭제할 지점의 이름을 말했고, 그 이름이 instance에 없는 경우입니다. 두번째 응답은 사용자가 삭제할 지점의 이름을 말했으나, 이미 그 이름이 instance에 있는 경우입니다. 마지막 응답은 사용자가 지점 삭제 발화를 하였으나 삭제할 지점의 이름을 말하지 않은 경우이고, 이때는 시스템이 삭제할 지점 이름을 물어봅니다.</p>
									<p class="center"><img src="../manualfiles/image176.png" /></p>
									<p>첫번째와 두번째 응답 후에는 관련된 Slot을 초기화합니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="3_6append">ManageRoute  Task</h5>
				<div>
					<h6 id="3_6_1append">Entity</h6>					
					<h7>SysVar Class</h7>
					<p class="center"><img src="../manualfiles/image178.png" /></p>
					<p>- SysVar Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td">SysVar</td>
								<td class="center_td">set_route</td>								
								<td>경로 설정이 되어있는지 여부, Sys.string, Instance와 연동 체크 해제<br>“true” 또는 “false”</td>
							</tr>
						</tbody>
					</table>				
					<h7>Address Class</h7>
					<p class="center"><img src="../manualfiles/image179.png" /></p>
					<p>- Address Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="7">SysVar</td>
								<td class="center_td">city</td>								
								<td>시, Sys.string<br>e.g. 서울특별시, 대전광역시</td>
							</tr>
							<tr>								
								<td class="center_td">gu</td>								
								<td>구, Sys.string<br>e.g. 용산구, 동작구
								</td>
							</tr>
							<tr>								
								<td class="center_td">dong</td>								
								<td>동, Sys.string<br>e.g. 노량진동, 여의도동</td>
							</tr>
							<tr>								
								<td class="center_td">num</td>								
								<td>번지, Sys.string<br>e.g. 40-999, 9-6</td>
							</tr>
							<tr>								
								<td class="center_td">road</td>								
								<td>도로명, Sys.string<br>e.g. 한강대로23길, 노량진로</td>
							</tr>
							<tr>								
								<td class="center_td">bdnum</td>								
								<td>건물번호, Sys.string<br>e.g. 55, 3</td>
							</tr>
						</tbody>
					</table>				
					<h7>Distance Class</h7>			
					<p class="center"><img src="../manualfiles/image180.png" /></p>
					<p>- Distance Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">Distance</td>
								<td class="center_td">dist</td>								
								<td>목적지까지의 거리 (km), Sys.number</td>
							</tr>
							<tr>								
								<td class="center_td">dist_le</td>								
								<td>거리 이하 (km), Sys.number, Instance와 연동 체크 해제<br>Instance에서 dist Slot의 값이 이 Slot의 값보다 낮은 항목들을 찾습니다.<br>e.g. dist_le = 3 이면, instance에서 dist <= 3 인 항목들을 찾습니다.</td>
							</tr>
						</tbody>
					</table>
					<p>- Distance.dist_le Slot 단어 정의</p>
					<p class="center"><img src="../manualfiles/image181.png" /></p>
					<p>오른쪽에 작성한 것과 같은 단어들이 Slot 값으로 잡혔을 때, 이 값이 왼쪽의 숫자로 바뀌도록 합니다.</p>
					<h7>Time Class</h7>			
					<p class="center"><img src="../manualfiles/image182.png" /></p>
					<p>- Time Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">Time</td>
								<td class="center_td">time</td>								
								<td>목적지까지의 소요 시간 (분), Sys.number</td>
							</tr>
							<tr>								
								<td class="center_td">time_le</td>								
								<td>시간 이하 (km), Sys.number, Instance와 연동 체크 해제<br>Instance에서 time Slot의 값이 이 Slot의 값보다 낮은 항목들을 찾습니다.<br>e.g. time_le = 3 이면, instance에서 time <= 3 인 항목들을 찾습니다.</td>
							</tr>
						</tbody>
					</table>
					<p>Time.time_le Slot 단어 정의</p>
					<p class="center"><img src="../manualfiles/image183.png" /></p>
					<p>오른쪽에 작성한 것과 같은 단어들이 Slot 값으로 잡혔을 때, 이 값이 왼쪽의 숫자로 바뀌도록 합니다.</p>
					<h7>POI Class</h7>			
					<p class="center"><img src="../manualfiles/image184.png" /></p>
					<p>- POI Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="5">POI</td>
								<td class="center_td">ref_name</td>								
								<td>정식 이름, Sys.string<br>e.g. 맥도날드 여의도점, 버거킹 노량진점</td>
							</tr>
							<tr>								
								<td class="center_td">name</td>								
								<td>부르는 이름, Sys.string<br>e.g. 맥도날드, 버거킹</td>
							</tr>
							<tr>								
								<td class="center_td">type</td>								
								<td>POI 종류, Sys.string<br>e.g. 역, 주민센터, 햄버거</td>
							</tr>
							<tr>								
								<td class="center_td">addr</td>								
								<td>주소 정보, Address</td>
							</tr>
							<tr>								
								<td class="center_td">info</td>								
								<td>POI 정보, Sys.string<br>e.g. 한강대로23길, 노량진로</td>
							</tr>
						</tbody>
					</table>
					<h7>Route  Class</h7>			
					<p class="center"><img src="../manualfiles/image185.png" /></p>
					<p>- Route Class의 Slot 설명</p>		
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="9">Route</td>
								<td class="center_td">idx</td>								
								<td>경로 인덱스, Sys.string</td>
							</tr>
							<tr>								
								<td class="center_td">dest</td>								
								<td>목적지, POI</td>
							</tr>
							<tr>								
								<td class="center_td">lm</td>								
								<td>랜드마크, POI</td>
							</tr>
							<tr>								
								<td class="center_td">dist</td>								
								<td>목적지까지의 거리 정보, Distance</td>
							</tr>
							<tr>								
								<td class="center_td">time</td>								
								<td>목적지까지의 소요 시간 정보, Time</td>
							</tr>
							<tr>								
								<td class="center_td">lm_dist</td>								
								<td>랜드마크에서 목적지까지의 거리 정보, Distance</td>
							</tr>
							<tr>								
								<td class="center_td">lm_time</td>								
								<td>랜드마크에서 목적지까지의 소요 시간 정보, Time</td>
							</tr>
							<tr>								
								<td class="center_td">traffic</td>								
								<td>교통 상황, Sys.string<br>e.g. 원활, 서행</td>
							</tr>
							<tr>								
								<td class="center_td">has_lm</td>								
								<td>랜드마크 존재 여부, Sys.string<br>“true” 또는 “false”</td>
							</tr>
						</tbody>
					</table>
					<p>- Route Class Instance DB</p>
					<p class="center"><img src="../manualfiles/image186.png" /></p>
					<p>Route Class instance에 작성된 경로들만 정보 제공이 가능합니다. 더 많은 경로 정보를 제공하기 위해서는 관련된 정보를 추가적으로 작성해주어야 합니다.</p>
					<p>위에 작성한 Class들간의 계층 관계는 아래와 같습니다. 왼쪽으로 갈수록 상위 계층의 Class입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
							<col width="*" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="center">
								<td rowspan="3">Route</td>
								<td rowspan="3">POI</td>
								<td>Address</td>
							</tr>
							<tr class="center">
								<td>Distance</td>
							</tr>
							<tr class="center">
								<td>Time</td>
							</tr>
						</tbody>
					</table>
					<p>위와 같이 Class를 구성한 이유는 크게 다음의 두 가지입니다.</p>
					<ol class="decimal_ol">
						<li>Class 재활용
							<p>하나의 경로는 여러 개의 POI로 구성될 수 있습니다. 본 도메인에서는 “~ 근처에 있는”과 같은 발화를 처리하기 위해 랜드마크를 두어서 하나의 경로에 두 개의 POI가 존재합니다. 또한, 주소, 거리, 시간과 같은 경우는 POI마다 필요합니다. 이와 같은 경우, 같은 Slot을 Class마다 여러 개 만들어야하는 수고를 덜기 위해 위와 같이 Class를 계층적으로 구성 후 재활용하였습니다.</p>
						</li>
						<li>중의적인 표현에 대한 태깅
							<p>사용자가 “용산역으로 가자”라고 했을 때, 목적지는 용산역입니다. 하지만 사용자가 “용산역”이라고만 말한 경우 이것이 목적지인지 경유지인지 (경유지가 있는 경우) 이 발화만 가지고는 알 수 없습니다. 이럴 때는 “용산역”을 Route보다 낮은 계층인 POI로 태깅하면, 대화 관리 모듈에서 문맥에 따라 목적지 또는 경유지로 Slot 값을 채워 넣습니다.</p>
						</li>
					</ol>
				</div>
				<div>
					<h6 id="3_6_2append">시스템 대화</h6>
					<ol>
						<li>응답 대화
							<ol>
								<li>inform_route
									<p>길찾기를 수행하는 Intent입니다. 예시 사용자 발화는 아래와 같습니다. 여기서의 사용자 발화는 WaitOrder Task에서 길찾기 발화를 했으나, 검색 결과가 두 개 이상이라서 사용자가 추가적인 정보를 말하는 경우입니다.</p>
									<table summary="">
										<caption class="hide"></caption>
										<colgroup>
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<td>
													<p>노량진에 있는 곳으로</p>
													<p>10분안에 갈 수 있는 곳으로</p>
												</td>
											</tr>
										</tbody>
									</table>
									<p>이때 정의된 응답 목록은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image187.png" /></p>
									<p class="center"><img src="../manualfiles/image188.png" /></p>
									<p>검색 결과가 없는 경우입니다. 이 경우 본 샘플 도메인에서 안내 가능한 경로들이 무엇들인지 확인할 수 있도록 하고, 일부 Slot들을 초기화합니다.</p>
									<p class="center"><img src="../manualfiles/image189.png" /></p>
									<p class="center"><img src="../manualfiles/image190.png" /></p>
									<p>검색 결과가 하나인 경우입니다. 이 경우 목적지를 안내하고, 경로 설정 여부를 나타내는 Slot의 값을 “true”로 설정합니다.</p>
									<p class="center"><img src="../manualfiles/image191.png" /></p>
									<p>검색 결과가 각각 두 개, 두 개 이상인 경우입니다. 이 경우 현재 조건에 맞는 항목들 중 두 개만 알려주고 사용자에게 목적지에 대한 추가적인 정보를 문의합니다.</p>
								</li>
								<li>cancel_route
									<p>찾은 경로를 취소하는 Intent입니다. 여기서는 WaitOrder Task의 cancel_route Intent에 대한 시스템 응답만을 정의합니다. 이때 정의된 응답 목록은 아래와 같습니다.</p>
									<p class="center"><img src="../manualfiles/image192.png" /></p>
									<p class="center"><img src="../manualfiles/image193.png" /></p>
									<p>사용자 발화의 Intent가 경로 취소 Intent일 때, 경로 안내 중이었으면, 경로 취소를 알리고 길찾기와 관련된 Slot들을 초기화합니다.</p>
									<p class="center"><img src="../manualfiles/image194.png" /></p>
									<p>사용자 발화의 Intent가 경로 취소 Intent이나, 경로 안내 중이 아니었을 경우입니다.</p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="3_7append">Bye Task</h5>
				<div>
					<h6 id="3_7_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image195.png" /></p>
							<p>끝인사 후 대화를 종료합니다. 이때 시스템 Intent를 bye로 설정하여 시스템 Intent가 bye일 때 대화가 종료되도록 합니다.</p>
						</li>
					</ol>					
				</div>
				
				<!-- 4장 start -->
				<h4 id="4append">가구 쇼핑 도메인</h4>
				<div>					
					<p>가구 쇼핑 도메인은 가구를 주문하는 상황을 시뮬레이션한 도메인입니다. 고객이 원하는 조건으로 가구를 추천해주고 사용자의 구매를 돕습니다. 가구 쇼핑 도메인에서 제공하는 가구 상품 정보는 내부 DB 에서 검색하기에 예로 검색되고 있습니다. 사용자가 원하는 조건은 가구 종류, 브랜드, 색상, 가격대로 정의하였고, 이를 기준으로 검색한 상품 중 2개를 안내합니다. 이때, 가구 상품 아이디, 상품명, 설명, 브랜드, 가격, 색상 정보를 제공합니다. 추천해 준 상품을 토대로 고객이 구매를 결정하면 결제 수단을 물어본 다음 주문을 접수합니다. 이때, 고객이 추가 주문을 원하면 다시 추천 및 주문을 진행하고, 종료를 원하면 대화가 종료됩니다.</p>					
				</div>		
				<h5 id="4_1append">대표 대화시나리오</h5>
				<div>
					<p>대표적인 대화시나리오는 아래와 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<p>system: 안녕하세요, 가구 주문 서비스입니다. ^^ 주문하고 싶으신 가구의 조건을 말씀해주세요~ (종류, 브랜드, 가격대, 색상)</p>
									<p>user: 수납장을 찾고 있어요</p>
									<p>system: 어떤 브랜드가 좋으신가요? (e.g. 이케아, 이룸, 동서 가구)</p>
									<p>user: 이케아가 좋아요</p>
									<p>system: 어느 가격대를 원하시나요? (보통, 저가, 고가)</p>
									<p>user: 상관없어요</p>
									<p>system: 네~ 어떤 색상으로 원하시나요? (e.g. 검정색, 흰색, 갈색)</p>
									<p>user: 상관없어요</p>
									<p>system: 네~ 말씀하신 조건으로 찾아드릴께요~ 검색 결과 8개의 상품을 찾았습니다. 아래의 상품들 중 원하시는 제품의 아이디를 말씀해주세요. </p>
									<p>제품 아이디 4.0 | 수납장4 상품 : 이케아 브랜드. 수납장4 설명. 가격 140000.0원. 색상 갈색, </p>
									<p>제품 아이디 5.0 | 수납장5 상품 : 이케아 브랜드. 수납장5 설명. 가격 100000.0원. 색상 갈색</p>
									<p>user: 더 저렴한 것은 없나요</p>
									<p>system: 말씀하신 조건으로 찾아드릴께요~ 검색 결과 3개의 상품을 찾았습니다. 아래의 상품들 중 원하시는 제품의 아이디를 말씀해주세요. </p>
									<p>제품 아이디 5.0 | 수납장5 상품 : 이케아 브랜드. 수납장5 설명. 가격 100000.0원. 색상 갈색, </p>
									<p>제품 아이디 7.0 | 수납장7 상품 : 이케아 브랜드. 수납장7 설명. 가격 100000.0원. 색상 검정색</p>
									<p>user: 7번으로 주세요</p>
									<p>system: 7.0번 수납장7 제품 100000.0원을 선택하셨습니다. 주문하시겠어요?</p>
									<p>user: 네</p>
									<p>system: 수납장7 상품으로 주문하셨고, 결제 금액은 100000.0원 입니다.</p>
									<p>user: 카드 되나요</p>
									<p>system: 네, 카드로 정상 결제되었습니다. 더 필요한 것이 있으신가요?</p>
									<p>user: 괜찮습니다</p>
									<p>system: 감사합니다, 또 오세요 ^^.</p>
									<p>-대화가 종료되었습니다.-</p>
								</td>
							</tr>
						</tbody>
					</table>
					<p>위의 대화시나리오는 크게 아래의 4단계로 구성되어 있습니다.</p>
					<ol class="decimal_ol">
						<li>우선 상품의 추천 조건을 입력 받습니다. 여기서 시스템은 가구 종류, 브랜드, 색상, 가격대를 물어봅니다</li>
						<li>시스템은 사용자로부터 입력 받은 추천 조건에 맞는 상품을 검색하고, 사용자에게 상품 아이디, 상품명, 상품 설명, 브랜드, 가격, 색상 정보를 제공합니다.</li>
						<li>사용자가 구매를 결정하면 사용자에게 결제 금액을 알려주고 결제 수단을 물어봅니다.</li>
						<li>입력 받은 결제 수단을 알려주고 사용자가 추가 주문을 원할 시 추천 조건 입력 과정부터 다시 시작하고, 추가 주문을 원하지 않을 시 대화를 종료합니다.</li>
					</ol>
				</div>
				<h5 id="4_2append">Task Graph</h5>
				<div>														
					<p>가구 쇼핑 도메인은 7개의 Task로 구성되어 있고, Task Graph는 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image196.png" /></p>
					<p>도메인에서 사용한 Task 정보입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="17%" />
							<col width="*" />
							<col width="17%" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>Task 이름</th>
								<th>수행 내용</th>
								<th>다음 Task</th>
								<th>이동 조건</th>
							</tr>
							<tr>
								<td class="center_td">Greet</td>
								<td>인사말로 대화를 시작합니다.</td>
								<td class="center_td">Get_product_condition</td>
								<td>항상 이동합니다. (true)</td>
							</tr>
							<tr>
								<td class="center_td">Get_product_condition</td>
								<td>원하는 상품의 검색 조건 정보를 수집합니다. 이때, 검색 조건은 가구 종류, 브랜드, 가격대, 색상으로 정의합니다.</td>
								<td class="center_td">Recommend</td>
								<td>4개의 검색 조건을 사용자가 모두 발화한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Recommend</td>
								<td>검색 조건을 확인하고 검색을 진행합니다. 사용자가 원하는 검색 조건이 바뀌었을 때, 되돌아오는 Task 역할을 합니다. </td>
								<td class="center_td">Order</td>
								<td>시스템 의도로 검색을 수행한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="3">Order</td>
								<td rowspan="3">검색 결과 중 2개의 상품을 추천하고 사용자의 구매 결정 여부를 묻습니다. </td>
								<td class="center_td">Recommend</td>
								<td>사용자가 검색 조건을 추가 또는 변경한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Get_payment_info</td>
								<td>사용자가 선택한 상품을 확인한 후 사용자가 구매하기로 결정한 경우 이동합니다.</td>
							</tr>
							<tr>								
								<td class="center_td">Get_product_condition</td>
								<td>추천 조건에 의한 검색 결과가 없는 경우 다시 조건을 묻기 위해 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">Get_payment_info</td>
								<td rowspan="2">주문한 상품의 정보와 가격 정보를 안내한 후 결제 수단 정보를 묻습니다.</td>
								<td class="center_td">Bye</td>
								<td>결제가 완료된 후 사용자가 추가 주문을 원하지 않는 경우 이동합니다.</td>
							</tr>
							<tr>								
								<td class="center_td">Get_product_condition</td>
								<td>결제가 완료된 후 사용자가 추가 주문을 원하는 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>끝인사</td>
								<td class="center_td">END</td>
								<td>시스템이 끝인사를 수행했을 때</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="4_3append">Greet Task</h5>
				<div>
					<h6 id="4_3_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image197.png" /></p>
							<p>Task 진입 시, 위와 같은 첫인사를 수행합니다. 이후 바로 다음 Task로 넘어갑니다.</p>	
						</li>
					</ol>					
				</div>
				<h5 id="4_4append">Get_product_condition Task </h5>
				<div>					
					<p>Get_product_condition Task는 사용자(고객)이 원하는 상품의 검색 조건 정보를 수집합니다. 본 도메인에서는 검색 조건을 가구 종류, 가구 브랜드, 가격대, 가구 색상으로 설정했습니다. 본 Task에서는 사용자에게 Slot 값으로 검색 조건을 질문합니다.</p>
					<p>제품 추천 결과가 없는 경우와 사용자가 재 주문을 원하는 경우에는 다른 Task로 이동해서 해당 Task를 다시 시작합니다. 이 때, 대화 도메인내에서 사용되는 Class의 Slot 값들을 초기화해주어야 대화 진행이 가능합니다. Task의 Property에서 Reset Slots으로 전체 Slot 또는 대화에서 사용자 발화와 관련된 Slot들을 초기화합니다. 유의할 점으로, Reset Slots의 작성란은 300자로 제한하고 있습니다. </p>
					<p class="center"><img src="../manualfiles/image197_1.png" /></p>
					<p class="center"><img src="../manualfiles/image197_2.png" /></p>								
				</div>
				<div>
					<h6 id="4_4_1append">Entity</h6>										
					<p>본 Task에서는 가구 정보를 사용하기 위해 가구 개념을 Class로 사용합니다. Product Class에 제품(가구)의 속성을 11개의 Slot으로 아래의 표와 같이 정의합니다. </p>
					<p class="center"><img src="../manualfiles/image198.png" /></p>					
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="11">Product</td>
								<td class="center_td">idx</td>								
								<td>상품 아이디</td>
							</tr>
							<tr>								
								<td class="center_td">type</td>								
								<td>가구 종류. 사용자가 반드시 발화해야하는 검색 조건</td>
							</tr>
							<tr>								
								<td class="center_td">name</td>								
								<td>상품명 </td>
							</tr>
							<tr>								
								<td class="center_td">description</td>								
								<td>상품 설명 </td>
							</tr>
							<tr>								
								<td class="center_td">price</td>								
								<td>상품의 가격</td>
							</tr>
							<tr>								
								<td class="center_td">brand</td>								
								<td>상품의 브랜드, 사용자가 발화할 검색 조건</td>
							</tr>
							<tr>								
								<td class="center_td">color</td>								
								<td>상품의 색상, 사용자가 발화할 검색 조건</td>
							</tr>
							<tr>								
								<td class="center_td">price_range</td>								
								<td>상품의 가격대, 사용자가 발화할 검색 조건</td>
							</tr>
							<tr>								
								<td class="center_td">brand_user_set</td>								
								<td>사용자가 brand 조건을 발화했는지 여부 정보<br>발화한 경우: “yes”<br>발화하지 않은 경우 (e.g. “상관없어요”) : “dontcare”</td>
							</tr>
							<tr>								
								<td class="center_td">color_user_set</td>								
								<td>사용자가 color 조건을 발화했는지 여부 정보<br>발화한 경우: “yes”<br>발화하지 않은 경우 (e.g. “상관없어요”) : “dontcare”</td>
							</tr>
							<tr>								
								<td class="center_td">price_range_user_set</td>								
								<td>사용자가 price_range 조건을 발화했는지 여부 정보<br>발화한 경우: “yes”<br>발화하지 않은 경우 (e.g. “상관없어요”) : “dontcare”</td>
							</tr>							
						</tbody>
					</table>
					<p>Product Class에서 살펴보아야 할 Slot은 검색 조건인 brand, color, price_range와 사용자가 발화하지 않은 경우(e.g. “상관없어요”)를 고려하기 위한 brand_user_set, color_user_set, price_range_user_set 입니다. 사용자가 검색 조건 Slot값을 발화한 경우, 해당 Slot의 사용자 발화 여부 Slot 값을 “yes”로 정의합니다. 예를 들어, 사용자가 검색 조건으로 특정 brand를 말했을 때, brand Slot의 사용자 발화 여부 Slot인 brand_user_set을 “yes”로 설정합니다. 이를 위해 아래의 그림과 같이 Slot value relationship을 추가해줍니다. 사용자가 검색 조건을 발화하지 않은 경우에는 Slot 값을 “dontcare”로 설정합니다. </p>
					<p class="center"><img src="../manualfiles/image199.png" /></p>
					<p>Product Class Instance DB는 다음과 같이 들어가 있습니다. </p>
					<p class="center"><img src="../manualfiles/image200.png" /></p>
					<p>Product Class에서 brand_user_set, color_user_set, price_range_user_set는 사용자가 Slot값을 발화했는지 알기 위한 Slot이므로 Class의 Instance DB와 연동하지 않습니다. </p>
					<p class="center"><img src="../manualfiles/image201.png" /></p>
					<p>사용자가 발화하는 Slot은 Product.type, Product.brand, Product.color, Product.price_range이고 각 Slot 단어는 다음과 같이 정의합니다. </p>
					<p class="center"><img src="../manualfiles/image201_1.png" /></p>
					<p class="center"><img src="../manualfiles/image201_2.png" /></p>
					<p class="center"><img src="../manualfiles/image201_3.png" /></p>
					<p class="center"><img src="../manualfiles/image201_4.png" /></p>
				</div>
				<div>
					<h6 id="4_4_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image202.png" /></p>
							<p>Get_product_condition Task 시작 시, 첫 시스템 대화입니다.</p>	
						</li>
						<li>진행 대화
							<p>상품 검색 및 추천에 필요한 조건 4개 상품 종류(Product.type), 브랜드(Product.brand), 가격대(Product.price_range), 색상(Product.color)를 문의합니다. 진행 대화이기에 사용자가 발화하지 않은 경우에만 출력합니다. </p>
							<p class="center"><img src="../manualfiles/image203.png" /></p>
							<p class="center"><img src="../manualfiles/image204.png" /></p>
							<p>유의할 점은 상품 종류를 제외한 브랜드, 가격대, 색상 Slot값을 질문할 때, 대상슬롯과 채우고자 하는 Slot의 값이 다름을 위의 그림에서 볼 수 있습니다. 이는 사용자가 특정 Slot 값을 말하지 않았을 때를 위한 경우입니다. brand Slot에 대하여 예를 들겠습니다. Product.brand_user_set Slot이 채워지지 않았다는 것은 사용자가 brand Slot을 발화하지 않은 경우입니다. 미리 정의한 brand Slot의 relationship에 의해 사용자가 발화했을 경우, “yes”로 채워지므로 시스템은 더 이상 brand Slot을 질문하지 않습니다. </p>	
						</li>
						<li>응답 대화
							<p>Get_product_condition Task에 정의된 사용자 발화 Intent 목록입니다.</p>
							<p class="center"><img src="../manualfiles/image205.png" /></p>
							<ol>
								<li>inform_product_info
									<p>검색 조건을 말하는 사용자의 발화 의도로 Slot Filling을 위함입니다. 사용자 발화에 4가지 조건 Slot을 넣어서 발화합니다.</p>
									<p class="center"><img src="../manualfiles/image206.png" /></p>
								</li>
								<li>dontcare
									<p>사용자가 검색 조건 Slot을 특정 값으로 발화하지 않는 사용자 발화 의도입니다. 다음과 같이 발화합니다.</p>									
									<p class="center"><img src="../manualfiles/image207.png" /></p>
									<p>여기서는 dontcare Intent의 직전 시스템 발화 의도(2턴 전 시스템 발화 의도)에 따라 시스템 응답을 다르게 정의합니다. </p>
									<p class="center"><img src="../manualfiles/image208.png" /></p>
									<p>2턴 전 시스템 Intent에 따라 시스템 응답을 따로 정의하는 이유는 사용자가 “상관 없다”고 말한 특정 Slot을 구별하기 위함입니다. 위의 시스템 발화 후에는 검색 조건 Slot의 값이 특정 값으로 발화되지는 않았지만, 사용자가 “상관없다”고 언급을 했다는 것을 표시하기 위해 해당 검색 조건 Slot의 발화 여부를 표시하는 Slot에 “dontcare” 값을 설정합니다. 설정 방법은 아래의 그림과 같고, 이렇게 설정하면 해당 값을 다시 묻지 않습니다.</p>
									<p class="center"><img src="../manualfiles/image209.png" /></p>
								</li>
							</ol>						
						</li>
					</ol>
				</div>
				<h5 id="4_5append">Recommend Task </h5>
				<div>					
					<p>Recommend Task는 상품 검색 조건을 찾는 Get_product_condition Task와 상품을 추천하는 Order Task의 중간에서 상품 검색 조건을 확인하고 검색을 진행하는 역할을 합니다. Recommend Task는 Order Task에서 상품 검색 조건이 바뀌거나 추가 되었을 때, 이 Task로 이동해오면서 새로운 조건으로 검색하기 위함입니다.</p>								
				</div>
				<div>
					<h6 id="4_5_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image210.png" /></p>
							<p>Task 진입 시, 위와 같은 시스템 발화를 수행합니다. 이때, 사용자가 발화한 조건들을 안내하는 발화를 작성해도 됩니다. 이후 바로 다음 Task로 넘어갑니다.</p>	
						</li>
					</ol>
				</div>
				<h5 id="4_6append">Order Task </h5>
				<div>					
					<p>Order Task에서는 검색 조건을 기반으로 상품을 추천하고 사용자의 구매 결정 여부를 묻습니다. Task를 시작할 때, 추천하는 상품을 안내하고 사용자의 발화 의도(답변 유형)에 따라 구매 결정 여부 질문, 상품 재 검색, 재 추천을 수행합니다. </p>								
				</div>
				<div>
					<h6 id="4_6_1append">Entity</h6>
					<p>Order Task에서 사용하는 Class는 Product Class입니다. 이 Task에서는 시스템 발화를 할 때, 채워진 Slot 값으로 Instance DB에서 검색한 결과를 사용합니다. </p>
				</div>
				<div>
					<h6 id="4_6_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p>Task가 시작될 때, 세가지 경우에 따라 아래와 같이 발화합니다. </p>
							<ol>
								<li>검색 결과가 1개 이상인 경우, 추천 상품 중 최대 2개를 안내합니다.
									<p class="center"><img src="../manualfiles/image211.png" /></p>	
								</li>
								<li>검색 결과가 1개 이상이고, 기존에 추천한 상품이 사용자가 마음에 들지 않은 경우(0턴 전 사용자 발화 의도가 anythingelse인 경우), 다시 추천합니다. 역시 검색된 추천 상품 중 최대 2개를 안내합니다.
									<p class="center"><img src="../manualfiles/image212.png" /></p>
								</li>
								<li>검색 결과가 0개인 경우, 추천할 상품이 없음을 안내하고 검색 조건 묻는 Task(Get_product_condition)부터 다시 시작함을 말합니다. 시스템 Intent를 no_product로 설정하여 Task 이동 조건에서 쓰이도록 합니다. 
									<p class="center"><img src="../manualfiles/image213.png" /></p>
								</li>
							</ol>	
						</li>
						<li>응답 대화
							<p>본 Task에서 시스템의 추천 상품 안내에 대해 상품을 선택하기 위한 사용자의 발화 의도는 다음과 같이 3가지로 정의합니다.</p>							
							<ol>
								<li>inform_product_id
									<p>시스템이 추천한 상품들 중 사용자가 원하는 상품을 지칭하는 의도입니다. 사용자 발화는 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image214.png" /></p>
									<p>해당 의도에 대한 시스템 응답은 3가지 유형으로 정의합니다. </p>
									<p>- 사용자가 제품을 잘 선택한 경우</p>
									<p>- 잘못된 제품 아이디로 선택한 경우</p>
									<p>- 복수개의 제품 중 특정 하나를 지칭하지 않은 경우</p>
									<p>사용자가 제품을 잘 선택한 경우는 Slot 값들이 Instance DB내의 하나의 예제에 맞은 경우로, 다음과 같이 발화합니다.</p>
									<p class="center"><img src="../manualfiles/image215.png" /></p>
									<p>사용자가 잘못된 제품 아이디로 선택한 경우, 추천 조건으로 Instance DB에서 검색한 결과의 객체들과 idx Slot 값이 맞지 않으므로 시스템이 발화할 수 없습니다. 그러므로 해당 경우 사용자가 제품 아이디를 다시 발화하도록 유도합니다.</p>
									<p class="center"><img src="../manualfiles/image216.png" /></p>
									<p>복수개의 제품 중 특정 제품의 아이디를 지칭하지 않은 경우, 시스템이 발화할 수 있는 예제가 복수가 되므로 다음과 같이 특정 제품의 아이디를 발화하도록 유도합니다. </p>
									<p class="center"><img src="../manualfiles/image217.png" /></p>
								</li>
								<li>plus_condition
									<p>사용자가 추천 조건을 변경하는 발화 의도입니다. 해당 발화를 하면 Recommend Task로 다시 돌아가 재 검색을 하게 됩니다. 사용자 발화는 다음과 같습니다. Product.brand, Product.color도 같은 방법으로 추천 조건을 변경하는 발화를 작성할 수 있습니다.</p>									
									<p class="center"><img src="../manualfiles/image218.png" /></p>
								</li>
								<li>anythingelse
									<p>사용자가 시스템이 추천한 제품이 마음에 들지 않을 때 재 추천을 요구하는 발화 의도입니다. 사용자 발화는 다음과 같습니다.</p>									
									<p class="center"><img src="../manualfiles/image219.png" /></p>
									<p>해당 시스템을 발화하면 같은 Task에서 재 검색을 합니다. </p>
								</li>
							</ol>						
						</li>
					</ol>
				</div>
				<h5 id="4_7append">Get_product_condition Task </h5>
				<div>					
					<p>Get_payment_info Task는 Get_product_condition Task에서 선택한 상품을 주문 및 결제를 합니다. 이를 위해 결제 수단 정보를 요청하고, 결제 완료 후 추가 주문 여부를 묻습니다. </p>								
				</div>
				<div>
					<h6 id="4_7_1append">Entity</h6>										
					<p>Get_payment_info Task에서는 Product Class와 Order Class를 사용합니다. Product Class의 상품명과 가격 정보를 사용하고, 주문 정보를 사용하기 위해 주문서 개념을 Order Class로 사용합니다. 해당 Class(주문서)의 속성을 아래의 표와 같이 정의합니다.</p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="3">Order</td>
								<td class="center_td">product_name</td>								
								<td>주문 제품 이름</td>
							</tr>
							<tr>								
								<td class="center_td">total_price</td>								
								<td>주문 금액</td>
							</tr>
							<tr>								
								<td class="center_td">payment</td>								
								<td>결제 수단</td>
							</tr>							
						</tbody>
					</table>
					<p class="center"><img src="../manualfiles/image220.png" /></p>
					<p>이 Task에서 결제 수단을 Order.payment Slot으로 질문하여 사용자에게 Slot Filling을 요청합니다. Order.payment의 Slot 단어는 다음과 같이 정의합니다.</p>
					<p class="center"><img src="../manualfiles/image220_1.png" /></p>
				</div>
				<div>
					<h6 id="4_7_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p>Task가 시작될 때, 시스템은 다음과 같이 발화합니다.</p>
							<p class="center"><img src="../manualfiles/image221.png" /></p>
							<p>주문 상품과 금액이 정해졌음으로 시스템 발화 후, 다음과 같이 Order.product_name을 Product.name값으로, Order.total_price를 Product.price 값으로 채웁니다. </p>
							<p class="center"><img src="../manualfiles/image222.png" /></p>
						</li>
						<li>진행 대화
							<p>사용자에게 결제 수단을 질문하여 Slot 값을 채우기 위해 아래와 같이 시스템 발화를 작성합니다.</p>
							<p class="center"><img src="../manualfiles/image223.png" /></p>	
						</li>
						<li>응답 대화
							<p>본 Task는 사용자가 선택한 상품을 주문 및 결제하고, 추가 주문 여부를 묻습니다. 그래서 사용자의 발화 의도로 결제 수단 묻기와 추가 주문 여부에 대한 대답을 정의합니다.</p>							
							<ol>
								<li>inform_payment
									<p>결제 수단을 말하는 사용자의 발화 의도로 Slot Filling을 위함입니다. 사용자 발화는 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image224.png" /></p>
									<p>시스템은 다음과 같이 결제 완료를 안내하고 추가 주문 여부를 질문합니다. 시스템 Intent는 payment_done으로 설정하여 Task 이동 조건에 사용합니다.</p>
									<p class="center"><img src="../manualfiles/image225.png" /></p>
								</li>
								<li>need_more_order
									<p>추가 주문을 원하는 사용자의 발화 의도입니다. 사용자 발화는 다음과 같이 작성합니다.</p>									
									<p class="center"><img src="../manualfiles/image226.png" /></p>
									<p>시스템의 추가 주문 여부 질문에 대해 사용자가 긍정으로 답하는 경우(affirm Intent)와 해당 Intent로 말하는 경우, Get_product_condition Task로 이동합니다. </p>
								</li>
								<li>no_thanks
									<p>추가 주문을 원하지 않는 사용자의 발화 의도입니다. 사용자 발화는 다음과 같이 작성합니다. 시스템의 추가 주문 여부 질문에 사용자가 부정으로 답하는 경우(negate Intent)와 해당 intent로 말하는 경우, Bye Task로 이동합니다.</p>									
									<p class="center"><img src="../manualfiles/image227.png" /></p>
								</li>
							</ol>						
						</li>
					</ol>
				</div>
				<h5 id="4_8append">Bye Task </h5>
				<div>					
					<p>Bye Task는 Order Task에서 사용자의 추가 주문 필요 여부에 부정일 경우 이동해옵니다. </p>								
				</div>
				<div>
					<h6 id="4_8_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image228.png" /></p>
							<p>사용자의 추가 주문 필요 여부가 부정이므로 대화를 종료합니다. 이때 시스템 Intent를 bye로 설정하여 END Task에서 이전 시스템 Intent가 bye일 때 대화가 종료되도록 합니다.</p>	
						</li>
					</ol>
				</div>
				
				<!-- 5장 start -->
				<h4 id="5append">환율 도메인</h4>
				<div>					
					<p>환율 도메인은 환율 정보를 제공하는 대화 도메인입니다. 해당 도메인에서는 특정 국가의 환율(매매기준율)과 두 국가간의 환율 변환 정보를 제공합니다. </p>					
				</div>		
				<h5 id="5_1append">대표 대화시나리오</h5>
				<div>
					<p>해당 도메인은 환율 및 변환 정보를 묻고 답하는 방식으로 대화를 구성되었습니다. 사용자 발화에 국가명 또는 통화명이 1개인 경우엔 환율 정보, 2개인 경우 또는 환율 변환 금액 정보가 포함되는 경우에는 환율 변환 정보를 묻는 것으로 설정하였습니다. 해당 도메인의 실제 구동 예시는 다음과 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<p>system: 안녕하세요, 환율 정보 조회 서비스입니다. (종료를 원하시면, '그만'을 말해주세요.) 무엇이 궁금하신가요?</p>
									<p>user: 엔화 얼마야</p>
									<p>system: 일본 환율 정보입니다. 매매기준율 951.0으로, 100 옌 당 951.0원입니다. 무엇이 궁금하신가요?</p>
									<p>user: 중국 환율 알려줘</p>
									<p>system: 중국 환율 정보입니다. 매매기준율 163.65으로, 1 위안화당 163.65원입니다. 무엇이 궁금하신가요?</p>
									<p>user: 달러 얼마야</p>
									<p>system: 어느 나라의 환율 정보를 원하시나요?</p>
									<p>user: 미국</p>
									<p>system: 미국 환율 정보입니다. 매매기준율 1066.9으로, 1 달러당 1066.9원입니다. 무엇이 궁금하신가요?</p>
									<p>user: 오늘 환율 변환하면 어떠려나</p>
									<p>system: 환율 변환의 기준 국가를 어디로 하시겠어요? 기준 국가는 A국가에서 B국가로 변환할 때 'A' 국가입니다.</p>
									<p>user: 미국</p>
									<p>system: 환율 변환의 대상 국가를 어디로 하시겠어요? 대상 국가는 A국가에서 B국가로 변환할 때 'B' 국가입니다.</p>
									<p>user: 중국</p>
									<p>system: 환율 변환할 기준 금액을 입력해주세요.</p>
									<p>user: 100달러</p>
									<p>system: 100 달러는 651.94015533476 위안화입니다. 무엇이 궁금하신가요?</p>
									<p>user: 고마워요</p>
									<p>system: 네, 환율 정보 조회 서비스를 이용해주셔서 감사합니다. *^^*</p>
									<p>-대화가 종료되었습니다.-</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="5_2append">Task Graph</h5>
				<div>														
					<p>해당 도메인은 5개의 Task로 구성되며, 아래의 Task graph와 같은 순서로 대화가 구동됩니다.</p>
					<p class="center"><img src="../manualfiles/image229.png" /></p>
					<p>각 Task의 이름과 역할, 다음 Task와 이동 조건은 아래의 표와 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="17%" />
							<col width="*" />
							<col width="17%" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>Task 이름</th>
								<th>수행 내용</th>
								<th>다음 Task</th>
								<th>이동 조건</th>
							</tr>
							<tr>
								<td class="center_td">Greet</td>
								<td>인사말로 대화를 시작합니다.</td>
								<td class="center_td">ExchangeRateInfo</td>
								<td>항상 이동합니다. (true)</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="3">Get_ExchangeRate_Info</td>
								<td rowspan="3">환율 및 환율 변환 검색에 필요한 정보를 수집하고 질문 유형을 분류합니다.</td>
								<td class="center_td">ExchangeRate_Info</td>
								<td>사용자가 환율 정보를 질문한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">ExchangeRate_Converter</td>
								<td>사용자가 환율 변환 정보를 질문한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>사용자가 대화 종료를 원할 때 이동합니다.</td>
							</tr>
							
							<tr>
								<td class="center_td" rowspan="2">ExchangeRate_Info</td>
								<td rowspan="2">환율 정보를 검색하는데 필요한 정보를 추가로 묻고, 검색 결과를 제공합니다.</td>
								<td class="center_td">Get_ExchangeRate_Info</td>
								<td>환율 정보 제공을 완료한 경우 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>사용자가 대화 종료를 원할 때 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">ExchangeRate_Converter</td>
								<td rowspan="2">환율 변환 정보를 검색하는데 필요한 정보를 추가로 묻고, 검색 결과를 제공합니다.</td>
								<td class="center_td">Get_ExchangeRate_Info</td>
								<td>환율 변환 정보 제공을 완료한 경우 이동합니다.</td>
							</tr>
							<tr>								
								<td class="center_td">Bye</td>
								<td>사용자가 대화 종료를 원할 때 이동합니다.</td>
							</tr>
							<tr>
								<td class="center_td">Bye</td>
								<td>끝인사를 하면서 대화를 종료합니다.</td>
								<td class="center_td">END</td>
								<td>대화 종료</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="5_3append">Greet Task</h5>
				<div>
					<h6 id="5_3_1append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p class="center"><img src="../manualfiles/image230.png" /></p>
							<p>Task 진입 시, 위와 같은 첫인사를 수행합니다. 이후 바로 다음 Task로 넘어갑니다.</p>	
						</li>
					</ol>					
				</div>
				<h5 id="5_4append">Get_ExchangeRate_Info Task</h5>
				<div>					
					<p>Get_ExchangeRate_Info Task에서 시스템은 환율 및 변환 검색에 필요한 정보를 수집하고 사용자 발화의 의도를 분류합니다. 환율 검색에는 국가명 또는 통화명, 환율 변환 검색에는 2개의 국가명 또는 통화명과 환율 변환 금액 정보가 필요합니다. 환율 정보는 저장된 Instance의 검색으로 결과를 얻고, 환전 정보는 두 개의 환율 정보의 연산으로 결과를 얻습니다. 예를 들어, 미국 100달러에서 한국 원화로의 환율 변환 정보를 얻고자 할 때, 미국(기준 국가/통화)의 매매기준율에서 한국(대상 국가/통화)의 매매기준율을 나누고 100달러를 곱하여 환율 변환 결과를 구합니다.</p>
					<p class="center"><img src="../manualfiles/image230_1.png" /></p>					
				</div>
				<div>
					<h6 id="5_4_1append">Entity</h6>										
					<p>Get_ExchangeRate_Info Task에서는 환율 정보를 사용하기 위해 환율 개념을 Class로 사용합니다. 변환 정보는 기준 환율 정보와 대상 환율 정보가 필요하므로 이를 고려하여 두 개의 Class로 정의합니다. 이때 두 Class로 정의되는 환율 정보와 저장되는 Instance DB, Slot 단어들은 동일하기 때문에 시스템이 사용자의 발화에서 특정 Slot의 Class를 구분하기 어려울 수 있습니다. 그러므로 대표 Class ExchangeRate를 만들고 기준 및 대상 환율 정보 Class는 대표 Class에서 정의한 Slot을 Type으로 사용하고자 합니다. </p>
					<p>먼저 ExchangeRate Class를 아래와 같이 정의합니다. 사용자가 발화할 Slot을 정의하고 Type은 New defined type의 Sys.string으로 설정합니다. </p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">ExchangeRate</td>
								<td class="center_td">nation</td>								
								<td>국가명, Type: New Defined type의 Sys.string</td>
							</tr>
							<tr>								
								<td class="center_td">currency</td>								
								<td>통화명, Type: New Defined type의 Sys.string</td>
							</tr>							
						</tbody>
					</table>
					<p class="center"><img src="../manualfiles/image230_2.png" /></p>
					<p>각 Slot 단어를 다음과 같이 정의합니다.</p>
					<p class="center"><img src="../manualfiles/image230_3.png" /></p>
					<p class="center"><img src="../manualfiles/image230_4.png" /></p>
					<p>다음으로 기준 및 대상 환율 정보 개념의 Class를 각각 ExchangeRateFrom, ExchangeRateTo의 이름으로 정의합니다. ExchangeRateFrom Class는 기준 환율 정보로, 환율 정보 검색과 환율 변환에서 사용되고, ExchangeRateTo Class는 대상 환율 정보로, 환율 변환에서만 사용됩니다. 위에서 설명했듯이, 기준 및 대상 환율 정보는 대표 환율 정보 Class에서 정의한 Slot을 Type으로 설정합니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="5">ExchangeRateFrom</td>
								<td class="center_td">id</td>								
								<td>기준 통화코드 (식별자)</td>
							</tr>
							<tr>								
								<td class="center_td">nation</td>								
								<td>기준 국가명, Type : ExchangeRate.nation</td>
							</tr>
							<tr>								
								<td class="center_td">currency_name</td>								
								<td>기준 통화명, Type : ExchangeRate.currency</td>
							</tr>
							<tr>								
								<td class="center_td">deal_bas_r</td>								
								<td>기준 통화의 매매기준율</td>
							</tr>
							<tr>								
								<td class="center_td">jp_deal_bas_r</td>								
								<td>기준 통화코드 (일본 환율 안내 포맷)</td>
							</tr>		
						</tbody>
					</table>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="4">ExchangeRateTo</td>
								<td class="center_td">nation</td>								
								<td>대상 국가명, Type : ExchangeRate.nation</td>
							</tr>
							<tr>								
								<td class="center_td">currency_name</td>								
								<td>대상 통화명, Type : ExchangeRate.currency</td>
							</tr>
							<tr>								
								<td class="center_td">deal_bas_r</td>								
								<td>대상 통화의 매매기준율</td>
							</tr>
							<tr>								
								<td class="center_td">id</td>								
								<td>대상 통화코드</td>
							</tr>			
						</tbody>
					</table>
					<p class="center"><img src="../manualfiles/image233.png" /></p>
					<p class="center"><img src="../manualfiles/image234.png" /></p>
					<p>그림과 같이 ExchangeRate의 Slot을 Type으로 설정하면, ExchangeRate에서 정의한 Slot 단어를 ExchangeRateFrom, ExchangeRateTo의 Slot 단어로 동일하게 사용할 수 있습니다. ExchangeRateFrom.jp_deal_bas_r은 우리나라에서 일본의 환율 정보를 제공할 때, 100엔 기준인 것을 고려하여 일본 환율 정보를 안내할 때 사용하기 위한 Slot입니다. 이러한 방식으로 특정 Slot의 포맷을 만들 수 있습니다.</p>
					<p>각 Class의 Instance DB는 다음과 같습니다. </p>
					<p class="center"><img src="../manualfiles/image235.png" /></p>
					<p class="center"><img src="../manualfiles/image235_1.png" /></p>
					<p>환율 변환을 위해 필요한 부가적인 정보를 FromTo 이름의 Class로 정의합니다. 현재 도메인에서 환율 변환을 위해 필요한 부가적인 정보는 환율 변환 기준 금액으로 아래 표와 같이 Slot으로 정의합니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>Class</th>
								<th>Slot</th>
								<th>Slot 설명</th>
							</tr>
							<tr>
								<td class="center_td">FromTo</td>
								<td class="center_td">sum</td>								
								<td>변환 기준 금액</td>
							</tr>		
						</tbody>
					</table>
					<p class="center"><img src="../manualfiles/image236.png" /></p>
				</div>
				<div>
					<h6 id="5_4_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p>Task가 시작될 때, 시스템은 다음과 같이 발화합니다. 같은 의도의 발화를 다양하게 정의할 수 있습니다. </p>
							<p class="center"><img src="../manualfiles/image237.png" /></p>							
						</li>
						<li>응답 대화
							<p>본 Task에서 사용자의 발화 의도는 다음과 같이 3가지로 정의합니다.</p>
							<ol>
								<li>request_exchange_rate(ExchangeRateFrom.deal_bas_r)
									<p>사용자가 시스템에게 특정 국가/통화의 환율을 질문하는 의도입니다. 해당 의도는 오직 기준 환율 정보(ExchangeRateFrom Class 해당)만 수집되고, 사용자 발화는 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image238.png" /></p>
								</li>
								<li>request_converter(ExchangeRateFrom.deal_bas_r, ExchangeRateTo.deal_bas_r) 
									<p>사용자가 시스템에게 특정 두 국가의 환율 변환 결과를 질문하는 의도입니다. 해당 의도는 기준 환율 정보(ExchangeRateFrom), 대상 환율 정보(ExchangeRateTo), 변환 부가 정보(FromTo)의 정보가 수집되고, 사용자 발화는 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image239.png" /></p>
								</li>
								<li>goodbye  
									<p>사용자가 대화를 종료하고 싶다는 의도입니다. 사용자 발화는 다음과 같습니다.</p>
									<p class="center"><img src="../manualfiles/image240.png" /></p>
									<p>해당 의도에 대한 시스템 응답은 다음과 같으며, 해당 시스템 의도를 발화하면 END Task로 이동하여 대화가 종료됩니다.</p>
									<p class="center"><img src="../manualfiles/image241.png" /></p>
									<p>Get_ExchangeRate_Info, ExchangeRate_Info, ExchangeRate_Converter Task 중 어느 Task에 있어도 사용자가 대화 종료를 원하면 Bye Task로 이동하도록 구성되어 있습니다. 그러므로 각 Task마다 goodbye Intent를 만드는 대신, 아래의 그림과 같이 all tasks의 Intent로 만들어도 됩니다. </p>
									<p class="center"><img src="../manualfiles/image241_1.png" /></p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="5_5append">ExchangeRate_Info Task</h5>
				<div>					
					<p>ExchangeRate_Info Task에서는 Get_ExchangeRate_Info Task에서의 사용자 발화 의도가 request_exchange_rate에 대한 검색 및 정보 제공 역할을 합니다. 이 Task에서는 환율 정보를 제공하는 Intent인 inform_exchange_rate를 시스템이 발화하면 Get_ExchangeRate_Info Task로 이동합니다.</p>					
				</div>
				<div>
					<h6 id="5_5_1append">Entity</h6>
					<p>ExchangeRate_Info Task는 기준 환율 정보인 ExchangeRateFrom Class를 사용합니다.</p>
				</div>
				<div>
					<h6 id="5_5_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p>Task 시작 시, 시스템 발화는 다음과 같이 2가지 유형으로 정의합니다. </p>
							<ol class="decimal_ol">
								<li>검색된 환율 정보가 1개인 경우</li>
								<li>검색된 환율 정보가 없는 경우</li>
							</ol>
							<p>환율 정보의 검색 결과가 1개인 경우, 시스템은 사용자가 질문한 환율 정보를 응답할 수 있습니다. 일본의 환율 정보인 경우, 100엔 기준으로 응답할 수 있도록 다음과 같은 조건문과 시스템 발화로 작성합니다. </p>
							<p>시스템 발화 조건: Count("ExchangeRateFrom.id", "system_response") == 1  and Value("ExchangeRateFrom.nation", "system_response") == "일본"</p>
							<p class="center"><img src="../manualfiles/image242.png" /></p>
							<p>이외의 국가는 다음과 같은 조건문과 시스템 발화로 작성합니다.</p>
							<p>시스템 발화 조건: Count("ExchangeRateFrom.id", "system_response") == 1  and Value("ExchangeRateFrom.nation", "system_response") ~= "일본"</p>
							<p class="center"><img src="../manualfiles/image243.png" /></p>
							<p>검색된 환율 정보가 없는 경우에는 다음과 같이 시스템 발화를 작성해줍니다.</p>
							<p class="center"><img src="../manualfiles/image244.png" /></p>
							<p>위의 세가지 시스템 발화 후, 사용자의 다음 질문을 받기 위해 다음과 같이 사용했던 Slot 정보를 Reset합니다.</p>
							<p class="center"><img src="../manualfiles/image245.png" /></p>	
						</li>
						<li>진행 대화
							<p>통화명이 동일하여 환율 정보 검색 결과가 복수인 경우(e.g. 미국 달러, 캐나다 달러), 또는 사용자가 발화하지 않아 정보를 검색할 대상이 없는 경우에 시스템이 발화할 수 있는 결과가 복수가 되므로 다음과 같이 특정 나라 Slot값을 질문하도록 정의합니다.</p>
							<p class="center"><img src="../manualfiles/image246.png" /></p>	
						</li>
						<li>응답 대화
							<p>ExchangeRate_Info Task에 정의된 사용자 발화 가능한 Intent는 두 개 입니다.</p>
							<ol>
								<li>inform_from_nation
									<p>환율 검색에 필요한 추가 정보를 발화하는 Intent입니다. 사용자 발화를 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image247.png" /></p>
									<p>사용자 발화에서 ExchangeRateFrom.nation대신 ExchangeRate.nation이 Slot tagging된 것을 볼 수 있습니다. 이것은 사용자 발화를 보다 정확하게 이해하기 위함이며, ExchangeRate.nation으로 인식된 값은 Task에서 직전에 요청한 Slot값으로 태깅됩니다. 이 Task에서는 ExchangeRateFrom.nation의 Slot 값을 요청했기 때문에 ExchangeRate.nation의 값이 ExchangeRateFrom.nation으로 태깅됩니다. </p>
									<p>이에 대한 시스템 응답은 2가지 유형으로 환율 검색 결과가 1개로 환율 정보를 제공할 수 있는 경우와 검색 결과가 0개로 환율 정보를 제공할 수 없는 경우입니다. Task 시작 시 시스템 발화의 시스템 발화 조건과 동일합니다.</p>
									<p class="center"><img src="../manualfiles/image248.png" /></p>
									<p>시스템 발화 후, 사용자의 다음 질문을 받기 위해 다음과 같이 사용했던 Slot 정보를 Reset합니다. </p>
									<p class="center"><img src="../manualfiles/image249.png" /></p>
								</li>
								<li>goodbye 
									<p>사용자가 대화를 종료하고 싶다는 의도입니다. 사용자 발화의 예시는 Get_ExchangeRate_Info Task의 goodbye Intent와 같습니다. ExchangeRate_Info Task에서 goodbye Intent를 발화하면 END Task로 이동한 후, 대화를 종료하게 됩니다. </p>
								</li>
							</ol>
						</li>
					</ol>					
				</div>
				<h5 id="5_6append">ExchangeRate_Converter Task</h5>
				<div>					
					<p>ExchangeRate_Converter Task에서는 Get_ExchangeRate_Info에서의 사용자 발화 의도   request_converter에 대한 검색 및 정보 제공 역할을 합니다. 이 Task에서는 환율 정보를 제공하는 Intent인 inform_exchange_convert_info를 시스템이 발화하면 Get_ExchangeRate_Info Task로 이동합니다.</p>					
				</div>
				<div>
					<h6 id="5_6_1append">Entity</h6>
					<p>ExchangeRate_Converter Task는 기준 환율 정보인 ExchangeRateFrom Class 와 대상 환율 정보인   ExchangeRateTo Class, 환율 변환에 필요한 정보인 FromTo Class를 사용합니다.</p>
				</div>
				<div>
					<h6 id="5_6_2append">시스템 대화</h6>
					<ol>
						<li>전이 대화
							<p>Task 시작 시, 시스템 발화는 검색된 기준 환율 정보와 대상 환율 정보가 각각 1개이고 환율 변환 금액을 사용자가 발화한 경우 환율 변환 정보를 제공할 수 있습니다. 시스템은 다음과 같이 발화합니다.</p>
							<p>시스템 발화 조건 : Count("ExchangeRateTo.id", "system_response") == 1 and Count("ExchangeRateFrom.id", "system_response") == 1 and Entity("FromTo.sum", "user_history", "true")</p>
							<p class="center"><img src="../manualfiles/image250.png" /></p>
							<p>기준 환율 정보 또는 대상 환율 정보가 0개인 경우 시스템은 환율 정보를 제공할 수 없기 때문에 다음과 같이 발화합니다.</p>
							<p class="center"><img src="../manualfiles/image250_1.png" /></p>
							<p>시스템 발화 후, 사용자의 다음 질문을 받기 위해 다음과 같이 사용했던 Slot 정보를 Reset합니다.</p>
							<table summary="">
								<caption class="hide"></caption>
								<colgroup>
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>																
										<td>Reset("ExchangeRateFrom.nation"); Reset("ExchangeRateFrom.currency_name"); Reset("ExchangeRateTo.nation"); Reset("ExchangeRateTo.currency_name"); Reset("FromTo.sum");</td>
									</tr>		
								</tbody>
							</table>
							<p class="center"><img src="../manualfiles/image250_2.png" /></p>
						</li>
						<li>진행 대화
							<p>통화명이 동일하여 기준/대상의 환율 정보 검색 결과가 복수인 경우(e.g. 미국 달러, 캐나다 달러), 또는 사용자가 발화하지 않아 정보를 검색할 대상이 없는 경우에 시스템이 발화할 수 있는 결과가 복수가 되므로 다음과 같이 특정 나라 Slot값을 질문하도록 정의합니다. </p>
							<p class="center"><img src="../manualfiles/image251.png" /></p>
							<p class="center"><img src="../manualfiles/image252.png" /></p>
							<p>사용자가 환율 변환 금액을 발화하지 않은 경우, 환율 변환 기준 금액이 정해지지 않아 정보를 제공할 수 없으므로, 다음과 같이 환율 변환 금액 Slot 값을 질문하도록 정의합니다.</p>
							<p class="center"><img src="../manualfiles/image253.png" /></p>	
						</li>
						<li>응답 대화
							<p>ExchangeRate_Converter Task에 정의된 사용자 발화 가능한 Intent는 두 개 입니다.</p>
							<ol>
								<li>inform_info
									<p>환율 변환 검색에 필요한 추가 정보(기준/대상 국가명, 환율 변환 금액)를 발화하는 Intent입니다. 사용자 발화를 다음과 같이 작성합니다.</p>
									<p class="center"><img src="../manualfiles/image254.png" /></p>
									<p>사용자 발화에서 ExchangeRateFrom.nation과 ExchangeRateTo.nation을 구분하기 위해 해당 Slot대신 ExchangeRate.nation이 Slot tagging된 것을 볼 수 있습니다. 여기서 ExchangeRate.nation으로 인식된 값은 Task에서 직전에 요청한 Slot값으로 태깅됩니다. 예를 들어, Task에서 직전에 ExchangeRateTo.nation의 Slot 값을 요청했다면, ExchangeRate.nation의 값이 ExchangeRateTo.nation으로 태깅됩니다. </p>
									<p>이에 대한 시스템 응답은 2가지 유형으로 환율 변환 검색 결과가 1개로 환율 정보를 제공할 수 있는 경우와 검색 결과가 0개로 환율 정보를 제공할 수 없는 경우입니다. Task 시작 시 시스템 발화의 시스템 발화 조건과 동일합니다.</p>
									<p class="center"><img src="../manualfiles/image255.png" /></p>
									<p>시스템 발화 후, 사용자의 다음 질문을 받기 위해 다음과 같이 사용했던 Slot 정보를 Reset합니다. Task 시작 시 시스템 발화의 시스템 발화 후 행동과 동일합니다.</p>
									<p class="center"><img src="../manualfiles/image255_1.png" /></p>
								</li>
								<li>goodbye 
									<p>사용자가 대화를 종료하고 싶다는 의도입니다. 사용자 발화의 예시는 Get_ExchangeRate_Info Task의 goodbye Intent와 같습니다. ExchangeRate_Converter Task에서 goodbye Intent를 발화하면 END Task로 이동한 후, 대화를 종료하게 됩니다.</p>
								</li>
							</ol>
						</li>
					</ol>
				</div>
				
				<!-- 부록2 start -->
				<h4 id="0append2">DevLog 정보 설명</h4>
				<div>					
					<p>ETRI 대화시스템, GenieDialog의 디버깅을 위한 중간 처리 과정 정보에 대한 XML 포맷 및 그 태그가 담는 정보를 기술합니다.</p>
					<p>ETRI GenieDialog의 디버깅 XML 태그 정보는 처리 과정 순서대로 출력되며, 대화이해와 대화관리로 구분하여 출력하고 있습니다. 대화이해에서는 사용자 의도를 인식하기 위한 분석 과정의 중간결과들을 출력하고 대화관리에서는 시스템이 응답하기 위해 검사하는 조건식 위주로 출력하여 그 시스템 응답 선택과 대화이력 변경 정보를 제공합니다. 이를 통하여 대화처리 응용 도메인 개발자가 의도하지 않은 대화시스템 결과를 분석하고 튜닝할 수 있습니다.</p>									
				</div>
				<h5 id="1append2">대화이해 Debugin XML</h5>
				<div>					
					<p>대화이해 처리 과정에서 출력한 중간 결과에 대한 디버깅 XML의 field 에 대해서 설명합니다.</p>
					<ol>
						<li>대화이해 예제 : “다음주 수요일이 좋아요”
							<p class="center"><img src="../manualfiles/image256.png" /></p>
						</li>
						<li>field description
							<table summary="">
								<caption class="hide"></caption>
								<colgroup>
									<col width="20%" />
									<col width="18%" />
									<col width="20%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>Element Tag</th>
										<th>상위 Element</th>
										<th>Value</th>
										<th>설명</th>
									</tr>
									<tr>
										<td>slu_processing</td>
										<td>없음</td>
										<td></td>
										<td>대화이해 디버깅 로그 시작 태그</td>
									</tr>
									<tr>
										<td>user_input</td>
										<td>slu_processing</td>
										<td>사용자 입력 문장</td>
										<td>대화이해 입력 사용자 발화 문장</td>
									</tr>
									<tr>
										<td>MA</td>
										<td>processing</td>
										<td>형태소 분석 결과</td>
										<td>표층형/대표형/품사/사전의미 형태로 형태소 분석된 결과 <br>(예: 수요일/수요일/NN.용언불가능/시간)</td>
									</tr>
									<tr>
										<td>NE</td>
										<td>processing</td>
										<td>사용자 및 일반 도메인 의미객체 인식 결과</td>
										<td>표층형/대표형/품사/NE의미 형태 <br>(예: 다음주수요일/다음주수요일/NN.용언불가능/<b>date.point</b>)</td>
									</tr>
									<tr>
										<td>slot_tagging</td>
										<td>processing</td>
										<td>SVM Slot태깅 결과</td>
										<td>Ti1,Ti2,Ti3,…,Tin (i번째열후보)각 태깅열 후보는 “ “로 구분<br>T:IBO식 태그, B:Slot시작, I:Slot계속, O:Slot이아님<br>(예:B-dest,I-dest,O,O,O)</td>
									</tr>
									<tr>
										<td>DA0 ~ DA#n</td>
										<td>processing</td>
										<td>SVM Dialog Act (DA) 분류 결과</td>
										<td>DAn:n번째 DA인식 후보(예: inform())</td>
									</tr>
									<tr>
										<td>SLU1 ~ SLU3</td>
										<td>processing</td>
										<td>최종 인식된 사용자 의도</td>
										<td>형태: DA(Slot명1=Slot값1 Slot명2=Slot값2,,…)<br>(예: inform(dest=”서울역”, time=”1시”)</td>
									</tr>										
								</tbody>
							</table>
						</li>
					</ol>					
				</div>
				<h5 id="2append2">대화관리/생성 Debuging XML</h5>
				<div>					
					<p>대화관리/생성 과정에서 출력한 중간 결과에 대한 디버깅 XML의 field 에 대해서 설명합니다. 대화관리/생성 특성 상 도메인에 대한 대화모델링 지식에 따라 그 출력 형태가 달라집니다. </p>
					<p>대화관리/생성(XML field: dm_processing)는 2단계로 이루어진다. 첫번째 사용자 발화를 대화관리/생성에서 분석하는 단계(XML field: processing_user_intents)와 분석한 사용자 발화에 대한 시스템 응답을 생성하는 단계(XML field: processing_system_response)로 구분할 수 있습니다.</p>
				</div>
				<h5 id="2_1append2">사용자 발화 대화관리/생성 Debuging XML – processing_user_intents</h5>
				<div>
					<ol>
						<li>대화관리 예제 – 사용자 발화 처리 로그 정보<br>사용자 발화: “페퍼로니 피자요”에 대한 결과
							<p class="center"><img src="../manualfiles/image257.png" /></p>
							<p class="center"><img src="../manualfiles/image258.png" /></p>
						</li>
						<li>field description
							<table summary="">
								<caption class="hide"></caption>
								<colgroup>
									<col width="20%" />
									<col width="18%" />
									<col width="20%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>Element Tag</th>
										<th>상위 Element</th>
										<th>Value</th>
										<th>설명</th>
									</tr>
									<tr>
										<td>dm_processing</td>
										<td>없음</td>
										<td></td>
										<td>대화관리 디버깅 로그 </td>
									</tr>
									<tr>
										<td>processing_user_intents</td>
										<td>dm_processing</td>
										<td></td>
										<td>사용자 발화 처리 과정 로그</td>
									</tr>
									<tr>
										<td>turn</td>
										<td>processing_user_intents</td>
										<td>진행 턴 수</td>
										<td>사용자 발화의 턴 수, 턴 정의: 시스템, 사용자 발화를 하나의 턴으로 카운트</td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">Slot 값 변경에 대한 정의한 Script 동작 결과 출력부</td>
									</tr>
									<tr>
										<td>slot_relation</td>
										<td>processing_user_intents</td>
										<td></td>
										<td>사용자 발화 또는 Slot값 변경으로 인해, Entity 정의에서 정의한 Slot값 변경 구현 과정 출력 (상기 예: Pizza.type 값을 Memory.pizza에 Copy하는 동작)</td>
									</tr>
									<tr>
										<td>source_slot</td>
										<td>slot_relation</td>
										<td>Slot 이름</td>
										<td>Slot값 변경 define이 정의되어 있는 Slot 이름</td>
									</tr>
									<tr>
										<td>define</td>
										<td>slot_relation</td>
										<td></td>
										<td>Slot값 변경 정의 부분</td>
									</tr>
									<tr>
										<td>target_slot</td>
										<td>define</td>
										<td>Slot 이름</td>
										<td>정의된 action 결과를 저장할 Slot 이름</td>
									</tr>
									<tr>
										<td>UIcondition</td>
										<td>define</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문</td>
									</tr>
									<tr>
										<td>condition</td>
										<td>define</td>
										<td>내부 Script</td>
										<td>현재 정의된 Slot값 변동 define 동작에 대한 조건문</td>
									</tr>
									<tr>
										<td>UIaction</td>
										<td>define</td>
										<td>GenieDialog 행동문</td>
										<td>아래 action으로 변경된 GenieDialog 행동문</td>
									</tr>
									<tr>
										<td>action</td>
										<td>define</td>
										<td>내부 Script</td>
										<td>현재 정의된 define 동작에서 조건이 만족할 때 수행할 행동</td>
									</tr>
									<tr>
										<td>lua_script</td>
										<td>define</td>
										<td>내부 Script 실행 결과</td>
										<td>lua_script는 특수 field로 다양한 상위 Element 아래에서 나타난다. LUA interpreter가 동작할 경우에 그 결과를 출력한다. (상기 예: action의 lua 스크립트 동작 결과 출력)</td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">사용자 발화 처리 이후, Task 이동 검사하는 출력부 (시스템 발화 이후에도 동일하게 출력)</td>
									</tr>
									<tr>
										<td>task_management</td>
										<td>processing_user_intents</td>
										<td></td>
										<td>Task 관련 체크 : 현재 Task 완료 여부 검사와 그 검사가 true일 경우에는 다음 Task를 검토하는 경우가 있음</td>
									</tr>
									<tr>
										<td>task_goal_check</td>
										<td>task_management</td>
										<td></td>
										<td>현재 Task 완료 여부 검사 결과 출력</td>
									</tr>
									<tr>
										<td>task</td>
										<td>task_goal_check</td>
										<td>Task 이름</td>
										<td>완료 여부 검사할 Task 이름</td>
									</tr>
									<tr>
										<td>UIcondition</td>
										<td>task_goal_check</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문</td>
									</tr>
									<tr>
										<td>condition</td>
										<td>task_goal_check</td>
										<td>내부 Script</td>
										<td>Task 완료 조건식, 보통 현 Task에서 다음 Task의 이동 조건을 OR하여 결합함. 다음 Task가 없는 경우는 직접 기술함.</td>
									</tr>
									<tr>
										<td>lua_script</td>
										<td>task_goal_check</td>
										<td>내부 Script 실행 결과</td>
										<td>lua_script는 특수 field로 다양한 상위 Element 아래에서 나타난다. LUA interpreter가 동작할 경우에 그 결과를 출력한다</td>
									</tr>
									<tr>
										<td>condition_reslut</td>
										<td>task_goal_check</td>
										<td>true 또는 false</td>
										<td>Task 완료 조건 검사의 결과 출력</td>
									</tr>
									<tr>
										<td>next_task_search</td>
										<td>task_management</td>
										<td></td>
										<td>다음 Task 이동 검사 출력(상기 예에는 없음, 아래 다른 예 참조)</td>
									</tr>
									<tr>
										<td>current_task</td>
										<td>next_task_search</td>
										<td>task 이름</td>
										<td>현재 Task 이름</td>
									</tr>
									<tr>
										<td>next_task</td>
										<td>next_task_search</td>
										<td></td>
										<td>이동할 다음 Task 정보</td>
									</tr>
									<tr>
										<td>task_name</td>
										<td>next_task</td>
										<td>task 이름</td>
										<td>이동할 Task 이름</td>
									</tr>
									<tr>
										<td>UIcondition</td>
										<td>next_task</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문</td>
									</tr>
									<tr>
										<td>condition</td>
										<td>next_task</td>
										<td>내부 Script</td>
										<td>Task 이동 조건<br>조건 검사에 사용한 lua_script 출력될 수 있음.</td>
									</tr>
									<tr>
										<td>condition_reslut</td>
										<td>task_goal_check</td>
										<td>true 또는 false</td>
										<td>Task 이동 조건 검사의 결과 출력</td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">대화이력 변동 사항 출력부 (시스템 발화 이후에도 동일하게 출력)</td>
									</tr>
									<tr>
										<td>history</td>
										<td>processing_user_intents</td>
										<td></td>
										<td>대화관리 이력 출력부 – 발화 문장(의도) 이력, Task 이력, Entity 이력으로 구분</td>
									</tr>
									<tr>
										<td>turn</td>
										<td>history</td>
										<td>진행 턴 수</td>
										<td>저장되는 시점의 턴</td>
									</tr>
									<tr>
										<td>talker</td>
										<td>history</td>
										<td>system 또는 user</td>
										<td>시스템 또는 사용자 발화 이후의 대화관리 변동인가를 표시</td>
									</tr>
									<tr>
										<td>dialog</td>
										<td>history</td>
										<td></td>
										<td>발화 문장(의도) 이력 출력부</td>
									</tr>
									<tr>
										<td>talker</td>
										<td>dialog</td>
										<td>system 또는 use</td>
										<td>발화자 정보</td>
									</tr>
									<tr>
										<td>turn</td>
										<td>dialog</td>
										<td>진행 턴 수</td>
										<td>발화 시점</td>
									</tr>
									<tr>
										<td>intention</td>
										<td>dialog</td>
										<td></td>
										<td>추가 발화된 의도 정보</td>
									</tr>
									<tr>
										<td>sentence</td>
										<td>intention</td>
										<td>문장</td>
										<td>입력 및 출력 발화 스트링 정보</td>
									</tr>
									<tr>
										<td>DA</td>
										<td>intention</td>
										<td>의도</td>
										<td>사용자/시스템 의도 표기</td>
									</tr>
									<tr>
										<td>task</td>
										<td>history</td>
										<td></td>
										<td>task 이력 출력 부분</td>
									</tr>
									<tr>
										<td>current_task</td>
										<td>task</td>
										<td>task 이름</td>
										<td>현재 Task 이름</td>
									</tr>
									<tr>
										<td>done_tasks</td>
										<td>task</td>
										<td>task 이름들</td>
										<td>현재 시점에서 완료된 Task들</td>
									</tr>
									<tr>
										<td>start_tasks</td>
										<td>task</td>
										<td>task 이름들</td>
										<td>현재 시점에서 시작된 Task들, 시작 Task와 current task가 동일할 수 있음</td>
									</tr>
									<tr>
										<td>entity</td>
										<td>history</td>
										<td></td>
										<td>entity 이력 출력 부분</td>
									</tr>
									<tr>
										<td>class</td>
										<td>entity</td>
										<td></td>
										<td>class 단위 변경 정보 출력</td>
									</tr>
									<tr>
										<td>name</td>
										<td>class</td>
										<td>class 이름</td>
										<td>class 이름</td>
									</tr>
									<tr>
										<td>slot</td>
										<td>class</td>
										<td></td>
										<td>현 class에서 변경된 slot 정보 출력</td>
									</tr>
									<tr>
										<td>name</td>
										<td>slot</td>
										<td>slot 이름</td>
										<td>slot 이름</td>
									</tr>	
									<tr>
										<td>status</td>
										<td>slot</td>
										<td></td>
										<td>slot의 변동 사항 턴 별로 출력</td>
									</tr>	
									<tr>
										<td>values</td>
										<td>status</td>
										<td>slot 값</td>
										<td>변동된 Slot 값</td>
									</tr>	
									<tr>
										<td>belief</td>
										<td>status</td>
										<td>0.0 ~ 1.0</td>
										<td>현 status의 우선 순위 1일수록 높음</td>
									</tr>	
									<tr>
										<td>turns</td>
										<td>status</td>
										<td>turn 값</td>
										<td>저장된 시점</td>
									</tr>									
								</tbody>
							</table>
						</li>
					</ol>					
				</div>		
				<h5 id="2_2append2">시스템 발화 대화관리 Debuging XML – processing_system_response</h5>
				<div>
					<p>processing_system_response는 크게 4가지 부분으로 나누어 져 있습니다.</p>
					<ol class="decimal_ol">
						<li>전체 정보: 턴 정보</li>
						<li>응답 처리 – Response(응답 대화)/Progress(진행 대화)/Transition(전이 대화) 응답 처리 과정 – 다수 처리
							<ol class="upper_ol">
								<li>응답 발화 선택(intent_check) – 조건 검토로 선택</li>
								<li>대화 생성(dialog_generation) – 선택 발화 생성</li>
								<li>Task 관리(task_management) – 시스템 발화 이후 Task 이동</li>
							</ol>
						</li>
						<li>전체 처리 대화이력 – 사용자 발화 대화관리 history와 동일</li>
						<li>시스템 최종 발화</li>					
					</ol>
					<p>&nbsp;</p>
					<p>1. 전체 정보 예제</p>
					<p class="center"><img src="../manualfiles/image259.png" /></p>
					<p>2. 응답처리 예제</p>
					<ol>
						<li>대화관리 예제 – Response 응답 처리 로그 정보
							<p class="center"><img src="../manualfiles/image260.png" /></p>	
						</li>
						<li>대화관리 예제 – Progress 응답 처리 로그 정보
							<p class="center"><img src="../manualfiles/image261.png" /></p>	
						</li>
						<li>대화관리 예제 – Transition 응답 처리 로그 정보
							<p class="center"><img src="../manualfiles/image262.png" /></p>	
						</li>
						<li>대화관리 예제 – Non response 응답 처리 로그 정보
							<p class="center"><img src="../manualfiles/image263.png" /></p>	
						</li>
					</ol>
					<p>3. 전체 처리 대화이력 – 사용자와 동일</p>
					<p>4. 시스템 최종 발화</p>
					<p class="center"><img src="../manualfiles/image264.png" /></p>
					<ol>
						<li>field description
							<table summary="">
								<caption class="hide"></caption>
								<colgroup>
									<col width="20%" />
									<col width="18%" />
									<col width="20%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>Element Tag</th>
										<th>상위 Element</th>
										<th>Value</th>
										<th>설명</th>
									</tr>
									<tr>
										<td>dm_processing</td>
										<td>없음</td>
										<td></td>
										<td>대화관리 디버깅 로그  </td>
									</tr>
									<tr>
										<td>processing_system_response</td>
										<td>dm_processing</td>
										<td></td>
										<td>시스템 발화 생성 과정 로그 </td>
									</tr>
									<tr>
										<td>turn</td>
										<td>processing_system_response</td>
										<td>진행 턴 수</td>
										<td>사용자 발화의 턴 수, 턴 정의: 시스템, 사용자 발화를 하나의 턴으로 카운트 </td>
									</tr>
									<tr>
										<td class="title_td3" colspan="4">시스템 응답 대화 검사 – Response, Progress, Transition, Non-Response 분류</td>
									</tr>
									<tr>
										<td>response_dialog_management</td>
										<td>processing_system_response</td>
										<td></td>
										<td>시스템 응답을 사용자 의도 정보로 찾는 작업 과정 로그 </td>
									</tr>
									<tr>
										<td>progress_dialog_management</td>
										<td>processing_system_response</td>
										<td></td>
										<td>시스템 응답을 정의된 Task에서 채울 Slot에 대한 질의로 찾는 작업 과정 로그 </td>
									</tr>
									<tr>
										<td>transition_dialog_management</td>
										<td>processing_system_response</td>
										<td></td>
										<td>시스템 응답을 Task 진입 조건으로 찾는 작업 과정 로그 </td>
									</tr>
									<tr>
										<td>non_response_dialog_management</td>
										<td>processing_system_response</td>
										<td></td>
										<td>상기 과정으로 응답을 찾지 못한 경우, non_response() 의도로 대답을 찾는 과정 로그 </td>
									</tr>
									<tr>
										<td class="title_td4" colspan="4">시스템 발화 검색 과정 - Response, Progress, Transition, Non-Response 동일 (일부 차이)</td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">시스템 발화 조건 검사</td>
									</tr>
									<tr>
										<td>intent_check</td>
										<td>response_dialog_management, <br>progress_dialog_management, <br>transition_dialog_management, <br>non_response_dialog_management </td>
										<td></td>
										<td>가능한 시스템 응답 검사 </td>
									</tr>
									<tr>
										<td>dialog_type</td>
										<td>intent_check</td>
										<td>response, <br>progress, <br>transition</td>
										<td>시스템 응답 종류 체크, non_response는 response 타입으로 표시 </td>
									</tr>
									<tr>
										<td>user_intent</td>
										<td>intent_check</td>
										<td>사용자 의도</td>
										<td>시스템 응답이 어떤 사용자 의도에 대한 것인가를 표시<br>response type에서만 표시 </td>
									</tr>
									<tr>
										<td>related_task</td>
										<td>intent_check</td>
										<td>task 이름 나열</td>
										<td>시스템 응답이 가능한 Task 표시<br>response와 transition type에서만 표시 </td>
									</tr>
									<tr>
										<td>progress_slot</td>
										<td>intent_check</td>
										<td>slot 이름</td>
										<td>시스템 응답으로 사용자에게 발화를 요청하는 Slot 이름 표기<br>progress type에서만 표시</td>
									</tr>
									<tr>
										<td>task</td>
										<td>intent_check</td>
										<td>task 이름</td>
										<td>transition type 발화에서 시스템 응답을 출력하는 Task 이름 표기 </td>
									</tr>
									<tr>
										<td>reply_check</td>
										<td>intent_check</td>
										<td></td>
										<td>가능한 시스템 응답 중에 다양한 발화 형태에 대한 검사<br>condition에 따라 다양한 시스템 발화 가능하게 한 것을 검사함. </td>
									</tr>
									<tr>
										<td>reply</td>
										<td>reply_check</td>
										<td></td>
										<td>시스템 발화 표시 </td>
									</tr>
									<tr>
										<td>dialog_act</td>
										<td>reply</td>
										<td>시스템 의도</td>
										<td>현재, 시스템 발화에 대한 대화 의도 </td>
									</tr>
									<tr>
										<td>pattern</td>
										<td>reply</td>
										<td>시스템 발화</td>
										<td>시스템 발화 문장 </td>
									</tr>
									<tr>
										<td>UIcondition</td>
										<td>reply_check</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문 </td>
									</tr>
									<tr>
										<td>condition</td>
										<td>reply_check</td>
										<td>내부 Script</td>
										<td>현재 시스템 발화에 대한 조건 </td>
									</tr>
									<tr>
										<td>lua_script</td>
										<td>reply_check</td>
										<td>내부 Script 실행 결과</td>
										<td>lua_script는 특수 field로 다양한 상위 Element 아래에서 나타난다. LUA interpreter가 동작할 경우에 그 결과를 출력한다 </td>
									</tr>
									<tr>
										<td>reply_condition_result</td>
										<td>reply_check</td>
										<td>true 또는 false</td>
										<td>상기 조건 검사 결과 </td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">시스템 발화 생성 과정 – 주로 시스템 발화의 slot 채우기</td>
									</tr>
									<tr>
										<td>dialog_generation</td>
										<td>response_dialog_management, <br>progress_dialog_management, <br>transition_dialog_management, <br>non_response_dialog_management </td>
										<td></td>
										<td>시스템 발화 생성 과정 로그 </td>
									</tr>
									<tr>
										<td>pattern</td>
										<td>dialog_generation</td>
										<td>시스템 발화 패턴</td>
										<td>시스템 발화 선택에서 선택한 시스템 발화 패턴 </td>
									</tr>
									<tr>
										<td>generation_report</td>
										<td>dialog_generation</td>
										<td>success 또는 error 또는 slot filing error from db</td>
										<td>대화 생성 과정의 성공 여부 보고<br>success: 성공,<br>slot filing error from db : 시스템 발화 패턴의 빈Slot을 채우는 값 검색 실패 오류<br>error: 기타 오류 </td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">사용자 발화 처리 이후, Task 이동 검사하는 출력부 (시스템 발화 이후에도 동일하게 출력)</td>
									</tr>
									<tr>
										<td>task_management</td>
										<td>response_dialog_management, <br>progress_dialog_management, <br>transition_dialog_management, <br>non_response_dialog_management </td>
										<td></td>
										<td>Task 관련 체크 : 현재 Task 완료 여부 검사와 그 검사가 true일 경우에는 다음 Task를 검토하는 경우가 있음 </td>
									</tr>
									<tr>
										<td>task_goal_check</td>
										<td>task_management</td>
										<td></td>
										<td>현재 Task 완료 여부 검사 결과 출력 </td>
									</tr>
									<tr>
										<td>task</td>
										<td>task_goal_check</td>
										<td>Task 이름</td>
										<td>완료 여부 검사할 Task 이름 </td>
									</tr>
									<tr>
										<td>UIcondtion</td>
										<td>task_goal_check</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문 </td>
									</tr>
									<tr>
										<td>condition</td>
										<td>task_goal_check</td>
										<td>내부 Script</td>
										<td>Task 완료 조건식, 보통 현 Task에서 다음 Task의 이동 조건을 OR하여 결합함. 다음 Task가 없는 경우는 직접 기술함. </td>
									</tr>
									<tr>
										<td>lua_script</td>
										<td>task_goal_check</td>
										<td>내부 Script 실행 결과</td>
										<td>lua_script는 특수 field로 다양한 상위 Element 아래에서 나타난다. LUA interpreter가 동작할 경우에 그 결과를 출력한다 </td>
									</tr>
									<tr>
										<td>condition_reslut</td>
										<td>task_goal_check</td>
										<td>true 또는 false</td>
										<td>Task 완료 조건 검사의 결과 출력 </td>
									</tr>
									<tr>
										<td>next_task_search</td>
										<td>task_management</td>
										<td></td>
										<td>다음 Task 이동 검사 출력(상기 예에는 없음, 아래 다른 예 참조) </td>
									</tr>
									<tr>
										<td>current_task</td>
										<td>next_task_search</td>
										<td>task 이름</td>
										<td>현재 Task 이름 </td>
									</tr>
									<tr>
										<td>next_task</td>
										<td>next_task_search</td>
										<td></td>
										<td>이동할 다음 Task 정보 </td>
									</tr>
									<tr>
										<td>task_name</td>
										<td>next_task</td>
										<td>task 이름</td>
										<td>이동할 Task 이름 </td>
									</tr>
									<tr>
										<td>UIcondition</td>
										<td>next_task</td>
										<td>GenieDialog 조건문</td>
										<td>아래 condition으로 변경된 GenieDialog 조건문 </td>
									</tr>
									<tr>
										<td>condition</td>
										<td>next_task</td>
										<td>내부 Script</td>
										<td>Task 이동 조건<br>조건 검사에 사용한 lua_script 출력될 수 있음. </td>
									</tr>
									<tr>
										<td>condition_result</td>
										<td>task_goal_check</td>
										<td>true 또는 false</td>
										<td>Task 이동 조건 검사의 결과 출력 </td>
									</tr>
									<tr>
										<td class="title_td2" colspan="4">대화이력 변동 사항 출력부 (시스템 발화 이후에도 동일하게 출력)</td>
									</tr>
									<tr>
										<td>history</td>
										<td>processing_system_response</td>
										<td></td>
										<td>대화관리 이력 출력부 – 발화 문장(의도) 이력, Task 이력, Entity 이력으로 구분</td>
									</tr>
									<tr>
										<td>turn</td>
										<td>history</td>
										<td>진행 턴 수</td>
										<td>저장되는 시점의 턴</td>
									</tr>
									<tr>
										<td>talker</td>
										<td>history</td>
										<td>system 또는 user</td>
										<td>시스템 또는 사용자 발화 이후의 대화관리 변동인가를 표시</td>
									</tr>
									<tr>
										<td>dialog</td>
										<td>history</td>
										<td></td>
										<td>발화 문장(의도) 이력 출력부</td>
									</tr>
									<tr>
										<td>talker</td>
										<td>dialog</td>
										<td>system 또는 use</td>
										<td>발화자 정보</td>
									</tr>
									<tr>
										<td>turn</td>
										<td>dialog</td>
										<td>진행 턴 수</td>
										<td>발화 시점</td>
									</tr>
									<tr>
										<td>intention</td>
										<td>dialog</td>
										<td></td>
										<td>추가 발화된 의도 정보</td>
									</tr>
									<tr>
										<td>sentence</td>
										<td>intention</td>
										<td>문장</td>
										<td>입력 및 출력 발화 스트링 정보</td>
									</tr>
									<tr>
										<td>DA</td>
										<td>intention</td>
										<td>의도</td>
										<td>사용자/시스템 의도 표기</td>
									</tr>
									<tr>
										<td>task</td>
										<td>history</td>
										<td></td>
										<td>task 이력 출력 부분</td>
									</tr>
									<tr>
										<td>current_task</td>
										<td>task</td>
										<td>task 이름</td>
										<td>현재 Task 이름</td>
									</tr>
									<tr>
										<td>done_tasks</td>
										<td>task</td>
										<td>task 이름들</td>
										<td>현재 시점에서 완료된 Task들</td>
									</tr>
									<tr>
										<td>start_tasks</td>
										<td>task</td>
										<td>task 이름들</td>
										<td>현재 시점에서 시작된 Task들, 시작 Task와 current task가 동일할 수 있음</td>
									</tr>
									<tr>
										<td>entity</td>
										<td>history</td>
										<td></td>
										<td>entity 이력 출력 부분</td>
									</tr>
									<tr>
										<td>class</td>
										<td>entity</td>
										<td></td>
										<td>class 단위 변경 정보 출력</td>
									</tr>
									<tr>
										<td>name</td>
										<td>class</td>
										<td>class 이름</td>
										<td>class 이름</td>
									</tr>
									<tr>
										<td>slot</td>
										<td>class</td>
										<td></td>
										<td>현 class에서 변경된 slot 정보 출력</td>
									</tr>
									<tr>
										<td>name</td>
										<td>slot</td>
										<td>slot 이름</td>
										<td>slot 이름</td>
									</tr>	
									<tr>
										<td>status</td>
										<td>slot</td>
										<td></td>
										<td>slot의 변동 사항 턴 별로 출력</td>
									</tr>	
									<tr>
										<td>values</td>
										<td>status</td>
										<td>slot 값</td>
										<td>변동된 Slot 값</td>
									</tr>	
									<tr>
										<td>belief</td>
										<td>status</td>
										<td>0.0 ~ 1.0</td>
										<td>현 status의 우선 순위 1일수록 높음</td>
									</tr>	
									<tr>
										<td>turns</td>
										<td>status</td>
										<td>turn 값</td>
										<td>저장된 시점</td>
									</tr>	
									<tr>
										<td class="title_td2" colspan="4">시스템 최종 발화 생성</td>
									</tr>
									<tr>
										<td>system_output</td>
										<td>processing_system_response</td>
										<td>시스템 발화</td>
										<td>대화관리 처리 과정을 통한 최종 시스템 발화 </td>
									</tr>
								</tbody>
							</table>
						</li>
					</ol>
				</div>								
			</div>
		</div>
	</div>
</body>
</html>
