<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>의도 Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">
		var pop;
		window.onload = function() {
			$('#tbslot').on({				
				'click focusin': function(e) {
					$('#tbslot').find('[name=IntentionSlot]').removeAttr('id');
					$(this).attr('id', 'tempid');
					pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=tempid&Type=FNC&SubType=IS", "AgentSearchpop", 430, 550, 0, 0);
					fnpreventDefault(e);
				}	
			}, '[name=IntentionSlot]');
						
			$('#tbslot').on({
				keyup: function(e) {
					var $this = $(this);
					if ($this.parent().parent().find('[id^=hidSaveChk]').length == 1) {
						$this.parent().parent().find('[id^=hidSaveChk]').val('Y');
					}
					$this.val($this.val().replace(/[^a-zA-Z0-9ㄱ-힣]/g, ''));	
				}			
			}, '[name=IntentionName]');
			
			$('#tbslot').on({
				keydown: function(e) {
					var $this = $(this);
					if (e.keyCode == 13) {
						if($this.parent().parent().next().length == 0 && $this.val() != '') {
							fnAddRow();
						}
						$this.parent().parent().next().find('input').eq(0).focus();
					}
					
				},
				keyup: function(e) {
					var $this = $(this);
					if ($this.parent().parent().find('[id^=hidSaveChk]').length == 1) {
						$this.parent().parent().find('[id^=hidSaveChk]').val('Y');
					}
					$this.val($this.val().replace(/[^a-zA-Z0-9ㄱ-힣\s]/g, ''));	
				}		
			}, '[name=IntentionSlotValue]');
			
			$('#tbslot').on('click', '[name="imgdel"]', function(e) {
				if (confirm('해당 의도 Slot을 삭제 하시겠습니까?\n삭제 하면 등록된 기존 의도 Slot은 사라집니다.')) {
					var $tr = $(this).parent().parent();
					if($tr.find('[id^=hidSeqno]').length > 0) {					
						fnLoading();
						var comAjax = new ComAjax();
						comAjax.setUrl("<c:url value='/view/deleteintention.do' />");
						comAjax.setCallback("fnAjaxCallBack");						
						comAjax.addParam("SEQ_NO", $tr.find('[id^=hidSeqno]').val());
						comAjax.addParam("INTENTION", $tr.find('[id^=hidIntentionName]').val());						
						comAjax.ajax();						
					}
					$tr.remove();	
				}				
				//fnpreventDefault(e);
			});
			
			if ('${fn:length(list)}' == '0') {
				fnAddRow();	
			}					
		}
		
		$(window).bind("beforeunload", function (e){
			if (!gfn_isNull(pop)) {
				pop.close();
			}
		});
		
		function fnAjaxCallBack(data) {
			if (data.STATUS != "OK") {
				alert('작업 중 오류가 발생 하였습니다.');
			}
			else {
				fnOpenerUpdate();
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/selectintention.do' />?Type=${param.Type}");		
				comSubmit.submit();
				
			}
			fnLoading();
		}
		
		function fnAddRow() {
			var str = "<tr name='trIn'>";
			
			str += "<td><input type='text' class='none_focus' value='' name='IntentionName' /></td>";
			str += "<td><input type='text' class='none_focus' value='' name='IntentionSlot' readonly='readonly' /></td>";
			str += "<td><input type='text' class='none_focus' value='' name='IntentionSlotValue' /></td>";			
			str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
			str += "</tr>";				
			$("#tb").append(str);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();									
			var bSaveChk = false;
			var message = '';
			var cnt = 0;
			var bAddChk = true;
			var arrname = new Array();
			var arrIntentionNamelist = new Array();
			var arrIntentionSlotlist = new Array();
			
			$('#tbslot tbody tr').each(function(idx, e) {								
				var item = new Object();
				var colcnt = 0;
				var messagecnt = 0;
				bAddChk = true;
				var $this = $(this);
				
				if ($this.find('[id^=hidSeqno]').length == 1) {
					item.SEQ_NO = $this.find('[id^=hidSeqno]').val();
					item.OLD_INTENTION_NAME = $this.find('[id^=hidIntentionName]').val();
					if ($this.find('[id^=hidSaveChk]').val() == 'N')
						bAddChk = false;								
				}
				else
					item.SEQ_NO = '0';									
				
				item.INTENTION_NAME = $this.find('[name=IntentionName]').val().trim();
				item.INTENTION_SLOT = $this.find('[name=IntentionSlot]').val().trim();
				item.INTENTION_SLOT_VALUE = $this.find('[name=IntentionSlotValue]').val().trim();
				
				if (bAddChk) {
					if (item.INTENTION_NAME == '') {
						messagecnt = 1;
						colcnt++;
					}
					
					if (item.INTENTION_SLOT == '') {											
						messagecnt = 2;
						colcnt++;
					}
					
					if (item.INTENTION_SLOT_VALUE == '') {												
						messagecnt = 3;
						colcnt++;
					}
					
					if (colcnt == 0) {
						arrobj.push(item);						
					}
					else if (colcnt != 3) {
						bSaveChk = true;
						if (messagecnt == 1)
							message = '의동 Slot명을 입력해 주세요.';
						else if (messagecnt == 2)
							message = '의동 Slot을 선택해 주세요.';
						else if (messagecnt == 3)
							message = 'Slot 값을 입력해 주세요.';
						return;
					}
				}
				
				if (arrname.contains(item.INTENTION_NAME)) {
					bSaveChk = true;						
					message = '이미 존재하는 의도명 입니다.';
					return;
				}
				
				if (item.INTENTION_NAME != '') {
					arrname.push(item.INTENTION_NAME);
					
					var Islotvalue = item.INTENTION_SLOT + '="' + item.INTENTION_SLOT_VALUE + '"';
					var objIname = new Object();
					objIname.slotvalue = Islotvalue;
					objIname.slotname = item.INTENTION_SLOT;
					arrIntentionNamelist[item.INTENTION_NAME] = objIname;
					
					var objISlot = new Object();
					objISlot.intentionname = item.INTENTION_NAME;
					arrIntentionSlotlist[Islotvalue] = objISlot;					
				}
			});
			
			if (bSaveChk) {
				alert(message);
				return;
			}
			
			fnLoading();
			
			if ('${param.Type}' == 'W') {
				opener.arrIntentionNamelist = arrIntentionNamelist;
				opener.arrIntentionSlotlist = arrIntentionSlotlist;
			}
			arr.ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/insertintention.do' />");
			comAjax.setCallback("fnAjaxCallBack");			
			comAjax.addParam("VALUES_ITEM", jsoobj);			
			comAjax.ajax();
			//fnpreventDefault(event);
		}
		
		function fnOpenerUpdate() {
			if ('${param.Type}' == 'W') {
				opener.fnSelectList();
				opener.fnSelectList();
			}
		}
	</script> 
	<style>
		input {border:1px solid #ddd !important;}
	</style>
</head>
<body>
	<div id="divLoading" style="display:none; box-shadow: 3px 3px 10px #999999;position: absolute;left: 50%;top: 50%;width: 180px;height: 180px;margin-top:-90px;margin-left:-90px;min-width: 170px !important;z-index: 1001;background: url(../images/loading.gif) no-repeat center center #ffffff !important;"></div>
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW entities entitiesSearch">
			<div class="liBox liBox3">
				<div class="title slotCond">의도 Slot 등록 <a href="#" onclick="fnAddRow();" style="float:right; color:#0488da; font-size:14px; font-weight:bold;">+Add row</a>				
				</div>
				<div class="conBox tableBox slotCondBox">
					<table id="tbslot" class="table">
						<caption class="hide" style="display:none;">Intention</caption>
						<colgroup>
							<col width="25%">
							<col width="40%">
							<col width="25%">
							<col width="">
						</colgroup>
						<thead>
							<tr>
								<th name="thTitle">의도 Slot 명칭</th>
								<th name="thTitle">의도 Slot</th>
								<th name="thTitle">Slot 값</th>
								<th name="thTitle"></th>
							</tr>
						</thead>
						<tbody id="tb">
							<c:set var="rowchk" value="0"></c:set>
							<c:forEach items="${list}" var="row">
								<c:choose>
									<c:when test="${rowchk == '0'}">
										<c:set var="rowchk" value="1"></c:set>
										<tr class="knowledge_th01" name="trUp">
									</c:when>
									<c:otherwise>
										<c:set var="rowchk" value="0"></c:set>
										<tr class="knowledge_th02" name="trUp">
									</c:otherwise>
								</c:choose>								
									<input type="hidden" id="hidSaveChk${row.SEQ_NO }" value="N" />
									<input type="hidden" id="hidSeqno${row.SEQ_NO }" value="${row.SEQ_NO }" />
									<input type="hidden" id="hidIntentionName${row.SEQ_NO }" value="${row.INTENTION_NAME }" />
									<%-- <td><input type="text" class="none_focus" value="${row.INTENTION_NAME }" name="IntentionName" style="border:none !important;" readonly /></td> --%>
									<td><input type="text" class="none_focus" value="${row.INTENTION_NAME }" name="IntentionName" /></td>
									<td><input type="text" class="none_focus" value="${row.INTENTION_SLOT }" name="IntentionSlot" /></td>
									<td><input type="text" class="none_focus" value="${row.INTENTION_SLOT_VALUE }" name="IntentionSlotValue" /></td>								
									<td><img src="../images/delete_btn.png" name="imgdel" /></td>
								</tr>
							</c:forEach>							
						</tbody>
					</table>
				</div>
			</div>
			<div class="btnBox" style="background:none !important">
				<span>
					<div class="cancle" onclick="fnSave();">Save</div>					
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
				</span>				
			</div>
		</div>
	</div>	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>













