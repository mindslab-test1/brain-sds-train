<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>복사/참조</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}
				
				fnpreventDefault(e);
			});		
		
			if ('${param.Type}' == 'E') {						
				<c:forEach items="${R_CLASS_LIST}" var="rcrow">
					$('#classck${rcrow.CLASS_SEQ}').prop('checked', true);
				</c:forEach>
			}						
			
			var referchk = true;
			$('.entitiesBox').each(function() {
				if ($(this).find('.check').length == 0) {
					$(this).hide();
				}
				else 
					referchk = false;
			});
			
			if (referchk) {
				alert('Reference할 대상이 없습니다.');
				fnWinClose();
			}
		}
				
		
		function fnCheck(obj, type, seqno) {
			if ($(obj).prop('checked') == false) {				
				$(obj).prop('checked', false);
				if (type == 'I') {
					$('#reponseck' + seqno).prop('checked', false);
				}
			}
			else {
				if (type == 'R') {
					if ($('#classck' + seqno).prop('checked') == true) {
						$(obj).prop('checked', true);
					}
				}
				else
					$(obj).prop('checked', true);
			}
		}
		
		function fnCopyClass() {
			var obj = new Object();
			var arr = new Array();
			
			if ('${param.Type}' == 'E') {
				$('[id^=classck]').each(function() {
					if ($(this).prop('checked') == true)
						arr.push($(this).val());
				});	
			}
			else {				
				$('[id^=classck]').each(function() {
					if ($(this).prop('checked') == true) {
						var subobj = new Object();
						subobj.INTENT_SEQ = $(this).val();
						subobj.INTENT_NAME = $('#sp' + subobj.INTENT_SEQ).text();
						if ($('#reponseck' + subobj.INTENT_SEQ).prop('checked') == true)						
							subobj.RESPONSE_CHK = 'Y';
						else 
							subobj.RESPONSE_CHK = 'N';
						arr.push(subobj);
					}
				});
			}			
			obj.ITEM = arr;
			
			var jsoobj = JSON.stringify(obj);
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/classreferorcopy.do' />");	
			comAjax.setCallback("fnCopyClassCallBack");
			comAjax.addParam("TYPE", '${param.Type}');
			comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comAjax.addParam("ITEM", jsoobj);
			comAjax.ajax();
		}
		
		function fnCopyClassCallBack(data) {
			if (data.STATUS == 'OK') {
				if ('${param.Type}' == 'I') {
					if (data.COPY_TYPE == 1) {
						alert('이미 같은 이름의 intent가 존재합니다.');
						retrun;
					}			
					else if (data.COPY_TYPE == 2) {
						alert('복사중 오류가 발생하였습니다.');
						retrun;
					} 
				}
				opener.window.location.reload();
				fnWinClose();	
			}
			else {
				if ('${param.Type}' == 'E')
					alert('class 참조 중 오류가 발생하였습니다.');
				else 
					alert('Intent 복사 중 오류가 발생하였습니다.');
			}
		}
	</script>    
</head>
<body style="min-width:550px;">
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
			<c:set var="AgentName" value=""></c:set>
			<c:choose>
				<c:when test="${param.Type eq 'E'}">
					<div class="title">Reference Class 선택</div>
					<div class="box box3">
						<c:forEach items="${submenu}" var="trow">						
							<c:if test="${trow.SEQ_NO ne param.AGENT_SEQ && trow.AGENT_NAME ne 'END'}">	
								<div class="entitiesBox">
									<div class="title"><div>${trow.AGENT_NAME} task에 선언된 class</div><span class="open"></span></div>
									<div class="checkbox">											
									<c:forEach items="${CLASS_LIST}" var="crow">
										<c:if test="${trow.SEQ_NO eq crow.AGENT_SEQ}">
											<div class="check">
												<div class="right" style="width:98%;">									
													<table style="width:98%;">
														<colgroup>
										                    <col width="60%">
										                    <col width="">
										                </colgroup>
														<tr>
															<td style="vertical-align:middle; word-break:break-all;">
																<span>
																	${crow.CLASS_NAME}
																</span>																						
															</td>
															<td style="vertical-align:middle; word-break:break-all;">																
																<input type="checkbox" class="checkbox_st" id="classck${crow.SEQ_NO}" name="classck${crow.SEQ_NO}" value="${crow.SEQ_NO}" onclick="fnCheck(this, 'C', ${crow.SEQ_NO});" />
																<label class="hide" for="classck${crow.SEQ_NO}"></label>
															</td>
														</tr>
													</table>								
												</div>
											</div>
										</c:if>
									</c:forEach>
									</div>
								</div>
							</c:if>	
						</c:forEach>
					</div>					
				</c:when>
				<c:otherwise>
					<div class="title">Copy Intent</div>
					<div class="box box3">
						<c:forEach items="${submenu}" var="trow">						
							<c:if test="${trow.SEQ_NO ne param.AGENT_SEQ && trow.AGENT_NAME ne 'END'}">
								<div class="entitiesBox">
									<div class="title"><div>${trow.AGENT_NAME}</div><span class="open"></span></div>
									<div class="checkbox">
									<c:set var="Cnt" value=""></c:set>
									<c:forEach items="${CLASS_LIST}" var="crow">										
										<c:if test="${trow.SEQ_NO eq crow.AGENT_SEQ}">
											<div class="check">
												<div class="right" style="width:98%;">	
													<c:if test="${Cnt == ''}">
														<c:set var="Cnt" value="N"></c:set>
														<table style="width:98%; text-align:center; font-weight: bold;">
															<colgroup>
											                    <col width="34%">
											                    <col width="33%">
											                    <col width="33%">
											                </colgroup>
															<tr>
																<td style="vertical-align:middle; word-break:break-all;">
																	<span>Intent 명</span>																					
																</td>
																<td style="vertical-align:middle; word-break:break-all;">																
																	<span>해당 intent 복사</span>
																</td>
																<td style="vertical-align:middle; word-break:break-all;">																
																	<span>시스템 응답도 복사</span>
																</td>
															</tr>
														</table>
													</c:if>		
													<table style="width:98%; text-align:center;">
														<colgroup>
										                    <col width="34%">
										                    <col width="33%">
										                    <col width="33%">
										                </colgroup>
														<tr>
															<td style="vertical-align:middle; word-break:break-all;">
																<span id="sp${crow.SEQ_NO}">${crow.INTENT_NAME}</span>																					
															</td>
															<td style="vertical-align:middle; word-break:break-all;">																
																<input type="checkbox" class="checkbox_st" id="classck${crow.SEQ_NO}" name="classck${crow.SEQ_NO}" value="${crow.SEQ_NO}" onclick="fnCheck(this, 'I', ${crow.SEQ_NO});" />
																<label class="hide" for="classck${crow.SEQ_NO}"></label>
															</td>
															<td style="vertical-align:middle; word-break:break-all;">																
																<input type="checkbox" class="checkbox_st" id="reponseck${crow.SEQ_NO}" name="reponseck${crow.SEQ_NO}" onclick="fnCheck(this, 'R', ${crow.SEQ_NO});" />
																<label class="hide" for="reponseck${crow.SEQ_NO}"></label>
															</td>
														</tr>
													</table>								
												</div>
											</div>
										</c:if>
									</c:forEach>
									</div>
								</div>
							</c:if>	
						</c:forEach>
					</div>
				</c:otherwise>
				
			</c:choose>
			</div>
			
			<div class="btnBox" style="background-color:#ffffff !important;">				
				<span>
					<div id="divLoading" style="display:none; box-shadow: 3px 3px 10px #999999;position: fixed;left: 50%;top: 50%;width: 180px;height: 180px;margin-top:-90px;margin-left:-90px;min-width: 170px !important;border:1px solid #999999;z-index: 999;background: url(../images/loading.gif) no-repeat center center #ffffff !important;"></div>
					<div class="ok" onclick="fnCopyClass();">Ok</div>
					<div class="cancle" onclick="fnWinClose();">Cancel</div>					
				</span>				
			</div>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>