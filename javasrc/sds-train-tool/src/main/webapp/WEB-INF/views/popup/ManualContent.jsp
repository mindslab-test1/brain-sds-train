<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>GenieDialog 매뉴얼</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/manual.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
	<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ui.all.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout.min.js'/>"></script> --%>
	<script type="text/javascript" src="<c:url value='/js/jquery-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout-latest.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">
		$(function() {
			if ('${param.spos}' != '0') {
				$(window).scrollTop(${param.spos});
			}
			else if ('${param.id}' != '') {
				fnMove('${param.id}'.replace('%23', '#'), 1);
			}
		});
		function fnMove(id, type) {
			if (type != 1) {
				window.parent.backType = 'menu';
				window.parent.backScrollPos = $(window).scrollTop();
				window.parent.$('#spBack').show();	
			}			
			
			if (id.indexOf('${param.type}') > -1) {
				window.parent.fnMove(id);	
			}
			else
				window.parent.fnSelect('append', id);
						
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<div id="content_wrap">
		<div id="content">
			<div class="content_body">
				<!-- 1장 start -->
				<h4 id="1menu">개요</h4>
				<div>					
					<p>GenieDialog는 새로운 도메인에서 대화시스템이 작동하기 위한 도메인 대화지식을 구축하는 것을 제공합니다.  GenieDialog의 개념도는 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image002.png" /></p>
					<p>개념도에서 보듯이, 일반 사용자는 대화시스템과 대화를 통하여 특정 업무를 수행할 수 있습니다. 이와 같은 특정 업무를 수행하도록 하는 대화지식은 응용 도메인마다 다르게 구축해야 합니다. 이 도메인마다 다르게 구축해야 하는 지식을 GenieDialog를 통하여 작성이 가능합니다.</p>					
				</div>				
				<h5 id="1_1menu">대화시스템</h5>										
				<div>										
					<p>GenieDialog의 대화시스템은 목적지향 대화시스템(Goal-oriented Dialog System) 또는 태스크 지향 대화시스템(Task-oriented Dialog System)입니다. 이 의미는 특정 업무를 처리하는 목적을 대화를 수행하는 대화시스템을 말합니다. 이와 달리, 신변 잡담 위주의 대화가 가능한 대화시스템을 보통 Chit-Chat 대화시스템 또는 챗봇(Chat-bot)이라고 불립니다.</p>
					<p>목적지향 대화시스템은 사용자의 입력이 어떤 의도인가를 파악하는 대화이해 모듈과 이해한 사용자 의도에 따라 시스템 응답 또는 행동을 하는 대화관리/생성 모듈로 구성됩니다.</p>
					<p>예를 들어, 날씨 도메인에서 사용자가 “서울 날씨 어때?”라고 질문하면 대화이해 모듈은 미리 정의한 의도(Intent) “request(Weather.info, Weather.city=”서울”)”로 인식합니다. Weather.info와 Weather.city는 날씨 도메인 작업을 하기 위한 개념 단위인 Entity입니다. 사용자가 필요한 정보를 채워서 알려줘야 시스템이 응답할 수 있기에 Slot이라고도 합니다. 대화관리/생성 모듈은 지금까지 사용자와의 대화를 관리한 대화 이력에서 정보를 제공할 날짜를 모르기에 “언제 날씨를 알려드릴까요?”라고 문의하는 응답을 생성합니다. 사용자가 “오늘”이라고 날짜를 알려주면 이를 대화이해와 대화관리/생성 과정을 거쳐서 대화시스템은 Weather.date=”오늘”이고 Weather.city=”서울”인 Weather.info를 알려줍니다.  대화시스템은 서울의 오늘 날씨 정보 Weather.info를 알려 주기 위해서는 날씨 Database를 검색해야 합니다. 이처럼 목적지향 대화시스템은 도메인 지식이 담은 Database를 이용하여 응답하는 것이 일반적입니다.</p>
				</div>				
				<h5 id="1_2menu">대화모델과 대화처리 응용 개발 도구</h5>																						
				<div>
					<h6 id="1_2_1menu">대화모델 정의</h6>										
					<p>대화시스템이 도메인에서 사용자와 대화하기 위한 방법을 구현한 것을 대화모델이라고 합니다. 도메인 대화모델을 구축하기 위해서는 먼저 도메인 서비스(처리)를 위한 Entity와 의도(Intent)를 정의하고, Entity와 의도로 표현한 대화이력 상태(대화상태)에 따라 시스템이 어떻게 응답할 것인가를 정의합니다. 도메인 대화모델을 정의하면 대화이해 모듈과 대화관리/생성 모듈이 정의한 대화모델에서 동작하도록 지식을 구체화합니다. 이 지식을 이용하여 대화시스템은 정의한 대화모델처럼 사용자와 대화합니다. GenieDialog(대화처리 응용 개발 도구)는 도메인 대화모델을 설계하는 도구이며, 이 도구를 통해 제작된 대화모델을 위한 지식을 구체적으로 작성하는 도구입니다.  GenieDialog은 또한 ETRI 목적지향 대화시스템이 구현된 모델대로 동작하는가를 확인하고 대화모델과 지식을 수정하도록 합니다.</p>
				</div>				
				<div>
					<h6 id="1_2_2menu">Entity</h6>										
					<p>Entity는 도메인 대화모델에서 사용자와 시스템이 주고 받는 개념들입니다. 예를 들어, 도시와 날짜로 날씨 정보를 알려 주는 날씨 검색 서비스에 필요한 Entity는 다음과 같이 할 수 있습니다.</p>
					<table summary="Entity">
						<caption class="hide">Entity</caption>
						<colgroup>
							<col width="33.3%" />
							<col width="33.3%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="subtitle">
								<th>Entity</th>
								<th>표기법</th>
								<th>설명</th>
							</tr>
							<tr>
								<td>도시</td>
								<td>Weather.city</td>
								<td>날씨 지역 정보 (예, 서울, 부산, 대구, …)</td>
							</tr>
							<tr>
								<td>날짜</td>
								<td>Weather.date</td>
								<td>날씨 날짜 정보 (예, 오늘, 내일, 모레, …)</td>
							</tr>
							<tr>
								<td>날씨 정보</td>
								<td>Weather.info</td>
								<td>날씨 정보 (예, 맑음, 흐림, 비, …)</td>
							</tr>
						</tbody>
					</table>					
					<p>Entity는 크게 Class와 Slot으로 구분합니다. Class로 특정 개념을 정의하고, Slot으로 Class 의 속성 또는 구성요소 등을 정의합니다. 위의 예에서, 날씨 개념 Weather Class에 city, date, info Slot들이 정의되어 있습니다. 복잡한 도메인에서는 여러 개의 Class를 정의할 수 있습니다. 예를 들어, 날씨와 호텔 검색이 모두 필요한 도메인에서 날씨 Weather Class와 호텔 Hotel Class를 둘 수 있습니다. 그리고 같은 도시 의미가 Weather Class의 도시와 Hotel Class의 소재 도시에 대한 Entity로 구분될 수 있습니다. </p>
				</div>
				<div>
					<h6 id="1_2_3menu">의도(Intent)</h6>										
					<p>의도는 도메인 대화모델에서 사용자가 말할 수 있는 내용(질문, 응답, 정보제공 등)들과 그에 대한 시스템 응답들을 유사한 것끼리 분류한 것입니다. 예를 들어 날씨 검색 서비스에서 사용자 질문과 시스템 응답을 다음 표로 분류할 수 있습니다.</p>
					<table summary="Intent">
						<caption class="hide">Intent</caption>
						<colgroup>
							<col width="33.3%" />
							<col width="33.3%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th colspan="3">사용자 의도</th>
							</tr>
							<tr class="subtitle">
								<th>의도 분류</th>
								<th>표기법</th>
								<th>실제 예 </th>
							</tr>
							<tr>
								<td>인사하기 </td>
								<td>hello() </td>
								<td>안녕, 안녕하세요, 잘 있었어 …</td>
							</tr>
							<tr>
								<td>날씨 묻기 </td>
								<td>request(Weather.info)</td>
								<td>오늘 대구 날씨는 어때?, 날씨가 어떻지? … </td>
							</tr>
							<tr>
								<td>정보 제공</td>
								<td>inform()</td>
								<td>서울, 알고 싶은 날짜는 내일이야, … </td>
							</tr>
							<tr>
								<td>끝인사</td>
								<td>bye() </td>
								<td>그만하자, 잘 있어, … </td>
							</tr>
						</tbody>
					</table>					
					<table summary="Intent">
						<caption class="hide">Intent</caption>
						<colgroup>
							<col width="33.3%" />
							<col width="33.3%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th colspan="3">시스템 의도</th>
							</tr>
							<tr class="subtitle">
								<th>의도 분류</th>
								<th>표기법</th>
								<th>실제 예 </th>
							</tr>
							<tr>
								<td>인사하기 </td>
								<td>hello() </td>
								<td>안녕하세요?</td>
							</tr>
							<tr>
								<td>정보제공 </td>
								<td>inform() </td>
								<td>오늘 날씨는 맑음입니다. 현재 온도는 23도입니다.</td>
							</tr>
							<tr>
								<td>도시 질문 </td>
								<td>request(Weather.city) </td>
								<td>날씨를 알고 싶은 도시는 어디입니까?</td>
							</tr>
							<tr>
								<td>날짜 질문 </td>
								<td>request(Weather.date)</td>
								<td>언제 날씨를 알려드릴까요? </td>
							</tr>
							<tr>
								<td>끝인사 </td>
								<td>bye() </td>
								<td>안녕히 계세요. 다음에 뵙겠습니다.</td>
							</tr>
						</tbody>
					</table>					
					<p>의도는 술어논리(Predicate Logic) 형태로 표현합니다. 술어(Predicate) 부분은 도메인 응용 개발자가 영문자와 숫자, “_”로 마음대로 기술할 수 있습니다. request 는 미리 정의된 술부로 값이 할당되지 않은 요청 Slot 을 변수로 하나 이상 가집니다. 요청 Slot 은 해당 Slot 에 대한 값을 요청하는 것을 의미합니다. 사용자가 시스템에게, 시스템이 사용자에게 모두 요청할 수 있습니다. 보다 자세한 내용은 <a href="#" onclick="fnMove('#5_2menu');">Intent 정의</a>를 참조하기 바랍니다.</p>
					<p>지금까지 날씨 도메인 대화모델의 의도와 Entity 로 사용자의 입력 “오늘 날씨가 어떻지?”를 표현하면, 사용자 발화를 의도 분류 “request(Weather.info)”와 Entity 정보 Weather.date=”오늘”로 표현할 수 있습니다. 이를 하나로 “request(Weather.info, Weather.date=”오늘”)”로 표기하고 사용자 입력 의도라고 합니다. 의도 분류 “request(Weather.info)”를 의도 Type이라고 정의합니다. 그리고, request 와 같이 술어 부분만을 따로 Dialog Act(DA) Type으로 정의합니다. </p>
					<p>상기 날씨 검색 서비스 도메인에서 사용자 발화에 대한 의도 Type 은 표와 같이 4 개로 정의됩니다. 하지만 사용자 발화 의도는 request(Weather.info) 의도 Type 에 날짜와 도시 정보가 모두 있거나 없거나 또는 하나씩만 있는 경우에 따라 4 가지가 가능하고, inform() 의도 Type 에서도 3 가지 다른 의도가 가능하기에, 나머지 2 개 의도 hello()와 bye()와 합쳐서 모두 9가지가 가능합니다.</p>
				</div>
				<div>
					<h6 id="1_2_4menu">Entity 와 의도에 의한 대화모델 동작 예 </h6>										
					<p>날씨 검색 서비스 도메인의 Entity 와 의도가 정의되면, 각 대화이력에서 알맞은 시스템 응답을 정의하여 날씨 검색 서비스에서 사용자와 대화하는 방식인 대화모델을 정의할 수 있습니다. 대화이력은 현재까지 사용자가 입력한 Entity 의 값 정보와 사용자와 시스템이 지금까지 대화한 의도로 정의합니다. 우리 대화시스템에서는 Task 정보를 추가하여 사용합니다. 다수 업무를 같이 처리하기 위해 Task 개념을 도입하여 보다 복잡한 대화모델이 가능하도록 했습니다. 자세한 내용은 <a href="#" onclick="fnMove('#3menu');">Task Graph</a>에서 파악하기 바랍니다.</p>
					<p>날씨 검색 서비스에서 날짜 정보 Weather.date 을 사용자가 발화한 적이 없다면, 시스템은 request(Weather.date) 의도로 사용자에게 날짜 정보를 요청하도록 규칙을 정할 수 있습니다. 또한 도시 정보 Weather.city 가 없으면, 시스템은 request(Weather.city)로 날짜 정보를 요구하는 규칙을, 두 정보를 모두 사용자가 발화했다면 시스템은 inform() 의도로 날씨 정보를 제공하게 하는 규칙을 정할 수 있습니다. 추가로 인사에 대해 인사로 응답을, 끝인사에 대해 끝인사로 응답하는 규칙을 정의하면 날씨 검색 서비스에서 우리가 정의한 의도에 대해 모두 응답이 가능한 대화모델을 정의할 수 있습니다. 날씨 도메인의 경우는 대화이력에서 Entity 의 Slot 정보가 채워졌는가에 따라 시스템 응답을 정의할 수 있습니다. 이와 같이 Slot 정보를 채워졌는가에 따라 대화하는 대화모델링 방식을 slot-filling 대화모델링 방식이라고 합니다. </p>
					<p>위에서처럼 도메인을 위한 정상적인 대화 규칙을 대화모델화 하였다면, 추가적으로 예외적인 경우를 위한 시스템 응답 규칙들을 추가하면 보다 실제적인 서비스가 가능합니다. 날씨 도메인의 경우에 준비된 날짜와 도시가 아닌 경우에는 알고 있는 날짜 범위를 알려주거나 그 도시는 정보를 가지고 있지 않다는 정보를 제공하는 규칙을 추가하는 것입니다.</p>
					<p>날씨 도메인과 같이 단순한 도메인이 아닌 경우에는 시스템 의도들을 미리 정의하기가 어렵습니다. 복잡한 도메인에서는 Entity와 사용자 의도를 미리 정의해 가면서 가능한 대화이력에 따라 어떠한 시스템 의도를 발화할 것인가를 단계적으로 작성하는 것이 편리할 것입니다. </p>
				</div>
				<h5 id="1_3menu">Task Graph 대화모델 </h5>										
				<div>										
					<p>GenieDialog 의 목적지향 대화시스템은 Task Graph 대화모델을 기반하였습니다. Task Graph 대화모델은 복잡한 도메인을 작은 Task 단위로 구분하고 그 Task 간의 수행 순서를 대화이력에 따라 규칙화할 수 있습니다. 따라서, 보다 작은 영역의 Task 단위로 대화모델을 설계하고 이를 결합하여 복잡한 업무를 처리하게 합니다. </p>
					<p>Task Graph 대화모델에서 각 Task 는 일종의 작은 도메인입니다. 각 Task 단위로 Entities 와 Intents를 구분하여 정의하여 Task 단위로 대화모델을 설계할 수 있습니다. 한 도메인에서 작성한 Task는 다른 도메인에서 활용할 수 있도록 GenieDialog 에서 지원하고 있습니다. </p>				
				</div>
				<h5 id="1_4menu">대화이해  </h5>										
				<div>										
					<p>사용자 입력을 도메인 대화모델에서 정의한 사용자 의도 Type 과 Entity 로 인식하는 것을 대화이해라고 합니다. 대화이해는 크게 2 가지 일, 의도 Type 분류과 Entity 의 Slot 값 태깅(tagging)을 수행합니다. </p>
					<p>날씨 도메인에서 사용자 입력 문장이 “오늘 서울 날씨 알려줘”이라고 가정합시다. 의도 Type 분류는 미리 정의한 의도 hello(), inform(), request(Weather.info), bye()와 정의되지 않은 의도 unknown() 중에서 가장 가능성이 높은 의도 Type 을 선택합니다. 그리고, Slot 값 태깅은 “오늘 서울 날씨 알려줘”라는 문장 중에서 Entity 의 Slot 값이 되는 것을 찾아서 표시합니다. 그 결과로 문장 중에서 Weather.date=”오늘”과 Weather.city=”서울”을 특정 Slot 의 값으로 태깅하여 인식합니다. </p>
					<p>대화이해에서 2 가지 문제를 해결할 수 있는 정보를 시스템에게 제공해 주고, 이를 학습해야 의도 Type 분류와 Slot 값 태깅을 할 수 있습니다. 학습하기 위해 제공하는 데이터를 학습데이터라고 합니다. GenieDialog 에서 Intents 를 작성하면서 그 의도로 발화 가능한 문장들을 기록합니다. 이 문장들의 특징을 학습하여 추후에 사용자 입력 문장을 어떤 의도 Type 으로 분류할 것인가를 판단합니다. 또한, 문장에 나타나는 Slot 값을 표시하도록 요청합니다. 문장에 어떤 단어들이 어떤 Slot 인가 정보를 학습하여 문장 내에서 Slot 값을 알아 냅니다. </p>
					<p>GenieDialog 에서는 Task 단위로 Intents 를 정의할 수 있게 지원하여 Task 정보를 대화이해 학습에 활용합니다. Task 정보를 활용한 대화이해 학습은 같은 단어가 Task에 따라 다른 Slot으로 인식하거나 유사한 발화를 Task 에 따라 다른 의도 Type으로 분류하는데 도움을 줄 수 있습니다. 예를 들어, 호텔/날씨 검색 도메인에서 “대구”라는 사용자 발화가 inform(Hotel.city=”대구”) 또는 inform(Weather.city=”대구”)가 될 수 있습니다. 이 경우에 호텔 Task 와 날씨 Task 를 분리하고 위의 발화를 각 Task 에서 정의하였다면, 현재 진행 중인 Task 정보를 이용하여 보다 정확하게 인식할 수 있습니다. </p>				
				</div>
				<h5 id="1_5menu">대화관리/생성  </h5>										
				<div>										
					<p>대화관리/생성 모듈은 현재 대화이력(Dialog History)과 사용자 의도에 적합한 시스템 응답을 생성하고 진행한 대화이력을 관리합니다. 대화이력은 대화상태(Dialog State)라고도 하며, 사용자와 시스템 간에 주고 받은 정보입니다. 구체적으로는 Task, 의도와 Entity의 이력 및 상태입니다. Task 이력은 Task 이동 상태와 현재 Task 정보이며, 의도 이력은 주고 받은 의도와 현재 사용자 의도 정보이고, Entity 이력은 현재까지 저장된 Entity 값 정보와 이 Entity 정보로 검색 가능한 도메인 지식 정보입니다. </p>
					<p>대화관리/생성 모듈에서는 기술한 Task 이동 규칙, 사용자 의도에 대한 시스템 응답 규칙과 slotfilling 규칙 중에서 현재 대화이력과 사용자 발화 의도가 만족하는 규칙을 찾아서 Task 를 이동하거나 만족하는 응답 규칙과 slot-filling 규칙을 찾아서 그 규칙에 기술된 시스템 응답을 제공하고 변환된 대화 상태를 대화이력에 관리합니다. </p>
					<p>예를 들어, 날씨 도메인에서 Entity 이력 정보에 Weather.city 나 Weather.date 가 없다면 각 없는 Slot 값을 문의하는 시스템 발화 규칙과 모두 있다면 “<Weather.date> <Weather.city>의 날씨는 <Weather.info>입니다.”라고 응답하는 규칙을 정의할 수 있습니다. 이렇게 정의된 대화모델 기반에서 대화관리/생성 모듈은 사용자가 “오늘 날씨는”이라고 문의한다면, 대화이력에 Weather.date=”오늘”만이 있기에 상기 규칙 중에서 Weather.city를 문의하는 시스템 응답 “날씨를 알고 싶은 도시는 어디입니까? “를 출력하게 됩니다. 사용자가 이에 대한 응답으로 “대구”라고 발화하면 대화이력에 필요한 2개 Slot 에 모든 값이 충족되어 시스템 발화 패턴 “<Weather.date> <Weather.city>의 날씨는 <Weather.info>입니다.”라는 것을 선택하고 패턴의 변수 부분에 들어갈 값을 대화이력의 Slot 정보와 도메인 지식 정보에서 검색하여 완전한 문장 “오늘 서울의 날씨는 맑음입니다.”를 생성합니다. </p>
				</div>
				<h5 id="1_6menu">도메인 지식제공 </h5>										
				<div>										
					<p>날씨 도메인의 예에서 보듯이, 목적지향 대화시스템은 사용자 발화 정보들로부터 도메인의 특정 정보 및 작업을 사용자에게 제공합니다. GenieDialog 에서는 도메인 지식 정보를 Entities 의 Class 단위마다 Database Table 화하여 저장합니다. GenieDialog 에서는 사용자가 발화한 Slot 값을 조건으로 다른 Slot 값을 Database Table 에서 검색하여 도메인 지식 정보를 활용 가능합니다. </p>
					<p>날씨 도메인에서 실제 날씨 정보로 대화하기 위해서는 매일 또는 일정 시간마다 변경하는 날씨 정보를 검색해야 합니다. 하지만, GenieDialog 는 이와 같은 외부에서 제공하는 모든 종류의 Database와 직접 연결할 수 있는 도구를 제공하지 않습니다. 이와 같이 도메인 지식 정보를 응용 도메인에 따라 응용 개발자가 따로 구현해야 할 필요도 있습니다. GenieDialog 의 개념도에서 도메인 지식제공 모듈이 이에 해당합니다.</p>
					<p>도메인의 원하는 Slot 값을 Database Table 의 단순한 검색으로만 모두 가능하지 않습니다. 예를 들어, 날씨 도메인이라도 “오늘 온도가 최저인 도시와 최고 도시 차이는 얼마야?”라고 할 때, 온도를 검색해서 최저와 최고의 도시를 찾고 그 값의 차이를 파악해야 합니다. 단순한 도시와 날짜를 알아서 정보를 DB 에서 검색하는 것만으로 불가능한 것이 많습니다. GenieDialog 에서는 Database Table 에서 Field(Slot) 값을 조건으로 다른 Field 를 검색하는 기능만을 제공합니다. 따라서, 추가적인 연산 처리와 기타 처리는 제공하지 못합니다. </p>
					<p>GenieDialog 에서는 위와 같은 추가적인 연산 처리가 필요한 Slot 값을 연산 처리한 결과로 미리 Database Table화하여 저장해서, 대화시스템이 작동하는지를 검사하기 바랍니다. 상기 예의 “오늘 온도가 최저인 도시와 최고인 도시의 차이는 얼마야?”에 대한 의도를 request(Weather. temp_diff, Weather.date=”오늘”)이라면(Weather.temp_diff 는 도시간 온도 차이 Slot), 다음 Database Table Record 를 Weather Class instance DB 에 넣어서 간단한 검색으로 17 도가 출력되도록 하여 테스트해야 합니다. </p>										
					<table summary="Entity">
						<caption class="hide">Entity</caption>
						<colgroup>
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="subtitle">
								<th>Weather.city </th>
								<th>Weather.date </th>
								<th>Weather.temp_diff </th>
								<th>Weather.info </th>
							</tr>
							<tr class="center">
								<td>-</td>
								<td>오늘</td>
								<td>17</td>
								<td>-</td>
							</tr>
						</tbody>
					</table>					
					<p>이처럼 GenieDialog 에서 테스트를 수행한 후에는 외부에서 실제로 날씨 DB 만을 가지고 매일 Weather.temp_diff 를 연산하도록 도메인 지식제공 모듈에 구현해야 합니다. 외부에서 구현한 도메인 지식제공 모듈과 연동하여 사용에 대한 테스트는 ETRI Open API 의 대화처리 Open API 에서 구축한 도메인을 이용할 때에 access_method 를 “external_data”로 설정해서 하기 바랍니다. 자세한 내용은 ETRI Open API 사이트를 방문해서 매뉴얼을 참조하거나 본 매뉴얼의 <a href="#" onclick="fnMove('#8menu');">대화처리 Open API 사용</a>을 참조하기 바랍니다. </p>					
				</div>
								
				<!-- 2장 start -->				
				<h4 id="2menu">도메인(Domain)</h4>
				<div>					
					<p>도메인(Domain)은 대화로 서비스할 대화모델의 단위입니다. 도메인의 예로는 “날씨”, “상품 주문”, “피자 주문” 등을 들 수 있습니다. 대화시스템에서는 각 도메인 별로 대화를 인지하고 응답을 하게 됩니다. 따라서, 도메인마다 독립적인 별개의 대화시스템으로 동작합니다.</p>					
					<p>도메인은 Task, Entity 와 Intent 로 구성되어 있습니다. 따라서, 도메인에 맞는 대화모델을 구축한다는 것은 Task, Entity 와 Intent 를 정의하는 것입니다. </p>
					<p>도메인은 대화서비스 개발자가 임의로 정의할 수 있습니다. 하나의 작업만 가능한 도메인 (예를 들어, “날씨”, “피자주문”, “지하철 검색” 등)들을 개발할 수도 있고, 여러 작업이 결합한 도메인(예를 들어, 정보 검색(날씨, 지하철, 지도 등), 다수 상품 주문 처리) 도메인을 구축할 수 있습니다. 작은 작업 범위의 도메인일수록 개발하기 쉽고 보다 나은 성능을 쉽게 보일 수 있습니다. 하지만 복잡한 도메인일수록 높은 성능을 가지기 어려울 수 있습니다. 하지만 개발자가 복잡한 도메인이라도 대화시스템이 잘 동작하도록 Task, Entity와 Intent를 구성하면 좋은 성능을 보일 수 있습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><p><b>[Open API 사용자 제한]</b><p> 
									<p>Open API 등록키로 등록한 사용자는 다음과 같은 제한을 가집니다.</p>
									<ol>
										<li>생성 가능한 도메인 수: 10개 도메인으로 제한</li>
										<li>한 도메인 당 구축 가능한 사용자 문장 수: 3,000 문장으로 제한</li>
										<li>한 도메인 당 슬롯 정의 단어 전체 수: 4,000 단어로 제한</li>
										<li>한 Class 당 Instance Record 전체 수: 2,000개로 제한</li>
									</ol>
								</td>
							</tr>
						</tbody>
					</table>						
				</div>				
				<h5 id="2_1menu">도메인 생성(Create Domain)</h5>										
				<div>										
					<p>도메인 이름(아래 그림에는 기존 도메인 없어서 null 부분이 도메인 이름입니다.) 옆의 <img class="img30" src="../manualfiles/image003.png" />를 눌러 주면, 도메인 관리를 위한 메뉴가 보인다. </p>
					<p><img src="../manualfiles/image004.png" /></p>
					<p>아래처럼 Domain List 와 + Create Domain 메뉴를 볼 수 있다.</p>
					<p><img src="../manualfiles/image005.png" /></p>
					<p>새로운 도메인은 아래의 + Create Domain 을 클릭하여, 이름과 설명(Description)을 작성하고 저장하면 됩니다.</p>
					<p><img src="../manualfiles/image006.png" /></p>
					<p>도메인 이름은 영문자와 숫자 “_”의 조합으로 작성할 수 있습니다. 예를 들어, “Weather”, “Pizza_Order”, “Navigation_V2” 등이 가능합니다. </p>
					<p>도메인 설명은 새로운 도메인에 대한 설명으로 참조 용도로만 사용됩니다. 대화시스템의 지식으로는 활용되지 않습니다.</p>
				</div>																								
				<div>
					<h6 id="2_1_1menu">챗봇 사용하기 </h6>										
					<p>챗봇 사용하기를 도메인에서 챗봇과 연동할 수 있습니다. 챗봇과의 연동은 대화시스템이 사용자 발화가 도메인에서 이해할 수 없는 의도로 인식하면 도메인 대화모델에서 응답을 하지 못하게 되기에 그 대답을 챗봇의 응답으로 대신 합니다. 예를 들어, “사랑은 뭐라고 생각하니?”에 대해 “뜨겁고 아프고 힘들고 행복한 것이요.응답과 구분하기 위해서, 챗봇 출력 문장 좌우에 “(chat) (/chat)”을 붙여서 출력하고 있습니다. 상기 예에서는 “(chat) 뜨겁고 아프고 힘들고 행복한 것이요. (/chat)”으로 출력됩니다. 그리고, 챗봇 응답이 없을 경우에는 “(chat) (/chat)”이 출력됩니다. 최종 서비스에서는 이 태그 기호를 이용하여 다른 글자색이나 폰트로 출력할 수 있습니다.</p>
					<p>연동하는 챗봇은 사용자 입력에 대해 일반적인 잡담(Chit-chat)으로 응답합니다. 이 챗봇은 약 21만 대화쌍 집합 기반 검색 모델과 약 150만 대화쌍 대화코퍼스로 학습한 심화학습 모델(DeepNeural Net Model)과 결합하여 구축한 대화시스템입니다. 먼저 21 만 대화쌍 집합에서 높은 매칭률로 검색되면 검색된 응답을, 그렇지 않을 경우에는 학습한 심화학습 모델로 응답을 자동으로 생성합니다. 경우에 따라서는 대답이 없을 수도 있습니다. 많은 데이터를 사용하지 않아서 좋은 답변을 내지 못할 수도 있습니다.</p>
				</div>
				<div>
					<h6 id="2_1_2menu">Sample Domain 으로 시작하기 </h6>										
					<p>GenieDialog 에서는 샘플 도메인을 제공합니다. 샘플도메인은 GenieDialog 사용법과 형상을 검토하기 위한 용도입니다. 샘플 도메인에서 작성한 내용을 가지고 새로운 도메인으로 새로운 이름을 부여합니다. 샘플 도메인을 그대로 활용 및 그 작성 내용을 확인할 수 있고, 이를 바탕으로 보다 나은 도메인으로 개선할 수 있습니다. 샘플도메인은 제한적인 범위에서 샘플로 구축되어 있기에 모든 대화가 가능하지 않습니다. 샘플도메인들에 대한 설명은 <a href="#" onclick="fnMove('#0append');">[부록 1] 샘플 도메인 설명</a>를 참조하기 바랍니다. </p>
					<p class="center"><img src="../manualfiles/image007.png" /></p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><b>[주의] Sample Domain 을 선택하여 새로 만드신 경우에, 도메인을 처음부터 만든 경우와 마찬가지로 학습과 지식저장을 해야 대화시스템을 구동할 수 있습니다. </b></td>
							</tr>
						</tbody>
					</table>	
				</div>
				<h5 id="2_2menu">도메인 설정</h5>										
				<div>										
					<p>현재 도메인의 설정을 보여 주고 수정할 수 있습니다. 도메인 이름 옆의 <img class="img30" src="../manualfiles/image008.png" />버튼을 눌러 설정을 볼 수 있습니다. 도메인 생성 시에 기술한 내용으로 채워져 있고 변경할 수 있습니다. 생성 시에 없었던 내용에 대해서 이 절에서 설명합니다. </p>
					<p class="center"><img src="../manualfiles/image009.png" /></p>
				</div>
				<div>
					<h6 id="2_2_1menu">대화의도 필터링 </h6>										
					<p>대화이해에서 인식한 사용자 입력 발화의 의도가 주어진 한계치 이하일 때, 도메인에서 설정한 의도가 아니라고 판단하도록 합니다. 이러한 의도를 unknown()이라고 합니다.  </p>
					<p>대화의도 신뢰도 값은 도메인에서 기술한 문장들과 의도 Type 들에 따라 달라지기에, 여기에서 도메인에 따라 그 한계치를 설정하도록 하였습니다. 이 값은 반복적인 실험을 통해 경험적으로 부여해야 합니다. </p>
					<p>대화의도 필터링 한계를 0.35 정도로 추천합니다. 경험적으로, 0.3 ~ 0.4 이하로 인식된 의도는 오류일 가능성이 어느 정도 있었습니다. 0.7 ~ 0.8 이상인 경우에는 매우 정확하며 대부분 인식된 의도를 위한 학습 문장들과 매우 유사하였습니다. 따라서, 정의한 문장 위주로만 의도를 파악하고자 하면 이 한계치를 0.8 정도로 해 주면 됩니다. 이런 경우에 조금 변형된 문장들을 unknown()으로 인식하는 경향이 커지게 됩니다. </p>
				</div>	
				<div>
					<h6 id="2_2_2menu">버전 관리 </h6>										
					<p>현재 개발 중인 도메인을 저장하여, 개발 버전을 관리할 수 있는 기능을 제공합니다. 기존 버전으로 되돌리고자 할 경우에 저장한 버전을 다시 로딩하면 됩니다. 버전 관리는 최대 3개까지 가능합니다. 기존 버전에 새 버전을 다시 저장할 수 있습니다. </p>
				</div>	
				<h5 id="2_3menu">도메인 리스트 (Domain List) </h5>										
				<div>										
					<p>기존 생성한 도메인들을 보여 주고 선택하여 추가 작업을 진행할 수 있습니다. 그리고, 해당 도메인을 삭제할 수도 있습니다. </p>
					<p class="center"><img src="../manualfiles/image010.png" /></p>
				</div>
				
				<!-- 3장 start -->				
				<h4 id="3menu">Task Graph</h4>
				<div>					
					<p>Task Graph 는 도메인을 Task 들로 구성하고 Task 간 진행 순서로 표현하는 대화모델 방법입니다. Task Graph 는 Task 노드(Node)와 Task 간의 흐름을 결정하는 방향성 에지(Directed Edge)로 구성됩니다. Task 노드는 전체 작업의 부분을 나타냅니다. 방향성 에지는 Task 간의 대화 순서를 나타냅니다.</p>					
					<p>예를 들어, 피자 주문 도메인에 대한 Task Graph 를 그려보면 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image011.png" /></p>
					<p>구현할 피자 주문 도메인은 피자 주문과 음료 주문을 하고 결제를 수행하는 대화로 정의합니다. 이 때, 사용자 정보는 미리 로그인 되어 알고 있어 이에 대한 대화는 생략합니다. 이 피자 주문 도메인의 경우, “첫인사”, “피자 주문”, “음료 추가 여부”, “음료 주문”, “결제”, “끝인사”로 Task 를 세분화하고 이들 간의 순서를 정의하여 구성할 수 있습니다. 정의한 Task 들로 피자 주문 도메인을 Task Graph 로 나타내면 위와 같습니다. “END” Task 노드는 대화 종료 상태임을 나타내는 예약 정의된 Task입니다. 그러므로, “END” Task로 이동하게 되면 대화는 종료됩니다. </p>
					<p>편의를 위해서, Task 이름은 한글로 공백이 허용되게 임의로 작성했습니다. Task 이름은 도메인 이름과 마찬가지로 영문자와 숫자 “_”의 조합으로 작성해야 합니다. </p>					
				</div>
				<h5 id="3_1menu">Task 이력 관리 </h5>										
				<div>										
					<p>목적지향 대화시스템은 도메인의 Task Graph 로 대화를 진행합니다. 목적지향 대화시스템에서는 2개의 Task 이력을 관리합니다. </p>
					<ol class="decimal_ol">
						<li>진행 중인 Task 이력 (Stack 자료 구조로 관리): 종료되지 않은 Task 들을 Stack 자료 구조로 관리합니다. Stack 의 Top에 있는 Task 가 현재 진행 중인 Task 입니다. 현재 진행 중인 Task 가 완료되면 다음에 설명할 완료 Task 이력의 마지막에 들어가고, 지금 완료된 Task의 다음 Task를 Stack의 Top에 저장합니다. 만약 Top의 Task가 다음 Task가 없는 경우에는 완료 Task 이력으로 이동하고 Top 에서 삭제됩니다. 그러면, 기존 Stack의 Top1에 있던 Task가 Top이 되어, 현재 진행 중인 Task가 됩니다. 이 Task는 기존에 시작은 하였지만 완료하지 않은 상태에서 다른 Task 들이 진행되었다가 완료되어 다시 진행하게 된 것입니다.</li>
						<li>완료 Task 이력 (Queue 자료 구조로 관리): 완료된 Task 들을 완료된 순서대로 저장합니다.</li>
					</ol>
					<p>정상적으로 대화가 진행되면 진행 중인 Task 이력 Stack 에는 항상 현재 진행 중인 Task 만이 존재합니다. 즉, 위의 피자 주문 도메인을 보면, “첫인사”를 수행하고 완료하면 종료된 Task 로 이동하고, 다음 Task “피자주문”을 진행 중인 Task 이력 Stack에 저장됩니다. 또 “피자주문” Task가 완료되면 다음 Task 가 진행 중인 Task 이력Stack 에서 “피자주문” Task는 제거되고 다음 Task 를 저장합니다. 따라서, 진행 중인 Task 이력 Stack에는 단 하나의 Task 만 존재합니다. </p>
					<p>본 Task Graph 대화모델에서는 순서대로만 진행하는 방식 이외에 연결되어 있지 않는 Task 로 Jump 이동을 허용합니다. 이와 같은 경우에 현재 Task 가 완료되어 다음 Task 로 이동하지 않아서 진행 중인 Task 이력 Stack 에 미완성인 Task 들이 Stack 에 쌓이게 됩니다. Jump 된 Task 들이 종료되면서 미완성 Task 들이 현재 Task 가 되어 이전 업무를 이어갈 수 있습니다. Task Jump 이동은 Related Slots 과 Task Jump 이동 파트에서 살펴보도록 하겠습니다. </p>					
				</div>
				<h5 id="3_2menu">Task 생성(Create Task)</h5>										
				<div>										
					<p>도메인을 새로 생성하면 다음 화면으로 이동합니다. Task Graph 창에는 종료 Task END 가 생성되어 있습니다. END Task 로 이동하면 대화가 종료되기에, 새로 정의하는 Task 에서 대화를 종료하고자 할 경우에는 END Task로 이동하도록 연결하면 됩니다.</p>					
					<p class="center"><img src="../manualfiles/image012.png" /></p>
					<p>Task 생성은 상기 화면에서 <img class="img100" src="../manualfiles/image013.png" /> 버튼을 클릭하여 다른 Task 복사해오기를 통하여 기존에 다른 도메인에서 생성한 Task를 가져오거나 새로 Task를 정의하여 만들 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image014.png" /></p>
				</div>
				<div>
					<h6 id="3_2_1menu">다른 Task 복사해오기 </h6>										
					<p>기존 개발한 도메인들에서 구축한 Task 를 복사해서 새로운 Task 로 설정하는 기능입니다. <img class="img30" src="../manualfiles/image015.png" />를 클릭하여 기존에 만든 Task들을 검색하여 선택할 수 있습니다. </p>
					<p>Task 복사하게 되면 해당 Task에서 기술된 내용(Property) 및 Entities와 Intents 정보도 모두 같이 가져옵니다 </p>
				</div>
				<div>
					<h6 id="3_2_2menu">새로 만들기 </h6>										
					<p>Task 를 신규로 작성하고자 하면 새로 만들기 부분의 내용을 채워주면 됩니다. Task Name 은 영문자와 숫자와 “_” 조합으로 작성해야 합니다. Description 은 Task 를 설명하는 내용을 기술합니다. 이 내용은 대화 지식으로 활용하지 않고 개발자를 위한 참조 용도입니다.</p>
					<p>Initial을 체크하면 그 Task가 전체 도메인에서 시작 Task임을 표시합니다. 도메인 대화는 Initial이 체크된 Task 부터 시작합니다. Initial Task 는 도메인 전체에서 한 개입니다. 따라서, 한 Task 를 Initial로 하면 기존 Initial Task 는 해제됩니다.</p>
				</div>
				<div>
					<h6 id="3_2_3menu">Task 시작 시, 시스템 발화 정의 </h6>										
					<p>Task가 시작할(Task로 진입할) 때 시스템이 발화할 내용을 기술합니다. 한 Task에서 다른 Task로 전이될 때, 시스템이 발화하는 대화이기에 전이(Transition) 대화라고 정의합니다. <img class="img50" src="../manualfiles/image016.png" /> 버튼을 눌러 발화 내용을 작성합니다. 만약 Task 시작 시에 어떠한 시스템 발화도 하지 않겠다면 작성하지 않아도 됩니다. </p>
					<p>예를 들어, Task 가 인사 목적이라면 <img class="img50" src="../manualfiles/image016.png" />를 눌러 생긴 텍스트 칸에 다음처럼 “안녕하세요. 반갑습니다.”를 기술할 수 있습니다. 또한 tab 문자나 현재 작성 중인 텍스트 상자를 클릭하면, 아래에 새로운 텍스트 상자가 생기며 이곳에 유사한 발화를 추가로 작성할 수 있습니다. 여러 개의 유사 발화 문장들을 기술하면, 대화시스템은 이중에서 하나를 임의로 선택하여 발화합니다. 이처럼 같은 시스템 의도에 다양한 발화를 작성하면 같은 대화상태에서 매번 같은 시스템 발화가 생성되는 것을 방지할 수 있습니다. </p>
					<p class="center"><img src="../manualfiles/image017.png" /></p>
					<p>Task 가 시작할 때, 대화상태에 따라서 다른 발화를 해야한다고 한다면, 추가적으로 <img class="img50" src="../manualfiles/image016.png" />를 눌러 다른 발화 창을 생성하기 바랍니다. 그리고, 각 텍스트 상자 옆의 <img class="img10" src="../manualfiles/image018.png" />를 눌러서 대화상태에 대한 시스템 발화 조건을 다르게 기술하고 각 조건에 맞는 다른 응답을 작성하기 바랍니다. 아래 예는 사용자가 자기 이름 User.name 을 발화했는가에 따라 다른 시스템 대화가 되도록 한 것입니다. </p>
					<p class="center"><img src="../manualfiles/image019.png" /></p>
					<p>상기 시스템 발화 조건들에 Entity 함수는 Entity 이력을 검사하여 “User.name”이라는 Slot 에 사용자가 발화한 것이 false 인지 true 인지를 검사합니다. 따라서, 상기 예에서는 “User.name”이 대화이력에 있으면 그 값을 이용해서 출력합니다. “<User.name>” 변수 부분은 대화시스템이 대화이력과 도메인 Instance DB 를 검색하여 자동으로 채웁니다. 자세한 표기 방법은 <a href="#" onclick="fnMove('#5_5_2menu');">시스템 발화 패턴 작성 방법</a>을 참조하기 바랍니다.</p>
					<p>시스템 발화 조건은 직접 입력하기 보다는 <img class="img30" src="../manualfiles/image020.png" />를 클릭해서 조건 작성 창으로 작성하기 바랍니다. 자세한 조건 작성 방법은 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>을 보기 바랍니다. 조건을 작성할 때 일부 조건들만 작성하고 일부 조건은 기술하지 않을 수 있습니다. 실제 대화 상황에서 기술하지 않은 상태로 진행될 경우, 시스템은 그 대화상태에서 발화한 내용이 정의되어 있지 않아서 발화하지 못합니다. 반대로 조건들이 상호 배제적으로 기술되지 않아서 특정 대화상태에서 동시에 만족할 수도 있습니다. 이런 경우, 시스템은 임의로 조건이 만족된 시스템 대화를 선택하여 출력합니다. 도메인 개발자들은 이런 상황을 의도적으로 계획하지 않았기에, 실제 대화가 자기 의도와 다르게 시스템이 마음대로 발화하는 착각을 가질 수 있습니다. 시스템이 마음대로 대답하는 상황에 맞닥뜨리면, 시스템이 발화된 문장에 기술된 조건들이 의도와 다르게 조건이 만족될 수 있는지를 살펴보기 바랍니다.</p>
					<p>시스템 intent 는 기본적으로 inform()으로 정의되어 있습니다. 수정하지 않아도 되지만, 새로이 정의하면 작성한 시스템 대화가 발화되었는지 안 됐는지를 대화이력에서 새로이 작성한 intent 로 검색하는데 도움이 됩니다. 시스템 intent 는 영문자와 숫자, “_”로 작성할 수 있습니다. </p>
					<p>“시스템 발화 후 행동”은 이 시스템 발화 이후에 대화이력에서 Entity 이력을 변경할 수 있는 방법을 제공합니다. 즉, Entity 에서 정의된 Class 나 Slot 의 값을 특정한 값으로 설정(Set)하거나 초기화(Reset)할 수 있습니다. 자세한 내용은 <a href="#" onclick="fnMove('#6_2menu');">행동</a> 파트를 보기 바랍니다. </p>
				</div>
				<div>
					<h6 id="3_2_4menu">Task 저장 </h6>										
					<p>Task 생성 정보를 다 기록하면 Save 버튼으로 저장합니다. 저장 버튼을 누르면 다음과 같이 Task Graph 창으로 이동합니다. 새로 만들어지는 Task 는 Task Graph 창에서 왼쪽 상단에 있습니다. 기존 Task 가 그 위치에 있으면 겹칠 수 있습니다. 아래 화면은 종료 END Task 와 겹쳐져 있습니다. Task 노드들을 움직여서 보기 쉽게 배열하고 Save 버튼을 눌러 화면 상태를 저장하기 바랍니다. Task Graph 창에서 Task 노드 위치를 변경하고 유지하고 싶으면 Save 해야 합니다. </p>
					<p class="center"><img src="../manualfiles/image021.png" /></p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><p><b>[Task 이름 변경 및 삭제 주의]</b><p> 
									<p>작성한 Task 이름 변경 및 삭제는 각종 Condition에 사용한 기존 Task 이름을 변경 및 삭제하지 못합니다. 따라서, 이름 변경이나 삭제에 대해 사용자가 주의하여 수정하셔야 합니다.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="3_3menu">Task Graph 편집 </h5>										
				<div>
					<h6 id="3_3_1menu">Task 연결과 이동</h6>										
					<p>Task Graph 편집창은 도구에서 “Task Graph” 를 클릭하면 보입니다. 새로운 Task는  <img class="img100" src="../manualfiles/image013.png" /> 버튼을 클릭하여 생성합니다. 생성된 Task 노드 간의 순서를 나타내는 방향성 에지는 시작 Task 노드를 클릭하고 드래그하여 다음 Task 로 연결하면 생성됩니다.</p>
					<p class="center"><img src="../manualfiles/image022.png" /></p>
					<p>연결한 에지마다 다음 Task 로 이동하는 조건을 기술해야 합니다. 에지를 마우스로 클릭하면 상기처럼 Transition Condition 편집 부분이 보입니다. 이 조건도 <img class="img30" src="../manualfiles/image020.png" />버튼을 이용하여 작성할 수 있습니다. 자세한 조건 작성 방법은 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>을 참조하세요.</p>
					<p>Task 간 Transition Condition 의 조건은 다음 2 지점에서 검사합니다.</p>
					<p class="dep1">1) 사용자 발화 처리 후: 사용자 발화의 의도를 인식하여 대화이력에 사용자 발화 내용을 저장한 이후 검사합니다.</p>
					<p class="dep1">2) 시스템 발화 생성 후: 사용자 발화에 대해 시스템 응답이 정해지면 각 시스템 응답과 행동을 대화이력에 저장하고 나서 검사합니다. 시스템이 발화를 여러 번 하게 되면, 각 시스템 발화 출력 이후 매번 검사합니다.</p>
					<p>상기 각 지점에서 Transition Condition 이 만족하게 되면, <a href="#" onclick="fnMove('#3_1menu');">Task 이력</a> 관리에서 설명한 Task 가 완료되어 진행 중인 Task 이력 Stack 과 완료 Task 이력 Queue 에 이동이 생깁니다. Task 에 연결된 다음 Task들 중에서 현재 대화이력으로 Transition Condition 이 만족되는 Task 가 다수 개 있을 경우에는 그 중에 하나를 무작위 선택하여 이동합니다. </p>
					<p>살펴보았듯이, Task 이동 조건은 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>으로 기술합니다. 대화이력 조건에는 Entity 와 Intent, Task 의 이력 상태로 조건을 기술해야 하기에, Entities 와 Intents 를 먼저 정의해야 조건 기술이 가능합니다. 작성 중인 Task 에서 처리할 업무 수행을 위한 Entities 와 Intents 를 정리하지 않으면 다음 Task로 이동할 조건을 기술할 수 없습니다. </p>
					<p>다음은 예제 피자도메인을 Task Graph 창으로 직접 작성한 화면입니다. </p>
					<p class="center"><img src="../manualfiles/image023.png" /></p>
					<p>상기 예에서 Transition Condition 은 Intent 이력 검사를 2 개 실행하여 모두 만족하는 경우에 Ask_drink Task 에서 Get_drink_info Task 로 이동합니다. 상기 조건에서 Intent 함수는 첫번째 인자는 발화자 정보이고, 2 번째 인자는 DA Type 정보, 3 번째 인자는 몇 턴 전의 Intent 이력을 검사할 것인가에 대한 정보, 4 번째 인자는 전체가 true 인가 false 인가를 정의합니다. 따라서, Intent(“system”, “inform_pizza_price_and_ask_drink“, “1”, “true”)는 시스템이 inform_pizza_price_and_ask_drink 의도를 “1”턴 전에 발화한 것이 true 인가를 검사하는 조건입니다. Intent(“user”, “affirm”, “0”, “true”)는 사용자가 affirm 의도를 “0”턴, 즉 지금 발화한 것이 true 인가에 대한 조건입니다. 따라서, 상기 조건에 의해서 Ask_drink 에서 Get_drink_info Task 로의 이동은 사용자 발화 처리 이후에 발생합니다. Task 이동이 이루어지면, 시스템 응답은 Get_drink_info Task 의 전이 대화(만약 존재하다면)로 출력합니다. 시스템 발화가 어떻게 이루어지는가에 대한 자세한 설명은 <a href="#" onclick="fnMove('#5_5_1menu');">시스템 발화 종류와 응답 순서</a>에 있습니다.</p>
					<p><b>대화 턴: <br />상기 설명에 나타난 턴 개념은 시스템이나 사용자가 발화하는 턴을 일련의 번호를 매긴 것입니다. 따라서, 시스템이 처음 발화로 1 번째 턴을 시작하고, 사용자가 응답하여 또 한 턴을 증가하여 2 번째 턴을 합니다. 다시 시스템과 사용자가 반복하여 턴을 증가합니다. 이와 같이 턴을 주고 받는 대화를 턴 대화라고 하며, 일반적인 대화시스템은 턴 대화를 하는 것을 기반으로 개발됩니다.</b></p>					
				</div>
				<div>
					<h6 id="3_3_2menu">Task 와 에지 삭제 </h6>										
					<p>삭제하고 싶은 Task 나 에지를 마우스 오른쪽 버튼을 더블클릭하면 다음 화면과 같이 해당 Task 또는 연결을 삭제할 것인가를 묻는 팝업창이 생깁니다. “확인” 버튼을 누르게 되면 삭제가 됩니다. 본 도구에서는 undo 기능이 없기에 신중하게 처리하기 바랍니다.</p>
					<p class="center"><img src="../manualfiles/image024.png" /></p>									
				</div>
				<div>
					<h6 id="3_3_3menu">Task Graph 화면 저장 </h6>										
					<p>상기 Task Graph 창에서 Task 나 에지를 이동하여 재배치한 경우에 오른쪽 상단의  <img class="img60" src="../manualfiles/image025.png" />버튼으로 저장하여야만 재배치된 Task Graph 구성이 저장됩니다. </p>
				</div>
				<h5 id="3_4menu">Task Property </h5>										
				<div>															
					<p>Task의 속성(Property)을 다음과 같이 각 Task 이름 아래의 <img class="img60" src="../manualfiles/image026.png" />를 클릭하여 작성합니다. Task 생성 시에 보여진 속성들과 새로운 속성들이 있습니다. </p>
					<p>Task 생성 시에 작성한 속성으로는 “Task Name”, “Description”, “Initial”, “Task 시작 시, 시스템 발화 정의”가 있습니다. 이러한 속성은 Entities 와 Intents를 정의하지 않아도 정의가 가능합니다. </p>
					<p>Task 생성 시에 작성하지 않은 속성으로는 “Task Goal”, “Related Slots”, “Reset Slots”, “사용자에게 Slot 값 질문하기”가 있습니다. 대부분이 Slot 즉, Entity 와 관련이 있어서 Task 에서 이용할 Entity 를 먼저 정의해야 작성할 수 있습니다. 아직 Entities 와 Intents 에 익숙하지 않으신 분들은 먼저 Entities 와 Intents를 살펴보고 본 절을 보는 것이 좋습니다. </p>
					<p class="center"><img src="../manualfiles/image027.png" /></p>
				</div>
				<div>
					<h6 id="3_4_1menu">사용자에게 Slot 값 질문하기 – Slot Filling 대화 방식</h6>										
					<p>Slot Filling 을 수행하기 위해, Task 의 Property 에서 사용자에게 Slot 값 질문하기를 두었습니다. 여기에서 Task 에서 사용자에게 얻기를 원하는 Entity Slot 을 정의하고 그 Slot 을 얻기 위한 질문들을 정의합니다. Slot Filling 을 위한 시스템 대화를 진행 대화(Progress Dialogue)라고 정의합니다. Task 에서 정의된 진행 대화가 여러 개일 경우에는 순서대로 우선 순위를 가집니다. 대화시스템은 이 Task 를 진행할 때, 우선 순위로 아직 채워지지 않은 Slot 을 선택하고 그 진행 대화를 출력합니다. Task의 업무가 Slot Filling 으로 처리할 수 없는 경우에는 이 부분을 비워 두기 바랍니다.</p>
					<p>상기 피자 도메인의 Get_pizza_info Task 는 사용자가 원하는 피자 종류(Pizza.type), 피자 크기(Pizza.size)와 도우 종류(Pizza.dough)를 취합하고 이 순서대로 진행 대화들이 출력되도록 정의되어 있습니다. 또한, Get_pizza_info Task 의 전이 대화로 “주문하실 피자 종류, 크기, 도우를 말씀해주세요.”가 정의되어 있습니다. 사용자와 대화에서 Get_pizza_info Task 가 시작되면 전이 대화를 먼저 발화합니다. Task 의 전이 대화를 출력한 경우에는 진행 대화가 발화할 수 있는 조건이어도 발화하지 않습니다. 따라서, 시스템은 Get_pizza_info Task 가 시작할 때, “주문하실 피자 종류, 크기, 도우를 말씀해주세요.”를 발화합니다. 사용자가 피자 종류와 크기, 도우를 말하지 않을 경우에 시스템에서 진행 대화 중에서 우선 순위가 가장 높은 Pizza.type 에 대한 진행 대화 “어떤 종류의 피자를 주문하시겠습니까? (e.g. 페페로니, 치즈, 불고기 등)”을 출력합니다. 만약 사용자가 “페페로니 피자”라고 발화하면 다음 우선 순위 진행 대화인 Pizza.size 에 대한 질문을 수행합니다. 사용자가 “미디엄 페페로니 피자”라고 발화하여 Pizza.size 까지 동시에 발화하면 시스템은 마지막 Pizza.dough 에 대한 진행 대화를 발화합니다.</p>
					<p>진행 대화는 사용자 의도에 대한 시스템 응답 대화(Response Dialogue)를 먼저 발화한 후에 진행 대화를 발화합니다. 응답 대화는 각 Task 단위로 사용자 의도에 대해 시스템이 어떻게 응답하는가를 정의하는 것을 말하며 Intents 에서 작성합니다. 응답 대화는 <a href="#" onclick="fnMove('#5_3_2menu');">시스템 응답 정의 (응답 대화)</a>을 참고하고, 시스템이 발화하는 방법은 <a href="#" onclick="fnMove('#5_5_1menu');">시스템 발화 종류와 응답 순서</a>를 참고하기 바랍니다. </p>
				</div>
				<div>
					<h7 id="3_4_1_1menu">대화 작성 방법</h7>					
					<p></p>										
					<p>사용자에게 Slot 값 질문하기에 대화를 작성하기 위해서 <img class="img50" src="../manualfiles/image016.png" />를 눌러서 새로운 질문 창을 생성합니다. </p>
					<p class="center"><img src="../manualfiles/image028.png" /></p>
					<p>대상슬롯을 클릭하면 Entities 에서 정의한 Slot 들이 보입니다. 질문 대상이 되는 Slot 을 선택합니다. 그러면 아래처럼 녹색 창에도 같은 Slot 이름이 할당됩니다. </p>
					<p class="center"><img src="../manualfiles/image029.png" /></p>
					<p>녹색 창 옆에 시스템이 할 질문을 작성하면 됩니다. 탭 문자 또는 마우스로 시스템 발화 창을 클릭하여 추가적인 유사 발화들을 작성할 수 있습니다. </p>
					<p class="center"><img src="../manualfiles/image030.png" /></p>
					<p>다수 개의 대상 Slot 들을 작성한 이후에 우선 순위는 <img class="img10" src="../manualfiles/image031.png" />와 <img class="img10" src="../manualfiles/image032.png" />로 조정할 수 있습니다. </p>
					<p>상기 시스템 발화 텍스트 상자 옆의 <img class="img10" src="../manualfiles/image018.png" />를 눌러서 대화상태에 대한 시스템 발화 조건을 기술할 수 있습니다. 아래에 대한 자세한 기술 방법은 먼저 <a href="#" onclick="fnMove('#5_2menu');">Intent 정의</a> 파트를 보고 참조하기 바랍니다.</p>
					<p class="center"><img src="../manualfiles/image033.png" /></p>
					<p>상기 화면에서 대상슬롯과 녹색창에 같은 Pizza.type 이 기술되어 있습니다. 대상슬롯은 Slot Filling하기 위한 Slot 이름이고 녹색창에 기술된 Slot 은 시스템 Intent request() DA Type 에 대한 요청 Slot 입니다. 질문 DA Type 계열은 요청 Slot 들을 가지는데 그것을 표기한 것입니다. 대부분의 경우, Slot-Filling 을 위한 Slot 과 시스템 Intent DA Type “request”에 대한 요청 Slot 이 같습니다. 경우에 따라, 다르게 설정하실 수도 있습니다. </p>
					<p>대상슬롯과는 달리, 요청 Slot 들은 다수 개를 선택할 수 있습니다. 이를 잘 이용한다면 같은 대상슬롯이라도 요청 Slot 들을 다르게 정의할 수 있습니다. 예를 들어, 상기에서 피자 종류를 질문하기 전에, 피자 사이즈와 도우 종류를 사용자가 알려주지 않은 것을 알았다면 이 세 개 Slot 들을 모두 요청할 수 있습니다. 이러한 정보는 <img class="img30" src="../manualfiles/image020.png" />를 클릭해서 시스템 발화 조건을 정의할 수 있습니다. 대상슬롯이 Pizza.type 이면서 시스템 발화 조건이 다른 시스템 질문 발화들을 작성할 수도 있습니다. 그러면, Pizza.type 이 아직 발화되지 않은 상황에서 시스템은 발화 조건이 맞는 시스템 질문을 선택하게 됩니다. </p>
					<p>다음 사용자 발화 DA 제한은 시스템 Intent 의 DA Type이 request 로 시작하면 나타납니다. 다음 사용자 발화 DA 제한은 시스템이 Slot 요청을 하는 경우에 사용자의 다음 발화에서 그 Slot 을 강제로 인식하기 위한 정의입니다. 다음 사용자 발화 DA 제한 부분에 강제로 인식할 사용자 발화 의도의 DA Type을 작성하면 작동이 됩니다. 실제 대화 상황에서 요청 Slot을 포함한 시스템 발화 직후의 사용자 발화는 정의한 DA Type 으로 인식하고 사용자 발화 부분에서 요청한 Slot 을 인식합니다. 단, 사용자 발화가 명사구 형태이거나 주어 없이 서술격 조사 “~이”로 기술된 평서형일 경우에만 적용됩니다. 이런 형태의 사용자 발화는 특정 Slot 을 분별하거나 DA Type 을 인식할 중요한 단어가 없는 형태입니다. 도메인 내에 이런 형태의 DA Type 이 유일하지 않다면, 단순한 명사만으로는 다른 의도의 DA Type과 Slot으로 인식될 가능성이 높습니다. 학습 정보로는 이런 것을 구별하기가 어렵기에 이러한 정보를 활용하여 강제로 인식하게 합니다. 사용자의 다음 발화가 명사구 형태이거나 주어 없이 서술격 조사로 기술된 형태가 아닌 경우에는 학습 정보로 사용자 의도를 인식합니다. </p>
					<p>상기 예에서 다음 사용자 발화 DA 제한에 “inform_pizza”을 기술하면, 사용자 발화가 “물고기”, “빅 베이컨”, “베이컨이요” 등은 무조건 inform() DA Type 에 명사 부분 “물고기”, “빅 베이컨”, “베이컨”을 요청 Slot Pizza.type 으로 인식합니다. </p>
					<p>상기처럼 사용자 발화가 명사구 형태이거나 주어 없이 서술격 조사 “~이”로 기술된 평서형일 경우라도, 학습데이터에 나타날 Slot 을 다 표현할 정도로 작다면 학습코퍼스에 다 표현하는 것이 좋습니다. 또한, Slot 으로 발화할 단어들을 리스트로 관리할 수 있다면 <a href="#" onclick="fnMove('#4_2_2_2menu');">None Slot Type 과 New Defined Type</a>에서 설명하는 New defined type으로 Slot 을 설정하는 것이 보다 좋은 성능과 유지 관리를 할 수 있습니다. </p>
				</div>
				<div>
					<h6 id="3_4_2menu">Task Goal</h6>										
					<p>Task Goal 은 Task의 업무가 완료되는 조건입니다. Task Goal 은 다음 Task 로 이동하는 조건들을 OR 로 조합한 조건으로 정의할 수 있습니다. 다음 Task 로 연결이 있는 경우에는 Task Goal 을 편집할 수 없습니다. 이 경우, Task Goal 의 변경은 연결 조건을 변경하거나 삭제로 가능할 수 있습니다. </p>
					<p>Task Goal 은 다음 Task 로 연결이 없는 경우에는 직접 정의할 수 있습니다. 즉, Task Graph 에서 Task Node 가 다음 연결이 없이 존재하는 경우입니다. 이렇게 정의한 Task 는 종료만 되고 다음 Task로 이동이 없습니다. 따라서 이러한 Task가 Task Goal이 만족하는 상황에서 Task가 완료되면, 진행 중인 Task 이력 Stack 에서 이 Task 이전에 시작했으나 아직 종료하지 않은 Task 가 새로운 현재 Task 가 됩니다. 만약 이미 진행했으나 완료하지 않은 Task 가 Slot Filling 방식으로 정의되었다면 아직 사용자가 요구하지 않은 Slot 을 문의하기 때문에 사용자에게 예전의 Task 로 복귀하였음을 알려줄 수 있습니다. Slot Filling 방식으로 정의되지 않은 Task 는 복귀하여도 시스템이 자동으로 복귀 사실을 알려주는 발화를 낼 수 없습니다.</p>
				</div>
				<div>
					<h6 id="3_4_3menu">Related Slots 과 Task Jump 이동 </h6>										
					<p>Task Graph 대화모델은 진행 중인 Task 내의 시스템 응답들을 검사하여 만족하는 응답을 찾아 발화합니다. 진행 중인 Task 내의 정의된 대화지식으로는 사용자 의도에 응답이 불가능하지만 다른 Task 에서는 가능한 경우가 있을 수 있습니다. 하지만, Task Graph 대화모델은 Task 작업 순서를 제약하여 대화를 진행하도록 설계되었기 때문에, 다른 Task 에서 가능한 응답이 있어도 하지 않습니다. 하지만 예를 들어, 어떤 Task 가 다른 모든 Task 에서 이동이 가능하다고 한다면 이러한 이동 조건을 모두 표현하면 Graph가 매우 복잡하고 유지 및 관리하기가 어려울 것입니다.  본 Task Graph 대화모델에서는 에지로 연결이 되어 있지 않아도 이동이 가능한 방법을 허용하여 Graph 를 보다 단순하게 유지 및 관리하게 했습니다. 에지 없이 하는 Task 이동을 Jump 라고 합니다. Jump 가능한 Task 는 에지 연결로 Task 이동도 같이 허용할 수 있습니다. </p>
					<p>다른 Task 에서 Jump 해서 진입이 가능한 Task 를 쉽게 구별할 수 있게 하기 위해서 Related Slots를 두었습니다. Related Slots 에는 Entity에서 정의한 Slot 들을 정의합니다.</p>
					<p>현재 진행 중인 Task 에서 사용자 발화에 대한 시스템 발화를 할 수 없으면, 사용자 의도에 나타난 Slot 들을 Related Slot 으로 정의한 Task 들을 검색합니다. 검색 순서는 현재 Task 에서 연결이 된 순서대로 찾으며 최종적으로는 연결되지 않은 Task 들까지 모두 검색합니다. 검색된 Task 중에서 현재 사용자 의도에 대한 응답 대화가 정의되어 있고 현재 대화상태에서 그 응답 대화가 출력하는 조건이 충족한다면 그 Task로 Jump합니다. Task 이력 Stack의 Top에 Jump한 Task 를 저장합니다. 이 때, Jump 한 Task 가 새로 시작했기에 전이 대화를 수행해야 하지만 Jump 한 이유가 조금 전에 찾은 응답 대화를 수행하기 위해서이기에 전이 대화를 발화하지 않고 그 응답 대화를 시스템 발화로 출력합니다. 만약 Jump한 Task 가 Slot Filling 형태로 <a href="#" onclick="fnMove('#5_5_1_2menu');">진행 대화</a>가 정의되어 있으면 응답 대화 다음에 우선 순위가 가장 높은 진행 대화도 출력합니다. </p>
					<p>예를 들어, 피자 주문 도메인에서 날씨 Task 를 Weather_Info 로 추가한다고 가정해 봅시다. 새로 정의한 Task 를 피자 주문 도메인의 Task Graph 에서 순서를 정의하기 위해서는 기존 피자 주문 도메인의 모든 Task들에서 날씨를 문의하면 Weather_Info Task로 연결하고 날씨를 알려주면 다시 이전 Task 로 다시 돌아가도록 연결을 해야 합니다. 하지만, Related Slots 를 도입하면 이것을 간단히 할 수 있습니다. Weather_Info Task 의 Related Slots 에 날씨 Class 의 Slot 들을 정의하고 Weather_Info Task 에서는 날씨 문의에 대한 사용자 의도에 대한 시스템 <a href="#" onclick="fnMove('#5_5_1_3menu');">응답 대화</a>를 정의해야 합니다. </p>
					<p>아래는 기존 피자 주문 도메인에 Weather_Info Task 를 독립적으로 추가한 예제입니다.</p>
					<p class="center"><img src="../manualfiles/image034.png" /></p>
					<p>그리고 아래는 Weather_Info Task 의 Property 에서 Related Slots 를 정의한 예입니다. </p>
					<p class="center"><img src="../manualfiles/image035.png" /></p>
					<p>Weather_Info Task 에서 날씨 문의에 대한 응답 대화를 정의해야 Task Jump 가 가능하기에 다음과 같이 Intents를 정의할 수 있습니다. </p>
					<p class="center"><img src="../manualfiles/image036.png" /></p>
					<p>사용자 의도 request(Weather.info)에 대해 시스템 응답 대화로, 도시와 날짜 정보를 모두 발화한 경우에는 시스템 응답 의도 inform_weather_info()로 날씨 정보를 알려주고, 도시와 날짜 정보 중에 하나라도 없는 경우에는 시스템 응답 의도는 default 의도인 inform()이고 응답 대화 패턴은 “”로 실제적으로 아무런 응답이 없도록 했습니다. Weather_Info Task 는 Slot Filling 방식의 진행 대화를 설정하였기에 Slot 을 모두 발화하지 않은 경우에는 응답 대화가 없어도 되기에 그 조건에 대해 응답 대화 “”가 출력되게 하고 이후 진행 대화에서 적절한 Slot 요청 대화를 출력하도록 했습니다. 이처럼 응답 대화가 필요 없다고 특정 조건에 대한 응답을 구축하지 않는다면 Jump할 Task에서의 응답 대화의 발화 가능성이 없기 때문에 Jump가 되지 않습니다. </p>
					<p>예를 들어, 피자 주문 도메인의 Get_pizza_info Task 에서 시스템이 “주문하실 피자 종류, 크기, 도우를 말씀해주세요.”라고 발화한 상황에서, 사용자가 “오늘 대구 날씨 어때?”라고 질문한다고 가정합시다. 아래는 GenieDialog 에서 작성한 도메인을 대화시스템을 구동한 예입니다. 동작 방법은 <a href="#" onclick="fnMove('#7menu');">대화시스템 구동</a>을 참조하기 바랍니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="center">
								<td><img src="../manualfiles/image037.png" /></td>
								<td><img src="../manualfiles/image038.png" /></td>
							</tr>
							<tr>
								<td>
									<p>현재 Task: Get_pizza_info 가 1턴에서 시작함 </p>
									<p>시스템 발화: Get_pizza_info 의 전이 대화를 발화함 </p>
								</td>
								<td>
									<p>현재 Task: Get_pizza_info 로 반환함</p> 
									<p>이전 Task: Weather_Info 가 Jump 이후 완료됨 </p>
									<p>시스템 발화: Weather_Info Task 의 응답 대화 “대구의 오늘 날씨는 맑음입니다.”와 Get_pizza_info 로 반환한 이후 진행 대화로 피자 종류 질문을 출력함</p>									
								</td>
							</tr>
						</tbody>
					</table>
					<p>사용자가 “날씨 어때”를 입력할 경우에는 아래처럼 Weather_Info Task 로 Jump 하여 응답 대화로 “”를 출력하고 진행 대화로 “어느 도시 날씨를 알고 싶으세요?”를 출력합니다.</p>
					<p class="center"><img src="../manualfiles/image039.png" /></p>
					<P>살펴보았듯이, Related Slots 와 Task Goal 를 이용하여 순서가 필요 없는 다른 업무 간의 이동이 가능하도록 할 수 있습니다. 물론, 순서가 있는 Task 간에도 Jump 를 정의할 수 있으나, Jump 로 건너뛴 Task 들이 수행되지 않기에 그 업무를 수행하지 않고도 계속 대화가 진행할 수 있는 경우에만 Jump하도록 Task Graph를 잘 설계해야 합니다. </P>
				</div>
				<div>
					<h6 id="3_4_4menu">Reset Slots </h6>										
					<p>Task Property 에 Reset Slots 로 설정한 Slot 들은 Task 로 진입할 때마다 Slot 값들이 초기값(_INIT_)으로 재설정됩니다. 상기 피자 주문 도메인에 추가한 Weather_Info Task 의 예에서 Reset Slots 에 Weather.date, Weather.city, Weather.info 로 설정하였기에 사용자가 날씨를 문의할 때마다 기존 문의한 내용은 사라지게 됩니다. </p>
				</div>
				
				<!-- 4장 start -->				
				<h4 id="4menu">Entities</h4>
				<div>					
					<p>Entity 는 도메인에서 사용자와 시스템이 주고 받는 개념들을 정의한 것입니다. Entity 는 Class 와 Slot 으로 구분합니다. Class 는 Slot 들로 구성되어 있습니다. Slot Entity는 Class Entity 의 구성 요소 또는 속성으로 볼 수 있습니다. 하나의 도메인에 Class 는 여러 개 또는 없을 수도 있습니다. </p>					
					<p>Entity는 도메인 전체에서 정의하는 Global Entity(All tasks 아래 표시)와 Task 마다 정의하는 Local Entity(각 Task 이름 아래 표시)로 나눌 수 있습니다. Task 에 국한된 Entity 는 Task 이름 아래에 있는 Entities를 통하여 구축하고, Global Entity는 도메인 아래에 있는 Entities 를 통하여 구축하면 됩니다. 다른 Task 에서 정의한 Local Entity 를 사용하고자 할 때는 상단 우측의 <img class="img100" src="../manualfiles/image040.png" />을 이용하여 참조하도록 정의하면 됩니다.</p>					
					<p>Task 별 Local Entity 를 선언과 Reference 로 특정 Task 에서 사용하는 Entity 들을 파악할 수 있습니다. 이 정보는 특정 Task 를 다른 도메인에서 사용하고자 할 때, 선택한 Task 와 관련된 Entity 들만 이식하기 위해서입니다. 특정 Task 에서 정의한 Local Entity, Reference 한 Entity 와 Global Entity 들을 함께 가지고 가서 다른 도메인에서 활용 가능하도록 합니다. Entity 정보는 Task 를 정의하고 Intents 를 정의하는데 사용되기에 기존 Entity 정보 변경은 Task 와 Intent 정의에도 영향을 미칩니다. 주의해서 Entity 변경이나 삭제를 해야 합니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><p><b>[Entity 이름 변경 및 삭제 주의]</b><p> 
									<p>작성한 Entity(Class와 Slot)의 이름을 변경하거나 삭제할 경우, 다음의 주의가 필요합니다.</p>
									<ol>
										<li>Task Property 정의에 사용한 기존 Class나 Slot 이름이 변경(삭제)되지 않습니다.</li>
										<li>Entities/Intents 작성 시에 사용한 기존 Class나 Slot 이름이 변경(삭제)되지 않습니다.</li>
										<li>각종 Condition과 Action에 정의한 Class나 Slot이름이 변경(삭제)되지 않습니다.</li>
									</ol>
									<p>따라서, 이름 변경이나 삭제에 대해 사용자가 주의하여 수정하셔야 합니다.</p>
								</td>
							</tr>
						</tbody>
					</table>									
				</div>
				<h5 id="4_1menu">Class </h5>
				<div>
					<h6 id="4_1_1menu">Class 생성 (Create Class) </h6>										
					<p class="center"><img class="img80" src="../manualfiles/image041.png" />를 클릭하여 도메인 및 Task에 새로운 Class를 생성할 수 있습니다. </p>
					<p class="center"><img src="../manualfiles/image042.png" /></p>
					<p>상기와 같이 Class Name 과 Description 을 작성하면 됩니다. Class Name 은 다른 이름 정의와 동일하게 영문자와 숫자, “_”로 작성할 수 있습니다. Class Name 을 Slot Name 과 구별하기 위해서 첫 문자를 대문자로 작성하면 구분에 편리합니다. Description 은 Class 에 대한 설명으로 실제 시스템에서는 활용하지 않습니다. </p>
					<p>작성한 Class에는 다음과 같이 4개 메뉴를 볼 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image043.png" /></p>
					<p>Create Slot 은 Class 에 속하는 Slot 를 생성하는 기능입니다. 일반적으로 Slot 간에는 관계가 있습니다. 임의 관계가 있는 Slot 들을 같은 Class 에 속하게 합니다. 자세한 내용은 <a href="#" onclick="fnMove('#4_2_1menu');">Slot 생성 (Create Slot)</a>을 확인하세요.</p>
					<p>Instances를 통해 Class에서 정의된 Slot들에 대한 Database Table을 작성합니다. 다음 절에서 더 자세하게 다루겠습니다.</p>
					<p>Edit 는 다음과 같이 Class 정보를 수정할 수 있습니다. 생성할 때 없었던 Defined task 는 Class가 속하는 Task를 설정하는 것입니다. 이것을 통해 다른 Task에서 정의한 것으로 변경 가능합니다.</p>
					<p class="center"><img src="../manualfiles/image044.png" /></p>
					<p>Delete는 해당 Class를 삭제합니다. </p>
				</div>
				<div>
					<h6 id="4_1_2menu">Instances</h6>
					<p>Class 의 Instance 는 Class 에서 정의된 Slot 들이 실제로 가지는 값들을 정의합니다. 도메인마다 특정한 지식이 필요한데 이러한 도메인 지식을 Class 의 Instance 를 통해 알 수 있습니다. 예를 들어, 날씨 도메인의 경우, Weather.date=”오늘”과 Weather.city=”서울”의 날씨 정보 Weather.info 는 Weather Class 의 Instances 에 2 개 슬롯에 대한 날씨 정보가 저장된 것으로 시스템이 응답이 가능합니다. 또한, 피자 주문 도메인인 경우에는 피자의 종류와 크기, 도우 종류에 따라 그 가격이 정해진 피자 메뉴 테이블이 필요합니다. 이러한 도메인 정보를 알기 위해, 피자 주문 도메인에서는 Pizza Class 에 type(피자 종류), size(피자 크기), dough(도우 종류), price(피자 가격)을 다음과 같이 생성할 필요가 있습니다.</p>
					<p class="center"><img src="../manualfiles/image045.png" /></p>
					<p>Pizza Class 에서 Instances 를 클릭하면 다음과 같이 정의한 Slot 들에 대한 Database Table 을 작성하는 양식이 보입니다. 아래처럼 직접 타이핑을 하여 작성하고 SAVE로 저장하기 바랍니다. </p>
					<p class="center"><img src="../manualfiles/image046.png" /></p>
					<p class="center"><img class="img100" src="../manualfiles/image047.png" />를 통하여 작성한 내용을 엑셀 파일 형식으로 저장이 가능합니다. 아래와 같이 슬롯 이름과 그 값들이 들어간 형태입니다. </p>
					<p class="center"><img src="../manualfiles/image048.png" /></p>
					<p>미리 이와 같은 Table 정보를 가진 경우이라면, 위의 엑셀파일 형식으로 작성해서 <img class="img100" src="../manualfiles/image049.png" />로 Class의 Instances 정보를 채울 수 있습니다. </p>
					<p>우리는 이와 같은 Class Instances 정보를 도메인 지식베이스(Knowledge Base)라고 하고 줄여서 도메인 KB 라고 합니다. 대화시스템은 이러한 Database 로 정형화된 도메인 KB 를 통하여 사용자에게 정보를 제공하거나 일 처리를 할 수 있습니다. 이때, 사용자 발화에 나타난 Slot 값들을 AND 논리연산자로 결합하여 Class (Class 에 정의된 Instance DB)내의 다른 Slot 값들을 검색하여 그 값을 사용자에게 알려줍니다. </p>
				</div>
				<div>
					<h6 id="4_1_3menu">Reference Class </h6>
					<p>Task 내의 Entities 화면에는 Create Class 와 같이 Reference Class 버튼이 보입니다. Reference Class 는 다른 Task 에서 정의한 Class 를 참조하여 같이 사용하고자 할 때, 그 Class 를 참조하면 됩니다. 참조한 Class 는 Local Class 밑에 Referenced 로 표기됩니다. </p>
					<p class="center"><img src="../manualfiles/image050.png" /></p>
					<p>Referenced Class 에서 Ref. cancel 버튼을 통하여 Reference 를 취소할 수 있습니다. 참조한 Class라도 원본 Class와 동일한 것입니다. 따라서, 기존 Class와 동일한 메뉴 Create Slot, Instance, Edit, Delete 는 원본 Class(Referenced Class 와 동일)에 적용됩니다. 만약 Delete 버튼을 클릭하여 삭제하면 다른 Task 에 정의된 Local Class 원본이나 Global Class 원본도 삭제됩니다. </p>
				</div>
				<h5 id="4_2menu">Slot </h5>
				<div>
					<h6 id="4_2_1menu">Slot 생성 (Create Slot)</h6>										
					<p>Slot 생성은 Class를 생성한 후에 Class의 구성요소나 속성 등을 정의하기 위해 작성합니다. </p>
					<p class="center"><img src="../manualfiles/image051.png" /></p>
					<p>생성된 Class에 우측 편에 메뉴에서 Create Slot 을 선택하여 생성합니다. 다음과 같은 편집 창에 Slot Name과 Type, Default Value, Description 을 작성합니다. </p>
					<p class="center"><img src="../manualfiles/image052.png" /></p>					
				</div>
				<div>
					<h7 id="4_2_1_1menu">Slot Name</h7>
					<p></p>										
					<p>Slot Name은 기존 Domain, Task, Class 이름과 동일하게 영문자, 숫자와 “_”로 작성할 수 있습니다. Slot 은 상위 개념 Class 와 같이 사용하여 다른 Slot 과 구분할 수 있기에 Class 이름과 함께 표기합니다. 상기 예에서 Class “Pizza”의 Slot “type”은 “Pizza.type”으로 표기합니다. </p>
					<p>Slot description 은 다른 설명과 같이 사용자 참초 편의 용도입니다. </p>					
				</div>
				<div>
					<h7 id="4_2_1_2menu">Default Value </h7>
					<p></p>										
					<p>Default Value 는 Slot 이 가지는 초기값을 설정할 수 있습니다. 예를 들어, 날씨 도메인에서 Weather Class 의 date Slot 에 “오늘”을, city Slot 에 “서울”을 설정하여 사용자가 날짜와 도시를 발화하지 않은 경우에도 서울 오늘 날씨를 알려주게 할 수 있습니다. </p>									
				</div>
				<div>
					<h6 id="4_2_2menu">Slot Type </h6>										
					<p>Slot Type 은 Slot 이 가지는 개념적 속성을 정의합니다. 대화시스템에서 Slot 은 사용자와 시스템이 주고 받는 단어 중에서 도메인 처리를 위해서 기억해야 할 것을 개념적으로 지정한 것입니다. 따라서, 우리가 정한 Slot 은 사용자 발화에서 그 부분을 찾아 내야 합니다. 예를 들어, “오늘 날씨는”에서 “오늘”이 우리가 정한 Slot Weather.date 임을 시스템이 찾아야 합니다. Slot Type 은 이렇게 사용자 발화에서 특정 단어들을 찾아서 Slot 을 찾는데 도움이 되도록 설정해야 합니다.</p>
					<p>초기에는 다음과 같이 8 개의 Slot Type이 보입니다. </p>
					<p class="center"><img src="../manualfiles/image053.png" /></p>					
				</div>
				<div>
					<h7 id="4_2_2_1menu">시스템 제공 Slot Type </h7>
					<p></p>					
					<p>Sys 아래의 4개 Type은 시스템에서 제공하는 것입니다. </p>
					<p>Slot description 은 다른 설명과 같이 사용자 참초 편의 용도입니다. </p>
					<ol>
						<li>person : 한국인 인명을 위한 Type입니다.  시스템이 가지고 있는 한국인 인명 찾는 방법으로 사용자 발화에서 찾아낼 수 있습니다. </li>
						<li>place : 지명(주로 한국)을 위한 Type 입니다. 시스템이 특정 장소명, 행정시도와 구군읍면을 사용자 발화에서 찾아낼 수 있습니다. </li>
						<li>
							date : 날짜를 위한 Type 입니다. 시스템이 날짜 관련 단어나 구를 찾아 낼 수 있습니다. 예를 들어, 오늘, 내일, 모레, 이번주 토요일, 다음주 토요일, 다음주, 8월 24일 등을 찾습니다.
							<ol>
								<li>찾아진 날짜를 년월일 표기법인 “년도-월-일” 형태로 변경하여 저장합니다. 만약 2018 년 12 월 3 일에 “오늘"이 date Type 의 Slot 으로 인식된다면 “2018-12-03”으로 저장됩니다. “다음주”처럼 특정한 날짜가 아닌 기간인 경우에는 “2018-12-09/2018-12-15”로 저장됩니다. 즉, “시작날짜/끝날짜” 형태로 저장됩니다. </li>
								<li>상대적 날짜를 언급하는 “그 다음날”이나 “그 다음주 토요일” 등은 대화이력에서 가장 최근 날짜를 기준으로 자동으로 계산되어 “년도-월-일”이 저장됩니다. 기간인 경우는 “시작날짜/끝날짜” 형태로 저장됩니다.</li>
								<li>date Type 의 Slot 을 다시 “오늘”, “내일”, … 의 표기법이나 기타 방법으로 시스템 응답으로 사용할 필요가 있을 것입니다. 이것은 <a href="#" onclick="fnMove('#6_3_4menu');">날짜/시간 Script 함수</a> 파트를 참조하기 바랍니다. </li>
							</ol> 						
						</li>
						<li>
							time : 시간을 위한 Type 입니다. 시스템이 시간 관련 어휘를 사용자 발화에서 찾아냅니다. 예를 들어, 오후 4시, 3시 30분, 2시간 뒤, 2시간 후, 오후 등을 찾습니다.
							<ol>
								<li>찾아진 시간은 “시:분:초” 표기법으로 저장됩니다. 예를 들어, “오후 3시”는 “15:00:00”으로, “오후 2 시 20 분”은 “14:20:00”으로 표기됩니다. 기간으로 나타내야 하는 “오후” 같은 경우는 “13:00:00/20:00:00”으로 “시작시간/끝시간” 형태로 저장됩니다. </li>
								<li>상대적 시간을 언급하는 경우, “2 시간 뒤”, “10 분 후” 등은 대화이력에서 최근 시간을 기준으로 자동으로 계산되어 “시:분:초” 표기로 저장됩니다. 기간인 경우는 “시작시간/끝시간” 형태로 저장됩니다. </li>
								<li>time Type 의 Slot 도 “오후 3 시”, “오전 10 시”, … 의 표기법이나 기타 방법으로 시스템 응답으로 사용할 필요가 있을 것입니다. 이것은 <a href="#" onclick="fnMove('#6_3_4menu');">날짜/시간 Script 함수</a> 파트를 참조하기 바랍니다.</li>
							</ol>
						</li>						
					</ol>
					<p>Slot 을 상기의 Type 으로 지정했다고 하여, 사용자 발화에서 무조건 찾는 것은 아닙니다. Intents 에서 다시 설명하겠지만, Intent 마다 가능한 사용자 발화를 작성하고 그 발화에 나타나는 Slot 을 지정해 줘야 합니다. 그 지정한 Slot 이 상기 Type 이면 학습 시에 우리가 Intent 작성에서 적은 몇 개의 예제들, 예를 들어, person Type 의 Slot Book.author 로 “한명기”, “김고은”, “김은희”만이 나타나는 문장들만 있어도 실제 대화에서 나타나는 다른 작가명들을 보다 쉽게 인식할 수 있습니다. </p>
					<p>하지만, 시스템이 위에서 정의한 Type 을 모두 제대로 인식하지 못할 수 있습니다. 또한, 특정한 도메인에서는 일반적이지 않은 person, place, date, time 을 사용할 수도 있습니다. 예를 들어, 가수 Slot 을 person으로 할 수 있지만, “소녀시대”, “방탄소년단” 등을 person 으로 인식할 수 없습니다. 따라서, 특정 슬롯에 대해서, 추가적인 값들을 추가해서 시스템이 알 수 있도록 하는 것이 좋습니다. <a href="#" onclick="fnMove('#4_2_3menu');">슬롯 단어 정의</a>에서 추가적으로 슬롯 단어들을 정의하는 방법을 보겠습니다.</p>
					<p>시스템 제공 Slot Type 은 시스템이 자체적으로 가진 방법과 사용자들이 추가적으로 슬롯 단어 정의로 넣은 단어 리스트를 이용하여 사용자 발화에서 Slot 을 인식합니다. </p>
				</div>
				<div>
					<h7 id="4_2_2_2menu">None Slot Type 과 New Defined Type </h7>
					<p></p>					
					<p>None 과 New defined type 에는 문자열 형태를 의미하는 Sys.string 과 숫자 형태를 의미하는 Sys.number 가 있습니다. Slot 이 문자열 형태 정보를 가지면 Sys.string 을, 숫자 형태 정보이면 Sys.number 로 정의하면 됩니다. Number Type 으로 선언한 Slot 들은 산술연산자(Arithmetic Operator)를 이용하여 계산이 가능합니다.</p>
					<p>None 과 New defined type 의 차이점은 New defined type 은 사용자가 정의하는 새로운 개체(Named Entity)이고 None은 개체 개념없이 사용한다는 의미입니다. </p>
					<p>개체 개념을 가진다는 것은 New defined type으로 선언한 Slot을 place, person, date, time과 같은 역할을 하게 하는 것입니다. 다음 2가지가 개체 개념의 특징입니다. </p>
					<ol>
						<li>학습 문장 작성 용이 
							<p>개체 개념을 가진 Slot 들은 Slot 태깅 학습 시에 각각의 단어와 어휘를 해당 개체 코드로 대체하여 학습합니다. 개체 코드로 학습하는 것은 Slot 인식 시에 같은 개체 코드를 가진 단어와 어휘들을 동일한 것으로 보고 인식하겠다는 것입니다. 따라서, 같은 개체 코드를 가진 모든 단어들이 학습 문장에 나타날 필요 없이, 몇 개만 있어도 같은 개체 코드를 표현한 것과 같은 효과가 있습니다. 같은 개체 코드를 가지기 위해서는 Slot 에서 슬롯 단어 정의에서 단어들을 추가하면 됩니다.</p>
							<p>예를 들어, 기업의 조직명을 Department.name 이름을 가진 Slot 으로 정의하고 Slot Type을 New defined type의 Sys.string으로 설정합니다. 그리고, 슬롯 단어 정의를 통하여 아래처럼 조직명을 모두 저장합니다. 그러면, 학습 문장에 아래의 모든 조직명이 모두 나타날 필요 없이, 몇 개의 조직명만 사용자 문장들에 나타나도록 작성하여도 기술하지 않은 조직명들도 조직명이 있는 학습 문장에서 나타난 것과 같은 효과가 있습니다. 따라서, Department.name Slot 의 슬롯 단어 정의에 기입한 조직명들에 대해서, 학습문장에 그 단어가 있었든지 없었든지 상관없이, 학습문장과 유사한 유형의 문장들에서 출현하면 Department.name Slot 으로 인식할 수 있습니다. </p>
							<p class="center"><img src="../manualfiles/image054.png" /></p>
						</li>
						<li>다른 Slot 의 새로운 Type 으로 활용 
							<p>상기처럼 New defined type 으로 정의된 Slot 은 다른 슬롯 정의에 사용할 수 있습니다. </p>
							<p class="center"><img src="../manualfiles/image055.png" /></p>
							<p>상기 화면처럼 새로운 Slot GlobalBiz.department 의 Slot Type 으로 사용자가 정의한 Department.name 을 선언할 수 있습니다. 이 의미는 Department.name 을 개체로 사용하여 GlobalBiz.department Slot 에서 활용하겠다는 것입니다. GlobalBiz.department Slot 으로 인식할 단어들을 모두 학습 문장에 작성할 필요없이 일부만 작성하여도, Department.name에서 정의한 단어들을 GlobalBiz.department Slot 으로 인식할 수 있다는 것입니다. 물론 Slot 단어들이 나타나는 문장들은 학습 문장들과 유사하여야 합니다. </p>
						</li>
					</ol>
					<p>New defined type 으로 선언한 Slot 이나 그것으로 선언한 다른 Slot 들에 대한 Slot 값(단어)는 슬롯 단어 정의에서 기술한 단어들만으로 한정됩니다. 따라서, 그기에 없는 단어에 대해서는 사용자 발화 상에서 Slot 값으로 인식하지 못합니다. </p>
					<p>None 의 Sys.string 과 Sys.number 로 선언하게 되면, 다른 Slot 의 Type 으로 사용하지 못합니다. 또한, Slot 에서 정의한 슬롯 단어 리스트는 사용자 발화에서 Slot 값 인식하는데 사용하지 않습니다. 따라서, None Type으로 선언한 Slot 은 학습 문장들에서 그 Slot 값으로 인식된 어휘들 중심으로 인식됩니다. 하지만, Slot 단어 리스트을 활용하지 않고 단지 주위 문맥을 보기에 학습 문장과 같거나 유사한 발화에 나타나는 임의 단어(학습문장에서 Slot 으로 인식(태깅) 표시되지 않은 단어)라도 인식할 가능성이 있는 장점이 있습니다. Slot 값들이 한정된 특정 문형 패턴에서만 나타나고 Slot 값(단어나 구)이 너무 종류와 수가 많아서 리스트로 작성하지 못하는 Slot 은 None으로 선택하는 것이 Slot 태깅(인식)에 유리합니다. </p>
					<p>아래는 None과 New defined type 을 다시 요약해서 차이를 정의해 보았습니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
							<col width="*" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="center">
								<td></td>								
								<td>None</td>
								<td>New defined type</td>
							</tr>
							<tr>
								<th>다른 Slot 의 Type으로 사용 여부 </th>
								<th>사용 못함</th>
								<th>사용</th>
							</tr>
							<tr class="center">
								<td>Slot 단어 리스트에 의한 Slot 인식 여부</td>								
								<td>다른 어휘 인식 가능 <br>(학습 문장 유형일 경우)</td>
								<td>Slot 단어 리스트의 단어만 한정하여 인식 <br>(학습 문장 유형일 경우 인식, 하지만 단어 리스트에 없으면 인식 못함) </td>
							</tr>
							<tr>
								<th>학습문장들에 다양한 Slot 단어 유형 작성 필요 여부 </th>								
								<th>필요</th>
								<th>필요 없음</th>
							</tr>
							<tr class="center">
								<td>다양한 학습문장 유형 작성 필요</td>
								<td>필요</td>
								<td>필요 없음</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<h7 id="4_2_2_3menu">Class 를 Slot Type 으로 정의 </h7>
					<p></p>
					<p>Slot Type 으로 이미 정의한 Class 를 Type 으로 설정할 수 있습니다. Class 가 다른 Class 의 속성이나 구성 요소가 되는 것입니다. 예로 네비게이션 도메인에서 먼저 관심 장소에 대한 Class POI 를 정의하고 경로 Route Class 를 정의할 때, 목적지 dest Slot 과 목적지 근처를 검색할 때 사용하는 랜드마크 Slot lm 을 모두 POI Class로 Type을 정의하였습니다.</p>
					<p class="center"><img src="../manualfiles/image056.png" /></p>
					<p>상기 예에서처럼 POI Class 를 각각 다른 Slot 의 Type 으로 정의함으로써, POI Class 에 새로운 Slot 을 추가하든지 Slot 을 삭제함으로 POI Class 로 선언된 모든 Slot 의 속성을 동일하게 관리할 수 있습니다.</p>
					<p>POI Class 처럼 Slot 의 Type 으로 사용되는 Class 를 기본 Class 라고 정의합니다. 일반적으로 기본 Class 는 도메인에서 실제로 활용하는 최종적인 Instance 로 사용하지 않을 수 있습니다. 즉, 상기 네비게이션 도메인에서 사용자 발화에 “여의도역 근처에 있는 맥도날드로 가자”라고 하면 “여의도역”은 Route.lm.name Slot 이고 “맥도날드”는 Route.dest.name Slot 이 되기에 실제로 POI.name으로 인식되지 않는다는 것입니다. </p>
					<p>기본 Class 의 Slot 으로 사용자 발화에 나타나는 단어를 태깅할 경우가 있습니다. 네비게이션 도메인에서 Route Class에 경유지 via_dest Slot 을 POI Class 로 추가한다고 가정합시다. 사용자가 “맥도날드”처럼 관심지역만 발화한 경우에 이것이 경유지인지 목적지인지 판단하기 어렵습니다.</p>
					<p>그렇기에 학습 문장으로 상기 “맥도날드”를 어떤 Slot 으로 태깅해야 할지도 문제입니다. 해결 방법은 2 가지가 있을 수 있습니다. 첫번째 방법은 경유지를 발화하는 Task 와 목적지를 발화하는 Task 를 구분하고 각 Task 에서 상기 학습 문장을 Task 에 맞도록 태깅하는 방법입니다. GenieDialog 대화이해모듈은 Task 정보를 이용하기에 구분하여 처리할 수 있습니다. 두번째 방법은 “맥도날드”를 POI.name Slot 으로 태깅하는 것입니다. 이 기본 Class 의 Slot 으로 태깅하는 경우는 이처럼 문장 내에서 그 실제 활용 Slot 을 구분하기 어려울 때 사용합니다. 이렇게 태깅한 학습 문장들은 추후에 실제 대화에서 POI.name Slot 으로 인식되기에 목적지인지 경유지인지 구분이 되지 않습니다. 이에 대해서는 개발자가 추가 작업이 필요합니다. 하지만, 대화에서 먼저 시스템이 목적지 이름이나 경유지 이름을 요구한 다음에 POI.name Slot 과 같이 기본 Class 의 Slot 이 태깅되었다면 대화시스템에서 POI.name 으로 인식된 단어를 요청한 Slot 으로 인식된 것으로 자동으로 변경합니다. 시스템 발화가 목적지 이름과 경유지 이름을 요구한 것인지 구분하기 위해서는 <a href="#" onclick="fnMove('#5_3_2menu');">시스템 응답 정의 (응답 대화)</a>에서 언급하는 시스템 Intent 에 DA Type 을 request 계열로 하고 요청 Slot 을 명시적으로 표기해야 합니다. </p>
				</div>
				<div>
					<h6 id="4_2_3menu">슬롯 단어 정의 </h6>										
					<p>슬롯 단어 정의는 각 Slot 으로 사용되는 단어(복합 단어 포함)를 정의합니다. 이 단어 정의로 2가지 역할을 수행합니다. 동의어 지정 및 Slot 단어 리스트(Slot Type이 New defined type 또는 Sys.place, Sys.person, Sys.date, Sys.time)로 활용합니다. </p>
					<p>마우스를 Slot 이름과 Type 근처를 클릭하면 다음과 같은 슬롯 단어 정의 창이 보입니다. </p>
					<p class="center"><img src="../manualfiles/image057.png" /></p>
					<p class="center"><img src="../manualfiles/image058.png" /></p>
					<p>위처럼 Pizza.type에 나타날 피자 타입을 정의하면 됩니다.</p>
					<p>동의어 정보는 각 행 정보입니다. 각 행에서 첫 열 단어가 대표어이고 나머지는 이형태입니다. 예를 들어, “리얼바비큐”와 “바비큐”에서 이형태는 “바비큐”이고 대표어는 “리얼바비큐”입니다. 동의어 정보 역할은 사용자가 이형태를 발화하여도 대화시스템에서 내부적으로 대표어로 변경해서 통일하여 사용합니다. 이렇게 통일하는 이유는 Class 의 Instance 정보, 즉 도메인 KB 정보 Table 에 저장된 형태를 대표어로만 주로 기록하기 때문입니다. 또한, 대화상태에 대한 조건 검사에서 Slot 값이 어떤 값인지를 검사할 수도 있는데 이렇게 이형태들을 하나의 대표어로 통일하지 않으면 조건에 모든 이형태들에 대한 검사들을 작성해야 하고, 이형태가 발견될 때마다 조건 내용을 계속 추가해야 하는 불편함을 방지합니다.</p>
					<p>동의어 정보는 Slot 이 어떤 Type이던지 상관없이 관리되며 작동됩니다. </p>
					<p>만약 Slot 의 Type 이 New defined type 또는 Sys.place, Sys.person, Sys.date, Sys.time 이라면 이 단어 리스트가 Slot 단어 리스트가 되어 추후 Slot 인식에서 인식할 단어를 찾는데 활용됩니다. 단어 리스트는 대표어와 이형태를 모두 포함하여 구분없이 모두 사용합니다.</p>
					<p>Slot Type 이 None인 경우에는 단어 리스트로 역할이 없습니다. 이런 경우에 위의 예에서 이형태 없이 대표형만 나타낼 필요는 없습니다. 하지만, 추후 Intent 에서 사용자 발화를 작성하고 발화에 특정 단어가 어떤 Slot 인가를 태깅할 때, Slot 단어 정의에 있는 단어들은 보다 편리하게 태깅할 수 있습니다. </p>
					<p>편의 기능으로 각 행 우측을 보면 삭제 기능이 있고, 각 이형태마다 삭제도 할 수 있습니다. 또한 Instance 에서 Download 와 Upload 기능을 제공한 것처럼 엑셀 파일 형태로 Download 하고 Upload 하는 기능을 제공합니다. 엑셀 파일 형식은 슬롯 단어 정의 창에 보이는 것과 동일한 형태입니다. </p>
					<p class="center"><img src="../manualfiles/image059.png" /></p>
				</div>
				<div>
					<h6 id="4_2_4menu">Slot 관리 기능 </h6>										
					<p>생성한 Slot 우편에 마우스를 올리면 편집 버튼 <img class="img20" src="../manualfiles/image060.png" />, 삭제 버튼 <img class="img20" src="../manualfiles/image061.png" /> 과 Class 내 Slot 배열 순서를 정하는 <img class="img20" src="../manualfiles/image062.png" />버튼 이 보입니다. </p>					
					<p class="center"><img src="../manualfiles/image062_1.png" /></p>
					<p>Slot 편집 버튼 <img class="img20" src="../manualfiles/image060.png" />을 클릭하면 다음과 같이 Slot 에 대한 정보 창이 보입니다. </p>
					<p class="center"><img src="../manualfiles/image063.png" /></p>
				</div>
				<div>
					<h7 id="4_2_4_2menu">Default Value </h7>
					<p></p>
					<p>Slot 에 초기로 가지는 값을 지정합니다. 사용자 발화로 다른 Slot 값이 들어오기 전까지 그 값이 유지됩니다. 날씨 검색 도메인에서 date를 “오늘”로 city를 현재 위치로 지정하여 도시나 날짜가 없어도 사용자에게 정보를 제공하는데 사용됩니다. </p>					
				</div>
				<div>
					<h7 id="4_2_4_3menu">Preceding Slots </h7>
					<p></p>
					<p>해당 Slot 을 지배적 관계에 있는 Slot 이 있어서, 그 Slot 의 값이 변화하면 해당 Slot 의 값이 초기화되도록 하는 지배적 관계의 Slot 들을 적습니다. </p>
					<p>예를 들어, Address Class 에 city(시), gu(구), dong(동) Slot 이 있다고 가정합시다. 사용자의 발화가 “서울 서초구 대치동 커피숍 찾아줘”이라면 city=”서울”, gu=”서초구”, dong=”대치동”으로 Entity 이력에 저장됩니다. 다시 사용자가 “방배동에서 찾아줘”라고 하면 city 와 gu 의 Slot 값은 그대로 유지되어야 합니다. 하지만, 사용자가 “부산에서 찾아줘”라고 한다면 gu 와 dong 은 유지되어 “부산 서초구 방배동”을 검색해서는 안되기에 초기화해야 합니다. 예에서, dong 의 Preceding Slots 으로 gu 와 city 를 정의하고, gu 의 Preceding Slots 으로 city 를 정의하면, city 만 값이 바뀔 경우 gu와 dong의 기존 값이 초기화되고 gu만 변경되면 dong의 기존 값이 초기화됩니다. </p>					
				</div>
				<div>
					<h7 id="4_2_4_4menu">Slot value relationship 정의 </h7>
					<p></p>
					<p>Slot value relationship 옆에 “+add”를 클릭하여 사용자 발화에 의해 Slot 값이 입력되면, 그 값 입력이 다른 Slot 에 영향을 미치는 내용을 정의할 수 있습니다. 즉, 사용자 입력에 대한 시스템 Action을 정의합니다.</p>
					<p class="center"><img src="../manualfiles/image064.png" /></p>
					<p>상기처럼 define 으로 구분한 Condition 과 Action 을 기록할 수 있습니다. Condition 은 대화이력 상황에 대한 조건이며, Action 은 특정한 Slot 에 값을 저장하거나 Reset 하는 작업을 수행합니다. Condition과 Action 에 대한 자세한 설명은 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>과 <a href="#" onclick="fnMove('#6_2menu');">행동</a> 파트를 참조하세요. </p>
					<p>위에서 정의한 Action 및 기타 다른 Action 에 의해서 Slot 값이 변경되는 경우에는 Slot value relationship 이 동작하지 않습니다. 사용자 발화에서 인식한 Slot 에 의해서 새로운 Slot 값이 입력될 때에만 Slot value relationship 의 define 들에서 Condition 을 검토하고 충족하는 Condition의 Action 을 수행합니다. </p>
					<p>Slot value relationship 의 예를 들면, 자동차에서 경로탐색과 날씨를 제공하는 도메인을 가정해 봅시다. 사용자가 특정 목적지의 도시를 발화하면 그 값을 바로 날씨 Class 의 도시 Slot 에 저장하도록 할 수 있습니다. 경로 Class의 목적지 도시 Slot인 Route.dest.addr.city에 Condition과 Action을 정의하면 됩니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="20%" />							
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td class="title_td">Condition</td>								
								<td>true</td>								
							</tr>
							<tr>
								<td class="title_td">Action</td>								
								<td>Set(“Weather.city”, Value(“Route.dest.addr.city”, “system_response”);</td>								
							</tr>
						</tbody>
					</table>
					<p>사용자가 “서울 서초구 이마트로 가자”라는 발화를 하면, 대화시스템이 “서울”을 "Route.dest.addr.city"로 인식하는 경우, Condition “true”에 의하여 무조건 시스템이 알고 있는 "Route.dest.addr.city"의 값을 Weather.city Slot 에 저장하게 합니다.</p>
					<p>사용자가 목적지를 발화할 경우, 날씨 Class 의 city 에 목적지 도시 정보를 저장하여 사용자가 날씨를 문의하면 바로 목적지 날씨를 안내할 수 있습니다. </p>
					<p>만약, 위의 경우에 사용자가 목적지 도시를 발화하지 않고 목적지만 발화할 경우도 있을 것입니다. 한데, 공교롭게도 그 목적지가 있는 도시가 한 곳이어서 굳이 사용자가 목적지 조시를 발화하지 않아서 상기 Route.dest.addr.city 에 정의한 Slot value relationship 의 define이 적용되지 않을 수 있습니다. 이런 경우를 대비한다면, 다음처럼 목적지 Slot Route.dest.name 에 다음을 기술할 수 있습니다. 그렇다면, 상기의 목적지 도시에 대한 기술은 필요 없을 수 있습니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="20%" />							
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td class="title_td">Condition</td>								
								<td>Entity("Route.dest.addr.city", "system_response", "true") and  Count("Route.dest.addr.city", "system_response") == 1</td>								
							</tr>
							<tr>
								<td class="title_td">Action</td>								
								<td>Set(“Weather.city”, Value(“Route.dest.addr.city”, “system_response”); </td>								
							</tr>
						</tbody>
					</table>
					<p>위의 Slot value relationship 의 define 은 Route.dest.name 을 사용자가 발화할 때마다 조건을 검사하고 만족하면 Action 을 수행합니다. Condition 은 목적지 도시 Route.dest.addr.city 의 값을 시스템이 알고 있는지를 검사하는 Entity("Route.dest.addr.city", "system_response", "true")와 그 도시의 값이 한 개인지를 검사하는 Count("Route.dest.addr.city", "system_response") == 1 를 모두 만족하는지에 대한 것입니다. Action 은 시스템이 응답(“system_response”)할 수 있는 목적지 도시 Route.dest.addr.city 의 값을 Weather.city Slot 에 저장합니다. </p>
				</div>
				<h5 id="4_3menu">추가적인 Entity 정보 </h5>
				<div>
					<h6 id="4_3_1menu">Class 내 Slot 간의 관계</h6>										
					<p>Class에서 선언한 Instance DB를 검색할 경우에 지금까지(한 발화이든지 여러 발화이든지) 언급된 Class 내 Instance 연동으로 체크된 Slot 값들로 AND 조건으로 검색합니다. 각 Slot 의 값은 Instance DB 에 있어도 AND 조건의 모든 Slot 들의 값들이 한 Record 로 Instance DB 에 없으면 검색 값이 없습니다. </p>
					<p>OR 논리 연산 관계 정의는 사용자가 한 발화에서 한 Slot 값을 여러 개 얘기하는 경우에 자동으로 OR 로 잡아 줍니다. 예를 들어, “내일 서울과 대구 날씨 알려줘”에 대해서 Weather.date=”내일”, Weather.city={“서울”, “대구”}로 Slot 태깅됩니다. 이 경우에 Instance DB 검색 조건은 Weather.date=”내일” AND (Weather.city=”서울” OR Weather.city=”대구”)가 되어 검색합니다.</p>					
					<p>OR 논리 연산 관계 정의는 한 Slot 에 국한되어 처리됩니다. 따라서, “내일 서울 날씨와 오늘 대구 날씨를 알려줘”라는 발화에 대해서는 Weather.date={“내일”, “오늘”}, Weather.city={“서울”, “대구”}로 Slot 태깅됩니다. 따라서 Instance DB 검색 조건은 (Weather.date=”내일” OR Weather.date=”오늘”) AND (Weather.city=”서울” OR Weather.city=”대구”)가 되어 사용자 발화 의도와 다른 검색을 하게 되기에 주의해서 사용하기 바랍니다. 이와 같이 한 발화나 여러 발화에 대해 AND 와 OR 논리 연산을 하고자 할 때는 Class 나 Slot 을 다른 이름으로 설정하여 <a href="#" onclick="fnMove('#1_6menu');">도메인 지식제공</a>에서 처리해 주기 바랍니다. 예를 들어 상기 예는 Class Weather1 과 Class Weather2, … 등을 두거나 한 Class 에 city1, city2, …와 date1, date2, … 등을 두어서, 대화이해에서 Slot 태깅을 하고 이를 해석해서 다르게 처리하실 필요가 있습니다. </p>					
				</div>
				<div>
					<h6 id="4_3_2menu">비교를 포함한 Slot 정의</h6>										
					<p>지금까지 우리는 Slot 과 값 간의 관계는 “=”으로만 정의했습니다. 즉, 사용자 발화에서 “대구 날씨 알려줘”와 같이 특정 Slot Weather.city 에 대한 값으로 “대구”가 있음에 대한 정보만 정의하였습니다. 하지만, 사용자 발화에서 “1000 원 이하로 찾아주세요.”와 같이 특정 값과의 차이를 언급할 수 있습니다. 이 경우에 기존 “=” 관계 연산자로만 표현하고 물건 Item Class 에 가격 Slot Item.price 가 정의되어 있다면, Item.price=”1000 원 이하”로 할당하고 추후에 “1000 원 이하”를 따로 처리해 줘야 합니다. 그렇지 않은 경우에는 Item.price_le=”1000”처럼 새로운 Slot 을 두어서 Item.pirce 가 1000 이하인 것들을 검색하도록 처리해 주어야 합니다. 여기에서 처리해 주어야 한다고 함은 <a href="#" onclick="fnMove('#1_6menu');">도메인 지식제공</a>에서 설명했듯이 도메인 지식에 대한 처리를 따로 구현해 줘야 합니다. </p>
					<p>하지만, GenieDialog 에서는 5가지 관계연산(Relational Operators)이 가능하도록 Slot 이름에 특수 문자열 접미사를 붙여서 새로운 Slot 을 정의하면 원 Slot 에 대한 연산자로 바뀌도록 엔진에서 내부적으로 처리합니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="27%" />
							<col width="18%" />
							<col width="27%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>관계연산자 </th>
								<th>접미사(Suffix) </th>
								<th>예제(Item.price) </th>
								<th>Instance DB 검색 내부처리 </th>
							</tr>
							<tr>
								<td>!= (not equal) </td>
								<td>_not </td>
								<td>Item.price_not=”1000”</td>
								<td>Item.price != 1000 </td>
							</tr>
							<tr>
								<td>&lt; (less than) </td>
								<td>_lt </td>
								<td>Item.pirce_lt=”1000” </td>
								<td>Item.price &lt; 1000 </td>
							</tr>
							<tr>
								<td>&gt; (greater than) </td>
								<td>_g</td>
								<td>Item.price_gt=”1000”</td>
								<td>Item.price &gt; 1000 </td>
							</tr>
							<tr>
								<td>&lt;= (less or equal)</td>
								<td>_le </td>
								<td>Item.price_le=”1000” </td>
								<td>Item.price &lt;= 1000 </td>
							</tr>
							<tr>
								<td>&gt;= (greater or equal) </td>
								<td>_ge</td>
								<td>Item.price_ge=”1000” </td>
								<td>Item.price &gt;= 1000 </td>
							</tr>
						</tbody>
					</table>									
					<p>상기 관계연산 접미사 사용 시에 주의할 점은 Item.price, Item.price_le, Item.price_ge 등을 2 개 이상 정의하였을 경우에 이 중에 한 개 Slot 의 값만 변경되어도 나머지 Slot 값들의 값은 초기화됩니다. 예를 들어, Item.price=”20000”이 있었더라도 Item.price_le=”1000”이 들어오게 되면 이 시점에서 Entity 대화이력에 Item.price의 값은 초기값으로 변경됩니다. 그 이유는 Item.price에 대한 6 가지 연산자를 다른 이름으로 표기한 것이므로 대화이력 관리는 Item.price Slot 하나만 관리하듯이 합니다. </p>
					<p>이러한 이유로 “1000 원보다 비싸고 2000 원보다 싼 것 찾아줘”처럼 AND 개념에 대해서는 각각 다른 Slot 을 정의해서 처리해 줘야 합니다. </p>					
				</div>
				<div>
					<h6 id="4_3_3menu">특수 Slot 값 min/max</h6>										
					<p>사용자가 특정 Slot 값을 발화할 때, 보편적으로 많이 할 수 있는 발화가 “가장 큰”, “가장 작은”, “가장 가까운”, “가장 싼” 등처럼 특정 값이 아니라 최소값이나 최대값을 의미하는 경우가 많습니다. <a href="#" onclick="fnMove('#1_6menu');">도메인 지식제공</a>에서 외부 처리를 해서 그 의미에 적합한 값을 선택하여 처리할 수 있습니다. GenieDialog 에서는 특정 Slot 값이 “min”이면 내부에 정의된 Class Instance DB 에서 최소값을, “max”이면 최대값을 의미하여 처리합니다.</p>
					<p>Slot 값이 “min”이나 “max”로 하는 방법은 2가지가 있을 수 있습니다. </p>
					<p>첫번째 방법은 <a href="#" onclick="fnMove('#4_2_3menu');">슬롯 단어 정의</a>에서 대표어를 min 이나 max 로 하는 것입니다. 예를 들어, 피자 주문 도메인에서 Pizza.price 의 슬롯 단어 정의를 다음과 같이 하고 대표어가 아닌 단어들을 Intents 에서 Pizza.price 로 태깅해 주면 됩니다. “가장 값이 싼 피자는”을 request(Pizza.type, Pizza.price=”가장 값이 싼”)이라는 의도가 되도록 “가장 값이 싼” 부분을 Slot Pizza.price로 태깅해 주면 됩니다. </p>
					<p class="center"><img src="../manualfiles/image065.png" /></p>
					<p>두번째 방법은 사용자 발화에는 Slot 태깅을 하지 않고 의도에 바로 “min”이나 “max”를 설정해 주는 방법입니다. <a href="#" onclick="fnMove('#5_2menu');">Intent 정의</a>에서 보다 자세히 설명하겠지만, Slot 태깅하지 않은 모든 부분을 대화이해에서 하나의 Intent 로 보고 인식합니다. 위의 문장 “가장 값이 싼 피자는”을 request(Pizza.type, Pizza.price=”min”)으로 의도를 정의하고 사용자 발화 문장의 어떠한 단어도 Pizza.price 로 태깅하지 않습니다. 전체 request(Pizza.type, Pizza.price=”min”)을 하나의 의도 심볼로 보고 처리합니다. 이렇게 태깅하는 방법은 <a href="#" onclick="fnMove('#5_3_1_6menu');">의도 Slot 설정 및 태깅</a>에서 자세히 설명합니다. </p>
					<p>첫번째 방법은 특정한 단어가 발화되면 min이나 max로 정의되도록 조정할 수 있는 장점이 있는 대신에 그 의미를 나타내는 모든 단어들을 정의해야 합니다. 두번째 방법은 특정한 단어가 항상 min 이나 max 가 되도록 조정할 수 없지만 여러 유사한 문장들을 request(Pizza.type, Pizza.price=”min”) 의도로 정의해 두고 학습하였다면 위에 없는 “값이 가장 나가는 피자는” 경우처럼 앞뒤 단어가 바뀌거나 새로운 단어가 추가되어도 잘 인식될 수 있는 장점을 가집니다.</p>					
				</div>
				<div>
					<h6 id="4_3_4menu">Entity 대화상태 변수로 사용하기 </h6>										
					<p>Entity 를 사용자와 주고 받는 개념 이외에 도메인 개발자, 즉 응용 개발자가 전체 도메인 대화모델을 작성하기에 편리하기 위해 변수 개념으로도 활용할 수 있습니다. 예를 들어, 도메인의 대화에서 어떤 상황을 기록하기 위해, 특정 Entity 를 두고 그기에 값을 저장하고 검사해서 사용할 수 있습니다. 예를 들어, 사용자가 특정 의도를 몇 번 발화했는지에 대한 정보를 기록하고 그 정보에 따라 다르게 시스템 응답을 하기 위해서 Slot 을 정의하고 사용자 특정 의도에 대한 응답을 하면서 Action 에서 그 Slot 에 값을 “+1” 씩 더해서 저장하도록 Set 함수를 사용하면 됩니다. 다른 정보로는 개발자가 Task, Slot, Entity 정보 이외에 특정 상태를 기록하고 싶으면 그 정보를 기록하는 Slot 을 정의하고 그기에 시스템 응답 이후에 Action 으로 Slot 의 값으로 상태를 저장하면 됩니다. 이러한 Slot 들은 Instance 와 연동을 체크하지 않아야 합니다.</p>
					<p>이와 같은 상태 정보 Slot 에 대한 값을 시스템 응답에서 정의하는 Action 부분에서 Set 함수로 하지 않고, 사용자 발화에서 정보를 얻어 올 수 있습니다. 앞 절에서 사용자 발화 부분에 Slot 태깅되지 않고 포함된 Slot 은 하나의 의도 Type 으로 파악한다고 하였습니다. 따라서, 예를 들어, “오늘 날씨 어때?” 의도를 request(Weather.info, Weather.date=”오늘”, User.state=”request_weather_info”)라고 정의하면 사용자가 상기 발화를 발화할 때, User.state Slot 에 특정 값이 저장되어 그 값 정보로 사용자 발화 상태도 알 수 있습니다.</p>
					<p>앞으로 살펴보겠지만 대화 상태를 의도의 Predicate 이름으로 정의할 수 있고 여기에서 설명한 특정 Slot 에 값을 지정하여 정의할 수도 있습니다. 이러한 대화 상태를 저장하는 것은 다양한 대화 상태에 따라 시스템이 적절하게 응답하도록 설계하기 위함입니다. 복잡한 상태를 정의할수록 시스템은 다양하고 지능적으로 대답이 가능하지만 유지 보수가 어려운 단점을 가집니다. </p>										
				</div>
				
				<!-- 5장 start -->				
				<h4 id="5menu">Intents</h4>
				<div>					
					<p>Intents 에서는 도메인에서 사용자가 발화하는 의도들을 분류하여 표현하고 각 의도에 대해 시스템의 응답을 정의합니다. Intents에서 작성할 내용은 다음과 같습니다. </p>										
					<p class="center"><img src="../manualfiles/image066.png" /></p>
					<p>먼저 의도를 정의하고 그 의도로 인식되어야 할 사용자 발화들을 작성합니다. 작성한 발화들은 같은 Slot 들이 나타나는 것들끼리 분류하여 저장합니다. 그리고, 마지막으로 정의한 의도에 대한 시스템 응답 대화를 정의합니다. Intents 에서 작성하는 3 가지 구성요소는 (1) Intent 정의, (2) 사용자 발화 작성, (3) 시스템 응답 대화 정의입니다. </p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><p><b>[Intent 이름 변경 및 삭제 주의]</b><p> 
									<p>작성한 DA Type 이름 변경 및 삭제는 각종 Condition과 Action에 사용한 기존 DA Type 이름을 변경하지 못합니다. 따라서, 이름 변경이나 삭제에 대해 사용자가 주의하여 수정하셔야 합니다.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h5 id="5_1menu">Global Intents와 Local Intents </h5>
				<div>														
					<p>Intents는 상기 화면에 보듯이, 도메인 이름 아래에 Intents에서 정의하는 Global Intent와 각 Task 이름 아래의 Intents 에서 정의하는 Local Intent 로 구분할 수 있습니다. Global Intent 들은 모든 Task 에서 사용합니다. Global Intent 에 시스템 응답이 정의되어 있으면 모든 Task 에서 같은 Intent 에 대해 동일한 응답을 합니다. Local Intent 에서 정의한 시스템 응답은 해당 Task 가 현재 진행 중인 Task인 경우에만 적용됩니다. </p>
					<p>Global Intent 에 정의된 사용자 발화들은 모든 Task 상황에서 동일하게 인식될 가능성을 가집니다. 하지만, Local Intent 에 정의된 사용자 발화들은 정의된 Task 가 대화이력에서 진행 중인 Task 로 될 때, 인식될 가능성이 높아지고 아닌 경우에는 낮아지게 됩니다. 따라서, 같은 사용자 발화라도 Task별로 다른 의도에 정의하게 되면 진행 중인 Task에 따라 의도가 다르게 인식될 수 있습니다. 하지만, 원칙적으로 대화모델링을 할 때, 같은 사용자 발화는 같은 의도로 정의해 주는 것이 시스템의 의도 인식 성공률을 높일 수 있습니다. </p>					
					<p>동일한 Intent 를 Global Intents 과 Local Intents에 둘 수도 있습니다. 이 경우는 Global Intent 에는 사용자 발화만 정의하고 시스템 응답은 정의하지 않고, 각 Task 마다 Local Intent 에는 시스템 응답만을 정의해서 진행 중인 Task 가 바뀜에 따라 같은 의도에 대해 다른 시스템 응답을 하도록 합니다. 같은 사용자 발화를 Task 마다 Local Intents 에 다른 의도 이름 또는 동일 의도 이름으로 정의하기 보다, Global Intent 에만 사용자 발화를 정의하고 Task 마다 그 의도를 다르게 해석하여 시스템 응답을 다르게 정의하는 것이 사용자 발화를 인식하는 성공률을 높이는데 좋습니다. </p>
					<p>반대로, Global Intents 와 각 Task 의 Local Intents 의 동일한 Intent 에서 다른 사용자 발화들을 작성할 수도 있습니다. 이러한 경우, Local Intents 에 정의한 사용자 발화를 인식하는데 있어서 Task 정보가 유리한 지를 고려해야 합니다.</p>
					<p>지금까지 살펴보았듯이, Intent 의 3 가지 구성 요소 중에서 의도를 정의하는 것은 필수이고 나머지는 작성하지 않아도 될 경우가 있습니다. 같은 사용자 발화를 다르게 해석하도록 다른 Intent 로 정의할수록 사용자 의도 인식률은 떨어지게 됩니다. </p>					
				</div>
				<h5 id="5_2menu">Intent 정의 </h5>
				<div>														
					<p>Intent, 의도는 다음과 같은 구성 요소를 가집니다. { } 부분은 필수가 아닌 부분입니다. </p>
					<ol>
						<li>의도 = 의도 Type + { 인식 Slot } </li>
						<li>의도 Type = Dialog Act Type + { 요청 Slot } + { 의도 Slot }</li>
					</ol>					
					<p>의도는 크게 의도 Type과 인식 Slot 부분으로 나눌 수 있습니다. 인식 Slot 부분은 사용자 발화에 나타난 Slot 값을 인식한 부분입니다. 그 나머지를 의도 Type 으로 보면 됩니다. 의도 Type 은 문장 전체 내용을 보고 시스템이 분류를 하고, 인식 Slot은 문장에서 각 단어와 주위 단어 정보로 Slot 부분을 인식하는 차이로 구분한 것입니다. </p>					
					<p>의도 Type 은 Dialog Act(DA)* Type 과 나머지 Slot 과 관련된 부분입니다. DA Type 은 의도 표현방식에서 술어 파트를 의미하고 “( )” 안에 나타나는 Slot 내용을 제외한 것입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>[참고] * 의도를 대화 행동, Dialog Act 라고도 합니다. 대화시스템에 따라 의도 Type 부분만을 Dialog Act 라고 하기도 하고 의도 전체를 Dialog Act 라고 하기도 합니다. </td>
							</tr>
						</tbody>
					</table>						
					<p>의도 표현을 표현 방법만으로 정의하면 다음처럼 할 수 있습니다. </p>
					<ol>
						<li>의도 = Dialog Act Type ( { Slot 부분 } ) </li>
						<li>Slot 부분 = { 요청 Slot } + { 의도 Slot  } + { 인식 Slot } 
							<p>- Dialog Act Type: 의도의 술어 부분입니다. 영문자와 숫자, “_” 조합으로 작성합니다. </p>
							<p>- 요청 Slot: 상대방에게 해당 Slot 값을 요청하는 의도이며, 단독 Slot 으로 표기합니다. request 계열(request 로 시작하는 DA Type)에서만 꼭 있어야 합니다. </p>
							<p>- 의도 Slot: 의도 의미의 일부를 Slot 과 Slot 값으로 표현하는 형태입니다. </p>
							<p>- 인식 Slot: 사용자 발화에서 인식된 Slot 값을 표현한 것입니다.</p>
						</li>
					</ol>					
					<p>예제를 표로 보면 다음과 같습니다. 대화모델 개발자에 따라 의도는 다르게 표현 가능합니다.</p>	
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="19%" />
							<col width="25%" />
							<col width="12%" />
							<col width="12%" />
							<col width="12%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th rowspan="2">발화 (사용자/ 시스템) </th>
								<th rowspan="2">의도 </th>
								<th colspan="3">의도 Type</th>
								<th rowspan="2">인식 Slot</th>
							</tr>
							<tr>
								<th>DA Type</th>
								<th>요청 Slot</th>
								<th>의도 Slot</th>
							</tr>
							<tr>
								<td>예</td>
								<td>affirm()</td>
								<td class="center_td">affirm </td>
								<td class="center_td">-</td>
								<td class="center_td">-</td>
								<td>-</td>
							</tr>
							<tr>
								<td>날씨 어떻지</td>
								<td>request(Weather.info)</td>
								<td class="center_td">request</td>
								<td class="center_td">Weather.info</td>
								<td class="center_td">-</td>
								<td>-</td>
							</tr>
							<tr>
								<td rowspan="2">오늘 날씨 어때?</td>
								<td>request(Weather.info, Weather.date=”오늘”)</td>
								<td class="center_td">request</td>
								<td class="center_td">Weather.info</td>
								<td class="center_td">-</td>
								<td>Weather.date=”오늘”</td>
							</tr>
							<tr>								
								<td>request(Weather.info, Weather.date=”오늘”, User.state=”request_weather_info”)</td>
								<td class="center_td">request</td>
								<td class="center_td">Weather.info</td>
								<td class="center_td">User.state=”request_weather_info”</td>
								<td>-</td>
							</tr>
							<tr>
								<td>이마트 가자</td>
								<td>inform_route(Route.dest.name=”이마트”)</td>
								<td class="center_td">inform_route</td>
								<td class="center_td">-</td>
								<td class="center_td">-</td>
								<td>Route.dest.name=”이마트”</td>
							</tr>
							<tr>
								<td rowspan="2">가장 가까운 곳으로 가자</td>
								<td>inform_route(Route.dist.dist=”가장 가까운”)</td>
								<td class="center_td">inform_route</td>
								<td class="center_td">-</td>
								<td class="center_td">-</td>
								<td>Route.dist.dist=”가장 가까운”</td>
							</tr>
							<tr>								
								<td>inform_route(Route.dist.dist=”min”)</td>
								<td class="center_td">inform_route</td>
								<td class="center_td">-</td>
								<td class="center_td">Route.dist.dist=”min”</td>
								<td>-</td>
							</tr>
							<tr>
								<td>가장 가까운 이마트로 가자</td>
								<td>inform_route(Route.dist.dist=”min”, Route.dest.name=”이마트”)</td>
								<td class="center_td">inform_route</td>
								<td class="center_td">-</td>
								<td class="center_td">Route.dist.dist=”min”</td>
								<td>Route.dest.name=”이마트”</td>
							</tr>
						</tbody>
					</table>								
				</div>
				<div>
					<h6 id="5_2_1menu">기정의 DA Type</h6>										
					<p>GenieDialog에서 미리 정의된 DA Type은 다음 표와 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="12%" />
							<col width="19%" />
							<col width="22%" />													
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th></th>
								<th>DA Type </th>
								<th>설명</th>
								<th>예제 또는 동작 설명</th>
							</tr>
							<tr>
								<td class="center_td" rowspan="4">대화 진행</td>
								<td class="center_td bold_td">request</td>
								<td class="center_td">요청 Slot에 대한 값을 요구</td>
								<td>사용자나 시스템이 상대방에게 필요한 Slot 값을 요구, request(Weather.info)</td>
							</tr>
							<tr>								
								<td class="center_td">affirm</td>
								<td class="center_td">예(Yes) 표현</td>
								<td>예, 맞아요, 그래, ... 등의 표현</td>
							</tr>
							<tr>								
								<td class="center_td">negate</td>
								<td class="center_td">아니오(No) 표현</td>
								<td>아니, 싫어, 안할래, ... 등의 표현</td>
							</tr>
							<tr>								
								<td class="center_td">inform</td>
								<td class="center_td">특정 Slot 값 제공</td>
								<td>일반적인 평서문 표현으로 Slot 값을 상대방에게 전해 주는 의도에 사용</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">특수</td>								
								<td class="center_td bold_td">unknown</td>
								<td class="center_td">정의되지 않은 의도</td>
								<td>정의된 의도로 인식되지 않을 경우의 발화 의도에 대한 DA Type</td>
							</tr>
							<tr>								
								<td class="center_td bold_td">non_response</td>
								<td class="center_td">시스템 응답이 없는 상황</td>
								<td>사용자 발화 의도에 응답할 수 없도록 설계된 상황을 표현한 DA Type, 이에 대한 시스템 응답을 구축하여 적절한 응답이 가능하도록 사용</td>
							</tr>
							<tr>
								<td class="center_td" rowspan="2">시스템 동작</td>								
								<td class="center_td bold_td">restart</td>
								<td class="center_td">처음부터 시작</td>
								<td>이 DA Type이 인식되면, 시스템은 도메인 처음부터 다시 시작함. 사용자 발화를 구축해야 함. 시스템 응답 대화를 만들어도 무시하고 처음부터 다시 함.</td>
							</tr>
							<tr>								
								<td class="center_td bold_td">repeat</td>
								<td class="center_td">시스템 이전 발화 재요청</td>
								<td>이 DA Type이 인식되면, 시스템이 이전 발화를 재발화하도록 처리함. 사용자 발화를 구축해야 함. 시스템 응답 대화를 만들어도 무시하고 재발화함.</td>
							</tr>
						</tbody>
					</table>	
					<p>기정의된 DA Type에서 엔진에서 그 의미에 따라 다르게 처리되는 것은 request, unknown, non_response, restart, repeat이고 나머지 기정의 DA Type은 새로 정의하는 DA Type과 똑같이 작동을 합니다. affirm, negate, inform에 대해서는 다르게 정의해도 상관이 없습니다.</p>
					<p>요청 Slot을 이용하여 의도 Type을 설정하고 request와 다른 DA Type으로 하고 싶다면, request로 시작하는 다른 DA Type을 설정해서 정의하면 됩니다. 즉, request_weather_info 등으로 정의하게 되면 request 계열 DA Type으로 인식합니다.</p>										
				</div>				
				<div>
					<h6 id="5_2_2menu">의도 정의 방법</h6>										
					<p>이 절의 의도 예제를 살펴 보았듯이, 의도는 개발자가 임의로 정의할 수 있습니다. 의도는 어떻게 정의하면 좋을까요?</p>					
					<p class="dep1">① 시스템이 의도를 잘 인식할 수 있도록 일관성을 가지고 정의해야 합니다. (대화이해 관점)</p>
					<p class="dep2">시스템은 의도 Type을 사용자 문장의 전체 구성으로 보고 찾습니다. 따라서, 문장에서 매우 중요한 동사와 Slot들이 같으면 같은 의도 Type으로 정의하는 것이 좋습니다. 예를 들어, “TV 켜줘”와 “전등 켜줘”를 다른 DA Type으로 정의하기 보다는 같은 DA Type으로 정의하고 대상을 Slot으로 하여 인식 Slot으로 전체 의도를 파악하도록 하는 것이 유리합니다.</p>						
					<p class="dep1">② 도메인 대화모델에서 대화 상태를 잘 구분할 수 있도록 정의해야 합니다. (대화관리 관점)</p>
					<p class="dep2">개발자는 의도 Type 또는 DA Type으로 대화의 현재 상태를 쉽게 파악할 수 있기에 상태마다 다르게 정의하고 싶어 합니다. 사용자 의도는 위에서 언급했듯이 상황에 사용되는 용도가 달라져도 실제 의도가 같다면 동일한 의도로 해주는 것이 시스템 성능에 매우 좋은 영향을 줍니다. 하지만, 시스템 의도는 그렇지 않기에 같은 대답이라도 대화 상태에 따라 다른 의도로 구분하여 DA Type이나 의도 Type으로 대화 상태를 파악하는데 이용하는 것이 좋습니다.</p>					
				</div>
				<h5 id="5_3menu">Intent 생성 (Create Intent)</h5>
				<div>														
					<p>도메인 아래 Intents를 클릭하여 Global Intent를 작성할 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image067.png" /></p>					
					<p>상기처럼 Global Intent에 3개의 Intent가 기본적으로 있습니다. 긍정 응답 affirm과 부정 응답 negate, unknown 의도 Type은 모든 Task에서 사용되고 해당 의도에 대한 사용자 발화가 도메인 내의 모든 Task에서 같을 것이기 때문에 Global로 미리 준비해 두었습니다. 이 3개 의도 Type의 시스템 응답은 Task별로 다를 것이기에 추후 Task 내의 Local Intent에서 같은 의도를 생성하고 시스템 응답만 각각 다르게 정의하기 바랍니다.</p>
					<p>Local Intent는 해당 Task 이름 아래의 Intents를 클릭하여 작성할 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image068.png" /></p>
					<p>Global과 Local 모두의 Intents 화면에서 Create Intent를 클릭하여 새로운 의도를 생성할 수 있습니다. </p>
					<p class="center"><img src="../manualfiles/image069.png" /></p>
					<p>Intent Type은 <a href="#" onclick="fnMove('#5_2menu');">Intent 정의</a>에서 설명한 방법에 따라 작성하면 됩니다. 간단하게는 영문자와 숫자, “_”로 작성하면 됩니다. 물론, 요청 Slot과 의도 Slot을 추가할 수 있습니다. 각 Slot은 Entity에 정의되어 있어야 합니다. 그렇지 않으면 추후에 오류가 발생할 수 있으니 주의해서 작성해 주기 바랍니다.</p>					
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td><b>[주의]<br>Entity의 Slot을 이용하여 Intent를 작성한 이후에, Entity에서 Slot 이름 변경이나 삭제가 이루어지면 다른 모든 경우에 반영이 안됩니다. Entities에서 정의한 Class와 Slot, Intents에서 정의한 Intent 표기법을 다른 곳에서 이용한 이후에 그 표기법을 변경하거나 삭제하면, 기존 작성 부분에서 문제가 발생합니다. 주의해 주기 바랍니다. </b></td>
							</tr>
						</tbody>
					</table>						
				</div>
				<div>
					<h6 id="5_3_1menu">사용자 발화 작성</h6>										
					<p>사용자 발화 부분에 사용자가 정의한 Intent로 발화할 수 있는 다양한 형태들을 기술합니다. 또한 사용자 발화 부분에서 인식할 Slot 부분을 수동 또는 자동 태깅으로 표시해 줘야 합니다. 사용자 발화와 Slot 태깅된 정보를 학습하여 대화시스템이 Intent Type과 인식 Slot을 수행합니다.</p>
				</div>
				<div>
					<h7 id="5_3_1_1menu">사용자 발화 작성 가이드</h7>
					<p></p>
					<p>사용자 발화를 작성할 때에, 인식할 Slot 부분을 같은 한 단어로 반복하여 표현하기 보다는 다양한 단어들로 다양한 사용자 발화 형태들로 작성해야 합니다. 한 유형의 발화에서 Slot 부분에 다양한 단어들을 바꾸어 가면서 굳이 작성할 필요는 없습니다. 하지만, 만약 그 유형의 발화가 매우 중요해서 꼭 잘 인식되어야 한다고 판단하면 그렇게 작성할 필요도 있습니다.</p>
					<p>사용자 발화 패턴의 다양성과 Slot 단어들의 다양성을 어느 정도로 하여 작성해야 하는지 의문이 생길 것입니다. 정확한 답은 없지만 다음처럼 가이드라인을 드립니다. 다음 가이드라인은 대화이해에서 Intent Type 인식과 Slot 태깅을 하는 방법과 관련된 것으로 통계/확률이 매우 중요합니다. “중요한 것은 많이, 안 중요한 것은 적게 작성하라”라는 법칙입니다.</p>
					<ol>
						<li>Slot 단어의 다양성: Slot이 None Slot Type과 <a href="#" onclick="fnMove('#4_2_2_2menu');">New Defined Type</a>에서 설명한 None Type과 New defined type인가에 따라 다릅니다. None Type의 Slot은 여기에서 나타날 수 있는 Slot 단어들을 충분히 많이 기술해 주어야 합니다. New defined type은 많이 기술해 줄 필요는 없습니다. 어차피 기술한 단어 정보를 학습하는 것이 아니라 defined type 개체 코드 정보로 학습하기 때문입니다.</li>
						<li>Slot 단어가 나타나는 위치: 될 수 있으면 다양한 위치에서 나타나도록 작성해 주기 바랍니다. 특정 Slot이나 특정 단어 앞에서도 나타나고 뒤에서도 나타난다면 모두 기술해 주면 좋습니다. 하지만 통계 법칙은 지켜주기 바랍니다. 2개의 Slot, Pizza.type과 Pizza.size가 있을 때, 일반적으로 Pizza.size 뒤에 Pizza.type이 나타나는 것이 매우 일반적이고 반대는 더물다고 합시다. 그렇다면 이 법칙에 따라, 대부분 발화에서는 원래 순서를 지켜주고 경우에 따라 조금 바꿔 주면 됩니다.</li>
						<li>발화 패턴의 다양성: 발화 패턴은 다양하게 해 주면 됩니다. 발화의 패턴이라고 하여 조사/어미의 형태만을 계속 바꾸는 것은 좋지 않습니다. 표현하는 단어들을 다양하게 적고 여기에 적절히 조사와 어미 형태를 바꾸어 주면 좋습니다. 여기에서도 통계의 법칙을 따르기 바랍니다. 일반적으로 이 의도에서 많이 사용하는 발화 패턴을 자주 작성해 주는 것이 좋습니다. 그 이유는 사용자들이 자주 사용하는 패턴이 잘 인식하도록 하는 것입니다.</li>
						<li>다른 Intent Type도 고려: 다른 Intent Type에서 기술하는 발화 패턴과 헷갈리는지를 잘 고려하기 바랍니다. 2개의 발화(Slot까지 포함)가 똑같다면 많이 기술한 쪽으로 인식할 수 밖에 없습니다. 대화이해에서는 이것을 구분할 수 있는 방법은 Task 정보 이외에는 없습니다. Task 정보로 구분이 안된다면 같은 의도로 하는 방법을 고민해 보기 바랍니다. 사실 Task 정보로 구분이 되어도 같은 의도로 하고 Task에서 그 의도에 대한 해석(시스템 응답)을 다르게 하는 것을 매우 권장합니다.</li>
					</ol>
				</div>
				<div>
					<h7 id="5_3_1_2menu">사용자 발화 수동 Slot 태깅</h7>
					<p></p>
					<p>사용자 발화 부분에는 정의한 Intent Type으로 나타날 수 있는 사용자 발화들을 작성합니다. </p>
					<p class="center"><img src="../manualfiles/image070.png" /></p>
					<p>작성한 사용자 발화들은 대화이해에서 Intent Type 인식과 Slot 태깅을 하기 위한 학습문장들로 이용합니다. 위에서처럼 사용자 발화에서 해당 Slot 단어 부분을 마우스로 드래그하여 마우스 오른쪽 버튼을 클릭하면 위에서처럼 인식할 Slot이 나타납니다. <a href="#" onclick="fnMove('#4_2_3menu');">슬롯 단어 정의</a>로 기술한 단어들과 드래그한 단어가 일치하는 Slot들이 선택할 수 있도록 보입니다. 보이지 않는 Slot이라면 “<직접선택>”을 통하여 Slot을 정의해 줍니다. 이렇게 정의해 주는 Slot 정보 표시가 Slot 태깅을 위한 학습 정보로 사용됩니다. </p>
				</div>
				<div>
					<h7 id="5_3_1_3menu">사용자 발화 그룹</h7>
					<p></p>
					<p>다양한 발화들을 작성하고 Slot 태깅을 하여 저장하면, 다음 화면처럼 같은 Slot들이 나타난 발화들을 그룹화합니다.</p>
					<p class="center"><img src="../manualfiles/image071.png" /></p>
					<p>그룹화된 사용자 발화는 <img class="img20" src="../manualfiles/image072.png" /> 를 클릭하여 볼 수 있습니다.</p>
				</div>
				<div>
					<h7 id="5_3_1_4menu">사용자 발화 Slot 태깅 정보 수정</h7>
					<p></p>
					<p>사용자 발화에 대해 Slot 태깅하고 저장하면 아래처럼 사용자 발화 및 Slot 태깅 정보를 수정할 수 없는 상태처럼 보입니다.</p>
					<p class="center"><img src="../manualfiles/image073.png" /></p>
					<p><img class="img20" src="../manualfiles/image074.png" /> 아이콘을 클릭하면 다음과 같이 편집 가능한 형태로 변경합니다. 이후 수정하면 됩니다.</p>
					<p class="center"><img src="../manualfiles/image075.png" /></p>
					<p>Slot 태깅된 단어들 범위가 틀렸거나 원문 형태로 되돌리고 싶으면 <img class="img20" src="../manualfiles/image076.png" /> 을 클릭하기 바랍니다.</p>
					<p class="center"><img src="../manualfiles/image077.png" /></p>
				</div>
				<div>
					<h7 id="5_3_1_5menu">사용자 발화 자동 Slot 태깅</h7>
					<p></p>
					<p>사용자 발화에 대해 자동으로 Slot을 태깅하는 기능을 제공합니다. 단, 기존에 유사한 문장들을 이미 수동 태깅한 경우에만 가능합니다. 자동 Slot 태깅 기능을 사용하기 전에, 먼저 지금까지 수동으로 Slot 태깅한 사용자 발화들을 학습하여야 시스템이 자동으로 태깅할 수 있습니다.</p>
					<p>GenieDialog 우측에 “Dialog”라는 세로 바가 있습니다. 그것을 클릭하면 다음과 같은 화면이 펼쳐집니다. 아래 화면에서 “학습”을 눌러 주면 시스템이 지금까지 입력한 사용자 발화로 학습을 수행합니다. 학습 로그 정보에 다음과 같이 “FINISHED”라고 출력되면 학습이 완료된 상태입니다.</p>
					<p class="center"><img src="../manualfiles/image078.png" /></p>					
					<p>학습 이후에, 아래처럼 사용자 발화를 입력하고  <img class="img20" src="../manualfiles/image079.png" /> 를 클릭하면 자동으로 Slot 태깅됩니다.</p>
					<p class="center"><img src="../manualfiles/image080.png" /></p>
					<p>아래처럼 자동 태깅된 결과에서 “미디엄” 부분은 Pizza.size Slot으로 태깅되었고, “치즈” 부분은 Pizza.type으로 태깅되지 않았습니다. 이 경우에 태깅되지 않은 부분을 수동 태깅해 주기 바랍니다.</p>
					<p class="center"><img src="../manualfiles/image081.png" /></p>
					<p>추가한 사용자 발화들이 자동 태깅에 바로 반영되지 않습니다. 추가한 발화들이 자동 태깅 및 대화시스템에서 이해하게 하기 위해서는 상기에서 수행한 학습을 다시 수행해야 합니다. 추가 및 변경한 사용자 발화를 시스템이 이해하도록 하기 위해서는 꼭 학습을 수행해야만 합니다.</p>
					<p>자동 태깅한 부분이 너무 잘못되어 원문 형태로 작업이 필요하면  <img class="img20" src="../manualfiles/image076.png" />을 클릭해서 원문 형태로 되돌리십시오.</p>
					<p>자동 태깅으로 완벽히 처리된다는 것은 이미 그 발화 유형에 대해서 잘 처리하고 있다는 의미입니다. 잘 처리 못하는 것을 넣는 것이 유리합니다. 사용자 발화 작성은 <a href="#" onclick="fnMove('#5_3_1_1menu');">사용자 발화 작성 가이드</a>를 참고해서 추가하거나 삭제해 주기 바랍니다.</p>
				</div>
				<div>
					<h7 id="5_3_1_6menu">의도 Slot 설정 및 태깅</h7>
					<p></p>
					<p>의도 Slot은 먼저 의도 Slot 등록을 해야 합니다. 의도 Slot 등록은 Intent 생성 화면 상단에서 할 수 있습니다. 다음처럼 명칭과 의도 Slot과 그 값을 설정합니다.</p>					
					<p class="center"><img src="../manualfiles/image082.png" /></p>
					<p>상기 예는 내비게이션 도메인에서 최소 시간 또는 최소 거리에 대한 사용자 발화에 대해 Slot 태깅을 하지 않고 의도 Slot으로 설정하기 위함입니다.</p>
					<p>의도 Slot을 등록한 다음에 태깅할 사용자 발화에서  <img class="img20" src="../manualfiles/image083.png" /> 아이콘을 클릭하여 등록한 의도 Slot 명칭을 선택합니다.</p>					
					<p class="center"><img src="../manualfiles/image084.png" /></p>
					<p class="center"><img src="../manualfiles/image085.png" /></p>					
					<p>상기처럼 선택이 완료되면, 아래처럼 선택한 의도 Slot 명칭이 보이게 됩니다. 의도 Slot 선택 창에서 1개 이상의 의도 Slot 명칭을 선택할 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image086.png" /></p>
					<p>다시 의도 Slot을 해제하고자 하면 다시  <img class="img20" src="../manualfiles/image083.png" /> 아이콘을 클릭하여 선택한 의도 Slot 명칭을 해제하고 “Ok” 버튼을 눌러 주면 됩니다.</p>
					<p>의도 Slot 태깅을 문장마다 하는 것이 불편하면 그룹화 아이콘  <img class="img20" src="../manualfiles/image072.png" /> 를 클릭해서 문장들을 작성하고 Save를 눌러 주면 됩니다. 그러면 그룹화 안에 작성된 모든 문장들은 같은 의도 Slot으로 태깅됩니다. 그룹화 안에서 작성한 것이 잘못 되었으면  <img class="img20" src="../manualfiles/image083.png" /> 아이콘을 클릭하여 의도 Slot 명칭을 추가 클릭 또는 해제하면 됩니다.</p>
					<p>의도 Slot 태깅은 물론 아래처럼 인식 Slot 태깅과 같이 할 수 있습니다.</p>				
					<p class="center"><img src="../manualfiles/image087.png" /></p>
				</div>
				<div>
					<h7 id="5_3_1_7menu">사용자 발화 Download 및 Upload</h7>
					<p></p>
					<p>Global과 Local Intents 화면에 Download Intent와 Upload Intent가 있어서 지금까지 작성된 사용자 발화들을 저장하고 외부에서 작성한 사용자 발화를 업로딩할 수 있습니다. Download 받으면 파일 구조를 알 수 있습니다.</p>					
					<p class="center"><img src="../manualfiles/image088.png" /></p>					
					<p>상기는 파지 주문 도메인에서 Download 받은 Intents의 일부분입니다. 형식은 다음과 같습니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>[원문] 탭문자 [Slot 태깅 문장] 탭문자 [의도]
									<ol>
										<li>원문: 사용자 발화 원문입니다.</li>
										<li>Slot 태깅 문장: 사용자 발화 원문에 Slot 태깅한 문장으로 Slot 태깅 학습 용도입니다.</li>
										<li>의도: 정의한 Intent와 정의된 Task 정보가 있습니다. Intent 뒤에 @@ 이후에 Task 이름이 있습니다. Global Intents에서 정의한 경우에는 Task 정보가 없습니다.</li>
									</ol>
								</td>
							</tr>
						</tbody>
					</table>
					<p>Upload할 파일도 위의 형식과 같이 하여 올리면 됩니다.</p>
				</div>
				<div>
					<h6 id="5_3_2menu">시스템 응답 정의 (응답 대화)</h6>										
					<p>Global 및 Local Intents에서 작성한 사용자 의도 Intent에 대해, 시스템이 대응하는 응답을 작성합니다. 사용자 발화 의도에 응답하는 시스템 발화를 응답 대화로 정의합니다.</p>
					<p>시스템 응답 대화는 아래 Intent 화면과 같이 DA Type이 사용자 입력으로 들어온 발화의 의도 정보와 일치할 때 조건 검사를 통해 발현됩니다.</p>
					<p class="center"><img src="../manualfiles/image089.png" /></p>
					<p class="center"><img src="../manualfiles/image090.png" /></p>
					<p>DA Type이 request 계열일 경우에는 요청 Slot들이 모두 일치할 경우에 사용됩니다. 그리고 정의한 시스템 발화 조건이 만족되는 대화상태이어야 시스템 응답 대화가 출력됩니다.</p>
					<p class="center"><img src="../manualfiles/image091.png" /></p>
					<p class="center"><img src="../manualfiles/image092.png" /></p>
					<p>Global Intent이면 모든 Task 상황에서, Local Intent이면 해당 Task 상황에서 사용자 발화의 DA Type이 일치할 경우에 조건 검사 이후에 출력됩니다. Global Intent에서 사용자 발화들을 정의하고 같은 DA Type으로 Local Intent에서 시스템 응답만 정의할 수 있습니다. 이 경우에는 사용자 발화에 대한 의도 이해는 Task 정보 없이 모든 Task에서 같은 조건으로 수행하고 시스템 응답만이 해당 Task 상황일 때만 가능하게 하는 것입니다.</p>
					<p>응답 대화 작성 방법은 앞에서 설명한 Task 진입 시의 전이 대화와 Slot Filling을 위한 진행 대화 작성 방법과 거의 같습니다. 아래는 피자 주문 도메인에서 사용자가 피자 종류, 크기, 도우 종류를 언급하는 DA Type inform_pizza에 대한 시스템 응답 대화의 예입니다.</p>
					<p class="center"><img src="../manualfiles/image093.png" /></p>
					<p>상기에서 보듯이 같은 응답 대화에서 시스템 발화 조건에 따라 다른 응답이 되도록 구성했습니다. 첫번째 발화 조건은 Pizza.type을 사용자가 발화한 적이 있지만 Pizza Class의 Instance에 사용자가 발화한 값이 없으면, “<Pizza.type> 피자는 판매하고 있지 않습니다.”를 출력하도록 정의한 것입니다. 발화 패턴에서 각 괄호 사이 “<>”의 Slot 이름은 시스템에서 대화이력 또는 해당 Class의 Instance에서 자동으로 찾아서 그 값으로 대체합니다. 두번째, 세번째 발화 조건도 Pizza.size와 Pizza.dough 각각에 대해 똑같은 검사를 하고 발화를 하도록 합니다. 시스템 발화 조건에 대한 보다 자세한 내용은 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>을, 발화 패턴 작성 방법은 <a href="#" onclick="fnMove('#5_5_2menu');">시스템 발화 패턴 작성 방법</a>을 참고하세요.</p>
					<p>시스템 발화 후 행동은 각 시스템 발화 조건을 만족하여 응답을 한 후에 시스템 내부적으로 Slot 값을 변경하는 작업을 수행합니다. 위의 예에 대한 시스템 발화 후 행동은 다음과 같이, 모두 사용자가 발화한 Slot 값을 Reset하여 발화한 적이 없는 것과 같이 초기화합니다. 그 이유는 피자 주문 도메인에서 이 응답 대화 이후에 진행 대화를 수행할 때, 사용자가 발화한 잘못된 값을 초기화함으로써 해당 Slot 값이 없는 것이 되어 진행 대화로 Slot 값을 요청할 수 있기 때문입니다. 시스템 발화 후 행동으로는 Set과 Reset이 있습니다.</p>
					<p class="center"><img src="../manualfiles/image094.png" /></p>
					<p>상기 화면들은 시스템 Intent의 DA Type이 request 계열이 아닌 경우입니다. request DA Type 계열은 진행 대화 화면과 비슷한 구성으로 보입니다. 시스템 intent에 request로 시작하는 DA Type을 선언하면 아래에 요청 Slot을 위한 녹색창이 나타납니다. 녹색 창을 클릭해서 지금까지 선언한 Entity의 Slot들을 선택하면 됩니다. request 계열 DA Type은 1개 이상의 요청 Slot을 요구합니다.</p>
					<p class="center"><img src="../manualfiles/image095.png" /></p>
					<p>request 계열 DA Type에서만 나타나는 “다음 사용자 발화 DA 제한”의 사용 방법은 Slot Filling 대화 방식의 <a href="#" onclick="fnMove('#3_4_1_1menu');">시스템 발화 패턴 작성 방법</a>에서 자세히 설명되어 있습니다.</p>
				</div>
				<h5 id="5_4menu">특수 Intent</h5>
				<div>
					<h6 id="5_4_1menu">unknown Intent</h6>										
					<p>unknown Intent는 모든 도메인에서 자동으로 설정되어 있고, 사용자 발화를 도메인에서 정의한 의도로 인식하지 못한 경우에 대한 의도입니다.</p>
				</div>
				<div>
					<h7 id="5_4_1_1menu">unknown 인식 방법</h7>
					<p></p>
					<p>unknown Intent 인식 방법은 다음 2가지 방법으로 가능합니다.</p>
					<ol class="decimal_ol">
						<li><a href="#" onclick="fnMove('#2_2_1menu');">대화의도 필터링</a>에 의한 unknown 처리: 도메인 설정에서 정의한 대화의도 필터링 한계치 이하 신뢰도로 사용자 의도가 인식된 경우에 unknown으로 처리합니다.</li>
						<li>unknown 용도 사용자 발화: unknown인식되었으면 하는 사용자 발화를 직접 unknown 의도의 사용자 발화로 추가하여 학습하면 됩니다.</li>
					</ol>
					<p class="dep1">아래 화면처럼 Global에 기정의된 unknown Intent에 unknown 처리되었으면 하는 발화들을 추가하면 됩니다. </p>
					<p class="center"><img src="../manualfiles/image096.png" /></p>
				</div>
				<div>
					<h7 id="5_4_1_2menu">unknown Intent 응답 방</h7>
					<p></p>
					<p>unknown Intent일 경우에 시스템 응답 대화를 하는 방법도 2가지가 있습니다.</p>
					<ol class="decimal_ol">
						<li><a href="#" onclick="fnMove('#2_1_1menu');">챗봇 사용하기</a> 설정으로 챗봇이 응답하도록 할 수 있습니다.</li>
						<li>직접 unknown Intent에 시스템 응답 대화를 정의하는 방법입니다. 아래처럼 정의하면 시스템은 unknown 사용자 발화에 대해 “무슨 말씀인지 이해 못했어요.”가 출력됩니다. unknown Intent에 대해서도 도메인에서 정의한 일반적인 사용자 의도와 동일한 방법으로 사용자 발화와 시스템 응답 대화를 정의할 수 있습니다.</li>
					</ol>			
					<p class="center"><img src="../manualfiles/image097.png" /></p>
					<p>만약 챗봇 설정이 되어 있고, 상기와 같이 응답 대화를 정의하면 둘 다 발화합니다.</p>
				</div>
				<div>
					<h6 id="5_4_2menu">non_response Intent</h6>										
					<p>GenieDialog의 목적지향 대화시스템은 사용자 발화에 대해 어떠한 시스템 대답을 할 수 없는 상황에서는 이전에 시스템이 발화한 내용을 다시 내 보냅니다. 이전 시스템 발화의 의도에 여러가지 발화 패턴이 있었으면 임의로 하나를 보내기에 이전 시스템 발화와 조금 다르게 발화할 수도 있습니다. 챗봇연동을 하여 챗봇이 대답을 한 상황이라도 목적지향 대화시스템이 사용자 발화에 어떠한 시스템 응답을 출력하지 못하면 이전 시스템 의도의 발화를 반복합니다.</p>
					<p>목적지향 시스템이 발화할 수 없는 상황이 되면 non_response DA Type에 정의된 시스템 응답을 찾도록 되어 있습니다. 이 non_response Intent가 정의되어 있지 않기에 기존 발화를 반복합니다. 이런 상황에서의 시스템 발화 반복이 싫으면 Global Intent에서 non_response Intent 작성하고 시스템 응답 발화를 “ “로 넣으면 시스템 마지막 발화가 출력되지 않습니다. 물론 다른 발화 “죄송하지만, 제가 대답할 수 없네요.”라는 발화를 넣을 수도 있습니다.</p>
					<p>목적지향 대화시스템에서 non_response가 되는 상황은 다음과 같습니다.</p>
					<ol class="decimal_ol">
						<li>사용자 의도가 unknown이고 그것에 대한 시스템 응답 대화가 정의되어 있지 않은 상황</li>
						<li>사용자 의도가 unknown이 아니지만, 현재 대화이력에서 시스템이 응답할 수 있는 조건의 응답 대화, 전이 대화, 진행 대화가 하나도 없는 상황</li>
					</ol>
					<p>1번 상황에 대해서는 챗봇 연동과 Global Intent로 unknown에 시스템 응답 대화를 정의해서 적절히 대처할 수 있습니다.</p>
					<p>2번의 경우에는 Task마다 non_response를 두고, Task에서 발생할 대화 상태(Entity 이력과 Intent 이력)에 따라 다양한 시스템 발화 조건을 기술하여 응답하도록 처리할 수 있습니다. Task에서 진행 대화와 사용자 의도에 따른 응답 대화를 하나도 기술하지 않고 non_response에서 대화상태에 따라 다양한 응답 대화를 기술하여 Task에서 시스템 응답이 가능하도록 할 수도 있습니다.</p>
				</div>
				<div>
					<h6 id="5_4_3menu">request(unknown) Intent</h6>										
					<p>사용자 발화에서 요청하는 정확한 질문 의도가 무엇인지 모를 경우가 있습니다. 예를 들어, “서울 오늘 날씨 어때?” 이후에 사용자가 “대전은?”이라고 했을 때, “대전은?”에 대해서 문맥없이 “대전은?”의 정확한 의도는 알 수 없습니다. 이전 사용자 발화가 “서울 오늘 미세먼지 수준이 어때?”라고 했을 때, 그 의도가 달라지기 때문입니다. </p>
					<p>사용자 발화만으로는 질문 의도를 정할 수 없는 의도에 대해 request(unknown)으로 정의합니다. 따라서, “대전은?”의 의도는 request(unknown, Weather.city=”대전”)으로 정의합니다.</p>
					<p>사용자 발화의 request 계열 DA Type에서 요청 Slot을 unknown으로 선택하면 이와 같이 현재 사용자 발화로는 정확하게 질문한 요청이 무엇인지 모르지만, 이전의 요청 Slot과 같은 것을 요청하고 있음을 표시합니다. 그리고, 대화 상에서 가장 최근의 요청 Slot으로 unknown을 변경합니다. 최근에 사용자 요청이 날씨정보 Weather.info인지 미세먼지 정보 Weather.dust_info인지에 따라 unknown이 변경됩니다.</p>
					<p>이전 사용자 발화에서 요청 Slot이 없었던 경우에는 unknown이 변경되지 않습니다. 따라서, request(unknown)에 대한 시스템 응답 대화를 “질문하시고 싶은 것이 무엇인지 정확하게 말씀해 주세요.”라고 작성하여 적절한 응답을 할 수 있습니다.</p>
				</div>
				<h5 id="5_5menu">시스템 발화</h5>
				<div>
					<h6 id="5_5_1menu">시스템 발화 종류와 응답 순서</h6>										
					<p>시스템 발화는 전이 대화, 진행 대화, 응대 대화 3가지 종류로 구축됩니다. 각각에 대해서 간단히 언급하면 다음과 같습니다.</p>
				</div>
				<div>
					<h7 id="5_5_1_1menu">전이 대화 (Transition Dialogue)</h7>
					<p></p>
					<p>Task를 새로 시작할 때 시스템이 발화하는 대화를 말합니다. <a href="#" onclick="fnMove('#3_2_3menu');">Task 시작 시, 시스템 발화 정의</a> 참조하기 바랍니다.</p>
				</div>
				<div>
					<h7 id="5_5_1_2menu">진행 대화 (Progress Dialogue)</h7>
					<p></p>
					<p>Task에서 필요한 Slot 정보를 사용자에게 요구하는 대화를 말합니다. <a href="#" onclick="fnMove('#3_4_1menu');">사용자에게 Slot값 질문하기 – Slot Filling 대화 방식</a> 참조하기 바랍니다.</p>
				</div>
				<div>
					<h7 id="5_5_1_3menu">응답 대화 (Response Dialogue)</h7>
					<p></p>
					<p>사용자 의도 마다 시스템이 응답하는 대화를 말합니다. <a href="#" onclick="fnMove('#5_3_2menu');">응답 대화</a> 참조하기 바랍니다.</p>
				</div>
				<div>
					<h7 id="5_5_1_4menu">시스템 응답 대화 순서</h7>
					<p></p>
					<p>시스템의 3가지 대화는 다음과 같은 순서와 알고리즘으로 동작합니다.</p>
					<p class="center"><img src="../manualfiles/image098.png" /></p>
					<ol class="decimal_ol">
						<li>응답 대화 우선: 사용자 의도에 대해 조건이 맞는 응답 대화를 검색하고 먼저 선택됩니다.</li>
						<li>전이 대화: Task 이동은 사용자 발화 이후와 시스템 발화 이후에 각각 일어날 수 있습니다.
							<ol class="upper_ol">
								<li>사용자 발화 이후 Task 이동에서 사용자 발화에 대한 응답 대화가 Task 내에서 선택되면 전이 대화는 선택되지 않습니다. 이동된 Task에서 응답 대화가 없는 경우에 전이 대화가 선택됩니다.</li>
								<li>시스템 발화 이후 Task 이동에서 전이 대화를 선택합니다. 시스템 발화 이후 Task 이동은 시스템의 응답 대화 또는 전이 대화 이후에 일어납니다. 단, 전이 대화로 인한Task 이동은 이전 Task에서 전이 대화만 발화하고 아무런 작업이 없이 다음 Task로 이동한 경우입니다.</li>
							</ol>
						</li>
						<li>진행 대화: Task 이동이 없고, 현재 Task에서 처리할 Slot Filling이 남은 경우에 선택됩니다.</li>
						<li>non_response 응답 대화: 상기에 의해 시스템 응답이 없는 경우, non_response 응답 대화 검색하여 있으면 출력하고 없으면 이전 시스템 발화를 출력합니다.</li>
					</ol>
					<p>사용자 발화 의도가 unknown인 경우에는 unknown에 대한 응답 대화를 정의하였다면 응답 대화가 동작하는 것처럼 작동합니다. 따라서, unknown에 대한 응답 대화를 선택하면, 상기 4번의 non_response 응답 대화가 처리되지 않습니다. unknown에 대한 응답 대화를 정의하지 않으셨다면 응답 대화가 없든지 챗봇이 대신 응답하였든지 상관없이 응답 대화가 없는 것으로 기록됩니다. 이후에 진행 대화 및 전이 대화가 없으면 non_response 응답 대화가 출력됩니다.</p>					
				</div>
				<div>
					<h6 id="5_5_2menu">시스템 발화 패턴 작성 방법</h6>										
					<p>상기 3가지 시스템 대화에는 각 발화 패턴을 작성합니다. 시스템 발화라고 하지 않고 발화 패턴이라고 한 것은 시스템 발화에 변수 부분이 들어가고 이것을 시스템이 알아서 처리하기 때문입니다. </p>
					<p>시스템 발화 패턴의 기본은 “안녕하세요?”처럼 순수하게 문자로만 구성된 것입니다. 문자열에 “<Slot>”로 표기된 변수 부분을 작성할 수 있습니다. 이 부분은 시스템이 값을 채울 수 있으면 채워서 제공합니다. 값을 채우는 방법은 (1) 사용자 발화한 Slot 값과 (2) 사용자 발화한 Slot 값 기반으로 Instance DB에서 검색한 Slot 값입니다.</p>
					<p>변수에 값이 채우질 때, 조사 변화가 있을 수 있습니다. 예를 들어, “<Pizza.type>이:j 있습니다.”에서 Pizza.type의 값이 유종성으로 끝날 경우에는 조사 “이”를, 무종성으로 끝날 경우에는 “가”가 사용될 수 있습니다. 예제처럼 “조사:j”로 표시하여 조사 부분에 대한 처리를 요청할 수 있습니다. 표기하는 조사는 유종성 조사를 붙여야 합니다. 예를 “<Slot>으로:j 갑니다.”처럼 “로”와 “으로” 중에 유종성 음절 뒤에 사용하는 “으로”를 적어야 합니다.</p>
					<p>Slot에 대해 값이 여러 개인 경우를 표기할 때가 있습니다. “우리는 <Pizza.type>, <Pizza.type>, <Pizza.type> 등을 가지고 있습니다.” 인 경우에는 Pizza.type 값이 3개 이상 존재해야 합니다. 보통 이러한 경우는 값을 Instance DB에서 검색하여 채우는 경우입니다. </p>
					<p>Slot의 값이 몇 개 존재하든지 모두 반복해서 표현하는 방법으로 { } 기호를 제공합니다. “{<Pizza.type>}을:j 가지고 있습니다.”라고 표기하면 검색된 모든 Pizza.type을 생성하고 마지막 값의 마지막 음절에 맞추어 “을” 또는 “를”을 생성합니다.</p>
					<p>반복 회수를 제한할 수 있습니다. 반복 회수는 { }:n으로 표기합니다. n은 싶은 숫자를 의미합니다. 상기 예에서 “{<Pizza.type>}:3을:j 가지고 있습니다.”라고 하면 Pizza.type 값을 3개 출력하고 조사를 부착합니다. 3개 이하인 경우에도 그 개수만큼 채웁니다. 단 0개에 대해서는 문제가 발생합니다.</p>
					<p>여러 개 Slot간 관계가 있는 경우의 생성 패턴 작성은 어떻게 해야 할까요? 예를 들어, “<Pizza.type>으로는:j <Pizza.size> 크기의 <Pizza.dough>와 <Pizza.size> 크기의 <Pizza.dough> 등이 있습니다.”라고 표기한 것은 사용자가 Pizza.type을 발화하고 그것에 의해 다수 개의 Pizza.size와 Pizza.dough가 검색된 예제입니다. 피자 Instance DB에서는 Pizza.size와 Pizza.dough가 Record별로 저장되어 있을 것입니다. 상기 패턴에서는 Pizza.size와 Pizza.dough를 각각 개별적으로 검색해서 먼저 Pizza.size 변수에 값들을 채우고 나서 Pizza.dough 변수에 값들을 채우기에 저장된 Record 별로 값을 채우지 않습니다. </p>
					<p>Instance DB에 저장된 Record 별로 Pizza.size와 Pizza.dough 값을 출력하기 위해서는 { }와 { }:n을 사용해서 관계를 묶어서 출력하게 해야 합니다. “<Pizza.type>으로는:j {<Pizza.size> 크기의 <Pizza.dough>}이:j 있습니다.”와 “<Pizza.type>으로는:j {<Pizza.size> 크기의 <Pizza.dough>}:2이:j 있습니다.”라고 표기할 수 있습니다. 보면 알겠지만, { } 안 쪽에는 검색되는 Slot들로, 바깥쪽에는 검색 조건이 되는 Slot들로 배치해야 합니다.</p>
					<p>시스템 발화 패턴에는 특정 함수를 사용하여 값을 출력하는 방법도 있습니다.. 함수는 <a href="#" onclick="fnMove('#6_3menu');">대화 Script 함수</a>에서 설명하는 함수들을 이용할 수 있습니다. 간단한 예를 들어 보겠습니다. “<Pizza.type>으로는:j &lt;script=Count(“Pizza.size”)&gt; 종류의 크기와 &lt;script=Count(“Pizza.dough”)&gt; 종류의 도우가 있습니다.”처럼 정의할 수 있습니다. 이 때, Count(“Slot”) 함수는 Slot이 가지는 값의 개수를 알려줍니다. 이와 같이 &lt;script=Script Command&gt;로 정의할 수 있습니다. Script Command는 함수는 <a href="#" onclick="fnMove('#6_3menu');">대화 Script 함수</a>에서 정의하는 함수들과 산술 연산자 “+, -, *, /, %, ^(자승)”을 사용할 수 있습니다. Script Command에 가장 많이 사용되는 함수 Value(“Slot”)은 Slot의 값을 출력합니다. 예를 들어, 피자 주문에서 개수를 선택하는 Slot Pizza.num을 추가한다면, 최종적으로 사용자가 종류, 크기, 도우를 선택하고 그 피자에 대해 주문한 개수를 선택했다면, “총 금액은 &lt;script=Value(“Pizza.price”)*Value(“Pizza.num”)&gt; 원입니다.”라고 정의할 수 있습니다.</p>
					<p>지금까지 살펴본 시스템 발화 패턴에서 사용한 예약 문자가 있습니다. 예약 문자 :, &lt;, &gt;, {, }, !(내부적 사용), \(예약문자 출력용도)를 발화 패턴에서 출력하고자 할 때는 \를 앞에 붙여서 입력하기 바랍니다. 따라서, \:, \<, \>, \{, \}, \!, \\로 작성하면 예약 문자들이 출력됩니다.</p>
					<p>Slot Type이 number인 경우에는 기본적으로 정수(Integer)로 출력합니다. 그 값이 실수여도 소수점 아래에 대해서는 기본적으로 출력되지 않습니다. 예를 들어, Slot FromTo.sum이 number Type이고 그 값이 “1243.6354”일 경우에, 대화 패턴이 “&lt;FromTo.sum&gt;입니다.”라고 하면 시스템은 “1243입니다.”만을 출력합니다. </p>
					<p>Slot Type이 number인 Slot에 대해서 실수 형태로 출력하고자 한다면, Slot 변수 다음에 “:f.n” 포맷을 정의해서 출력 가능합니다. 여기에서, “:f.”는 실수 형태 출력을 의미하고 n은 소수점 “.” 다음에 최대 출력할 자리를 표시합니다. n은 1부터 9까지의 수만 허용합니다. 나머지 형태에 대해서는 실수 출력 포맷으로 보지 않고 문자열로 보고 출력합니다. 따라서, “:f.0”이나 “:f.a” 등은 그대로 출력됩니다. 그리고, n자리 이후 자리에 대해 내림을 반영합니다.  다음은 출력 패턴에 따른 실제 출력 예제입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="35%" />
							<col width="30%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>생성 패턴</th>
								<th>실제 출력</th>
								<th>비고</th>
							</tr>
							<tr>
								<td>&lt;FromTo.sum&gt;:f.2입니다.</td>
								<td>1243.63입니다.</td>
								<td>소수 3자리이하 내림 적용</td>
							</tr>
							<tr>
								<td>&lt;FromTo.sum&gt;:f.9입니다.</td>
								<td>1243.6354입니다.</td>
								<td>모자란 소수자리 0으로 채우지 않음</td>
							</tr>
							<tr>
								<td>&lt;FromTo.sum&gt;:f.10입니다.</td>
								<td>1243.60입니다.</td>
								<td>“:f.1”까지 포맷으로 보고, 나머지는 “0입니다.”는 출력 문자열로 봄</td>
							</tr>
							<tr>
								<td>&lt;FromTo.sum&gt;:f.0입니다.</td>
								<td>1243:f.0입니다.</td>
								<td>&lt;FromTo.sum&gt; 정수 출력하고 나머지는 문자열로 봄. </td>
							</tr>
							<tr>
								<td>&lt;FromTo.sum&gt;:f.a입니다.</td>
								<td>1243:f.a입니다.</td>
								<td>&lt;FromTo.sum&gt; 정수 출력하고 나머지는 문자열로 봄. </td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<!-- 6장 start -->				
				<h4 id="6menu">대화이력 조건과 행동 정의</h4>
				<div>					
					<p>대화시스템은 사용자와 대화한 상태를 관리하고 그 상태에 가장 적합한 시스템 응답을 찾습니다. 대화상태를 정의하기 위해서 우리는 지금까지 Task, Entity, Intent에 대해 정의했습니다. 대화상태 및 대화이력은 3가지 Task, Entity, Intent에 대한 기록입니다. 대화시스템은 이 이력을 검사하여 Task 이동, Slot 값 변경 및 Instance DB 검색, 시스템 응답을 결정합니다. 따라서, 대화이력 조건을 검색하고 그 대화이력을 변경하는 것이 매우 중요합니다. 이 장에서 대화이력 조건 설정 방법과 대화이력 중 Entity를 변경하는 행동 설정 방법에 대해 설명합니다.</p>
				</div>
				<h5 id="6_1menu">대화이력 조건</h5>
				<div>														
					<p>대화이력 조건을 검색하는 부분은 Task 이동 조건, Slot value relationship 조건과 시스템 발화 조건이 있습니다. 3개 대화이력 조건 창은 동일하게 Intent, Entity, Task 검사를 제공하고 각 검사를 and/or로 결합하게 되어 있습니다. </p>
				</div>
				<div>
					<h6 id="6_1_1menu">Intent 검사</h6>										
					<p>Intent 검사는 대화 이력에서 사용자, 시스템이 발화한 DA Type을 검사합니다. Task 이동 조건, Slot 관계 조건, 시스템 발화 조건에서 Intent 검사 화면에 차이가 있습니다. 먼저 Task 이동 조건 기준으로 Intent 검사 항목을 설명합니다.</p>
					<p class="center"><img src="../manualfiles/image099.png" /></p>
					<p>모든 대화이력 조건 창은 상단의 편집 창에 각 Intent 검사, Entity 검사, Task 검사에서 Add한 조건이 첨가됩니다. 첨가되는 조건을 기존 조건과 and/or 관계와 우선 순위를 ( )로 정의합니다.</p>
					<p>Intent 검사에서 사용하는 Intent 이력 검사 함수는 다음 1가지입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="15%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th colspan="2">Intent(“talker”, “DA Type”, “turn”, “true/false”):<br>talker가 turn 전에 DA Type 발화가 한 것이 true/false인지를 검사
								</th>
							</tr>
							<tr>
								<td>talker</td>
								<td>“user” or “system”</td>
							</tr>
							<tr>
								<td>DA Type</td>
								<td>검사할 의도의 DA Type</td>
							</tr>
							<tr>
								<td>turn</td>
								<td>“true” or “false”로 표기한 조건의 원하는 결과 표시</td>
							</tr>
							<tr>
								<td>true/false</td>
								<td>“true” or “false”로 표기한 조건의 원하는 결과 표시</td>
							</tr>
						</tbody>
					</table>
					<p>Task 이동 조건 창에서는 중간에 “사용자”와 “시스템” 버튼이 있는 것이 다른 조건 창과 가장 큰 차이입니다. Task 이동이 사용자 발화 또는 시스템 발화 이후에 검사하여 이루어지기 때문에, 그 기준을 잡기 위한 것입니다. 사용자 발화 이후에 Intent 이력 검사로 수행하고자 하면 “사용자”를, 그렇지 않으면 “시스템”을 클릭하면 됩니다. 상기 창과 같이 “사용자” 클릭을 하면 “0턴 전 사용자 DA Type”이 보입니다. “0턴 전”의 의미는 지금 발화한 사용자 의도 중에서 선택하는 DA Type이 있는지 없는지를 검사한다는 의미입니다. 그 아래 “inform_payment_type”이 적힌 부분을 클릭하면 Intents에서 정의한 모든 사용자 DA Type을 볼 수 있습니다. 그 중에서 원하는 것을 선택하면 됩니다. 그리고, 그 DA Type이 Intent 이력에 있어야 하는지 없어야 하는지를 옆의 “true”나 “false”를 선택하면 됩니다. 그리고, 마지막으로 정의한 조건을 “Add” 버튼을 클릭하여 전체 조건 편집창에 추가합니다. 이 때, 추가한 조건은 “Intent(“user”, “inform_payment_type”, “0”, “true”)”입니다. </p>
					<p>Slot value relationship에서의 조건은 사용자 발화에서 Slot의 값 입력이 있을 때에, 조건 검사가 시작되기에 아래 화면과 같이 “0턴 전 사용자 DA Type”으로 시작하도록 되어 있습니다.</p>
					<p class="center"><img src="../manualfiles/image100.png" /></p>
					<p>시스템 발화 조건 창은 전이 대화, 진행 대화, 응답 대화에 따라서 다 Intent 이력 검사 시점이 달라서 다르게 표현되어 있습니다. 시스템이 발화할 응답들을 검색하는 시점에, 시스템이 발화할 턴으로 대화이력이 정의됩니다. 따라서, 지금 사용자 입력은 1턴전의 사용자 발화가 됩니다. 그리고, 시스템들이 지금 출력하는 발화들은 0턴전의 시스템 발화입니다. 전이 대화와 진행 대화는 Intent 검사 창에 “0턴 전 시스템 DA Type”으로 시작합니다. 응답 대화의 경우는 사용자 DA Type에 대한 시스템 응답이기에 응답 대화는 시스템 발화 중에서 가장 먼저 시작하기에 “0턴 전”과 “1턴 전” 조건을 검사할 필요가 없습니다. 따라서 아래처럼 “2턴 전 시스템 DA Type”부터 검사하면 됩니다.</p>
					<p class="center"><img src="../manualfiles/image101.png" /></p>					
				</div>
				<div>
					<h6 id="6_1_2menu">Entity 검사</h6>
					<p>Entity 이력 검사는 대화이력 검사가 필요한 모든 경우에 동일한 화면을 제공합니다. </p>
					<p class="center"><img src="../manualfiles/image102.png" /></p>
					<p>Entity 검사는 4가지 함수가 제공됩니다. 다음 3가지 함수는 Slot 상태를 검사하는 부분입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="24%" />
							<col width="26%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>UI 상 명칭</th>
								<th>함수명</th>
								<th>설명</th>
							</tr>
							<tr>
								<td class="center_td">슬롯 값 존재 확인</td>
								<td class="center_td">Entity(“Slot”, “검사대상”, “true/false”)</td>
								<td>검사대상에 Slot이 값을 가지고 있는지에 대한 true 또는 false 검사</td>
							</tr>
							<tr>
								<td class="center_td">슬롯 값 개수 확인</td>
								<td class="center_td">Count(“Slot”, “검사대상”)</td>
								<td>검사대상에서 Slot이 가지는 값의 개수</td>
							</tr>
							<tr>
								<td class="center_td">슬롯 값 정보 확인</td>
								<td class="center_td">Value(“Slot”, “검사대상”)</td>
								<td>검사대상에서 Slot이 가지는 값 확인</td>
							</tr>
						</tbody>
					</table>
					<p>검사 대상은 “사용자 발화 내용”과 “시스템 응답 가능 내용”로 구분합니다.</p>
					<ol>
						<li>사용자 발화 내용: Entity 대화이력의 각 Slot에 기록된 값을 대상으로 합니다. Entity 대화이력에는 사용자가 발화한 Slot의 값들이 기록되어 있습니다. 그리고, 시스템에서 행동(action)으로 Set과 Reset함수로 처리한 Slot 값도 기록되어 있습니다. 즉, 사용자 입력 슬롯 값과 행동(action)으로 이루어진 슬롯 값을 대상으로 합니다.</li>
						<li>시스템 응답 가능 내용: 시스템이 Entity 대화이력의 Slot 내용(사용자 발화 내용과 action결과)과 이 정보들로 각 Class의 Instance DB에서 검색한 Slot값을 대상으로 합니다. 시스템은 해당 Slot 정보를 먼저 Entity 대화이력에서 검색합니다. 있으면 그 정보를 이용합니다. 하지만, Entity 대화이력에 없는 경우에는 검사 대상 Slot의 Class Instance DB에서 값과 정보를 검색합니다. Class Instance DB의 검색은 Entity 대화이력에서의 해당 Class의 Slot들(Instance와 연동한 Slot들)이 가진 값들을 AND 조건하여 검색합니다.</li>
					</ol>
					<p>상기 Entity 함수들로 원하는 Slot 값이 사용자가 발화했는지, 시스템이 응답이 가능한지를 Entity() 함수로 판단할 수 있습니다. 그리고, 각 Slot 값의 개수를Count() 함수로, Slot 값이 어떠한 값이 있는지를 Value() 함수로 알 수 있습니다.</p>
					<p>상기 3개 함수와 달리, “사용자 발화 슬롯 값 Instance 존재 확인”의 함수 Instance(Slot, true/false)는 해당 Slot이 Entity 대화이력에 있을 경우(사용자가 직접 발화 또는 행동 Set함수 사용), 그 값이 도메인 Instance DB에도 있는지 없는지를 판단합니다. 이는 피자 주문 도메인에서 사용자가 “물고기 피자 주세요”라고 발화하여 Pizza.type=”물고기”로 인식되었을 때, Instance DB에 있는지 없는지를 확인하고 없다면 시스템이 “물고기 피자는 판매하지 않습니다.”라고 답변할 수 있습니다. 이 Instance 함수는 Slot 값이 Entity 대화이력에 존재하지 않으면 무조건 false입니다.</p>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="24%" />
							<col width="26%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>UI 상 명칭</th>
								<th>함수명</th>
								<th>설명</th>
							</tr>
							<tr>
								<td class="center_td">사용자 발화 슬롯 값 Instance 존재 확인</td>
								<td class="center_td">Instance(“Slot”, “true/false”)</td>
								<td>Slot이 Entity 대화이력에 값이 있고 그 값이 Instance DB에도 존재하는지에 대한 true 또는 false</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<h6 id="6_1_3menu">Task 검사</h6>
					<p>Task 이력 검사는 2가지 함수를 제공합니다. 지금 Task 이전에 어떤 Task로 진행 되었는지를 검사할 수 있고, Task의 완료 여부를 검사할 수 있습니다.</p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="24%" />
							<col width="26%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>UI 상 명칭</th>
								<th>함수명</th>
								<th>설명</th>
							</tr>
							<tr>
								<td class="center_td">완료 Task 여부</td>
								<td class="center_td">FinishedTask(“Task”, “true/false”)</td>
								<td>Task가 완료 되었는지를 검사</td>
							</tr>
							<tr>
								<td class="center_td">Task 이력 확인</td>
								<td class="center_td">PreviousTask(“N”, “Task”, “true/false”)</td>
								<td>N 번째 이전 Task가 맞는지를 검사</td>
							</tr>
						</tbody>
					</table>
					<p class="center"><img src="../manualfiles/image103.png" /></p>					
				</div>
				<h5 id="6_2menu">행동(Action)</h5>
				<div>														
					<p>행동(Action)은 Entity 이력의 Slot 값을 변경하는 역할을 합니다. 상기 대화이력 조건 검사와 쌍으로 나타납니다. Task 이동을 제외하고, Slot value relationship의 Action과 시스템 발화 후 행동이 있습니다. 2가지 행동의 화면 창은 동일합니다. 행동은 Set 함수와 Reset 함수를 제공합니다. Set 함수는 Set(“Slot”, “Value”)로 표기되며 Entity 이력의 Slot에 Value로 변경합니다. 아래는 Pizza.dough Slot에 “미디엄” 값으로 변경하라는 것입니다.</p>
					<p class="center"><img src="../manualfiles/image104.png" /></p>
					<p>Set함수의 Value 부분에 다른 함수나 그 함수들로 된 산술 연산이 가능합니다. 피자 주문 도메인에서 전체 가격인 Order.total_price에 피자 가격과 음료 가격을 더한 값을 Set 함수로 정의할 수 있습니다. Value 버튼을 클릭하여 각 Slot을 선택하고 “+” 연산자로 결합하여 작성하여Value("Pizza.price", "system_response")+Value("Drink.price", "system_response")로 정의하여 전체 가격을 Order.total_price에 기록할 수 있습니다.</p>
					<p>상기 Set 함수에서 PrintDate과 PrintTime은 Slot이 system이 제공하는 date Type이나 time Type으로 설정되어 내부적으로 “0000-00-00” 의 날짜 표현과 “00:00:00”의 시간 표현을 문자열 표현으로 변경합니다.</p>
					<p>PrintDate은 다음과 같이 3가지 문자열 형태를 제공합니다. Set함수에 PrintDate을 적용하여 “2019-01-23”과 같은 표현을 아래처럼 바꾸어서 다른 string Type Slot에 저장하여 출력에 사용할 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image105.png" /></p>
					<ol>
						<li>년월일 형식: 2019년 1월 23일과 같은 형식으로 출력합니다. 단, 현재 일과 년도가 일치하면 년도는 출력하지 않습니다.</li>
						<li>년월일(요일) 형식: 2019년 1월 23일 수요일과 같은 형식으로 출력합니다. 단, 현재 일과 년도가 일치하면 년도는 출력하지 않습니다.</li>
						<li>오늘/내일 형식: 그저께, 어제, 오늘, 내일, 모레까지는 명칭으로 출력하고 나머지는 년월일 형식으로 출력합니다.</li>
					</ol>
					<p>PrintTime도 time Type의 Slot을 문자열 표현으로 변경합니다. 아래 화면을 보면 쉽게 표현 방법을 알 수 있습니다.</p>
					<p class="center"><img src="../manualfiles/image106.png" /></p>
					<p>행동에서 Reset 함수를 제공합니다. Reset 함수의 인자로 Slot이면 해당 Slot의 값을 대화이력에서 초기화합니다. 만약 인자가 Class이면 Class의 모든 Slot들의 값을 대화이력에서 초기화합니다.</p>
					<p class="center"><img src="../manualfiles/image107.png" /></p>
					<p>행동은 여러 개의 Set과 Reset들을 같이 표현할 수 있습니다. “;”으로 각 함수를 구분하여 작성하면 순차적으로 여러 개의 함수들을 행동하게 합니다.</p>
				</div>
				<h5 id="6_3menu">대화 Script 함수</h5>
				<div>
					<p>대화 Script 함수는 대화이력을 검색하는 함수들입니다. 지금까지 설명한 대화이력 조건 검사와 행동의 함수들은 일반 사용자가 쉽게 이해하도록 표현한 것입니다. 이들 함수는 다음의 실제 함수들로 Mapping이 되어서 내부적으로 수행합니다. 본 장에서 시스템 내부적으로 사용하는 함수들을 소개하는 이유는 다음 2가지 때문입니다.</p>
					<ol class="decimal_ol">						
						<li>디버깅 용도: 도메인 대화지식 구축 후에, 실제 동작 여부를 검토해야 합니다. <a href="#" onclick="fnMove('#7menu');">대화시스템 구동</a>에서 설명하는 방식으로 구축한 대화지식을 동작해 볼 수 있습니다. 하지만 구축한 의도대로 동작하지 않을 수 있습니다. 이에 대한 검토가 필요한데, 대화시스템이 출력한 로그 정보로 분석이 가능합니다. 이 로그 정보는 <a href="#" onclick="fnMove('#0append2');">[부록 2] DevLog 정보 설명</a>에 자세히 설명되어 있습니다. 이 로그에 우리가 위에서 작성한 대화이력 조건 함수가 다른 함수로 Mapping되어 출력합니다. 그 정보를 해석하기 위해 필요합니다.</li>
						<li>시스템 발화 패턴 script 사용: <a href="#" onclick="fnMove('#5_5_2menu');">시스템 발화 패턴 작성 방법</a>에서 &lt;script=Script Command&gt;를 사용할 수 있기 위해, Script Command에서 사용할 수 있는 함수는 이 절에서 소개하는 함수들만 사용이 가능합니다. 상기 <a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>과 <a href="#" onclick="fnMove('#6_2menu');">행동(Action)</a>에서 소개한 함수들을 직접적으로 사용할 수 없습니다.</li>
					</ol>
				</div>
				<div>
					<h6 id="6_3_1menu">DA Type 검사 Script 함수</h6>
					<p>Intent 이력에서 DA Type의 존재 여부를 검사하는 내부 Script 함수는 다음과 같습니다. <a href="#" onclick="fnMove('#6_1_1menu');">Intent 검사</a>의 Intent(“talker”, “DA Type”, “turn”, “true/false”) 함수가 다음처럼 내부적으로 변경됩니다. 이 함수들은 시스템 발화 패턴 script에서 사용되지 않습니다. </p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>내부 Script 함수</th>
								<th>설명</th>
								<th>Intent 검사 대응 관계</th>
							</tr>
							<tr>
								<td class="center_td">IsDAType(“talker”, “DA Type”)==true/false</td>
								<td>Intent 이력 전체에서 “talker(user or system)”이 “DA Type”을 발화한 적이 있는가 검사하는 함수</td>
								<td>Intent() 함수에서 “turn”이 전체를 나타내는 “*”일 때 대응</td>
							</tr>
							<tr>
								<td class="center_td">IsDATypeAtUtterHistory(“talker”, “DA Type”, “turn”)==true/false</td>
								<td>“talker”가 “turn” 전에 “DA Type”을 발화했는가 검사하는 함수</td>
								<td>위를 제외하고 나머지 Intent() 함수에 대응</td>
							</tr>
						</tbody>
					</table>									
				</div>
				<div>
					<h6 id="6_3_2menu">Entity 관련 Script 함수</h6>
					<p>Entity 이력에 대한 내부 Script 함수와 대응되는 <a href="#" onclick="fnMove('#6_1_2menu');">Entity 검사</a>의 함수는 다음과 같습니다.</p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
							<col width="8%" />
							<col width="15%" />
						</colgroup>
						<tbody>
							<tr>
								<th rowspan="2">내부 Script 함수</th>
								<th rowspan="2">설명</th>
								<th colspan="2">Entity 검사 함수 대응</th>
							</tr>
							<tr>								
								<th>검사대항</th>
								<th>Entity 함수</th>
							</tr>
							<tr>
								<td>ExistValueFromDialog(“Slot”)==true/false</td>
								<td>Entity 대화이력에 Slot의 값이 있는가 검사하는 함수</td>
								<td class="center_td" rowspan="4">사용자 발화 내용</td>						
								<td>Entity(“Slot”, “user_history”, “true/false”)</td>
							</tr>
							<tr>
								<td>CountHistory(“Slot”)</td>
								<td>Entity 대화이력의 Slot 값 개수를 출력하는 함수	</td>
								<td>Count(“Slot”, “user_history”)</td>
							</tr>
							<tr>
								<td>HasValueHistory(“Slot”, “Value”)==true/false</td>
								<td>Entity 대화이력의 Slot의 값 중에 Value가 있는가를 검사하는 함수 Entity 함수에서는 ==, != 에만 해당함. <br>크기 비교는 아래 ValueHistory(“Slot”)함수 이용</td>
								<td>Value(“Slot”, “user_history”) ==/!= “Value”</td>
							</tr>
							<tr>
								<td>ValueHistory(“Slot”)</td>
								<td>Entity 대화이력에서 Slot의 값을 출력하는 함수 Slot 값이 다수 개일 때는 임의의 하나만 출력함.<br>Slot 값을 없으면 “”를 출력함. Entity 함수 Value()에서 값 크기 비교 >, >=, <, <=에 사용</td>
								<td>Value(“Slot”, “user_history”) >/>=/</<= “Value”</td>
							</tr>
							<tr>
								<td>ExistValue(“Slot”)==true/false</td>
								<td>Entity 대화이력과 Instance DB를 이용하여 Slot의 값을 검색할 수 있는가를 검사하는 함수</td>
								<td class="center_td" rowspan="5">시스템 응답 가능 내용</td>
								<td>Entity(“Slot”, “system_response”, “true/false”)</td>
							</tr>
							<tr>
								<td>Count(“Slot”)</td>
								<td>Entity 대화이력과 Instance DB를 기반으로 출력한 Slot의 값 개수를 출력하는 함수	</td>
								<td>Count(“Slot”, “system_response”)</td>
							</tr>
							<tr>
								<td>HasValue(“Slot”, “Value”)==true/false</td>
								<td>Entity 대화이력과 Instance DB를 기반으로 출력한 Slot의 값 중에 Value가 있는가를 검사하는 함수 Entity 함수에서는 ==, != 에만 해당함. <br>크기 비교는 아래 Value(“Slot”)함수 이용</td>
								<td>Value(“Slot”, “system_response”) ==/!= “Value”</td>
							</tr>
							<tr>
								<td>Value(“Slot”)</td>
								<td>Entity 대화이력과 Instance DB를 기반으로 출력한 Slot의 값을 출력하는 함수<br>Slot 값이 다수 개일 때는 임의의 하나만 출력함. <br>Slot 값을 없으면 “”를 출력함.<br>Entity 함수 Value()에서 값 크기 비교 >, >=, <, <=에 사용</td>
								<td>Value(“Slot”, “system_response”) >/>=/</<= “Value</td>
							</tr>
							<tr>
								<td>ExistValueAtDB(“Slot”)==true/false</td>
								<td>Entity 대화이력의 Slot에 값이 존재할 때, 그 값이 Instance DB에 존재하는가를 검사하는 함수</td>
								<td>Instance(“Slot”, “true/false”)</td>
							</tr>
						</tbody>
					</table>	
					<p>상기 Entity 관련 내부 Script 함수 중에서 특정 값을 출력하는 함수, Value(), Count(), ValueHistory(), CountHistory()은 시스템 발화 패턴의 &lt;script=Script Command&gt;에 사용할 수 있습니다.</p>								
				</div>
				<div>
					<h6 id="6_3_3menu">Task 관련 Script 함수</h6>
					<p>Task 이력 검사하는 내부 Script 함수는 다음과 같습니다. 이 함수들은 true/false를 출력하기에 시스템 발화 패턴의 script에 사용할 수 없습니다.</p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>내부 Script 함수</th>
								<th>설명</th>
								<th>Task 검사 대응 관계</th>
							</tr>
							<tr>
								<td>FinishedTask(“Task”)==true/false</td>
								<td>Task가 완료 되었는지를 검사하는 함수</td>
								<td>FinishedTask(“Task”, “true/false”)</td>
							</tr>
							<tr>
								<td>PreviousTask(“N”, “Task”)==true/false</td>
								<td>N 번째 이전 Task가 맞는지를 검사하는 함수</td>
								<td>PreviousTask(“N”, “Task”, “true/false”)</td>
							</tr>
						</tbody>
					</table>									
				</div>
				<div>
					<h6 id="6_3_4menu">날짜/시간 Script 함수</h6>
					<p>Slot Type이 date와 time인 경우에 내부적 날짜 표현 “0000-00-00”와 시간 표현 “00:00:00”을 문자열로 변환하는 내부 Script 함수는 다음과 같습니다. 이 함수들은 시스템 발화 패턴 script에 사용하면 내부 날짜/시간 표현을 일반적인 표현으로 변경하여 출력할 수 있습니다.</p>									
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>날짜 내부 Script 함수</th>
								<th>설명</th>
								<th>Set의 PrintDate 함수 대응 관계</th>
							</tr>
							<tr>
								<td>PrintDateToday(“Slot”)</td>
								<td>오늘/내일 형식: 그저께, 어제, 오늘, 내일, 모레까지는 명칭으로 출력하고 나머지는 년월일 형식으로 출력합니다.</td>
								<td>PrintDate(“Slot”, “오늘/내일 형식”)</td>
							</tr>
							<tr>
								<td>PrintDate(“Slot”)</td>
								<td>년월일 형식: 2019년 1월 23일과 같은 형식으로 출력합니다. 단, 현재 일과 년도가 일치하면 년도는 출력하지 않습니다.</td>
								<td>PrintDate(“Slot”, “년월일 형식”)</td>
							</tr>
							<tr>
								<td>PrintDateWeek(“Slot”)</td>
								<td>년월일(요일) 형식: 2019년 1월 23일 수요일과 같은 형식으로 출력합니다. 단, 현재 일과 년도가 일치하면 년도는 출력하지 않습니다.</td>
								<td>PrintDate(“Slot”, “년월일(요일) 형식”)</td>
							</tr>
						</tbody>
					</table>
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
							<col width="30%" />
						</colgroup>
						<tbody>
							<tr>
								<th>시간 내부 Script 함수</th>
								<th>설명</th>
								<th>Set의 PrintTime 함수 대응 관계</th>
							</tr>
							<tr>
								<td>PrintTimeAMPMWithoutSec(“Slot”)</td>
								<td>오전/오후 X시 X분 형식 출력</td>
								<td>PrintTime("Slot", "오전/오후 표시, 초 비포함")</td>
							</tr>
							<tr>
								<td>PrintTimeAMPM(“Slot”)</td>
								<td>오전/오후 X시 X분 X초 형식 출력</td>
								<td>PrintTime("Slot", "오전/오후 표시, 초 포함")</td>
							</tr>
							<tr>
								<td>PrintTimeWithoutSec(“Slot”)</td>
								<td>0~23시 x분 형식 출력</td>
								<td>PrintTime("Slot", "24시간 표시, 초 비포함")</td>
							</tr>
							<tr>
								<td>PrintTime(“Slot”)</td>
								<td>0~23시 x분 x초 형식 출력</td>
								<td>PrintTime("Slot", "24시간 표시, 초 포함")</td>
							</tr>
						</tbody>
					</table>									
				</div>
				<div>
					<h6 id="6_3_5menu">Set/Reset Script 함수</h6>
					<p>Set과 Reset의 내부 Script 함수는 <a href="#" onclick="fnMove('#6_2menu');">행동(Action)</a>에서 정의한 것과 동일합니다. 단지, Set(“Slot”, Script Command)형식일 경우에 Script Command를 위에서 정의한 내부 Script 함수로 표현합니다.</p>
				</div>
				
				<!-- 7장 start -->				
				<h4 id="7menu">대화시스템 구동</h4>
				<div>					
					<p>도메인 구축 과정에서 구축한 내용이 제대로 동작하는지를 대화시스템으로 직접 수행하여서 검토할 수 있습니다. 아래 화면에서 빨간색 박스로 표시된 “Dialog”를 마우스를 클릭하면 대화시스템을 구동할 수 있는 DIALOG 화면이 보입니다.</p>
					<p class="center"><img src="../manualfiles/image108.png" /></p>
					<p class="center"><img src="../manualfiles/image109.png" /></p>
				</div>
				<h5 id="7_1menu">학습과 지식저장</h5>
				<div>														
					<p>대화시스템을 동작하기 전에 먼저 “학습”을 해 주어야 합니다. “학습”은 지금까지 도구에서 작업한 내용을 대화시스템이 사용하는 지식으로 저장하고, 대화이해 학습을 수행하여 대화시스템이 동작할 수 있는 지식들을 준비합니다. 학습이 완료되면 아래와 같은 메세지 “FINISHED”가 출력됩니다.</p>
					<p class="center"><img src="../manualfiles/image078.png" /></p>
					<p>도메인에 사용자 발화를 추가하는 작업을 수행했고, 그 추가한 사용자 발화들을 시스템이 이해하는데 사용했으면 하면 “학습”을 다시 해 줘야 합니다.</p>
					<p>Entity에서 Slot의 Type을 New defined type으로 설정을 바꾸고, New defined type 또는 Sys.person, Sys.place, Sys.date, Sys.time으로 변경하거나 반대로 개체로 설정된 Slot Type을 None으로 바꾸게 되면 “학습”을 다시 해 줘야 합니다.</p>
					<p>사용자 발화 추가 없거나 Slot Type 변경이 없는 경우, 도메인의 다른 지식을 변경, 추가, 삭제 등을 하셨다면, “지식저장”만을 눌러 주면 됩니다. “학습” 시간이 더 많이 걸리기에 학습이 필요없이 최근 지식만 반영하고자 하는 경우를 위해 “지식저장”이 준비되어 있습니다. “학습”은 지식저장과 더불어 대화이해 학습을 하기에, 학습을 했으면 다시 지식저장을 하지 않아도 됩니다.</p>
				</div>
				<h5 id="7_2menu">대화시스템 구동</h5>
				<div>														
					<p>“학습”과 “지식저장”으로 대화지식이 준비됐으면, 먼저 “구동”을 수행해서 준비한 지식을 가진 대화시스템이 구동 되도록 합니다. 구동 이후, Start 버튼으로 대화 섹션을 시작합니다. Start 이후에 다음과 같은 시스템 초기 인사말이 나오고 대화를 입력해서 테스트가 가능하십니다. 다양한 사용자 입력을 넣어서 원하는 동작이 되는지 검토가 가능합니다. 그리고, 아래 화면에 Task 이력 정보와 Entity 이력 정보, Intent 이력 정보를 Turn 별로 검사가 가능합니다. 이러한 정보를 보시면서 구축한 내용에서 대화가 잘 작동하는지 검토하고 불충분한 부분은 추가 구축해 주기 바랍니다.</p>					
					<table summary="">
						<caption class="hide"></caption>
						<colgroup>
							<col width="50%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr class="center">								
								<td><p class="center"><img src="../manualfiles/image110.png" /></p></td>
								<td><p class="center"><img src="../manualfiles/image111.png" /></p></td>
							</tr>
						</tbody>
					</table>
					<p>대화시스템이 END Task까지 대화상태가 이동하면 상기 대화섹션이 자동으로 종료됩니다. 그 전에 대화를 종료하고자 하면 “Stop”을 누르세요. 화면에 보이는 대화이력 정보로 대화시스템 동작이 잘 이해가 되시지 않으면 “DevLog”를 클릭하여 다음과 같은 자세한 로그를 볼 수 있습니다. 이 로그의 xml 태그에 대한 설명은 <a href="#" onclick="fnMove('#0append2');">[부록 2] DevLog 정보 설명</a>을 참조하기 바랍니다. 여기에서 여러분이 작성한 조건과 행동이 실제로 어떻게 동작 되었는지에 대하여 보다 자세하게 기술되어 있습니다. 각 Script 함수는 <a href="#" onclick="fnMove('#6_3menu');">대화 Script 함수</a>에서 설명한 내용을 참고하기 바랍니다. 혹시, 로그정보 출력에 있어서 XML 포맷이 맞지 않아서 XML Viewer에 로그 정보가 나타나지 않으면, “DevTest용”을 클릭하여 화면 제일 하단에 텍스트 창에 나타나는 로그 정보를 다른 편집창에서 검토해 보기 바랍니다.</p>
					<p class="center"><img src="../manualfiles/image112.png" /></p>
				</div>
				
				<!-- 8장 start -->				
				<h4 id="8menu">대화처리 Open API 사용</h4>
				<div>					
					<p>본 GenieDialog에서 개발한 도메인의 대화를 Open API를 통하여 이용할 수 있습니다. 공공 인공지능 오픈 API·DATA 서비스 포털(<a target="_blank"  href="http://aiopen.etri.re.kr">http://aiopen.etri.re.kr</a>)의 대화처리 기술에서 소개하는 대화처리 API 사용 방법에 따라 이용할 수 있습니다.</p>
					<p>대화처리 Open API에서 대화를 처리하는 방식은 다음과 같습니다.</p>
					<p class="center"><img src="../manualfiles/image113.png" /></p>
					<p>대화처리 API는 대화할 도메인을 선택하고 이전 대화를 고려한 대화를 수행하기 위해, 다음 3개의 POST 방식의 API method로 구성되어 있습니다.</p>
					<ol>
						<li>open_dialog: 대화할 도메인 이름으로 새로운 대화를 오픈합니다. 서버에서는 대화 섹션을 관리하기 위한 uuid 키 값을 발급합니다.</li>
						<li>dialog: 사용자의 입력을 분석하고 시스템 응답을 제공합니다. open_dialog method 호출 시에 발급받은 uuid 키로 대화 도메인과 이력을 유지합니다.</li>
						<li>close_dialog: 대화 섹션 종료를 제공합니다.</li>
					</ol>
					<p>보다 자세한 대화처리 Open API 사용 방법에 대해서는 상기 포털에서 참고하기 바랍니다.</p>
					<p>본 장에서는 대화처리 Open API를 사용하기 위해 주의할 점과 고려할 점을 기술합니다.</p>
				</div>
				<h5 id="8_1menu">도메인 연동 주의</h5>
				<div>														
					<p>GenieDialog에서 개발한 본인의 도메인만 대화처리 Open API를 통해 사용할 수 있습니다. GenieDialog에서 개발하여 “지식저장” 및 “학습”한 도메인 대화지식을 대화처리 Open API에서 같이 사용합니다. 따라서, GenieDialog에서 “지식저장” 및 “학습”을 수행하게 되면 대화처리 Open API 서비스와 충돌이 발생할 수 있습니다. 대화처리 Open API를 사용하지 않는 시점에 “지식저장” 및 “학습”을 수행하거나 안정화된 도메인을 대화처리 Open API를 통해 사용하기 바랍니다.</p>					
				</div>
				<h5 id="8_2menu">외부 데이터 사용하기 위한 대화모델 구축 주의점</h5>
				<div>														
					<p><a href="#" onclick="fnMove('#1_6menu');">도메인 지식제공</a>에서도 언급했듯이, 목적지향 대화시스템은 도메인에 맞는 지식 DB 및 지식 처리를 해서 시스템 응답을 수행합니다.</p>
					<p>개발한 도메인이 GenieDialog에서 정의한 Class의 Instance DB로만 대화가 가능하면 추가적인 외부 도메인 지식제공 없이 대화처리 Open API를 사용하면 됩니다. 대화처리 Open API에서 이런 도메인에 대해서는 open_dialog 시에 access_method를 internal_data 방식으로 요청합니다.</p>
					<p>반대로, 외부 Data를 가져와서 대화시스템과 연동하고자 하면, open_dialog 시에 access_method를 external_data 방식으로 요청합니다.</p>
					<p>external_data 방식은 사용자 발화를 분석한 이후에 대화시스템이 필요한 Class의 Instance Data를 추가 요청하는 것이 가장 큰 차이입니다. external_data 방식이 잘 작동하기 위해서는 대화시스템이 언제 외부 Instance DB를 요청하는지 알고 적절하게 이것을 요청할 수 있도록 대화모델링해야 합니다.</p>
					<p>사용자 발화를 대화 이해한 의도에 나타나는 Slot(의도 Slot, 요청 Slot, 인식 Slot 상관없이)이 소속된 Class에 대한 Instance Data를 요청합니다. Instance Data는 Entity 대화이력에 있는 해당 Class의 Slot 값들이 조건이 됩니다. 날씨 검색 도메인에서 “날씨 알려줘”를 request_weather_info()라고 인식하면 어떠한 외부 Data도 요청하지 않습니다. 이유는 사용자 의도에 Slot이 나타나지 않았기 때문입니다. request(Weather.info)라고 인식되도록 정의해야 Weather Class의 Slot들로 그 Data를 요청합니다. </p>
					<p>그러므로, 외부 Data를 가지고 와서 연동하고자 하면, 사용자 의도를 처리한 이후에 시스템 응답을 찾기 위한 조건과 시스템 응답 패턴에 사용하는 Slot들에 대한 정보를 가져올 수 있도록 사용자 의도를 정의해야 합니다. Entity Class 정의 시에도 이 부분을 고려해서 Class의 Slot들도 정의해야 합니다.</p>
					<p>또한 외부 Data와 연동 시에 너무 많은 외부 Data를 입력받도록 설계해서는 안됩니다. Class Instance 검색 결과의 수가 너무 큰 경우에 그 모든 값들을 출력하지 않도록 해야 합니다. 단지 검색 결과 수만 알면 되도록 구현하면 됩니다. 예를 들어, “주문하신 상품 검색 결과로 A1, A2, A3, A4 외 10,032 건이 검색되었습니다. 원하는 가격대를 말씀해 주세요.” 처럼 실제 값을 몇 개만 언급하고 나머지는 개수로 표현하도록 시스템 발화를 정의하기 바랍니다. 그리고, 외부 Data를 대화처리 Open API에 전달할 경우에도 시스템 발화에서 사용할 수 있을 정도의 실제 값과 개수를 전달하면 됩니다. 이러한 전달 방법은 대화처리 Open API에서 dialog method의 external_data access_method에서 참조할 수 있습니다.</p>					
				</div>
			</div>
		</div>
	</div>
</body>
</html>
