<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Sense 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {
			//checkbox
			$('input[type=checkbox]').picker({
				customClass: 'list_check'
			});
			//checkbox_end
			
			$('.popupck dt a').click(function(e) {
				if($(this).attr('class') == 'ck_box_on') {
					$(this).attr('class', 'ck_box_off');
					$(this).parent().siblings().hide();
				}
				else {
					$(this).attr('class', 'ck_box_on');
					$(this).parent().siblings().show();
				}
				
				fnpreventDefault(e);
			});
			
						
			$('#popupck${param.Sense}').picker('check');					
		}
				
		
		function fnCheck(obj, sensname) {			
			$('[id^=popupck]').picker("uncheck");			
			$(obj).picker('check');
						
			if (sensname != '') {
				opener.$('#chkSet').picker("uncheck");
			}
			opener.$('#SENSE_NAME').val(sensname);
			
			fnWinClose();			
		}	
	</script>
    <style>		
		.popupck {margin-bottom:25px; position:relative;}
		.popupck dt {margin-left:30px;}
		.popupck dt .picker-label {line-height:25px; padding-left:3px; font-size:16px; font-weight:bold; color:#474747;}
		.popupck dd {margin-left:55px; margin-top:5px;}
		.popupck dd .picker-label {line-height:25px; padding-left:3px; font-size:14px; color:#474747;}
		.popupck .picker-handle,
		.popupck .picker-handle {float:left !important; display:inline-block; vertical-align:middle;}
		.popupck .ck_box_on {width:20px; height:20px; display:inline-block; background: url(../images/btn_minus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
		.popupck .ck_box_off {width:20px; height:20px; display:inline-block; background: url(../images/btn_Plus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
    </style>
</head>
<body style="min-width:413px;">
	<div class="popup_function class_popup" style="width:360px !important; min-width:319px;">
		<p>Class/Slot 검색</p>
		
		<!--search box start-->
		<div class="search_box" style="display:none;">
			<div class="s_word">
				<label for="slot_search">
					<input type="text" style="width:80%;" id="txtSearch" value="search type…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" /><a href="#" title="검색" class="solt_search" onclick="fnSearch();"><span class="hide">search</span></a>
				</label>
			</div>
		</div>
		<!--search box end-->
		<div class="popupck">
			<dl>
				<dt style="height: 20px; line-height: 25px;">
					<a href="#" class="ck_box_on"></a>
					<span class="check popupck_d1">
						<label>Sense</label>
					</span>
				</dt>			
				<dd>
					<span class="check popupck_d2">
						<input type="checkbox" id="popupckPerson" name="popupckPerson" onclick="fnCheck(this, 'Person');" /><label for="popupckPerson">Person</label>
					</span>
				</dd>
				<dd>
					<span class="check popupck_d2">
						<input type="checkbox" id="popupckLocation" name="popupckLocation" onclick="fnCheck(this, 'Location');" /><label for="popupckLocation">Location</label>
					</span>
				</dd>
				<dd>
					<span class="check popupck_d2">
						<input type="checkbox" id="popupckDate" name="popupckDate" onclick="fnCheck(this, 'Date');" /><label for="popupckDate">Date</label>
					</span>
				</dd>
				<dd>
					<span class="check popupck_d2">
						<input type="checkbox" id="popupckTime" name="popupckTime" onclick="fnCheck(this, 'Time');" /><label for="popupckTime">Time</label>
					</span>
				</dd>
				<dd>
					<span class="check popupck_d2">
						<input type="checkbox" id="popupckNone" name="popupckNone" onclick="fnCheck(this, '');" /><label for="popupckNone">None</label>
					</span>
				</dd>
			</dl>
		</div>
				
		<div class="layer_box_btn" style="margin-bottom:20px;">
			<!-- <a href="#" onclick="fnApply();">적용</a> -->
			<a href="#" onclick="fnWinClose();">Cancel</a>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>