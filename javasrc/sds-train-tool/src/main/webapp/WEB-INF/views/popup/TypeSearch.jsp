<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Class/Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />	
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">	
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
	
		window.onload = function() {						
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}				
			});
			
			if ('${param.pType}' == 'T') {
				$('#popupckC${param.ClassSeq}').parent().parent().parent().hide();				
				fnGetNotUsingClass();
			}
			else {
				$('.popupck').eq(0).hide();				
			}						
			
			$('[id^=dd_]').each(function() {
				var arrid = $(this).attr('id').substr(3).split('_');				
				var str = '';
				
				str += fnAddSlotClass(arrid[0], arrid[1], arrid[0], $(this).text().trim(), '', 20);							
				//$(this).append($('#popupckC' + seqno).parents('dl').html()).find('dt').remove();
				$(this).append(str);
			});
			
			if ('${param.SubSeq}' == '') {
				if ('${param.nchk}' == 'Y')
					$('#popupckNew${param.Type}${param.Seq}').prop("checked", true);	
				else if ('${param.nchk}' == 'N')
					$('#popupckNone${param.Type}${param.Seq}').prop("checked", true);
				else
					$('#popupck${param.Type}${param.Seq}').prop("checked", true);
			}
			else {
				if('${param.pType}' == 'T')
					$('#popupck${param.SubSeq}').prop("checked", true);
			}
			
			if ('${param.Title}' != '') {
				$('#spTitle').text('${param.Title}');
			}
		}
		
		function fnAddSlotClass(typeseqno, seqno, idseqno, parentname, prevtype, leftpoint) {
			var str = '';			
			
			$.each(slotdata[typeseqno], function(key, value) {
				if (value.SLOT_TYPE == 'C') {					
					parentname += '.' + value.SLOT_NAME;
									
					//if (prevtype == 'C') {						
						var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
						str += '<div class="checkbox" style="margin-left:' + leftpoint + 'px;"><div class="check"><div class="right">';
						str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + parentname + '\', \'' + id + '\');" /><label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';
						str += '</div></div>';
						//leftpoint += 20;
					//}					
					str += fnAddSlotClass(value.TYPE_SEQ, value.SEQ_NO + '_' + seqno, idseqno + '_' + value.TYPE_SEQ, parentname, value.SLOT_TYPE, leftpoint + 10);
					
					//if (prevtype == 'C')
						str += '</div>';			
				}
				else {
					var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
					str += '<div class="check"  style="margin-left:' + leftpoint + 'px;"><div class="right">';
					str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + parentname + '.' + value.SLOT_NAME + '\', \'' + id + '\');" /><label for="popupck' + id + '">' + parentname + '.' + value.SLOT_NAME + '</label>';
					str += '</div></div>';
				}
			});
			
			return str;
		}
		
		function fnGetNotUsingClass() {
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", ${param.ClassSeq});			
			comAjax.setUrl("<c:url value='/view/getparentclass.do' />");
			comAjax.setCallback("fnGetNotUsingClassCallBack");
			comAjax.ajax();
		}
		
		function fnGetNotUsingClassCallBack(data) {
			$.each(data.list, function(key, value){
				$('#popupckC' + value.CLASS_SEQ_NO).parent().parent().parent().hide();				
			});
		}
		
		function fnCheck(obj, slottype, typeseq, slotname, subid) {			
			$('[id^=popupck]').prop("checked", false);			
			$(obj).prop("checked", true);
						
			if(slottype == 'S')
				slotname = 'Sys.' + slotname;
			else if(slottype == '') {	
				slotname = $(obj).parent().parent().siblings('dt').text().trim() + '.' + slotname;
			}
			/* else if(slottype == 'O')
				slotname = $(obj).parent().parent().siblings('dt').text().trim() + '.' + slotname; */
			
			if ('${param.pType}' == 'T') {
				opener.$('#hidslottype').val(slottype);
				opener.$('#hidtypeseq').val(typeseq);
				opener.$('#TYPE_NAME').val(slotname);
				opener.$('#hidtypesubseq').val(subid)
				if ($(obj).attr('id').indexOf('popupckNewS') > -1) {
					opener.$('#chkSet').prop("checked", true);				
				}
				else if ($(obj).attr('id').indexOf('popupckNoneS') > -1) {
					if (opener.$('#chkSet').prop('checked') == true) {
						opener.$('#chkSet').prop("checked", false);
					}
				}
			}
			/* else if ('${param.pType}' == 'P') {				
				opener.$('#hidPreceSlotSeq').val(typeseq);
				opener.$('#PRCE_SLOT_NAME').val(slotname);
				opener.$('#hidPreceSlotSubSeq').val(subid)
			} */
			else if ('${param.pType}' == 'S') {
				opener.$('#hidSenseSeq').val(typeseq);
				opener.$('#SENSE_NAME').val(slotname);
			}
			else if ('${param.pType}' == 'D') {
				opener.$('#hidTargetSeq${param.hidSeq}').val(typeseq);
				opener.$('#TARGET_NAME${param.hidSeq}').val(slotname);
				opener.$('#hidTargetSubSeq${param.hidSeq}').val(subid);
			}
			
			fnWinClose();
			/* $('#hidslottype').val(slottype);
			$('#hidtypeseq').val(typeseq);
			$('#hidslotname').val(slotname); */			
		}
		
		function fnApply() {
			opener.$('#hidslottype').val($('#hidslottype').val());
			opener.$('#hidtypeseq').val($('#hidtypeseq').val());
			opener.$('#TYPE_NAME').val($('#hidslotname').val());
			fnWinClose();
		}
				
		function fnSearch() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/opentypesearch.do' />");
			comSubmit.addParam("TYPE_NAME", ($('#txtSearch').val() == 'search type…') ? '' : $('#txtSearch').val());			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
	</script>
</head>
<body style="min-width:413px;">
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title"><span id="spTitle" style="font-size:15px;">Class/Slot</span> 설정</div>
				<div class="box box3">
					<div class="entitiesBox">
						<div class="title"><div>Sys</div><span class="open"></span></div>
						<div class="checkbox">					
					<c:forEach items="${TYPE_LIST }" var="row">			
					<c:choose>
						<c:when test="${row.SLOT_TYPE eq 'O'}">	
							<c:choose>
							<c:when test='${param.pType eq "T"}'>
								<c:if test='${row.SUB_SLOT_TYPE eq "S" && row.SET_FLAG eq "Y"}'>
									<div class="check">
										<div class="right">																	
											<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.CLASS_NAME}.${row.TYPE_NAME}', '');" />
											<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.CLASS_NAME}.${row.TYPE_NAME}</label>
										</div>								
									</div>													
								</c:if>							
							</c:when>
							<c:otherwise>
								<div class="check">
									<div <c:if test="${row.SUB_SLOT_TYPE eq 'C'}">id="dd_${row.TYPE_SEQ}_${row.SEQ_NO }"</c:if> class="right">																	
										<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.CLASS_NAME}.${row.TYPE_NAME}', '');" />
										<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.CLASS_NAME}.${row.TYPE_NAME}</label>
									</div>								
								</div>
							</c:otherwise>						
							</c:choose>
						</c:when>
						<c:when test="${row.SLOT_TYPE eq 'S'}">
							<div class="check">
								<div class="right">
									<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" />
									<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>								
								</div>								
							</div>		
						</c:when>
						<c:otherwise>							
								</div>
							</div>							
							<div class="entitiesBox">
								<div class="title">
									<div>
									<%-- <c:choose>	
										<c:when test='${param.pType eq "T"}'>
											<span id="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</span>
										</c:when>
										<c:otherwise>
											<input class="checkbox_st" type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupckC${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" />
											<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>
										</c:otherwise>
									</c:choose> --%>
										<input class="checkbox_st" type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" />
										<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>	
									</div>
									<span class="open"></span>
								</div>
								<div class="checkbox">																	
						</c:otherwise>
					</c:choose>					
					</c:forEach>					
						</div>
					</div>
					
					<c:if test='${param.pType eq "T"}'>						
						<div class="entitiesBox">
							<div class="title"><div>None</div><span class="open"></span></div>
							<div class="checkbox">					
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupckNoneS2" name="popupckNoneS2" onclick="fnCheck(this, 'S', 2, 'string', '');" />
										<label for="popupckNoneS2">Sys.string</label>								
									</div>								
								</div>
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupckNoneS1" name="popupckNoneS1" onclick="fnCheck(this, 'S', 1, 'number', '');" />
										<label for="popupckNoneS1">Sys.number</label>								
									</div>								
								</div>
							</div>
						</div>					
						<div class="entitiesBox">
							<div class="title"><div>New defined type</div><span class="open"></span></div>
							<div class="checkbox">					
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupckNewS2" name="popupckNewS2" onclick="fnCheck(this, 'S', 2, 'string', '');" />
										<label for="popupckNewS2">Sys.string</label>								
									</div>								
								</div>
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupckNewS1" name="popupckNewS1" onclick="fnCheck(this, 'S', 1, 'number', '');" />
										<label for="popupckNewS1">Sys.number</label>								
									</div>								
								</div>
							</div>
						</div>								
					</c:if>
				</div>							
			</div>
			
			<input type="hidden" id="hidslottype" />
			<input type="hidden" id="hidtypeseq" />
			<input type="hidden" id="hidslotname" />
			<div class="btnBox" style="background:none !important">
				<span>						
					<!-- <div class="ok" onclick="fnApply();">OK</div> -->
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
				</span>				
			</div>
		</div>
	</div>
	
	
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>