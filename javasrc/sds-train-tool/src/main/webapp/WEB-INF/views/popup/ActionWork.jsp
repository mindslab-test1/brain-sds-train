<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>함수생성</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">	
	var pop;
	$(function() {
		$('#btnClear').click(function(e) {
			$('#txtResult').val('');
			fnpreventDefault(e);
		});	
		
		/* $('#txtReset').click(function() {
			fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(this).attr('id') + "&Type=F", "ActionSearchpop", 430, 550, 0, 0);
		}); */
		
		$('#divActionbtn').on('click', 'div', function(e) {
			if ($(this).text() == 'Count')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=txtAs" + "&Type=FNC" + "&SubType=CO", "AgentSearchpop", 875, 473, 0, 0);
			else if ($(this).text() == 'Value')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=txtAs" + "&Type=FNC" + "&SubType=V", "AgentSearchpop", 875, 473, 0, 0);				
			else if ($(this).text() == 'PrintDate')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=txtAs" + "&Type=FNC" + "&SubType=PD", "AgentSearchpop", 875, 473, 0, 0);
			else if ($(this).text() == 'PrintTime')
				pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=txtAs" + "&Type=FNC" + "&SubType=PT", "AgentSearchpop", 875, 473, 0, 0);
			
			fnpreventDefault(e);
		});		
		
		$('[name="txtSlot"]').click(function(e) {
			fnOpenPop($(this).attr('id'), 'F', '');
			fnpreventDefault(e);
		});
		
		$('[name="txtSlotNoClass"]').click(function(e) {
			fnOpenPop($(this).attr('id'), 'FNC', '');
			fnpreventDefault(e);
		});
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			$('#txtResult').val(opener.$('#${obj}').text());
		else
			$('#txtResult').val(opener.$('#${obj}').val());			
	});
	
	$(window).bind("beforeunload", function (e){
		if (!gfn_isNull(pop)) {
			pop.close();
		}
	});
	
	function fnOpenPop(id, type, subtype) {
		if (type == 'P') 
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + id + "&Type=" + type + "&SubType=" + subtype, "AgentSearchpop", 430, 550, 0, 0);			
		else
			pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + id + "&Type=" + type + "&SubType=" + subtype, "AgentSearchpop", 430, 550, 0, 0);
	}
	
	function fnResultAdd(type) {
		var str = '';
		
		if (type == 1) {
			var Asval = $('#txtAs').val();
			if($('#txtSet').val().trim() != '' && Asval.trim() != '') {
				var reg = /(Count\(|Value\(|PrintDate\(|PrintTime\()/g;				
				if (!reg.test(Asval)) {
					Asval = '"' + Asval + '"';
				}
				str = 'Set("' + $('#txtSet').val() + '", ' + Asval + ');';
			}
		}
		else {
			if($('#txtReset').val().trim() != '') {
				str = 'Reset("' + $('#txtReset').val() + '");';
			}
		}
		
		if (str == '') {
			alert('입력값을 확인해주세요.');
			return;
		}
		
		fnEntiryClear(type);
		if ($('#txtResult').val().trim() == '')						
			$('#txtResult').val($('#txtResult').val() + str).focus();
		else
			$('#txtResult').val($('#txtResult').val() + ' ' + str).focus();
		
		fnpreventDefault(event);
	}
		
	function fnEntiryClear(type) {
		if (type == 1) {
			$('#txtSet, #txtAs').val('');
		}
		else {
			$('#txtReset').val('');
		}
		fnpreventDefault(event);
	}
	
	function fnSetParent() {
		if($('#txtResult').val().length > 5000) { 
			alert('5000자 이상 저장할 수 없습니다.');
			return false;
		}
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			opener.$('#${obj}').text($('#txtResult').val().trim());
		else
			opener.$('#${obj}').val($('#txtResult').val().trim());
		opener.bWorkChk = true;
		
		if ('${param.Type}' == 'G') {
			opener.bConWork = true;
			opener.$('#${obj}').focus();
		}
		
		fnWinClose();
	}
</script>

</head>
<body>
	<div id="popWrap" class="popWrap">
		<div class="middleW createTask fnSG">
			<div class="searchBox">
				<div class="title">
					<c:choose>
						<c:when test="${param.Type == 'S'}">Slot value define 행동</c:when>
						<c:otherwise>시스템 발화 후 행동</c:otherwise>
					</c:choose>					
				</div>
				<div class="box box_textarea"><textarea id="txtResult"></textarea><!-- <input type="text" id="txtResult" /> --><button type="button" id="btnClear">Clear</button></div>
			</div>
			<div class="searchBox">
				<div class="box box2 title_con">
					<div class="zoom_style">
						<div class="title">Set</div>
						<div class="btnBox">
							<div onclick="fnResultAdd(1);" class="btn downloadIn">Add</div>
							<div onclick="fnEntiryClear(1);" class="btn uploadIn">Clear</div>
						</div>
					</div>
					<div class="zoom_style">
						<div class="title bg_none">Set</div>
						<div class="inputBox input_box_st"><input type="text" id="txtSet" name="txtSlotNoClass" /></div>
					</div>
					<div class="zoom_style">
						<div class="title bg_none">as</div>
						<div class="inputBox input_box_st"><input type="text" id="txtAs" /></div>
					</div>
				</div>
				<div id="divActionbtn" class="btnBox_con">
					<div class="ok">Count</div>
					<div class="cancle">Value</div>
					<div class="cancle">PrintDate</div>
					<div class="cancle">PrintTime</div>
				</div>
			</div>
			<div class="searchBox">
				<div class="box box2 title_con">
					<div class="zoom_style">
						<div class="title">Reset</div>
						<div class="btnBox">
							<div class="btn downloadIn" onclick="fnResultAdd(2);">Add</div>
							<div class="btn uploadIn" onclick="fnEntiryClear(2);" >Clear</div>
						</div>
					</div>
					<div class="zoom_style">
						<div class="title bg_none">Reset</div>
						<div class="inputBox input_box_st"><input type="text" id="txtReset" name="txtSlot" /></div>
					</div>
				</div>
			</div>
			<div class="btnBox btn_style_02" style="background:none !important; margin-bottom:10px;">
				<div class="ok" onclick="fnSetParent();">Ok</div>
				<div class="cancle" onclick="fnWinClose();">Cancel</div>
			</div>
		</div>
	</div>
</body>
</html>