<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>GenieDialog 매뉴얼</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/manual.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/main-style.css'/>" />

	<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
	<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ui.all.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout.min.js'/>"></script> --%>
	<script type="text/javascript" src="<c:url value='/js/jquery-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout-latest.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">
		var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method
		var scrollpos = 0;
		var curType = 'menu';
		var backType = '';
		var backScrollPos = '';
		
		$(document).ready(function () {	
			var width = $(document).width() * 0.25;
			
			myLayout = $('body').layout({
				west__size:					width
			,	west__minSize:				360
			,	west__spacing_closed:		20
			,	west__togglerLength_closed:	130
			,	west__togglerAlign_closed:	"top"
			,	west__togglerContent_closed:"펼<BR>치<BR>기<BR>"
			,	west__togglerTip_closed:	"Open Manual"
			,	west__sliderTip:			"Slide Open Manual"			
			,	west__slideTrigger_open:	"mouseover"
			,	east__initClosed:			false
			,	north__spacing_open:	0
			,	center__maskContents:		true // IMPORTANT - enable iframe masking
			,	resizable:					false
			});
			
			$('#mainlogo').click(function() {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/manual.go' />");		
				comSubmit.submit();
				fnpreventDefault(event);
			});					
		});		
		
		function fnMove(id) {
			$mainFrame = $('#mainFrame');		
			scrollpos = $mainFrame.contents().scrollTop();				
			var offset = $mainFrame.contents().find(id).offset();
			$mainFrame.contents().scrollTop(offset.top);

			fnpreventDefault(event);
		}
		
		function fnSelect(type, id, spos) {
			if (id == undefined) {
				id = '';
			}
			
			if (spos == undefined) {
				spos = '0';
			}
			
			if (type == 'menu') {
				$('#divManual').show();
				$('#divAppendix').hide();
			}
			else {
				$('#divManual').hide();
				$('#divAppendix').show();
			}						
			document.getElementById("mainFrame").src = "<c:url value='/view/manualcontent.go' />?type=" + type + "&id=" + encodeURI(encodeURIComponent(id)) + "&spos=" + spos;
			curType = type;
			$('.ui-layout-pane-west').scrollTop(0);
		}
		
		function fnBack() {
			if (curType == backType) {
				$('#mainFrame').contents().scrollTop(backScrollPos);	
			}
			else
				fnSelect(backType, '', backScrollPos);
			
			window.parent.$('#spBack').hide();
		}
	</script>
	
	<style>
		.ui-layout-resizer { width:0px !important; }
		.ui-layout-toggler { border:none !important; background:none !important;}
	</style>
</head>
<body>
	<div class="header ui-layout-north ui-layout-pane ui-layout-pane-north">
		<div class="logo"><a id="mainlogo" href="#">GENIE<span>DIALOG</span></a></div>
		<div class="title"><span id="spNavi">Manual</span> <span id="spBack" onclick="fnBack();" style="cursor:pointer; margin-left:70px; color:#FFE400; display:none;">돌아가기</span></div>
		<div class="userLog" style="background:none;">
			<span id="splogout" class="join_st" onclick="fnSelect('menu');" style="border-left:none; font-size:16px;">매뉴얼</span>			
			<span id="spUserJoin" class="join_st" onclick="fnSelect('append');" style="font-size:16px;">샘플도메인 설명 및 기타</span>
			<span id="spUserJoin" class="join_st" style="font-size:16px;"><a target="_blank" href="../resources/GenieDialogManual.pdf">PDF 다운로드</a></span>
		</div>
	</div>
	
	<iframe id="mainFrame" name="mainFrame" class="ui-layout-center" style="width:100%; height:100%; frameborder:0; scrolling:auto;"		
		src="<c:url value='/view/manualcontent.go' />?type=menu&id=&spos=0">
	</iframe>	
	
	<div class="ui-layout-west ui-layout-pane ui-layout-pane-west" style="display:none;">
		<div id="right">
			<div class="rightW" style="right:-450px">
				<div class="body">
					<div id="content_wrap" style="width:100%; min-width:0px; margin-bottom:30px;">
						<div id="content" style="width:100%;">
							<div id="lnb" class="f_left">
								<div id="divManual">
									<h3>매뉴얼 목차</h3>
									<ul>
										<!-- 1장 start -->
										<span><a href="#" onclick="fnMove('#1menu');">개요</a></span>
										<li><a href="#" onclick="fnMove('#1_1menu');">대화시스템</a></li>
										<li><a href="#" onclick="fnMove('#1_2menu');" href="#1_2menu">대화모델과 대화처리 응용 개발 도구</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#1_2_1menu');">대화모델 정의</a></li>
												<li><a href="#" onclick="fnMove('#1_2_2menu');">Entity</a></li>
												<li><a href="#" onclick="fnMove('#1_2_3menu');">의도(Intent)</a></li>
												<li><a href="#" onclick="fnMove('#1_2_4menu');">Entity와 의도에 의한 대화모델 동작 예</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#1_3menu');">TASK GRAPH 대화모델</a></li>
										<li><a href="#" onclick="fnMove('#1_4menu');">대화이해</a></li>
										<li><a href="#" onclick="fnMove('#1_5menu');">대화관리/생성</a></li>
										<li><a href="#" onclick="fnMove('#1_6menu');">도메인 지식제공</a></li>
										
										<!-- 2장 start -->
										<span><a href="#" onclick="fnMove('#2menu');">도메인(Domain) </a></span>
										<li><a href="#" onclick="fnMove('#2_1menu');">도메인 생성(Create Domain) </a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#2_1_1menu');">챗봇 사용하기 </a></li>
												<li><a href="#" onclick="fnMove('#2_1_2menu');">Sample Domain 으로 시작하기 </a></li>
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#2_2menu');">도메인 설정</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#2_2_1menu');">대화의도 필터링</a></li>
												<li><a href="#" onclick="fnMove('#2_2_2menu');">버전 관리</a></li>
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#2_3menu');">도메인 리스트 (DOMAIN LIST)	</a></li>
										
										<!-- 3장 start -->
										<span><a href="#" onclick="fnMove('#3menu');">TASK GRAPH</a></span>
										<li><a href="#" onclick="fnMove('#3_1menu');">TASK 이력 관리</a></li>
										<li><a href="#" onclick="fnMove('#3_2menu');">TASK 생성(CREATE TASK)</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#3_2_1menu');">다른 Task 복사해오기</a></li>
												<li><a href="#" onclick="fnMove('#3_2_2menu');">새로 만들기</a></li>
												<li><a href="#" onclick="fnMove('#3_2_3menu');">Task 시작 시, 시스템 발화 정의</a></li>
												<li><a href="#" onclick="fnMove('#3_2_4menu');">Task 저장	</a></li>
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#3_3menu');">TASK GRAPH 편집	</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#3_3_1menu');">Task 연결과 이동</a></li>
												<li><a href="#" onclick="fnMove('#3_3_2menu');">Task와 에지 삭제</a></li>
												<li><a href="#" onclick="fnMove('#3_3_3menu');">Task Graph 화면 저장</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#3_4menu');">TASK PROPERTY</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#3_4_1menu');">사용자에게 Slot값 질문하기 – Slot Filling 대화 방식</a></li>
												<li><a href="#" onclick="fnMove('#3_4_2menu');">Task Goal</a></li>
												<li><a href="#" onclick="fnMove('#3_4_3menu');">Related Slots 과 Task Jump 이동</a></li>
												<li><a href="#" onclick="fnMove('#3_4_4menu');">Reset Slots</a></li>
											</ul>
										</li>
										
										<!-- 4장 start -->
										<span><a href="#" onclick="fnMove('#4menu');">ENTITIES</a></span>
										<li><a href="#" onclick="fnMove('#4_1menu');">CLASS</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#4_1_1menu');">Class 생성 (Create Class)</a></li>
												<li><a href="#" onclick="fnMove('#4_1_2menu');">Instances</a></li>
												<li><a href="#" onclick="fnMove('#4_1_3menu');">Reference Class</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#4_2menu');">SLOT</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#4_2_1menu');">Slot 생성 (Create Slot)</a></li>
												<li><a href="#" onclick="fnMove('#4_2_2menu');">Slot Type</a></li>
												<li><a href="#" onclick="fnMove('#4_2_3menu');">슬롯 단어 정의</a></li>
												<li><a href="#" onclick="fnMove('#4_2_4menu');">Slot 관리 기능</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#4_3menu');">추가적인 ENTITY 정보</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#4_3_1menu');">Class내 Slot 간의 관계</a></li>
												<li><a href="#" onclick="fnMove('#4_3_2menu');">비교를 포함한 Slot 정의</a></li>
												<li><a href="#" onclick="fnMove('#4_3_3menu');">특수 Slot 값 min/max</a></li>
												<li><a href="#" onclick="fnMove('#4_3_4menu');">Entity 대화상태 변수로 사용하기</a></li>											
											</ul>
										</li>
										
										<!-- 5장 start -->
										<span><a href="#" onclick="fnMove('#5menu');">INTENTS</a></span>
										<li><a href="#" onclick="fnMove('#5_1menu');">GLOBAL INTENTS와 LOCAL INTENTS</a></li>
										<li><a href="#" onclick="fnMove('#5_2menu');">INTENT 정의	</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#5_2_1menu');">기정의 DA Type</a></li>
												<li><a href="#" onclick="fnMove('#5_2_2menu');">의도 정의 방법</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#5_3menu');">INTENT 생성 (CREATE INTENT)</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#5_3_1menu');">사용자 발화 작성</a></li>
												<li><a href="#" onclick="fnMove('#5_3_2menu');">시스템 응답 정의 (응답 대화)</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#5_4menu');">특수 INTENT</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#5_4_1menu');">unknown Intent</a></li>
												<li><a href="#" onclick="fnMove('#5_4_2menu');">non_response Intent</a></li>
												<li><a href="#" onclick="fnMove('#5_4_3menu');">request(unknown) Intent</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#5_5menu');">시스템 발화</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#5_5_1menu');">시스템 발화 종류와 응답 순서</a></li>
												<li><a href="#" onclick="fnMove('#5_5_2menu');">시스템 발화 패턴 작성 방법</a></li>											
											</ul>
										</li>
																			
										<!-- 6장 start -->
										<span><a href="#" onclick="fnMove('#6menu');">대화이력 조건과 행동 정의</a></span>
										<li><a href="#" onclick="fnMove('#6_1menu');">대화이력 조건</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#6_1_1menu');">Intent 검사</a></li>
												<li><a href="#" onclick="fnMove('#6_1_2menu');">Entity 검사</a></li>
												<li><a href="#" onclick="fnMove('#6_1_3menu');">Task 검사</a></li>											
											</ul>
										</li>
										<li><a href="#" onclick="fnMove('#6_2menu');">행동(ACTION)</a></li>
										<li><a href="#" onclick="fnMove('#6_3menu');">대화 SCRIPT 함수</a>
											<ul class="sub">
												<li><a href="#" onclick="fnMove('#6_3_1menu');">DA Type 검사 Script 함수</a></li>
												<li><a href="#" onclick="fnMove('#6_3_2menu');">Entity 관련 Script 함수	</a></li>
												<li><a href="#" onclick="fnMove('#6_3_3menu');">Task 관련 Script 함수</a></li>
												<li><a href="#" onclick="fnMove('#6_3_4menu');">날짜/시간 Script 함수</a></li>
												<li><a href="#" onclick="fnMove('#6_3_5menu');">Set/Reset Script 함수</a></li>
											</ul>
										</li>									
										
										<!-- 7장 start -->
										<span><a href="#" onclick="fnMove('#7menu');">대화시스템 구동	</a></span>
										<li><a href="#" onclick="fnMove('#7_1menu');">학습과 지식저장	</a></li>
										<li><a href="#" onclick="fnMove('#7_2menu');">대화시스템 구동	</a></li>
										
										<!-- 8장 start -->
										<span><a href="#" onclick="fnMove('#8menu');">대화처리 OPEN API 사용</a></span>
										<li><a href="#" onclick="fnMove('#8_1menu');">도메인 연동 주의</a></li>
										<li><a href="#" onclick="fnMove('#8_2menu');">외부 데이터 사용하기 위한 대화모델 구축 주의점</a></li>
									</ul>
								</div>
								<div id="divAppendix" style="display:none;">
									<h3>샘플 도메인 설명</h3>
									<ul>
										<!-- 1장 start -->
										<span><a href="#" onclick="fnMove('#1append');">날씨 검색 도메인</a></span>
										<li><a href="#" onclick="fnMove('#1_1append');">대표 대화시나리오</a></li>
										<li><a href="#" onclick="fnMove('#1_2append');">Task Graph</a></li>
										<li><a href="#" onclick="fnMove('#1_3append');">Weather_Info Task</a></li>
										
										<!-- 2장 start -->
										<span><a href="#" onclick="fnMove('#2append');">피자 주문 도메인</a></span>
										<li><a href="#" onclick="fnMove('#2_1append');">대표 대화시나리오</a></li>
										<li><a href="#" onclick="fnMove('#2_2append');">Task Graph와 Entity</a></li>
										<li><a href="#" onclick="fnMove('#2_3append');">Greet Task</a></li>
										<li><a href="#" onclick="fnMove('#2_4append');">Get_pizza_info Task</a></li>
										<li><a href="#" onclick="fnMove('#2_5append');">Ask_drink Task</a></li>
										<li><a href="#" onclick="fnMove('#2_6append');">Get_drink_info Task</a></li>
										<li><a href="#" onclick="fnMove('#2_7append');">Get_payment_info Task</a></li>
										<li><a href="#" onclick="fnMove('#2_8append');">Bye Task</a></li>
										
										<!-- 3장 start -->
										<span><a href="#" onclick="fnMove('#3append');">차량용 내비게이션 도메인</a></span>
										<li><a href="#" onclick="fnMove('#3_1append');">대표 대화시나리오</a></li>
										<li><a href="#" onclick="fnMove('#3_2append');">Task Graph와 Entity</a></li>
										<li><a href="#" onclick="fnMove('#3_3append');">Greet Task</a></li>
										<li><a href="#" onclick="fnMove('#3_4append');">WaitOrder Task</a></li>
										<li><a href="#" onclick="fnMove('#3_5append');">EditLocation Task</a></li>
										<li><a href="#" onclick="fnMove('#3_6append');">ManageRoute Task</a></li>
										<li><a href="#" onclick="fnMove('#3_7append');">Bye Task</a></li>
										
										<!-- 4장 start -->
										<span><a href="#" onclick="fnMove('#4append');">가구 쇼핑 도메인</a></span>
										<li><a href="#" onclick="fnMove('#4_1append');">대표 대화시나리오</a></li>
										<li><a href="#" onclick="fnMove('#4_2append');">Task Graph</a></li>
										<li><a href="#" onclick="fnMove('#4_3append');">Greet Task</a></li>
										<li><a href="#" onclick="fnMove('#4_4append');">Get_product_condition Task</a></li>
										<li><a href="#" onclick="fnMove('#4_5append');">Recommend Task</a></li>
										<li><a href="#" onclick="fnMove('#4_6append');">Order Task</a></li>
										<li><a href="#" onclick="fnMove('#4_7append');">Get_product_condition Task</a></li>
										<li><a href="#" onclick="fnMove('#4_8append');">Bye Task</a></li>
										
										<!-- 5장 start -->
										<span><a href="#" onclick="fnMove('#5append');">환율 도메인</a></span>
										<li><a href="#" onclick="fnMove('#5_1append');">대표 대화시나리오</a></li>
										<li><a href="#" onclick="fnMove('#5_2append');">Task Graph</a></li>
										<li><a href="#" onclick="fnMove('#5_3append');">Greet Task</a></li>
										<li><a href="#" onclick="fnMove('#5_4append');">Get_ExchangeRate_Info Task</a></li>
										<li><a href="#" onclick="fnMove('#5_5append');">ExchangeRate_Info Task</a></li>
										<li><a href="#" onclick="fnMove('#5_6append');">ExchangeRate_Converter Task</a></li>
									</ul>
									
									<h3>DEVLOG 정보 설명</h3>
									<ul>
										<!-- 부록2 1장 start -->
										<span><a href="#" onclick="fnMove('#1append2');">대화이해 DEBUGIN XML</a></span>
										
										<!-- 부록2 2장 start -->
										<span><a href="#" onclick="fnMove('#2append2');">대화관리/생성 DEBUGING XML</a></span>
										<li><a href="#" onclick="fnMove('#2_1append2');">사용자 발화 대화관리/생성 Debuging XML – processing_user_intents</a></li>
										<li><a href="#" onclick="fnMove('#2_2append2');">시스템 발화 대화관리 Debuging XML – processing_system_response</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form id="commonForm" name="commonForm">
	</form>	
</body>
</html>