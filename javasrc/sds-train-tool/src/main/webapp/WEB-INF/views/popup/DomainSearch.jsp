<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>도메인 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">
		var bCopy = false;
		$(function () {
			if ('${param.type}' == 'v') {
				
			} else if ('${param.type}' == 'c') {
				$('#aCopy').show();
				
				$('[id^=popupck]').click(function() {
					$('[id^=popupck]').removeAttr('checked');
					this.checked = true;
				});
			} else if ('${param.type}' == 'sa') {
				$('#aSample').show();
				
				$('[id^=popupck]').click(function() {
					$('[id^=popupck]').removeAttr('checked');
					this.checked = true;
				});
			}
			else if ('${param.type}' == 's') {
				<c:forEach items="${list }" var="row">
					$('#popupck${row.PROJECT_SEQ}').prop('checked', true);
				</c:forEach>				
			}					
		});
			
		function fnCheck(obj, prjseq) {			
			var flag = 'D';
			if ($(obj).is(":checked") == true) {
				flag = 'I';
			}
						
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/insertuserdomain.do' />");
			comAjax.addParam("USER_SEQ", '${param.userno}');
			comAjax.addParam("PROJECT_SEQ", prjseq);
			comAjax.addParam("FLAG", flag);
			comAjax.ajax();					
		}
		
		function fnSample() {
			var cnt = 0;			
			var domainseq;
			var domainname;
			var domainuserseq;
			$('[id^=popupck]').each(function(e) {
				if ($(this).is(":checked") == true) {
					domainseq = $(this).val();
					domainname = $(this).next().text();
					domainuserseq = $(this).next().next().val();
					cnt ++;					
				}
			});
			if (cnt == 0) {
				alert('선택 된 도메인이 없습니다.');
			}
			else {
				domainname = domainname.substring(0, domainname.lastIndexOf(' - '));
				opener.$('#${param.userid}').next().next().val(domainuserseq);
				opener.$('#${param.userid}').next().val(domainseq);
				opener.$('#${param.userid}').val(domainname);
				fnWinClose();
			}
		}
		
		function fnCopy() {	
			if (bCopy) {
				alert('복사중 입니다.');
				return false;
			}
			bCopy = true;
			fnLoading();
		
			var arrDomain = new Array();
			$('[id^=popupck]').each(function(e) {
				if ($(this).is(":checked") == true) {
					arrDomain.push($(this).val());
				}
			});
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/copydomain.do' />");
			comAjax.setCallback("fnCopyCallBack");
			comAjax.addParam("USER_SEQ", '${param.userno}');
			comAjax.addParam("USER_ID", '${param.userid}');
			comAjax.addParam("USER_API_KEY", '${param.userapi}');
			comAjax.addParam("PROJECT_SEQ", arrDomain.toString());							
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnCopyCallBack(data) {
			fnLoading();
			bCopy = false;
			if(data.state == 'OK') {
				if (data.namelist.length == 0) {
					alert('복사 되었습니다.');
					fnWinClose();	
				}
				else {
					var prjname = '';
					for (var i = 0; i < data.namelist.length; i++) {
						if (i == 0) 
							prjname += data.namelist[i];	
						else
							prjname += ", " + data.namelist[i];
					}
					alert(prjname + ' 프로젝트는 이미 존재 합니다.');					
				}					
			}
			else {
				alert('복사 중 오류가 발생 되었습니다.');
			}
		}
	</script>
</head>
<body>
	<div id="divLoading" style="display:none; box-shadow: 3px 3px 10px #999999;position: absolute;left: 50%;top: 50%;width: 180px;height: 180px;margin-top:-90px;margin-left:-90px;min-width: 170px !important;z-index: 1001;background: url(../images/loading.gif) no-repeat center center #ffffff !important;"></div>
	<div class="popup setEncoding" style="width:100%; height:100%; margin:0; border:none; border-radius:0; overflow:none;">
    	<div class="title">도메인 리스트</div>
        <dl class="function_list">        	
        	<c:choose>
        		<c:when test="${param.type eq 'v'}">
        			<c:forEach items="${prjlist }" var="row">
		    	        <dd class="function_list_bg01 ck_box">
			            	<span class="check popupck_d1">
			                    <label>${row.PROJECT_NAME}</label>
			                </span>
			            </dd>        							                
		        	</c:forEach>
        		</c:when>
        		<c:when test="${param.type eq 'c' || param.type eq 'sa'}">
        			<c:forEach items="${prjlist }" var="row">
        				<c:if test="${row.DELETE_FLAG == 'N'}">
			    	        <dd class="function_list_bg01 ck_box">
				            	<span class="check popupck_d1">
				                    <input type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" value="${row.SEQ_NO}" class="checkbox_st"/><label for="popupck${row.SEQ_NO}">${row.PROJECT_NAME} - ${row.USER_NAME}(${row.USER_ID})</label>
				                    <input type="hidden" id="hidPrjUserSeq${row.SEQ_NO}" value="${row.USER_SEQ_NO}" />
				                </span>
				            </dd>      
			            </c:if>  							                
		        	</c:forEach>
        		</c:when>
        		<c:when test="${param.type eq 's'}">
	        		<c:forEach items="${prjlist }" var="row">
	        			<c:if test="${row.DELETE_FLAG == 'N'}">
			    	        <dd class="function_list_bg01 ck_box">
				            	<span class="check popupck_d1">
				                    <input type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" onclick="fnCheck(this, ${row.SEQ_NO});" class="checkbox_st"/><label for="popupck${row.SEQ_NO}">${row.PROJECT_NAME}</label>
				                </span>
				            </dd>
			            </c:if>        							                
		        	</c:forEach>
        		</c:when>
        	</c:choose>	        	
        </dl>
        <div class="btnBox">
			<div class="save" id="aCopy" style="display:none;" onclick="fnCopy();">Copy</div>
			<div class="save" id="aSample" style="display:none;" onclick="fnSample();">Ok</div>
			<div class="cancle" onclick="javascript:window.close();">Cancel</div>
		</div>
    </div>
    
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>