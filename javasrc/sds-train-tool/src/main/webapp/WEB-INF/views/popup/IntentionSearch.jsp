<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>의도 Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {
			if (${fn:length(list)} == 0) {
				alert('등록된 의도 Slot이 없습니다.\n등록 후 사용해 주세요.');
				fnWinClose();
			}
			var arr = '${INTENTION}'.split(' ');			
			var tempname;
			$('.check').each(function() {
				
				for (var i = 0; i < arr.length; i++) {
					if (arr[i] == $(this).find('label').text()) {
						$(this).find('input:checkbox').prop('checked', true);
					}
				}
			});												
		}				
		
		function fnOk() {				
			if (opener.$('#${param.obj}').parent().find('.modify').length > 0) {
				opener.fnUserSayEdit('${param.obj}', opener.$('#${param.obj}').parent().find('.modify').get(0));
			}
			opener.$('#${param.obj}').parent().find('.intent_title').text(fnGetIntention());			
			fnWinClose();		
		}
		
		function fnSave() {
			var intentval = fnGetIntention();
			if (intentval == "Error") {
				alert('의도 Slot과 사용자 발화에 태깅된 Slot이 중복 됩니다.');
				return;
			}
		
			if (confirm('해당 그룹의 의도를 변경 하시겠습니까?')) {
				var comAjax = new ComAjax();				
				comAjax.setUrl("<c:url value='/view/updateintentiongroup.do' />");
				comAjax.setCallback("fnSaveComplete");
				comAjax.addParam("INTENT_SEQ", '${param.IntentSeq}');				
				comAjax.addParam("SLOT_SEQ_NO", '${param.SlotSeq}');
				comAjax.addParam("OLD_INTENTION", '${INTENTION}');
				comAjax.addParam("INTENTION", intentval);
				comAjax.ajax();
			}
		}
		
		function fnSaveComplete(data) {
			if (data.STATUS == 'OK') {
				opener.fnSelectList();
				fnWinClose();
			}
			else {
				alert('그룹의도 변경 중 오류가 발생하였습니다.');	
			}			
		}
		
		function fnGetIntention() {
			var result = '';
			var $obj = opener.$('#${param.obj}');
			var bReturnChk = false;
			
			$('[id^=popupck]').each(function() {
				if ($(this).prop('checked')) {
					if ('${param.SlotSeq}' != '') {
						if ($obj.html().indexOf($(this).parent().find('[id^=hid]').val()) != -1) {
							bReturnChk = true;
						}
					}
					result += $(this).parent().find('label').text() + ' '; 
				}
			});
			if (bReturnChk) {
				return "Error";
			}
			else
				return result.trim();			
		}
	</script>    
</head>
<body>
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title">의도 Slot 선택</div>
				<div class="box box3">
					<div class="entitiesBox">						
						<div class="checkbox">
							<c:forEach items="${list }" var="row">			
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupck${row.SEQ_NO }" name="popupck${row.SEQ_NO }" />
										<label id="lbl${row.SEQ_NO }" for="popupck${row.SEQ_NO }">${row.INTENTION_NAME }</label>
										<input type="hidden" id="hid${row.SEQ_NO }" value='${row.INTENTION_SLOT }' />
									</div>								
								</div>
							</c:forEach>
						</div>
					</div>	
				</div>
			</div>
			<div class="btnBox" style="background:none !important">
				<span>
					<c:choose>
						<c:when test="${empty param.SlotSeq}">
							<div class="cancle" onclick="fnOk();">Ok</div>
						</c:when>
						<c:otherwise>
							<div class="cancle" onclick="fnSave();">Save</div>
						</c:otherwise>
					</c:choose>					
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
				</span>				
			</div>
		</div>
	</div>	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>