<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>DA Type 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">		
		window.onload = function() {
			var arr = new Array();
			Array.prototype.contains = function(elem) {
				for (var i in this) {
					if (this[i] == elem) return true;
				}
				return false;
			}
			var tempname;
			$('dd').each(function() {
				tempname = $(this).find('label').text(); 
				if (tempname.indexOf('(') > -1) {
					tempname = tempname.substring(0, tempname.indexOf('('));
				}
				
				if (arr.contains(tempname))
					$(this).hide();				
				else {
					$(this).find('label').text(tempname);
					arr.push(tempname);
				}
			});	
						
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}	
			});									
		}				
		
		function fnCheck(obj, seqno) {			
			$('[id^=popupck]').prop("checked", false);			
			$(obj).prop("checked", true);
									
			var datype = $('#lbl' + seqno).text();
			
			if (datype.indexOf('(') > -1) {
				datype = datype.substring(0, datype.indexOf('('));
			}
			
			opener.$('#${param.obj}').val(datype);			
			fnWinClose();		
		}	
	</script>    
</head>
<body>
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title">DA Type 선택</div>
				<div class="box box3">
					<div class="entitiesBox">						
						<div class="checkbox">
							<c:forEach items="${DA_LIST }" var="row">			
								<div class="check">
									<div class="right">
										<input type="checkbox" id="popupckPerson${row.SEQ_NO }" name="popupckPerson${row.SEQ_NO }" onclick="fnCheck(this, '${row.SEQ_NO }');" />
										<label id="lbl${row.SEQ_NO }" for="popupckPerson${row.SEQ_NO }">${row.INTENT_NAME }</label>																	
									</div>								
								</div>
							</c:forEach>
						</div>
					</div>	
				</div>
			</div>
			<div class="btnBox" style="background:none !important">
				<span>
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
				</span>				
			</div>
		</div>
	</div>	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>