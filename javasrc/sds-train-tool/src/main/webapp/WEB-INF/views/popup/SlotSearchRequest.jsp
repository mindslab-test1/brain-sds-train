<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Entity 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	
	<script type="text/javascript">
		var slotdata = jQuery.parseJSON('${SLOT_LIST2}');
	
		window.onload = function() {
			if ('${fn:length(SLOT_LIST)}' == '0') {
				alert('Entities의 slot이 정의되어 있지 않습니다.\nEntities에서 먼저 정의해 주세요.');
				fnWinClose();
			}
			
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}
				
				fnpreventDefault(e);
			});
			
			$('[id^=dd_]').each(function() {
				var arrid = $(this).attr('id').substr(3).split('_');				
				var str = '';
				
				str += fnAddSlotClass(arrid[0], arrid[1], arrid[0], $(this).text().trim(), '', 30);							
				//$(this).append($('#popupckC' + seqno).parents('dl').html()).find('dt').remove();
				$(this).append(str);
			});
						
			if ('${param.Type}' == 'related') {				
				if ('${param.value}' != '') {
					var arrval = '${param.value}'.split(',');
					for (var i = 0; i < arrval.length; i++) {						
						arrval[i] = arrval[i].trim();
					}
					
					Array.prototype.contains = function(elem) {
						for (var i in this) {
							if (this[i] == elem) return true;
						}
						return false;
					}
					
					$('[id^=popupck]').each(function() {
						$this = $(this);
						if (arrval.contains($this.attr('name'))) {
							$this.prop("checked", true);	
							$this.parent().append('<input type="hidden" name="SLOT_NAME" value="' + $this.attr('name') + '" /><input type="hidden" name="SEQ_NO" value="' + $this.attr('id').substring(8) + '" />')
						}
					});
				}
			}
			else if ('${param.Type}' == 'P') {	
				$('.entitiesBox .title').each(function() {
					if ($(this).text().trim() == '${param.Cname}') {
						$(this).next().find('.check').each(function() {
							if($(this).text().trim() == '${param.Cname}.${param.Sname}') {
								$(this).hide();
								return;
							}
						});
					}
				});
				
				if ('${param.Chklist}' != '') {					
					var arrlist = '${param.Chklist}'.split(',');
					var $obj;
					for (var i = 0; i < arrlist.length; i++) {
						$obj = $('input[' + "name='" + arrlist[i] + "'" + ']');	
					
						$obj.prop('checked', true);
						
						if ($obj.parent().find('[name=SEQ_NO]').length == 0) {
							$obj.prop("checked", true);			
							
							if ($obj.attr('id').indexOf('_') > -1)
								$obj.parent().append('<input type="hidden" name="SLOT_NAME" value="' + $obj.attr('name') + '" /><input type="hidden" name="SEQ_NO" value="' + $obj.attr('id').substring($obj.attr('id').lastIndexOf('_') + 1) + '" />');
							else
								$obj.parent().append('<input type="hidden" name="SLOT_NAME" value="' + $obj.attr('name') + '" /><input type="hidden" name="SEQ_NO" value="' + $obj.attr('id') + '" />');
						}
					}
				}
			}						
		}

		function fnAddSlotClass(typeseqno, seqno, idseqno, parentname, prevtype, leftpoint) {
			var str = '';			
			
			$.each(slotdata[typeseqno], function(key, value) {				
				if (value.SLOT_TYPE == 'C') {					
					parentname += '.' + value.SLOT_NAME;
									
					//if (prevtype == 'C') {						
						var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
						str += '<div class="checkbox" style="margin-left:' + leftpoint + 'px;"><div class="check"><div class="right">';
						str += '<input type="checkbox" id="popupck' + id + '" name="' + parentname + '" onclick="fnCheck(this, \'' + parentname + '\', ' + value.SEQ_NO + ');" /><label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';
						str += '</div></div>';
						//leftpoint += 20;
					//}					
						str += fnAddSlotClass(value.TYPE_SEQ, value.SEQ_NO, idseqno + '_' + value.TYPE_SEQ, parentname, value.SLOT_TYPE, leftpoint + 30);
					
					//if (prevtype == 'C')
						str += '</div>';			
				}
				else {
					var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
					str += '<div class="check"  style="margin-left:' + leftpoint + 'px;"><div class="right">';
					str += '<input type="checkbox" id="popupck' + id + '" name="' + parentname + '.' + value.SLOT_NAME + '" onclick="fnCheck(this, \'' + parentname + '.' + value.SLOT_NAME + '\', ' + value.SEQ_NO + ');" /><label for="popupck' + id + '">' + parentname + '.' + value.SLOT_NAME + '</label>';
					str += '</div></div>';
				}
			});
			
			return str;
		}
		
		function fnCheck(obj, slotname, seqno) {			
			//$('[id^=popupck]').picker("uncheck");
			if (seqno == 0) {
				opener.$('#${param.id}').val(slotname);
				fnWinClose();
			}
			
			$obj = $(obj);
			
			if ($obj.parent().find('[name=SEQ_NO]').length == 0) {
				$obj.prop("checked", true);			
				
				//slotname = $(obj).parents().siblings('dt').eq(0).text().trim() + '.' + slotname;
				$obj.parent().append('<input type="hidden" name="SLOT_NAME" value="' + slotname + '" /><input type="hidden" name="SEQ_NO" value="' + seqno + '" />');
				return;
			}
			else {
				$obj.prop("checked", false);
				$obj.parent().find('[name=SEQ_NO], [name=SLOT_NAME]').remove();
				return;
			}						
		}
		
		function fnClear() {
			$('[id^=popupck]').prop("checked", false);
			$('[name=SLOT_NAME]').remove();
			fnpreventDefault(event);
		}
		
		function fnApply() {
			var iCnt = 0;
			var slotName = '';
			var slotSeq = '';
			$('[name=SLOT_NAME]').each(function() {
				if (iCnt == 0) {
					slotName = $(this).val();
					slotSeq = $(this).next().val();
				}
				else { 
					slotName += ', ' + $(this).val();
					slotSeq += ', ' + $(this).next().val();
				}
				
				iCnt++;
			});
			
			
			if ('${param.Type}' == 'related' || '${param.Type}' == 'IR') {
				opener.$('#${param.id}').val(slotName);
				fnWinClose();
			}
			else if ('${param.Type}' == 'P') {
				opener.$('#hidPreceSlotSeq').val(slotSeq);
				opener.$('#PRCE_SLOT_NAME').val(slotName);
				opener.$('#hidPreceSlotSubSeq').val('');
				fnWinClose();
			}
			else {
				if (iCnt == 0) {
					alert('슬롯을 선택하세요.');
				}
				else {					
					opener.$('#tempid').text(slotName);
					opener.$('#tempid').next().val(slotSeq);
					opener.$('#tempid').removeAttr('id');
					opener.bWorkChk = true;
					fnWinClose();
				}
			}						
		}
		
	</script>    
</head>
<body>
	<div id="popWrap" class="popWrap" style="min-width:auto;">
		<div class="middleW createTask entitiesSearch">
			<div class="searchBox">
				<div class="title">Entities 검색</div>
				<div class="box box3">
					<c:if test="${param.Type == 'IR' }">
						<div class="entitiesBox">							
							<div class="checkbox">
								<div class="check">
									<div class="right">								
										<input type="checkbox" id="popupckO" name="unknown" onclick="fnCheck(this, 'unknown', 0);">
										<label for="popupckO">unknown</label>
									</div>
								</div>
							</div>
						</div>
					</c:if>
					<c:set var="Chk" value="" />
					<c:forEach items="${SLOT_LIST }" var="row">			
					<c:choose>
						<c:when test="${row.SLOT_TYPE eq 'O'}">	
							<div class="check">
								<div <c:if test="${row.SUB_SLOT_TYPE eq 'C'}">id="dd_${row.TYPE_SEQ}_${row.SEQ_NO }"</c:if> class="right">								
									<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="${row.CLASS_NAME}.${row.TYPE_NAME}" onclick="fnCheck(this, '${row.CLASS_NAME}.${row.TYPE_NAME}', ${row.SEQ_NO});" />
									<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.CLASS_NAME}.${row.TYPE_NAME}</label>
								</div>
								
							</div>
						</c:when>
						<c:when test="${row.SLOT_TYPE eq 'S'}">
							<div class="check">
								<div class="right">
									<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="${row.CLASS_NAME}.${row.TYPE_NAME}" onclick="fnCheck(this, '${row.TYPE_NAME}', ${row.SEQ_NO});" />
									<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>								
								</div>								
							</div>						
						</c:when>
						<c:otherwise>
						<c:if test="${Chk != ''}">
							</div>
						</div>
						</c:if>
						<c:set var="Chk" value="C" />
						<div class="entitiesBox">
							<div class="title"><div>${row.TYPE_NAME}</div><span class="open"></span></div>
							<div class="checkbox">																	
						</c:otherwise>
					</c:choose>					
					</c:forEach>
					<c:if test="${Chk != ''}">
						</div>
					</div>
					</c:if>
				</div>
			</div>
			<div class="btnBox" style="background:none !important;">
				<span>
					<div class="ok" onclick="fnApply();">Ok</div>
					<div class="cancle" onclick="fnWinClose();">Cancel</div>
					<c:if test="${param.Type == 'related'}">
						<div class="clear" onclick="fnClear();">Clear</div>
					</c:if>
				</span>				
			</div>
		</div>
	</div>
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>