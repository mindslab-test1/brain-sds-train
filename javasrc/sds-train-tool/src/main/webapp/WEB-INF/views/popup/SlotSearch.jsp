<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Entity 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript">
		var slotdata = jQuery.parseJSON('${SLOT_LIST2}');
		
		window.onload = function() {
			if ('${fn:length(SLOT_LIST)}' == '0') {
				alert('Entities의 slot이 정의되어 있지 않습니다.\nEntities에서 먼저 정의해 주세요.');
				fnWinClose();
			}
			else {
				$('.entitiesBox').each(function() {
					if ($(this).find('.check').length == 0)
						$(this).hide();
				});
			}
			
			$('.title').click(function(e) {
				if($(this).find('span').attr('class') == 'open') {
					$(this).find('span').attr('class', 'close');
					$(this).next().hide();
				}
				else {
					$(this).find('span').attr('class', 'open');
					$(this).next().show();
				}
			});
			
			$('[id^=dd_]').each(function() {
				var $this = $(this);
				if ('${param.Type}' == 'FNC')
					$this.find('[id^=popupck]').remove();
				
				var arrid = $this.attr('id').substr(3).split('_');				
				var str = '';
				
				str += fnAddSlotClass(arrid[0], arrid[1], arrid[2], arrid[0], $this.text().trim(), '', 20);							
				//$(this).append($('#popupckC' + seqno).parents('dl').html()).find('dt').remove();
				$this.append(str);
			});
			
			if ('${param.SubType}' == 'CO' || '${param.SubType}' == 'V') {
				$('#td2').show();
				$('#searchchkInstance').prop("checked", true);
			}
			else if ('${param.SubType}' == 'PD') {
				$('#td3').show();
				$('#chkpd3').prop("checked", true);
			}
			else if ('${param.SubType}' == 'PT') {
				$('#td4').show();
				$('#chkpt1_2').prop("checked", true);
				$('#chkpt2_2').prop("checked", true);
			}
			else {
				$('#td5').removeAttr('colspan');				
			}
			
			if ('${param.Type}' == 'TS') {
				$(window).bind("beforeunload", function (e){
					opener.$('#tempid').removeAttr('id');
				});
			}
		}
		
		function fnAddSlotClass(typeseqno, seqno, classseqno, idseqno, parentname, prevtype, leftpoint) {
			var str = '';			
			
			$.each(slotdata[typeseqno], function(key, value) {			
				if (value.SLOT_TYPE == 'C') {					
					parentname += '.' + value.SLOT_NAME;
									
					//if (prevtype == 'C') {						
						var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
						str += '<div class="checkbox" style="margin-left:' + leftpoint + 'px;"><div class="check"><div class="right">';
						if ('${param.Type}' != 'FNC')
							str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + parentname + '\', \'' + value.COLOR + '\', \'' + value.TYPE_NAME + '\');" /><label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';
						else
							str += '<label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';
						str += '</div></div>';
						//leftpoint += 20;
					//}					
						str += fnAddSlotClass(value.TYPE_SEQ, value.SEQ_NO, value.CLASS_SEQ_NO, idseqno + '_' + value.TYPE_SEQ, parentname, value.SLOT_TYPE, leftpoint + 10);
					
					//if (prevtype == 'C')
						str += '</div>';			
				}
				else {
					var id = 'O' + idseqno + '_' + value.SEQ_NO + '_' + seqno;
					var tempslotname = parentname + '.' + value.SLOT_NAME;
					str += '<div class="check"  style="margin-left:' + leftpoint + 'px;"><div class="right">';
					if ('${param.Type}' != 'U')
						str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + tempslotname + '\', \'' + value.COLOR + '\', \'' + value.TYPE_NAME + '\');" /><label for="popupck' + id + '">' + tempslotname + '</label>';
					else {						
						fnGetSlotColor(classseqno, tempslotname.substr(tempslotname.indexOf('.') + 1), "<c:url value='/view/getslotcolor.do' />");
						str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', \'' + classseqno + '_' + seqno + '\', \'' + tempslotname + '\', \'' + $('#hidSlotColor').val() + '\', \'' + value.TYPE_NAME + '\');" /><label for="popupck' + id + '">' + tempslotname + '</label>';
					}
											
					str += '</div></div>';
				}
			});
			
			return str;
		}			
		
		function fnCheck(obj, slottype, typeseq, slotname, color, typename) {			
			$('[id^=popupck]').prop("checked", false);			
			$(obj).prop("checked", true);
			
			/* if (slottype == 'C')
				slotname = slotname;	
			else
				slotname = $(obj).parents().siblings('dt').eq(0).text().trim() + '.' + slotname; */
			
			if ('${param.Type}' == '') {
				opener.$('#${obj}').next().val(typeseq);
				opener.$('#${obj}').parent().next().find('[name=OBJECT_REQUEST]').text(slotname);
				opener.bWorkChk = true;
			}
			
			if ('${param.Type}' == 'TS' || '${param.SubType}' == 'IS') {
				if (opener.$('#tempid').get(0).tagName == 'INPUT')
					opener.$('#tempid').val(slotname);
				else
					opener.$('#tempid').text(slotname);				
				if ('${param.SubType}' == 'IS') {
					if (opener.$('#tempid').parent().parent().find('[id^=hidSaveChk]').length == 1) {
						opener.$('#tempid').parent().parent().find('[id^=hidSaveChk]').val('Y');
					}
				}
				opener.$('#tempid').removeAttr('id');
			}
			else if ('${param.Type}' != 'U') {
				if ('${param.SubType}' == 'V') {
					var $openerObj = opener.$('#${obj}');
					if ($openerObj.val() == '')
						$openerObj.val('Value("' + slotname + '", "' + $('#hidSearch').val() + '")');										
					else 
						$openerObj.val($openerObj.val() + ' Value("' + slotname + '", "' + $('#hidSearch').val() + '")');					
				}
				else if ('${param.SubType}' == 'CO') {
					var $openerObj = opener.$('#${obj}');
					if ($openerObj.val() == '')
						opener.$('#${obj}').val('Count("' + slotname + '", "' + $('#hidSearch').val() + '")');										
					else 
						opener.$('#${obj}').val($openerObj.val() + ' Count("' + slotname + '", "' + $('#hidSearch').val() + '")');										
				}
				else if ('${param.SubType}' == 'PD') {
					opener.$('#${obj}').val('PrintDate("' + slotname + '", "' + $('#hidPd').val() + '")');
				}
				else if ('${param.SubType}' == 'PT') {
					opener.$('#${obj}').val('PrintTime("' + slotname + '", "' + $('#hidPt1').val() + '", "' + $('#hidPt2').val() + '")');
				}
				else {
					opener.$('#${obj}').val(slotname);
					if (opener.$('#hid${obj}').length > 0) {
						opener.$('#hid${obj}').val(typename);
					}
				}
			}
			else			
				opener.fnSetSlot(typeseq, slotname, color, false);
			
			fnWinClose();
		}
		
		function fnSearchChk(obj, val, type) {
			if (type == 'S') {
				$('[id^=searchchk]').prop("checked", false);
				$('#hidSearch').val(val);
			}
			else if (type == 'DP'){
				$('[id^=chkpd]').prop("checked", false);
				$('#hidPd').val(val);
			}
			else if (type == 'DT1') {
				$('[id^=chkpt1]').prop("checked", false);
				$('#hidPt1').val(val);
			}
			else if (type == 'DT2') {
				$('[id^=chkpt2]').prop("checked", false);
				$('#hidPt2').val(val);
			}
						
			$(obj).prop("checked", true);			
		}
		
	</script>   
</head>
<body>
	<div id="popWrap" class="popWrap">
		<div class="middleW createTask entitiesSearch">
			<table class="popup_table">
				<tr id="tr1">
					<td id="td1">						
						<div class="searchBox">
							<div class="title">
								<c:choose>
									<c:when test='${param.Type == "FNC"}'>Slot 검색</c:when>
									<c:otherwise>Entity 검색</c:otherwise>
								</c:choose>
							</div>
							<div class="box box3">
								<c:set var="Chk" value="" />
								<c:set var="bSlotCreate" value="" />
								<c:forEach items="${SLOT_LIST }" var="row">			
								<c:choose>
									<c:when test="${row.SLOT_TYPE eq 'O'}">	
										<c:choose>								
											<c:when test="${param.SubType eq 'PD' }">
												<c:if test="${row.SLOT_TYPE_NAME eq 'Sys.date'}">
													<c:set var="bSlotCreate" value="1" />
												</c:if>
											</c:when>
											<c:when test="${param.SubType eq 'PT' }">
												<c:if test="${row.SLOT_TYPE_NAME eq 'Sys.time'}">
													<c:set var="bSlotCreate" value="1" />
												</c:if>
											</c:when>
											<c:otherwise>
												<c:set var="bSlotCreate" value="1" />
											</c:otherwise>
										</c:choose>
										<c:if test="${bSlotCreate eq '1'}">
											<c:set var="bSlotCreate" value="" />
											<div class="check">
												<div <c:if test="${row.SUB_SLOT_TYPE eq 'C'}">id="dd_${row.TYPE_SEQ}_${row.SEQ_NO }_${row.CLASS_SEQ}"</c:if> class="right">								
													<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.CLASS_NAME}.${row.TYPE_NAME}', '${row.COLOR}', '${row.SLOT_TYPE_NAME}');" />
													<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.CLASS_NAME}.${row.TYPE_NAME}</label>
												</div>											
											</div>
										</c:if>
									</c:when>
									<c:when test="${row.SLOT_TYPE eq 'S'}">											
										<div class="check">
											<div class="right">	
												<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '${row.COLOR}', '${row.SLOT_TYPE_NAME}');" />
												<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>
											</div>
										</div>									
									</c:when>
									<c:otherwise>
									<c:if test="${Chk != ''}">
										</div>
									</div>
									</c:if>
									<c:set var="Chk" value="C" />
									<div class="entitiesBox">
										<div class="title">
											<div>
												<%-- ${row.TYPE_NAME} --%>
												<c:choose>	
												<c:when test='${param.Type eq "F" || param.Type eq "A"}'>
													<input class="checkbox_st" type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" />
													<label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>
												</c:when>
												<c:otherwise>
													${row.TYPE_NAME}										
												</c:otherwise>
												</c:choose>	
											</div>								
											<span class="open"></span>
										</div>
										<div class="checkbox">																	
									</c:otherwise>
								</c:choose>					
								</c:forEach>
								<c:if test="${Chk != ''}">
									</div>
								</div>
								</c:if>
							</div>
						</div>
					</td>
					<td id="td2" style="display:none;">				
						<input type="hidden" id="hidSearch" value="system_response" />
						<div class="searchBox ml20">
							<div class="box box3">
								<div class="entitiesBox">
									<div>검색 위치</div>
									<div class="checkbox">
										<div class="check">
											<div class="right"><input type="checkbox" id="searchchkUser" name="searchchkUser" onclick="fnSearchChk(this, 'user_history', 'S');" /><label for="searchchkUser">사용자 발화 내용</label></div>											
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="searchchkInstance" name="searchchkInstance" onclick="fnSearchChk(this, 'system_response', 'S');" /><label for="searchchkInstance">시스템 응답 가능 내용</label></div>											
										</div>
										<!-- <div class="check">
											<div class="right"><input type="checkbox" id="searchchkUser" name="searchchkUser" onclick="fnSearchChk(this, '사용자 발화 이력에서', 'S');" /><label for="searchchkUser">사용자 발화 이력에서</label></div>											
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="searchchkInstance" name="searchchkInstance" onclick="fnSearchChk(this, 'Instance에서', 'S');" /><label for="searchchkInstance">Instance에서</label></div>											
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="searchchkAll" name="searchchkAll" onclick="fnSearchChk(this, '둘 다에서', 'S');" /><label for="searchchkAll">둘 다에서</label></div>											
										</div> -->
									</div>
								</div>								
							</div>						
						</div>
					</td>
					<td id="td3" style="display:none;">				
						<input type="hidden" id="hidPd" value="오늘/내일 형식" />
						<div class="searchBox ml20">
							<div class="box box3">
								<div class="entitiesBox">
									<div>날짜 출력 형식</div>
									<div class="checkbox">
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpd1" name="chkpd1" onclick="fnSearchChk(this, '년월일 형식', 'DP');" /><label for="chkpd1">년월일 형식</label></div>
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpd2" name="chkpd2" onclick="fnSearchChk(this, '년월일(요일) 형식', 'DP');" /><label for="chkpd2">년월일(요일) 형식</label></div>
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpd3" name="chkpd3" onclick="fnSearchChk(this, '오늘/내일 형식', 'DP');" /><label for="chkpd3">오늘/내일 형식</label></div>
										</div>
									</div>
								</div>								
							</div>						
						</div>
					</td>
					<td id="td4" style="display:none;">				
						<input type="hidden" id="hidPt1" value="오전/오후 표시" />
						<input type="hidden" id="hidPt2" value="초 비포함" />
						<div class="searchBox ml20">
							<div class="box box3">
								<div class="entitiesBox">
									<div>시간출력형식</div>
									<div class="checkbox">
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpt1_1" name="chkpt1_1" onclick="fnSearchChk(this, '24시간 표시', 'DT1');" /><label for="chkpt1_1">24시간 표시</label></div>
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpt1_2" name="chkpt1_2" onclick="fnSearchChk(this, '오전/오후 표시', 'DT1');" /><label for="chkpt1_2">오전/오후 표시</label></div>
										</div>
									</div>
								</div>	
								
								<div class="entitiesBox">
									<div>초 출력 포함 여부</div>
									<div class="checkbox">
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpt2_1" name="chkpt2_1" onclick="fnSearchChk(this, '초 포함', 'DT2');" /><label for="chkpt2_1">초 포함</label></div>
										</div>
										<div class="check">
											<div class="right"><input type="checkbox" id="chkpt2_2" name="chkpt2_2" onclick="fnSearchChk(this, '초 비포함', 'DT2');" /><label for=chkpt2_2>초 비포함</label></div>
										</div>
									</div>
								</div>								
							</div>						
						</div>
					</td>
				</tr>
				<tr>
					<td id="td5" colspan="2" class="entitiesSearch">
						<div class="btnBox">
							<span>
								<div class="cancle" onclick="fnWinClose();">Cancel</div>
							</span>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<input type="hidden" id="hidSlotColor" />
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>