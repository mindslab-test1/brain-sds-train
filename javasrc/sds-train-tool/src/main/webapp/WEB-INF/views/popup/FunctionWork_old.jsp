<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>함수생성</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">
	var pop;
	$(function() {
		$('#txtSearch').keypress(function(e) {
			if(e.keyCode==13)
				fnOpenPop();
		});
		
		$('#divBtnlist li').click(function(e) {
			if($(this).text() == 'space')
				$('#txtResult').val($('#txtResult').val() + ' ');
			else if($(this).text() == 'delete')
				$('#txtResult').val('');
			else
				$('#txtResult').val($('#txtResult').val() + $(this).text() + ' ');
			fnpreventDefault(e);	
		});
		
		$('#addfn').click(function(e) {
			if(fnValidation())
				fnSetAdd();
			fnpreventDefault(e);
		});
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			$('#txtResult').val(opener.$('#${obj}').text());
		else
			$('#txtResult').val(opener.$('#${obj}').val());
	});
	
	$(window).bind("beforeunload", function (e){
		if (!gfn_isNull(pop)) {
			pop.close();
		}
	});
	
	function fnValidation() {
		if($('#txtSearch').val() == '') {
			alert('함수명을 입력하세요.');
			return false;			
		}
		
		var bcheck = true;		
		$('[name=popup_function_text]').each(function(e) {
			if($(this).css('display') != 'none') {
				if($(this).val() == '') {
					alert('파라미터 값을 입력하세요.');
					$(this).focus();
					bcheck = false;
					return false;
				}
			}
		});
		
		return bcheck;
	}
	
	function fnSetAdd() {
 		var addcomma = false;
 		var $txtResult = $('#txtResult');
 		if($txtResult.val().trim() == 'true' || $txtResult.val().trim() == 'false') 
 			$txtResult.val(''); 
		var value = $('#txtSearch').val() + '(';
		if ($('#ddlTalker').css('display') != 'none') {
			addcomma = true;
			value += '"' + $('#ddlTalker').val() + '"';
		}
		$('[name=popup_function_text]').each(function(e) {
			if($(this).css('display') != 'none') {
				if(addcomma) {
					 value += ', ';					
				} else
					addcomma = true;
					
				 value += '"' + $(this).val() + '"';
			}
		});
		value += ') ';
		$txtResult.val($txtResult.val() + value);
	}
	
	function fnOpenPop() {
		if($('#txtSearch').val() == 'search functions…')
			$('#txtSearch').val('');
		else
			$('#txtSearch').val($('#txtSearch').val().replace(/\'/, '\\\''));
		$('#ddlTalker').val('system');
		$('[name=popup_function_text]').val('').unbind('click');
		pop = fnWinPop("<c:url value='/view/openfunctionsearch.do' />" + '?FUNCTION_NAME=' + $("#txtSearch").val(), "Searchpop", 581, 388, 50, 50);
		fnpreventDefault(event);
	}
	
	function fnPramSet(cnt, des) {				
		$txt01 = $('#popup_function_text01');
		$txt02 = $('#popup_function_text02');
		$txt03 = $('#popup_function_text03');			
		
		$('#ddlTalker, [name=popup_function_text]').hide();
		
		des = des.replace(/[\(\)\"]/g, '');
		var arrdes = des.split(',');
		
		if(cnt == 1) {
			$txt01.show();
			$txt01.val(arrdes[0]);
		} else if(cnt == 2) {
			$txt01.show();
			$txt02.show();
			$txt01.val(arrdes[0]);
			$txt02.val(arrdes[1]);
		} else if(cnt == 3) {
			$txt01.show();
			$txt02.show();
			$txt03.show();
			$txt01.val(arrdes[0]);
			$txt02.val(arrdes[1]);
			$txt03.val(arrdes[2]);
		}
		
		$('[name=popup_function_text]').each(function(e) {
			var $this = $(this);
			if ($this.val() == '슬롯이름' || $this.val() == '클래스이름') {
				$this.click(function() {
					pop = fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $this.attr('id') + "&Type=F", "SlotSearchpop", 430, 550, 0, 0);
				});
			}
			else if ($this.val().trim() == 'DA type') {
				$this.click(function() {
					pop = fnWinPop("<c:url value='/view/opendatypesearch.do' />" + "?obj=" + $this.attr('id') + '&type=' + $('#ddlTalker').val(), "DASearchpop", 430, 550, 0, 0);
				});
			}
			else if ($this.val().trim() == '검사할시점') {		
				$this.bind({
					click: function() {
						$(this).val('');
					},
					keyup: function() {
						$this.val($this.val().replace(/[^0-9]/gi, ""));
						if ($this.val() == '0') {
							alert('0보다 큰 숫자만 입력 가능합니다.');
							$this.val('');
						}
					}
				});
			}
			else if ($this.val().trim() == '발화자') {
				$this.hide();
				$('#ddlTalker').show();
			}			
		});
	}
	
	function fnSetParent() {
		if($('#txtResult').val() == '') { 
			alert('생성된 값이 없습니다.')
			return false;
		}
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			opener.$('#${obj}').text($('#txtResult').val().trim());
		else
			opener.$('#${obj}').val($('#txtResult').val().trim());
		opener.bWorkChk = true;
		
		if ('${param.Type}' == 'G') {
			opener.bConWork = true;
			opener.$('#${obj}').focus();
		}
		
		fnWinClose();
	}
</script>

</head>
<body style="min-width:0px; overflow:auto;">
	<div id="popWrap" class="popWrap">
		<div class="middleW createTask fnSG">
			<div class="searchBox">
				<div class="title">함수 생성</div>
				<div class="box"><input type="text" id="txtSearch" placeholder="search functions…" />
				<button type="button" onclick="fnOpenPop()"><img src="../images/icon_searchW.png" alt="search" title="Search"></button></div>
				<div class="box box2">
					<select id="ddlTalker" style="display:none;" class="popup_function_select">
		        		<option value="system">system</option>
		        		<option value="user">user</option>
		        	</select>
        			<input type="text" id="popup_function_text01" name="popup_function_text" />
        			<input type="text" id="popup_function_text02" name="popup_function_text" />
        			<input type="text" id="popup_function_text03" name="popup_function_text" />
        			<button id="addfn" type="button">적용</button>
       			</div>
				<div id="divBtnlist" class="box box2">
					<ul>
						<li><div>==</div></li>
						<li><div>~=</div></li>
						<li><div>&lt;</div></li>
						<li><div>&gt;</div></li>
						<li><div>&lt;=</div></li>
						<li><div>&gt;=</div></li>
						<li><div>true</div></li>
						<li><div>false</div></li>
						<li><div>and</div></li>
						<li><div>or</div></li>
						<li><div>(</div></li>
						<li><div>)</div></li>
						<li><div>space</div></li>
					</ul>
				</div>
				<div class="box box2"><textarea id="txtResult"></textarea></div>
			</div>
			<div class="btnBox">
				<div onclick="fnSetParent();" class="ok">Ok</div>
				<div onclick="fnWinClose();" class="cancle">Cancel</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- <div class="popup_function">
    	<p>함수 생성</p>
        
        search box start
        <div class="search_box">
            <div class="s_word">
                <label for="slot_search">
                    <input type="text" id="txtSearch" value="search functions…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" />
                    <a onclick="fnOpenPop()" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>
                </label>
            </div>
        </div>
        search box end
        <div class="popup_function_text">
        	<select id="ddlTalker" style="display:none;" class="popup_function_select">
        		<option value="system">system</option>
        		<option value="user">user</option>
        	</select>
        	<label for="popup_function_text01">
                    <input type="text" id="popup_function_text01" name="popup_function_text" />
            </label>
        	<label for="popup_function_text02">
                    <input type="text" id="popup_function_text02" name="popup_function_text" />
            </label>
        	<label for="popup_function_text03">
                    <input type="text" id="popup_function_text03" name="popup_function_text" />
            </label>
            <a id="addfn" class="popup_function_add" href="#">적용</a>
        </div>
        <div class="popup_function_btn">        	
        	<a href="#">==</a>
        	<a href="#">~=</a>
        	<a href="#">&lt;</a>
        	<a href="#">&gt;</a>
        	<a href="#">&lt;=</a>
        	<a href="#">&gt;=</a>
        	<a href="#">true</a>
            <a href="#">false</a>        	
        	<a href="#">and</a>
            <a href="#">or</a>
            <a href="#">(</a>
            <a href="#">)</a>                                      
            <a href="#">space</a>
            <a href="#">delete</a>
        </div>
        <div class="popup_function_result">
            <label for="popup_function_text04">
                    <input type="text" id="txtResult" />
                    <textarea type="text" id="txtResult"></textarea>
            </label>
        </div>
        <div class="layer_box_btn">
            <a onclick="fnSetParent();" href="#">OK</a>
            <a onclick="fnWinClose();" href="#">Cancel</a>
        </div>        
    </div> -->
</body>
</html>