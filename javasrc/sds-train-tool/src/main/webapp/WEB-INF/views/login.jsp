<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js?ver=9'/>"></script>
	<script type="text/javascript">
		$(function () {
			if (window.frameElement != undefined) {
				window.parent.fnlogout();
			}
			
			$('#txtid').val(getCookie("UserId"));
			if ($('#txtid').val().trim() != '') {
				$("#chkidsave").prop("checked", true);	
			}
			
			$('#txtPwd').bind('keydown', function(e) {
				if(e.keyCode == 13) {
					fnLogin();
				}
			});					
		});
		
		function fnLogin() {
			if($("#txtid").val() != '' && $("#txtPwd").val() != '') {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/logincheck.go' />");
				comAjax.setCallback("fnLoginCallBack");
				comAjax.addParam("USER_ID",$("#txtid").val());
				comAjax.addParam("USER_PWD", $("#txtPwd").val());				
				comAjax.ajax();
				fnpreventDefault(event);							
			} else {
				fnAlert();
				fnpreventDefault(event);
			}				
		}
		
		function fnLoginCallBack(data) {
			if (data.STATUS == 'OK') {
				if ($('#chkidsave').is(":checked") == true)
					setCookie("UserId", $("#txtid").val(), 30);
				else 
					deleteCookie("UserId");
				
				setCookie("USER_TYPE", data.userinfo.USER_TYPE);
				setCookie("USER_SEQ", data.userinfo.SEQ_NO);
				setCookie("USER_ID", data.userinfo.USER_ID);
				setCookie("USER_NAME", data.userinfo.USER_NAME);
				setCookie("API_KEY", data.userinfo.API_KEY);
				setCookie("UNIQID", uniqid());
				
				if (data.userinfo.API_KEY == '')
					deleteCookie('ProjectApiKey');
				else
					setCookie("ProjectApiKey", data.userinfo.API_KEY);
				fnMain();
			}
			else
				fnAlert();
		} 
		
		function fnAlert() {
			alert('아이디, 패스워드를 확인 하세요.');
		}
		
		function fnJoin(type) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/join.go' />");
			comSubmit.addParam("TYPE", type);
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<div id="wrap" class="login">
		<div>
			<div class="logo"><a href="#" onclick="fnMain();">GENIE<span>DIALOG</span></a></div>
			<div class="input id"><input type="text" id="txtid" name="txtid" class="tf_login" maxlength="50" tabindex="2" placeholder="ID" /></div>
			<div class="input pw"><input type="password" id="txtPwd" name="txtPwd" class="tf_login tf_pw" maxlength="30" tabindex="3" placeholder="password" /></div>
			<div class="remember">
				<div><input type="checkbox" id="chkidsave"/><label for="chkidsave"></label></div>
				<div><label for="reId">아이디 기억하기</label></div>
			</div>
			<button type="button" onclick="fnLogin();">LOGIN</button>
			<div class="find_info">
				<a onclick="fnJoin('I');">아이디 찾기</a>
				<span>|</span>
				<a onclick="fnJoin('P');">비밀번호 찾기</a>
				<span>|</span>
				<a onclick="fnJoin('J');">사용자 등록</a>
			</div>
		</div>
	</div>

	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>
