<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>		
	<title>대화모델링구축도구</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/main-style.css'/>" />
	<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
	<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ui.all.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout.min.js'/>"></script> --%>
	<script type="text/javascript" src="<c:url value='/js/jquery-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery-ui-latest.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.layout-latest.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>	
	
	<script type="text/javascript">
		var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method
		var enginedata = '';
		var idxcal = 0;
		var enginestate = false;
		var engineEndchk = false;
		var leanInterval;
		//var enginedata = JSON.parse('{"SYS_UTTER":"어떤 피자 주문할꺼냐고","TURN":"9","DIALOG_END":"FALSE","LOG_HIST":[{"USER":{"INPUT":"","INTENTION":[]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할래?","PATTERN":"어떤 피자 주문할래?","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"흠","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"생각좀하자","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"ㅡㅡ","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]},{"USER":{"INPUT":"야","INTENTION":[{"DA":"unknown()","CONFIDENCE":"1"}]},"SYSTEM":{"OUTPUT":"어떤 피자 주문할꺼냐고","PATTERN":"어떤 피자 주문할꺼냐고","PATTERN_ID":"NULL"},"TASK_HIST":{"DONE_TASKS":[],"CURRENT_TASKS":{"TASK_NAME":"Get_pizza_info","START_TURN":"1"}},"ENTITY_HIST":[]}]}');
		$(document).ready(function () {	
			var width = $(document).width() * 0.25;
			
			myLayout = $('body').layout({
				east__size:					width
			,	east__min_size:				300
			,	east__spacing_closed:		20
			,	east__togglerLength_closed:	130
			,	east__togglerAlign_closed:	"top"
			,	east__togglerContent_closed:"D<BR>i<BR>a<BR>l<BR>o<BR>g<BR>"
			,	east__togglerTip_closed:	"Open Dialog"
			,	east__sliderTip:			"Slide Open Dialog"
			,	east__slideTrigger_open:	"mouseover"
			,	center__maskContents:		true // IMPORTANT - enable iframe masking
			,	resizable:					true
			});
			
			//기본 세팅			
			<%-- var iframe2 = document.getElementById("iframe2");
			if (!gfn_isNull(iframe2))
				iframe2.src = encodeURI('<%= session.getServletContext().getInitParameter("engineUrl")%>/api.start.php?prjname=' + getCookie("ProjectName") + '&port=<%= session.getAttribute("ServerPort")%>&type=dynamic&uniqid=<%= session.getAttribute("UNIQID")%>' + '&engine=' + getCookie("ProjectEngine") + '&dic=' + getCookie("ProjectDic")); --%> 	
							
			$('#areInput').keypress(function(e) {
				if(e.keyCode == 13) {
					if($(this).val().trim() != '') {
						$('#divResult').append('user: ' + $(this).val() + '<br />');
						fnInput('I');						
					}
					
					fnpreventDefault(e);
				}
					
			});
			
			if (getCookie("DialProjectName") == '')
				$('#spDomain').text(getCookie("ProjectName"));	
			else
				$('#spDomain').text(getCookie("DialProjectName"));
			
			if (getCookie("LeanProjectName") == '')
				$('#spLeanDomain').text(getCookie("ProjectName"));	
			else
				$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			if ('<%= session.getServletContext().getInitParameter("ShowV2")%>' != '35000' && getCookie("USER_TYPE") != 'OA') {
				$('#aTutorMap').hide();
			}
		});
			
		function fnLearning() {
			$('#divlean').show();
			$('#divleanbox').text('학습 중...');
			$('#divStart').hide();
			$('#divDialtitle').text('Learning Log..');				
			setCookie("LeanProjectName", getCookie("ProjectName"));
			$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearning.do' />");
			comAjax.setCallback("fnLearningCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnLearningCallBack(data) {				
			if (data.STATUS == 'OK') {
				leanInterval = setInterval(fnLearningCompliteChk, 1000);	
			}
			else
				alert('학습 중 오류가 발생하였습니다.');						
		}
		
		function fnLearningCompliteChk() {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearningchk.do' />");
			comAjax.setCallback("fnLearningCompliteChkCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
		}

		function fnLearningCompliteChkCallBack(data) {
			$('#divleanbox').html(data.LOG);
			if (data.LOG.indexOf('FINISHED') > -1) {
				clearInterval(leanInterval);
			}			
		}
		
		function fnStart() {
			$('#divStart').show();
			$('#divlean').hide();
			$('#divDialtitle').text('Dialog');
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogstart.do' />");			
			comAjax.setCallback("fnStartCallBack");
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			
			/* engineEndchk = true;
			setTimeout(function() {
				fnErrorChk();				
			}, 5000); */
			
			//var iframe2 = document.getElementById("iframe2");
			//iframe2.src = encodeURI('<%= session.getServletContext().getInitParameter("engineUrl")%>/api.start.php?prjname=' + getCookie("ProjectName") + '&port=<%= session.getAttribute("ServerPort")%>&type=dynamic&uniqid=<%= session.getAttribute("UNIQID")%>' + '&engine=' + getCookie("ProjectEngine") + '&dic=' + getCookie("ProjectDic"));
			$('#divLog').hide();			
			fnpreventDefault(event);			
		}
		
		function fnStartCallBack(data) {
			engineEndchk = false;
			if (data.STATUS == 'OK') {
				$('#divResult').html('');
				$('#areInput').removeAttr('disabled');
				$('#divLog').hide();				
				setCookie("DialProjectName", getCookie("ProjectName"));
				$('#spDomain').text(getCookie("DialProjectName"));
				alert('엔진이 구동 되었습니다.');
			}
			else
				alert('엔진 구동 중 오류가 발생하였습니다.');
		}
		
		function fnInput(type) {			
			engineEndchk = true;
			/* setTimeout(function() {
				fnErrorChk();
			}, 5000); */
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialoginput.do' />");
			comAjax.setCallback("fnInputCallBack");
			if (type != 'S') { 
				comAjax.addParam("MSG", $('#areInput').val().replace(/\s/g, '|@').replace(/\\/g, '\\\\').replace(/\"/g, '\\"'));
				$('#areInput').val('');
				$('#areInput').attr('disabled', 'disabled');
			}
			else {
				$('#divResult').html('');
			}
			
			comAjax.addParam("TYPE", type);
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			
			fnpreventDefault(event);
		}
		
		function fnInputCallBack(data) {
			engineEndchk = false;
			if (data.LOG != '') {
				enginestate = true;
				try {
					enginedata = JSON.parse(data.LOG);
					$('#areInput').removeAttr('disabled').focus();
					if($('#divLog').css('display') == 'block')
						fnLog();
					if($('#divDevLog').css('display') == 'block')
						fnDevLog();
					if (enginedata.DIALOG_END == 'TRUE') {					
						if (enginedata.SYS_UTTER != '_EMPTY_STRING_')
							$('#divResult').append('system: ' + enginedata.SYS_UTTER + '<br />').scrollTop($("#divResult")[0].scrollHeight);
						fnStop('M');
						return;
					}
					else
						$('#divResult').append('system: ' + enginedata.SYS_UTTER + '<br />').scrollTop($("#divResult")[0].scrollHeight);					
				} catch (e) {
					// TODO: handle exception					
					alert('구동 중에 문제가 발생하였습니다. 구축한 대화 모델을 한 번 더 확인해주세요.');
					fnEngineStop();
					return;
				}											
			}			
			else
				alert('엔진을 구동시켜주세요.');			
		}
		
		function fnStop(type) {
			if (!enginestate) {
				alert('종료시킬 대화가 없습니다.');
				return;
			}
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogstop.do' />");
			comAjax.setCallback("fnStopCallBack");			
			comAjax.addParam("TYPE", type);
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(false);
			comAjax.ajax();
			
			if (type == 'M')
				fnpreventDefault(event);
		}
		
		function fnStopCallBack(data) {
			//var stopdata = JSON.parse(data.LOG);
			if (data.LOG == 'OK') {						
				if (data.TYPE == 'M')
					$('#divResult').append('-대화가 종료되었습니다.-<br />');									
				else
					$('#divResult').html('');
				$('#areInput').attr('disabled', 'disabled');
				enginestate = false;
			}
			else {
				alert('엔진 중지 중 문제가 발생하였습니다.');
			}
		}
		
		function fnErrorChk() {
			if (engineEndchk) {
				engineEndchk = false;
				alert('구동 중에 문제가 발생하였습니다. 구축한 대화 모델을 한 번 더 확인해주세요.');
				fnEngineStop();
				$('#divResult').append('-대화가 종료되었습니다.-<br />');
			}	
		}
		
		function fnEngineStop() {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/enginestop.do' />");
			comAjax.setCallback("");
			comAjax.addParam("PORT", '<%= session.getAttribute("ServerPort")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
		}
		
		function fnClear() {
			$('#divResult').html('');
			if ($('#areInput').attr('disabled') == 'disabled') {
				enginedata = '';
				$('#txtDevLog').val('');
				$('#divLog, #divDevLog').hide();	
			}			
			fnpreventDefault(event);
		}
		
		function fnLog() {
			if (!gfn_isNull(enginedata)) {				
				var idx = Math.ceil(enginedata.TURN/2) - 1 + idxcal;
											
				if (idx == 0) {		
					idx = 0;
					idxcal = (enginedata.LOG_HIST.length + 1) - (enginedata.LOG_HIST.length * 2);
					$('#spTurn').text('1');										
				}
				else {
					if (enginedata.LOG_HIST[idx] == undefined) {
						idxcal --;
						return;
					}
					var titleidx = idx * 2 + 1;
					$('#spTurn').text((titleidx - 1).toString() + ', ' + titleidx.toString()); 	
				}
				$('#hidTurn').val(idx);
				
				var str = '';				
				for (var i = 0; i < enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS.length; i++) {
					if (i == 0)
						str += enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].END_TURN + ')'; 
					else	
						str += ', ' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.DONE_TASKS[i].END_TURN + ')';										 
				}
				$('#tdPrevTask').text(str);
				str = '';
				str = enginedata.LOG_HIST[idx].TASK_HIST.CURRENT_TASKS.TASK_NAME + '(' + enginedata.LOG_HIST[idx].TASK_HIST.CURRENT_TASKS.START_TURN + ')';
				$('#tdCurrentTask').text(str);				
				
				str = '';
				for (var i = 0; i < enginedata.LOG_HIST[idx].ENTITY_HIST.length; i++) {
					str += '<tr>';
					str += '<th>' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].SLOT_NAME + '&nbsp;=&nbsp;</th>';
					str += '<td>' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].SLOT_VAL + '(' + enginedata.LOG_HIST[idx].ENTITY_HIST[i].CHANGED_TURN + ')' + '</td>';
					str += '</tr>';
				}
				$('#tbEntities').html(str);			
							
				str = '';
				str += '<tr><th colspan="2">User</th></tr>';
				if (enginedata.LOG_HIST[idx].USER.INTENTION.length == 0) 
					str += '<tr><th class="th_bg02">utterance<span>&nbsp;:&nbsp;</span></th>';
				else
						str += '<tr><th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>';				
				str += '<td>' + enginedata.LOG_HIST[idx].USER.INPUT + '</td></tr>';
				
				var classnm = 'th_bg01';
				for (var i = 0; i < enginedata.LOG_HIST[idx].USER.INTENTION.length; i++) {
					if ((i + 1) == enginedata.LOG_HIST[idx].USER.INTENTION.length)
						classnm = 'th_bg02';
					
					str += '<tr><th class="' + classnm + '">DA (' + (i + 1).toString() + ')<span>&nbsp;:&nbsp;</span></th>';
					str += '<td>' + enginedata.LOG_HIST[idx].USER.INTENTION[i].DA + ' (' + enginedata.LOG_HIST[idx].USER.INTENTION[i].CONFIDENCE + ')</td></tr>';	
				}
				
				str += '<tr><th colspan="2">System</th></tr>';
				str += '<tr><th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.OUTPUT + '</td></tr>';
				str += '<tr><th class="th_bg01">pattern<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.PATTERN.replace(/</g, '&lt;').replace(/>/g, '&gt;') + '</td></tr>';
				str += '<tr><th class="th_bg02">pattern_id<span>&nbsp;:&nbsp;</span></th>';
				str += '<td>' + enginedata.LOG_HIST[idx].SYSTEM.PATTERN_ID + '</td></tr>';
				$('#tbIntents').html(str);
				
				$('#divLog').show();
				$('#divDevLog').hide();
			}
			else {
				alert('엔진을 시작해주세요.');				
			}
			//fnWinPop("<c:url value='/view/logview.do' />", "LogPop", 713, 626, 0, 0);	
		}
		
		function fnLogPrevAndNext(type) {
			if (type == 'P') {
				if(Math.ceil(enginedata.TURN/2) - 1 + idxcal != 0)
					idxcal --;
			}
			else 
				idxcal ++;
			fnLog();
		}
		
		function fnDataDownload() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/common/downloadzipFile.do' />");
			comSubmit.addParam("PROJECT_NAME", getCookie("ProjectName"));		
			comSubmit.submit();
			fnpreventDefault(event);	
		}
		
		function fnLeanFile() {
			$('#divlean').show();
			$('#divleanbox').text('지식 저장 중...');
			$('#divStart').hide();
			setCookie("LeanProjectName", getCookie("ProjectName"));
			$('#spLeanDomain').text(getCookie("LeanProjectName"));
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/dialogslearnfilesave.do' />");
			comAjax.setCallback("fnLeanFileCallBack");
			comAjax.addParam("UNIQID", '<%= session.getAttribute("UNIQID")%>');
			comAjax.setAsync(true);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnLeanFileCallBack(data) {				
			if (data.STATUS == 'OK') {
				$('#divleanbox').text('지식 저장 완료');					
			}
			else
				alert('지식 저장 중 오류가 발생하였습니다.');						
		}
		
		function fnDevLog() {
			$('#divLog').hide();
			if (!gfn_isNull(enginedata)) {
				$('#divDevLog').show();
				$('#txtDevLog').val(enginedata.DEV_LOG).scrollTop($("#txtDevLog")[0].scrollHeight);
			}
		}
		
		function fnTutor() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/tutordomainlist.do' />");					
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnRecursiveReplace(str, srep, erep, idx) {			
			var s_idx = str.indexOf(srep, idx);
			var e_idx = str.indexOf(erep, s_idx);
			var reg;
			if(s_idx > -1 && e_idx > -1) {
				var strtemp = str.substring(s_idx + srep.length, e_idx);
				if (strtemp.indexOf('<') > -1) {
					reg = new RegExp(srep + strtemp + erep,'g');
					str = str.replace(reg, srep + strtemp.replace(/</, '&lt;').replace(/>/, '&gt;') + erep);	
				}			
				fnRecursiveReplace(str, srep, erep, e_idx);
			}	
			else
				return str;
		}
		
		/* 
		function fnOpenXmlView() {
			var str = $('#txtDevLog').val();
			var strtemp = '';
			var arrReg = new Array();
			arrReg.push('sentence');
			arrReg.push('SLU1');
			arrReg.push('pattern');			
			
			for (var i = 0; i < arrReg.length; i++) {
				str = fnRecursiveReplace(str, '<' + arrReg[i] + '>', '</' + arrReg[i] + '>', 0);
			}
			
			$('#hidDevLog').val('<dev_log>' + str + '</dev_log>');
			var pop_title = "NoticePop" ;
	         	        
	        fnWinPop("", pop_title, 713, 626, 0, 0);
	        
	        var frmData = document.frmData ;
	        frmData.target = pop_title ;
	        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
	        frmData.submit();
	        fnpreventDefault(event);
		}
		
		function fnOpenXmlView() {
			var str = $('#txtDevLog').val();
			var strtemp = '';
			var arrReg = new Array();
			arrReg[0] = 'sentence';
			arrReg[1] = 'SLU1';
			arrReg[2] = 'pattern';			
			var arrRegExec;
			
			var reg;
			var subreg;
			
			for (var i = 0; i < arrReg.length; i++) {
				reg = new RegExp('<' + arrReg[i] + '>[\\W0-9a-zA-Z]+<\/' + arrReg[i] + '>','g');
				arrRegExec = str.match(reg);
				
				if (arrRegExec != null) {
					for (var j = 0; j < arrRegExec.length; j++) {						
						strtemp = '<' + arrReg[i] + '>' + arrRegExec[j].replace(new RegExp('<' + arrReg[i] + '>|<\/' + arrReg[i] + '>','g'), '').replace('<', '&lt;').replace('>', '&gt;') + '</' + arrReg[i] + '>';
						str.replace(arrRegExec[j], strtemp);						
					}
				}
			}
			
			$('#hidDevLog').val('<dev_log>' + str + '</dev_log>');
			var pop_title = "NoticePop" ;
	         	        
	        fnWinPop("", pop_title, 713, 626, 0, 0);
	        
	        var frmData = document.frmData ;
	        frmData.target = pop_title ;
	        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
	        frmData.submit();
	        fnpreventDefault(event);
		} */
		
		function fnOpenXmlView() {								
			$('#hidDevLog').val('<dev_log>' + $('#txtDevLog').val() + '</dev_log>');
			var pop_title = "NoticePop" ;
	         	        
	        fnWinPop("", pop_title, 713, 626, 0, 0);
	        
	        var frmData = document.frmData ;
	        frmData.target = pop_title ;
	        frmData.action = "<c:url value='/view/xmlviewer.do?Type=X' />" ;	         
	        frmData.submit();
	        fnpreventDefault(event);
		}
		function fnNotice() {			
			fnWinPop("<c:url value='/view/noitce.do' />" + "?Type=N", "NoticePop", 713, 626, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnlogout() {
			deleteCookie("USER_TYPE");
			deleteCookie("USER_ID");
			deleteCookie("USER_NAME");
			deleteCookie("USER_SEQ");
			deleteCookie("UNIQID");
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/logout.do' />");	
			comSubmit.submit();
			fnpreventDefault(event);
		}		
	</script>
</head>
<body>
	<iframe id="mainFrame" name="mainFrame" class="ui-layout-center"
		width="100%" height="100%" frameborder="0" scrolling="auto"
		src="<c:url value='/view/projectlist.do' />">
	</iframe>	
	<div class="ui-layout-east">
		<div id="right">
			<!--search box start-->
			<div class="search_box">
				<div class="logout">
					<a href="#" class="orange01" id="aTutorMap" style="background:#2F5597 !important;" onclick="fnTutor();">대화맵</a>
					<a href="#" class="orange01" onclick="fnNotice();">공지사항</a>
					<span style="font-size: 12px; color: #666666;"><%= session.getAttribute("USER_NAME")%> 님 접속중</span> 
					<a href="#" onclick="fnlogout();">&bull;로그아웃</a>
				</div>
				<ul class="inner_frame">
					<li><a href="#" class="orange01" onclick="fnLeanFile();">지식저장</a></li>					
					<li><a href="#" class="blue" onclick="fnLearning();">학습</a></li>
					<li><a href="#" class="orange" onclick="fnStart();">구동</a></li>						
					<c:if test="${USER_TYPE ne 'GU' && USER_TYPE ne 'OA'}">
						<li><a href="#" class="orange02" onclick="fnDataDownload();">data 다운</a></li>
					</c:if>
				</ul>
				<div id="divDialtitle" class="name_right">
					Dialog
				</div>
			</div>
			<div id="divStart">
				<!--search box end-->				
				<div class="leanbox">
					<!-- <iframe id="iframe2" style="width:100%; height:100%;"></iframe> -->
					<div>
						<table>
							<tbody class="leanboxtable">
								<tr>
									<td>
										<table>
											<tbody>
												<tr>
													<td colspan="4">
														<span>현재 도메인 : <b><span id="spDomain"></span></b></span>
													</td>
												</tr>
												<tr>
													<td style="text-align: left; padding:10px 0px;" colspan="3">
														<button id="btnStart" class="topbtn" onclick="fnInput('S');"> Start </button>
														<button id="btnStop" class="topbtn" onclick="fnStop('M');"> Stop </button>
														<button id="btnClear" class="topbtn" onclick="fnClear();"> Clear </button>
														<button id="btnLog" class="topbtn" onclick="fnLog();"> Log </button>
														<button id="btnDevLog" class="topbtn" onclick="fnDevLog();"> DevLog </button>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<div class="divresult" id="divResult" style="overflow-y:auto;"></div>
									</td>
								</tr>
								<tr>
									<td style="border:none;">
										<table style="width: 100%; height: 100%; border=none">
											<tbody>
												<tr>
													<td colspan="2" style="border:none;">
														<textarea id="areInput" class="inputarea" disabled='disabled'></textarea>   
													</td>
												</tr>
												<tr>
													<td style="width: 50%;border:none;">
														<button id="59c49cfbca4a1_asr_button" style="display:none; width: 100%; border-radius: 3px; vertical-align: middle; border: none; background: #ea5404; color: #ffffff; padding: 5px; height: 25px;" onclick="run_asr(this, '59c49cfbca4a1')" disabled="disabled">Init ASR</button>
													</td>
													<td style="width: 50%;border:none;">
														<button id="59c49cfbca4a1_speak_button" style="display:none; width: 100%; border-radius: 3px; vertical-align: middle; border: none; background: #ea5404; color: #ffffff; padding: 5px; height: 25px;" onclick="dialog_system_user_speak('59c49cfbca4a1');" disabled="disabled">Speak</button>   
													</td>
												</tr>
											</tbody>
										</table>   
									</td>
								</tr>
							</tbody>
						</table>	
					</div>
					
					<div id="divLog" style="margin-top:15px; display:none;">
						<!-- Title h1 -->
						<p class="logtitle">Turn <span id="spTurn"></span><input type="hidden" id="hidTurn" /></p>					
						<div class="logtitle_btn">
							<a onclick="fnLogPrevAndNext('P');" href="#"><img src="../images/btn_prev.png" alt="이전 보기"></a>
							<a onclick="fnLogPrevAndNext('N');" href="#"><img src="../images/btn_next.png" alt="다음 보기"></a>
						</div>
						<!-- Title h1// -->
						
						<hr />
						<div class="turn_chapter01">
							<span class="t_title_buu t_title_but" style="float:none;">Task</span>
							<div class="turn_table_u">
								<table class="turn_table" summary="task">
									<caption class="hide">정보 표</caption>
									<colgroup>
										<col width="38%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th>Previous task&nbsp;:&nbsp;</th>
											<td id="tdPrevTask">&nbsp;</td>
										</tr>
										<tr>
											<th>Current task&nbsp;:&nbsp;</th>
											<td id="tdCurrentTask">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="turn_chapter02">
							<span class="t_title_buu t_title_but" style="float:none;">Entities</span>
							<div class="turn_table_u">
								<table class="turn_table" summary="Entities">
									<caption class="hide">정보 표</caption>
									<colgroup>
										<col width="38%" />
										<col width="*" />
									</colgroup>
									<tbody id="tbEntities">
										<!-- <tr>
											<th colspan="2">Weather</th>
										</tr>
										<tr>
											<th class="th_bg01">date<span>&nbsp;:&nbsp;</span></th>
											<td>내일</td>
										</tr>
										<tr>
											<th class="th_bg01">date<span>&nbsp;:&nbsp;</span></th>
											<td>내일</td>
										</tr>
										<tr>
											<th class="th_bg02">city</span><span>&nbsp;:&nbsp;</span></th>
											<td>대구</td>
										</tr> -->
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="turn_chapter03">
							<span class="t_title_buu t_title_but" style="float:none;">Intents</span>
							<div class="turn_table_u">
								<table class="turn_table" summary="Intents">
									<caption class="hide">정보 표</caption>
									<colgroup>
										<col width="38%" />
										<col width="*" />
									</colgroup>
									<tbody id="tbIntents">
										<tr>
											<th colspan="2">User</th>
										</tr>
										<tr>
											<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th class="th_bg02">DA</span><span>&nbsp;:&nbsp;</span></th>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th colspan="2">System</th>
										</tr>
										<tr>
											<th class="th_bg01">utterance<span>&nbsp;:&nbsp;</span></th>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th class="th_bg01">pattern</span><span>&nbsp;:&nbsp;</span></th>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th class="th_bg02">pattern_id</span><span>&nbsp;:&nbsp;</span></th>
											<td>&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>					
					
					<div id="divDevLog" style="display:none;">
						<textarea id="txtDevLog" style="margin:0; margin-top:15px; padding:0px; width:100%; min-height:150px; max-height:250px;" class="leanbox">
						
						</textarea>
						<a href="#" onclick="fnOpenXmlView();" style="float:right; color:#ea5404;">xml viewer</a>
					</div>											
				</div>
			</div>
			<div id="divlean" style="display:none;">
				<div style="font-size:14px; color:#666666; margin:16px;">
					<span>현재 도메인 : <b><span id="spLeanDomain"></span></b></span>
				</div>				
				<div id="divleanbox" class="leanbox">
					<!-- <iframe id="iframe1" style="width:100%;height: 580px;"></iframe> -->
				</div>				
			</div>
		</div>
	</div>
	<form id="commonForm" name="commonForm">
		<input type="hidden" id="depth1" name="depth1" value="1" />
		<input type="hidden" id="depth2" name="depth2" value="1" />
		<input type="hidden" id="depth3" name="depth3" value="1" />
	</form>
	<!-- 팝업창으로 전송하는 정보 -->
	<form name="frmData" id="frmData" method="post">
	    <input type="hidden" id="hidDevLog" name="STR" />
	</form>
</body>
</html>