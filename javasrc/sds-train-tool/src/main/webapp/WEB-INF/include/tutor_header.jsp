<%@ page pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<title>대화모델링구축도구</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/tutor-common.css?ver=1'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/chosen.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery-ui-1.10.4.custom.css'/>" />

<script type="text/javascript" src="<c:url value='/js/jquery-latest.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/chosen.jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js?ver=11'/>"></script>   
   
<script type="text/javascript">	
		
	$(function () {
		//프로젝트 리스트 s
		$('#prjcontrol').bind({
		  click: function(e) {				
				//fnShowHide($('#divprjlist'));
				fnpreventDefault(e);	    				
		  }
		});
		
		$('#prjlist').click(function(e) {
			fnpreventDefault(e);
			fnMenuNavigator('L');
		});
		
		$('#prjcreate, #aCreateDomain').click(function(e) {
			fnpreventDefault(e);
			fnMenuNavigator('W');
		});
		//프로젝트 리스트 e
		
		//셀렉트 박스 레이아웃
		$('.u_group').chosen({ disable_search_threshold: 10, allow_single_deselect: true });
	});		
	
	//도메인 수정
	function fnProejctModify(prjseq) {
		if (prjseq != 'null') {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/tutorprojectupdate.do' />");
			comSubmit.addParam("SEQ_NO", prjseq);		
			comSubmit.submit();
		}
		else {
			alert('선택된 도메인이 없습니다.');			
			fnpreventDefault(event);
		}
	}	
	
	function fnMenuNavigator(type) {
		var comSubmit = new ComSubmit();
		var url = 'tutorprojectwrite.do';
		
		if (type == 'L') {
			url = 'tutordomainlist.do';
		}
		
		comSubmit.setUrl("<c:url value='/view/" + url + "' />");			
		//comSubmit.addParam("depth2", dep2);			
		comSubmit.submit();		
	}
	
	function fnSelectProject(seqno, name, port, langseq, prjuserseq) {
		//parent.fnStop('F');
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/view/settutorprojectlist.do' />");		
		comSubmit.addParam("SEQ_NO", seqno);
		comSubmit.addParam("PROJECT_NAME", name);
		comSubmit.addParam("SERVER_PORT", port);
		comSubmit.addParam("LANG_SEQ_NO", langseq);
		comSubmit.addParam("PROJECT_USER_SEQ", prjuserseq);
		comSubmit.submit();
	}	
	
	function fnGetLanguage() {
		var comAjax = new ComAjax();
		comAjax.setUrl("<c:url value='/view/getdomainlanguage.do' />");
		comAjax.setCallback("fnGetLanguageCallBack");
		if (getCookie("ProjectLangNo") == '')
			comAjax.addParam("LANG_SEQ_NO", 1);
		else
			comAjax.addParam("LANG_SEQ_NO", getCookie("ProjectLangNo"));
		comAjax.ajax();
		fnpreventDefault(event);
	}
	
	function fnGetLanguageCallBack(data) {
		if (data.STATUS == 'OK') {
			$('#hidEngineName').val(data.ENGINE_NAME);
			$('#hidDicName').val(data.DIC_NAME);	
		}
		else {
			$('#hidEngineName').val('dial');
			$('#hidDicName').val('nlp_dict');
		}	
	}
</script>