<%@ page pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<title>대화모델링구축도구</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css?ver=51'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/chosen.css'/>" />
<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery-ui-1.10.4.custom.css'/>" />

<script type="text/javascript" src="<c:url value='/js/jquery-latest.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/chosen.jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-ui-1.10.4.custom.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js?ver=11'/>"></script>   
   
<script type="text/javascript">
	//메뉴 index 변수
	var depth1;
	var depth2;
	var depth3;
	var AgentSeq = 0;
	var bWorkChk = false;
		
	$(function () {
		//프로젝트 리스트 s
		$('#prjcontrol').bind({
		  click: function(e) {				
				fnShowHide($('#divprjlist'));
				fnpreventDefault(e);	    				
		  }
		});
		
		$('#prjlist').click(function(e) {
			fnpreventDefault(e);
			fnMenuNavigator('projectlist', 0, 0);
		});
		
		$('#prjcreate, #aCreateDomain').click(function(e) {
			fnpreventDefault(e);
			fnMenuNavigator('projectwrite', 0, 0);
		});
		//프로젝트 리스트 e
				
		
		//1뎁스 s
		$('#lnb').find('> ul > li > a').click(function (e) {			
			if($(this).hasClass('on') == true) {
				fnMenuNavigator($(this).attr('id'), 0, 0);
				/* if($(this).attr('id') == 'menu1') {									
					$('#lnb .sub').slideUp();
					$(this).removeClass('on');
				}
				else {
					fnMenuNavigator($(this).attr('id'), 0, 0);	
				} */				
			}
			else {
				$('#lnb').find('> ul > li > a').removeClass('on');
				$(this).addClass('on');
				if($(this).attr('id') == 'menu1') {					
					$(this).next('.sub').slideDown();
				}
				else {
					$('#lnb .sub').slideUp();					
				}
				
				fnMenuNavigator($(this).attr('id'), 0, 0);
			}
			fnpreventDefault(e);
			return false;
		});
		//1뎁스 e
		
		$('#spb1btn').click(function(e) {
			if ($(this).hasClass('b1_btn_plus') == true) {
				$('#lnb .sub').slideDown();
				$(this).attr('class','b1_btn_minus');
			}
			else {
				$('#lnb .sub').slideUp();
				$(this).attr('class','b1_btn_plus');
			}
			fnpreventDefault(e);
			event.cancelBubble = true;
		});
		
		//2뎁스 s
		$('#lnb').on({				
			click: function(e) {				
				if($(this).children('span').hasClass('b2_btn') == true){				
					$(this).children('span').attr('class','b2_btn_off');
					$(this).parent().children('.sub02').slideDown();
				}else{
					$(this).children('span').attr('class','b2_btn');					
					$(this).parent().children('.sub02').slideUp();					
				}
				fnpreventDefault(e);
				//fnMenuNavigator('menu1', $(this).parent().index(), 1);
			},
			mouseover: function(e) {
				if($(this).hasClass('on') == false) {
					//$(this).children('span').attr('class','b2_btn_off');	
				}						    			
		  	},
		  	mouseleave: function(e) {
		  		if($(this).hasClass('on') == false) {
		  			//$(this).children('span').attr('class','b2_btn');
		  		}
		  	}	
		}, '.sub > li > a');
		//2뎁스 e		
		
		//3뎁스 s
		$('#lnb').on('click', '.sub > li > ul > li > a', function(e) {
			var $this = $(this);
			AgentSeq = $this.parent().parent().parent().find('input:hidden').val();
			fnMenuNavigator('menu1', $this.parent().parent().parent().index() + 1, $this.parent().index() + 1);
			
			//$(this).parents('.sub02').find('a').removeClass('onsub2');
			//$(this).addClass('onsub2');
			fnpreventDefault(e);
		});		
		//3뎁스 e
		
		//셀렉트 박스 레이아웃
		$('.u_group').chosen({ disable_search_threshold: 10, allow_single_deselect: true });
		
		//show xml 
		$('#showxml').click(function(e) {
			$('#divshowxml').toggle();
			fnpreventDefault(e);
		});
		
		//mainlogo
		$('#mainlogo').click(function(e) {
			fnMenuNavigator('menu1', 0, 0);
			fnpreventDefault(e);
		});
		
		//그래프 팝업
		$('#spPopGraph').bind({
			click:function(e) {		
				fnWinPop("<c:url value='/view/openagentgraph.do' />", "AgentGraph", 700, 650, 0, 0);
				fnpreventDefault(e);
				event.cancelBubble = true;
			},
			mouseover:function(e) {
				$(this).css('color', 'blue');
			},
			mouseleave:function(e) {
				$(this).css('color', '#1DDB16');
			}
		});
		
		//상단메뉴 고정
		var jbOffset = $( '.cont_title' ).offset();
		if (!gfn_isNull(jbOffset)) {
			$(window).bind('scroll resize', function() {			
				if ($( document).scrollTop() > jbOffset.top) {
	            	$('.cont_title').css('position', 'fixed').css('top', '0px').css('width', $('#contents').css('width'));
				}
				else {
					$('.cont_title').css('position', 'relative').css('top', '0px').css('width', $('#contents').css('width'));
				}
			});
		}		
				
		//메뉴세팅
		fnSetMenu();
		//$(top.document).find('#spDomain').text(getCookie("ProjectName"));
	});	
	
	function fnSetMenu() {		
		//메뉴 초기 세팅
		if ('${param.depth1 }' == '') {
			depth1 = '${depth1 }';
			depth2 = '${depth2 }';
			depth3 = '${depth3 }';	
		}
		else {
			depth1 = '${param.depth1 }';
			depth2 = '${param.depth2 }';
			depth3 = '${param.depth3 }';
		}
		
				
		//$('#lnb').find('.sub').hide();
				
		$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('> a').addClass('on');
		if (depth1 == 1) {
			$('#lnb .sub').slideDown();
			$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('.sub').show();
			$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('.sub').find('> li:nth-child(' + depth2 + ') a').addClass('on');
			$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('.sub').find('> li:nth-child(' + depth2 + ') a span').attr('class','b2_btn_off');
			$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('.sub').find('> li:nth-child(' + depth2 + ') ul').show();
			$('#lnb').find('> ul > li:nth-child(' + depth1 + ')').find('.sub').find('> li:nth-child(' + depth2 + ') ul > li:nth-child(' + depth3 + ') a').addClass('onsub2');
			$('#spb1btn').attr('class','b1_btn_minus');
		} else {
			$('#lnb .sub').hide();
		}
		
		$("#ulTask").sortable({
			start: function(event, ui) {
				var start_pos = ui.item.index();
				ui.item.data('start_pos', start_pos);
				
		    },
			update: function (event, ui) {
				var start_pos = ui.item.data('start_pos');
				var end_pos = ui.item.index();
				if (start_pos == 0 || end_pos == 0) {
					if (!confirm('init 태스크를 변경 하시겠습니까?')) {
						$("#ulTask").sortable('cancel');
						return;
					}
				}
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/updateagentsortno.do' />");
				comAjax.setCallback("fnGetLanguageCallBack");
				comAjax.addParam("SEQ_NO", ui.item.find('input:hidden').val());
				comAjax.addParam("SORT_NO", end_pos);
				if (start_pos > end_pos) {
					comAjax.addParam("SET", ' + 1');
					comAjax.addParam("START_POS", end_pos);
					comAjax.addParam("END_POS", start_pos - 1);	
				}
				else {
					comAjax.addParam("SET", ' - 1');
					comAjax.addParam("START_POS", start_pos + 1);
					comAjax.addParam("END_POS", end_pos);
				}
				
				comAjax.ajax();
		    }			
		});
		//메뉴 초기 세팅
	}
	
	//도메인 수정
	function fnProejctModify(prjseq) {
		if (prjseq != 'null') {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/projectupdate.do' />");		
			comSubmit.addParam("SEQ_NO", prjseq);		
			comSubmit.submit();
		}
		else {
			alert('선택된 도메인이 없습니다.');			
			fnpreventDefault(event);
		}
	}	
	
	function fnMenuNavigator(dep1, dep2, dep3) {
		var bSubmit = true;
		if (bWorkChk) {
			if (!confirm('작업된 내용이 있습니다. 저장하지 않고 이동 하시겠습니까?')) {
				bSubmit = false;
			}	
		}
		
		if (bSubmit) {
			var url = '';	
			var comSubmit = new ComSubmit();
			
			if(dep1 == 'projectwrite') {
				url = 'projectwrite.do';
			}
			else if(dep1 == 'projectlist') {
				url = 'projectlist.do';
			}
			else if(dep1 == 'menu1') {
				url = 'agentlist.do';
				if (dep2 != 0 && dep3 != 0) {				
					url = 'agentsub.do';
					if (AgentSeq.trim() == '') {
						AgentSeq = 0;
					}
					comSubmit.addParam("AGENT_SEQ", AgentSeq);	
				}
			}		
			else if(dep1 == 'menu2') {
				url = 'slotlist.do';	
			}
			else if(dep1 == 'menu3') {
				url = 'useruttrlist.do';
			}
			else if(dep1 == 'menu4') {
				url = 'knowledgelist.do';
			}
			else if(dep1 == 'menu5') {
				url = 'userlist.do';
			}
			else if(dep1 == 'menu6') {
				url = 'logslist.do';
			}
			
			comSubmit.setUrl("<c:url value='/view/" + url + "' />");
			if (dep2 != 0) 
				comSubmit.addParam("depth2", dep2);	
			if (dep3 != 0) 
				comSubmit.addParam("depth3", dep3);
			comSubmit.submit();
		}
	}
	
	function fnSelectProject(seqno, name, port, langseq, prjuserseq) {
		//parent.fnStop('F');
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/view/setprojectlist.do' />");		
		comSubmit.addParam("SEQ_NO", seqno);
		comSubmit.addParam("PROJECT_NAME", name);
		//comSubmit.addParam("SERVER_PORT", port);
		comSubmit.addParam("LANG_SEQ_NO", langseq);
		comSubmit.addParam("PROJECT_USER_SEQ", prjuserseq);
		comSubmit.submit();
	}	
	
	function fnGetLanguage() {
		var comAjax = new ComAjax();
		comAjax.setUrl("<c:url value='/view/getdomainlanguage.do' />");
		comAjax.setCallback("fnGetLanguageCallBack");
		if (getCookie("ProjectLangNo") == '')
			comAjax.addParam("LANG_SEQ_NO", 1);
		else
			comAjax.addParam("LANG_SEQ_NO", getCookie("ProjectLangNo"));
		comAjax.ajax();
		fnpreventDefault(event);
	}
	
	function fnGetLanguageCallBack(data) {
		if (data.STATUS == 'OK') {
			$('#hidEngineName').val(data.ENGINE_NAME);
			$('#hidDicName').val(data.DIC_NAME);	
		}
		else {
			$('#hidEngineName').val('dial');
			$('#hidDicName').val('nlp_dict');
		}	
	}
</script>