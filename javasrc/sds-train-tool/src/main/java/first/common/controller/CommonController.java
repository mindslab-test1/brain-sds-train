package first.common.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.sun.org.apache.xpath.internal.operations.Mod;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import maum.brain.sds.train.DesignerScenario;
import maum.brain.sds.train.Edge;
import maum.brain.sds.train.Node;
import maum.brain.sds.train.Position;
import maum.brain.sds.train.SystemUtter;
import maum.brain.sds.train.Task;
import maum.brain.sds.train.TaskType;
import maum.common.LangOuterClass.Lang;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import first.common.common.CommandMap;
import first.common.service.CommonService;
import first.view.service.ViewService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CommonController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Resource(name="viewService")
	private ViewService viewService;
	
	@RequestMapping(value="/common/downloadFile.do")
	public void downloadFile(CommandMap commandMap, HttpServletResponse response) throws Exception{		
		Map<String,Object> map = commonService.selectFileInfo(commandMap.getMap());
		String storedFileName = (String)map.get("STORED_FILE_NAME");
		String originalFileName = (String)map.get("ORIGINAL_FILE_NAME");
		
		byte fileByte[] = FileUtils.readFileToByteArray(new File("E:\\dev\\file\\"+storedFileName));
		
		response.setContentType("application/octet-stream");
		response.setContentLength(fileByte.length);
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + URLEncoder.encode(originalFileName,"UTF-8")+"\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.getOutputStream().write(fileByte);
		
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
	
	@RequestMapping(value="/common/downloadzipFile.do")
	public void downloadZipFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		String copyPath = String.format("%s%s/", viewService.GetPath("D", false, request), viewService.GetDomainName("D", false, commandMap.getMap(), request));
		log.debug(copyPath);
		
		String fileName = copyPath + commandMap.get("PROJECT_NAME").toString() + ".zip";
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileName));
		byte fileByte[] = new byte[4096];
		
		Vector DataFiles = new Vector();
		File sourceF = new File(copyPath);
		File[] Files = sourceF.listFiles();
		
		for (File file : Files) {
			//log.debug(file.getName());
			if(!file.isDirectory() && !file.getName().contains(".zip")) {
				//폴더 제외 하고, 모두 다운				
				FileInputStream fis = new FileInputStream(file);			
				out.putNextEntry(new ZipEntry(file.getName()));
				//fileByte = FileUtils.readFileToByteArray(file);
				
				int len = 0;
				while((len = fis.read(fileByte)) > 0) {
					out.write(fileByte, 0, len);						
				}
				out.closeEntry();
				fis.close();
				
				/*if (file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".DAtype") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".dialogLib") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".slot") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".task") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".txt") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".sql")) {		
					FileInputStream fis = new FileInputStream(file);			
					out.putNextEntry(new ZipEntry(file.getName()));
					//fileByte = FileUtils.readFileToByteArray(file);
					
					int len = 0;
					while((len = fis.read(fileByte)) > 0) {
						out.write(fileByte, 0, len);						
					}
					out.closeEntry();
					fis.close();
				}*/
				
			}
		}		
		out.close();
		
		response.setContentType("application/x-zip-compressed; charset=utf-8");
		response.setHeader("Content-Disposition", "inline; filename="+new File(fileName).getName());
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		InputStream in = new FileInputStream(new File(fileName));
		int read = in.read(fileByte);
		while (read != -1) {
			response.getOutputStream().write(fileByte, 0, read);
			read = in.read(fileByte);
		}
		response.getOutputStream().flush();
		response.getOutputStream().close();
		
		File f = new File(fileName);
		if (f.exists()) {
			f.delete();
		}
	}
}

@RestController
class ScenarioController {
	Logger log = Logger.getLogger(this.getClass());

	@Resource(name="viewService")
	private ViewService viewService;

	// 설계 시나리오 정보를 DB에 삽입하는 API
	@RequestMapping (value="/scenario/insertScenarioOutlineToDB.do", method = RequestMethod.POST)
	public ResponseEntity<?> insertScenarioOutlineToDB (HttpServletRequest request) {

		Map<String, String> result = new HashMap<>();
		HttpStatus httpStatus;
		Map map = new HashMap();
		int langNo = 1;
		int endAgentId = 0;

		String scenarioJson = "";
		DesignerScenario.Builder builder = DesignerScenario.newBuilder();
		DesignerScenario scenario;

		try {
			scenarioJson = ((String[]) request.getParameterMap().get("scenarioJson"))[0];
			JsonFormat.parser().ignoringUnknownFields().merge(scenarioJson, builder);
			scenario = builder.build();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("scenarioJson 형식이 잘못되었습니다. \n" + scenarioJson);

			result.put("status", "FAIL");
			result.put("data", scenarioJson);
			result.put("message", "scenarioJson 형식이 잘못되었습니다. ");
			httpStatus = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(result, httpStatus);
		}

		// USER_SEQ ( from cookie )
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(int i=0; i < cookies.length; i++){
				if (cookies[i].getName().equals("USER_SEQ")) {
					map.put("USER_SEQ", cookies[i].getValue());
					map.put("USER_SEQ_NO", cookies[i].getValue());
					map.put("PROJECT_USER_SEQ", cookies[i].getValue());
				}
			}
		}

		if (map.get("USER_SEQ") == null) {

			// COOKIE에 없을 경우 body 조회
			if (request.getParameterMap().get("USER_SEQ") != null) {
				String userSeqStr = ((String[]) request.getParameterMap().get("USER_SEQ"))[0];
				int userSeq = Integer.parseInt(userSeqStr);
				map.put("USER_SEQ", userSeq);
				map.put("USER_SEQ_NO", userSeq);
				map.put("PROJECT_USER_SEQ", userSeq);

			} else {

				log.error("사용자 인증이 필요합니다. ");

				result.put("status", "FAIL");
				result.put("message", "사용자 인증(쿠키)이 필요합니다. ");
				httpStatus = HttpStatus.FORBIDDEN;
				return new ResponseEntity<>(result, httpStatus);
			}

		}

		// PROJECT_NAME
		if (scenario.getName().isEmpty()) {
			result.put("status", "FAIL");
			result.put("data", scenarioJson);
			result.put("message", "scenario명이 필요합니다.");
			httpStatus = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(result, httpStatus);
		}
		map.put("PROJECT_NAME", scenario.getName());

		// LANG_NO ( from scenario ) - 1: KOR, 2: ENG
		langNo = scenario.getLangValue() + 1;
		map.put("LANG_SEQ_NO", langNo);
		request.getSession().setAttribute("ProjectLangNo", langNo);

		// DESCRIPTION
		map.put("DESCRIPTION", scenario.getDescription());

		try {
			List<Map> list = viewService.SelectProjectName(map);

			// project가 이미 존재할 경우
			if (list != null && list.size() != 0) {

				String seqNo = list.get(0).get("SEQ_NO").toString();
				map.put("SEQ_NO", seqNo);
				viewService.deleteProject(map, request);
			}

			// ETC field
			map.put("TYPE", "I");
			map.put("CHATBOT_USE_FLAG", "N");
			map.put("CHATBOT_DATA_FLAG", "N");
			map.put("FILTER_VALUE", "0.35");
			map.put("USER_API_KEY", "");

			viewService.insertProject(map, request);

			// userDomain 에 추가, 기본 task(END) 추가
			Map paramMap = new HashMap();
			paramMap.put("USER_SEQ", map.get("USER_SEQ"));
			paramMap.put("PROJECT_SEQ", map.get("SEQ_NO"));
			paramMap.put("AGENT_NAME", "END");
			paramMap.put("RESET_SLOT", "0");
			paramMap.put("TASK_TYPE", "essential");
			paramMap.put("RELATED_SLOTS", "");

			viewService.insertUserDomain(paramMap);
			viewService.insertOnlyAgent(paramMap);
			endAgentId = Integer.parseInt(paramMap.get("SEQ_NO").toString());

			// Task
			Map taskIdMap = new HashMap();
			for (Node node : scenario.getNodesList()) {
				Map agentMap = new HashMap();
				int sortNo = 2;

				JSONObject objectItem = new JSONObject();
				JSONArray intentionItem = new JSONArray();
				JSONObject intentItObj = new JSONObject();
				JSONArray objArray = new JSONArray();
				JSONObject intentObj = new JSONObject();
				JSONArray objDetailArray = new JSONArray();
				JSONObject intentObjDetail = new JSONObject();
				JSONArray slotFillingArray = new JSONArray();


				for (SystemUtter utter : node.getAttrList()) {
					if (utter.getType().equals("멘트")) {
						intentObjDetail.put("TYPE", "T");
						intentObjDetail.put("OBJECT_VALUE", utter.getText());
						objDetailArray.add(intentObjDetail);
					}
				}

				intentObj.put("OBJECT_VALUE", "inform()");
				intentObj.put("LIMIT_DA", "");
				intentObj.put("OBJECT_DETAIL", objDetailArray);
				objArray.add(intentObj);

				intentItObj.put("UTTR", "true");
				intentItObj.put("ACT", "");
				intentItObj.put("INTENTION_TYPE", 1);
				intentItObj.put("OBJECT", objArray);
				intentionItem.add(intentItObj);

				objectItem.put("INTENTION_ITEM", intentionItem);
				objectItem.put("SLOT_FILLING_ITEM", slotFillingArray);

				agentMap.put("AGENT_NAME", node.getText());
				agentMap.put("RESET_SLOT", "");
				agentMap.put("PROJECT_SEQ", map.get("SEQ_NO"));
				agentMap.put("TASK_TYPE", "");
				agentMap.put("RELATED_SLOTS", "");

				agentMap.put("DESCRIPTION", node.getDescription());
				agentMap.put("TASK_GOAL", "");
				agentMap.put("SEQ_NO", "");
				agentMap.put("OBJECT_ITEM", objectItem.toJSONString());

				// 시작 노드일 시 SORT_NO = 0
				if (node.getType().equals("start")) {
					agentMap.put("SORT_NO", "0");
				} else {
					agentMap.put("SORT_NO", sortNo);
					sortNo ++;
				}

				// Position
				agentMap.put("LEFT_POS", node.getPosition().getLeft());
				agentMap.put("TOP_POS", node.getPosition().getTop());

				viewService.insertAgent(agentMap);

				// 종료 노드일 경우
				if (node.getType().equals("end")) {

					// END task 위치 조절
					Map endAgentMap = new HashMap();
					endAgentMap.put("SEQ_NO", endAgentId);
					endAgentMap.put("LEFT_POS", node.getPosition().getLeft() + 150);
					endAgentMap.put("TOP_POS", node.getPosition().getTop());
					viewService.updateAgentPositionStraightly(endAgentMap);

					// 종료 Node - 'END' task connection 생성
					Map connectionMap = new HashMap<>();
					connectionMap.put("SOURCE_ID", Integer.parseInt(agentMap.get("SEQ_NO").toString()));
					connectionMap.put("TARGET_ID", endAgentId);
					connectionMap.put("CONDITION", true);
					connectionMap.put("PROJECT_SEQ", map.get("SEQ_NO"));
					viewService.insertAgentGraph(connectionMap);
				}

				// table seq_id 와 json id map
				taskIdMap.put(node.getId(), agentMap.get("SEQ_NO"));
			}

			// Edge (Task 연결 선)
			for (Edge edge : scenario.getEdgesList()) {

				Map connectionMap = new HashMap<>();
				connectionMap.put("SOURCE_ID", taskIdMap.get(edge.getSource()));
				connectionMap.put("TARGET_ID", taskIdMap.get(edge.getTarget()));
				connectionMap.put("CONDITION", edge.getData().getLabel());
				connectionMap.put("PROJECT_SEQ", map.get("SEQ_NO"));
				viewService.insertAgentGraph(connectionMap);

			}

			result.put("status", "SUCCESS");
			result.put("data", com.googlecode.protobuf.format.JsonFormat.printToString(scenario));
			httpStatus = HttpStatus.OK;

		} catch (Exception e) {
			e.printStackTrace();

			result.put("status", "ERROR");
			result.put("message", "DB insert error");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(result, httpStatus);
	}

}
