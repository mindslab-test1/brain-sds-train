package first.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class Unescape {

  /**
   * String Escape 처리
   */
  public static String escape(String src) {
    try {
      return URLEncoder.encode(src, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return src;
    }
  }

  /**
   * String Escape 처리
   */
  public static String unescape(String src) {
    try {
      return URLDecoder.decode(src, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return src;
    }
  }
}
