package first.common.util;

public class SlotObject {
	private int SeqNo;
	private int ClassSeqNo;
	private String Color;
	
	public SlotObject(int seqno, int classseqno, String color) {
		this.SeqNo = seqno;
		this.ClassSeqNo = classseqno;
		this.Color = color;
	}
	/* getter */
	public int getSeqNo() {
		return SeqNo;
	}

	public int getClassSeqNo() {
		return ClassSeqNo;
	}
	
	public String getColor() {
		return Color;
	}
	/* getter */	
}
