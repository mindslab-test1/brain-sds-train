package first.common.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.web.servlet.view.AbstractView;
/*import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.web.servlet.view.document.AbstractExcelView;*/

import first.common.common.CommandMap;
import first.view.dao.ViewDAO;
import first.view.service.ViewService;

public class ExcelUtils extends AbstractView {
	Logger log = Logger.getLogger(this.getClass());
	@Resource(name="viewService")
	private ViewService viewService;
	
	private static final String CONTENT_TYPE = "application/vnd.ms-excel";
	
	public ExcelUtils() {
	    setContentType(CONTENT_TYPE);
	}
	
	protected void renderMergedOutputModel(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    try {
	        Workbook workbook = new XSSFWorkbook();	       
	        Sheet worksheet;
	        Row row;
	        
	        // Doing
	        String excelName = "Entry";
	        String Type = map.get("TYPE").toString();
	      
	        
	        if(Type == "O") {			
				excelName = String.format("[Slot]%s#%s.%s", map.get("PROJECT_NAME").toString(), map.get("CLASS_NAME").toString(), map.get("SLOT_NAME").toString());
				worksheet = workbook.createSheet(map.get("SLOT_NAME").toString());
				
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> objlist = (List<Map<String, Object>>)map.get("OBJECT_LIST");
				List<Map<String, Object>> objdtllist = (List<Map<String, Object>>)map.get("DETAIL_LIST");			
				
				int iCol = 0;				
				for(int i = 0; i < objlist.size(); i++){
					iCol = 0;
					row = worksheet.createRow(i);
					
					row.createCell(iCol).setCellValue(objlist.get(i).get("OBJECT_NAME").toString());
					for (int j = 0; j < objdtllist.size(); j++) {
						if (objlist.get(i).get("SEQ_NO").equals(objdtllist.get(j).get("PARENT_SEQ_NO"))) {
							iCol++;						
							row.createCell(iCol).setCellValue(objdtllist.get(j).get("OBJECT_NAME").toString());
						}												
					}					
				}             			 
			}
	        else if(Type == "I") {
	        	excelName = String.format("[Instance]%s#%s", map.get("PROJECT_NAME").toString(), map.get("CLASS_NAME").toString());
				worksheet = workbook.createSheet(map.get("CLASS_NAME").toString());
				String[] arrHeader = map.get("HEADER_LIST").toString().split(",");
				String[] arrCeader = map.get("COL_LIST").toString().split(",");
				String[] arrTemp;
			
		        int iRow = 1;
		        int iCol = 0;
		        CommandMap ObjMap;
		        List<Map<String, Object>> list;
		        
		        row = worksheet.createRow(0);
		        Row instancRow;
		        for (int i = 0; i < arrHeader.length; i++) {		        	
					row.createCell(i).setCellValue(arrHeader[i]);									
				}
		        
		        ObjMap = new CommandMap();					
				ObjMap.put("SEQ_NO", map.get("SEQ_NO"));
				ObjMap.put("COL_LIST", map.get("SEL_COL_LIST"));
				
				list = viewService.selectSlotInstances2(ObjMap.getMap());
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					row = worksheet.createRow(iRow);
					
					Map<String, Object> obj = (Map<String, Object>) iterator.next();
					
					for (int j = 0; j < arrCeader.length; j++) {
						row.createCell(j).setCellValue(obj.get(arrCeader[j]).toString());
					}
					
		        	iRow++;	
				}
	        }
	        else if(Type.equals("U")) {	
				excelName = "Users";
				worksheet = workbook.createSheet("Users");
				
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> list = (List<Map<String, Object>>)viewService.selectUserAllList(map);
				
				int iRow = 1;
				row = worksheet.createRow(0);		        
		        row.createCell(0).setCellValue("ID");
		        row.createCell(1).setCellValue("이름");
		        row.createCell(2).setCellValue("등록일자");
		        row.createCell(3).setCellValue("최근 로그인");
		        row.createCell(4).setCellValue("도메인 수");
		        
		        
				for (Map<String, Object> item : list) {
					row = worksheet.createRow(iRow);
					
					row.createCell(0).setCellValue(item.get("USER_ID").toString());
					row.createCell(1).setCellValue(item.get("USER_NAME").toString());
					row.createCell(2).setCellValue(item.get("CREATE_DATE").toString());
					row.createCell(3).setCellValue(item.get("LAST_LOGIN_TIME").toString());
					row.createCell(4).setCellValue(item.get("DOMAIN_COUNT").toString());
					
					iRow++;
				}    			 
			}
	        
	        excelName = getDisposition(excelName, getBrowser(request));	        
	        //response.setContentType("application/vnd.ms-excel; charset=euc-kr");
	        response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xlsx");
	        response.setHeader("Content-Transfer-Encoding", "binary");
	        ServletOutputStream out = response.getOutputStream();
	        workbook.write(out);
	
	        if (out != null) out.close();
	        
	    } catch (Exception e) {
	        throw e;
	    }
	}		
	
	public List<Map<String, String>> read(ReadOption readOption) {				
		Workbook wb = FileType.getWorkbook(readOption.getFilePath());		
		Sheet sheet = wb.getSheetAt(0);				
		int numOfRows = sheet.getPhysicalNumberOfRows();
		int numOfCells = 0;
		
		Row row = null;
		Cell cell = null;	
		String cellName = "";		
		Map<String, String> map = null;		
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		List<Map<String, String>> result = new ArrayList<Map<String, String>>(); 
		
		for(int rowIndex = readOption.getStartRow() - 1; rowIndex < numOfRows; rowIndex++) {
			row = sheet.getRow(rowIndex);
			
			if(row != null) {
				if (rowIndex == 0) {
					numOfCells = row.getPhysicalNumberOfCells();
				}
				else {
					if (numOfCells < row.getPhysicalNumberOfCells()) {
						numOfCells = row.getPhysicalNumberOfCells();	
					}
				}
				
				map = new HashMap<String, String>();				

				for(int cellIndex = 0; cellIndex < numOfCells; cellIndex++) {
					cell = row.getCell(cellIndex);					
					cellName = CellRef.getName(cell, cellIndex);	

					/*
					 * 추출 대상 컬럼인지 확인한다.
					 * 추출 대생 컬럼이 아니라면
					 * for로 다시 올라간다. 
					 */
					if( !readOption.getOutputColumns().contains(cellName) ) {
						continue;
					}
					
					/*
					 * 맵 객체의 Cell의 이름을 키(Key)로 데이터를 담는다.
					 */
					
					if (cell != null && cell.getCellType() == CellType.NUMERIC) {
						try {												
							map.put(cellName, decimalFormat.format(Double.parseDouble(CellRef.getValue(cell))));
						} catch (Exception e) {
							// TODO: handle exception
							map.put(cellName, CellRef.getValue(cell));
						}					
					}
					else
						map.put(cellName, CellRef.getValue(cell));
				}
				result.add(map);
			}
		}
		return result;
	} 
	
	public String ExcelToHtml(String Path) {
		StringBuilder sbhtml = new StringBuilder();
		Workbook wb = FileType.getWorkbook(Path);
		try {
			Sheet sheet = wb.getSheetAt(0);		
			Iterator<Row> rows = sheet.rowIterator();		
			Iterator<Cell> cells = null;
			Row row;
			String tdstyle = "";					
			String strxlsColor = "";
			Map<Short, String> xlsColor = new HashMap<Short, String>();
			xlsColor.put((short) 14, "9900FF");
			xlsColor.put((short) 12, "0000FF");
			xlsColor.put((short) 10, "FF0000");
			xlsColor.put((short) 8, "000000");
			
			sbhtml.append("<table>");			
			while (rows.hasNext()) {
				row = rows.next();			
				cells = row.cellIterator();
				/*if (iCnt < 50) {				
					log.debug(row.getFirstCellNum());
					log.debug(row.getLastCellNum());				
					log.debug("=======================");
				}*/
				
				sbhtml.append("<tr>");
				if (row.getFirstCellNum() != 0) {
					for (int i = 0; i < row.getFirstCellNum(); i++) {
						sbhtml.append("<td>&nbsp;</td>");
						sbhtml.append("</td>");
					}
				}
				while (cells.hasNext()) {
					Cell cell = cells.next();
					tdstyle = "";					
					
					if(Path.toUpperCase().endsWith(".XLS")) {
						HSSFCellStyle style = (HSSFCellStyle) cell.getCellStyle();
						//log.debug(style);						
						HSSFFont font = style.getFont(wb);	
						
						if (xlsColor.containsKey(font.getColor()))
							strxlsColor = xlsColor.get(font.getColor());
						else
							strxlsColor = "000000";
							
						if (font.getBold()) {							
							tdstyle = String.format("font-weight:bold; color:#%s;", strxlsColor);
						}	  
					}
					else if(Path.toUpperCase().endsWith(".XLSX")) {
						XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
						//log.debug(style);
						XSSFFont font = style.getFont();
						//log.debug(font);
						XSSFColor colour = font.getXSSFColor();						
						if (font.getBold()) {
							tdstyle = String.format("font-weight:bold; color:#%s;", colour.getARGBHex().substring(2));
						} 
					}	   					
					
					sbhtml.append(String.format("<td style='%s'>", tdstyle));
					sbhtml.append(cell.toString().replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
					sbhtml.append("</td>");
				}
				sbhtml.append("</tr>");
			}				
			
			sbhtml.append("</table>");			
			wb.close();
		} catch (Exception e) {
			log.debug(e.getMessage());
			try {
				wb.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return sbhtml.toString();
	}
	
	private String getBrowser(HttpServletRequest request) { 
		String header = request.getHeader("User-Agent"); 
		if (header.indexOf("MSIE") > -1) { 
			return "MSIE"; 
		} 
		else if (header.indexOf("Chrome") > -1) { 
			return "Chrome"; 
		} 
		else if (header.indexOf("Opera") > -1) { 
			return "Opera"; 
		}
		else if (header.indexOf("Trident/7.0") > -1) { 
			//IE 11 이상 //IE 버전 별 체크 >> Trident/6.0(IE 10) , Trident/5.0(IE 9) , Trident/4.0(IE 8) 
			return "MSIE"; 
		} 
		
		return "Firefox";
	}
	
	private String getDisposition(String filename, String browser) throws Exception { 
		String encodedFilename = null; 
		if (browser.equals("MSIE")) { 
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20"); 
		}
		else if (browser.equals("Firefox")) { 
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\""; 
		}
		else if (browser.equals("Opera")) { 
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\""; 
		} 
		else if (browser.equals("Chrome")) { 
			StringBuffer sb = new StringBuffer(); 
			for (int i = 0; i < filename.length(); i++) { 
				char c = filename.charAt(i); 
				if (c > '~') { 
					sb.append(URLEncoder.encode("" + c, "UTF-8")); 
				} 
				else { 
					sb.append(c); 
				} 
			} 
			encodedFilename = sb.toString(); 
		} 
		else { 
			throw new RuntimeException("Not supported browser"); 
		} 
		return encodedFilename; 
	}
			
	/*@Override
	protected void buildExcelDocument(Map<String, Object> map, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String Type = map.get("TYPE").toString();
		String excelName = "";
		HSSFSheet worksheet = null;
        HSSFRow row = null;
        
        log.debug(map);        
        
		if(Type == "O") {			
			excelName = URLEncoder.encode("Entry","UTF-8");
			worksheet = workbook.createSheet(map.get("SLOT_NAME").toString());
			
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> objlist = (List<Map<String, Object>>)map.get("OBJECT_LIST");
			List<Map<String, Object>> objdtllist = (List<Map<String, Object>>)map.get("DETAIL_LIST");			
			
			row = worksheet.createRow(0);
			for(int i = 0; i < objlist.size(); i++){
				row.createCell(i).setCellValue(objlist.get(i).get("OBJECT_NAME").toString());            	
			}             			 
		}
		
		response.setContentType("Application/Msexcel");
        response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+"-excel");
	}*/
}
