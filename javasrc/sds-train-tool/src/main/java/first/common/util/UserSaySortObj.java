package first.common.util;

import java.util.Comparator;

public class UserSaySortObj {
	private Integer SeqNo;
	private String SlotSeq;
	
	public UserSaySortObj(int seqno, String slotseq) {
		this.SeqNo = seqno;
		this.SlotSeq = slotseq;
	}
	/* getter */
	public Integer getSeqNo() {
		return SeqNo;
	}

	
	public String getSlotSeq() {
		return SlotSeq;
	}
	
	/*//String 오름차순
	public class AscendingObjStr implements Comparator<UserSaySortObj> {
		@Override
		public int compare(UserSaySortObj o1, UserSaySortObj o2) {
			return o1.getSlotSeq().compareTo(o2.getSlotSeq());
		}
	}

	//String 내림차순
	public class DescendingObjStr implements Comparator<UserSaySortObj> {
		@Override
		public int compare(UserSaySortObj o1, UserSaySortObj o2) {
			return o2.getSlotSeq().compareTo(o1.getSlotSeq());
		}
	}

	//Integer 오름차순
	public class AscendingObjInt implements Comparator<UserSaySortObj> {
		@Override
		public int compare(UserSaySortObj o1, UserSaySortObj o2) {
		    return o1.getSeqNo().compareTo(o2.getSeqNo());
		}
	}

	//Integer 내림
	public class DescendingObjInt implements Comparator<UserSaySortObj> {
		@Override
		public int compare(UserSaySortObj o1, UserSaySortObj o2) {
			return o2.getSeqNo().compareTo(o1.getSeqNo());
		}
	}*/
}