package first.common.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import first.common.common.CommandMap;


public class CommonUtils {
	private static final Logger log = Logger.getLogger(CommonUtils.class);
	
	public static String getRandomString(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}	
	
	public static void printMap(Map<String,Object> map){
		Iterator<Entry<String,Object>> iterator = map.entrySet().iterator();
		Entry<String,Object> entry = null;
		log.debug("--------------------printMap--------------------\n");
		while(iterator.hasNext()){
			entry = iterator.next();
			log.debug("key : "+entry.getKey()+",\tvalue : "+entry.getValue());
		}
		log.debug("");
		log.debug("------------------------------------------------\n");
	}
	
	public static void printList(List<Map<String,Object>> list){
		Iterator<Entry<String,Object>> iterator = null;
		Entry<String,Object> entry = null;
		log.debug("--------------------printList--------------------\n");
		int listSize = list.size();
		for(int i=0; i<listSize; i++){
			log.debug("list index : "+i);
			iterator = list.get(i).entrySet().iterator();
			while(iterator.hasNext()){
				entry = iterator.next();
				log.debug("key : "+entry.getKey()+",\tvalue : "+entry.getValue());
			}
			log.debug("\n");
		}
		log.debug("------------------------------------------------\n");
	}
	
	public static String SetSlotListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String classSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (classSeq.equals(item.get("CLASS_SEQ_NO").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(classSeq, templist);
					classSeq = item.get("CLASS_SEQ_NO").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				classSeq = item.get("CLASS_SEQ_NO").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(classSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	return jsonData;
	}
	
	public static String SetClassNameSlotListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String className = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (className.equals(item.get("CLASS_NAME").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(className, templist);
					className = item.get("CLASS_NAME").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				className = item.get("CLASS_NAME").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(className, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	return jsonData;
	}
	
	public static String SetIntentListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String agentSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (agentSeq.equals(item.get("AGENT_SEQ").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(agentSeq, templist);
					agentSeq = item.get("AGENT_SEQ").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				agentSeq = item.get("AGENT_SEQ").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(agentSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	return jsonData;
	}
	
	/*public static Map<String, SlotObject> setSlotListToMapSlotName(List<Map<String, Object>> slist) {
		Map<String, SlotObject> returnMap = new HashMap<String, SlotObject>();
		
		for (Iterator iterator = slist.iterator(); iterator.hasNext();) {
			Map<String, Object> sitem = (Map<String, Object>) iterator.next();
			
			returnMap.put(sitem.get("SLOT_NAME").toString(), new SlotObject((int)sitem.get("SEQ_NO"), (int)sitem.get("CLASS_SEQ_NO"), sitem.get("COLOR").toString()));			
		}
		return returnMap;		
	}*/
	
	public static Map<String, Map<String, String>> setSlotColorToMap(List<Map<String, Object>> slist) {
		Map<String, Map<String, String>> returnMap = new HashMap<String, Map<String, String>>();		
		Map<String, String> tempmap = new HashMap<String, String>();		
		String classseq = "";

		for (int i = 0; i < slist.size(); i++) {
			if (!classseq.isEmpty() && !classseq.equals(slist.get(i).get("CLASS_SEQ").toString())) {							
				returnMap.put(classseq, tempmap);
				tempmap = new HashMap<String, String>();						
			}
			
			classseq = slist.get(i).get("CLASS_SEQ").toString();
			tempmap.put(slist.get(i).get("SLOT_NAME").toString(), slist.get(i).get("COLOR").toString());
			
			if (i + 1 == slist.size()) {
				returnMap.put(classseq, tempmap);
				tempmap = new HashMap<String, String>();
			}
		}
		return returnMap;
	}
	
	public static Map<String, String> setClassListToMapName(List<Map<String, Object>> clist) {
		Map<String, String> returnMap = new HashMap<String, String>();
		
		for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			returnMap.put(citem.get("CLASS_NAME").toString(), citem.get("SEQ_NO").toString());			
		}
		return returnMap;		
	}
	
	public static void slotRecursive(List<Map<String,Object>> slotlist, StringBuilder sbcol, String SeqNo, String SlotSeqNo, JSONObject jsonObj, JSONArray objArray, String SlotName, String ParentSeq, int typeSum) {	
		for(int i = 0 ; i < objArray.size() ; i++){
			JSONObject SlotObj = (JSONObject)objArray.get(i);
			int tempsum = typeSum + Integer.parseInt(SlotObj.get("SEQ_NO").toString());	
			if (SlotObj.get("SLOT_TYPE").toString().equals("C")) {
				if (SlotName.isEmpty()) {
					slotRecursive(slotlist, sbcol, SeqNo, SlotObj.get("SEQ_NO").toString(), jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), SlotObj.get("SLOT_NAME").toString(), ParentSeq + "_" + SlotObj.get("SEQ_NO").toString(), tempsum);
				}
				else {
					slotRecursive(slotlist, sbcol, SeqNo, SlotSeqNo, jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), SlotName + "." + SlotObj.get("SLOT_NAME"), ParentSeq, tempsum);
				}
			}
			else {
				if (SlotObj.get("INSTANCE_FLAG").toString().equals("Y")) {
					Map<String,Object> sublist = new HashMap<String,Object>();
					if (SlotName.isEmpty()) {
						sublist.put("VIEW_COL", SlotObj.get("SLOT_NAME"));
						sublist.put("QUERY_COL", SeqNo + "_" + SlotObj.get("SEQ_NO"));
					}
					else {
						sublist.put("VIEW_COL", SlotName + "." + SlotObj.get("SLOT_NAME"));
						sublist.put("QUERY_COL", String.format("%s_%s_%s", ParentSeq, typeSum, SlotObj.get("SEQ_NO")));
					}				
					slotlist.add(sublist);	
										
					if (sbcol.length() == 0)
						sbcol.append(sublist.get("QUERY_COL"));
					else
						sbcol.append(String.format(", %s", sublist.get("QUERY_COL")));
				}				
			}
		}
	}	
	
	public static void replaceNewSeq(String[] arrSubSeq, Map<String, String> classmap, Map<String, String> slotmap, StringBuilder sbSubSeq) {
		for (int i = 0; i < arrSubSeq.length; i++) {
			if (i == 0 || i == arrSubSeq.length - 2)
				arrSubSeq[i] = classmap.get(arrSubSeq[i]);					
			else 						
				arrSubSeq[i] = slotmap.get(arrSubSeq[i]);
			
			if (i == 0)
				sbSubSeq.append(arrSubSeq[i]);
			else
				sbSubSeq.append(String.format("_%s", arrSubSeq[i]));
		}
	}
	
	public static void ReplaceDevTag(CommandMap commandMap) {		
		List<String> arrSplitTag = new ArrayList<String>();
		arrSplitTag.add("sentence");
		arrSplitTag.add("SLU1");
		arrSplitTag.add("SLU2");
		arrSplitTag.add("SLU3");
		arrSplitTag.add("pattern");
		arrSplitTag.add("db_search");
		arrSplitTag.add("system_output");
		arrSplitTag.add("user_input");
		arrSplitTag.add("MA");
		arrSplitTag.add("NE");
		
		for (String item : arrSplitTag) {
			DevTagRecursive(commandMap, String.format("<%s>", item), String.format("</%s>", item), 0);
		}		
	}
	private static void DevTagRecursive(CommandMap commandMap, String srep, String erep, int idx) {
		String str = commandMap.get("STR").toString().replaceAll("<br>", "<br/>");
		int s_idx = str.indexOf(srep, idx);
		int e_idx = str.indexOf(erep, s_idx);
		
		if(s_idx > -1 && e_idx > -1) {
			String strtemp = str.substring(s_idx + srep.length(), e_idx);						
			if (strtemp.indexOf('<') > -1 || strtemp.indexOf('&') > -1) {				
				str = str.replace(String.format("%s%s%s", srep, strtemp, erep), String.format("%s%s%s", srep, strtemp.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("&LT;", "&lt;").replaceAll("&GT;", "&gt;"), erep));				
				commandMap.put("STR", str);
			}
			DevTagRecursive(commandMap, srep, erep, e_idx);
		}
	}
	
	public static String GetDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
}
