package first.view.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import first.common.dao.AbstractDAO;

@Repository("viewDAO")
public class ViewDAO extends AbstractDAO {
	public void CreateTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.createtable", map);
	}
	
	public void DropTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.droptable", map);
	}
	
	public void CreateColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.createcolumn", map);
	}
	
	public void AlterColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.altercolumn", map);
	}
	
	public void DropColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletecolumn", map);
	}
	
	public void insertProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertProject", map);	
	}
	
	public void insertProject2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertProject2", map);	
	}
	
	public void insertTutorProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertTutorProject", map);	
	}
	
	public void updatePorts(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateports", map);	
	}
	
	public void updatePorts2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateports2", map);	
	}
	
	public void insertSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotclass", map);	
	}
	
	public void insertBasicIntents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertBasicIntents", map);	
	}

	public void insertBasicIntentsEnglish(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertBasicIntentsEnglish", map);
	}
	
	public void insertSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslot", map);	
	}
	
	public void insertSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotcolor", map);	
	}
	
	public void insertSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslot2", map);	
	}
	
	public void insertSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotdefine", map);	
	}
	
	public void updateSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateSlotDefine", map);	
	}
	
	public void insertSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslottypeclass", map);	
	}
	
	public void deleteSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotdefine", map);
	}	
	
	public void deleteSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslottypeclass", map);
	}	
	
	public void deleteSlotTypeClassAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslottypeclassall", map);
	}
	
	public void insertSlotSub(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertSlotSub", map);	
	}
	
	public void updateSlotSub(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateSlotSub", map);	
	}
	
	public void updateSlotSub2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateSlotSub2", map);	
	}
	
	public void deleteSlotSub(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteSlotSub", map);
	}	
	
	public void deleteSlotSubClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteSlotSubClass", map);
	}
	
	public void deleteProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteproject", map);	
	}
	
	public void deleteTutorProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteTutorproject", map);	
	}
	
	public void deleteSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotclass", map);	
	}
	
	public void deleteSlotClassSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotclassseq", map);	
	}	

	public void deleteSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslot", map);	
	}
	
	public void deleteReferSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteReferSlotClassSeq", map);	
	}
	
	public void updateProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateproject", map);	
	}
	
	public void updateProjectDeleteFlag(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateprojectdeleteflag", map);	
	}
	
	public void updateProjectLearnCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateProjectLearnCheck", map);	
	}
	
	public void updateTutorProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateTutorproject", map);	
	}
	
	public void updateTutorProjectDeleteFlag(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateTutorprojectdeleteflag", map);	
	}
	
	public void updateSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateslotclass", map);	
	}
	
	public void updateSlotClassNewCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateSlotClassNewCheck", map);	
	}
	
	public void updateSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslot", map);
	}
	
	public void updateSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslot2", map);
	}
	
	public void updateSlotSort(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslotsort", map);
	}
	
	public void deleteSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobject", map);
	}
	
	public void deleteSlotObjectAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobjectAll", map);
		delete("view.deleteslotobjectdetailAll", map);
	}
	
	public void deleteSlotObjectDetailSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobjectdetailSeq", map);
	}
	
	public void deleteSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobjectdetail", map);
	}
	
	public void updateSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.updateslotobject", map);
	}
	
	public void updateSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.updateslotobjectdetail", map);
	}
	
	public void insertSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotobject", map);
	}
	
	public void insertSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotobjectdetail", map);
	}	
	
	public void insertChatbotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertchatbotobject", map);
	}
	
	public void updateChatbotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.updatechatbotobject", map);
	}
	
	public void deleteChatbotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletechatbotobject", map);
	}

	public void insertChatbotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertchatbotobjectdetail", map);
	}
	
	public void deleteChatbotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletechatbotobjectdetail", map);
	}
			
	public void insertChatbotTrain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertchatbottrain", map);
	}
	
	public void insertChatbotTrainDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertchatbottraindetail", map);
	}
	
	public void deleteChatbotTrain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletechatbottrain", map);
	}
	
	public void deleteChatbotTrainDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletechatbottraindetail", map);
	}
	
	public void deleteChatbotTrainDetailAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletechatbottraindetailall", map);
	}
	
	public void updateChatbotTrainDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatechatbottraindetail", map);
	}
	
	public void insertSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotinstans", map);
	}
	
	public void insertSlotInstans2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotinstans2", map);
	}
	
	public void updateSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslotinstans", map);
	}
	
	public void deleteSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotinstans", map);
	}
	
	public void deleteSlotInstansAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotinstansAll", map);
	}
	
	public void insertSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotMapping", map);
	}	
	
	public void updateSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslotMapping", map);
	}
	
	public void deleteSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotMapping", map);
	}
	
	public void insertIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintent", map);
	}
	
	public void updateIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateintent", map);
	}
	
	public void insertIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentusersay", map);
	}
	
	public void updateIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateintentusersay", map);
	}
	
	public void insertIntentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentintention", map);
	}
	
	public void insertIntentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentobject", map);
	}
	
	public void insertIntentObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentobjectdetail", map);
	}	
	
	public void deleteIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintent", map);
	}
	
	public void deleteIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentusersay", map);
	}
	
	public void deleteIntentUserSayDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentusersaydetail", map);
	}
	
	public void deleteIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsusersays", map);
	}
	
	public void updateCopyIntentUsersay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatecopyintentusersay", map);
	}
	
	public void deleteIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsintention", map);
	}
	
	public void deleteIntentsIntention2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsintention2", map);
	}
	
	public void updateIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateintentsintention", map);
	}
	
	public void deleteIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobject", map);
	}
	
	public void deleteIntentsObject2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobject2", map);
	}
	
	public void deleteIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobjectdetail", map);
	}
	
	public void deleteIntentsObjectDetail2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobjectdetail2", map);
	}
	
	public void insertReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertReplaceIntent", map);
	}
	
	public void updateReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateReplaceIntent", map);
	}
	
	public void deleteReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteReplaceIntent", map);
	}
	
	public void insertEditIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertEditIntent", map);
	}
	
	public void updateEditIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateEditIntent", map);
	}
	
	public void insertAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagent", map);
	}

	public void insertAgentWithPosition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertAgentWithPosition", map);
	}

	public void insertAgent2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagent2", map);
	}
	
	public void insertAgentFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentfillingslot", map);
	}
	
	public void insertAgentFillingSlotIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentfillingslotintention", map);
	}
	
	public void insertAgentIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentintention", map);
	}
	
	public void insertAgentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentobject", map);
	}
	
	public void insertAgentObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentobjectdetail", map);
	}
	
	public void insertAgentMonitoring(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentMonitoring", map);
	}
	
	public void insertAgentActions(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentActions", map);
	}
	
	public void deleteAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagent", map);
	}
	
	public void deleteAgentFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentfillingslot", map);
	}
	
	public void deleteAgentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintention", map);
	}
	
	public void deleteAgentIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintentsobject", map);
	}
	
	public void deleteAgentIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintentsobjectdetail", map);
	}
	
	public void deleteAgentGraph2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph2", map);
	}
	
	public void deleteAgentGraph3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph3", map);
	}
	
	public void deleteAgentMonitoring(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentMonitoring", map);
		delete("view.deleteagentAction", map);
	}
	
	public void updateAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagent", map);
	}
	
	public void updateAgentSortNo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagentsortno", map);
	}
	
	public void updateAgentSortNo2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagentsortno2", map);
	}
	
	public void updateAgentSortNo3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagentsortno3", map);
	}
	
	public void updateAgentPosition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagentposition", map);
	}
	
	public void insertAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentgraph", map);
	}
	
	public void deleteAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph", map);
	}
	
	public void insertUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertuser", map);
	}
	
	public void updateUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateuser", map);
	}
	
	public void updateUserPassword(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateUserPassword", map);
	}
	
	public void updateUserLastLogin(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateUserLastLogin", map);
	}
	
	public void deleteUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteuser", map);
	}
	
	public void insertLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertlua", map);
	}
	
	public void updateLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatelua", map);
	}
	
	public void insertIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.insertIntention", map);
	}
	
	public void updateIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateIntention", map);
	}
	
	public void updateUserSayIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateUserSayIntention", map);
	}
	
	public void updateIntentionGroup(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateIntentionGroup", map);
	}
	
	public void deleteIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.deleteIntention", map);
	}
	
	public void deleteLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletelua", map);
	}
	
	public void deleteLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletelanguage", map);
	}
	
	public void insertUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertuserdomain", map);
	}
	
	public void deleteUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteuserdomain", map);
	}
	
	public void updateEncoding(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateEncodingReset", map);
		update("view.updateEncoding", map);
	}
	
	public void updateCountMng(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.updateCountMng", map);
	}
	
	public void insertTutorUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertTutoruserdomain", map);
	}
	
	public void deleteTutorUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteTutoruserdomain", map);
	}
	
	public void insertReferClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertreferclass", map);
	}
	
	public void deleteReferClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletereferclass", map);
	}
	
	public void insertLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertlanguage", map);
	}
	
	public void updateLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatelanguage", map);
	}
			
	public void copyIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.copyintent", map);	
	}
	
	public void insertSampleDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertSampleDomain", map);	
	}
	
	public void deleteSampleDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.deleteSampleDomain", map);	
	}
	
	public void insertTutorDialogMap(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertTutorDialogMap", map);
	}
	
	public void deleteTutorDialogMap(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.deleteTutorDialogMap", map);
	}
	
	public void updateTutorDialogMap(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateTutorDialogMap", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectProjectSa(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectListSa", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectProjectGu(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectListGu", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectProjectDelete(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectListDelete", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectCreateProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectCreateProjectList", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectProjectName(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectName", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectSampleDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.SelectSampleDomain", map);	
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorProjectSa(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorProjectListSa", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorProjectGu(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorProjectListGu", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorProjectDelete(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorProjectListDelete", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorCreateProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorCreateProjectList", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorProjectName(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorProjectName", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectTutorDialogMap(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectTutorDialogMap", map);	
	}
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClassRefer(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclassrefer", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClassAndRefer(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclassAndrefer", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass3", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClassName(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclassname", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlot3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectslot3", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot4(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot4", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot5(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot5", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot6(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot6", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotTypeCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotTypeCheck", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotTypeClassCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotTypeClassCheck", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotSub(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotSub", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotSub2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotSub2", map);	
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotSub3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotSub3", map);	
	}
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotintents", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotdefine", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotDefine2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotdefine2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotAddition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectSlotAddition", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFunction(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectfunction", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotType(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslottype", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobject", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObjectPaging(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectslotobjectPaging", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotObjectInsertChk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectMap("view.selectslotobjectInsertChk", map, "OBJECT_NAME");	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectdetail", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotObjectDetailInsertChk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectMap("view.selectslotobjectDetailInsertChk", map, "OBJECT_NAME");	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectslotobjectUpdatechk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectUpdatechk", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectslotobjectDetailUpdatechk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectDetailUpdatechk", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectdetailexcel", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectChatbotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectchatbotobject", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectChatbotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectchatbotobjectdetail", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectChatbotTrainDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectchatbottraindetail", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) {
		// TODO Auto-generated method stub		//
		return (List<Map<String, Object>>)selectPagingList("view.selectslotinstansces", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclassintances2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub		//
		return (List<Map<String, Object>>)selectPagingList("view.selectSlotMapping", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotMapping2(Map<String, Object> map) {
		// TODO Auto-generated method stub		//
		return (List<Map<String, Object>>)selectPagingList("view.selectSlotMapping2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslottypeclass", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotTypeClass2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslottypeclass2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectvslotobject", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintens", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectintensusersays", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectintensusersays2", map);
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays3", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays4(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays4", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays5(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays5", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays6(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays6", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays7(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays7", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensintention", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobject", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobjectdtl", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectReplaceIntent", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectReplaceIntent2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectReplaceIntent2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObjectDtl2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobjectdtl2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObjectDtl3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobjectdtl3", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsIntention2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintentsintention2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsIntention3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintentsintention3", map);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectEncoding(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectencoding", map);	
	}
		
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectIntention", map);	
	}
		
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectfillingslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagents", map);	
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentsCopy(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentscopy", map);	
	}
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectagent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentslotfilling", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentintention", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentobject", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentobjectdtl", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentObjectdtl2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentobjectdtl2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentgraph", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanNextAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleannextagent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentMornitoring(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentMornitoring", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentActions(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selecatagentActions", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSystemDAtype(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSystemDAtype", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLibGroup1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglibgroup1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1_1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1_1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1_2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1_2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1Obj(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1obj", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1ObjDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1objdtl", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2Main(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2main", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2UserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2usersay", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2_1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2_1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2_2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2_2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2Obj(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2obj", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2ObjDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2objdtl", map);	
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib3Main(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib3main", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib3", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanTaskSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleantaskslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectleanDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDatype(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandatype", map);	
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSqlLiteSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectsqlliteslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectsqlLiteInstance(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectsqlliteinstance", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuseridchk", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserApiKeyChk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuserapikeychk", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectuserlist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserAllList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectuserAllList", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLuaList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectlualist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectRererenceClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectreferenceclass", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectlua", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectuser", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> loginUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.loginuser", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectintent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectEditIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectEditIntent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotColorCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectslotcolorcheck", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotColorCheck2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectSlotColorCheck2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectslotcolor", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotColor2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotcolor2", map);	
	}
		
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuserdomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuserdomainlist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectMigration(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectMigration", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectMigration2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectMigration2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> copyTask(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.copytask", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> copyDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.copydomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectDomainVersion(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectdomainversion", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectlanguage", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectCountMng(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectCountMng", map);	
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectCountCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectCountCheck", map);	
	}

	public Map<String, Object> getBizChatbotUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.getBizChatbotUser", map);
	}
}
