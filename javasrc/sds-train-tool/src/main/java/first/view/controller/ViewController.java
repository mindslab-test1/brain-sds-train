package first.view.controller;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import first.common.common.CommandMap;
import first.common.util.AES256Util;
import first.common.util.ColorObject;
import first.common.util.CommonUtils;
import first.common.util.Ports;
import first.common.util.Unescape;
import first.view.service.ViewService;


@Controller
public class ViewController {	
	Logger log = Logger.getLogger(this.getClass());	
	String key = "etri-dialog-key!@"; //key 16자 이상
	
	@Resource(name="viewService")
	private ViewService viewService;
	
	private ModelAndView SetMenu(ModelAndView mv, CommandMap commandMap, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String depth1 = "0";
		String depth2 = "0";
    	String depth3 = "0";
    	
		if (mv.getViewName().contains("agent") == true) {
			depth1 = "1";									
		} else if (mv.getViewName().contains("slot") == true) {
			depth1 = "2";
		} else if (mv.getViewName().contains("useruttr") == true) {
			depth1 = "3";
		} else if (mv.getViewName().contains("know") == true) {
			depth1 = "4";
		} else if (mv.getViewName().contains("usermng") == true) {
			depth1 = "5";
		} else if (mv.getViewName().contains("sample") == true) {
			depth1 = "6";
		} else if (mv.getViewName().contains("logs") == true) {
			depth1 = "7";
		} 		
		
		//서브 메뉴 있을 경우 1뎁스는 무조건 1
		if(commandMap.get("depth2") != null && commandMap.get("depth3") != null) {
			if(commandMap.get("depth2").toString().equals("0") && commandMap.get("depth3").toString().equals("0")) {
				depth1 = commandMap.get("depth1").toString();
			}
			else {
				depth1 = "1";
			}
			depth2 = (String)commandMap.get("depth2");
			depth3 = (String)commandMap.get("depth3");			
		}
		
		String LoginUserType = "";
		String LoginUserSeq = "";
		Boolean bLoginChk = true;
		Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("USER_TYPE")) {                	                	
                	mv.addObject("USER_TYPE", cookies[i].getValue());                	
                	LoginUserType = cookies[i].getValue();
                	bLoginChk = false;
				}
                if (cookies[i].getName().equals("USER_SEQ")) {
                	LoginUserSeq = cookies[i].getValue();
				}
            }
        }
    	    	
    	if (bLoginChk) {    		
    		//mv = new ModelAndView("redirect:/view/logout.go");
    		request.getSession().setAttribute("ProjectName", "선택된 도메인이 없습니다.");
    		return mv;
		}
    	
		List<Map<String, Object>> prjlist = null;		
		
		prjlist = SetProject(0, request);
		
		if (request.getSession().getAttribute("ServerPort") == null) {
			request.getSession().setAttribute("ServerPort", viewService.GetPort(Ports.arrUsingPort));
		}
		
		List<Map<String, Object>> submenulist = new ArrayList<Map<String, Object>>();
		if(request.getSession().getAttribute("ProjectNo") == null && prjlist.size() > 0) {
			request.getSession().setAttribute("ProjectNo", prjlist.get(0).get("SEQ_NO"));
			request.getSession().setAttribute("ProjectName", prjlist.get(0).get("PROJECT_NAME"));
			request.getSession().setAttribute("ProjectLangNo", prjlist.get(0).get("LANG_SEQ_NO"));
			//request.getSession().setAttribute("ServerPort", prjlist.get(0).get("SERVER_PORT"));
		}
		else if(request.getSession().getAttribute("ProjectNo") != null && prjlist.size() == 0) {
			request.getSession().removeAttribute("ProjectNo");
			request.getSession().removeAttribute("ProjectName");
			request.getSession().removeAttribute("ProjectLangNo");
			//request.getSession().removeAttribute("ServerPort");
		}
		else {
			Boolean bPrjChk = false;
			String PrjUserSeq = "";
			for (Iterator iterator = prjlist.iterator(); iterator.hasNext();) {
				Map<String, Object> prjitem = (Map<String, Object>) iterator.next();
				
				/*if (commandMap.containsKey("PROJECT_NAME")) {
					if (prjitem.get("PROJECT_NAME").toString().equals(commandMap.get("PROJECT_NAME").toString())) {
						mv.addObject("SERVER_PORT", prjitem.get("SERVER_PORT"));
					}
				}*/
				
				if (LoginUserType.equals("SA")) {			
					if (prjitem.get("PROJECT_NAME").toString().equals(request.getSession().getAttribute("ProjectName").toString())) {						
						bPrjChk = true;						
						break;
					}
				}
				else {
					if (prjitem.get("PROJECT_NAME").toString().equals(request.getSession().getAttribute("ProjectName").toString())
							&& prjitem.get("USER_SEQ_NO").toString().equals(LoginUserSeq)) {
						request.getSession().setAttribute("ProjectNo", prjitem.get("SEQ_NO"));
						request.getSession().setAttribute("ProjectName", prjitem.get("PROJECT_NAME"));
						request.getSession().setAttribute("ProjectLangNo", prjitem.get("LANG_SEQ_NO"));
						bPrjChk = true;
						PrjUserSeq = prjitem.get("USER_SEQ_NO").toString();
						
						if(cookies != null){            
				            for(int i=0; i < cookies.length; i++){
				            	if (cookies[i].getName().equals("ProjectUserSeq")) {									
									cookies[i].setValue(PrjUserSeq);					
								}
				            }
				        }
						
						Cookie prjuserseq = new Cookie("ProjectUserSeq", PrjUserSeq);    	
						prjuserseq.setMaxAge(60 * 60 * 24 * 90);				
						prjuserseq.setPath("/");				
						response.addCookie(prjuserseq);
						break;
					}
				}				
			}
			if (!bPrjChk) {
				request.getSession().removeAttribute("ProjectNo");
				request.getSession().setAttribute("ProjectName", "선택된 도메인이 없습니다.");
			}
		}
		
		if (request.getSession().getAttribute("ProjectNo") != null) {
			int prjseq = Integer.parseInt(request.getSession().getAttribute("ProjectNo").toString());
			CommandMap cmmap = new CommandMap();
			cmmap.put("PROJECT_SEQ", prjseq);
			submenulist = viewService.selectAgents(cmmap.getMap());			
		}	
		
		if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
			mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));	
		}
				
		mv.addObject("depth1", depth1);
		mv.addObject("depth2", depth2);
		mv.addObject("depth3", depth3);		
		mv.addObject("prjlist", prjlist);
		mv.addObject("submenu", submenulist);
		return mv;
	}
	
	private ModelAndView SetTutorMenu(ModelAndView mv, CommandMap commandMap, HttpServletRequest request) throws Exception {		
		List<Map<String, Object>> prjlist = null;		
		
		prjlist = SetTutorProject(0, request);
		
		if(prjlist.size() == 0) {
			request.getSession().removeAttribute("TutorProjectNo");
			request.getSession().removeAttribute("TutorProjectName");
			request.getSession().removeAttribute("TutorProjectLangNo");
			//request.getSession().removeAttribute("ServerPort");
		}
		else {			
			Boolean bPrjChk = false;
			if (request.getSession().getAttribute("TutorProjectName") != null) {					
				for (Iterator iterator = prjlist.iterator(); iterator.hasNext();) {
					Map<String, Object> prjitem = (Map<String, Object>) iterator.next();
									
					if (prjitem.get("PROJECT_NAME").toString().equals(request.getSession().getAttribute("TutorProjectName").toString())) {
						bPrjChk = true;
						break;
					}
				}
			}
			
			if (!bPrjChk) {
				request.getSession().removeAttribute("TutorProjectNo");
				request.getSession().setAttribute("TutorProjectName", "선택된 도메인이 없습니다.");
			}
		}		
		
		mv.addObject("prjlist", prjlist);
		
		if (request.getSession().getAttribute("ServerPort") == null) {
			mv = new ModelAndView("redirect:/view/logout.go");
		}
		
		return mv;
	}
		
	private List<Map<String, Object>> SetProject(int SeqNo, HttpServletRequest request) throws Exception {
		CommandMap cmmap = new CommandMap();
		cmmap.put("SEQ_NO", SeqNo);
		
		Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_SEQ")) {
                	cmmap.put("USER_SEQ", cookies[i].getValue());
                	request.getSession().setAttribute("USER_SEQ", Unescape.unescape(cookies[i].getValue()));
				}
                else if (cookies[i].getName().equals("USER_TYPE")) {
                	cmmap.put("USER_TYPE", cookies[i].getValue());
				}
                else if (cookies[i].getName().equals("USER_NAME")) {
                	try {
										String name = URLDecoder.decode(cookies[i].getValue(), "UTF-8");
										request.getSession().setAttribute("USER_NAME", name);
									} catch (Exception e) {
										request.getSession().setAttribute("USER_NAME", Unescape.unescape(cookies[i].getValue()));
									}
                }
                else if (cookies[i].getName().equals("ProjectNo")) {               
                	request.getSession().setAttribute("ProjectNo", cookies[i].getValue());
                }
                else if (cookies[i].getName().equals("ProjectName")) {
                	request.getSession().setAttribute("ProjectName", cookies[i].getValue());
                }
                else if (cookies[i].getName().equals("ProjectLangNo")) {
                	request.getSession().setAttribute("ProjectLangNo", cookies[i].getValue());
                }
                /*else if (cookies[i].getName().equals("ServerPort")) {
                	request.getSession().setAttribute("ServerPort", cookies[i].getValue());
                }*/
                else if (cookies[i].getName().equals("UNIQID")) {
                	request.getSession().setAttribute("UNIQID", cookies[i].getValue());
                }
            }
        }		    	
		List<Map<String, Object>> prjlist = viewService.selectProject(cmmap.getMap());		
		return prjlist;
	}
		
	
	private List<Map<String, Object>> SetTutorProject(int SeqNo, HttpServletRequest request) throws Exception {
		CommandMap cmmap = new CommandMap();
		cmmap.put("SEQ_NO", SeqNo);
		
		Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_SEQ")) {
                	cmmap.put("USER_SEQ", cookies[i].getValue());
                	request.getSession().setAttribute("USER_SEQ", Unescape.unescape(cookies[i].getValue()));
				}
                else if (cookies[i].getName().equals("USER_TYPE")) {
                	cmmap.put("USER_TYPE", cookies[i].getValue());
				}
                else if (cookies[i].getName().equals("USER_NAME")) {                	
                	request.getSession().setAttribute("USER_NAME", Unescape.unescape(cookies[i].getValue()));
                }
                else if (cookies[i].getName().equals("TutorProjectNo")) {               
                	request.getSession().setAttribute("TutorProjectNo", cookies[i].getValue());
                }
                else if (cookies[i].getName().equals("TutorProjectName")) {
                	request.getSession().setAttribute("TutorProjectName", cookies[i].getValue());
                }
                else if (cookies[i].getName().equals("TutorProjectLangNo")) {
                	request.getSession().setAttribute("TutorProjectLangNo", cookies[i].getValue());
                }
                else if (cookies[i].getName().equals("UNIQID")) {
                	request.getSession().setAttribute("UNIQID", cookies[i].getValue());
                }
            }
        }		    	
		List<Map<String, Object>> prjlist = viewService.selectTutorProject(cmmap.getMap());		
		return prjlist;
	}
	
	@RequestMapping(value="/view/setprojectlist.do")
    public ModelAndView setProjectList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{		
		ModelAndView mv = new ModelAndView("redirect:/view/agentlist.do");
		
		Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
            	if (cookies[i].getName().equals("ProjectNo")) {									
					cookies[i].setValue(commandMap.get("SEQ_NO").toString());					
				}
				else if (cookies[i].getName().equals("ProjectName")) {
					cookies[i].setValue(commandMap.get("PROJECT_NAME").toString());					
				}
				else if (cookies[i].getName().equals("ProjectLangNo")) {
					cookies[i].setValue(commandMap.get("LANG_SEQ_NO").toString());					
				}
				else if (cookies[i].getName().equals("ProjectUserSeq")) {
					cookies[i].setValue(commandMap.get("PROJECT_USER_SEQ").toString());
				}
				else if (cookies[i].getName().equals("ProjectApiKey")) {
					if (commandMap.get("API_KEY").toString().isEmpty())
						cookies[i].setValue("null");
					else
						cookies[i].setValue(commandMap.get("API_KEY").toString());
					
				}
				/*else if (cookies[i].getName().equals("ServerPort")) {
					cookies[i].setValue(commandMap.get("SERVER_PORT").toString());					
				}*/
            }
        }
    	
		Cookie prjno = new Cookie("ProjectNo", commandMap.get("SEQ_NO").toString()); 
		Cookie prjname = new Cookie("ProjectName", commandMap.get("PROJECT_NAME").toString());
		Cookie prjlanseq = new Cookie("ProjectLangNo", commandMap.get("LANG_SEQ_NO").toString());
		Cookie prjuserseq = new Cookie("ProjectUserSeq", commandMap.get("PROJECT_USER_SEQ").toString());
		Cookie prjapikey = new Cookie("ProjectApiKey", commandMap.get("API_KEY").toString());
		//Cookie prjport = new Cookie("ServerPort", commandMap.get("SERVER_PORT").toString());
		prjno.setMaxAge(60 * 60 * 24 * 90);
		prjname.setMaxAge(60 * 60 * 24 * 90);
		prjlanseq.setMaxAge(60 * 60 * 24 * 90);
		prjuserseq.setMaxAge(60 * 60 * 24 * 90);
		prjapikey.setMaxAge(60 * 60 * 24 * 90);
		//prjport.setMaxAge(60 * 60 * 24 * 90);
		prjno.setPath("/");
		prjname.setPath("/");
		prjlanseq.setPath("/");
		prjuserseq.setPath("/");
		prjapikey.setPath("/");
		//prjport.setPath("/");
		response.addCookie(prjno);
		response.addCookie(prjname);
		response.addCookie(prjlanseq);
		response.addCookie(prjuserseq);
		response.addCookie(prjapikey);
		//response.addCookie(prjport);
								
		
		request.getSession().setAttribute("ProjectNo", commandMap.get("SEQ_NO"));
		request.getSession().setAttribute("ProjectName", commandMap.get("PROJECT_NAME"));
		request.getSession().setAttribute("ProjectLangNo", commandMap.get("LANG_SEQ_NO"));
		//request.getSession().setAttribute("ServerPort", commandMap.get("SERVER_PORT"));
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/settutorprojectlist.do")
    public ModelAndView setTutorProjectList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{		
		ModelAndView mv = new ModelAndView("redirect:/view/dialogmap.do");
		
		Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
            	if (cookies[i].getName().equals("TutorProjectNo")) {									
					cookies[i].setValue(commandMap.get("SEQ_NO").toString());					
				}
				else if (cookies[i].getName().equals("TutorProjectName")) {
					cookies[i].setValue(commandMap.get("PROJECT_NAME").toString());					
				}
				else if (cookies[i].getName().equals("TutorProjectLangNo")) {
					cookies[i].setValue(commandMap.get("LANG_SEQ_NO").toString());					
				}
				else if (cookies[i].getName().equals("TutorProjectUserSeq")) {
					cookies[i].setValue(commandMap.get("PROJECT_USER_SEQ").toString());					
				}
            }
        }    	
    	
		Cookie prjno = new Cookie("TutorProjectNo", commandMap.get("SEQ_NO").toString()); 
		Cookie prjname = new Cookie("TutorProjectName", commandMap.get("PROJECT_NAME").toString());
		Cookie prjlanseq = new Cookie("TutorProjectLangNo", commandMap.get("LANG_SEQ_NO").toString());
		Cookie prjuserseq = new Cookie("TutorProjectUserSeq", commandMap.get("PROJECT_USER_SEQ").toString());
		//Cookie prjport = new Cookie("ServerPort", commandMap.get("SERVER_PORT").toString());
		prjno.setMaxAge(60 * 60 * 24 * 90);
		prjname.setMaxAge(60 * 60 * 24 * 90);
		prjlanseq.setMaxAge(60 * 60 * 24 * 90);
		prjuserseq.setMaxAge(60 * 60 * 24 * 90);
		//prjport.setMaxAge(60 * 60 * 24 * 90);
		prjno.setPath("/");
		prjname.setPath("/");
		prjlanseq.setPath("/");
		prjuserseq.setPath("/");
		//prjport.setPath("/");
		response.addCookie(prjno);
		response.addCookie(prjname);
		response.addCookie(prjlanseq);
		response.addCookie(prjuserseq);
		//response.addCookie(prjport);
								
		
		request.getSession().setAttribute("TutorProjectNo", commandMap.get("SEQ_NO"));
		request.getSession().setAttribute("TutorProjectName", commandMap.get("PROJECT_NAME"));
		request.getSession().setAttribute("TutorProjectLangNo", commandMap.get("LANG_SEQ_NO"));
		//request.getSession().setAttribute("ServerPort", commandMap.get("SERVER_PORT"));
		
    	return mv;
    }

	@RequestMapping(value="/view/projectlist.do")
    public ModelAndView openProjectdList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectList");    	
    	
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){                
                if (cookies[i].getName().equals("USER_TYPE")) {
                	commandMap.put("USER_TYPE", cookies[i].getValue());
                	mv.addObject("USER_TYPE", cookies[i].getValue());
				}
            }
        }
    	if (commandMap.get("USER_TYPE").toString().equals("SA")) {
    		List<Map<String, Object>> delprjlist = viewService.SelectProjectDelete(commandMap.getMap());		
    		mv.addObject("delprjlist", delprjlist);
		}
    	
    	Map<String, Object> item = viewService.selectCountMng(commandMap.getMap());
    	mv.addObject("item", item);
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/projectwrite.do")
    public ModelAndView openProjectdWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectWrite");
    	
    	CommandMap prjCmd = new CommandMap();
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){    		
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_SEQ")) {                	
                	prjCmd.put("USER_SEQ", cookies[i].getValue());
                	mv.addObject("USER_SEQ_NO", cookies[i].getValue());
				}
            }                                                
        }
    	
    	List<Map<String, Object>> prjlist = viewService.SelectCreateProject(prjCmd.getMap());
    	commandMap.put("LANG_SEQ_NO", 0);
    	List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    	List<Map<String, Object>> samplelist = viewService.SelectSampleDomain(commandMap.getMap());		
		
    	mv.addObject("SAMPLE_LIST", samplelist);		
    	mv.addObject("VERSION_LIST", prjlist);
    	mv.addObject("LANG_LIST", langlist);
    	
    	return SetMenu(mv, commandMap, request, response);
    }
		
	@RequestMapping(value="/view/projectupdate.do")
    public ModelAndView openProjectdUpdate(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectWrite");
    
    	List<Map<String, Object>> prjlist = SetProject(Integer.parseInt(commandMap.get("SEQ_NO").toString()), request);
    	
    	if(prjlist.size() > 0) {
    		mv.addObject("SEQ_NO", prjlist.get(0).get("SEQ_NO"));
    		mv.addObject("PROJECT_NAME", prjlist.get(0).get("PROJECT_NAME"));
    		mv.addObject("DESCRIPTION", prjlist.get(0).get("DESCRIPTION"));
    		mv.addObject("USER_SEQ_NO", prjlist.get(0).get("USER_SEQ_NO"));
    		mv.addObject("FROM_SEQ_NO", prjlist.get(0).get("FROM_SEQ_NO"));    		
    		mv.addObject("FROM_PROJECT_NAME", prjlist.get(0).get("FROM_PROJECT_NAME"));
    		mv.addObject("LANG_SEQ_NO", prjlist.get(0).get("LANG_SEQ_NO"));
    		mv.addObject("CHATBOT_USE_FLAG", prjlist.get(0).get("CHATBOT_USE_FLAG"));
    		mv.addObject("CHATBOT_DATA_FLAG", prjlist.get(0).get("CHATBOT_DATA_FLAG"));
    		mv.addObject("FILTER_VALUE", prjlist.get(0).get("FILTER_VALUE"));
    		List<Map<String, Object>> versionlist = viewService.selectDomainVersion(commandMap.getMap());
    		commandMap.put("LANG_SEQ_NO", 0);
    		List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    		
    		mv.addObject("VERSION_LIST", versionlist);
    		mv.addObject("LANG_LIST", langlist);
    		
    	}

    	return SetMenu(mv, commandMap, request, response);
    }	
	
	@RequestMapping(value="/view/insertproject.do")
    public ModelAndView insertProject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/setprojectlist.do");    	
    	
    	if (commandMap.get("SEQ_NO").toString().isEmpty()) {      		
    		commandMap.put("TYPE", "I");
    		CommandMap cmmap = new CommandMap();
    		
    		Cookie[] cookies = request.getCookies();
        	if(cookies != null){
        		
                for(int i=0; i < cookies.length; i++){
                    if (cookies[i].getName().equals("USER_SEQ")) {
                    	commandMap.put("USER_SEQ", cookies[i].getValue());
                    	cmmap.put("USER_SEQ", cookies[i].getValue());
    				}
                }
            }        	        	        
        	request.getSession().setAttribute("ProjectLangNo", commandMap.get("LANG_SEQ_NO"));
        	
    		if (!commandMap.containsKey("FROM_SEQ_NO")) {
    			if (!commandMap.get("DEL_SEQ_NO").toString().equals("0")) {
    				CommandMap delcmmap = new CommandMap();
    				delcmmap.put("SEQ_NO", commandMap.get("DEL_SEQ_NO"));
    				delcmmap.put("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
    				delcmmap.put("PROJECT_USER_SEQ", commandMap.get("PROJECT_USER_SEQ"));
    				delcmmap.put("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
					viewService.deleteProject(delcmmap.getMap(), request);
				}
    			
    			viewService.insertProject(commandMap.getMap(), request);
    			
    			if(cookies != null){
            		cmmap.put("PROJECT_SEQ", commandMap.get("SEQ_NO"));      
            		cmmap.put("AGENT_NAME", "END");
                    cmmap.put("RESET_SLOT", "0");
                    cmmap.put("TASK_TYPE", "essential");
                    cmmap.put("RELATED_SLOTS", "");
                	viewService.insertUserDomain(cmmap.getMap());            	
                	viewService.insertOnlyAgent(cmmap.getMap());
    			}
			}
    		else {
    			viewService.otherDomainCrate(commandMap.getMap(), request);
    		}
		}
		else {
			viewService.updateProject(commandMap.getMap(), request);
		}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	mv.addObject("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
    	mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
    	mv.addObject("PROJECT_USER_SEQ", commandMap.get("PROJECT_USER_SEQ"));
    	mv.addObject("API_KEY", commandMap.get("API_KEY"));
    	//SetProject(0, request);
    	return mv;
    }
	
	@RequestMapping(value="/view/selectprojectname.do")
    public ModelAndView selectProjectName(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		List<Map<String, Object>> prjlist = viewService.SelectProjectName(commandMap.getMap());
		
		Map<String, Object> item = viewService.selectCountMng(commandMap.getMap());
		commandMap.put("TABLE", "project");
		commandMap.put("WHERE", String.format("USER_SEQ_NO = %s and PARENT_SEQ_NO = 0 and DELETE_FLAG = 'N'", commandMap.get("USER_SEQ_NO")));
		Map<String, Object> cntchk = viewService.selectCountCheck(commandMap.getMap());
		if (Integer.parseInt(cntchk.get("CNT").toString()) >= Integer.parseInt(item.get("DOMAIN_COUNT").toString())) {
			mv.addObject("COUNT_ERROR", "Y");			
		}
		else
			mv.addObject("COUNT_ERROR", "N");
		
		mv.addObject("list", prjlist);
    	return mv;
    }
		
	@RequestMapping(value="/view/deletproject.do")
    public ModelAndView deleteProject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/projectlist.do");

    	if (commandMap.get("DELETE_FLAG").toString().equals("N")) {
    		commandMap.put("DELETE_FLAG", "Y");
    		viewService.updateProjectDeleteFlag(commandMap.getMap());
		}
    	else {
    		request.getSession().setAttribute("ProjectLangNo", commandMap.get("LANG_SEQ_NO"));
    		viewService.deleteProject(commandMap.getMap(), request);
    	}
    	//SetProject(0, request);
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }		
	
	@RequestMapping(value="/view/updateprojectdelflag.do")
    public ModelAndView updateProjectDelFlag(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/projectlist.do");    	
    	
    	viewService.updateProjectDeleteFlag(commandMap.getMap());
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/agentlist.do")
    public ModelAndView openAgentList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentList");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectAgentGraph(commandMap.getMap());
    	mv.addObject("conlist", list);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/openagentgraph.do")
    public ModelAndView openAgentGraph(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/AgentGraph");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectAgentGraph(commandMap.getMap());
    	mv.addObject("conlist", list);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updateagentposition.do")
    public ModelAndView updateAgentPosition(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = null;
    	if (commandMap.get("TYPE").toString().equals("M")) {
    		mv = new ModelAndView("redirect:/view/agentlist.do");
		}
    	else {
    		mv = new ModelAndView("redirect:/view/openagentgraph.do");
    	}
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.updateAgentPosition(commandMap.getMap());    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertagentgraph.do")
    public ModelAndView insertAgentGraph(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.insertAgentGraph(commandMap.getMap());
    	    	
    	mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteagentgraph.do")
    public ModelAndView deleteAgentGraph(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.deleteAgentGraph(commandMap.getMap());
    	mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/openfunctionwrite.do")
    public ModelAndView openFunctionWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/FunctionWork");    	
    	
    	/*if (request.getSession().getServletContext().getInitParameter("ShowV2") != null && request.getSession().getServletContext().getInitParameter("ShowV2").toString().equals("35000"))
    		mv = new ModelAndView("/popup/FunctionWork");
    	else {
    		String UserType = "";
    		Cookie[] cookies = request.getCookies();
        	if(cookies != null){            
                for(int i=0; i < cookies.length; i++) {
                    if (cookies[i].getName().equals("USER_TYPE")) {                    	
                    	UserType = cookies[i].getValue();
    				}
                }
            }
        	
        	if (UserType.equals("OA"))
        		mv = new ModelAndView("/popup/FunctionWork");	
        	else
        		mv = new ModelAndView("/popup/FunctionWork_old");
    	}*/
    	
    	mv.addObject("obj", commandMap.get("obj"));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/openactionwrite.do")
    public ModelAndView openActionWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/ActionWork");    	
    	mv.addObject("obj", commandMap.get("obj"));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/openfliingslotsearch.do")
    public ModelAndView openFillingSlotSearch(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/SlotSearch");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("obj", commandMap.get("obj"));
    	List<Map<String, Object>> list = viewService.selectFliingSlot(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("SLOT_LIST", list);    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/requestslotsearch.do")
    public ModelAndView RequestSlotSearch(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/SlotSearchRequest");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("obj", commandMap.get("obj"));
    	List<Map<String, Object>> list = viewService.selectFliingSlot(commandMap.getMap());
		List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("SLOT_LIST", list);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/openfunctionsearch.do")
    public ModelAndView openFunctionSearh(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/FunctionSearch");    	

    	mv.addObject("search", commandMap.get("FUNCTION_NAME")); 
    	mv.addObject("fnlist", viewService.selectFunction(commandMap.getMap()));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/opendatypesearch.do")
    public ModelAndView openDaTypeSearch(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/DaTypeSearch");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	List<Map<String, Object>> list;
    	if (commandMap.get("type").toString().equals("user"))			
    		list = viewService.selectIntents(commandMap.getMap());
    	else
    		list = viewService.selectSystemDAtype(commandMap.getMap());
	   	
    	mv.addObject("DA_LIST", list);    	
    	return mv;
    }
	
	@RequestMapping(value="/view/agentsub.do")
    public ModelAndView openAgentSub(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = null;
    	String depth3 = "0";
    	
		if(commandMap.get("depth2") != null && commandMap.get("depth3") != null) {
			depth3 = (String)commandMap.get("depth3");
			
			if(depth3.equals("1")) {				
				mv = new ModelAndView("redirect:/view/slotlist.do");		
			}
			else if(depth3.equals("2")) {
				mv = new ModelAndView("redirect:/view/useruttrlist.do");
			}
			else if(depth3.equals("3")) {
				mv = new ModelAndView("redirect:/view/agentedit.do");
			}						
		}
		else
			mv = new ModelAndView("redirect:/view/agentwrite.do");

    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/agentwrite.do")
    public ModelAndView openAgentWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentWrite");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	        	       
    	mv.addObject("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));    	
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/agentedit.do")
    public ModelAndView openAgentEdit(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentWrite");    	
            	
    	Map<String,Object> Agent = viewService.selectAgent(commandMap.getMap());
    	List<Map<String,Object>> SlotFillinglist = viewService.selectAgentSlotFilling(commandMap.getMap());
    	List<Map<String,Object>> Intentionlist = viewService.selectAgentIntention(commandMap.getMap());
    	List<Map<String,Object>> Objectlist = viewService.selectAgentObject(commandMap.getMap());
    	List<Map<String,Object>> ObjectDtllist = viewService.selectAgentObjectdtl(commandMap.getMap());
    	List<Map<String,Object>> Mornitoringlist = viewService.selectAgentMornitoring(commandMap.getMap());
    	List<Map<String,Object>> Actionslist = viewService.selectAgentActions(commandMap.getMap());
    	
    	List<Map<String,Object>> Intentionlist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Intentionlist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Intentionlist_3 = new ArrayList<Map<String, Object>>();
    	List<Integer> lstIn1 = new ArrayList<Integer>();
    	List<Integer> lstIn2 = new ArrayList<Integer>();
    	List<Integer> lstIn3 = new ArrayList<Integer>();
    	
    	for (Iterator iterator = Intentionlist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (map.get("INTENTION_TYPE").toString().equals("1")) {
				Intentionlist_1.add(map);
				lstIn1.add((int)map.get("SEQ_NO"));
			}
			else if (map.get("INTENTION_TYPE").toString().equals("2")) {
				Intentionlist_2.add(map);
				lstIn2.add((int)map.get("SEQ_NO"));
			}
			else {
				Intentionlist_3.add(map);
				lstIn3.add((int)map.get("SEQ_NO"));
			}
		}
    	
    	List<Map<String,Object>> Objectlist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Objectlist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Objectlist_3 = new ArrayList<Map<String, Object>>();
    	List<Integer> lstOb1 = new ArrayList<Integer>();
    	List<Integer> lstOb2 = new ArrayList<Integer>();
    	List<Integer> lstOb3 = new ArrayList<Integer>();
    	for (Iterator iterator = Objectlist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (lstIn1.contains(map.get("INTENTION_SEQ"))) {
				Objectlist_1.add(map);
				lstOb1.add((int)map.get("SEQ_NO"));
			}
			else if (lstIn2.contains(map.get("INTENTION_SEQ"))) {
				Objectlist_2.add(map);
				lstOb2.add((int)map.get("SEQ_NO"));
			}
			else {
				Objectlist_3.add(map);
				lstOb3.add((int)map.get("SEQ_NO"));
			}
		}
    	
    	List<Map<String,Object>> ObjectDtllist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> ObjectDtllist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> ObjectDtllist_3 = new ArrayList<Map<String, Object>>();
    	
    	Pattern p = Pattern.compile("<[a-zA-Zㄱ-�R0-9.=\\-_\"()+\\s]+>");
    	
    	for (Iterator iterator = ObjectDtllist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (lstOb1.contains(map.get("OBJECT_SEQ"))) {
				ReplaceTag(map, p);
				ObjectDtllist_1.add(map);
			}
			else if (lstOb2.contains(map.get("OBJECT_SEQ"))) {
				ReplaceTag(map, p);
				ObjectDtllist_2.add(map);				
			}
			else {
				ReplaceTag(map, p);
				ObjectDtllist_3.add(map);				
			}
		}
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));    
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	String Condition = "";
    	List<Map<String, Object>> nexttasklist = viewService.selectLeanNextAgent(commandMap.getMap());
		for (Iterator iterator2 = nexttasklist.iterator(); iterator2.hasNext();) {
			Map<String, Object> nextitem = (Map<String, Object>) iterator2.next();
			
			if (nexttasklist.size() == 1) {
				Condition = nextitem.get("_CONDITION").toString();
			}
			else {
				if (Condition.isEmpty()) {
					Condition = String.format("(%s)", nextitem.get("_CONDITION").toString());
				}
				else {
					Condition +=  String.format(" or (%s)", nextitem.get("_CONDITION").toString());
				}
			}
		}
		
		if (!Condition.isEmpty()) {
			Condition = "(" + Condition + ")";	
		}
		
    	mv.addObject("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));    	
    	mv.addObject("Agent", Agent);
    	mv.addObject("SlotFillinglist", SlotFillinglist);
    	mv.addObject("Intentionlist_1", Intentionlist_1);
    	mv.addObject("Intentionlist_2", Intentionlist_2);
    	mv.addObject("Intentionlist_3", Intentionlist_3);
    	mv.addObject("Objectlist_1", Objectlist_1);
    	mv.addObject("Objectlist_2", Objectlist_2);
    	mv.addObject("Objectlist_3", Objectlist_3);
    	mv.addObject("ObjectDtllist_1", ObjectDtllist_1);
    	mv.addObject("ObjectDtllist_2", ObjectDtllist_2);
    	mv.addObject("ObjectDtllist_3", ObjectDtllist_3);
    	mv.addObject("Mornitoringlist", Mornitoringlist);
    	mv.addObject("Actionslist", Actionslist);
    	mv.addObject("SEQ_NO", commandMap.get("AGENT_SEQ"));	
    	mv.addObject("Condition", Condition);
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	private void ReplaceTag(Map<String, Object> map, Pattern p) {				
		String str = map.get("OBJECT_VALUE").toString();
		//String temp = "";
		
		str = str.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("&lt;br&gt;", "<br>");
		
		/*Matcher m = p.matcher(str);		
				
		try {
	        while (m.find()) {	        	
	        	if (!m.group().equals("<br>")) {
	        		temp = m.group().replace("<", "&lt;").replace(">", "&gt;"); 
	        		str = str.replace(m.group(), temp);
				}    	        	
	        }
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage());
		}*/
		
		map.put("OBJECT_VALUE", str);
	}
	
	@RequestMapping(value="/view/insertagent.do")
    public ModelAndView insertAgent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv;
		if (commandMap.get("SEQ_NO").toString().isEmpty())
			mv = new ModelAndView("redirect:/view/agentlist.do");
		else
			mv = new ModelAndView("redirect:/view/agentedit.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		viewService.insertAgent(commandMap.getMap());
		   			
		if (commandMap.get("AGENT_SEQ").toString().isEmpty()) 
			mv.addObject("AGENT_SEQ", commandMap.get("SEQ_NO"));	
		else
			mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		mv.addObject("SAVE_FLAG", "Y");
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deleteagent.do")
    public ModelAndView deleteAgent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/agentlist.do");
		
		if (commandMap.get("TYPE").toString().equals("P")) {
			mv = new ModelAndView("redirect:/view/openagentgraph.do");
		}
		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));		
		viewService.deleteAgent(commandMap.getMap());
		
    	return SetMenu(mv, commandMap, request, response);
    }
		
	@RequestMapping(value="/view/updateagentsortno.do")
    public ModelAndView updateAgentSortNo(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.updateAgentSortNo(commandMap.getMap());
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/insertagentmotitor.do")
    public ModelAndView insertAgentMotitor(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/agentedit.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		viewService.insertAgentMonitoring(commandMap.getMap());
		
		if (commandMap.get("AGENT_SEQ").toString().isEmpty()) 
			mv.addObject("AGENT_SEQ", commandMap.get("SEQ_NO"));	
		else
			mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));		

    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/slotlist.do")
    public ModelAndView openSlotList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/slot/SlotList");    	
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	String strWhere = "";
    	String strOrderby = "";
    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";
    		strOrderby = "DESC";
		}
    		
    	commandMap.put("WHERE_SQL", strWhere);
    	commandMap.put("ORDERBY_SQL", strOrderby);
    	List<Map<String, Object>> clist = viewService.selectSlotClass(commandMap.getMap());
    	List<Map<String, Object>> rclist = viewService.selectSlotClassRefer(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	
    	mv.addObject("CLASS_LIST", clist);
    	mv.addObject("CLASS_LIST_REFER", rclist);
    	mv.addObject("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
    	
    	return SetMenu(mv, commandMap, request, response);
    }
		
	@RequestMapping(value="/view/classnamecheck.do")
    public ModelAndView ClassNameCheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String,Object>> list = viewService.selectSlotClassName(commandMap.getMap());
    	if (list.size() == 0) 
    		mv.addObject("CHECK", true);
    	else    		
    		mv.addObject("CHECK", false);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotdefine.do")
    public ModelAndView getSlotDefine(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	// Sub 슬롯들 addition 관련
    	/*if (commandMap.get("SLOT_SEQ_NO").toString().indexOf('_') > -1) {    		
    		Map<String,Object> addition = viewService.selectSlotAddition(commandMap.getMap());
    		mv.addObject("addition", addition);
		} */
    	List<Map<String, Object>> list2 = viewService.selectSlotTypeCheck(commandMap.getMap());
    	mv.addObject("list2", list2);	
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO").toString());    	
    	List<Map<String,Object>> list = viewService.selectSlotDefine(commandMap.getMap());    	
    	mv.addObject("list", list);	
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotlist.do")
    public ModelAndView selectSlotList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	List<Map<String,Object>> slotlist = viewService.selectSlot2(commandMap.getMap());
    	mv.addObject("list", slotlist);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/opentypesearch.do")
    public ModelAndView openTypeSearchh(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/TypeSearch");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectSlotType(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("TYPE_LIST", list);    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/opentasksearch.do")
    public ModelAndView openTaskSearchh(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/TaskSearch");    	
    		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/opensensesearch.do")
    public ModelAndView openSenseSearchh(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/SenseSearch");    	
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/openreferencesearch.do")
    public ModelAndView openReferenceSearchh(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/ReferenceClass");    	
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	String strWhere = "AND AGENT_SEQ IS NOT NULL";	
    	
    	if (commandMap.get("Type").toString().equals("E")) {
    		commandMap.put("WHERE_SQL", strWhere);
        	commandMap.put("ORDERBY_SQL", "");
        	List<Map<String, Object>> clist = viewService.selectSlotClass(commandMap.getMap());        	
        	List<Map<String, Object>> rclist = viewService.selectRererenceClass(commandMap.getMap());        	
        	mv.addObject("CLASS_LIST", clist);
        	mv.addObject("R_CLASS_LIST", rclist);	
		}
    	else {
    		strWhere = "AND AGENT_SEQ != 0";
        	
        	commandMap.put("WHERE_SQL", strWhere);
        	List<Map<String,Object>> list = viewService.selectIntents(commandMap.getMap());
        	mv.addObject("CLASS_LIST", list);
    	}
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/classreferorcopy.do")
    public ModelAndView classReferOrCopy(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	if (commandMap.get("TYPE").toString().equals("E"))
    		viewService.insertReferClass(commandMap.getMap());
    	else {
    		
    		try {
    			mv.addObject("COPY_TYPE", 0);
    			viewService.copyIntents(commandMap.getMap());
			} catch (Exception e) {
				// TODO: handle exception
				log.debug(e.getMessage());
				if (commandMap.containsKey("ERROR")) {
					mv.addObject("COPY_TYPE", 1);	
				}
				else
					mv.addObject("COPY_TYPE", 2);
			}    		
    	}
    	mv.addObject("STATUS", "OK");    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/getparentclass.do")
    public ModelAndView selectSlotTypeClass(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectSlotTypeClass(commandMap.getMap());
    	mv.addObject("list", list);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/slotinstansces.do")
    public ModelAndView openSlotInstans(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		/*ModelAndView mv = new ModelAndView("/slot/SlotInstances");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
		mv.addObject("SLOT_LIST", SetSlotListToJson(slist));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));*/
    	ModelAndView mv = new ModelAndView("/slot/SlotInstances");
    
    	//List<Map<String,Object>> slotlist = viewService.selectSlot2(commandMap.getMap());    	
    	List<Map<String,Object>> slotlist = GetSlotList(commandMap);
    	mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	private List<Map<String,Object>> GetSlotList(CommandMap commandMap) {		
		List<Map<String,Object>> slotlist = new ArrayList<Map<String,Object>>();
    	
    	JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;
        StringBuilder sbcol = new StringBuilder();         
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)commandMap.get("SLOT_LIST"));			
			JSONArray objArray = (JSONArray)jsonObj.get(commandMap.get("SEQ_NO"));
			CommonUtils.slotRecursive(slotlist, sbcol, commandMap.get("SEQ_NO").toString(), "", jsonObj, objArray, "", commandMap.get("SEQ_NO").toString(), Integer.parseInt(commandMap.get("SEQ_NO").toString()));			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return slotlist;
	}
	
	@RequestMapping(value="/view/selectslotinstansces.do")
    public ModelAndView selectSlotInstans(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	if (commandMap.containsKey("WHERE_SQL")) {
    		commandMap.put("WHERE_SQL", commandMap.get("WHERE_SQL").toString().replaceAll("§", "%"));
		}
    	
    	List<Map<String,Object>> list = viewService.selectSlotInstances(commandMap.getMap());    	
    	mv.addObject("list", list);
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/insertslotinstansces.do")
    public ModelAndView insertSlotInstans(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{		
    	/*ModelAndView mv = new ModelAndView("jsonView");
    	viewService.insertSlotInstans(commandMap.getMap());
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));*/
		ModelAndView mv = new ModelAndView("/slot/SlotInstances");
    	viewService.insertSlotInstans(commandMap.getMap());
    	
    	List<Map<String,Object>> slotlist = GetSlotList(commandMap);
    	
    	mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
            	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deleteslotinstansall.do")
    public ModelAndView deleteSlotInstansAll(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{		
    	ModelAndView mv = new ModelAndView("/slot/SlotInstances");
    	viewService.deleteSlotInstansAll(commandMap.getMap());
    	
    	List<Map<String,Object>> slotlist = GetSlotList(commandMap);
    	
    	mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
            	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updateslotinstansces.do")
    public ModelAndView updateSlotInstans(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		viewService.updateSlotInstans(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteslotinstansces.do")
    public ModelAndView deleteSlotInstans(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteSlotInstans(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotinstanscesexcel.do")
    public ModelAndView getSlotInstansExcel(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("excelView"); 	
    	
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("HEADER_LIST", commandMap.get("HEADER_LIST"));
        mv.addObject("COL_LIST", commandMap.get("COL_LIST"));
        mv.addObject("SEL_COL_LIST", commandMap.get("SEL_COL_LIST"));
        //mv.addObject("CLASS_LIST", commandMap.get("CLASS_LIST"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
        mv.addObject("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	mv.addObject("TYPE", "I");
    	return mv;
    }
	
	@RequestMapping(value="/view/setslotinstanscesexcel.do")
	public ModelAndView setSlotInstansExcel(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/slot/SlotInstances");		
		
		try {
			viewService.insertSlotInstansExcel(commandMap.getMap(), request);
			mv.addObject("UPLOAD_STATUS", "S");
		} catch (Exception e) {
			// TODO: handle exception
			if(commandMap.containsKey("UPLOAD_STATUS"))
				mv.addObject("UPLOAD_STATUS", commandMap.get("UPLOAD_STATUS"));
			else
				mv.addObject("UPLOAD_STATUS", "F");
		}
		
		List<Map<String,Object>> slotlist = GetSlotList(commandMap);		
		mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
        
    	return SetMenu(mv, commandMap, request, response);
	}
	
	@RequestMapping(value="/view/slotmapping.do")
    public ModelAndView openSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/slot/SlotMapping");    	        
    	
    	mv.addObject("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/selectslotmapping.do")
    public ModelAndView selectSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String,Object>> list = viewService.selectSlotMapping(commandMap.getMap());    	
    	mv.addObject("list", list);
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertslotmapping.do")
    public ModelAndView insertSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{    	
		ModelAndView mv = new ModelAndView("/slot/SlotMapping");
    	viewService.insertSlotMapping(commandMap.getMap());    	               
    	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updateslotmapping.do")
    public ModelAndView updateSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		viewService.updateSlotMapping(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteslotmapping.do")
    public ModelAndView deleteSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteSlotMapping(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
		
	@RequestMapping(value="/view/slotwrite.do")
    public ModelAndView openSlotWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/slot/SlotWrite");    	
        
    	//List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	//mv.addObject("OBJ_LIST", list);
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/getslotobject.do")
    public ModelAndView getSlotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");				
		
		if (commandMap.containsKey("WHERE_SQL")) {
    		commandMap.put("WHERE_SQL", commandMap.get("WHERE_SQL").toString().replaceAll("§", "%"));
		}
		
		List<Map<String, Object>> list = viewService.selectSlotObjectPaging(commandMap.getMap());
    	mv.addObject("list", list);		
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("ROWCNT", commandMap.get("PAGE_ROW"));
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotobjectdetail.do")
    public ModelAndView getSlotObjectDetail(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	List<Map<String, Object>> list = viewService.selectSlotObjectDetail(commandMap.getMap());
    	mv.addObject("DETAIL_LIST", list); 
    	mv.addObject("PARENT_SEQ_NO", commandMap.get("PARENT_SEQ_NO"));
    	return mv;
    }	
	
	@RequestMapping(value="/view/getslotobjectexcel.do")
    public ModelAndView getSlotObjectExcel(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("excelView");    	
    	
    	List<Map<String, Object>> objlist = viewService.selectSlotObject(commandMap.getMap());
    	List<Map<String, Object>> objdtllist = viewService.selectSlotObjectDetailExcel(commandMap.getMap());
    	mv.addObject("OBJECT_LIST", objlist);
    	mv.addObject("DETAIL_LIST", objdtllist);
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	mv.addObject("TYPE", "O");
    	return mv;
    }
	
	@RequestMapping(value="/view/setslotobjectexcel.do")
	public ModelAndView setSlotObjectExcel(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/slot/SlotWrite");		
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		try {
			viewService.insertSlotObjectExcel(commandMap.getMap(), request);		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	//mv.addObject("OBJ_LIST", list);    	
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));    	
    	mv.addObject("STATUS", commandMap.get("STATUS"));
    	
    	return SetMenu(mv, commandMap, request, response);
	}
	
	
	@RequestMapping(value="/view/insertslotclass.do")
    public ModelAndView insertSlotClass(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		if (commandMap.get("SEQ_NO").toString().isEmpty()) {
			viewService.insertSlotClass(commandMap.getMap());			
			
			CommandMap cdb = new CommandMap();			
			cdb.put("SEQ_NO", commandMap.get("SEQ_NO"));
			cdb.put("COL_NAME", "SEQ_NO INT NOT NULL AUTO_INCREMENT PRIMARY KEY");
		    viewService.CreateTable(cdb.getMap());
		}
		else {
			viewService.updateSlotClass(commandMap.getMap());
			
			if (!commandMap.get("UPDATE_TASK_SEQ").toString().isEmpty() && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
				CommandMap rcCmd = new CommandMap();
				rcCmd.put("AGENT_SEQ", commandMap.get("UPDATE_TASK_SEQ").toString());
				rcCmd.put("WHERE", String.format("AND CLASS_SEQ = %s", commandMap.get("SEQ_NO")));
				List<Map<String, Object>> rclist = viewService.selectRererenceClass(rcCmd.getMap());				
												
				for (Map<String, Object> rcitem : rclist) {					
					rcCmd.put("CLASS_SEQ", rcitem.get("CLASS_SEQ"));
					viewService.deleteReferSlotClass(rcCmd.getMap());	
				}
			}
		}
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updateslotclassnewcheck.do")
    public ModelAndView updateClassNewcheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.updateSlotClassNewCheck(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/getslottypecheck.do")
    public ModelAndView getSlotTypeCheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));		

		List<Map<String, Object>> list = viewService.selectSlotTypeCheck(commandMap.getMap());		 
		if (list.size() == 0) {
			mv.addObject("STATUS", "S");
			
			list = viewService.selectSlotTypeClassCheck(commandMap.getMap());
			if (list.size() == 0) 
				mv.addObject("STATUS", "S");	
			else
				mv.addObject("STATUS", "US");	
		}
		else
			mv.addObject("STATUS", "UC");
		
    	return mv;
    }
			
	@RequestMapping(value="/view/insertslot.do")
    public ModelAndView insertSlot(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
			
		if (commandMap.get("SEQ_NO").toString().isEmpty()) {		
			viewService.insertSlot(commandMap.getMap(), request);
		}
		else {						
			viewService.updateSlot(commandMap.getMap(), request);
		}
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deletereferslotclass.do")
    public ModelAndView deleteReferSlotClass(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
				
		viewService.deleteReferSlotClass(commandMap.getMap());
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deleteslotclass.do")
    public ModelAndView deleteSlotClass(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
				
		viewService.deleteSlotClass(commandMap.getMap());		
	    
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deleteslot.do")
    public ModelAndView deleteSlot(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.deleteSlot(commandMap.getMap());
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deleteslotcheck.do")
    public ModelAndView deleteSlotCheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		//ModelAndView mv = new ModelAndView("/slot/SlotWrite");
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		List<Map<String, Object>> list = viewService.selectSlotTypeCheck(commandMap.getMap());
		 
		if (list.size() == 0)
			mv.addObject("STATUS", "S");
		else
			mv.addObject("STATUS", "F");
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/insertslotobject.do")
    public ModelAndView insertSlotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		//ModelAndView mv = new ModelAndView("/slot/SlotWrite");
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		
    	try {
    		viewService.insertSlotObject(commandMap.getMap(), request);		
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	//List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	//mv.addObject("OBJ_LIST", list);    	    	
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));    	
    	mv.addObject("STATUS", commandMap.get("STATUS"));
    	return SetMenu(mv, commandMap, request, response);
    }	
	
	@RequestMapping(value="/view/updateslotobject.do")
    public ModelAndView updateSlotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		
		viewService.updateSlotObject(commandMap.getMap(), request);
		mv.addObject("SAVE_CHK", commandMap.get("SAVE_CHK"));
		mv.addObject("TYPE", commandMap.get("TYPE"));
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteslotobject.do")
    public ModelAndView deleteSlotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		
		viewService.deleteSlotObject(commandMap.getMap(), request);
		viewService.deleteSlotObjectDetail(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteslotobjectdetail.do")
    public ModelAndView deleteSlotObjectDetail(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		
		viewService.deleteSlotObjectDetailSeq(commandMap.getMap(), request);
		
    	return mv;
    }
	
	@RequestMapping(value="/view/updateslotsort.do")
    public ModelAndView updateSlotSort(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		
		viewService.updateSlotSort(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/useruttrlist.do")
    public ModelAndView openUserUttrList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrList");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	String strWhere = "";
    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		//strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";    		
    		strWhere = "AND AGENT_SEQ = " + commandMap.get("AGENT_SEQ").toString();
		}
    	
    	commandMap.put("WHERE_SQL", strWhere);
    	List<Map<String,Object>> list = viewService.selectIntents(commandMap.getMap());
    	
    	mv.addObject("list", list);
    	mv.addObject("INTENTS_LIST", CommonUtils.SetIntentListToJson(list));
    	if (commandMap.containsKey("UPLOAD_STATUS")) {
    		mv.addObject("UPLOAD_STATUS", commandMap.get("UPLOAD_STATUS"));
		}
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/useruttrwrite.do")
    public ModelAndView openUserUttrWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrWrite");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));

    	List<Map<String,Object>> intlist = viewService.selectIntention(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	        	       
    	mv.addObject("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
    	mv.addObject("intlist", intlist);
    	if (commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		mv.addObject("AGENT_SEQ", 0);
		}
    	else {
    		mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
    	}
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/useruttredit.do")
    public ModelAndView openUserUttrEdit(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrWrite");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	//List<Map<String,Object>> UserSaylist;
    	List<Map<String,Object>> SlotList = new ArrayList<Map<String,Object>>();
    	String tempSlotSeq = "";
    	 
    	if (commandMap.get("SLOT_SEQ_NO").toString().isEmpty()) {
       		//UserSaylist = viewService.selectIntentsUserSays(commandMap.getMap());
       	}
    	else {
       		//UserSaylist = viewService.selectIntentsUserSays2(commandMap.getMap());
    		tempSlotSeq = commandMap.get("SLOT_SEQ_NO").toString();    		
       		String[] arrSlotSeq = commandMap.get("SLOT_SEQ_NO").toString().split(",");
       		String[] arrSlotSeqSub;
    		String SlotSeq = "";
    		for (int i = 0; i < arrSlotSeq.length; i++) {
    			if (arrSlotSeq[i].contains("_")) {
    				arrSlotSeqSub = arrSlotSeq[i].split("_");
    				if (i == 0)
        				SlotSeq = arrSlotSeqSub[arrSlotSeqSub.length - 1];	
        			else
        				SlotSeq += String.format(",%s", arrSlotSeqSub[arrSlotSeqSub.length - 1]);	
				}
    			else {
    				if (i == 0)
        				SlotSeq = arrSlotSeq[i];	
        			else
        				SlotSeq += String.format(",%s", arrSlotSeq[i]);
    			}    			
			}

    		commandMap.put("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO").toString().replaceAll(" ", ""));
    		
       		CommandMap SlotCmd = new CommandMap();
       		SlotCmd.put("SLOT_SEQ_NO", SlotSeq);
       		SlotList = viewService.selectSlotIntents(SlotCmd.getMap());
    	}
    	
    	List<Map<String,Object>> intlist = viewService.selectIntention(commandMap.getMap());
    	if (!commandMap.get("INTENTION").toString().isEmpty()) {
    		String[] arrIntention = commandMap.get("INTENTION").toString().split(" ");
    		String IntentionSlot = "";    		
    		    	
    		for (Iterator iterator = intlist.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();
				
				for (int i = 0; i < arrIntention.length; i++) {
					if (item.get("INTENTION_NAME").toString().equals(arrIntention[i])) {
						if (i == 0)
							IntentionSlot = String.format("%s=\"%s\"", item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE"));
						else
							IntentionSlot = String.format("%s, %s=\"%s\"", IntentionSlot, item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE"));
					}
				}
			}
    		mv.addObject("INTENTION_SLOT", IntentionSlot);
		}
    	
    	List<Map<String,Object>> Intentionlist = viewService.selectIntentIntention(commandMap.getMap());
    	List<Map<String,Object>> Objectlist = viewService.selectIntentsObejct(commandMap.getMap());
    	List<Map<String,Object>> ObjectDtllist = viewService.selectIntentsObjectDtl(commandMap.getMap());

    	Pattern p = Pattern.compile("<[a-zA-Zㄱ-�R0-9.=\\-_\"()+\\s]+>");
    	
    	for (Iterator iterator = ObjectDtllist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();			
			ReplaceTag(map, p);
		}
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	        	       
    	if (!tempSlotSeq.isEmpty()) {
    		commandMap.put("SLOT_SEQ_NO", tempSlotSeq);
		}
    	
    	mv.addObject("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	//mv.addObject("UserSaylist", UserSaylist);
    	mv.addObject("Intentionlist", Intentionlist);
    	mv.addObject("Objectlist", Objectlist);
    	mv.addObject("ObjectDtllist", ObjectDtllist);
    	mv.addObject("SlotList", SlotList);
    	mv.addObject("intlist", intlist);
    	mv.addObject("SEQ_NO", commandMap.get("INTENT_SEQ"));
    	mv.addObject("INTENT_NAME", commandMap.get("INTENT_NAME"));
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("INTENTION", commandMap.get("INTENTION"));
    	mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
    	if (commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		mv.addObject("AGENT_SEQ", 0);
		}
    	else {
    		mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
    	}
    	if (commandMap.containsKey("UPLOAD_STATUS")) {
    		mv.addObject("UPLOAD_STATUS", commandMap.get("UPLOAD_STATUS"));
		}
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/getusersaylist.do")
    public ModelAndView selectUserSayList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	List<Map<String,Object>> list;
    	if (commandMap.get("SLOT_SEQ_NO").toString().isEmpty() && commandMap.get("INTENTION").toString().isEmpty()) {
    		list = viewService.selectIntentsUserSays(commandMap.getMap());
       	}
    	else {    		    		
    		list = viewService.selectIntentsUserSays2(commandMap.getMap());       		
    	}
    	
    	mv.addObject("list", list);		
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("ROWCNT", commandMap.get("PAGE_ROW"));
    	mv.addObject("list", list);   	
    	return mv;
	}
	
	@RequestMapping(value="/view/insertusersayfile.do")
    public ModelAndView insertUserSayFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv;
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		//List<Map<String, Object>> clist = viewService.selectSlotClass(commandMap.getMap());
		List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());	
		commandMap.put("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));
		//commandMap.put("CLASS_MAP", CommonUtils.setClassListToMapName(clist));
    	//commandMap.put("SLOT_MAP", CommonUtils.setSlotListToMapSlotName(slist));
    	
		try {
			viewService.insertUserSayFile(commandMap.getMap(), request);
			commandMap.put("UPLOAD_STATUS", "S");
		} catch (Exception e) {
			// TODO: handle exception								
			commandMap.put("UPLOAD_STATUS", "F");
		}
				
		if (commandMap.get("TYPE").toString().contains("W")) {
			mv = new ModelAndView("redirect:/view/useruttredit.do");
		}
		else 
			mv = new ModelAndView("redirect:/view/useruttrlist.do");
		mv.addObject("INTENT_SEQ", commandMap.get("SEQ_NO"));		
		mv.addObject("INTENT_NAME", commandMap.get("INTENT_NAME"));
		mv.addObject("SLOT_SEQ_NO", "");
		mv.addObject("INTENTION", "");
		mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
		mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
		mv.addObject("UPLOAD_STATUS", commandMap.get("UPLOAD_STATUS"));
		log.debug(commandMap.getMap());
		if (commandMap.containsKey("STATUS"))
			mv.addObject("STATUS", commandMap.get("STATUS"));	

    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertintent.do")
    public ModelAndView insertIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/useruttr/UserUttrWrite");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.insertIntent(commandMap.getMap());
		
		commandMap.put("INTENT_SEQ", commandMap.get("SEQ_NO"));
    	//List<Map<String,Object>> UserSaylist;
    	List<Map<String,Object>> SlotList = new ArrayList<Map<String,Object>>();
    	String tempSlotSeq = "";
    	if (commandMap.get("SLOT_SEQ_NO").toString().isEmpty()) {
       		//UserSaylist = viewService.selectIntentsUserSays(commandMap.getMap());
       	}
    	else { 		    		
       		//UserSaylist = viewService.selectIntentsUserSays2(commandMap.getMap());
    		tempSlotSeq = commandMap.get("SLOT_SEQ_NO").toString();
       		String[] arrSlotSeq = commandMap.get("SLOT_SEQ_NO").toString().split(",");
    		String SlotSeq = "";
    		for (int i = 0; i < arrSlotSeq.length; i++) {
    			if (arrSlotSeq[i].contains("_")) {
    				if (i == 0)
        				SlotSeq = arrSlotSeq[i].split("_")[1];	
        			else
        				SlotSeq += String.format(",%s", arrSlotSeq[i].split("_")[1]);	
				}
    			else {
    				if (i == 0)
        				SlotSeq = arrSlotSeq[i];	
        			else
        				SlotSeq += String.format(",%s", arrSlotSeq[i]);
    			}    			
			}

    		commandMap.put("SLOT_SEQ_NO", SlotSeq);
    		
       		CommandMap SlotCmd = new CommandMap();
       		SlotCmd.put("SLOT_SEQ_NO", SlotSeq);
       		SlotList = viewService.selectSlotIntents(SlotCmd.getMap());
    	}
    	    	
    	List<Map<String,Object>> intlist = viewService.selectIntention(commandMap.getMap());
    	if (!commandMap.get("INTENTION").toString().isEmpty()) {
    		String[] arrIntention = commandMap.get("INTENTION").toString().split(" ");
    		String IntentionSlot = "";    		
    		
    		for (Iterator iterator = intlist.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();
				
				for (int i = 0; i < arrIntention.length; i++) {
					if (item.get("INTENTION_NAME").toString().equals(arrIntention[i])) {
						if (i == 0)
							IntentionSlot = String.format("%s=\"%s\"", item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE"));
						else
							IntentionSlot = String.format("%s, %s=\"%s\"", IntentionSlot, item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE"));
					}
				}
			}
    		mv.addObject("INTENTION_SLOT", IntentionSlot);
		}
    
    	List<Map<String,Object>> Intentionlist = viewService.selectIntentIntention(commandMap.getMap());
    	List<Map<String,Object>> Objectlist = viewService.selectIntentsObejct(commandMap.getMap());
    	List<Map<String,Object>> ObjectDtllist = viewService.selectIntentsObjectDtl(commandMap.getMap());

    	Pattern p = Pattern.compile("<[a-zA-Zㄱ-�R0-9.=\\-_\"()+\\s]+>");
    	
    	for (Iterator iterator = ObjectDtllist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();			
			ReplaceTag(map, p);
		}
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	
    	if (!tempSlotSeq.isEmpty()) {
    		commandMap.put("SLOT_SEQ_NO", tempSlotSeq);
		}
    	mv.addObject("SLOT_LIST", CommonUtils.SetClassNameSlotListToJson(slist));
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	//mv.addObject("UserSaylist", UserSaylist);
    	mv.addObject("Intentionlist", Intentionlist);
    	mv.addObject("Objectlist", Objectlist);
    	mv.addObject("ObjectDtllist", ObjectDtllist);
    	mv.addObject("SlotList", SlotList);
    	mv.addObject("intlist", intlist);
    	mv.addObject("SEQ_NO", commandMap.get("INTENT_SEQ"));
    	mv.addObject("INTENT_NAME", commandMap.get("INTENT_NAME"));
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("INTENTION", commandMap.get("INTENTION"));
    	mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
    	if (commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		mv.addObject("AGENT_SEQ", 0);
		}
    	else {
    		mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
    	}
    	if (commandMap.containsKey("UPLOAD_STATUS")) {
    		mv.addObject("UPLOAD_STATUS", commandMap.get("UPLOAD_STATUS"));
		}
    	
    	
    	return SetMenu(mv, commandMap, request, response);
    	
		/*ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.insertIntent(commandMap.getMap());
		
		Map<String,Object> ObjIntent = viewService.selectIntent(commandMap.getMap());
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		mv.addObject("INTENT_SEQ", commandMap.get("SEQ_NO"));
		mv.addObject("INTENT_NAME", ObjIntent.get("INTENT_NAME"));
		mv.addObject("SLOT_SEQ_NO", "");

    	return mv;*/
    }
	
	@RequestMapping(value="/view/deleteintent.do")
    public ModelAndView deleteIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/useruttrlist.do");
		
		viewService.deleteIntent(commandMap.getMap());
		viewService.deleteIntentsUserSays(commandMap.getMap());
		viewService.deleteIntentsIntention(commandMap.getMap());
		viewService.deleteIntentsObject(commandMap.getMap());
		viewService.deleteIntentsObjectDetail(commandMap.getMap());
    	
    	return SetMenu(mv, commandMap, request, response);
    }
 	
	@RequestMapping(value="/view/deleteintentusersay.do")
    public ModelAndView deleteIntentUserSay(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
	
		viewService.deleteIntentUserSay(commandMap.getMap());
				
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/downloadintentfile.do")
    public ModelAndView downloadIntentFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		commandMap.put("LANG_SEQ_NO", request.getSession().getAttribute("ProjectLangNo"));
		
		viewService.DownloadIntent(commandMap.getMap(), request);			
		
		byte fileByte[] = FileUtils.readFileToByteArray(new File(commandMap.get("Path").toString()));		
		
		response.setContentType("application/octet-stream");
		response.setContentLength(fileByte.length);
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + commandMap.get("FileName").toString() +"\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.getOutputStream().write(fileByte);		
		response.getOutputStream().flush();
		response.getOutputStream().close();
		
		File f = new File(commandMap.get("Path").toString());
		if (f.exists()) {
			f.delete();
		}
		
    	return SetMenu(mv, commandMap, request, response);
    }	
	
	@RequestMapping(value="/view/deleteintentusersaydtl.do")
    public ModelAndView deleteIntentUserSayDtl(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteIntentUserSayDetail(commandMap.getMap());
				
    	return mv;
    }
	
	@RequestMapping(value="/view/replaceintent.do")
    public ModelAndView openReplaceIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/ReplaceIntent");
    	
    	mv.addObject("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/selectreplaceintent.do")
    public ModelAndView selectReplaceIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	if (commandMap.containsKey("WHERE_SQL")) {
    		commandMap.put("WHERE_SQL", commandMap.get("WHERE_SQL").toString().replaceAll("§", "%"));
		}
    	
    	List<Map<String,Object>> list = viewService.selectReplaceIntent(commandMap.getMap());    	
    	mv.addObject("list", list);
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertreplaceintent.do")
    public ModelAndView insertReplaceIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{	
		ModelAndView mv = new ModelAndView("redirect:/view/replaceintent.do");
    	viewService.insertReplaceIntent(commandMap.getMap());
    	
        mv.addObject("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));          
    	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updatereplaceintent.do")
    public ModelAndView updateReplaceIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		viewService.updateReplaceIntent(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deletereplaceintent.do")
    public ModelAndView deleteReplaceIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteReplaceIntent(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/editintent.do")
    public ModelAndView openEditIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/EditIntent");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	Map<String,Object> editintent = viewService.selectEditIntent(commandMap.getMap());
    	
    	mv.addObject("editintent", editintent);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/inserteditintent.do")
    public ModelAndView insertEditIntent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{	
		ModelAndView mv = new ModelAndView("redirect:/view/editintent.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.insertEditIntent(commandMap.getMap());
    	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/selectintention.do")
    public ModelAndView selectIntention(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{	
		ModelAndView mv = new ModelAndView("/popup/IntentionList");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		List<Map<String,Object>> list = viewService.selectIntention(commandMap.getMap());    	
		mv.addObject("list", list);
    	
        return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertintention.do")
    public ModelAndView insertIntention(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{	
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.insertIntention(commandMap.getMap());
		
		mv.addObject("STATUS", "OK");
        return mv;
    }

	@RequestMapping(value="/view/deleteintention.do")
    public ModelAndView deleteIntention(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");				
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.deleteIntention(commandMap.getMap());
					
		mv.addObject("STATUS", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/updateintentiongroup.do")
    public ModelAndView updateIntentionGroup(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");				
		
		viewService.updateIntentionGroup(commandMap.getMap());
					
		mv.addObject("STATUS", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/openintention.do")
    public ModelAndView openIntention(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{	
		ModelAndView mv = new ModelAndView("/popup/IntentionSearch");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		List<Map<String,Object>> list = viewService.selectIntention(commandMap.getMap());
		mv.addObject("list", list);		
		mv.addObject("INTENTION", URLDecoder.decode(request.getParameter("intention"), "UTF-8"));
    	log.debug(URLDecoder.decode(request.getParameter("intention"), "UTF-8"));
        return mv;
    }
	
	@RequestMapping(value="/view/getslotmapping.do")
    public ModelAndView getSlotMapping(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");				
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		String slotReturn = viewService.GetSlotMapping(commandMap.getMap(), request);
		
		mv.addObject("slotmapping", slotReturn);
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotobjectlist.do")
    public ModelAndView selectSlotObjectList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String,Object>> list = viewService.selectVSlotObject(commandMap.getMap());
    	mv.addObject("list", list);   	
    	return mv;
	}
	
	@RequestMapping(value="/view/getslotcolor.do")
    public ModelAndView selectSlotColor(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("COLOR", viewService.selectSlotColor(commandMap.getMap()));   	
    	return mv;
	}
	
	@RequestMapping(value="/view/knowledgelist.do")
    public ModelAndView openKnowledgeList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/knowledge/KnowledgeList");    	
        	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/knowledgedetaillist.do")
    public ModelAndView openKnowledgeDetailList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/knowledge/KnowledgeDetailList");    	
        	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/dialogslearnfilesave.do")
    public ModelAndView DialogsLearnFileSave(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		  commandMap.put("LANG_SEQ_NO", request.getSession().getAttribute("ProjectLangNo"));

    	viewService.DialogsLearning(commandMap.getMap(), request);
    	mv.addObject("STATUS", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/dialogslearning.do")
    public ModelAndView DialogsLearning(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
		  commandMap.put("LANG_SEQ_NO", request.getSession().getAttribute("ProjectLangNo"));
    	
    	viewService.DialogsLearning(commandMap.getMap(), request);
    	viewService.SetLeanLog(commandMap.getMap(), request);    	
    	//mv.addObject("LOG", viewService.GetLeanLog(commandMap.getMap(), request));
    	mv.addObject("STATUS", "OK");
    	return mv;
    }	
	
	@RequestMapping(value="/view/dialogslearningchk.do")
    public ModelAndView DialogsLearningChk(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	    	
    	mv.addObject("LOG", viewService.GetLeanLog(commandMap.getMap(), request));
    	mv.addObject("STATUS", "OK");
    	return mv;
    }	
	
	@RequestMapping(value="/view/dialogstart.do")
    public ModelAndView DialogStart(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	List<Map<String, Object>> prjlist = SetProject(Integer.parseInt(request.getSession().getAttribute("ProjectNo").toString()), request);
    	
    	if (prjlist.size() > 0) {
			if (prjlist.get(0).get("LEARN_CHECK").toString().equals("N")) {
				mv.addObject("STATUS", "NL");
				return mv;		
			}
		}
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	commandMap.put("SEQ_NO", request.getSession().getAttribute("ProjectNo"));
			commandMap.put("LANG_SEQ_NO", request.getSession().getAttribute("ProjectLangNo"));
    	commandMap.put("USER_TYPE", "SA");
    	
    	try {			    		
    		viewService.DialogStart(commandMap.getMap(), request);
    		mv.addObject("STATUS", "OK");
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage());
			mv.addObject("STATUS", "F");
		}
    	
    	return mv;
    }

	@RequestMapping(value = "/view/dialoginput.do")
	public ModelAndView DialogInput(CommandMap commandMap, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));

		if (request.getSession().getAttribute("ProjectLangNo").toString().equals("2")) {
			mv.addObject("LOG", viewService.DialogInputEnglish(commandMap.getMap(), request));
		} else {
			mv.addObject("LOG", viewService.DialogInput(commandMap.getMap(), request));
		}

		mv.addObject("STATUS", commandMap.get("STATUS"));
		return mv;
	}

	@RequestMapping(value="/view/dialogstop.do")
    public ModelAndView DialogStop(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    commandMap.put("LANG_SEQ_NO", request.getSession().getAttribute("ProjectLangNo"));
    	    	    	
    	mv.addObject("LOG", viewService.DialogStop(commandMap.getMap(), request));
    	mv.addObject("STATUS", "OK");
    	mv.addObject("TYPE", commandMap.get("TYPE"));
    	return mv;
    }
	
	@RequestMapping(value="/view/enginestop.do")
    public ModelAndView EngineStop(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	viewService.EngineStop(commandMap.getMap(), request);
    	return mv;
    }
	
	@RequestMapping(value="/view/userlist.do")
    public ModelAndView openUserList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/UserList");    	    	
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/selectUserList.do")
    public ModelAndView selectUserList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	
    	
    	List<Map<String,Object>> list = viewService.selectUserList(commandMap.getMap());
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("list", list);
    	return mv;
    }	
	
	@RequestMapping(value="/view/userwrite.do")
    public ModelAndView openUserWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/UserWrite");
    	
    	if (!commandMap.get("SEQ_NO").toString().equals("0")) {
    		Map<String,Object> userinfo = viewService.selectUser(commandMap.getMap());
    		AES256Util aes256 = new AES256Util(key);
    		URLCodec codec = new URLCodec();
    		userinfo.put("USER_PWD", aes256.aesDecode(codec.decode(userinfo.get("USER_PWD").toString())));
    		mv.addObject("userinfo", userinfo);
    	}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/userdomain.do")
    public ModelAndView openUserDomain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/DomainSearch");    	
    	
    	commandMap.put("SEQ_NO", 0);
    	List<Map<String, Object>> prjlist = null;
    	List<Map<String, Object>> list = null;
    	
    	commandMap.put("SEQ_NO", request.getParameter("userno"));
    	
    	if (commandMap.get("type").toString().equals("v")) {
    		prjlist = viewService.selectUserDomainList(commandMap.getMap());	
		}
    	else if (commandMap.get("type").toString().equals("c") || commandMap.get("type").toString().equals("sa")) {
    		prjlist = SetProject(0, request);
    	}
    	else {
    		prjlist = SetProject(0, request);
    		list = viewService.selectUserDomain(commandMap.getMap());
    	}    	    
    	     	
    	mv.addObject("prjlist", prjlist);
    	mv.addObject("list", list);
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/getuesersexcel.do")
    public ModelAndView getUsersExcel(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("excelView");    	    	
        mv.addObject("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("TYPE", "U");
    	return mv;
    }
	
	@RequestMapping(value="/view/copytask.do")
    public ModelAndView openCopyTask(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/TaskCopy");    	
    	
    	String UserType = "";
    	String UserSeq = "";
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){                
                if (cookies[i].getName().equals("USER_TYPE")) {
                	UserType = cookies[i].getValue();
				}
                else if (cookies[i].getName().equals("USER_SEQ")) {
                	UserSeq = cookies[i].getValue();
				}
            }
        }
    	if (!UserType.equals("SA")) {
    		commandMap.put("WHERE_SQL", String.format(" AND b.USER_SEQ_NO = %s", UserSeq));
		}
    	
    	List<Map<String,Object>> agentlist = viewService.selectAgentsCopy(commandMap.getMap());
    	
    	mv.addObject("agentlist", agentlist);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/taskcopy.do")
    public ModelAndView TaskCopy(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
		try {
			mv.addObject("COPY_TYPE", 0);			
			viewService.copyTask(commandMap.getMap(), request);
			mv.addObject("SEQ_NO",commandMap.get("SEQ_NO"));
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage());
			if (commandMap.containsKey("ERROR")) {
				mv.addObject("COPY_TYPE", 1);	
			}
			else
				mv.addObject("COPY_TYPE", 2);
		}    		    	
    	
    	mv.addObject("STATUS", "OK");    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/lualist.do")
    public ModelAndView openLuaList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaList");    	    	
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/languagewrite.do")
    public ModelAndView openLanguageWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LanguageWrite");    	    	
    	
    	commandMap.put("LANG_SEQ_NO", 0);
    	List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    	
    	mv.addObject("LANG_LIST", langlist);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/selectlualist.do")
    public ModelAndView selectLuaList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	
    	
    	List<Map<String,Object>> list = viewService.selectLuaList(commandMap.getMap());
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("list", list);
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/luawrite.do")
    public ModelAndView openLuaWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaWrite");
    	
    	if (!commandMap.get("SEQ_NO").toString().equals("0")) {
    		Map<String,Object> luainfo = viewService.selectLua(commandMap.getMap());    		
    		mv.addObject("luainfo", luainfo);
    	}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/setencoding.do")
    public ModelAndView setEncoding(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/EncodingSet");    	    	
    	
    	
		Map<String,Object> encodingdata = viewService.selectEncoding(commandMap.getMap());    		
		mv.addObject("encodingdata", encodingdata);    	    	    
		
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updateencoding.do")
    public ModelAndView updateEncoding(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");    	
    	    	
    	viewService.updateEncoding(commandMap.getMap());
    	mv.addObject("state", "OK");
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/copydomain.do")
    public ModelAndView copyDomain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		List<Map<String, Object>> prjlist = SetProject(0, request);
		
		List<String> namelist = viewService.copyDomain(commandMap.getMap(), prjlist, request);
		
		mv.addObject("namelist", namelist);
		mv.addObject("state", "OK");		
					
    	return mv;
    }
	
	@RequestMapping(value="/view/saveversion.do")
    public ModelAndView saveVersion(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/project/ProjectWrite");		
		
		if (!commandMap.get("VERSION_SEQ").toString().equals("0")) {
			CommandMap delCmd = new CommandMap();
			delCmd.put("SEQ_NO", commandMap.get("VERSION_SEQ"));
			delCmd.put("VERSION", "Y");
			viewService.deleteProject(delCmd.getMap(), request);
		}
		
		viewService.saveVersion(commandMap.getMap(), request);		
    	
		commandMap.put("SEQ_NO", commandMap.get("PROJECT_SEQ"));
		mv.addObject("SEQ_NO", commandMap.get("PROJECT_SEQ"));
		mv.addObject("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
		mv.addObject("DESCRIPTION", commandMap.get("DESCRIPTION"));
		mv.addObject("USER_SEQ_NO", commandMap.get("USER_SEQ"));
		mv.addObject("CHATBOT_USE_FLAG", commandMap.get("CHATBOT_USE_FLAG"));
		mv.addObject("CHATBOT_DATA_FLAG", commandMap.get("CHATBOT_DATA_FLAG"));
		mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
		mv.addObject("FILTER_VALUE", commandMap.get("FILTER_VALUE"));
		
		List<Map<String, Object>> versionlist = viewService.selectDomainVersion(commandMap.getMap());
		commandMap.put("LANG_SEQ_NO", 0);
    	List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    	    	
    	mv.addObject("LANG_LIST", langlist);
		mv.addObject("VERSION_LIST", versionlist);
		mv.addObject("state", "OK");

    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/loadversion.do")
    public ModelAndView loadVersion(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/project/ProjectWrite");		
				
		CommandMap delCmd = new CommandMap();
		delCmd.put("SEQ_NO", commandMap.get("SEQ_NO"));
		delCmd.put("VERSION", "Y");
		viewService.deleteProject(delCmd.getMap(), request);		
		
		viewService.loadVersion(commandMap.getMap(), request);		
    	
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		mv.addObject("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
		mv.addObject("DESCRIPTION", commandMap.get("DESCRIPTION"));
		mv.addObject("USER_SEQ_NO", commandMap.get("USER_SEQ"));
		mv.addObject("CHATBOT_USE_FLAG", commandMap.get("CHATBOT_USE_FLAG"));
		mv.addObject("CHATBOT_DATA_FLAG", commandMap.get("CHATBOT_DATA_FLAG"));
		mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
		mv.addObject("FILTER_VALUE", commandMap.get("FILTER_VALUE"));
		
		List<Map<String, Object>> versionlist = viewService.selectDomainVersion(commandMap.getMap());
		commandMap.put("LANG_SEQ_NO", 0);
    	List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    	    	
    	mv.addObject("LANG_LIST", langlist);
		mv.addObject("VERSION_LIST", versionlist);
		mv.addObject("state", "OK");

    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/userinsert.go")
    public ModelAndView InsertUser(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView"); 	

    	AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();		
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));
		
		mv.addObject("STATUS", "S");
    	if (commandMap.get("SEQ_NO").toString().equals("0")) {
    		commandMap.put("TYPE", "UI");
	    	List<Map<String,Object>> list = viewService.selectUserIdChk(commandMap.getMap());
	    	
	    	if (list.size() == 0) {	    			    		
	    		list.clear();
	    		commandMap.put("TYPE", "UA");
		    	list = viewService.selectUserIdChk(commandMap.getMap());
	    			    				    		
		    	if (list.size() == 0) {
		    		commandMap.put("USER_PWD", encpwd);	
		    		viewService.insertUser(commandMap.getMap());
		    	}
		    	else {
		    		mv.addObject("STATUS", "UA");	
		    	}
			}
	    	else {	    		
	    		mv.addObject("STATUS", "UI");	    		
	    	}
    	}
    	else {
    		commandMap.put("USER_PWD", encpwd);    		
			viewService.updateUser(commandMap.getMap());
		}
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/updateuserpassword.go")
    public ModelAndView UpdateUserPassword(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");;    	

    	AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();		
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));
		
		mv.addObject("STATUS", "S");
		try {
			commandMap.put("USER_PWD", encpwd);
			viewService.updateUserPassword(commandMap.getMap());			
		} catch (Exception e) {
			// TODO: handle exception
			mv.addObject("STATUS", "F");
		}		
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/userdelete.do")
    public ModelAndView DeleteUser(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/userlist.do");    	
    	    	
    	viewService.deleteUser(commandMap.getMap());
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/luainsert.do")
    public ModelAndView InsertLua(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaList");;    	    
    	if (commandMap.get("SEQ_NO").toString().equals("0")) {		
	    	viewService.insertLua(commandMap.getMap());
    	}
    	else {    		    		
			viewService.updateLua(commandMap.getMap());
		}
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/luadelete.do")
    public ModelAndView DeleteLua(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/lualist.do");    	
    	    	
    	viewService.deleteLua(commandMap.getMap());
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertlanguage.do")
    public ModelAndView InsertLanguage(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/languagewrite.do");    	    
    	
    	viewService.insertLanguage(commandMap.getMap());
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deletelanguage.do")
    public ModelAndView deleteLanguage(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
					
		viewService.deleteLanguage(commandMap.getMap());
		
		mv.addObject("status", "OK");
		return mv;
    }
	
	@RequestMapping(value="/view/selectcountmng.do")
    public ModelAndView selectCountMng(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/CountList");    	    
    	
    	Map<String, Object> item = viewService.selectCountMng(commandMap.getMap());
    	
    	mv.addObject("item", item);
    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/updatecountmng.do")
    public ModelAndView updateCountMng(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/selectcountmng.do");    	    
    	
    	viewService.updateCountMng(commandMap.getMap());
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/insertuserdomain.do")
    public ModelAndView insertuserdomain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		if (commandMap.get("FLAG").toString().equals("I")) {
			viewService.insertUserDomain(commandMap.getMap());
		}
		else {			
			viewService.deleteUserDomain(commandMap.getMap());
		}
		return mv;
    }
	
	@RequestMapping(value="/view/logslist.do")
    public ModelAndView openLogsList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/logs/LogsList");    	
    	
    	//List<Map<String, Object>> delprjlist = viewService.SelectProjectDelete(commandMap.getMap());		
		//mv.addObject("delprjlist", delprjlist);
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/getdomainlogs.do")
    public ModelAndView getDomainLogs(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		try {
			mv.addObject("MSG", viewService.getDomainLogs(commandMap.getMap(), request));
			mv.addObject("STATUS", "OK");
			mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		} catch (Exception e) {
			// TODO: handle exception
			mv.addObject("STATUS", "ERROR");
		}
		
		return mv;
    }
	
	@RequestMapping(value="/view/samplelist.do")
    public ModelAndView SampleList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/sample/SampleList");    	
    	
    	List<Map<String, Object>> list = viewService.SelectSampleDomain(commandMap.getMap());		
		mv.addObject("list", list);
    	
		mv.addObject("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/insertsampledomain.do")
    public ModelAndView insertSampleDomain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/samplelist.do");    	
    	
    	viewService.insertSampleDomain(commandMap.getMap());				
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/deletesampledomain.do")
    public ModelAndView deleteSampleSomain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/samplelist.do");    	
    	
    	viewService.deleteSampleDomain(commandMap.getMap());				
    	    	
    	return SetMenu(mv, commandMap, request, response);
    }
	
	@RequestMapping(value="/view/chatbotmain.do")
    public ModelAndView ChatbotMain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/chatbot/ChatbotMain");
		
		List<Map<String, Object>> list = viewService.selectChatbotObject(commandMap.getMap());
    	mv.addObject("OBJ_LIST", list);
		return mv;
    }
	
	@RequestMapping(value="/view/savechatbottalk.do")
    public ModelAndView SaveChatbotTalk(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		mv.addObject("STATUS", viewService.SaveChatbotTalk(commandMap.getMap(), request));
		return mv;
    }
	
	@RequestMapping(value="/view/popchatbottalk.do")
    public ModelAndView LoadChatbotTalk(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/chatbot/ChatbotLog");
		
		mv.addObject("STR", viewService.LoadChatbotTalk(commandMap.getMap(), request));
		return mv;
    }	
	
	@RequestMapping(value="/view/popchatbotlean.do")
    public ModelAndView LoadChatbotLean(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/chatbot/ChatbotLean");
		
		List<Map<String, Object>> list = viewService.selectChatbotTrainDetail(commandMap.getMap());
		mv.addObject("TRAIN_LIST", list); 
		return mv;
    }
	
	@RequestMapping(value="/view/insertchatbotobject.do")
    public ModelAndView insertChatbotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
    	viewService.insertChatbotObject(commandMap.getMap(), request);
    	
    	mv.addObject("STATUS", true);
    	return mv;
    }
	
	@RequestMapping(value="/view/deletechatbotobject.do")
    public ModelAndView deleteChatbotObject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteChatbotObject(commandMap.getMap());
		viewService.deleteChatbotObjectDetail(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/getchatbotobjectdetail.do")
    public ModelAndView getChatbotObjectDetail(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	List<Map<String, Object>> list = viewService.selectChatbotObjectDetail(commandMap.getMap());
    	mv.addObject("DETAIL_LIST", list); 
    	mv.addObject("PARENT_SEQ_NO", commandMap.get("PARENT_SEQ_NO"));
    	return mv;
    }
	
	@RequestMapping(value="/view/insertchatbotlean.do")
    public ModelAndView insertChatbotLean(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/popchatbotlean.do");
		
		viewService.insertChatbotLean(commandMap.getMap());
		mv.addObject("USER_SEQ", commandMap.get("USER_SEQ"));
		return mv;
    }
	
	@RequestMapping(value="/view/deletechatbotlean.do")
    public ModelAndView deleteChatbotLean(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/popchatbotlean.do");
		
		viewService.deleteChatbotLean(commandMap.getMap());
		mv.addObject("USER_SEQ", commandMap.get("USER_SEQ"));
		return mv;
    }
	
	@RequestMapping(value="/view/savechatbotlean.do")
    public ModelAndView saveChatbotLean(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
    	viewService.saveChatbotObject(commandMap.getMap(), request);
    	
    	mv.addObject("STATUS", true);
    	return mv;
    }
	
	@RequestMapping(value="/view/logview.do")
    public ModelAndView openLogView(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/UiLog");    	
    	    	
    	return mv;
    }
		
	@RequestMapping(value="/view/tutordomainlist.do")
    public ModelAndView openTutorDomainList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/tutor/ProjectList");    	    			    	
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){                
                if (cookies[i].getName().equals("USER_TYPE")) {
                	commandMap.put("USER_TYPE", cookies[i].getValue());
                	mv.addObject("USER_TYPE", cookies[i].getValue());
				}
            }
        }
    	if (commandMap.get("USER_TYPE").toString().equals("SA")) {
    		List<Map<String, Object>> delprjlist = viewService.SelectTutorProjectDelete(commandMap.getMap());		
    		mv.addObject("delprjlist", delprjlist);
		}
    	    	
    	return SetTutorMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/tutorprojectwrite.do")
    public ModelAndView openTutorProjectdWrite(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/tutor/ProjectWrite");
    	
    	CommandMap prjCmd = new CommandMap();
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){
    		
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_SEQ")) {                	
                	prjCmd.put("USER_SEQ", cookies[i].getValue());
				}
            }                                                
        }
    	
    	List<Map<String, Object>> prjlist = viewService.SelectTutorCreateProject(prjCmd.getMap());
    	commandMap.put("LANG_SEQ_NO", 0);
    	//List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    	
    	mv.addObject("VERSION_LIST", prjlist);
    	//mv.addObject("LANG_LIST", langlist);
    	
    	return SetTutorMenu(mv, commandMap, request);
    }
		
	@RequestMapping(value="/view/tutorprojectupdate.do")
    public ModelAndView openTutorProjectdUpdate(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/tutor/ProjectWrite");
    
    	List<Map<String, Object>> prjlist = SetTutorProject(Integer.parseInt(commandMap.get("SEQ_NO").toString()), request);
    	
    	if(prjlist.size() > 0) {
    		mv.addObject("SEQ_NO", prjlist.get(0).get("SEQ_NO"));
    		mv.addObject("PROJECT_NAME", prjlist.get(0).get("PROJECT_NAME"));
    		mv.addObject("DESCRIPTION", prjlist.get(0).get("DESCRIPTION"));
    		mv.addObject("USER_SEQ_NO", prjlist.get(0).get("USER_SEQ_NO"));
    		mv.addObject("LANG_SEQ_NO", prjlist.get(0).get("LANG_SEQ_NO"));    		
    		commandMap.put("LANG_SEQ_NO", 0);
    		/*List<Map<String,Object>> langlist = viewService.selectLanguage(commandMap.getMap());
    		    		
    		mv.addObject("LANG_LIST", langlist);*/
    		
    	}

    	return SetTutorMenu(mv, commandMap, request);
    }	
	
	@RequestMapping(value="/view/inserttutorproject.do")
    public ModelAndView insertTutorProject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/settutorprojectlist.do");    	
    	
    	if (commandMap.get("SEQ_NO").toString().isEmpty()) {
    		commandMap.put("TYPE", "I");     		    
    		CommandMap cmmap = new CommandMap();
    		
    		Cookie[] cookies = request.getCookies();
        	if(cookies != null){
        		
                for(int i=0; i < cookies.length; i++){
                    if (cookies[i].getName().equals("USER_SEQ")) {
                    	commandMap.put("USER_SEQ", cookies[i].getValue());
                    	cmmap.put("USER_SEQ", cookies[i].getValue());
    				}
                }                          
            }
        	request.getSession().setAttribute("TutorProjectLangNo", commandMap.get("LANG_SEQ_NO"));
        	
        	viewService.insertTutorProject(commandMap.getMap(), request);
			
			if(cookies != null){
        		cmmap.put("PROJECT_SEQ", commandMap.get("SEQ_NO"));        	
            	viewService.insertTutorUserDomain(cmmap.getMap());            	
			}
		}
		else {
			viewService.updateTutorProject(commandMap.getMap(), request);
		}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	mv.addObject("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
    	mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
    	mv.addObject("PROJECT_USER_SEQ", commandMap.get("PROJECT_USER_SEQ"));
    	//SetProject(0, request);
    	return mv;
    }
	
	@RequestMapping(value="/view/selecttutorprojectname.do")
    public ModelAndView selectTutorProjectName(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		List<Map<String, Object>> prjlist = viewService.SelectTutorProjectName(commandMap.getMap());
		
		mv.addObject("list", prjlist);
    	return mv;
    }
		
	@RequestMapping(value="/view/delettutorproject.do")
    public ModelAndView deleteTutorProject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/tutordomainlist.do");    	
    	
    	if (commandMap.get("DELETE_FLAG").toString().equals("N")) {
    		commandMap.put("DELETE_FLAG", "Y");
    		viewService.updateTutorProjectDeleteFlag(commandMap.getMap());
		}
    	else {
    		request.getSession().setAttribute("TutorProjectLangNo", commandMap.get("LANG_SEQ_NO"));
    		viewService.deleteTutorProject(commandMap.getMap(), request);
    	}
    	    	
    	return mv;
    }		
	
	@RequestMapping(value="/view/updatetutorprojectdelflag.do")
    public ModelAndView updateTutorProjectDelFlag(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/tutordomainlist.do");    	
    	
    	viewService.updateTutorProjectDeleteFlag(commandMap.getMap());
    	    	
    	return mv;
    }

	@RequestMapping(value="/view/dialogmap.do")
    public ModelAndView selectTutorDialogMap(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/tutor/DialogMap");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("TutorProjectNo"));    	
    	
		List<Map<String, Object>> filelist = viewService.SelectTutorDialogMap(commandMap.getMap());
		
		mv.addObject("filelist", filelist);    	
		mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
		
    	return SetTutorMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertdialogmapfile.do")
    public ModelAndView insertDialogMapFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/dialogmap.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("TutorProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
		
		try {
			viewService.insertTutorDialogMap(commandMap.getMap(), request);
			commandMap.put("UPLOAD_STATUS", "S");
		} catch (Exception e) {
			// TODO: handle exception
			commandMap.put("UPLOAD_STATUS", "F");
		}
		
		mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
    	return mv;
    }
	
	@RequestMapping(value="/view/deletedialogmapfile.do")
    public ModelAndView deleteDialogMapFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/dialogmap.do");		
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
		
		viewService.deleteTutorDialogMap(commandMap.getMap(), request);
		
		mv.addObject("LANG_SEQ_NO", commandMap.get("LANG_SEQ_NO"));
    	return mv;
    }
	
	@RequestMapping(value="/view/opendialogfile.do")
	public ModelAndView openDialogFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/tutor/FileView");					
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("TutorProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
		
		mv.addObject("CON", viewService.openDialogFile(commandMap.getMap(), request));
    	return mv;
	}
	
	@RequestMapping(value="/view/dialogtrain.do")
    public ModelAndView DialogTrain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("TutorProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
						
		try {
			viewService.updateTutorDialogMap(commandMap.getMap(), request);
			mv.addObject("STATUS", "S");
			mv.addObject("MSG", commandMap.get("MSG"));
		} catch (Exception e) {
			// TODO: handle exception
			mv.addObject("STATUS", "F");
		}
			
		return mv;
    }
	
	@RequestMapping(value="/view/getcountcheck.do")
    public ModelAndView getCountCheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");					
		Map<String, Object> item = viewService.selectCountMng(commandMap.getMap());				
		Map<String, Object> cntchk = viewService.selectCountCheck(commandMap.getMap());

		mv.addObject("CNT", Integer.parseInt(cntchk.get("CNT").toString()));
		mv.addObject("MAX_CNT", Integer.parseInt(item.get(commandMap.get("COUNT_NAME")).toString()));

		return mv;
    }
	
	@RequestMapping(value="/view/gettutordialogmission.do")
    public ModelAndView getTutorDialogMission(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView("jsonView");		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("TutorProjectNo"));
		commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
						
		try {			
			mv.addObject("STATUS", "S");
			mv.addObject("STR", viewService.getTutorDialogMission(commandMap.getMap(), request));
			mv.addObject("FILE_EXISTS", commandMap.get("FILE_EXISTS"));
		} catch (Exception e) {
			// TODO: handle exception
			mv.addObject("STATUS", "F");
		}
		
		return mv;
    }
	
	@RequestMapping(value="/view/tutorrunstart.do")
    public ModelAndView TuTorRunStart(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("TutorProjectName"));
    	
    	try {			
    		viewService.TuTorRunStart(commandMap.getMap(), request);
    		mv.addObject("STATUS", "S");
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage());
			mv.addObject("STATUS", "F");
		}
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/tutorruninput.do")
    public ModelAndView TutorRunInput(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	    	
    	    	    	
    	mv.addObject("LOG", viewService.TutorRunInput(commandMap.getMap(), request));
    	mv.addObject("STATUS", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/tutordialogstop.do")
    public ModelAndView TutorDialogStop(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	    	
    	    	    	
    	try {
    		viewService.TutorDialogStop(commandMap.getMap(), request);
        	mv.addObject("STATUS", "S");
		} catch (Exception e) {
			// TODO: handle exception
			mv.addObject("STATUS", "S");
		}
    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/login.go")
    public ModelAndView LogIn(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("login");
		
		return mv;
    }
	
	@RequestMapping(value="/view/join.go")
    public ModelAndView Join(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("join");
		
		mv.addObject("TYPE", commandMap.get("TYPE"));
		return mv;
    }
	
	@RequestMapping(value="/view/logincheck.go")
    public ModelAndView LoginCheck(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");	

		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();		
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));
		
		commandMap.put("USER_PWD", encpwd);
		
		Map<String,Object> userinfo = viewService.loginUser(commandMap.getMap());
		
		if (userinfo == null) {
			mv.addObject("STATUS", "FA");
		} 
		else {
			mv.addObject("STATUS", "OK");
			mv.addObject("userinfo", userinfo);
			request.getSession().setAttribute("ServerPort", viewService.GetPort(Ports.arrUsingPort));
			commandMap.put("LAST_LOGIN_TIME", CommonUtils.GetDateTime());
			viewService.updateUserLastLogin(commandMap.getMap());
		}	
		
		return mv;
    }
	
	@RequestMapping(value="/view/changepassword.do")
    public ModelAndView ChangePassword(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		

		mv.addObject("STATUS", "0");
		
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();		
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));		
		commandMap.put("USER_PWD", encpwd);
		
		Map<String,Object> userinfo = viewService.loginUser(commandMap.getMap());
		
		if (userinfo == null) {
			mv.addObject("STATUS", "1");
		}
		else {
			encpwd = codec.encode(aes256.aesEncode(commandMap.get("NEW_USER_PWD").toString()));
			commandMap.put("USER_PWD", encpwd);
			try {			
				viewService.updateUserPassword(commandMap.getMap());			
			} catch (Exception e) {
				// TODO: handle exception
				mv.addObject("STATUS", "2");
			}
		}
		
		return mv;
	}
	
	@RequestMapping(value="/view/noitce.do")
    public ModelAndView Notice(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/Notice");
		
		mv.addObject("STR", viewService.Notice(commandMap.getMap(), request));
		return mv;
    }
	
	@RequestMapping(value="/view/xmlviewer.do")
    public ModelAndView XmlViewer(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/XmlViewer");
		CommonUtils.ReplaceDevTag(commandMap);
		mv.addObject("STR", commandMap.get("STR"));
		return mv;
    }

    @RequestMapping(value="/view/xmlviewerEnglish.do")
    public ModelAndView xmlviewerEnglish(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
			ModelAndView mv = new ModelAndView("jsonView");
			commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));

			viewService.getDevLogEnglish(commandMap.getMap(), request);

			mv.addObject("STR", commandMap.get("STR"));
			return mv;
    }
	
	@RequestMapping(value="/view/manual.go")
    public ModelAndView Manual(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/Manual");
				
		return mv;
    }
	
	@RequestMapping(value="/view/manualcontent.go")
    public ModelAndView ManualContent(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{		
		ModelAndView mv;
		if (commandMap.get("type").toString().equals("menu"))
			mv = new ModelAndView("/popup/ManualContent");	
		else
			mv = new ModelAndView("/popup/ManualAppendix");
		return mv;
    }
	
	@RequestMapping(value="/view/openchangepassword.do")
    public ModelAndView OpenChangePassword(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/ChangePassword");
		
		return mv;
    }
	
	@RequestMapping(value="/view/logout.go")
    public ModelAndView LogOut(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/main");
		request.getSession().invalidate();
		
		/*Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_ID") ||
                		cookies[i].getName().equals("USER_SEQ") ||
                		cookies[i].getName().equals("USER_NAME") ||
                		cookies[i].getName().equals("USER_TYPE") ||
                		cookies[i].getName().equals("UNIQID")) {
                	cookies[i].setMaxAge(0);
                	cookies[i].setPath("/");
                	
                	response.addCookie(cookies[i]);
				}
            }
        }*/
		return mv;
    }
	
	@RequestMapping(value="/view/main.go")
    public ModelAndView GoMain(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/main");

    	return SetMenu(mv, commandMap, request, response);
    }

	@RequestMapping(value="/view/mainIframe.go")
	public ModelAndView GoMainIframe(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/main");
		if (request.getSession().getAttribute("BIZ_CHATBOT") == null) {
			mv = new ModelAndView("redirect:/view/intro.go");
		} else if (request.getSession().getAttribute("ProjectName") != null) {
			mv = new ModelAndView("redirect:/view/agentlist.do");
		} else {
			mv = new ModelAndView("redirect:/view/projectlist.do");
		}
		return mv;
	}
	
	@RequestMapping(value="/view/intro.go")
    public ModelAndView GoIntro(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/intro");    	    
    	
    	mv.addObject("USER_TYPE", commandMap.get("USER_TYPE"));
    	return mv;
    }
	
	@RequestMapping(value="/view/migration.go")
    public ModelAndView Migration(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/main");    	    
    	viewService.updateMigration(commandMap.getMap());    	
    	
    	return mv;
    }


    // Biz-Chabot(maum chatbot) 사용자가 로그인하는 API
	@RequestMapping(value="/view/loginmodule.do")
	public ModelAndView loginmodule(HttpServletRequest request, HttpServletResponse response) throws Exception{

		ModelAndView mv = new ModelAndView("/main");

		Map map = new HashMap();
		map.put("USER_ID", "maumChatbot");
		Map resMap = viewService.getBizChatbotUser(map);

		int maxAge = 60 * 60 * 24 * 90;

		if (resMap != null) {

			String name = URLEncoder.encode(resMap.get("USER_NAME").toString(), "UTF-8");
      Cookie userTypeC = new Cookie("USER_TYPE", resMap.get("USER_TYPE").toString());
      Cookie userSeqC = new Cookie("USER_SEQ", resMap.get("SEQ_NO").toString());
      Cookie userIdC = new Cookie("USER_ID", resMap.get("USER_ID").toString());
      Cookie userNameC = new Cookie("USER_NAME", name);
      Cookie apiKeyC = new Cookie("API_KEY", resMap.get("API_KEY").toString());
      //Cookie uniqueIdC = new Cookie("UNIQID", "");

      userTypeC.setMaxAge(maxAge);
      userSeqC.setMaxAge(maxAge);
      userIdC.setMaxAge(maxAge);
      userNameC.setMaxAge(maxAge);
      apiKeyC.setMaxAge(maxAge);
      //uniqueIdC.setMaxAge(maxAge);

      userTypeC.setPath("/");
      userSeqC.setPath("/");
      userIdC.setPath("/");
      userNameC.setPath("/");
      apiKeyC.setPath("/");
      //uniqueIdC.setPath("/");

      response.addCookie(userTypeC);
      response.addCookie(userSeqC);
      response.addCookie(userIdC);
      response.addCookie(userNameC);
      response.addCookie(apiKeyC);
      //response.addCookie(uniqueIdC);
    }

		request.getSession().setAttribute("BIZ_CHATBOT", "FROM_BIZ_CHATBOT");

		return mv;
	}
}
