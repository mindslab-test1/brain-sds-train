package first.view.service;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jdom.output.Format;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import first.common.common.CommandMap;
import first.common.util.ColorObject;
import first.common.util.CommonUtils;
import first.common.util.ExcelUtils;
import first.common.util.FileUtils;
import first.common.util.Ports;
import first.common.util.ReadOption;
import first.common.util.SlotObject;
import first.common.util.UserSaySortObj;
import first.view.dao.ViewDAO;


@Service("viewService")
public class ViewServiceImpl implements ViewService {
Logger log = Logger.getLogger(this.getClass());
int idx = 1;
StringBuilder sbControl;
Boolean bServer = true;

	@Resource(name="viewDAO")
	private ViewDAO viewDAO;

	@Resource(name="fileUtils")
	private FileUtils fileUtils;

	private Map<String, String> slotConversionMap;

	public String GetPath(String Type, Boolean LoginChk, HttpServletRequest request) {
		String apiKey = "";
		if (LoginChk) //로그인된 유저의 API
			apiKey = GetCookie("API", request);	
		else //선택된 프로젝트 유저의 API
			apiKey = GetCookie("SELECT_API", request);
		if (bServer) {						
			if (apiKey.equals("null"))				
				return String.format("%s/KB/%s/dialog_domain/", request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie(Type + "L", request));
			else
				return String.format("%s/KB/%s/dialog_domain/%s/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie(Type + "L", request), apiKey.substring(0, 3), apiKey);
		}
		else {
			if (apiKey.equals("null"))							
				return "E:\\dev\\file\\webtool\\www_data\\KB\\" + GetCookie(Type + "L", request) + "\\dialog_domain\\";
			else
				return String.format("E:\\dev\\file\\webtool\\www_data\\KB\\%s\\dialog_domain\\%s\\%s\\", GetCookie(Type + "L", request), apiKey.substring(0, 3), apiKey);
		}
	}	
	
	private String GetCookie(String Type, HttpServletRequest request) {
		String returnvalue = "";
		String LoginUserSeq = "";
		Cookie[] cookies = request.getCookies();    	
		
		if (Type.equals("API")) {    		
    		if(cookies != null){            
                for(int i=0; i < cookies.length; i++){            	
                	if (cookies[i].getName().equals("API_KEY")) {
            			returnvalue = cookies[i].getValue();
    				}
                }
            }
    		if (returnvalue.isEmpty())
    			returnvalue = "null";
		}
    	else if (Type.equals("SELECT_API")) {    		    		
    		if(cookies != null){            
                for(int i=0; i < cookies.length; i++){            	
                	if (cookies[i].getName().equals("ProjectApiKey")) {
            			returnvalue = cookies[i].getValue();
    				}
                }
            }
    		if (returnvalue.isEmpty())
    			returnvalue = "null";			
		}
    	else if (Type.contains("U")) {
    		if(cookies != null){            
                for(int i=0; i < cookies.length; i++){                	
                	if (Type.equals("DU")) {
                		if (cookies[i].getName().equals("ProjectUserSeq")) {
                			returnvalue = cookies[i].getValue();
        				}
                		else if (cookies[i].getName().equals("USER_SEQ")) {
                			LoginUserSeq = cookies[i].getValue();
                		}
                	}
                	else {
                		if (cookies[i].getName().equals("TutorProjectUserSeq")) {
                			returnvalue = cookies[i].getValue();
        				}
                		else if (cookies[i].getName().equals("USER_SEQ")) {
                			LoginUserSeq = cookies[i].getValue();
                		}
                	}
                }
            }
    		
    		if (returnvalue.isEmpty()) {
    			returnvalue = LoginUserSeq;
			}
		}
    	else if (Type.contains("L")) {
    		returnvalue = "KOR";
    		if (Type.equals("DL")) {
    			if (request.getSession().getAttribute("ProjectLangNo").toString().equals("2")) {
        			returnvalue = "ENG";
    			}
			}
    		else {
    			if (request.getSession().getAttribute("TutorProjectLangNo").toString().equals("2")) {
        			returnvalue = "ENG";
    			}
    		}    		
		}    	
    	return returnvalue;
	}
		
	public String GetDomainName(String Type, Boolean LoginChk, Map<String, Object> map, HttpServletRequest request) {
		String apiKey = "";
		if (LoginChk) //로그인된 유저의 API
			apiKey = GetCookie("API", request);	
		else //선택된 프로젝트 유저의 API
			apiKey = GetCookie("SELECT_API", request);
		
		if (apiKey.equals("null")) {
			String TypeValue = "IS";
			if (Type.equals("T")) {
				TypeValue = "ED";
			}
			return String.format("%s-%s-@-%s", TypeValue, GetCookie(Type + "U", request), map.get("PROJECT_NAME").toString());	
		}
		else {
			return map.get("PROJECT_NAME").toString();
		}
	}
	
	@Override
	public void CreateTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.CreateTable(map);
	}
	
	@Override
	public void DropTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.DropTable(map);
	}
	
	@Override
	public void CreateColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.CreateColumn(map);
	}
	
	private void CreateMultiColumn(Map<String, Object> map, JSONObject jsonObj) {
		// TODO Auto-generated method stub	
		log.debug(jsonObj);
		log.debug(map.get("TYPE_SEQ").toString());
		if (jsonObj.containsKey(map.get("TYPE_SEQ").toString())) {
			JSONArray objArray = (JSONArray)jsonObj.get(map.get("TYPE_SEQ").toString());	
			int tempsum = Integer.parseInt(map.get("SEQ_NO").toString()) + Integer.parseInt(map.get("SLOT_SEQ_NO").toString());			
			ColRecursive(map, jsonObj, objArray, 1, tempsum, String.format("%s.%s.", map.get("CLASS_NAME"), map.get("SLOT_NAME")), String.format("%s.%s.", map.get("SEQ_NO"), map.get("SLOT_SEQ_NO")), String.format("%s_%s", map.get("SEQ_NO"), map.get("SLOT_SEQ_NO")), "");	
		}		
	}
	
	@SuppressWarnings("unused")
	private void ColRecursive(Map<String, Object> map, JSONObject jsonObj, JSONArray objArray, int Type, int typeSum, String SlotName, String SlotNameSeq, String ParentClassSeq, String SlotSeq) {
		CommandMap SlotObjMap = new CommandMap();
		if (!SlotSeq.isEmpty()) {
			ParentClassSeq = String.format("%s_%s", ParentClassSeq, SlotSeq);
		}
		
		for(int i = 0 ; i < objArray.size() ; i++) {
			JSONObject SlotObj = (JSONObject)objArray.get(i);			
			String slotname;
			String slotnameseq;
			String SubSlotSeq = "";
			String slotcolname;
						
			int tempsum = typeSum + Integer.parseInt(SlotObj.get("SEQ_NO").toString());			
			if (SlotObj.get("SLOT_TYPE").toString().equals("C")) {							
				ColRecursive(map, jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), Type, tempsum, SlotName + SlotObj.get("SLOT_NAME").toString() + ".", SlotNameSeq + SlotObj.get("SEQ_NO").toString() + ".", ParentClassSeq, SlotObj.get("SEQ_NO").toString());
			}
			else {
				SubSlotSeq = String.format("%s_%s_%s", ParentClassSeq, SlotObj.get("CLASS_SEQ_NO"), SlotObj.get("SEQ_NO"));				
				slotname = SlotName + SlotObj.get("SLOT_NAME");
				slotnameseq = SlotNameSeq + SlotObj.get("SEQ_NO");
				slotcolname = String.format("%s_%s_%s_%s", map.get("SEQ_NO"), map.get("SLOT_SEQ_NO"), typeSum, SlotObj.get("SEQ_NO"));
				
				SlotObjMap.put("SEQ_NO", map.get("SEQ_NO"));
				SlotObjMap.put("COL_NAME", slotcolname);
				SlotObjMap.put("COL_TYPE", "varchar(200)");
				SlotObjMap.put("SLOT_SUB_SEQ", SubSlotSeq);
				SlotObjMap.put("SLOT_SUB_NAME", slotname);
				SlotObjMap.put("SLOT_SUB_NAME_SEQ", slotnameseq);
				SlotObjMap.put("SLOT_COLUMN_NAME", slotcolname);
				SlotObjMap.put("SLOT_SEQ_NO", SlotObj.get("SEQ_NO"));
				SlotObjMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
				
				if (Type == 1) {
					viewDAO.insertSlotSub(SlotObjMap.getMap());
					viewDAO.CreateColumn(SlotObjMap.getMap());
				}
				else {
					viewDAO.deleteSlotSub(SlotObjMap.getMap());
					viewDAO.DropColumn(SlotObjMap.getMap());
				}
			}
		}
	}
	
	@Override
	public void AlterColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.AlterColumn(map);
	}
	
	@Override
	public void DropColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.DropColumn(map);
	}
	
	public void DropMultiColumn(Map<String, Object> map, JSONObject jsonObj) {
		// TODO Auto-generated method stub
		JSONArray objArray = (JSONArray)jsonObj.get(map.get("TYPE_SEQ").toString());
		log.debug(map);
		int tempsum = Integer.parseInt(map.get("SEQ_NO").toString()) + Integer.parseInt(map.get("SLOT_SEQ_NO").toString());			
		ColRecursive(map, jsonObj, objArray, 2, tempsum, String.format("%s.%s.", map.get("CLASS_NAME"), map.get("SLOT_NAME")), String.format("%s.%s.", map.get("SEQ_NO"), map.get("SLOT_SEQ_NO")), String.format("%s_%s", map.get("SEQ_NO"), map.get("SLOT_SEQ_NO")), "");		
	}
	
	@Override
	public void insertProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub		
		if (!map.containsKey("PARENT_SEQ_NO")) {
			map.put("PARENT_SEQ_NO", 0);
		}
		if (!map.containsKey("FROM_SEQ_NO")) {
			map.put("FROM_SEQ_NO", 0);
		}
		if (!map.containsKey("LANG_SEQ_NO")) {
			map.put("LANG_SEQ_NO", 1);
		}
		
		if (!map.get("TYPE").toString().equals("L")) {
			viewDAO.insertProject(map);

			if (map.get("TYPE").toString().equals("I")) {

				// 영어 모델인 경우 영어 basic 코퍼스 insert
				if (map.get("LANG_SEQ_NO").toString().equals("2")) {
					viewDAO.insertBasicIntentsEnglish(map);
				} else {
					viewDAO.insertBasicIntents(map);
				}
			}			
		}
		else {
			viewDAO.insertProject2(map);
		}
		
		//viewDAO.updatePorts(map);				
		
		String emptyPath = "";
		String copyPath = "";
		String domainName = GetDomainName("D", true, map, request);
		
		if (!map.get("TYPE").toString().equals("V") && !map.get("TYPE").toString().equals("L")) {
			//emptyPath = GetPath("D", request) + "empty";
			if (bServer)
				emptyPath = String.format("%s/KB/%s/dialog_domain/empty", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG");	
			else
				emptyPath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\empty";
			
			String tempPath = "";
			
			String apiKey = "";
			
			if (!map.get("TYPE").toString().equals("C"))
				apiKey = GetCookie("API", request);
			else
				apiKey = map.get("USER_API_KEY").toString();			
			
			if (apiKey.isEmpty()) {
				apiKey = "null";
			}
			log.debug("=======================0");
			log.debug(apiKey);
			log.debug("=======================1");
			
			if (!apiKey.equals("null")) { //폴더 생성
				if (bServer)
					tempPath = String.format("%s/KB/%s/dialog_domain/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3));
				else
					tempPath = String.format("E:\\dev\\file\\webtool\\www_data\\KB\\%s\\dialog_domain\\%s\\", (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3));
				
				File t = new File(tempPath);
				if(!t.exists()) {
					t.mkdir();
					
					tempPath += apiKey + "/"; 
					t = new File(tempPath);
					
					if(!t.exists()) {
						t.mkdir();
					}
				}		
			}
			
			if (map.get("TYPE").toString().equals("C")) {
				if (bServer) {
					if (apiKey.equals("null"))
						tempPath = String.format("%s/KB/%s/dialog_domain/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG");					
					else
						tempPath = String.format("%s/KB/%s/dialog_domain/%s/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3), apiKey);											
				}
				else {
					if (apiKey.equals("null"))
						tempPath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\";
					else
						tempPath = String.format("E:\\dev\\file\\webtool\\www_data\\KB\\%s\\dialog_domain\\%s\\%s\\", (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3), apiKey);
				}

				if (apiKey.equals("null"))
					domainName = String.format("IS-%s-@-%s", map.get("USER_SEQ"), map.get("PROJECT_NAME").toString());
				else
					domainName = map.get("PROJECT_NAME").toString();
				copyPath = tempPath + domainName;
			}
			else {
				copyPath = GetPath("D", true, request) + domainName;
			}
			
			log.debug(emptyPath);
			log.debug(copyPath);
			
			File s = new File(emptyPath);
			File t = new File(copyPath);
			if(!t.exists()) {
				t.mkdir();						
			}
			
			log.debug("=======================2");
			Directorycopy(s, t);			
			log.debug("=======================3");
			File oldsql = new File(copyPath + "/empty_DB.sqlite3");
			File renamesql = new File(copyPath + "/" + domainName + "_DB.sqlite3");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.task.txt");
			renamesql = new File(copyPath + "/" + domainName + ".task.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.slot.txt");
			renamesql = new File(copyPath + "/" + domainName + ".slot.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.dialogLib.txt");
			renamesql = new File(copyPath + "/" + domainName + ".dialogLib.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.DAtype.txt");
			renamesql = new File(copyPath + "/" + domainName + ".DAtype.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.svm");
			renamesql = new File(copyPath + "/" + domainName + ".svm");
			oldsql.renameTo(renamesql);
		}
	}
	
	@Override
	public void insertTutorProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub				
		if (!map.containsKey("LANG_SEQ_NO")) {
			map.put("LANG_SEQ_NO", 1);
		}
		
		viewDAO.insertTutorProject(map);
		
		String copyPath = "";
		copyPath = GetPath("T", true, request) + GetDomainName("T", true, map, request);
		//copyPath = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString());						
		File t = new File(copyPath);
		log.debug(copyPath);
		if(!t.exists()) {
			t.mkdir();
		}		
	}
	
	@Override
	public void insertSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("NEW_CHECK", "Y");
		viewDAO.insertSlotClass(map);
	}
	
	@Override
	public void insertSlot(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub		
		String ColType = "varchar(200)";//주석품
		
		if (!map.get("SLOT_TYPE").toString().equals("C")) {			
			List<Map<String,Object>> slotlist = viewDAO.selectSlotColor2(map);			
			String color = SlotColorCheck(map, slotlist, 0);			
			map.put("COLOR", color);
		}
		
		viewDAO.insertSlot(map);
		SlotDefine(map);		
		
		if (!map.containsKey("SUB_SLOT_SEQ")) {
			CommandMap cdb = new CommandMap();			
			cdb.put("SEQ_NO", map.get("CLASS_SEQ_NO"));			
			cdb.put("COL_TYPE", ColType); //주석품		
			cdb.put("TYPE_SEQ", map.get("TYPE_SEQ"));
			cdb.put("CLASS_NAME", map.get("CLASS_NAME"));
			cdb.put("SLOT_NAME", map.get("SLOT_NAME"));
			cdb.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
			
			JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObj = null;
	        JSONArray objArray;			
			List<Map<String, Object>> list = null;
			try {
				list = selectSlotTypeClass(map);
				jsonObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			cdb.put("SLOT_SEQ_NO", map.get("SEQ_NO"));
			cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SEQ_NO"));
			
			if (map.get("SLOT_TYPE").toString().equals("C")) {
				insertSlotTypeClass(map);
				CreateMultiColumn(cdb.getMap(), jsonObj);
			}
			else {
				CreateColumn(cdb.getMap());
			}			
			
			int tempsum = 0;
			String[] tempseq;
			cdb.remove("SLOT_NAME");
			
			List<Map<String, Object>> slist = selectSlot(cdb.getMap());
			jsonObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slist));
			//부모 슬롯들 작업			
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();
				cdb.put("SEQ_NO", item.get("CLASS_SEQ_NO"));
				cdb.put("COL_TYPE", ColType); //주석품
				cdb.put("CLASS_NAME", item.get("CLASS_NAME"));
				cdb.put("SLOT_SEQ_NO", item.get("SLOT_SEQ_NO"));
				cdb.put("SLOT_NAME", item.get("SLOT_NAME"));
				cdb.put("COL_NAME", item.get("CLASS_SEQ_NO") + "_" + item.get("SLOT_SEQ_NO"));				
				cdb.put("TYPE_SEQ", item.get("TYPE_SEQ"));
				if (map.get("SLOT_TYPE").toString().equals("C")) {
					CreateMultiColumn(cdb.getMap(), jsonObj);
				}
				else {
					tempsum = Integer.parseInt(item.get("CLASS_SEQ_NO").toString());
					tempseq = item.get("SUB_SLOT_SEQ").toString().split("_");
					for (int i = 0; i < tempseq.length; i++) {
						tempsum += Integer.parseInt(tempseq[i]);
					}
													
					cdb.put("SLOT_SUB_SEQ", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SUB_SLOT_SEQ"), map.get("CLASS_SEQ_NO"), map.get("SEQ_NO")));
					cdb.put("SLOT_SUB_NAME", String.format("%s.%s.%s", item.get("CLASS_NAME"), item.get("SLOT_NAME"), map.get("SLOT_NAME")));
					cdb.put("SLOT_SUB_NAME_SEQ", String.format("%s.%s.%s", item.get("CLASS_SEQ_NO"), item.get("SUB_SLOT_SEQ").toString().replaceAll("_",  "."), map.get("SEQ_NO")));
					cdb.put("SLOT_COLUMN_NAME", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SLOT_SEQ_NO"), tempsum, map.get("SEQ_NO")));
					cdb.put("SLOT_SEQ_NO", map.get("SEQ_NO"));			
					cdb.put("COL_NAME", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SLOT_SEQ_NO"), tempsum, map.get("SEQ_NO")));
					viewDAO.insertSlotSub(cdb.getMap());
					CreateColumn(cdb.getMap());
				}
			}
			
			log.debug(map.get("TYPE_NAME"));
/*			if (map.get("TYPE_NAME").toString().equals("Sys.number")) {
				ExecSql(map, "I", request);
			}*/
		}				
	}
	
	/*private void ExecSql(Map<String, Object> map, String Type, HttpServletRequest request) throws IOException, InterruptedException {
		String Path = GetPath("D", false, request) + String.format("%s/%s", GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
		StringBuilder sbslotmapping = new StringBuilder();
		if (Type.equals("I")) { 
			sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s.%s', '%s', '%s', '%s', 'ORDER BY(CAST \"%s.%s\" AS FLOAT) ASC LIMIT 1');\n", 1, map.get("CLASS_NAME"), map.get("SLOT_NAME"), "min", "*", "_WEHER_EXTRA_", map.get("CLASS_NAME"), map.get("SLOT_NAME")));
			sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s.%s', '%s', '%s', '%s', 'ORDER BY(CAST \"%s.%s\" AS FLOAT) DESC LIMIT 1');\n", 1, map.get("CLASS_NAME"), map.get("SLOT_NAME"), "max", "*", "_WEHER_EXTRA_", map.get("CLASS_NAME"), map.get("SLOT_NAME")));
		}
		else {
			sbslotmapping.append(String.format("DELETE FROM slot_mapping WHERE tagged_slot_key = '%s.%s' AND tagged_slot_value = 'min';\n", map.get("CLASS_NAME"), map.get("SLOT_NAME")));
			sbslotmapping.append(String.format("DELETE FROM slot_mapping WHERE tagged_slot_key = '%s.%s' AND tagged_slot_value = 'max';\n", map.get("CLASS_NAME"), map.get("SLOT_NAME")));
		}
		sbslotmapping.append("COMMIT;");		
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".sql", "UTF-8");
		writer.write(sbslotmapping.toString());
		writer.close();		
		
		ExecSqlite(map, Path, request);
	}*/
	
	@Override
	public String selectSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub			
		Map<String, Object> obj = viewDAO.selectSlotColorCheck(map);
		
		if (obj != null) {
			return obj.get("COLOR").toString();
		}
		else {
			List<Map<String,Object>> slotlist = viewDAO.selectSlotColor2(map);
			
			String color = SlotColorCheck(map, slotlist, 0);
			map.put("COLOR", color);
			viewDAO.insertSlotColor(map);
			return color;
		}
	}
	
	private String SlotColorCheck(Map<String, Object> map, List<Map<String,Object>> slotlist, int cnt) {
		Boolean bcolorChk;
		String returnValue = "";
		for (int i = 0; i < ColorObject.arrColor.length; i++) {
			bcolorChk = true;
			for (Iterator iterator = slotlist.iterator(); iterator.hasNext();) {
				Map<String, Object> sitem = (Map<String, Object>) iterator.next();
				
				if (cnt == 0) {
					if (sitem.get("COLOR").equals(ColorObject.arrColor[i])) {
						bcolorChk = false;
						break;
					}					
				}
				else {					
					map.put("COLOR", ColorObject.arrColor[i]);
					Map<String, Object> obj = viewDAO.selectSlotColor(map);

					if (obj != null) {
						bcolorChk = false;
						break;
					} 
				}				
			}
			
			if (bcolorChk) {
				returnValue = ColorObject.arrColor[i];
				break;
			}
		}
		
		if (!returnValue.isEmpty())
			return returnValue;
		else
			return SlotColorCheck(map, slotlist, 1);		
	}
	
	private void SlotDefine(Map<String, Object> map) {
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap DefineObjMap;
        viewDAO.deleteSlotDefine(map);    
		try {			
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("DEFINE_ITEM"));
			
			if (jsonObj.containsKey("DEFINE_ITEM")) {
				JSONArray objArray = (JSONArray)jsonObj.get("DEFINE_ITEM");
							
				for(int i=0 ; i< objArray.size() ; i++){
	                JSONObject DefineObj = (JSONObject)objArray.get(i);
	                DefineObjMap = new CommandMap();
	                DefineObjMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
	                DefineObjMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
	                DefineObjMap.put("SLOT_SEQ_NO", map.get("SEQ_NO"));
	                DefineObjMap.put("TARGET_NAME", DefineObj.get("TARGET_NAME"));
	                DefineObjMap.put("TARGET_SEQ", DefineObj.get("TARGET_SEQ"));
	                DefineObjMap.put("CON", DefineObj.get("CON"));
	                DefineObjMap.put("ACT", DefineObj.get("ACT"));
	                DefineObjMap.put("TARGET_SUB_SEQ", DefineObj.get("TARGET_SUB_SEQ"));
	                DefineObjMap.put("SUB_SLOT_SEQ", map.get("SUB_SLOT_SEQ"));
	                
	                viewDAO.insertSlotDefine(DefineObjMap.getMap());                
	            }
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String UttrReplace(String Uttr) {
		Pattern p = Pattern.compile("(Intent\\([a-zA-Z0-9,.\\s\\-_=*\"]+(\\(\\))?(\\()?[a-zA-Z0-9,.\\s\\-_=*\"]+(\\))?[a-zA-Z0-9,.\\s\\-_=*\"]+\\))|(Value\\([a-zA-Z0-9가-힣,./\\s\\-_\"]+\\)\\s(==|~=|>=|<=|<|>)\\s([0-9]+|\"[a-zA-Z0-9가-힣,./\\s\\-_]+\"))|((Entity|Count|Instance|PrintDate|PrintTime|FinishedTask|PreviousTask|Value)\\([a-zA-Z0-9가-힣,./\\s\\-_\"]+(\"년월일\\(요일\\) 형식\")?\\))");
		Matcher m = p.matcher(Uttr);
		
		StringBuilder sb = new StringBuilder();
		String[] tempArr;
		String returnVal = Uttr;
		
		try {
	        while (m.find()) {
	        	sb.setLength(0);
	        	log.debug(m.group());        	        	
	        	if (m.group().contains("Intent(")) {
	        		tempArr = m.group().replace(" ", "").split(",");
	        		
	        		if (tempArr.length > 3) {
	        			for (int i = 0; i < tempArr.length; i++) {
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");
						}
	        			
	        			if (tempArr[2].equals("*")) {
	        				if (tempArr[1].contains("=value"))
	        					sb.append(String.format("%s, ", tempArr[0].replace("Intent", "IsDA").replace("user", "\"user\"").replace("system", "\"system\"")));
	        				else
	        					sb.append(String.format("%s, ", tempArr[0].replace("Intent", "IsDAType").replace("user", "\"user\"").replace("system", "\"system\"")));
	        				        				
		        			sb.append(String.format("\"%s\")==", tempArr[1]));
		        			sb.append(tempArr[3].substring(0, tempArr[3].length() -1));
						}
	        			else {
	        				if (tempArr[1].contains("=value"))
	        					sb.append(String.format("%s, ", tempArr[0].replace("Intent", "IsDAAtUtterHistory").replace("user", "\"user\"").replace("system", "\"system\"")));
	        				else
	        					sb.append(String.format("%s, ", tempArr[0].replace("Intent", "IsDATypeAtUtterHistory").replace("user", "\"user\"").replace("system", "\"system\"")));
		        			
		        			sb.append(String.format("\"%s\", ", tempArr[1]));
		        			sb.append(String.format("\"%s\")==", tempArr[2]));
		        			sb.append(tempArr[3].substring(0, tempArr[3].length() -1));
	        			}
					}
				}
	        	else if (m.group().contains("Entity")) {
	        		tempArr = m.group().replace(" ", "").split(",");	        			        	
	        		if (tempArr.length > 2) {
	        			for (int i = 0; i < tempArr.length; i++) {	        				
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");	        				
						}
	        			
	        			tempArr[2] = tempArr[2].substring(0, tempArr[2].length() -1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1 && tempArr[1].equals("user_history")) {
	    					sb.append(String.format("ExistValueFromDialog(\"%s\")==%s", tempArr2[1], tempArr[2]));
	        			}
	    				else if (tempArr2.length > 1 && tempArr[1].equals("system_response")) {
	    					sb.append(String.format("ExistValue(\"%s\")==%s", tempArr2[1], tempArr[2]));
	    				}	        			
					}
				}
	        	else if (m.group().contains("Count")) {
	        		tempArr = m.group().replace(" ", "").split(",");
	        		
	        		if (tempArr.length > 1) {
	        			for (int i = 0; i < tempArr.length; i++) {
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");
						}
	        			
	        			tempArr[1] = tempArr[1].substring(0, tempArr[1].length() - 1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1 && tempArr[1].equals("user_history")) {
	        				sb.append(String.format("CountHistory(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("system_response")) {
	        				sb.append(String.format("Count(\"%s\")", tempArr2[1]));
	        			}
					}
	        	}
	        	else if (m.group().contains("Value")) {
        			tempArr = m.group().split(",");
	        		if (tempArr.length > 1) {
	        			tempArr[0] = tempArr[0].replaceAll("\"", "");
	        			tempArr[1] = tempArr[1].trim();
	        			String[] tempArr1 = tempArr[1].split(" ");  
	        			tempArr1[0] = tempArr1[0].replaceAll("\"", "").replaceAll("\\)", "");;
	        			String tempStr = "";
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        				        			
	        			if (tempArr1.length > 3) {
	        				for (int i = 2; i < tempArr1.length; i++) {		        						        				
	        					tempStr += String.format("%s ", tempArr1[i]); 								
							}
	        				tempArr1[2] = tempStr.trim();
						}
	        				        			
	        			if (tempArr1.length > 2 && (tempArr1[1].equals("==") || tempArr1[1].equals("~="))) {
	        				if (tempArr2.length > 1 && tempArr1[0].equals("user_history")) {
	        					if (tempArr1[1].equals("==")) {
	        						sb.append(String.format("HasValueHistory(\"%s\", %s) == true", tempArr2[1], tempArr1[2]));
								}
	        					else {
	        						sb.append(String.format("HasValueHistory(\"%s\", %s) == false", tempArr2[1], tempArr1[2]));
	        					}
							}
	        				else if (tempArr2.length > 1 && tempArr1[0].equals("system_response")) {
	        					if (tempArr1[1].equals("==")) {
	        						sb.append(String.format("HasValue(\"%s\", %s) == true", tempArr2[1], tempArr1[2]));
								}
	        					else {
	        						sb.append(String.format("HasValue(\"%s\", %s) == false", tempArr2[1], tempArr1[2]));
	        					}
							}
	        			}
	        			else if (tempArr1.length > 2) {
	        				if (tempArr2.length > 1 && tempArr1[0].equals("user_history")) {
	        					sb.append(String.format("ValueHistory(\"%s\") %s %s", tempArr2[1], tempArr1[1], tempArr1[2]));	        					
	        				}
	        				else if (tempArr2.length > 1 && tempArr1[0].equals("system_response")) {
	        					sb.append(String.format("Value(\"%s\") %s %s", tempArr2[1], tempArr1[1], tempArr1[2]));
	        				}
	        					
						}
	        			else if (tempArr.length == 2 && tempArr1.length == 1) { //Action	        				
	        				tempArr2 = tempArr[0].split("\\(");
	        				
	        				if (tempArr2.length == 2) {
	        					if (tempArr1[0].equals("user_history")) {
		        					sb.append(String.format("ValueHistory(\"%s\")", tempArr2[1]));
								}	 
	        					else {
	        						sb.append(String.format("Value(\"%s\")", tempArr2[1]));	
								}		        				        			
							}
						}
					}	        		 
	        	}
	        	else if (m.group().contains("Instance")) {
	        		tempArr = m.group().replace(" ", "").split(",");
	        		
	        		if (tempArr.length > 1) {
	        			for (int i = 0; i < tempArr.length; i++) {
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");
						}
	        			
	        			tempArr[1] = tempArr[1].substring(0, tempArr[1].length() - 1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1) {
	        				sb.append(String.format("ExistValueAtDB(\"%s\") == %s", tempArr2[1], tempArr[1]));
	        			}
					}
	        	}
	        	else if (m.group().contains("PrintDate")) {
	        		tempArr = m.group().replaceAll(" ", "").replaceAll("\"", "").split(",");
	        			        		
	        		if (tempArr.length > 1) {
	        			tempArr[1] = tempArr[1].substring(0, tempArr[1].length() - 1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        				        				        				        			
	        			if (tempArr2.length > 1 && tempArr[1].equals("년월일형식")) {
	        				sb.append(String.format("PrintDate(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("년월일(요일)형식")) {
	        				sb.append(String.format("PrintDateWeek(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("오늘/내일형식")) {
	        				sb.append(String.format("PrintDateToday(\"%s\")", tempArr2[1]));
	        			}
					}
	        	}
	        	else if (m.group().contains("PrintTime")) {
	        		tempArr = m.group().replaceAll(" ", "").replaceAll("\"", "").split(",");        		
	        		
	        		if (tempArr.length > 1) {
	        			tempArr[2] = tempArr[2].substring(0, tempArr[2].length() - 1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1 && tempArr[1].equals("24시간표시") && tempArr[2].equals("초포함")) {
	        				sb.append(String.format("PrintTime(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("24시간표시") && tempArr[2].equals("초비포함")) {
	        				sb.append(String.format("PrintTimeWithoutSec(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("오전/오후표시") && tempArr[2].equals("초포함")) {
	        				sb.append(String.format("PrintTimeAMPM(\"%s\")", tempArr2[1]));
	        			}
	        			else if (tempArr2.length > 1 && tempArr[1].equals("오전/오후표시") && tempArr[2].equals("초비포함")) {
	        				sb.append(String.format("PrintTimeAMPMWithoutSec(\"%s\")", tempArr2[1]));
	        			}
					}
	        	}
	        	else if (m.group().contains("FinishedTask")) {
	        		tempArr = m.group().replace(" ", "").split(",");
	        		
	        		if (tempArr.length > 1) {
	        			for (int i = 0; i < tempArr.length; i++) {
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");
						}
	        			
	        			tempArr[1] = tempArr[1].substring(0, tempArr[1].length() - 1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1) {
	        				sb.append(String.format("FinishedTask(\"%s\") == %s", tempArr2[1], tempArr[1]));
	        			}
					}
	        	}
	        	else if (m.group().contains("PreviousTask")) {
	        		tempArr = m.group().replace(" ", "").split(",");	        			        	
	        		if (tempArr.length > 2) {
	        			for (int i = 0; i < tempArr.length; i++) {	        				
	        				tempArr[i] = tempArr[i].replaceAll("\"", "");	        				
						}
	        			
	        			tempArr[2] = tempArr[2].substring(0, tempArr[2].length() -1);
	        			String[] tempArr2 = tempArr[0].split("\\(");
	        			
	        			if (tempArr2.length > 1) {
	    					sb.append(String.format("PreviousTask(\"%s\", \"%s\") == %s", tempArr2[1], tempArr[1], tempArr[2]));
	        			}
					}
				}
	        	
	            returnVal = returnVal.replace(m.group(), sb.toString());        	            
	        }
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage());
		}
        return returnVal;
	}
	
	@Override
	public void insertSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertSlotTypeClass(map);
	}
	
	@Override
	public void deleteSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotTypeClass(map);
	}
	
	@Override
	public void deleteSlotTypeClassAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotTypeClassAll(map);
	}
	
	@Override
	public void deleteProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		map.put("PROJECT_SEQ", map.get("SEQ_NO"));
		List<Map<String, Object>> clist = viewDAO.selectSlotClass2(map);
		for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			DropTable(citem);
		}
		
		viewDAO.deleteProject(map);		
		//viewDAO.updatePorts2(map);
				
		if (!map.containsKey("VERSION")) {
			map.put("USER_SEQ_NO", map.get("PROJECT_USER_SEQ"));
			List<Map<String, Object>> prjchk = viewDAO.SelectProjectName(map);
			if (prjchk.size() == 0) {
				String Filename = "";						
				String tempPath = "";
				String DeletePath = "";
				String apiKey = "";
				if (map.containsKey("API_KEY"))
					apiKey = map.get("API_KEY").toString();
				else
					apiKey = GetCookie("SELECT_API", request);
				
				if (apiKey.isEmpty()) {
					apiKey = "null";
				}
				
				if (apiKey.equals("null"))
					Filename = String.format("IS-%s-@-%s", map.get("PROJECT_USER_SEQ"), map.get("PROJECT_NAME").toString());
				else
					Filename = map.get("PROJECT_NAME").toString();
				
				final String fiFileName = Filename;
				
				if (bServer) {										
					if (apiKey.equals("null"))
						tempPath = String.format("%s/KB/%s/dialog_domain/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG");
					else
						tempPath = String.format("%s/KB/%s/dialog_domain/%s/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3), apiKey);
					DeletePath = String.format("%s/KB/%s/dialog_domain/DeletedDomains", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG");
				}				
				else {
					if (apiKey.equals("null"))
						tempPath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\";
					else
						tempPath = String.format("E:\\dev\\file\\webtool\\www_data\\KB\\%s\\dialog_domain\\%s\\%s\\", (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3), apiKey);
					DeletePath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\DeletedDomains";
				}
				
				String Path = tempPath + Filename;				
				
				File tempf = new File(DeletePath);
				if (!tempf.exists()) {
					tempf.mkdir();
				}
				
				String fileList[] = tempf.list(new FilenameFilter() {
					 
				    @Override
				    public boolean accept(File dir, String name) {
				        return name.startsWith(fiFileName);        // TEMP로 시작하는 파일들만 return
				    }
				 
				});
				DeletePath += String.format("/%s-%s", Filename, fileList.length); 
				log.debug(Path);
				log.debug(DeletePath);
				
				File s = new File(Path);
				File t = new File(DeletePath);
				if(!t.exists()) {
					t.mkdir();
				}
				
				Directorycopy(s, t);
				deleteAllFiles(Path);	
			}
		}		
	}
	
	@Override
	public void deleteTutorProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub		
		viewDAO.deleteTutorProject(map);			
		
		String Path = GetPath("T", false, request) + GetDomainName("T", false, map, request);
		//String Path = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString());
		log.debug(Path);
		deleteAllFiles(Path);
	}
	
	@Override
	public void deleteSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotClass(map);
		deleteSlotClassSeq(map);
		deleteSlotTypeClassAll(map);
		viewDAO.deleteSlotSubClass(map);
	    DropTable(map);		
	}
	
	@Override
	public void deleteSlotClassSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotClassSeq(map);
	}
	
	@Override
	public void deleteSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		viewDAO.deleteSlot(map);
		viewDAO.deleteSlotDefine(map);
		deleteSlotTypeClass(map);
		
		CommandMap cdb = new CommandMap();			
		cdb.put("SEQ_NO", map.get("CLASS_SEQ_NO"));
		cdb.put("SLOT_SEQ_NO", map.get("SEQ_NO"));							
		cdb.put("TYPE_SEQ", map.get("TYPE_SEQ"));		
		cdb.put("CLASS_NAME", map.get("CLASS_NAME"));
		cdb.put("SLOT_NAME", map.get("OLD_SLOT_NAME"));
		cdb.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = null;	
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (map.get("SLOT_TYPE").toString().equals("C")) {
			//cdb.put("OLD_TYPE_SEQ", commandMap.get("TYPE_SEQ"));
			DropMultiColumn(cdb.getMap(), jsonObj);
		}
		else {
			cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SEQ_NO"));
			DropColumn(cdb.getMap());
		}
		
		List<Map<String, Object>> list = viewDAO.selectSlotSub2(cdb.getMap());
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			cdb.put("SEQ_NO", item.get("SLOT_COLUMN_NAME").toString().substring(0, item.get("SLOT_COLUMN_NAME").toString().indexOf("_")));
			cdb.put("COL_NAME", item.get("SLOT_COLUMN_NAME"));
			cdb.put("SLOT_SUB_SEQ", item.get("SLOT_SUB_SEQ"));
			viewDAO.deleteSlotSub(cdb.getMap());
			DropColumn(cdb.getMap());
		}
	}
	
	@Override
	public void deleteReferSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteReferSlotClass(map);
	}
	
	@Override
	public void updateProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		viewDAO.updateProject(map);					
				
		if (!map.get("OLD_NAME").toString().equals(map.get("PROJECT_NAME").toString())) {		
			String orgFileName = "";
						
			if (GetCookie("SELECT_API", request).equals("null"))
				orgFileName = String.format("IS-%s-@-%s", GetCookie("DU", request), map.get("OLD_NAME").toString());
			else
				orgFileName = map.get("OLD_NAME").toString();
			
			String orgPath = GetPath("D", false, request) + orgFileName;
			String newPath = GetPath("D", false, request) + GetDomainName("D", false, map, request);
			log.debug(orgPath);
			log.debug(newPath);
			//String orgPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("OLD_NAME").toString();
			//String newPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
						
			File orgfile = new File(orgPath);
			File newfile = new File(newPath);
			
			if(!newfile.exists()) {
				newfile.mkdir();
			}
			
			Directorycopy(orgfile, newfile);
			deleteAllFiles(orgPath);
			//언어 방향 바뀌면 이동이 아니고 복사로 바꿀것(전화로 물어보고)
									
			//orgfile = new File(newPath + "\\" + map.get("OLD_NAME").toString() + "_DB.sqlite3");
			//newfile = new File(newPath + "\\" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
			orgfile = new File(newPath + "/" + orgFileName + "_DB.sqlite3");
			newfile = new File(newPath + "/" + GetDomainName("D", false, map, request) + "_DB.sqlite3");
			orgfile.renameTo(newfile);
			
			File file = new File(newPath);
			if(file.exists()) {
				File[] tempFile = file.listFiles();
				if(tempFile.length > 0){
					for (int i = 0; i < tempFile.length; i++) {
						if(tempFile[i].getName().contains(String.format("%s.", orgFileName)) || tempFile[i].getName().contains(String.format("%s_", orgFileName))){
							tempFile[i].delete();
						}
					}
				}
			}
		}
	}
	
	@Override
	public void updateProjectDeleteFlag(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateProjectDeleteFlag(map);		
	}
	
	@Override
	public void updateTutorProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		viewDAO.updateTutorProject(map);
		
		if (!map.get("OLD_NAME").toString().equals(map.get("PROJECT_NAME").toString())) {		
			String orgPath = GetPath("T", false, request) + String.format("ED-%s-@-%s", GetCookie("TU", request), map.get("OLD_NAME").toString());
			String newPath = GetPath("T", false, request) + GetDomainName("T", false, map, request);
							
			log.debug(orgPath);
			log.debug(newPath);			
			
			//String orgPath = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s", map.get("LANG").toString(), map.get("OLD_NAME").toString());
			//String newPath = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString());
						
			File orgfile = new File(orgPath);
			File newfile = new File(newPath);
			
			orgfile.renameTo(newfile);							
		}
	}
	
	@Override
	public void updateTutorProjectDeleteFlag(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateTutorProjectDeleteFlag(map);		
	}
	
	@Override
	public void updateSlotClass(Map<String, Object> map) throws JsonGenerationException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		viewDAO.updateSlotClass(map);
		
		CommandMap cdb = new CommandMap();
		cdb.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		cdb.put("CLASS_SEQ_NO", map.get("SEQ_NO"));
		log.debug(map);
		JSONObject slotSubobj = null;
		if (!map.get("CLASS_NAME").toString().equals(map.get("OLD_CLASS_NAME"))) {
			List<Map<String, Object>> listsubslot = viewDAO.selectSlotSub(map);
			JSONParser jsonParser = new JSONParser();
	        
			try {
				slotSubobj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(listsubslot));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			updateSlotSub(map, cdb, slotSubobj, "C");
		}
	}
	
	@Override
	public void updateSlotClassNewCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotClassNewCheck(map);
	}
	
	@Override
	public void updateSlot(Map<String, Object> map, HttpServletRequest request) throws JsonGenerationException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		if (map.containsKey("SUB_SLOT_SEQ"))
			map.put("WHERE_SQL", String.format(" AND SUB_SLOT_SEQ = '%s'", map.get("SUB_SLOT_SEQ")));
		else
			map.put("WHERE_SQL", " AND (SUB_SLOT_SEQ = '' OR SUB_SLOT_SEQ is null)");
		
		viewDAO.updateSlot(map);
		SlotDefine(map);			
		
		if (!map.containsKey("SUB_SLOT_SEQ")) {
			CommandMap cdb = new CommandMap();
			cdb.put("SEQ_NO", map.get("CLASS_SEQ_NO"));
			cdb.put("CLASS_SEQ_NO", map.get("CLASS_SEQ_NO"));
			cdb.put("TYPE_SEQ", map.get("TYPE_SEQ"));
			cdb.put("CLASS_NAME", map.get("CLASS_NAME"));
			cdb.put("SLOT_NAME", map.get("SLOT_NAME"));
			cdb.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
			
			JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObj = null;
	        JSONArray objArray;
			List<Map<String, Object>> list = null;
			try {
				list = selectSlotTypeClass(map);
				jsonObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			cdb.put("SLOT_SEQ_NO", map.get("SEQ_NO"));
			cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SEQ_NO"));
			
			if (map.get("SLOT_TYPE").toString().equals("C")) {
				if (!map.get("OLD_SLOT_TYPE").toString().equals("C")) {
					insertSlotTypeClass(map);
					//1개 컬럼 삭제 후 여러건 추가					
					DropColumn(cdb.getMap());
					CreateMultiColumn(cdb.getMap(), jsonObj);
				}
				else if (map.get("OLD_SLOT_TYPE").toString().equals("C") && 
						!map.get("TYPE_SEQ").toString().equals(map.get("OLD_TYPE_SEQ").toString())) {
					//삭제 후 인서트
					deleteSlotTypeClass(map);
					insertSlotTypeClass(map);
					
					//여러 건 삭제 후 여러건 추가
					cdb.put("TYPE_SEQ", map.get("OLD_TYPE_SEQ"));						
					DropMultiColumn(cdb.getMap(), jsonObj);
					cdb.put("TYPE_SEQ", map.get("TYPE_SEQ"));
					CreateMultiColumn(cdb.getMap(), jsonObj);
				}
			}
			else { // SLOT_TYPE이 C가 아닐 때
				if (map.get("OLD_SLOT_TYPE").toString().equals("C")) {
					deleteSlotTypeClass(map);
					cdb.put("TYPE_SEQ", map.get("OLD_TYPE_SEQ"));
					DropMultiColumn(cdb.getMap(), jsonObj);
					CreateColumn(cdb.getMap());
				}
			}	
			
			JSONObject slotSubobj = null;
			if (!map.get("SLOT_NAME").toString().equals(map.get("OLD_SLOT_NAME"))) {
				List<Map<String, Object>> listsubslot = viewDAO.selectSlotSub(map);
				JSONParser jsonParser2 = new JSONParser();
		        
				try {
					slotSubobj = (JSONObject)jsonParser2.parse(CommonUtils.SetSlotListToJson(listsubslot));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				updateSlotSub(map, cdb, slotSubobj, "S");
			}
			
			int tempsum = 0;
			String[] tempseq; 
			String tempslotname = "";
			try {
				cdb.remove("SLOT_NAME");
				List<Map<String, Object>> slist = selectSlot(cdb.getMap());
				jsonObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slist));				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//부모 슬롯들 작업
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();				
								
				cdb.put("SEQ_NO", item.get("CLASS_SEQ_NO"));
				cdb.put("COL_TYPE", "varchar(200)");
				cdb.put("CLASS_NAME", item.get("CLASS_NAME"));
				cdb.put("SLOT_SEQ_NO", item.get("SLOT_SEQ_NO"));
				cdb.put("SLOT_NAME", item.get("SLOT_NAME"));
				cdb.put("COL_NAME", item.get("CLASS_SEQ_NO") + "_" + item.get("SLOT_SEQ_NO"));				
				cdb.put("TYPE_SEQ", item.get("TYPE_SEQ"));				
				
				tempsum = Integer.parseInt(item.get("CLASS_SEQ_NO").toString());
				tempseq = item.get("SUB_SLOT_SEQ").toString().split("_");
				for (int i = 0; i < tempseq.length; i++) {
					tempsum += Integer.parseInt(tempseq[i]);
				}
				
				if (map.get("SLOT_TYPE").toString().equals("C")) {
					if (map.get("OLD_SLOT_TYPE").toString().equals("C")) {
						if(!map.get("TYPE_SEQ").toString().equals(map.get("OLD_TYPE_SEQ").toString())) {
							cdb.put("SEQ_NO", item.get("SLOT_SEQ_NO")); //Type Class 때문에 임시로 교체							
							deleteSlotTypeClass(cdb.getMap());
							insertSlotTypeClass(cdb.getMap());
							cdb.put("SEQ_NO", item.get("CLASS_SEQ_NO"));
							//여러 건 삭제 후 여러건 추가
							cdb.put("TYPE_SEQ", map.get("OLD_SLOT_TYPE"));
							DropMultiColumn(cdb.getMap(), jsonObj);
							//삭제 후 클래스번호  변경
							cdb.put("TYPE_SEQ", map.get("TYPE_SEQ"));
							CreateMultiColumn(cdb.getMap(), jsonObj);
						}
						else {
							if (!map.get("SLOT_NAME").toString().equals(map.get("OLD_SLOT_NAME"))) {
								//기존과 지금 모두 타입이 C이고 타입 번호도 같고, 슬롯 이름만 변경 되었을 때
								updateSlotSub(map, cdb, slotSubobj, "S");
							}
						}							
					}
					else {
						//삭제 후  추가					
						cdb.put("SLOT_SUB_SEQ", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SUB_SLOT_SEQ"), map.get("CLASS_SEQ_NO"), map.get("SEQ_NO")));
						cdb.put("COL_NAME", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SLOT_SEQ_NO"), tempsum, map.get("SEQ_NO")));
						viewDAO.deleteSlotSub(cdb.getMap());
						//삭제 후 클래스번호  변경			
						CreateMultiColumn(cdb.getMap(), jsonObj);
					}
				}
				else {
					if (map.get("OLD_SLOT_TYPE").toString().equals("C")) {
						cdb.put("SEQ_NO", item.get("SLOT_SEQ_NO")); //Type Class 때문에 임시로 교체							
						deleteSlotTypeClass(cdb.getMap());
						cdb.put("SEQ_NO", item.get("CLASS_SEQ_NO"));
						
						DropMultiColumn(cdb.getMap(), jsonObj);
						//삭제후 1개 컬럼 추가
						cdb.put("SLOT_SUB_SEQ", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SUB_SLOT_SEQ"), map.get("CLASS_SEQ_NO"), map.get("SEQ_NO")));
						cdb.put("SLOT_SUB_NAME", String.format("%s.%s.%s", item.get("CLASS_NAME"), item.get("SLOT_NAME"), map.get("SLOT_NAME")));
						cdb.put("SLOT_SUB_NAME_SEQ", String.format("%s.%s.%s", item.get("CLASS_SEQ_NO"), item.get("SUB_SLOT_SEQ").toString().replaceAll("_",  "."), map.get("SEQ_NO")));
						cdb.put("SLOT_COLUMN_NAME", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SLOT_SEQ_NO"), tempsum, map.get("SEQ_NO")));
						cdb.put("SLOT_SEQ_NO", map.get("SEQ_NO"));			
						cdb.put("COL_NAME", String.format("%s_%s_%s_%s", item.get("CLASS_SEQ_NO"), item.get("SLOT_SEQ_NO"), tempsum, map.get("SEQ_NO")));
						cdb.put("COL_TYPE", "varchar(200)"); 
						viewDAO.insertSlotSub(cdb.getMap());
						CreateColumn(cdb.getMap());
					}
					else {
						if (!map.get("SLOT_NAME").toString().equals(map.get("OLD_SLOT_NAME"))) {
							//기존과 지금 모두 타입이 C가 아닌 슬롯을 때, slot_sub 업데이트
							updateSlotSub(map, cdb, slotSubobj, "S");
						}
					}
				}
			}
			
			/*if (!map.get("OLD_TYPE_NAME").toString().equals("Sys.number") && map.get("TYPE_NAME").toString().equals("Sys.number")) {
				try {
					ExecSql(map, "I", request);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (map.get("OLD_TYPE_NAME").toString().equals("Sys.number") && !map.get("TYPE_NAME").toString().equals("Sys.number")) {
				try {
					ExecSql(map, "D", request);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
		}
	}
	
	private void updateSlotSub(Map<String, Object> map, CommandMap cdb, JSONObject slotSubobj, String Type) {
		Boolean bSaveChk = false;
			
		if (slotSubobj.containsKey(cdb.get("CLASS_SEQ_NO").toString())) {
			JSONArray slotArray = (JSONArray)slotSubobj.get(cdb.get("CLASS_SEQ_NO").toString());
			String[] tempSubName;
			String[] tempSubSlotName;
			
			if (slotArray != null) {
				for (int i = 0; i < slotArray.size(); i++) {
					JSONObject slotobj = (JSONObject)slotArray.get(i);
					tempSubName = slotobj.get("SLOT_SUB_NAME").toString().split("\\.");
					tempSubSlotName = slotobj.get("SLOT_SUB_NAME_SEQ").toString().split("\\.");
				
					if (Type.equals("S")) {
						for (int j = 1; j < tempSubSlotName.length; j++) {
							if (tempSubSlotName[j].contains(map.get("SEQ_NO").toString())) {
								tempSubName[j] = map.get("SLOT_NAME").toString();
								bSaveChk = true;
							}
						}	
					}
					else {
						if (tempSubSlotName[0].contains(map.get("SEQ_NO").toString())) {
							tempSubName[0] = map.get("CLASS_NAME").toString();
							bSaveChk = true;
						}
					}
					
					if (bSaveChk) {
						String strTemp = "";
						for (int j = 0; j < tempSubName.length; j++) {								
							if (j == 0)
								strTemp = tempSubName[j];
							else
								strTemp += "." + tempSubName[j];
						}
						cdb.put("SLOT_SUB_NAME", strTemp);
						cdb.put("SLOT_SUB_SEQ", slotobj.get("SLOT_SUB_SEQ"));
						viewDAO.updateSlotSub(cdb.getMap());
					}
				}
			}
		}
	}
	
	@Override
	public void updateSlotSort(Map<String, Object> map) {
		// TODO Auto-generated method stub
		CommandMap SlotObjMap = new CommandMap();
		SlotObjMap.put("SEQ_NO", map.get("S_SEQ_NO"));
		SlotObjMap.put("SORT_NO", map.get("EINDEX"));
		viewDAO.updateSlotSort(SlotObjMap.getMap());
		SlotObjMap = new CommandMap();
		SlotObjMap.put("SEQ_NO", map.get("E_SEQ_NO"));
		SlotObjMap.put("SORT_NO", map.get("SINDEX"));			
		viewDAO.updateSlotSort(SlotObjMap.getMap());
	}
	
	@Override
	public void insertSlotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = null;      
        CommandMap SlotObjMap;
        CommandMap SlotObjDetailMap;                 	
		Map<String, Object> objlist = viewDAO.selectSlotObjectInsertChk(map);		
		Map<String, Object> objdtllist = viewDAO.selectSlotObjectDetailInsertChk(map);
		Map<String, Object> tempmap = null;
		
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		JSONArray objArray = (JSONArray)jsonObj.get("OBJECT_ITEM");				
		map.put("STATUS", "S");		
		//throw new Exception();
		
		for(int i=0 ; i< objArray.size() ; i++){
            JSONObject SlotObj = (JSONObject)objArray.get(i);
            SlotObjMap = new CommandMap();
            if (SlotObj.get("SEQ_NO").toString().equals("0")) {
	            SlotObjMap.put("OBJECT_NAME", SlotObj.get("OBJECT_NAME"));
	            SlotObjMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));
	            
	            if (objdtllist.containsKey(SlotObj.get("OBJECT_NAME"))) {     	
	            	map.put("STATUS", String.format("%s : 대표어와 동의어 중복 사용", SlotObj.get("OBJECT_NAME")));	            	
	            	throw new InterruptedException();            	
	            }                        
	            if (objlist.containsKey(SlotObj.get("OBJECT_NAME"))) {
	            	tempmap = (Map<String, Object>)objlist.get(SlotObj.get("OBJECT_NAME"));            	
	            	SlotObjMap.put("SEQ_NO", tempmap.get("SEQ_NO"));
	            }
	            else {                        
	            	viewDAO.insertSlotObject(SlotObjMap.getMap());	            	
	            }
            }
            else
            	SlotObjMap.put("SEQ_NO", SlotObj.get("SEQ_NO"));
	            
            JSONArray objDetailArray = (JSONArray)SlotObj.get("OBJECT_DETAIL");
            for(int j=0 ; j< objDetailArray.size() ; j++) {
            	JSONObject SlotObjDetail = (JSONObject)objDetailArray.get(j);
            	if (objlist.containsKey(SlotObjDetail.get("OBJECT_NAME"))) {
            		map.put("STATUS", String.format("%s : 대표어와 동의어 중복 사용", SlotObjDetail.get("OBJECT_NAME")));
                	throw new InterruptedException();                	
                }
            	
            	if (objdtllist.containsKey(SlotObjDetail.get("OBJECT_NAME"))) {
            		tempmap = (Map<String, Object>)objdtllist.get(SlotObjDetail.get("OBJECT_NAME"));
            		map.put("STATUS", String.format("%s : \"%s\", \"%s\"의 동의어로 중복 사용", SlotObjDetail.get("OBJECT_NAME"), tempmap.get("PARENT_OBJECT_NAME"), SlotObj.get("OBJECT_NAME")));
                	throw new InterruptedException();                	
                }
            	
            	SlotObjDetailMap = new CommandMap();
            	SlotObjDetailMap.put("OBJECT_NAME", SlotObjDetail.get("OBJECT_NAME"));
            	SlotObjDetailMap.put("PARENT_SEQ_NO", SlotObjMap.get("SEQ_NO"));
            	SlotObjDetailMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));
            	
            	viewDAO.insertSlotObjectDetail(SlotObjDetailMap.getMap());
            }
        }
		
		/* 페이징으로 바뀌면서 삭제
		 
		//String Path = String.format("E:\\dev\\file\\webtool\\www_data\\dialog_domain\\%s\\SlotLists", request.getSession().getAttribute("ProjectName"));
		//String FileName = String.format("\\%s.%s.list.txt", map.get("CLASS_NAME"), map.get("SLOT_NAME"));			
		
		String Path = String.format("%s%s/SlotLists", GetPath("D", request), GetDomainName("D", map, request));
		
		File filepath = new File(Path);
		if (!filepath.exists()) {
			filepath.mkdir();
		}
		
		//String Path = String.format("%s/dialog_domain/%s/SlotLists", request.getSession().getServletContext().getInitParameter("enginePath"), request.getSession().getAttribute("ProjectName"));
		String FileName = String.format("/%s.%s.list.txt", map.get("CLASS_NAME"), map.get("SLOT_NAME"));
		
		Map<String, Object> encodingdata = null;
		try {
			encodingdata = selectEncoding(map);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (encodingdata.get("ENCODING_NAME").toString().equals("UTF-8"))
			map.put("FILE_ENC", "UTF8");	
		else
			map.put("FILE_ENC", "CP949");
		map.put("ENCODING", encodingdata.get("ENCODING_NAME").toString());			
		
		try {			
			Runtime rt = Runtime.getRuntime();
			//Process proc = rt.exec(String.format("%s/dial –cmd DEL_PERSONAL_DICT -dic DIC_PATH -domain %s -meaning %s.%s", request.getSession().getServletContext().getInitParameter("enginePath"), request.getSession().getServletContext().getInitParameter("ProjectName"), map.get("CLASS_NAME"), map.get("SLOT_NAME")));
			
			String cmd = String.format("%s/dial -cmd USERDIC_AUTO -act DEL -slotname %s.%s -dic %s/KB/%s -domain %s -FILE_ENC %s", request.getSession().getServletContext().getInitParameter("enginePath"), map.get("CLASS_NAME"), map.get("SLOT_NAME"), request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("DL", request), GetDomainName("D", map, request), map.get("FILE_ENC").toString());
			Process proc;
			
			filepath = new File(Path + FileName);
			if (filepath.exists()) {
				log.debug(cmd);
				proc = rt.exec(cmd);
				proc.waitFor();
			}
											
			try {
				CreateTxtFile(map, Path + FileName, sb);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			cmd = String.format("%s/dial -cmd USERDIC_AUTO -act ADD -slotname %s.%s -dic %s/KB/%s -domain %s -FILE_ENC %s", request.getSession().getServletContext().getInitParameter("enginePath"), map.get("CLASS_NAME"), map.get("SLOT_NAME"), request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("DL", request), GetDomainName("D", map, request), map.get("FILE_ENC").toString());
			log.debug(cmd);
			proc = rt.exec(cmd);
            proc.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace();
		}	  
		*/   
	}
	
	@Override
	public void insertSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertSlotObjectDetail(map);
	}
	
	@Override
	public void deleteSlotObject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotObject(map);		
	}

	@Override
	public void deleteSlotObjectDetailSeq(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub		
		viewDAO.deleteSlotObjectDetailSeq(map);		
	}
	
	@Override
	public void deleteSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotObjectDetail(map);
	}
	
	@Override
	public void updateSlotObject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		// viewDAO.updateSlotObject(map);
		map.put("SAVE_CHK", "FAILED");		
		if (map.get("TYPE").toString().equals("O")) {
			List<Map<String, Object>> list = viewDAO.selectslotobjectUpdatechk(map);
			if (list.size() == 0) {
				viewDAO.updateSlotObject(map);
				map.put("SAVE_CHK", "OK");
			}
		}
		else {
			List<Map<String, Object>> list = viewDAO.selectslotobjectDetailUpdatechk(map);
			if (list.size() == 0) {
				viewDAO.updateSlotObjectDetail(map);
				map.put("SAVE_CHK", "OK");
			}
		}		
	}
	
	@Override	
	public void insertSlotObjectExcel(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		String strFilePath = "";
		try {
			strFilePath = fileUtils.SaveFile("", request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}
			
		List<String> lstHeader = new ArrayList<String>();
		List<String> lstObjval = new ArrayList<String>();					
		
		for (int i = 1; i < 256; i++) {
			lstHeader.add(IntToLetter(i));
		}
		
		ReadOption readOption = new ReadOption();
		readOption.setFilePath(strFilePath);
		readOption.setOutputColumns(lstHeader);
		readOption.setStartRow(1);
		
		ExcelUtils exu = new ExcelUtils();
		List<Map<String, String>> excelContent = exu.read(readOption);
		
		if (excelContent.size() > 30) {
			map.put("STATUS", "MC");
			throw new Exception();			
		}
		
		if (map.get("DEL_FLAG").toString().equals("Y")) {
			viewDAO.deleteSlotObjectAll(map);
		}
		
		CommandMap SlotObjMap = new CommandMap();
		CommandMap SlotObjDetailMap;
		Map<String, Object> objlist = viewDAO.selectSlotObjectInsertChk(map);		
		Map<String, Object> objdtllist = viewDAO.selectSlotObjectDetailInsertChk(map);
		Map<String, Object> tempmap = null;
		
		map.put("STATUS", "S");
		
		Map<String, Object> cntitem = selectCountMng(map);			
        map.put("TABLE", "v_slot_object");
        map.put("WHERE", "SLOT_SEQ_NO in (select SEQ_NO from slot where PROJECT_SEQ = " + map.get("PROJECT_SEQ").toString() + ")");
		Map<String, Object> cntchk = selectCountCheck(map);

        int iTotCnt = Integer.parseInt(cntchk.get("CNT").toString());
        int iMaxCnt = Integer.parseInt(cntitem.get("SLOTOBJ_COUNT").toString());
				
		for(Map<String, String> article : excelContent) {
			log.debug(article);
			if (article.size() > 21) {
				map.put("STATUS", "SC");
				throw new Exception();			
			}
			for (int i = 0; i < article.size(); i++) {
				if (!article.get(lstHeader.get(i)).isEmpty()) {
					iTotCnt++;
					if (iTotCnt > iMaxCnt) {
		        		map.put("STATUS", "TC");
						throw new Exception();
					}		        	
		        	
					if (i == 0) {												
						SlotObjMap = new CommandMap();
						SlotObjMap.put("OBJECT_NAME", article.get(lstHeader.get(i)).toString());
		                SlotObjMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));	
		                
						if (objdtllist.containsKey(article.get(lstHeader.get(i)).toString())) {            	
							map.put("STATUS", "F");
			            	throw new Exception();            	
			            }                        
			            if (objlist.containsKey(article.get(lstHeader.get(i)).toString())) {			            	
			            	tempmap = (Map<String, Object>)objlist.get(article.get(lstHeader.get(i)).toString());
			            	SlotObjMap.put("SEQ_NO", tempmap.get("SEQ_NO"));
			            }
			            else {                        
			            	viewDAO.insertSlotObject(SlotObjMap.getMap());
			            }			            
					}
					else {
						if (objlist.containsKey(article.get(lstHeader.get(i)).toString()) || objdtllist.containsKey(article.get(lstHeader.get(i)).toString())) {            		
		            		map.put("STATUS", "F");
		                	throw new Exception();                	
		                }
						
						SlotObjDetailMap = new CommandMap();
	                	SlotObjDetailMap.put("OBJECT_NAME", article.get(lstHeader.get(i)).toString());
	                	SlotObjDetailMap.put("PARENT_SEQ_NO", SlotObjMap.get("SEQ_NO"));
	                	SlotObjDetailMap.put("SLOT_SEQ_NO", SlotObjMap.get("SLOT_SEQ_NO"));
                		viewDAO.insertSlotObjectDetail(SlotObjDetailMap.getMap());
					}
				}
				else {
					break;
				}
			}
		}
	}
	
	@Override
	public void insertSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		//viewDAO.insertSlotInstans(map);
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotInstansMap;
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotInstansMap = new CommandMap();
                SlotInstansMap.put("INSERT", map.get("INSERT_ITEM"));
                SlotInstansMap.put("VALUES", SlotObj.get("ITEM_DETAIL").toString().replaceAll("et§", "'"));
                                
            	viewDAO.insertSlotInstans(SlotInstansMap.getMap());                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}            
	}
	
	@Override
	public void updateSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotInstans(map);
	}

	@Override
	public void deleteSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotInstans(map);
	}
	
	@Override
	public void deleteSlotInstansAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotInstansAll(map);
	}
	
	@Override	
	public void insertSlotInstansExcel(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String strFilePath = "";
		
		try {
			strFilePath = fileUtils.SaveFile("", request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}				
		
		String[] arrHeader = map.get("HEADER_LIST").toString().split(",");
		String[] arrCeader = map.get("COL_LIST").toString().split(",");
		List<String> lstHeader = new ArrayList<String>();
		List<String> insertHeader = new ArrayList<String>();
		List<String> insertValue = new ArrayList<String>();
		
		for (int i = 1; i <= arrHeader.length; i++) {
			lstHeader.add(IntToLetter(i));			
		}
					
		ReadOption readOption = new ReadOption();		
		readOption.setFilePath(strFilePath);
		readOption.setOutputColumns(lstHeader);
		readOption.setStartRow(1);
		
		ExcelUtils exu = new ExcelUtils();
		List<Map<String, String>> excelContent = exu.read(readOption);
		
		if (excelContent.size() > 200) {
			map.put("UPLOAD_STATUS", "UC");
			throw new Exception();			
		}
				    
		StringBuilder strinsert = new StringBuilder();
		
		/*strinsert.append("INSERT INTO slot_class_" + map.get("SEQ_NO") + "(");
		for (int i = 0; i < arrCeader.length; i++) {		
			if ((1+i) != arrCeader.length) {
				strinsert.append(arrCeader[i] + ", ");
			}
			else
				strinsert.append(arrCeader[i]);
		}
		strinsert.append(")");*/
		
		StringBuilder strvalues = new StringBuilder();
		int iCnt = 0;
				
		if (map.get("DEL_FLAG").toString().equals("Y")) {
			viewDAO.deleteSlotInstansAll(map);
		}						
		
		Map<String, Object> cntitem = selectCountMng(map);			
        map.put("TABLE", "slot_class_" + map.get("SEQ_NO").toString());
        map.put("WHERE", "1=1");
		Map<String, Object> cntchk = selectCountCheck(map);

        int iTotCnt = Integer.parseInt(cntchk.get("CNT").toString());
        int iMaxCnt = Integer.parseInt(cntitem.get("INSTANCE_COUNT").toString());
        
		for(Map<String, String> article : excelContent){
			//if (iCnt == 0) {과 안에 내용 새로 추가 기존은 else안에 있는거)
			
			if (iCnt == 0) {				
				for (int k = 0; k < lstHeader.size(); k++) {
					for (int j = 0; j < arrHeader.length; j++) {						
						if (article.containsKey(lstHeader.get(k)) && arrHeader[j].equals(article.get(lstHeader.get(k)).toString())) {
							insertHeader.add(arrCeader[j]);							
							break;
						}
					}													
				}
				
				strinsert.append("INSERT INTO slot_class_" + map.get("SEQ_NO") + "(");
				for (int i = 0; i < insertHeader.size(); i++) {		
					if ((1+i) != insertHeader.size()) {
						strinsert.append(insertHeader.get(i) + ", ");
					}
					else
						strinsert.append(insertHeader.get(i));
				}
				strinsert.append(")");
			}
			else {
				iTotCnt++;				
				if (iTotCnt > iMaxCnt) {
					map.put("UPLOAD_STATUS", "TC");
					throw new Exception();
				}	        	
	        	
				insertValue.clear();
				strvalues.setLength(0);
				
				for (int i = 0; i < lstHeader.size(); i++) {					
					if (article.containsKey(lstHeader.get(i)))
						insertValue.add(article.get(lstHeader.get(i)));										
				}
				if (insertValue.size() > 0) {
					strvalues.append("VALUES(");
					for (int i = 0; i < insertValue.size(); i++) {
						if (i+1 == insertValue.size())
							strvalues.append(String.format("\"%s\"", insertValue.get(i)));
						else
							strvalues.append(String.format("\"%s\" , ", insertValue.get(i)));
					}
					strvalues.append(")");
				}				
				
				map.put("INSERT", strinsert.toString());
				map.put("VALUES",  strvalues.toString());
				
				viewDAO.insertSlotInstans(map);
			}
			iCnt++;;
		}
		
		File f = new File(strFilePath);
		if (f.exists()) {
			f.delete();
		}
	}	
	
	public static String IntToLetter(int Int) {
		if (Int<27){
			return Character.toString((char)(Int+96)).toUpperCase();
		} else {
			if (Int%26==0) {
				return IntToLetter((Int/26)-1)+IntToLetter(((Int-1)%26+1));
			} else {
				return IntToLetter(Int/26)+ IntToLetter(Int%26);
			}
		}
	}
	
	@Override
	public void insertSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub
		//viewDAO.insertSlotInstans(map);
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotInstansMap;
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotInstansMap = new CommandMap();
                SlotInstansMap.put("INSERT", map.get("INSERT_ITEM"));
                SlotInstansMap.put("VALUES", SlotObj.get("ITEM_DETAIL").toString().replaceAll("et§", "'"));
                                
            	viewDAO.insertSlotMapping(SlotInstansMap.getMap());                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}            
	}
	
	@Override
	public void updateSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotMapping(map);
	}

	@Override
	public void deleteSlotMapping(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotMapping(map);
	}
	
	@Override
	public void insertIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonItObj;
        JSONObject UserSayObj;
        CommandMap UserSayMap;
        CommandMap IntentItMap;
        CommandMap IntentObjMap;
        CommandMap IntentObjDetailMap;
        
        int iIntentNo;        
		try {
			if (map.get("SEQ_NO").toString().isEmpty()) {
				viewDAO.insertIntent(map);
				iIntentNo = (int)map.get("SEQ_NO");
			}
			else {
				iIntentNo = Integer.parseInt(map.get("SEQ_NO").toString());				
				CommandMap ObjDelMap = new CommandMap();				
				viewDAO.updateIntent(map);
				ObjDelMap.put("SEQ_NO", iIntentNo);
				if (map.get("SLOT_SEQ_NO").toString().isEmpty()) {
					viewDAO.deleteIntentsIntention(ObjDelMap.getMap());
					viewDAO.deleteIntentsObject(ObjDelMap.getMap());
					viewDAO.deleteIntentsObjectDetail(ObjDelMap.getMap());
				}
				else {
					ObjDelMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));					
					viewDAO.deleteIntentsObjectDetail2(ObjDelMap.getMap());
					viewDAO.deleteIntentsObject2(ObjDelMap.getMap());
					viewDAO.deleteIntentsIntention2(ObjDelMap.getMap());									
				}
			}
													
			UserSayObj = (JSONObject)jsonParser.parse((String)map.get("USER_SAYS"));
			JSONArray UserSayArray = (JSONArray)UserSayObj.get("USER_SAYS");
			
			for(int i=0 ; i< UserSayArray.size() ; i++){
				
				JSONObject UsObj = (JSONObject)UserSayArray.get(i);
				UserSayMap = new CommandMap();
				UserSayMap.put("SAY", UsObj.get("SAY"));
				UserSayMap.put("SAY_TAG", UsObj.get("SAY_TAG"));
				UserSayMap.put("SLOT_TAG", UsObj.get("SLOT_TAG"));
				UserSayMap.put("SLOT_SEQ_NO", UsObj.get("SLOT_SEQ_NO"));
				UserSayMap.put("SLOT_NAME", UsObj.get("SLOT_NAME"));
				UserSayMap.put("SLOT_MAP", UsObj.get("SLOT_MAP"));
				UserSayMap.put("SLOT_MAPDA", UsObj.get("SLOT_MAPDA"));
				UserSayMap.put("SEQ_NO", UsObj.get("SEQ_NO"));
				UserSayMap.put("INTENTION", UsObj.get("INTENTION"));
				UserSayMap.put("INTENT_SEQ", iIntentNo);
				
				if (UsObj.get("SEQ_NO").toString().equals("0")) {
					viewDAO.insertIntentUserSay(UserSayMap.getMap());					
				}
				else {
					viewDAO.updateIntentUserSay(UserSayMap.getMap());
				}				
			}
			
			jsonItObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray ItObjArray = (JSONArray)jsonItObj.get("OBJECT_ITEM");
						
			for(int i=0 ; i< ItObjArray.size() ; i++){
                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
                IntentItMap = new CommandMap();
                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
                IntentItMap.put("ACT", IntentItObj.get("ACT"));               
                IntentItMap.put("SLOT_SEQ_NO", IntentItObj.get("SLOT_SEQ_NO"));
                IntentItMap.put("INTENT_SEQ", iIntentNo);
                
                viewDAO.insertIntentIntention(IntentItMap.getMap());                     
                                
    			JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
                for(int k=0 ; k< objArray.size() ; k++){
                    JSONObject IntentObj = (JSONObject)objArray.get(k);
                    IntentObjMap = new CommandMap();            
                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));                
                    IntentObjMap.put("INTENT_SEQ", iIntentNo);
                    IntentObjMap.put("LIMIT_DA", IntentObj.get("LIMIT_DA"));
                    
                    viewDAO.insertIntentObject(IntentObjMap.getMap());       
                                
                
	                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
	                for(int j=0 ; j< objDetailArray.size() ; j++) {
	                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
	                	IntentObjDetailMap = new CommandMap();
	                	IntentObjDetailMap.put("INTENT_SEQ", iIntentNo);
	                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
	                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
	                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
	                	
	                	viewDAO.insertIntentObjectDetail(IntentObjDetailMap.getMap());
	                }
                }
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}           
	}
		
	@Override
	public void deleteIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntent(map);	
	}
	
	@Override
	public void deleteIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentUserSay(map);
	}
	
	@Override
	public void deleteIntentUserSayDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentUserSayDetail(map);
	}
	
	@Override
	public void deleteIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsUserSays(map);
	}
	
	@Override
	public void deleteIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsIntention(map);
	}
	
	@Override
	public void deleteIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsObject(map);
	}
	
	@Override
	public void deleteIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsObjectDetail(map);
	}
	
	@Override
	public void insertReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		//viewDAO.insertSlotInstans(map);
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotInstansMap;
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotInstansMap = new CommandMap();
                SlotInstansMap.put("INSERT", map.get("INSERT_ITEM"));
                SlotInstansMap.put("VALUES", SlotObj.get("ITEM_DETAIL").toString().replaceAll("et§", "'"));
                                
            	viewDAO.insertReplaceIntent(SlotInstansMap.getMap());                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}            
	}
	
	@Override
	public void updateReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateReplaceIntent(map);
	}

	@Override
	public void deleteReplaceIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteReplaceIntent(map);
	}
	
	@Override
	public void insertEditIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		if (map.containsKey("SEQ_NO"))
			viewDAO.updateEditIntent(map);
		else
			viewDAO.insertEditIntent(map);
	}
	
	@Override
	public void insertAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonItObj;
        JSONObject FillSlotObj;
        CommandMap FillSlotMap;
        CommandMap IntentItMap;
        CommandMap IntentObjMap;
        CommandMap IntentObjDetailMap;
        
        int iAgentNo;
		try {
			if (map.get("SORT_NO") != null && map.get("SORT_NO").toString().equals("0")) {
				viewDAO.updateAgentSortNo(map);
			}
			
			if (map.get("SEQ_NO").toString().isEmpty()) {
				log.debug(map);
				if (map.get("LEFT_POS") == null) {
					viewDAO.insertAgent(map);
				} else {
					viewDAO.insertAgentWithPosition(map);
				}
				log.debug(map);
				iAgentNo = (int)map.get("SEQ_NO");
			}
			else {
				iAgentNo = Integer.parseInt(map.get("SEQ_NO").toString());				
				CommandMap ObjDelMap = new CommandMap();				
				viewDAO.updateAgent(map);
				ObjDelMap.put("SEQ_NO", iAgentNo);
				
				viewDAO.deleteAgentFillingSlot(ObjDelMap.getMap());				
				viewDAO.deleteAgentIntention(ObjDelMap.getMap());
				viewDAO.deleteAgentIntentsObject(ObjDelMap.getMap());
				viewDAO.deleteAgentIntentsObjectDetail(ObjDelMap.getMap());
			}																			
			
			jsonItObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray ItObjArray = (JSONArray)jsonItObj.get("INTENTION_ITEM");
						
			for(int i=0 ; i< ItObjArray.size() ; i++){
                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
                IntentItMap = new CommandMap();
                IntentItMap.put("AGENT_SEQ", iAgentNo);
                IntentItMap.put("FILLING_SEQ", 0);
                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
                IntentItMap.put("ACT", IntentItObj.get("ACT"));
                IntentItMap.put("INTENTION_TYPE", IntentItObj.get("INTENTION_TYPE"));
                
                viewDAO.insertAgentIntent(IntentItMap.getMap());                     
                                
    			JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
                for(int k=0 ; k< objArray.size() ; k++){
                    JSONObject IntentObj = (JSONObject)objArray.get(k);
                    IntentObjMap = new CommandMap();            
                    IntentObjMap.put("AGENT_SEQ", iAgentNo);
                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));
                    IntentObjMap.put("LIMIT_DA", IntentObj.get("LIMIT_DA"));
                    
                    viewDAO.insertAgentObject(IntentObjMap.getMap());       
                            
	                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
	                for(int j=0 ; j< objDetailArray.size() ; j++) {
	                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
	                	IntentObjDetailMap = new CommandMap();
	                	IntentObjDetailMap.put("AGENT_SEQ", iAgentNo);
	                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
	                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
	                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
	                	
	                	viewDAO.insertAgentObjectDetail(IntentObjDetailMap.getMap());
	                }
                }
            }	
			
			JSONArray jsonSlotFillObj = (JSONArray)jsonItObj.get("SLOT_FILLING_ITEM");
			
			ItObjArray.clear();
			
			for (int s = 0; s < jsonSlotFillObj.size(); s++) {
				FillSlotObj = (JSONObject)jsonSlotFillObj.get(s);
				FillSlotMap = new CommandMap();
				FillSlotMap.put("AGENT_SEQ", iAgentNo);
				FillSlotMap.put("SLOT_SEQ", FillSlotObj.get("SLOT_SEQ"));
				FillSlotMap.put("SLOT_NAME", FillSlotObj.get("SLOT_NAME"));
				FillSlotMap.put("CONDITION", FillSlotObj.get("CONDITION"));
				FillSlotMap.put("PROGRESS_SLOT", FillSlotObj.get("PROGRESS_SLOT"));
				viewDAO.insertAgentFillingSlot(FillSlotMap.getMap());
				
				ItObjArray = (JSONArray)FillSlotObj.get("INTENTION_ITEM");
				for(int i=0 ; i< ItObjArray.size() ; i++){
	                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
	                
	                IntentItMap = new CommandMap();
	                IntentItMap.put("AGENT_SEQ", iAgentNo);
	                IntentItMap.put("FILLING_SEQ", FillSlotMap.get("SEQ_NO"));
	                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
	                IntentItMap.put("ACT", IntentItObj.get("ACT"));	        
	                IntentItMap.put("INTENTION_TYPE", IntentItObj.get("INTENTION_TYPE"));
	                
	                viewDAO.insertAgentIntent(IntentItMap.getMap());                     
	                                
	                JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
	                for(int k=0 ; k< objArray.size() ; k++){
	                    JSONObject IntentObj = (JSONObject)objArray.get(k);
	                    IntentObjMap = new CommandMap();    
	                    IntentObjMap.put("AGENT_SEQ", iAgentNo);
	                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
	                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));                	           
	                    IntentObjMap.put("LIMIT_DA", IntentObj.get("LIMIT_DA"));
	                    viewDAO.insertAgentObject(IntentObjMap.getMap());       
	                                
		                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
		                for(int j=0 ; j< objDetailArray.size() ; j++) {
		                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
		                	IntentObjDetailMap = new CommandMap();
		                	IntentObjDetailMap.put("AGENT_SEQ", iAgentNo);
		                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
		                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
		                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
		                	
		                	viewDAO.insertAgentObjectDetail(IntentObjDetailMap.getMap());
		                }
	                }
	            }
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void insertOnlyAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertAgent(map);
	}
	
	@Override
	public void insertAgentMonitoring(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj;        		
		CommandMap MonitoringMap;
		CommandMap ActionMap;
        log.debug(map);          
		try {
			viewDAO.deleteAgentMonitoring(map);
			if (!map.get("MONITORIN_TASK_0").toString().isEmpty()) {
				MonitoringMap = new CommandMap();
				MonitoringMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
				MonitoringMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
				MonitoringMap.put("TYPE", "T");
				MonitoringMap.put("INTEREST", map.get("MONITORIN_TASK_0"));
				MonitoringMap.put("WARNING", map.get("MONITORIN_TASK_1"));
				MonitoringMap.put("DANGER", map.get("MONITORIN_TASK_2"));			
				viewDAO.insertAgentMonitoring(MonitoringMap.getMap());
				
				MonitoringMap = new CommandMap();
				MonitoringMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
				MonitoringMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
				MonitoringMap.put("TYPE", "U");
				MonitoringMap.put("INTEREST", map.get("MONITORIN_USER_0"));
				MonitoringMap.put("WARNING", map.get("MONITORIN_USER_1"));
				MonitoringMap.put("DANGER", map.get("MONITORIN_USER_2"));
				
				viewDAO.insertAgentMonitoring(MonitoringMap.getMap());
			}
			
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("TASK_ACTIONS_ITEM"));
			JSONArray ObjArray = (JSONArray)jsonObj.get("ACTION_ITEM");
			
			for(int i=0 ; i< ObjArray.size() ; i++){
                JSONObject IntentItObj = (JSONObject)ObjArray.get(i);
                ActionMap = new CommandMap();
                ActionMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
                ActionMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
                ActionMap.put("TYPE", IntentItObj.get("TYPE"));
                ActionMap.put("CONDITION", IntentItObj.get("CON"));
                ActionMap.put("ACT", IntentItObj.get("ACT"));
                
                viewDAO.insertAgentActions(ActionMap.getMap());
            }				
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateAgentPosition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj;
        CommandMap AgentMap;
        CommandMap ConnetcionMap;
                
		try {										
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT"));
			JSONArray AgentArray = (JSONArray)jsonObj.get("Agent");
			JSONArray ConnectionArray = (JSONArray)jsonObj.get("Connection");
			
			for(int i=0 ; i< AgentArray.size() ; i++){
				
				JSONObject AgentObj = (JSONObject)AgentArray.get(i);
				AgentMap = new CommandMap();
				AgentMap.put("SEQ_NO", AgentObj.get("Seq"));
				AgentMap.put("LEFT_POS", AgentObj.get("Left"));
				AgentMap.put("TOP_POS", AgentObj.get("Top"));				
				
				viewDAO.updateAgentPosition(AgentMap.getMap());
			}
			
			viewDAO.deleteAgentGraph(map);
			
			for(int i=0 ; i< ConnectionArray.size() ; i++){
				
				JSONObject ConnectionObj = (JSONObject)ConnectionArray.get(i);
				ConnetcionMap = new CommandMap();
				ConnetcionMap.put("SOURCE_ID", ConnectionObj.get("SourceId"));
				ConnetcionMap.put("TARGET_ID", ConnectionObj.get("TargetId"));
				ConnetcionMap.put("CONDITION", ConnectionObj.get("Condition"));				
				ConnetcionMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
				viewDAO.insertAgentGraph(ConnetcionMap.getMap());
			}
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void updateAgentPositionStraightly(Map<String, Object> map) {
		viewDAO.updateAgentPosition(map);
	}
	
	@Override
	public void insertAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteAgentGraph3(map);
		viewDAO.insertAgentGraph(map);								
	}
	
	@Override
	public void deleteAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteAgentGraph3(map);
	}
	
	@Override
	public void deleteAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("PROJECT_SEQ", map.get("SEQ_NO"));
		List<Map<String, Object>> clist = viewDAO.selectSlotClass3(map);
		for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			DropTable(citem);
		}
		
		viewDAO.deleteAgent(map);
		/*viewDAO.deleteAgentFillingSlot(map);
		viewDAO.deleteAgentIntention(map);
		viewDAO.deleteAgentIntentsObject(map);
		viewDAO.deleteAgentIntentsObjectDetail(map);
		viewDAO.deleteAgentGraph2(map);*/
	}
	
	@Override
	public void updateAgentSortNo(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		viewDAO.updateAgentSortNo3(map);
		viewDAO.updateAgentSortNo2(map);		
	}
	
	@Override
	public void insertUser(Map<String, Object> map) {
		// TODO Auto-generated method stub			 
		map.put("CREATE_DATE", CommonUtils.GetDateTime());
		viewDAO.insertUser(map);
	}
	
	@Override
	public void updateUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateUser(map);
	}
	
	@Override
	public void updateUserPassword(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateUserPassword(map);
	}
	
	@Override
	public void updateUserLastLogin(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateUserLastLogin(map);
	}
	
	@Override
	public void deleteUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteUser(map);
	}
	
	@Override
	public void insertLua(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		viewDAO.insertLua(map);
	}
	
	@Override
	public void updateLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateLua(map);
	}
	
	@Override
	public void deleteLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteLua(map);
	}	

	@Override
	public void insertIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotIntentionMap;
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotIntentionMap = new CommandMap();                
                
                SlotIntentionMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
                SlotIntentionMap.put("INTENTION_NAME", SlotObj.get("INTENTION_NAME"));
                SlotIntentionMap.put("INTENTION_SLOT", SlotObj.get("INTENTION_SLOT"));
                SlotIntentionMap.put("INTENTION_SLOT_VALUE", SlotObj.get("INTENTION_SLOT_VALUE"));
                SlotIntentionMap.put("INTENTION", SlotObj.get("OLD_INTENTION_NAME")); //usersay 업데이트용
                
                if (SlotObj.get("SEQ_NO").toString().equals("0")) {                	
                	viewDAO.insertIntention(SlotIntentionMap.getMap());	
				}
                else {
                	SlotIntentionMap.put("SEQ_NO", SlotObj.get("SEQ_NO"));
                	viewDAO.updateIntention(SlotIntentionMap.getMap());                	
                	if (!SlotObj.get("INTENTION_NAME").toString().equals(SlotObj.get("OLD_INTENTION_NAME").toString())) {
                		userSayIntentionUpdate(SlotIntentionMap.getMap(), SlotObj.get("OLD_INTENTION_NAME").toString(), SlotObj.get("INTENTION_NAME").toString(), "U");
					}
                }            	                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void userSayIntentionUpdate(Map<String, Object> map, String OldItentnm, String NewIntentNm, String Type) {
		String[] tempIntent;
		StringBuilder sb = new StringBuilder();
		CommandMap tempMap = new CommandMap();
		List<Map<String,Object>> saylist = viewDAO.selectIntentsUserSays6(map);
		for (Iterator iterator = saylist.iterator(); iterator.hasNext();) {
			Map<String, Object> sayitem = (Map<String, Object>) iterator.next();
			tempIntent = sayitem.get("INTENTION").toString().split(" ");
			sb.setLength(0);
			for (int j = 0; j < tempIntent.length; j++) {				
				if (OldItentnm.equals(tempIntent[j])) {
					if (Type.equals("U"))
						sb.append(String.format("%s ", NewIntentNm));					
				}
				else {
					sb.append(String.format("%s ", tempIntent[j]));
				}
			}

			tempMap.put("SEQ_NO", sayitem.get("SEQ_NO"));
			tempMap.put("INTENTION", sb.toString().trim());
			viewDAO.updateUserSayIntention(tempMap.getMap());
		}
	}
	
	@Override
	public void deleteIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntention(map);
		
		userSayIntentionUpdate(map, map.get("INTENTION").toString(), "", "D");
	}
	
	@Override
	public void updateIntentionGroup(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateIntentionGroup(map);
	}
	
	@Override
	public void insertLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap CmdMap;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("LANGUAGE_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("LANGUAGE_ITEM");
						
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject LangObj = (JSONObject)objArray.get(i);
                CmdMap = new CommandMap();
                
                CmdMap.put("SEQ_NO", LangObj.get("SEQ_NO"));
                CmdMap.put("LANG_NAME", LangObj.get("LANG_NAME"));
                CmdMap.put("ENGINE_NAME", LangObj.get("ENGINE_NAME"));
                CmdMap.put("DIC_NAME", LangObj.get("DIC_NAME"));
                
                
                if (CmdMap.get("SEQ_NO").toString().equals("0")) {
                	viewDAO.insertLanguage(CmdMap.getMap());                    
				}
                else {                	        			                   
                	viewDAO.updateLanguage(CmdMap.getMap());
                }                     
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                
	}
	
	@Override
	public void deleteLanguage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteLanguage(map);
	}
	
	@Override
	public void insertUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertUserDomain(map);
	}
	
	@Override
	public void deleteUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteUserDomain(map);
	}
	
	@Override
	public void updateEncoding(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateEncoding(map);
	}
	
	@Override
	public void updateCountMng(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateCountMng(map);
	}
	
	@Override
	public void insertTutorUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertTutorUserDomain(map);
	}
	
	@Override
	public void deleteTutorUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteTutorUserDomain(map);
	}
	
	@Override
	public void insertReferClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteReferClass(map);
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap CmdMap = new CommandMap();
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
						
			CmdMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
			CmdMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
			
			for(int i=0 ; i< objArray.size() ; i++){				
                CmdMap.put("CLASS_SEQ", objArray.get(i).toString());                
                
                viewDAO.insertReferClass(CmdMap.getMap());                  
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}           
	}	
	
	@Override
	public void insertSampleDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotInstansMap;
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotInstansMap = new CommandMap();
                SlotInstansMap.put("VALUES", SlotObj.get("ITEM_DETAIL").toString());
                                
                viewDAO.insertSampleDomain(SlotInstansMap.getMap());                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Override
	public void deleteSampleDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String[] arrSeq = map.get("ARR_SEQ_NO").toString().split(",");
		
		for (int i = 0; i < arrSeq.length; i++) {
			map.put("SEQ_NO", arrSeq[i]);
			viewDAO.deleteSampleDomain(map);	
		}		
	}
	
	@Override
	public void copyIntents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		String strWhere = "AND AGENT_SEQ = " + map.get("AGENT_SEQ").toString();		
		map.put("WHERE_SQL", strWhere);
		List<Map<String,Object>> Intentlist = selectIntents(map);
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = null;
        JSONArray objArray = null;
        List<String> arrIntentSeq = new ArrayList<String>();
        
		try {			
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("ITEM"));
			objArray = (JSONArray)jsonObj.get("ITEM");
								
			for(int i=0 ; i< objArray.size() ; i++){				
				JSONObject arrObj = (JSONObject)objArray.get(i);				
				for (Map<String, Object> imap : Intentlist) {					
					if (arrObj.get("INTENT_NAME").toString().equals(imap.get("INTENT_NAME").toString())) {
						map.put("ERROR", 1);
						throw new Exception();
					}
				}
				arrIntentSeq.add(arrObj.get("INTENT_SEQ").toString());				
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
		String inentSeq = arrIntentSeq.toString().substring(1, arrIntentSeq.toString().length() -1);
		log.debug(inentSeq);
		map.put("INTENT_SEQ", inentSeq);
		List<String> arrSlotSeq = new ArrayList<String>();
		String[] temparr;
		List<Map<String, Object>> usersaylist = viewDAO.selectIntentsUserSays4(map);
		for (Map<String, Object> usmap : usersaylist) {
			temparr = usmap.get("SLOT_SEQ_NO").toString().split(",");
			for (int i = 0; i < temparr.length; i++) {			
				if (!arrSlotSeq.contains(temparr[i].trim()) && temparr[i].indexOf('_') == -1) {
					arrSlotSeq.add(temparr[i].trim());
				}				
			}			
		}
		map.put("SLOT_SEQ_NO", arrSlotSeq.toString().substring(1, arrSlotSeq.toString().length() -1));
		log.debug(map);
		List<Map<String, Object>> rclist = viewDAO.selectSlotClassAndRefer(map);
		List<Map<String, Object>> addrclist = new ArrayList<Map<String, Object>>();
		
		if (!map.get("SLOT_SEQ_NO").toString().isEmpty()) {
			addrclist = viewDAO.selectSlot4(map);
		}
		log.debug(addrclist);
		
		CommandMap cmdRc = new CommandMap();
		cmdRc.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		cmdRc.put("AGENT_SEQ", map.get("AGENT_SEQ"));		
		
		for (Map<String, Object> rcmap : addrclist) {
			cmdRc.put("CLASS_SEQ", rcmap.get("SEQ_NO"));
			viewDAO.insertReferClass(cmdRc.getMap());
			
			if(!rclist.contains(rcmap))
				rclist.add(rcmap);
		}
		
		Pattern p = Pattern.compile("<[a-zA-Z0-9_\\-]+.[.a-zA-Z0-9_\\-]+(=[\"`',%\\+\\/\\\\?~!@#$%^&*·;:=|\\(\\)\\[\\]{}a-zA-Z가-힣0-9.\\s_\\-]+)?>");
		Matcher m;
		
		map.remove("WHERE_SQL");
		List<Map<String, Object>> classlist = selectSlotClass(map);		
		String[] arrclass;
		String classnm = "";
		Boolean savechk;
		List<Map<String, Object>> objdtllist = viewDAO.selectIntentsObjectDtl2(map);		
		for (Map<String, Object> objdtlmap : objdtllist) {			
			m = p.matcher(objdtlmap.get("OBJECT_VALUE").toString());		
			while (m.find()) {		
				arrclass = m.group().split("\\.");
				savechk = true;
				for (Map<String, Object> rcmap : rclist) {					
					classnm = arrclass[0].substring(1);					
					if(rcmap.get("CLASS_NAME").toString().equals(classnm)) {						
						savechk = false;
						break;
					}
				}
				
				if (savechk) {
					for (Map<String, Object> cmap : classlist) {
						if (cmap.get("CLASS_NAME").toString().equals(classnm)) {
							cmdRc.put("CLASS_SEQ", cmap.get("SEQ_NO"));
							viewDAO.insertReferClass(cmdRc.getMap());
							break;
						}
					}
				}
    		}
		}
					
		for(int i=0 ; i< objArray.size() ; i++){				
			JSONObject arrObj = (JSONObject)objArray.get(i);
				
			cmdRc.put("INTENT_SEQ", arrObj.get("INTENT_SEQ"));
			cmdRc.put("RESPONSE_CHK", arrObj.get("RESPONSE_CHK"));
			
			viewDAO.copyIntent(cmdRc.getMap());			
        }				
	}
	
	@Override
	public void copyTask(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		viewDAO.insertAgent2(map);					
		List<Map<String, Object>> tasklist = selectAgents(map);
		if (tasklist.size() == 2) {
			map.put("SORT_NO", 0);
			viewDAO.updateAgentSortNo2(map);
		}		
		
		Map<String, Object> agent = viewDAO.selectAgent(map);
		Pattern p = Pattern.compile("<[a-zA-Z0-9_\\-]+.[.a-zA-Z0-9_\\-]+(=[\"`',%\\+\\/\\\\?~!@#$%^&*·;:=|\\(\\)\\[\\]{}a-zA-Z가-힣0-9.\\s_\\-]+)?>");
		Matcher m;
		Boolean CopyCheck = false;
		if (agent.get("PROJECT_SEQ").toString().equals(map.get("PROJECT_SEQ").toString())) {
			CopyCheck = true;
		}
		map.put("FROM_PROJECT_SEQ", agent.get("PROJECT_SEQ"));
		
		CommandMap cmdCp = new CommandMap();
		cmdCp.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		cmdCp.put("AGENT_SEQ", map.get("AGENT_SEQ"));
		
		if (CopyCheck) {
			//같은 도메인일 경우 클래스 참조
			CommandMap cmdRc = new CommandMap();
			cmdRc.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
			cmdRc.put("AGENT_SEQ", map.get("SEQ_NO"));
			
			List<Map<String, Object>> aclist = viewDAO.selectSlotClassAndRefer(map);
			for (Map<String, Object> cmap : aclist) {
				cmdRc.put("CLASS_SEQ", cmap.get("SEQ_NO"));
				viewDAO.insertReferClass(cmdRc.getMap());
			}		
		}
		
		
		List<Map<String, Object>> clist = viewDAO.selectSlotClass2(map);			
		List<Map<String, Object>> tclist = null;
		if (!CopyCheck) {
			//다른 도메인 일경우 PROJECT_SEQ 변경
			cmdCp.put("PROJECT_SEQ", map.get("TARGET_PROJECT_SEQ"));
			cmdCp.put("SEQ_NO", map.get("PROJECT_SEQ"));
			tclist = viewDAO.selectSlotClass2(cmdCp.getMap());
		}
		
		
		Boolean savechk = null;
		String[] arrclass;			
		String classnm = "";					
		Map<String, Object> addClass = new HashMap<String, Object>();
				 
		List<Map<String, Object>> taclist = viewDAO.selectSlotClassAndRefer(cmdCp.getMap());				
		List<Map<String, Object>> tafslist = viewDAO.selectAgentSlotFilling(cmdCp.getMap());						
		List<Map<String, Object>> taobjdtllist = viewDAO.selectAgentObjectdtl2(cmdCp.getMap());
		List<Map<String, Object>> usersaylist = viewDAO.selectIntentsUserSays5(cmdCp.getMap());		
		
		String[] temparr;
		List<String> arrSlotSeq = new ArrayList<String>();			
				
		for (Map<String, Object> usmap : usersaylist) {
			temparr = usmap.get("SLOT_SEQ_NO").toString().split(",");
			for (int i = 0; i < temparr.length; i++) {
				if (!arrSlotSeq.contains(temparr[i].trim()) && temparr[i].indexOf('_') == -1) {
					arrSlotSeq.add(temparr[i].trim());
				}
			}
		}
		
		for (Map<String, Object> tafsmap : tafslist) {									
			if (!tafsmap.get("SLOT_SEQ").toString().isEmpty() && !arrSlotSeq.contains(tafsmap.get("SLOT_SEQ").toString())) {
				arrSlotSeq.add(tafsmap.get("SLOT_SEQ").toString());
			}
		}
		
		cmdCp.put("SLOT_SEQ_NO", arrSlotSeq.toString().substring(1, arrSlotSeq.toString().length() -1));		
		
		List<Map<String, Object>> addrclist = new ArrayList<Map<String, Object>>();
				
		if (!cmdCp.get("SLOT_SEQ_NO").toString().isEmpty()) {
			addrclist = viewDAO.selectSlot4(cmdCp.getMap());
		}
		CommandMap cmdRc = new CommandMap();
		cmdRc.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		cmdRc.put("AGENT_SEQ", map.get("AGENT_SEQ"));		
		for (Map<String, Object> rcmap : addrclist) {
			if (CopyCheck) {
				cmdRc.put("CLASS_SEQ", rcmap.get("SEQ_NO"));
				viewDAO.insertReferClass(cmdRc.getMap());
			}
			
			if(!taclist.contains(rcmap))
				taclist.add(rcmap);
		}
		List<Map<String, Object>> stypelist = viewDAO.selectSlot5(cmdCp.getMap());
		for (Map<String, Object> rcmap : stypelist) {
			
			if(!taclist.contains(rcmap)) {
				if (CopyCheck) {
					cmdRc.put("CLASS_SEQ", rcmap.get("SEQ_NO"));
					viewDAO.insertReferClass(cmdRc.getMap());
				}
				
				taclist.add(rcmap);
				GetTypeClass(taclist, cmdRc, rcmap, CopyCheck);
			}
		}
		for (Map<String, Object> tafsitem : taobjdtllist) {				
			m = p.matcher(tafsitem.get("OBJECT_VALUE").toString());		
			while (m.find()) {		
				arrclass = m.group().split("\\.");
				classnm = arrclass[0].substring(1);
				savechk = true;
				for (Map<String, Object> acitem : taclist) {
					if (classnm.equals(acitem.get("CLASS_NAME"))) {
						savechk = false;
						break;
					}
				}
				
				if (savechk) {
					if (CopyCheck) {
						for (Map<String, Object> cmap : clist) {
							if (cmap.get("CLASS_NAME").toString().equals(classnm)) {
								addClass.put("SEQ_NO", cmap.get("SEQ_NO"));
								addClass.put("CLASS_NAME", cmap.get("CLASS_NAME"));
								addClass.put("DESCRIPTION", cmap.get("DESCRIPTION"));
										
								cmdRc.put("CLASS_SEQ", cmap.get("SEQ_NO"));
								viewDAO.insertReferClass(cmdRc.getMap());
								
								if(!taclist.contains(addClass))
									taclist.add(addClass);
								
								break;
							}
						}
					}
					else {
						for (Map<String, Object> cmap : tclist) {
							if (cmap.get("CLASS_NAME").toString().equals(classnm)) {
								addClass.put("SEQ_NO", cmap.get("SEQ_NO"));
								addClass.put("CLASS_NAME", cmap.get("CLASS_NAME"));
								addClass.put("DESCRIPTION", cmap.get("DESCRIPTION"));
								
								if(!taclist.contains(addClass))
									taclist.add(addClass);
								
								break;
							}
						}
					}
				}
    		}
		}
				
		if (!CopyCheck) {
			for (Map<String, Object> citem : clist) {
				for (Map<String, Object> atcitem : taclist) {
					log.debug(citem.get("CLASS_NAME").toString());
					log.debug(atcitem.get("CLASS_NAME").toString());
					if (citem.get("CLASS_NAME").toString().equals(atcitem.get("CLASS_NAME").toString())) {
						map.put("ERROR", 1);
						throw new Exception();
					}
				}
			}				
		}
		
		//복사시작
		SaveCopyTask(map, taclist, CopyCheck, request);
	}
	
	private void GetTypeClass(List<Map<String, Object>> taclist, CommandMap cmdRc, Map<String, Object> rcmap, Boolean CopyCheck) {			
		cmdRc.put("SEQ_NO", rcmap.get("SEQ_NO"));
		List<Map<String, Object>> stypelist = viewDAO.selectSlotTypeClass2(cmdRc.getMap());
		for (Iterator iterator = stypelist.iterator(); iterator.hasNext();) {
			Map<String, Object> stypemap = (Map<String, Object>) iterator.next();
			
			if(!taclist.contains(stypemap)) {
				if (CopyCheck) {
					cmdRc.put("CLASS_SEQ", rcmap.get("SEQ_NO"));
					viewDAO.insertReferClass(cmdRc.getMap());
				}
				
				taclist.add(stypemap);
				GetTypeClass(taclist, cmdRc, stypemap, CopyCheck);
			}
		}		
	}
	
	private void SaveCopyTask(Map<String, Object> map, List<Map<String, Object>> taclist, Boolean copycheck, HttpServletRequest request) throws Exception {
		CommandMap cdb = new CommandMap();
		CommandMap tempmap = new CommandMap();
		List<CommandMap> slotmaplist = new ArrayList<CommandMap>();
		Map<String, String> classmap = new HashMap<String, String>();		
		map.put("COPY_CHECK", "N");
		
		if (!copycheck) {
			map.put("COPY_CHECK", "Y");
			tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
			tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));
								
			for (Map<String, Object> taitem : taclist) {
				tempmap.put("CLASS_NAME", taitem.get("CLASS_NAME"));							
				tempmap.put("DESCRIPTION", taitem.get("DESCRIPTION"));
				tempmap.put("CLASS_SEQ_NO", taitem.get("SEQ_NO"));
				
				insertSlotClass(tempmap.getMap());
				
				cdb.put("SEQ_NO", tempmap.get("SEQ_NO"));
				cdb.put("COL_NAME", "SEQ_NO INT NOT NULL AUTO_INCREMENT PRIMARY KEY");			
			    CreateTable(cdb.getMap());
				
				viewDAO.insertSlot2(tempmap.getMap());	
				
				CommandMap tempsc = new CommandMap();
				tempsc.put("SEQ_NO", tempmap.get("SEQ_NO"));
				tempsc.put("OLD_SEQ_NO", taitem.get("SEQ_NO"));
				slotmaplist.add(tempsc);
				classmap.put(taitem.get("SEQ_NO").toString(), tempmap.get("SEQ_NO").toString());
			}
			
			tempmap.clear();
	    	tempmap.put("SEQ_NO", map.get("PROJECT_SEQ"));
	    	tempmap.put("PROJECT_SEQ", map.get("TARGET_PROJECT_SEQ"));
	    	viewDAO.updateSlot2(tempmap.getMap());
	    	
	    	tempmap.clear();
	    	tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
	    	tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));
								
	    	for (Iterator iterator = slotmaplist.iterator(); iterator.hasNext();) {
				CommandMap slotmapcmd = (CommandMap) iterator.next();
				
				List<Map<String,Object>> slotlist = viewDAO.selectSlot2(slotmapcmd.getMap());
				
				for (Iterator iterator2 = slotlist.iterator(); iterator2.hasNext();) {
					Map<String, Object> sitem = (Map<String, Object>) iterator2.next();					
					cdb.put("SEQ_NO", slotmapcmd.get("SEQ_NO"));
					cdb.put("COL_TYPE", "varchar(200)");
					cdb.put("COL_NAME", String.format("%s_%s", slotmapcmd.get("SEQ_NO"), sitem.get("SEQ_NO")));
										
					if (!sitem.get("SLOT_TYPE").toString().equals("C")) {			
						CreateColumn(cdb.getMap());
					}
				}
			}
		}
		
    	List<Map<String,Object>> slotmaplst = viewDAO.copyTask(map);
    	if (!copycheck) {
	    	Map<String, String> slotmap = new HashMap<String, String>();
	    	
	    	for (Iterator iterator = slotmaplst.iterator(); iterator.hasNext();) {
				Map<String, Object> slotmapitem = (Map<String, Object>) iterator.next();
				
				slotmap.put(slotmapitem.get("OLD_SEQ_NO").toString(), slotmapitem.get("NEW_SEQ_NO").toString());				
			}
	    	
	    	String[] SlotSeqNos = null;	    	
	    	String SlotSeqNo = "";
	    	
	    	
	    	tempmap.clear();	    	
	    	tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));
	    	
	    	List<Map<String,Object>> intentionlist = viewDAO.selectIntentsIntention3(tempmap.getMap());
	    	for (Iterator iterator = intentionlist.iterator(); iterator.hasNext();) {
				Map<String, Object> intentionitem = (Map<String, Object>) iterator.next();
				tempmap.clear();
				SlotSeqNos = intentionitem.get("SLOT_SEQ_NO").toString().split(",");
				
				for (int j = 0; j < SlotSeqNos.length; j++) {
					if (j == 0) {
						SlotSeqNo = slotmap.get(SlotSeqNos[j]); 
					}
					else {
						SlotSeqNo += ", " + slotmap.get(SlotSeqNos[j]); 
					}
				}
				tempmap.put("SEQ_NO", intentionitem.get("SEQ_NO"));
				tempmap.put("SLOT_SEQ_NO", SlotSeqNo);
				viewDAO.updateIntentsIntention(tempmap.getMap());
			}
	    		    	
	    	tempmap.clear();
	    	tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));	    	
	    	String[] arrSubSeq;
	    	StringBuilder sbSubSeq = new StringBuilder();
	    	Map<String, String> SubSeqmap = new HashMap<String, String>();
	    	SlotSeqNos = null;	    	
	    	SlotSeqNo = "";
	    	List<Map<String,Object>> usersaylist = viewDAO.selectIntentsUserSays5(tempmap.getMap());
	    	
	    	for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
				Map<String, Object> usersayitem = (Map<String, Object>) iterator.next();
				tempmap.clear();														
				SlotSeqNos = usersayitem.get("SLOT_SEQ_NO").toString().split(",");				
				
				for (int j = 0; j < SlotSeqNos.length; j++) {
					if (SlotSeqNos[j].indexOf("_") > -1) {
						if (!SubSeqmap.containsKey(SlotSeqNos[j])) {
							arrSubSeq = SlotSeqNos[j].split("_");
							sbSubSeq.setLength(0);							
							CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
							SubSeqmap.put(SlotSeqNos[j], sbSubSeq.toString());
						}

						SlotSeqNos[j] = SubSeqmap.get(SlotSeqNos[j]);						
					}
					else
						SlotSeqNos[j] = slotmap.get(SlotSeqNos[j]);
					
					if (j == 0) {
						SlotSeqNo = SlotSeqNos[j]; 
					}
					else {
						SlotSeqNo += "," + SlotSeqNos[j];
					}
				}
				
				tempmap.put("SEQ_NO", usersayitem.get("SEQ_NO"));
				tempmap.put("SLOT_SEQ_NO", SlotSeqNo);				
				viewDAO.updateCopyIntentUsersay(tempmap.getMap());
			}
	    	
	    	//이것은 Task 복사에만 있는 것
	    	/*
	    	tempmap.clear();
	    	tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));	 
	    	List<String> lstIntentionName = new ArrayList<String>();
	    	String[] arrIntentionName;
	    	
	    	usersaylist = viewDAO.selectIntentsUserSays7(tempmap.getMap());
	    	
	    	for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
				Map<String, Object> usersayitem = (Map<String, Object>) iterator.next();
			
				if (!usersayitem.get("INTENTION").toString().isEmpty()) {
					arrIntentionName = usersayitem.get("INTENTION").toString().split(" ");
					for (int j = 0; j < arrIntentionName.length; j++) {
						if (!lstIntentionName.contains(arrIntentionName[j])) {
							lstIntentionName.add(arrIntentionName[j]);
						}
					}
				}
			}
	    	
	    	if (lstIntentionName.size() > 0) {	    		
		    	tempmap.clear();
		    	tempmap.put("PROJECT_SEQ", map.get("FROM_PROJECT_SEQ"));
		    	List<Map<String,Object>> fromitlist = selectIntention(tempmap.getMap());
		    	Map<String, Map<String,Object>> fromitmap = new HashMap<String, Map<String,Object>>();
		    	for (Iterator iterator = fromitlist.iterator(); iterator.hasNext();) {
					Map<String, Object> map2 = (Map<String, Object>) iterator.next();
					fromitmap.put(map2.get("INTENTION_NAME").toString(), map2);
				}
		    	
		    	tempmap.clear();
		    	tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		    	List<Map<String,Object>> itlist = selectIntention(tempmap.getMap());
		    	Map<String, Map<String,Object>> itmap = new HashMap<String, Map<String,Object>>();
		    	for (Iterator iterator = itlist.iterator(); iterator.hasNext();) {
					Map<String, Object> map2 = (Map<String, Object>) iterator.next();
					itmap.put(map2.get("INTENTION_NAME").toString(), map2);
				}
		    	
		    	Map<String, Object> map3;
		    	for (int i = 0; i < lstIntentionName.size(); i++) {
		    		if (!itmap.containsKey(lstIntentionName.get(i))) {
		    			map3 = fromitmap.get(lstIntentionName.get(i));
		    			tempmap.put("INTENTION_NAME", map3.get("INTENTION_NAME"));
		    			tempmap.put("INTENTION_SLOT", map3.get("INTENTION_SLOT"));
		    			tempmap.put("INTENTION_SLOT_VALUE", map3.get("INTENTION_SLOT_VALUE"));
		    		
		    			viewDAO.insertIntention(tempmap.getMap());	
					}		    		
				}
			}
			*/
	    	//이것은 Task 복사에만 있는 것	    		    	
	    	
	    	tempmap.clear();
	    	tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));	    	
	    	tempmap.put("WHERE_SQL", String.format(" AND AGENT_SEQ = '%s'", map.get("SEQ_NO")));	    	
	    	
			List<Map<String, Object>> definelist = viewDAO.selectSlotDefine2(tempmap.getMap());
	    	for (Iterator iterator = definelist.iterator(); iterator.hasNext();) {
				Map<String, Object> defineitem = (Map<String, Object>) iterator.next();
				if (!SubSeqmap.containsKey(defineitem.get("SUB_SLOT_SEQ").toString())) {
					arrSubSeq = defineitem.get("SUB_SLOT_SEQ").toString().split("_");
					sbSubSeq.setLength(0);
					CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
					log.debug(sbSubSeq.toString());
					SubSeqmap.put(defineitem.get("SUB_SLOT_SEQ").toString(), sbSubSeq.toString());
				}
				
				tempmap.put("SEQ_NO", defineitem.get("SEQ_NO"));
				tempmap.put("SUB_SLOT_SEQ", SubSeqmap.get(defineitem.get("SUB_SLOT_SEQ").toString()));
				viewDAO.updateSlotDefine(tempmap.getMap());	
			}

	    	tempmap.clear();
	    	tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));	    	
	    	tempmap.put("AGENT_SEQ", map.get("SEQ_NO"));
	    	int tempsum = 0;
	    	StringBuilder sbSlotSubSeq = new StringBuilder();
	    	List<Map<String, Object>> slotSublist = viewDAO.selectSlotSub3(tempmap.getMap());
	    	for (Iterator iterator = slotSublist.iterator(); iterator.hasNext();) {
	    		Map<String, Object> slotsubitem = (Map<String, Object>) iterator.next();
				slotsubitem.put("SLOT_SUB_SEQ_NEW", slotsubitem.get("SLOT_SUB_SEQ").toString().replaceAll("temp", ""));
								
				if (!SubSeqmap.containsKey(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString())) {
					arrSubSeq = slotsubitem.get("SLOT_SUB_SEQ_NEW").toString().split("_");
					sbSubSeq.setLength(0);
					CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
					SubSeqmap.put(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString(), sbSubSeq.toString());
				}	
				tempmap.put("SLOT_SUB_SEQ", slotsubitem.get("SLOT_SUB_SEQ"));
				tempmap.put("SLOT_SUB_SEQ_NEW", SubSeqmap.get(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString()));

				sbSubSeq.setLength(0);
				sbSlotSubSeq.setLength(0);
				tempsum = 0;
				arrSubSeq = slotsubitem.get("SLOT_SUB_SEQ_NEW").toString().split("_");
				
				for (int i = 0; i < arrSubSeq.length - 2; i++) {
					if (i == 0) {
						sbSlotSubSeq.append(classmap.get(arrSubSeq[i]));
						sbSubSeq.append(classmap.get(arrSubSeq[i]));						
						tempsum += Integer.parseInt(classmap.get(arrSubSeq[i]));
					}
					else {						
						sbSlotSubSeq.append("." + slotmap.get(arrSubSeq[i]));
						if (i < 2) {
							sbSubSeq.append("_" + slotmap.get(arrSubSeq[i]));	
						}						
						tempsum += Integer.parseInt(slotmap.get(arrSubSeq[i]));
					}					
				}
				tempmap.put("SLOT_SUB_NAME_SEQ", String.format("%s.%s", sbSlotSubSeq.toString(), slotmap.get(arrSubSeq[arrSubSeq.length - 1])));
				tempmap.put("SLOT_COLUMN_NAME", String.format("%s_%s_%s", sbSubSeq.toString(), tempsum, slotmap.get(arrSubSeq[arrSubSeq.length - 1])));
				log.debug(tempmap.getMap());
				viewDAO.updateSlotSub2(tempmap.getMap());	
				
				cdb.put("SEQ_NO", slotsubitem.get("CLASS_SEQ_NO"));
				cdb.put("COL_TYPE", "varchar(200)");
				cdb.put("COL_NAME", tempmap.get("SLOT_COLUMN_NAME"));
				CreateColumn(cdb.getMap());
			}
	    	
	        tempmap.clear();
	        tempmap.put("PROJECT_SEQ", map.get("TARGET_PROJECT_SEQ"));
	        List<Map<String, Object>> slotOldlist = selectSlot(tempmap.getMap());
	        
	        tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
	        List<Map<String, Object>> slotNewlist = selectSlot(tempmap.getMap());
	        
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonOldObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotOldlist));
	        JSONObject jsonNewObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotNewlist));

	        JSONArray objArray;
	        StringBuilder sbOldcol = new StringBuilder();
	        StringBuilder sbNewcol = new StringBuilder();
	        
	    	for (Iterator iterator = slotmaplist.iterator(); iterator.hasNext();) {
				CommandMap slotmapcmd = (CommandMap) iterator.next();
				sbOldcol.setLength(0);
				sbNewcol.setLength(0);
				objArray = (JSONArray)jsonOldObj.get(slotmapcmd.get("OLD_SEQ_NO").toString());
				if (objArray != null) {
					CommonUtils.slotRecursive(slotOldlist, sbOldcol, slotmapcmd.get("OLD_SEQ_NO").toString(), "", jsonOldObj, objArray, "", slotmapcmd.get("OLD_SEQ_NO").toString(), Integer.parseInt(slotmapcmd.get("OLD_SEQ_NO").toString()));
					objArray = (JSONArray)jsonNewObj.get(slotmapcmd.get("SEQ_NO").toString());
					CommonUtils.slotRecursive(slotNewlist, sbNewcol, slotmapcmd.get("SEQ_NO").toString(), "", jsonNewObj, objArray, "", slotmapcmd.get("SEQ_NO").toString(), Integer.parseInt(slotmapcmd.get("SEQ_NO").toString()));
				
					if (sbOldcol.length() > 0 && sbNewcol.length() > 0) {
						slotmapcmd.put("OLD_COL", sbOldcol.toString());
						slotmapcmd.put("NEW_COL", sbNewcol.toString());				
						viewDAO.insertSlotInstans2(slotmapcmd.getMap());	
					}	
				}							
			}
    	}
	}
		
	
	@Override
	public List<Map<String, Object>> selectProject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub		
		if (!map.get("USER_TYPE").toString().equals("SA")) {
			return viewDAO.SelectProjectGu(map);
		}
		else {
			return viewDAO.SelectProjectSa(map);
		}		
	}
	
	@Override
	public List<Map<String, Object>> SelectProjectDelete(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectProjectDelete(map);	
	}
	
	@Override
	public List<Map<String, Object>> SelectCreateProject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectCreateProject(map);
	}
	
	@Override
	public List<Map<String, Object>> SelectProjectName(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectProjectName(map);
	}
	
	@Override
	public List<Map<String, Object>> SelectSampleDomain(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectSampleDomain(map);
	}
	
	
	
	
	@Override
	public List<Map<String, Object>> selectTutorProject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		if (!map.get("USER_TYPE").toString().equals("SA")) {
			return viewDAO.SelectTutorProjectGu(map);
		}
		else {
			return viewDAO.SelectTutorProjectSa(map);
		}		
	}
	
	@Override
	public List<Map<String, Object>> SelectTutorProjectDelete(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectTutorProjectDelete(map);	
	}

	@Override
	public List<Map<String, Object>> SelectTutorCreateProject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectTutorCreateProject(map);
	}
	
	@Override
	public List<Map<String, Object>> SelectTutorProjectName(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectTutorProjectName(map);
	}
	
	@Override
	public List<Map<String, Object>> SelectTutorDialogMap(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectTutorDialogMap(map);
	}

	
	
	
	@Override
	public List<Map<String, Object>> selectSlotClass(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotClass(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotClassRefer(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotClassRefer(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotClassName(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotClassName(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlot(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlot2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlot2(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotTypeCheck(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotTypeCheck(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotTypeClassCheck(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotTypeClassCheck(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotIntents(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		if (map.get("SLOT_SEQ_NO").toString().indexOf('_') > -1) {
			map.put("WHERE_SQL", String.format(" AND SUB_SLOT_SEQ = '%s'", map.get("SLOT_SEQ_NO")));
			String[] tmparr = map.get("SLOT_SEQ_NO").toString().split("_");
			map.put("SLOT_SEQ_NO", tmparr[tmparr.length - 1]);
		}
		else {
			map.put("WHERE_SQL", " AND (SUB_SLOT_SEQ = '' OR SUB_SLOT_SEQ is null)");
			map.put("SUB_SLOT_SEQ", "");
		}
		
		return viewDAO.selectSlotDefine(map);
	}
	
	@Override
	public Map<String, Object> selectSlotAddition(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub	
		return viewDAO.selectSlotAddition(map);
	}
	
	@Override
	public List<Map<String, Object>> selectFunction(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectFunction(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotType(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotType(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObjectPaging(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		if (map.containsKey("WHERE_SQL") && !map.get("WHERE_SQL").toString().isEmpty()) {
			StringBuilder sbSeq = new StringBuilder();;
			int iCnt = 0;
			List<Map<String, Object>> list = viewDAO.selectslotobjectUpdatechk(map);
			for (Map<String, Object> item : list) {				
				if (iCnt == 0) {
					sbSeq.append(" OR SEQ_NO IN (");
					sbSeq.append(item.get("PARENT_SEQ_NO"));					
				}
				else
					sbSeq.append(String.format(", %s", item.get("PARENT_SEQ_NO")));
			}
			if (!sbSeq.toString().isEmpty()) {
				sbSeq.append(")");	
			}
			map.put("WHERE_SQL", map.get("WHERE_SQL").toString().replaceAll("#EVR", sbSeq.toString()));
		}
		return viewDAO.selectSlotObjectPaging(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObjectDetail(map);
	}		
	
	@Override
	public List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObjectDetailExcel(map);
	}	
	
	@Override
	public List<Map<String, Object>> selectChatbotObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectChatbotObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectChatbotObjectDetail(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectChatbotObjectDetail(map);
	}
	
	@Override
	public List<Map<String, Object>> selectChatbotTrainDetail(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectChatbotTrainDetail(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotInstances(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotInstances2(map);
	}
		
	@Override
	public List<Map<String, Object>> selectSlotMapping(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotMapping(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		//return viewDAO.selectSlotTypeClass(map);
		
		List<Map<String, Object>> list = viewDAO.selectSlotTypeClass(map);
		List<Map<String, Object>> returnlist = new ArrayList<Map<String, Object>>();
		GetParentCalss(list, returnlist, map.get("CLASS_SEQ_NO").toString(), "", "");		
		return returnlist;
	}

	private void GetParentCalss(List<Map<String, Object>> list, List<Map<String, Object>> returnlist, String ClassSeq, String SlotName, String SlotSeq) {
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			
			if (item.get("TYPE_SEQ_NO").toString().equals(ClassSeq)) {
				Map<String, Object> addmap = new HashMap<String, Object>();
				addmap.put("SLOT_SEQ_NO", item.get("SLOT_SEQ_NO"));
				addmap.put("CLASS_SEQ_NO", item.get("CLASS_SEQ_NO"));
				addmap.put("CLASS_NAME", item.get("CLASS_NAME"));
				addmap.put("TYPE_SEQ", item.get("TYPE_SEQ_NO"));
								
				if (SlotName.isEmpty()) {					
					addmap.put("SLOT_NAME", item.get("SLOT_NAME").toString());
					addmap.put("SUB_SLOT_SEQ", item.get("SLOT_SEQ_NO").toString());
					returnlist.add(addmap);
					GetParentCalss(list, returnlist, item.get("CLASS_SEQ_NO").toString(), item.get("SLOT_NAME").toString(), item.get("SLOT_SEQ_NO").toString());					
				}
				else {					
					addmap.put("SLOT_NAME", String.format("%s.%s", item.get("SLOT_NAME"), SlotName));
					addmap.put("SUB_SLOT_SEQ", String.format("%s_%s", item.get("SLOT_SEQ_NO"), SlotSeq));
					returnlist.add(addmap);
					GetParentCalss(list, returnlist, item.get("CLASS_SEQ_NO").toString(), String.format("%s.%s", item.get("SLOT_NAME"), SlotName), String.format("%s_%s", item.get("SLOT_SEQ_NO"), SlotSeq));
				}											
			}
		}		
	}
	
	@Override
	public List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectVSlotObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntents(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsUserSays(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsUserSays2(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentIntention(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsIntention(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsObejct(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsObjectDtl(map);
	}
	
	@Override
	public List<Map<String, Object>> selectReplaceIntent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectReplaceIntent(map);
	}
	
	@Override
	public List<Map<String, Object>> selectFliingSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectFillingSlot(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgents(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentsCopy(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentsCopy(map);
	}
	
	@Override
	public Map<String, Object> selectAgent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgent(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentSlotFilling(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentIntention(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentObjectdtl(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentGraph(map);
	}
	
	@Override
	public List<Map<String, Object>> selectLeanNextAgent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLeanNextAgent(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentMornitoring(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentMornitoring(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentActions(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentActions(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSystemDAtype(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSystemDAtype(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		if (map.get("TYPE").toString().equals("UI")) {
			return viewDAO.selectUserIdChk(map);
		}
		else {
			return viewDAO.selectUserApiKeyChk(map);
		}		
	}
	
	@Override
	public List<Map<String, Object>> selectUserList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserList(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserAllList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserAllList(map);
	}
	
	@Override
	public List<Map<String, Object>> selectLuaList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLuaList(map);
	}
	
	@Override
	public List<Map<String, Object>> selectRererenceClass(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectRererenceClass(map);
	}
	
	@Override
	public Map<String, Object> selectLua(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLua(map);
	}
	
	@Override
	public Map<String, Object> selectUser(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUser(map);
	}
	
	@Override
	public Map<String, Object> loginUser(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.loginUser(map);
	}
	
	@Override
	public Map<String, Object> selectIntent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntent(map);
	}
	
	@Override
	public Map<String, Object> selectEditIntent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectEditIntent(map);
	}
	
	@Override
	public Map<String, Object> selectEncoding(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectEncoding(map);
	}

	@Override
	public Map<String, Object> selectCountMng(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectCountMng(map);
	}
	
	@Override
	public Map<String, Object> selectCountCheck(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectCountCheck(map);				
	}
	
	@Override
	public List<Map<String, Object>> selectIntention(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntention(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserDomain(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserDomain(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserDomainList(map);
	}
	
	@Override
	public void updateMigration(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		CommandMap cm = new CommandMap();
		
		map.put("SEQ_NO", 0);
		List<Map<String, Object>> prjlist = selectProject(map);
		List<Map<String, Object>> slotlist = null;
		List<Map<String, Object>> saylist = null;
		String[] arrSlot;
		String[] arrSlotSub;
		for (Iterator iterator = prjlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			cm.put("PROJECT_SEQ", item.get("SEQ_NO"));
		
			slotlist = selectSlot(cm.getMap());
			saylist = viewDAO.selectMigration(cm.getMap());			
			JSONParser jsonParser = new JSONParser();
	        JSONObject slotobj = null;
			try {
				slotobj = (JSONObject)jsonParser.parse(CommonUtils.SetClassNameSlotListToJson(slotlist));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			JSONArray slotArray;
			String SlotSeqNo = "";
			Boolean bUpdateChk = false;
			for (Iterator iterator2 = saylist.iterator(); iterator2.hasNext();) {
				Map<String, Object> sayitem = (Map<String, Object>) iterator2.next();				
				SlotSeqNo = "";
				bUpdateChk = false;
				arrSlot = sayitem.get("SLOT_SEQ_NO").toString().split(",");
				log.debug(sayitem.get("SLOT_SEQ_NO"));
				for (int i = 0; i < arrSlot.length; i++) {
					Boolean bchk = true;
					
					if (arrSlot[i].indexOf("_") == -1) {
						for (Iterator iterator3 = slotlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> slotitem = (Map<String, Object>) iterator3.next();
							
							if (slotitem.get("SEQ_NO").toString().equals(arrSlot[i])) {
								bchk = false;
								break;
							}
						}
						
						if (bchk) {
							cm.put("SEQ_NO", arrSlot[i]);
							Map<String, Object> orislotname = viewDAO.selectMigration2(cm.getMap());							
							if (orislotname != null) {							
								arrSlotSub = orislotname.get("SLOT_NAME").toString().split("\\.");
								
								if (arrSlotSub.length > 1 && slotobj.containsKey(arrSlotSub[0])) {
									slotArray = (JSONArray)slotobj.get(arrSlotSub[0]);
									
									for (int j = 0; j < slotArray.size(); j++) {
										JSONObject obj = (JSONObject)slotArray.get(j);
										
										if (obj.get("SLOT_NAME").toString().equals(arrSlotSub[1])) {
											bUpdateChk = true;
											arrSlot[i] = obj.get("SEQ_NO").toString();										
										}
									}
								}
							}
						}
					}
				}
				
				if (bUpdateChk) {
					for (int i = 0; i < arrSlot.length; i++) {
						if (SlotSeqNo.isEmpty())
							SlotSeqNo = arrSlot[i];
						else
							SlotSeqNo = String.format("%s,%s", SlotSeqNo, arrSlot[i]);
					}
					cm.put("SEQ_NO", sayitem.get("SEQ_NO"));
					cm.put("SLOT_SEQ_NO", SlotSeqNo);
					viewDAO.updateCopyIntentUsersay(cm.getMap());
				}
			}
		}
	}

  @Override
	public Boolean DialogsLearning(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		log.debug("======================0");
		log.debug(map);
		String Path = GetPath("D", false, request) + String.format("%s/%s", GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));	
		log.debug(Path);
		Map<String,Object> encodingdata = selectEncoding(map);
		map.put("ENCODING", encodingdata.get("ENCODING_NAME"));
		sbControl = new StringBuilder();
		
		CommandMap prjcm = new CommandMap();
		prjcm.put("SEQ_NO", map.get("PROJECT_SEQ"));
		prjcm.put("USER_TYPE", "SA");
		List<Map<String, Object>> prjlist = selectProject(prjcm.getMap());
		if (prjlist.get(0).get("CHATBOT_USE_FLAG").toString().equals("Y"))
			sbControl.append("<ChatBot>\nYes\n</ChatBot>\n");
		else
			sbControl.append("<ChatBot>\nNo\n</ChatBot>\n");
		map.put("FILTER_VALUE", prjlist.get(0).get("FILTER_VALUE"));

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			setSlotConversionMap(map);
		}

		LeanTask(map, Path);
		LeanSlot(map, Path);
 		LeanDAType(map, Path);
 		LeanDialogLib(map, Path);
		LeanDomain(map, Path);
		CreateSql(map, Path, request);
		CreateEtcFile(map, Path, request);
		
		return true;
	}

	private void setSlotConversionMap(Map<String, Object> map) {

		slotConversionMap = new HashMap<>();

		// get slot list by project_seq
		List<Map<String, Object>> slotList = viewDAO.selectSlot(map);

		// make conversion map with slot_list
		for (Map<String, Object> slotMap : slotList) {
			String slotBefore = String.format("%s.%s", slotMap.get("CLASS_NAME"), slotMap.get("SLOT_NAME"));
			String slotAfter = String.format("%s\\$%s", slotMap.get("CLASS_NAME"), slotMap.get("SLOT_NAME"));
			slotConversionMap.put(slotBefore, slotAfter);
		}

	}

	// from 'Class.Slot' -> 'Class$Slot'
	private StringBuilder convertSlotString(StringBuilder sb) {
		if (slotConversionMap == null) {
			return sb;
		}
		String s = sb.toString();
		for ( String key : slotConversionMap.keySet()) {
			s = s.replaceAll(key, slotConversionMap.get(key));
		}
		sb = new StringBuilder(s);
		return sb;
	}

	private void LeanTask(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> tasklist = selectAgents(map);
		List<Map<String, Object>> fliinglist;
		List<Map<String, Object>> relatedslotlist;
		List<Map<String, Object>> nexttasklist;
		List<Map<String, Object>> monitoringlist;
		List<Map<String, Object>> actionlist;
		CommandMap taskmap = new CommandMap();
		
		StringBuilder sb = new StringBuilder();			
		StringBuilder sbMornitoring = new StringBuilder();
		StringBuilder sbAction = new StringBuilder();
		int iCnt = 1;
			
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			if (!item.get("AGENT_NAME").toString().equals("END")) {
				sb.append("<task>\n");	
				
				sb.append("<task_name>\n");
				sb.append(item.get("AGENT_NAME").toString() + "\n");
				sb.append("</task_name>\n");
				
				String Condition = "";
				taskmap.put("AGENT_SEQ", item.get("SEQ_NO"));
				nexttasklist = viewDAO.selectLeanNextAgent(taskmap.getMap());
				for (Iterator iterator2 = nexttasklist.iterator(); iterator2.hasNext();) {
					Map<String, Object> nextitem = (Map<String, Object>) iterator2.next();
					
					if (nexttasklist.size() == 1) {
						Condition = nextitem.get("_CONDITION").toString();
					}
					else {
						if (Condition.isEmpty()) {
							Condition = String.format("(%s)", nextitem.get("_CONDITION").toString());
						}
						else {
							Condition +=  String.format(" or (%s)", nextitem.get("_CONDITION").toString());
						}
					}
				}
				
				if (Condition.isEmpty()) {
					if (item.get("TASK_GOAL").toString().isEmpty())
						Condition = "false";						
					else
						Condition = item.get("TASK_GOAL").toString();
				}
				
				sb.append("<task_goal>\n");
				sb.append(UttrReplace(Condition) + "\n");				
				sb.append("</task_goal>\n");

				sb.append("<UI_task_goal>\n");
				sb.append(Condition + "\n");				
				sb.append("</UI_task_goal>\n");
				
				sb.append("<task_type>\n");
				sb.append(item.get("TASK_TYPE").toString() + "\n");
				sb.append("</task_type>\n");
				
				sb.append("<fill_slot>\n");							
				
				fliinglist = selectAgentSlotFilling(taskmap.getMap());
				
				for (Iterator iterator2 = fliinglist.iterator(); iterator2.hasNext();) {
					Map<String, Object> fillingitem = (Map<String, Object>) iterator2.next();
					sb.append("<slot>\n");
					sb.append(fillingitem.get("SLOT_NAME").toString() + ", 1, progress\n");
					sb.append("</slot>\n");
				}
				sb.append("</fill_slot>\n");
				
				sb.append("<related_slot>\n");
				/*relatedslotlist = viewDAO.selectLeanTaskSlot(taskmap.getMap());
				
				if (relatedslotlist.size() > 0) {					
					for (Iterator ireslot = relatedslotlist.iterator(); ireslot.hasNext();) {
						Map<String, Object> slotitem = (Map<String, Object>) ireslot.next();
					
						String[] slots = slotitem.get("SLOT_NAME").toString().split(",");
						
						for (int i = 0; i < slots.length; i++) {
							sb.append(slots[i].replaceAll("=value", "") + "\n");
						}
					}					
				}
				else {
					sb.append("\n");
				}*/
				
				if (!item.containsKey("RELATED_SLOTS")) {
					sb.append("\n");
				}
				else if (item.get("RELATED_SLOTS").toString().isEmpty()) {
					sb.append("\n");
				}
				else {
					String[] slots = item.get("RELATED_SLOTS").toString().split(",");
					
					for (int i = 0; i < slots.length; i++) {
						sb.append(slots[i].trim() + "\n");
					}
				}
				sb.append("</related_slot>\n");
	
				sb.append("<next_task>\n");
				for (Iterator iterator2 = nexttasklist.iterator(); iterator2.hasNext();) {
					Map<String, Object> nextitem = (Map<String, Object>) iterator2.next();				
					sb.append("<next_task_item>\n");
									    
				    sb.append("<task_name>\n");
					sb.append(nextitem.get("AGENT_NAME").toString() + "\n");
					sb.append("</task_name>\n");
				    				    
				    sb.append("<condition>\n");
					sb.append(UttrReplace(nextitem.get("_CONDITION").toString()) + "\n");
					sb.append("</condition>\n");
					
					sb.append("<UIcondition>\n");
					sb.append(nextitem.get("_CONDITION").toString() + "\n");				
					sb.append("</UIcondition>\n");
				    
				    sb.append("<controller>\n");
					sb.append("system\n");
					sb.append("</controller>\n");
				   
				    sb.append("</next_task_item>\n");
				}
				//END TASK
				/*if (item.get("SORT_NO").toString().equals("2")) {
					sb.append("<next_task_item>\n");
				    
				    sb.append("<task_name>\n");
					sb.append("END\n");
					sb.append("</task_name>\n");
					
					sb.append("<condition>\n");
					sb.append(UttrReplace(item.get("END_CONDITION").toString()) + "\n");
					sb.append("</condition>\n");					
				    
					sb.append("<UIcondition>\n");
					sb.append(item.get("END_CONDITION").toString() + "\n");				
					sb.append("</UIcondition>\n");
					
				    sb.append("<controller>\n");
					sb.append("system\n");
					sb.append("</controller>\n");
				   
				    sb.append("</next_task_item>\n");
				}*/
				
				sb.append("</next_task>\n");
				
				sb.append("<reset_slot>\n");
				sb.append(String.format("%s\n", item.get("RESET_SLOT")));
				sb.append("</reset_slot>\n");								
				sb.append("</task>\n\n");
							
				monitoringlist = selectAgentMornitoring(taskmap.getMap());
				if (monitoringlist != null && monitoringlist.size() > 0) {
					sbMornitoring.append("<monitor>\n");
					sbMornitoring.append(String.format("<task>%s</task>\n", item.get("AGENT_NAME")));
					for (Map<String, Object> mitem : monitoringlist) {				
						if (mitem.get("_TYPE").equals("T"))
							sbMornitoring.append(String.format("<task_repeat>%s %s %s</task_repeat>\n", mitem.get("INTEREST"), mitem.get("WARNING"), mitem.get("DANGER")));
						else
							sbMornitoring.append(String.format("<user_turns>%s %s %s</user_turns>\n", mitem.get("INTEREST"), mitem.get("WARNING"), mitem.get("DANGER")));
					}	
					sbMornitoring.append("</monitor>\n");
				}			
				
				actionlist = selectAgentActions(taskmap.getMap());
				if (actionlist != null && actionlist.size() > 0) {					
					
					for (Map<String, Object> aitem : actionlist) {
						sbAction.append("<control>\n");
						sbAction.append(String.format("<task>%s</task>\n", item.get("AGENT_NAME")));
						if (aitem.get("_TYPE").equals("0"))
							sbAction.append("<adaption_time>before user utterance processing</adaption_time>\n");
						else
							sbAction.append("<adaption_time>after user utterance processing</adaption_time>\n");
						
						sbAction.append(String.format("<condition>%s</condition>\n", aitem.get("_CONDITION")));
						sbAction.append(String.format("<action>%s</action>\n", aitem.get("ACT")));
						sbAction.append("</control>\n");
					}						
				}
				
				iCnt++;
			}
		}		
		
		if (!sbMornitoring.toString().isEmpty()) {
			sbControl.append(String.format("<MonitorControl>\n%s</MonitorControl>\n\n", sbMornitoring.toString()));
		}
		
		if (!sbAction.toString().isEmpty()) {
			sbControl.append(String.format("<ActionControl>\n%s</ActionControl>\n\n", sbAction.toString()));
		}
		
		// 4. 파일에 출력
		//FileWriter writer = new FileWriter(Path + ".task.txt");
		log.debug(Path);

		if (Integer.parseInt(map.get("LANG_SEQ_NO").toString()) == 2) {
			sb = convertSlotString(sb);
		}

		CreateTxtFile(map, Path + ".task.txt", sb);
	}
	
	@SuppressWarnings("unused")
	private void LeanSlot(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> classlist = selectSlotClass(map);
		List<Map<String, Object>> slotlist = selectSlot(map);		
		
		JSONParser jsonParser = new JSONParser();
        JSONObject slotobj = null;
		try {
			slotobj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotlist));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		StringBuilder sbslot = new StringBuilder();
		StringBuilder sbdomainslot = new StringBuilder();
		StringBuilder sbslotinit = new StringBuilder();
		StringBuilder sbslotobj = new StringBuilder();
		String AgentName = "";
		
		for (Iterator iterator = classlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			if (item.get("AGENT_NAME").toString().equals("All tasks"))
				AgentName = "All_tasks";
			else
				AgentName = item.get("AGENT_NAME").toString();
			
			sb.append("<class>\n");
			
			sb.append("<name>\n");
			sb.append(item.get("CLASS_NAME").toString() + "\n");
			sb.append("</name>\n");
		
			sb.append("<source>\n");
			sb.append("user\n");
			sb.append("</source>\n");

			sb.append("<slots>\n");			
			AddSlot(sb, sbslot, sbdomainslot, sbslotinit, sbslotobj, slotobj, item.get("SEQ_NO").toString(), map.get("PROJECT_SEQ").toString(), item.get("CLASS_NAME").toString() + ".", 1, item.get("CLASS_NAME").toString() + ".", AgentName, item.get("SEQ_NO").toString(), "");			
			sb.append("</slots>\n");

			sb.append("<task>\n");
			sb.append(AgentName);
			sb.append("\n</task>\n");
			
			sb.append("<description>\n");
			sb.append("<korean>" + item.get("DESCRIPTION") + "</korean>\n");
			sb.append("</description>\n");
			
			sb.append("<key>\n");
			sb.append("\n");
			sb.append("</key>\n");
			
			sb.append("</class>\n\n");
		}
		
		sb.append(sbslot.toString());
		
		if (!sbslotinit.toString().isEmpty()) {
			sbControl.append(String.format("<SlotInit>\n%s</SlotInit>\n\n", sbslotinit.toString()));
		}

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			sb = convertSlotString(sb);
		}

		//FileWriter writer = new FileWriter(Path + ".slot.txt");
		CreateTxtFile(map, Path + ".slot.txt", sb);				
		CreateTxtFile(map, Path + "_slot.txt", sbdomainslot);
		if (!sbslotobj.toString().isEmpty()) {
			CreateTxtFile(map, Path + "_slotlist.txt", sbslotobj);
		}
		
	}	
	
	@SuppressWarnings("unused")
	private void LeanDAType(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> intentlist = selectIntents(map);
		
		StringBuilder sb = new StringBuilder();
		List<String> arrNotDatype = new ArrayList<String>();		
		arrNotDatype.add("inform");
		arrNotDatype.add("request");
		arrNotDatype.add("reqalts");
		arrNotDatype.add("reqmore");
		arrNotDatype.add("search");
		arrNotDatype.add("confirm");
		arrNotDatype.add("confreq");
		arrNotDatype.add("select");
		arrNotDatype.add("affirm");
		arrNotDatype.add("negate");
		arrNotDatype.add("hello");
		arrNotDatype.add("greet");
		arrNotDatype.add("bye");
		arrNotDatype.add("command");
		arrNotDatype.add("silence");
		arrNotDatype.add("hangup");
		arrNotDatype.add("repeat");
		arrNotDatype.add("help");
		arrNotDatype.add("restart");
		arrNotDatype.add("unknown");
		arrNotDatype.add("misunderstanding");
		arrNotDatype.add("thankyou");
		arrNotDatype.add("welcome");
		arrNotDatype.add("ack");
		arrNotDatype.add("hereis");
		arrNotDatype.add("complain_misunderstanding");
		arrNotDatype.add("non_response");
		String AgentName = "";
		String SlotName = "";						
				
		Map<String, DAtypeObject> arrDAtype = new HashMap<String, DAtypeObject>(); 		
		
		for (Iterator iterator = intentlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();								
			
			MakeDaType(item, arrNotDatype, arrDAtype);
		}
		intentlist.clear();
		
		intentlist = viewDAO.selectLeanDatype(map); 
		for (Iterator iterator = intentlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
						
			MakeDaType(item, arrNotDatype, arrDAtype);			
		}
		
		Set <Map.Entry <String, DAtypeObject >> set = arrDAtype.entrySet();

	    for (Map.Entry <String, DAtypeObject> item : set) {
    		sb.append("<define>\n");
			
			sb.append("<DA_class>\n");
			sb.append("default\n");
			sb.append("</DA_class>\n");
		
			sb.append("<DA_type>\n");
			sb.append(item.getKey() + "\n");
			sb.append("</DA_type>\n");
							
			for (String string : item.getValue().arrSlotName) {
				SlotName += string + " "; 
			}
			
			sb.append("<slot>\n");
			sb.append(SlotName.trim() + "\n");
			sb.append("</slot>\n");
			
			for (String string : item.getValue().arrAgentName) {
				AgentName += string + " "; 
			}
			
			sb.append("<task>\n");
			sb.append(AgentName.trim() + "\n");
			sb.append("</task>\n");				
			
			sb.append("</define>\n\n");
	    }		
		
		//FileWriter writer = new FileWriter(Path + ".DAtype.txt");
		CreateTxtFile(map, Path + ".DAtype.txt", sb);
	}		
	
	@SuppressWarnings("unused")
	private void LeanDialogLib(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> tasklist = selectAgents(map);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder sbsub = new StringBuilder();
		StringBuilder sbintentcontrol = new StringBuilder();
		int patternid = 1;
		int sub_patternid = 1;
		//transaction
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			//start
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");
			
			sbsub.append("<dialog_type>\n");
			sbsub.append("transaction\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
			sbsub.append("</task>\n");					
			
			sbsub.append("<subtype>\n");
			sbsub.append("start\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append("\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
								
			sbsub.append("<request_utterance>\n");
			sbsub.append("\n");
			sbsub.append("</request_utterance>\n");			
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> t1list = viewDAO.selectLeanDialogLib1_1(map);

			sbsub.append("<utterance_set>\n");
			for (Iterator iterator2 = t1list.iterator(); iterator2.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) iterator2.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");				
				sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<UIcondition>\n");				
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</UIcondition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();										
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
							
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
								sbsub.append(String.format("%s\n", objval));
								sbsub.append("</DA>\n");
								
								if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
									sbintentcontrol.append("<control>\n");
									sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
									sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
									sbintentcontrol.append("</control>\n");
								}
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>%s-transaction-start-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							//sbsub.append(String.format("<pattern_id>%s-transaction-start-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
							sub_patternid++;
						}
						sbsub.append("</intention>\n");	
					}
					patternid++;
					sub_patternid = 1;
				}
				
				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty()) {					
					if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
						sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
					}
					else
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					
					sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
				}					
				else
					sbsub.append("<action></action>\n<UIaction></UIaction>\n");
				sbsub.append("</utterance>\n");
			}		
			sbsub.append("</utterance_set>\n");											
			
			sbsub.append("</dialog_node>\n\n");
			if (t1list.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
						
			//restart
			sub_patternid = 1;
			patternid = 1;
			
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");

			sbsub.append("<dialog_type>\n");
			sbsub.append("transaction\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
			sbsub.append("</task>\n\n");
			
			sbsub.append("<subtype>\n");
			sbsub.append("restart\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append("\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
						
			sbsub.append("<request_utterance>\n");
			sbsub.append("\n");
			sbsub.append("</request_utterance>\n");
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> t2list = viewDAO.selectLeanDialogLib1_2(map);

			sbsub.append("<utterance_set>\n");
			for (Iterator iterator2 = t2list.iterator(); iterator2.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) iterator2.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<UIcondition>\n");				
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</UIcondition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
					
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");							
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
								sbsub.append(String.format("%s\n", objval));
								sbsub.append("</DA>\n");
								
								if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
									sbintentcontrol.append("<control>\n");
									sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
									sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
									sbintentcontrol.append("</control>\n");
								}
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>%s-transaction-restart-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							//sbsub.append(String.format("<pattern_id>%s-transaction-restart-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
							sub_patternid++;
						}
						sbsub.append("</intention>\n");	
					}
					patternid ++;
					sub_patternid = 1;
				}
				
				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty()) {					
					if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
						sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
					}
					else
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					
					sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
				}					
				else
					sbsub.append("<action></action>\n<UIaction></UIaction>\n");
				sbsub.append("</utterance>\n");
			}		
			sbsub.append("</utterance_set>\n");
					
			sbsub.append("</dialog_node>\n\n");
			if (t2list.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
		}
		
		//response
		sub_patternid = 1;
		patternid = 1;
		
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> rmainllist = viewDAO.selectLeanDialogLib2Main(map);
			
			for (Iterator iterator2 = rmainllist.iterator(); iterator2.hasNext();) {
				Map<String, Object> rmainitem = (Map<String, Object>) iterator2.next();
				map.put("INTENT_SEQ", rmainitem.get("SEQ_NO"));
				List<Map<String, Object>> usersaylist = viewDAO.selectLeanDialogLib2UserSay(map);
				
				//DAtype slot이 있는경우
				for (Iterator usiterator = usersaylist.iterator(); usiterator.hasNext();) {
					Map<String, Object> usersayitem = (Map<String, Object>) usiterator.next();
					
					sbsub.append("<dialog_node>\n");
					
					sbsub.append("<talker>\n");
					sbsub.append("system\n");
					sbsub.append("</talker>\n");
				
					sbsub.append("<dialog_type>\n");
					sbsub.append("response\n");
					sbsub.append("</dialog_type>\n");
					
					sbsub.append("<task>\n");
					sbsub.append("\n");
					sbsub.append("</task>\n");
					
					sbsub.append("<subtype>\n");
					sbsub.append("\n");
					sbsub.append("</subtype>\n");
					
					sbsub.append("<related_task>\n");
					sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
					sbsub.append("</related_task>\n");
					
					sbsub.append("<progress_slot>\n");
					sbsub.append("\n");
					sbsub.append("</progress_slot>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append("\n");
					sbsub.append("</condition>\n");										
					
					sbsub.append("<request_utterance>\n");
					sbsub.append("<talker> user </talker>\n");
					sbsub.append("<condition> 1 </condition>\n");
					
					if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
						if (rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1 == rmainitem.get("INTENT_NAME").toString().indexOf(")")) {
							String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
							sbsub.append(String.format("<DA> %s(%s) </DA>\n", intentName, usersayitem.get("SLOT_NAME").toString()));
						}
						else {												
							if (usersayitem.get("SLOT_NAME").toString().isEmpty()) {							
								sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
							}
							else {
								String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
								String tempMapda = rmainitem.get("INTENT_NAME").toString().substring(rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1, rmainitem.get("INTENT_NAME").toString().indexOf(")"));
								sbsub.append(String.format("<DA> %s(%s, %s) </DA>\n", intentName, tempMapda, usersayitem.get("SLOT_NAME").toString()));
							}
						}
					}
					else {
						sbsub.append(String.format("<DA> %s(%s) </DA>\n", rmainitem.get("INTENT_NAME").toString(), usersayitem.get("SLOT_NAME").toString()));
					}
					
					
					//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "(" + usersayitem.get("SLOT_NAME").toString() + ") </DA>\n");				 
					sbsub.append("</request_utterance>\n");
													
					map.put("SLOT_SEQ_NO", usersayitem.get("SLOT_SEQ_NO"));
					List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_1(map);
													
					sbsub.append("<utterance_set>\n");
					for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
						Map<String, Object> titem = (Map<String, Object>) riterator.next();
																	
						sbsub.append("<utterance>\n");
						
						sbsub.append("<condition>\n");
						sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
						sbsub.append("</condition>\n");
						
						sbsub.append("<UIcondition>\n");				
						sbsub.append(titem.get("UTTR").toString() + "\n");
						sbsub.append("</UIcondition>\n");
						
						sbsub.append("<template>\n");
						
						sbsub.append("<weight>\n");
						sbsub.append(" 1.0 \n");
						sbsub.append("</weight>\n");
						
						map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
						List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
						for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
							Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
							map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
							
							if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
								sbsub.append("<intention>\n");					
						
								sbsub.append("<DA>\n");							
								
								List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
								
								for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
									Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
									if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
										String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
										sbsub.append(String.format("%s\n", objval));
										sbsub.append("</DA>\n");
										
										if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
											sbintentcontrol.append("<control>\n");
											sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
											sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
											sbintentcontrol.append("</control>\n");
										}
									}
									else {
										sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
										//sbsub.append(String.format("<pattern_id>%s-response-%s-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
										sub_patternid++;
									}
								}
								sbsub.append("</intention>\n");						
							}
							else {
								sbsub.append("<intention>\n");					
								
								sbsub.append("<DA>\n");
								sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
								sbsub.append("</DA>\n");
								
								List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
								
								for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
									Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
									//sbsub.append(String.format("<pattern_id>%s-response-%s-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
									sub_patternid++;
								}
								sbsub.append("</intention>\n");	
							}
							patternid++;
							sub_patternid = 1;
						}

						sbsub.append("</template>\n");
						if (!titem.get("ACT").toString().isEmpty()) {					
							if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
								sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
							}
							else
								sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
							
							sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
						}					
						else
							sbsub.append("<action></action>\n<UIaction></UIaction>\n");
						sbsub.append("</utterance>\n");
					}
					sbsub.append("</utterance_set>\n");
					
					sbsub.append("</dialog_node>\n\n");
					
					if (rlist.size() > 0) {
						sb.append(sbsub.toString());
					}
					sbsub.setLength(0);
				}				
				
				//기본 DAType
				patternid = 1;
				sub_patternid = 1;
				
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");						
				
				sbsub.append("<dialog_type>\n");
				sbsub.append("response\n");
				sbsub.append("</dialog_type>\n");
				
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append("\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append("\n");
				sbsub.append("</condition>\n");
			
				sbsub.append("<request_utterance>\n");
				sbsub.append("<talker> user </talker>\n");
				sbsub.append("<condition> 1 </condition>\n");
				if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
					sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
				}
				else {
					sbsub.append(String.format("<DA> %s() </DA>\n", rmainitem.get("INTENT_NAME").toString()));
				}
				
				//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "() </DA>\n");				 							
				sbsub.append("</request_utterance>\n");
				
				List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_2(map);
				
				sbsub.append("<utterance_set>\n");
				for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) riterator.next();
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<UIcondition>\n");				
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</UIcondition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
									sbsub.append(String.format("%s\n", objval));
									sbsub.append("</DA>\n");
									
									if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
										sbintentcontrol.append("<control>\n");
										sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
										sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
										sbintentcontrol.append("</control>\n");
									}
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
									//sbsub.append(String.format("<pattern_id>%s-response-%s-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
									sub_patternid++;
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>%s-response-%s-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
							sbsub.append("</intention>\n");	
						}
						patternid++;
						sub_patternid = 1;
					}

					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty()) {					
						if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
							sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
						}
						else
							sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
						
						sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
					}					
					else
						sbsub.append("<action></action>\n<UIaction></UIaction>\n");
					sbsub.append("</utterance>\n");	
				}
				sbsub.append("</utterance_set>\n");
								
				sbsub.append("</dialog_node>\n\n");
				if (rlist.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
		}
		
		//response2
		patternid = 1;
		sub_patternid = 1;
		String taskName = "";
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> tasknameitem = (Map<String, Object>) iterator.next();
			
			if (taskName.isEmpty()) {
				taskName += tasknameitem.get("AGENT_NAME").toString();
			}
			else {
				taskName += String.format(", %s", tasknameitem.get("AGENT_NAME").toString());
			}
		}
		
		map.put("AGENT_SEQ", 0);
		List<Map<String, Object>> rmainllist = viewDAO.selectLeanDialogLib2Main(map);
		
		for (Iterator iterator2 = rmainllist.iterator(); iterator2.hasNext();) {
			Map<String, Object> rmainitem = (Map<String, Object>) iterator2.next();
			map.put("INTENT_SEQ", rmainitem.get("SEQ_NO"));
			List<Map<String, Object>> usersaylist = viewDAO.selectLeanDialogLib2UserSay(map);
			
			//DAtype slot이 있는경우
			for (Iterator usiterator = usersaylist.iterator(); usiterator.hasNext();) {
				Map<String, Object> usersayitem = (Map<String, Object>) usiterator.next();
				
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");
			
				sbsub.append("<dialog_type>\n");
				sbsub.append("response\n");
				sbsub.append("</dialog_type>\n");
				
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskName + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append("\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append("\n");
				sbsub.append("</condition>\n");										
				
				sbsub.append("<request_utterance>\n");
				sbsub.append("<talker> user </talker>\n");
				sbsub.append("<condition> 1 </condition>\n");
				
				if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
					if (rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1 == rmainitem.get("INTENT_NAME").toString().indexOf(")")) {
						String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
						sbsub.append(String.format("<DA> %s(%s) </DA>\n", intentName, usersayitem.get("SLOT_NAME").toString()));
					}
					else {												
						if (usersayitem.get("SLOT_NAME").toString().isEmpty()) {							
							sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
						}
						else {
							String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
							String tempMapda = rmainitem.get("INTENT_NAME").toString().substring(rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1, rmainitem.get("INTENT_NAME").toString().indexOf(")"));
							sbsub.append(String.format("<DA> %s(%s, %s) </DA>\n", intentName, tempMapda, usersayitem.get("SLOT_NAME").toString()));
						}
					}
				}
				else {
					sbsub.append(String.format("<DA> %s(%s) </DA>\n", rmainitem.get("INTENT_NAME").toString(), usersayitem.get("SLOT_NAME").toString()));
				}
				
				
				//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "(" + usersayitem.get("SLOT_NAME").toString() + ") </DA>\n");				 
				sbsub.append("</request_utterance>\n");
												
				map.put("SLOT_SEQ_NO", usersayitem.get("SLOT_SEQ_NO"));
				List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_1(map);
												
				sbsub.append("<utterance_set>\n");
				for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) riterator.next();
																
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<UIcondition>\n");				
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</UIcondition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
									sbsub.append(String.format("%s\n", objval));
									sbsub.append("</DA>\n");
									
									if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
										sbintentcontrol.append("<control>\n");
										sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
										sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
										sbintentcontrol.append("</control>\n");
									}
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
									//sbsub.append(String.format("<pattern_id>All_tasks-response-%s-%s-%s</pattern_id>\n", rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
									sub_patternid++;
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>All_tasks-response-%s-%s-%s</pattern_id>\n", rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
							sbsub.append("</intention>\n");	
						}
						patternid++;
						sub_patternid = 1;
					}

					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty()) {					
						if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
							sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
						}
						else
							sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
						
						sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
					}					
					else
						sbsub.append("<action></action>\n<UIaction></UIaction>\n");
					sbsub.append("</utterance>\n");
				}
				sbsub.append("</utterance_set>\n");
				
				sbsub.append("</dialog_node>\n\n");
				
				if (rlist.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
			
			//기본 DAType
			patternid = 1;
			sub_patternid = 1;
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");						
			
			sbsub.append("<dialog_type>\n");
			sbsub.append("response\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append("\n");
			sbsub.append("</task>\n");
			
			sbsub.append("<subtype>\n");
			sbsub.append("\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append(taskName + "\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
		
			sbsub.append("<request_utterance>\n");
			sbsub.append("<talker> user </talker>\n");
			sbsub.append("<condition> 1 </condition>\n");
			if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
				sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
			}
			else {
				sbsub.append(String.format("<DA> %s() </DA>\n", rmainitem.get("INTENT_NAME").toString()));
			}
			
			//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "() </DA>\n");				 							
			sbsub.append("</request_utterance>\n");
			
			List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_2(map);
			
			sbsub.append("<utterance_set>\n");
			for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) riterator.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<UIcondition>\n");				
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</UIcondition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
					
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");							
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
								sbsub.append(String.format("%s\n", objval));
								sbsub.append("</DA>\n");
								
								if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
									sbintentcontrol.append("<control>\n");
									sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
									sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
									sbintentcontrol.append("</control>\n");
								}
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>All_tasks-response-%s-%s-%s</pattern_id>\n", rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							//sbsub.append(String.format("<pattern_id>All_tasks-response-%s-%s-%s</pattern_id>\n", rmainitem.get("INTENT_NAME").toString(), patternid, sub_patternid));
							sub_patternid++;
						}
						sbsub.append("</intention>\n");	
					}
					patternid++;
					sub_patternid = 1;
				}

				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty()) {					
					if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
						sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
					}
					else
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					
					sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
				}					
				else
					sbsub.append("<action></action>\n<UIaction></UIaction>\n");
				sbsub.append("</utterance>\n");	
			}
			sbsub.append("</utterance_set>\n");
							
			sbsub.append("</dialog_node>\n\n");
			if (rlist.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
		}
	
		
		//progress
		patternid = 1;
		sub_patternid = 1;
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> pmainlist = viewDAO.selectLeanDialogLib3Main(map);
			
			for (Iterator pmainiterator = pmainlist.iterator(); pmainiterator.hasNext();) {
				Map<String, Object> pmainitem = (Map<String, Object>) pmainiterator.next();
				
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");				
				
				sbsub.append("<dialog_type>\n");
				sbsub.append("progress\n");
				sbsub.append("</dialog_type>\n");
							
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append(pmainitem.get("PROGRESS_SLOT").toString() + "\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(pmainitem.get("_CONDITION").toString() + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<request_utterance>\n");
				sbsub.append("\n");
				sbsub.append("</request_utterance>\n");
								
				map.put("FILLING_SEQ", pmainitem.get("SEQ_NO"));
				List<Map<String, Object>> t1list = viewDAO.selectLeanDialogLib3(map);

				sbsub.append("<utterance_set>\n");
				for (Iterator iterator2 = t1list.iterator(); iterator2.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) iterator2.next();
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(UttrReplace(titem.get("UTTR").toString()) + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<UIcondition>\n");				
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</UIcondition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									String objval = String.format("%s(%s)", objitem.get("OBJECT_VALUE").toString().substring(0, objitem.get("OBJECT_VALUE").toString().indexOf("(")), objdtlitem.get("OBJECT_VALUE").toString());
									sbsub.append(String.format("%s\n", objval));
									sbsub.append("</DA>\n");
									
									if (!objitem.get("LIMIT_DA").toString().isEmpty()) {
										sbintentcontrol.append("<control>\n");
										sbintentcontrol.append(String.format("<system_intent>%s</system_intent>\n", objval));
										sbintentcontrol.append(String.format("<user_response>%s</user_response>\n", objitem.get("LIMIT_DA")));
										sbintentcontrol.append("</control>\n");
									}
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
									//sbsub.append(String.format("<pattern_id>%s-progress-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
									sub_patternid++;
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								//sbsub.append(String.format("<pattern_id>%s-progress-%s-%s</pattern_id>\n", taskitem.get("AGENT_NAME").toString(), patternid, sub_patternid));
								sub_patternid++;
							}
							sbsub.append("</intention>\n");	
						}
						patternid++;
						sub_patternid = 1;
					}
					
					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty()) {					
						if (titem.get("ACT").toString().contains("Count(") || titem.get("ACT").toString().contains("Set(")) {
							sbsub.append("<action>" + UttrReplace(titem.get("ACT").toString()) + "</action>\n");						
						}
						else
							sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
						
						sbsub.append("<UIaction>" + titem.get("ACT").toString() + "</UIaction>\n");
					}					
					else
						sbsub.append("<action></action>\n<UIaction></UIaction>\n");
					sbsub.append("</utterance>\n");
				}		
				sbsub.append("</utterance_set>\n");											
				
				sbsub.append("</dialog_node>\n\n");
				if (t1list.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
		}
		
		if (!sbintentcontrol.toString().isEmpty()) {
			sbControl.append(String.format("<IntentControl>\n%s</IntentControl>\n\n", sbintentcontrol.toString()));
		}

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			sb = convertSlotString(sb);
		}

		//FileWriter writer = new FileWriter(Path + ".dialogLib.txt");
		CreateTxtFile(map, Path + ".dialogLib.txt", sb);
	}
	
	private void LeanDomain(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> usersaylist = viewDAO.selectleanDomain(map);
		List<Map<String, Object>> intentionlist = viewDAO.selectIntention(map);
		
		Map<String, String> intentionMap = new HashMap<String, String>();
		for (Iterator iterator = intentionlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			intentionMap.put(item.get("INTENTION_NAME").toString(), String.format("%s=\"%s\"", item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE")));		
		}
		
		StringBuilder sb = new StringBuilder();		
		String AgentName = "";
		String SlotMap = "";
		String IntentionSlot = "";
		String[] arrIntention;
		
		Map<String, String> arrSlotMap = new HashMap<String, String>();
		
		Pattern p = Pattern.compile("<[a-zA-Z0-9_\\-]+.[.a-zA-Z0-9_\\-]+=[\"`',%\\+\\/\\\\?~!@#$%^&*·;:=|\\(\\)\\[\\]{}a-zA-Z가-힣0-9.\\s_\\-]+>");
		Matcher m;
		
		for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();

			if (map.get("LANG_SEQ_NO").toString().equals("2")) {
				item.put("INTENT_NAME", String.format("# %s", item.get("INTENT_NAME")));
			}
			
			if (!item.get("AGENT_NAME").toString().isEmpty())
				// 영어 지식 파일에는 이 부분이 없어야 정상 작동.
				if (!map.get("LANG_SEQ_NO").toString().equals("2")) {
					AgentName = "@@" + item.get("AGENT_NAME").toString();
				}
			else
				AgentName = "";
			
			IntentionSlot = "";
			if (!item.get("INTENTION").toString().isEmpty()) {
				arrIntention = item.get("INTENTION").toString().split(" ");
				
				for (int i = 0; i < arrIntention.length; i++) {
					if (intentionMap.containsKey(arrIntention[i])) {
						if (IntentionSlot.isEmpty())
							IntentionSlot = intentionMap.get(arrIntention[i]);						
						else
							IntentionSlot = String.format("%s, %s", IntentionSlot, intentionMap.get(arrIntention[i]));
					}						
				}				
			}
			
			arrSlotMap.clear();
			
			SlotMap = item.get("SLOT_MAP").toString();
			m = p.matcher(item.get("SLOT_MAP").toString());
    		int iCnt = 0;
    		while (m.find()) {
    			arrSlotMap.put(String.format("ET_%s", iCnt), m.group());
    			SlotMap = SlotMap.replace(m.group(), String.format("ET_%s", iCnt));
    			iCnt++;
    		}
    		
    		SlotMap = SlotMap.replaceAll("<", "&LT;").replaceAll(">", "&GT;").replaceAll("\\(", "&LB;").replaceAll("\\)", "&RB;").replaceAll("=", "&EQ;").replaceAll("\"", "&DQ;").replaceAll("@", "&AT;").replaceAll("/", "&SL;").replaceAll("#", "&SH;");
    		
    		for( Map.Entry<String, String> slotmap : arrSlotMap.entrySet() ){
    			SlotMap = SlotMap.replace(slotmap.getKey(), slotmap.getValue());
            }
    		
			if (item.get("INTENT_NAME").toString().indexOf("(") > -1) {
				if (item.get("INTENT_NAME").toString().indexOf("(") + 1 == item.get("INTENT_NAME").toString().indexOf(")")) {
					String intentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
					if (IntentionSlot.isEmpty())
						sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s)", intentName, item.get("SLOT_MAPDA").toString()), AgentName));
					else
						sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s, %s)", intentName, IntentionSlot, item.get("SLOT_MAPDA").toString()), AgentName));
				}
				else {
					String intentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
					String tempMapda = item.get("INTENT_NAME").toString().substring(item.get("INTENT_NAME").toString().indexOf("(") + 1, item.get("INTENT_NAME").toString().indexOf(")"));
										
					if (item.get("SLOT_MAPDA").toString().isEmpty()) {
						if (IntentionSlot.isEmpty())
							sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, item.get("INTENT_NAME").toString(), AgentName));
						else
							sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s, %s)", intentName, tempMapda, IntentionSlot), AgentName));
						
					}
					else {
						if (IntentionSlot.isEmpty())
							sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s, %s)", intentName, tempMapda, item.get("SLOT_MAPDA").toString()), AgentName));
						else
							sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s, %s, %s)", intentName, tempMapda, IntentionSlot, item.get("SLOT_MAPDA").toString()), AgentName));					
					}
				}
			}
			else {
				if (IntentionSlot.isEmpty())
					sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s)", item.get("INTENT_NAME").toString(), item.get("SLOT_MAPDA").toString()), AgentName));
				else
					sb.append(String.format("%s\t%s\t%s%s\n", item.get("SAY").toString(), SlotMap, String.format("%s(%s, %s)", item.get("INTENT_NAME").toString(), IntentionSlot, item.get("SLOT_MAPDA").toString()), AgentName));				
			}			
		}

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			sb = convertSlotString(sb);
		}

		//FileWriter writer = new FileWriter(Path + ".DAtype.txt");
		CreateTxtFile(map, Path + ".txt", sb);
	}
	
	private void CreateSql(Map<String, Object> map, String Path, HttpServletRequest request) throws Exception {
		StringBuilder sb = new StringBuilder();
		StringBuilder sbticol = new StringBuilder();
		StringBuilder sbti = new StringBuilder();
		StringBuilder sbtiinsertcol = new StringBuilder();
		StringBuilder sbtiinsert = new StringBuilder();
		StringBuilder sbtiinstance = new StringBuilder();
		StringBuilder sbcanonicalfm = new StringBuilder();		
		StringBuilder sbslotmapping = new StringBuilder();
		
		sb.append("PRAGMA foreign_keys=OFF;\n");
		sb.append("BEGIN TRANSACTION;\n");
		//sb.append("DROP TABLE scenario;\n");
		sb.append("DROP TABLE canonical_form;\n");
		sb.append("CREATE TABLE canonical_form (_idx TEXT DEFAULT '', _key_from TEXT DEFAULT '', _key_to TEXT DEFAULT '', slot_from TEXT DEFAULT '', slot_to TEXT DEFAULT '', _norm_type TEXT DEFAULT '');\n");		
		
		List<Map<String, Object>> classlist = selectSlotClass(map);
		List<Map<String, Object>> sqlslotlist = viewDAO.selectSqlLiteSlot(map);
		List<Map<String, Object>> slotlist = selectSlot(map);
		List<Map<String, Object>> slotmappinglist = viewDAO.selectSlotMapping2(map);		
		List<Map<String, Object>> instancelist = null;
		
		JSONParser jsonParser = new JSONParser();
		JSONObject sqslotobj = null;		
		JSONObject slotobj = null;
		try {
			sqslotobj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(sqlslotlist));
			slotobj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotlist));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		idx = 1;
		int iCnt = 1;
		int iSlotMapingCnt = 1;
		CommandMap timap = new CommandMap();
		for (Iterator iterator = classlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			
			AddSqlSlot(sb, sbcanonicalfm, sbslotmapping, sqslotobj, item.get("SEQ_NO").toString(), item.get("CLASS_NAME").toString() + ".", "cf");
			idx = 1;			
			AddSqlSlot(sbticol, sbcanonicalfm, sbslotmapping, slotobj, item.get("SEQ_NO").toString(), item.get("CLASS_NAME").toString() + ".", "ti");			
			AddSqlInstanceCol(sbtiinsertcol, slotobj, item.get("SEQ_NO").toString(), item.get("SEQ_NO").toString(), "", "", item.get("CLASS_NAME").toString() + ".", item.get("SEQ_NO").toString(), Integer.parseInt(item.get("SEQ_NO").toString()), "tc");			
			AddSqlInstanceCol(sbti, slotobj, item.get("SEQ_NO").toString(), item.get("SEQ_NO").toString(), "", "", item.get("CLASS_NAME").toString() + ".", item.get("SEQ_NO").toString(), Integer.parseInt(item.get("SEQ_NO").toString()), "ic");			
			timap.put("SELECT_SQL", sbti.toString());
			timap.put("CLASS_SEQ", item.get("SEQ_NO").toString());
			if (!sbti.toString().isEmpty()) {							
				instancelist = viewDAO.selectsqlLiteInstance(timap.getMap());	
			}
			String[] arrInstanceCol = sbti.toString().split(",");
			
			if (instancelist != null && !sbti.toString().trim().isEmpty()) {								
				for (Iterator iinstance = instancelist.iterator(); iinstance.hasNext();) {
					Map<String, Object> instanceitem = (Map<String, Object>) iinstance.next();
					for (int i = 0; i < arrInstanceCol.length; i++) {
						if (i == 0) {
							if (instanceitem.containsKey(arrInstanceCol[i].trim()))
								sbtiinsert.append(String.format("'%s'", instanceitem.get(arrInstanceCol[i].trim()).toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''")));
							else
								sbtiinsert.append("''");		
						}
						else {
							if (instanceitem.containsKey(arrInstanceCol[i].trim()))
								sbtiinsert.append(String.format(", '%s'", instanceitem.get(arrInstanceCol[i].trim()).toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''")));
							else
								sbtiinsert.append(", ''");
						}						
					}
					
					sbtiinstance.append(String.format("INSERT INTO task_information (_idx%s) VALUES ('%s', %s);\n", sbtiinsertcol.toString(), iCnt, sbtiinsert.toString()));
					iCnt++;
					sbtiinsert.setLength(0);				
				}
			}
			sbti.setLength(0);
			sbtiinsertcol.setLength(0);
		}		
		
		sb.append("DROP TABLE task_information;\n");
		sb.append(String.format("CREATE TABLE task_information (_idx TEXT DEFAULT ''%s);\n", sbticol.toString()));
		sb.append(sbtiinstance.toString());
		
		sb.append(sbcanonicalfm.toString());		
		sb.append("CREATE INDEX canonical_form_idx ON canonical_form ('_idx','_key_from','_key_to','slot_from','slot_to','_norm_type');\n");
		sb.append("CREATE INDEX task_information_idx ON task_information ('_idx');\n");
		
		for (Map<String, Object> slotmapitem : slotmappinglist) {		
			sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', '%s', '%s', '%s', '%s');\n", idx, slotmapitem.get("TAGGED_SLOT_KEY"), slotmapitem.get("TAGGED_SLOT_VALUE").toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''"), slotmapitem.get("QUERY_COND"), slotmapitem.get("DB_SLOT_KEY"), slotmapitem.get("DB_SLOT_VALUE").toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''")));
			idx++;
		}
		sb.append("DROP TABLE slot_mapping;\n");
		sb.append("CREATE TABLE slot_mapping (_idx TEXT DEFAULT '', tagged_slot_key TEXT DEFAULT '', tagged_slot_value TEXT DEFAULT '', query_cond TEXT DEFAULT '', db_slot_key TEXT DEFAULT '', db_slot_value TEXT DEFAULT '');\n");		
		sb.append(sbslotmapping.toString());
		sb.append("COMMIT;");

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			sb = convertSlotString(sb);
		}

		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".sql", "UTF-8");
		writer.write(sb.toString());
		writer.close();		
		
		ExecSqlite(map, Path, request);		
	}
	
	private void ExecSqlite(Map<String, Object> map, String Path, HttpServletRequest request) throws InterruptedException {
		try { 							
			Runtime rt = Runtime.getRuntime(); 
                        
            Process proc = rt.exec(String.format("chmod -R 777 %s/%s", GetPath("D", false, request), GetDomainName("D", false, map, request)));
            proc.waitFor();
            proc = rt.exec(String.format("chmod -R 777 %s/%s/log", GetPath("D", false, request), GetDomainName("D", false, map, request)));
            proc.waitFor();
            
            proc = Runtime.getRuntime().exec(new String[]{"sh","-c",String.format("sqlite3 %s_DB.sqlite3 < %s.sql", Path, Path)});
            InputStream is = proc.getInputStream(); 
            InputStreamReader isr = new InputStreamReader(is); 
            BufferedReader br = new BufferedReader(isr);
            
            String line;
            while((line=br.readLine())!= null){
                System.out.println(line);
                System.out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	private void CreateEtcFile(Map<String, Object> map, String Path, HttpServletRequest request) throws Exception {
		//domainControl.txt 생성
		
		if (!sbControl.toString().isEmpty()) {
			CreateTxtFile(map, Path + ".domainControl.txt", sbControl);	
		}
		
		List<Map<String, Object>> replacelist = viewDAO.selectReplaceIntent2(map);
		
		StringBuilder sbreplace = new StringBuilder();
		for (Map<String, Object> ritem : replacelist) {
			sbreplace.append(String.format("%s|%s|\n", ritem.get("R_BEFORE").toString().replaceAll("\\|", "&BAR;"), ritem.get("R_AFTER").toString().replaceAll("\\|", "&BAR;")));
		}
		if (!sbreplace.toString().isEmpty()) {
			CreateTxtFile(map, Path + "_replacement.txt", sbreplace);	
		}
		
		StringBuilder sbeditintent = new StringBuilder();
		Map<String,Object> editintent = selectEditIntent(map);
		if (editintent != null && editintent.size() > 0) {
			
		}
		
		if (map.containsKey("FILTER_VALUE") && !map.get("FILTER_VALUE").toString().isEmpty()) {
			sbeditintent.append(String.format("@SVM_Threshold=%s\n", map.get("FILTER_VALUE")));
		}
		if (!sbeditintent.toString().isEmpty()) {
			CreateTxtFile(map, Path + "_prepattern.txt", sbeditintent);	
		}									
		
		if (map.get("ENCODING").toString().equals("UTF-8"))
			map.put("ENCODING", "UTF8");	
		else
			map.put("ENCODING", "CP949");				
				
		// 슬롯 엔트리 사전 엔진 호출 후 임시파일 삭제
		String cmdPath = String.format("%s%s/%s_slotlist.txt", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
		String delfilePath = "";
		File f = new File(cmdPath);
		File delf;
		if (f.exists()) {				
			//6개 파일 삭제
			delfilePath = String.format("%s%s/%s.cdt", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();
			delfilePath = String.format("%s%s/%s.ctx", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();
			delfilePath = String.format("%s%s/%sLexNorm.cdt", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();
			delfilePath = String.format("%s%s/%sLexNorm.ctx", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();
			delfilePath = String.format("%s%s/%sSem.cdt", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();
			delfilePath = String.format("%s%s/%sSem.ctx", GetPath("D", false, request), GetDomainName("D", false, map, request), GetDomainName("D", false, map, request));
			delf = new File(delfilePath);
			if (delf.exists()) delf.delete();						
			
			Runtime rt = Runtime.getRuntime();
			String cmd = String.format("%s/dial -cmd USERDIC_FILE_AUTO -dic %s/KB/%s -domain %s -infilepath %s -FILE_ENC %s -access_key %s", request.getSession().getServletContext().getInitParameter("enginePath"), request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("DL", request), GetDomainName("D", false, map, request), cmdPath, map.get("ENCODING").toString(), GetCookie("SELECT_API", request));
			log.debug(cmd);
			Process proc = rt.exec(cmd); 
	        proc.waitFor();
	        proc = rt.exec(String.format("chmod -R 777 %s/%s", GetPath("D", false, request), GetDomainName("D", false, map, request)));
			//f.delete();
		}
	}
		
	private void CreateTxtFile(Map<String, Object> map, String Path, StringBuilder sb) throws IOException {
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path, map.get("ENCODING").toString());
		writer.write(sb.toString());
		writer.close();
	}
	
	public class DAtypeObject {
		List<String> arrAgentName = new ArrayList<String>();
		List<String> arrSlotName = new ArrayList<String>();		
	}
	
	private void MakeDaType(Map<String, Object> item, List<String> arrNotDatype, Map<String, DAtypeObject> arrDAtype) {
		String AgentName = "";
		String IntentName = "";
		String[] arrTempSlotName = null;
		DAtypeObject TempDaObj = new DAtypeObject();
				
		if (item.get("INTENT_NAME").toString().indexOf("(") > -1) {
			IntentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
			
			if (arrDAtype.containsKey(IntentName))
				TempDaObj = arrDAtype.get(IntentName);
			
			if (item.get("INTENT_NAME").toString().indexOf("(") + 1 != item.get("INTENT_NAME").toString().indexOf(")")) {
				arrTempSlotName = item.get("INTENT_NAME").toString().substring(item.get("INTENT_NAME").toString().indexOf("(") + 1, item.get("INTENT_NAME").toString().indexOf(")")).split(",");											
				for (int i = 0; i < arrTempSlotName.length; i++) {
					if (arrTempSlotName[i].trim().indexOf("=") == -1) {
						if (!TempDaObj.arrSlotName.contains(arrTempSlotName[i].trim())) {
							TempDaObj.arrSlotName.add(arrTempSlotName[i].trim());
						}
					}
				}
			}
		}
		else {
			IntentName = item.get("INTENT_NAME").toString();
		}
		
		AgentName = item.get("AGENT_NAME").toString();
		if (AgentName.equals("All tasks"))
			AgentName = "All_tasks"; 
		
		if (!TempDaObj.arrAgentName.contains(AgentName)) {
			TempDaObj.arrAgentName.add(AgentName);
		}
		
		if (!arrNotDatype.contains(IntentName)) 
			arrDAtype.put(IntentName, TempDaObj);
	}
	
	/*@SuppressWarnings("unused")
	private JSONObject SetSlotListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String classSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (classSeq.equals(item.get("CLASS_SEQ_NO").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(classSeq, templist);
					classSeq = item.get("CLASS_SEQ_NO").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				classSeq = item.get("CLASS_SEQ_NO").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(classSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	
    	JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = null;
             
		try {
			jsonObj = (JSONObject)jsonParser.parse(jsonData);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return jsonObj;
	}*/
	
	@SuppressWarnings("unused")
	private void AddSlot(StringBuilder sb, StringBuilder sbslot, StringBuilder sbdomainslot, StringBuilder sbslotinit, StringBuilder sbslotobj, JSONObject jsonobj, String ClassSeq, String ProjectSeq, String SlotName, int Type, String ClassName, String AgentName, String ParentClassSeq, String SlotSeq) {
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);
		String slotname;
		
		if (slotArray != null) {
			String SubSlotSeq = "";
			if (!SlotSeq.isEmpty()) {
				ParentClassSeq = String.format("%s_%s", ParentClassSeq, SlotSeq);
			}
			
			for (int i = 0; i < slotArray.size(); i++) {
				JSONObject slotobj = (JSONObject)slotArray.get(i);
		
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {					
					AddSlot(sb, sbslot, sbdomainslot, sbslotinit, sbslotobj, jsonobj, slotobj.get("TYPE_SEQ").toString(), ProjectSeq, SlotName + slotobj.get("SLOT_NAME").toString() + ".", Type, ClassName + slotobj.get("TYPE_NAME").toString() + ".", AgentName, ParentClassSeq, slotobj.get("SEQ_NO").toString());
				}
				else {
					SubSlotSeq = String.format("%s_%s_%s", ParentClassSeq, ClassSeq, slotobj.get("SEQ_NO"));
					sbslot.append("<slot>\n");

					sbslot.append("<name>\n");
					slotname = SlotName + slotobj.get("SLOT_NAME");
					sbslot.append(slotname + "\n");
					sb.append(slotname + "\n");
					if (!slotobj.get("DEFAULT_VALUE").toString().isEmpty()) {
						sbslotinit.append(String.format("<%s>\n", slotname));
						sbslotinit.append("<set>\n");
						sbslotinit.append("<condition>true</condition>\n");
						sbslotinit.append(String.format("<value>%s</value>\n", slotobj.get("DEFAULT_VALUE")));
						sbslotinit.append("</set>\n");
						sbslotinit.append(String.format("</%s>\n", slotname));
					}
					sbslot.append("</name>\n");
					
					sbslot.append("<source>\n");
					if (slotobj.get("INSTANCE_FLAG").toString().equals("Y"))
						sbslot.append("KB\n");
					else 
						sbslot.append("system\n");					
					sbslot.append("</source>\n");
					
					sbslot.append("<sense>\n");					
					if (slotobj.get("SLOT_TYPE").toString().equals("O")) {
						CommandMap SlotCmd = new CommandMap();
						SlotCmd.put("SEQ_NO", slotobj.get("TYPE_SEQ").toString());						
						Map<String,Object> SlotItem = viewDAO.selectSlot3(SlotCmd.getMap());
						
						sbslot.append(SlotItem.get("CLASS_NAME") + "." + SlotItem.get("SLOT_NAME") + "\n");
						sbdomainslot.append(String.format("%s \t *%s.%s\n", SlotName + slotobj.get("SLOT_NAME"), SlotItem.get("CLASS_NAME"), SlotItem.get("SLOT_NAME")));
					}
					else {																       				      
						Pattern p = Pattern.compile("\\.");
						Matcher m = p.matcher(SlotName);						
						
						int count = 0;
				        while (m.find())
				            count++;
				        
						if (count > 1) {
							sbslot.append(slotobj.get("CLASS_NAME") + "." + slotobj.get("SLOT_NAME") + "\n");
						}
						else {
							sbslot.append("\n");
							//sbslot.append(slotobj.get("SENSE_NAME") + "\n");
						}
						
						
						if (slotobj.get("TYPE_NAME").toString().equals("Sys.number") || slotobj.get("TYPE_NAME").toString().equals("Sys.string")) {
							if (slotobj.get("SET_FLAG").toString().equals("Y")) {
								sbdomainslot.append(String.format("%s\t*%s\n", SlotName + slotobj.get("SLOT_NAME"), slotobj.get("CLASS_NAME") + "." + slotobj.get("SLOT_NAME")));						
							}
							else {
								sbdomainslot.append(String.format("%s\tNone\n", SlotName + slotobj.get("SLOT_NAME")));
							}
						}
						else {
							if (slotobj.get("SLOT_TYPE").toString().equals("S")) {
								sbdomainslot.append(String.format("%s\t%s\n", SlotName + slotobj.get("SLOT_NAME"), slotobj.get("TYPE_NAME").toString().substring(4)));
							}
							else {
								sbdomainslot.append(String.format("%s\t*%s\n", SlotName + slotobj.get("SLOT_NAME"), slotobj.get("CLASS_NAME") + "." + slotobj.get("SLOT_NAME")));
							}
						}
												
						/*if (slotobj.get("SET_FLAG").toString().equals("Y")) {
							sbdomainslot.append(String.format("%s\tSET\n", SlotName + slotobj.get("SLOT_NAME")));
						}
						else {
							if (slotobj.get("TYPE_NAME").toString().equals("Sys.number") || slotobj.get("TYPE_NAME").toString().equals("Sys.string"))
								sbdomainslot.append(String.format("%s\tNone\n", SlotName + slotobj.get("SLOT_NAME")));				
							else {
								if (slotobj.get("SLOT_TYPE").toString().equals("S")) {
									sbdomainslot.append(String.format("%s\t%s\n", SlotName + slotobj.get("SLOT_NAME"), slotobj.get("TYPE_NAME").toString().substring(4)));
									//sbdomainslot.append(String.format("%s\tdate\n", SlotName + slotobj.get("SLOT_NAME")));
								}
								else {
									sbdomainslot.append(String.format("%s\t*%s\n", SlotName + slotobj.get("SLOT_NAME"), slotobj.get("CLASS_NAME") + "." + slotobj.get("SLOT_NAME")));
								}
							}
						}*/
					}
					sbslot.append("</sense>\n");
					
					sbslot.append("<preceding_slots>\n");
					sbslot.append(slotobj.get("PRCE_SLOT_NAME").toString().replaceAll(",", "") + "\n");
					sbslot.append("</preceding_slots>\n");
					
					sbslot.append("<type>\n");
					if (slotobj.get("SLOT_TYPE").toString().equals("S")) {
						if (slotobj.get("TYPE_NAME").toString().equals("Sys.date") || slotobj.get("TYPE_NAME").toString().equals("Sys.time"))
							sbslot.append(slotobj.get("TYPE_NAME").toString().substring(4) + "\n");
						else if (slotobj.get("TYPE_NAME").toString().equals("Sys.number"))
							sbslot.append("float\n");
						else
							sbslot.append("string\n");
					}
					else {
						sbslot.append(slotobj.get("TYPE_NAME").toString() + "\n");
					}
					sbslot.append("</type>\n");
					
					sbslot.append("<task>\n");
					sbslot.append(AgentName);
					sbslot.append("\n</task>\n");
					
					sbslot.append("<description>\n");
					sbslot.append(slotobj.get("SLOT_DESCRIPTION") + "\n");
					sbslot.append("</description>\n");
					
					CommandMap defineCmd = new CommandMap();
					defineCmd.put("PROJECT_SEQ", ProjectSeq);
					defineCmd.put("SLOT_SEQ_NO", slotobj.get("SEQ_NO"));
					if (SlotSeq.isEmpty()) {
						defineCmd.put("WHERE_SQL", " AND (SUB_SLOT_SEQ = '' OR SUB_SLOT_SEQ is null)");	
					}
					else
						defineCmd.put("WHERE_SQL", String.format(" AND SUB_SLOT_SEQ = '%s'", SubSlotSeq));					
					
					List<Map<String, Object>> definelist = viewDAO.selectSlotDefine(defineCmd.getMap());
					
					sbslot.append("<value_define>\n");
					if (definelist.size() == 0) {
						sbslot.append("\n");
					}
					else {
						for (Iterator defiter = definelist.iterator(); defiter.hasNext();) {
							Map<String, Object> defineitem = (Map<String, Object>) defiter.next();
							sbslot.append("<define>\n");
							sbslot.append(String.format("<condition>%s</condition>\n", UttrReplace(defineitem.get("CON").toString())));
							sbslot.append(String.format("<UIcondition>%s</UIcondition>\n", defineitem.get("CON")));
							sbslot.append(String.format("<target_slot>%s</target_slot>\n", defineitem.get("TARGET_NAME")));
							sbslot.append(String.format("<action>%s</action>\n", UttrReplace(defineitem.get("ACT").toString())));
							sbslot.append(String.format("<UIaction>%s</UIaction>\n", defineitem.get("ACT")));
							sbslot.append("</define>\n");
						}
					}
					sbslot.append("</value_define>\n");
		
					sbslot.append("</slot>\n\n");
					
					CommandMap slotobjCmd = new CommandMap();
					List<Map<String, Object>> slotobjlist = viewDAO.selectSlotObject(defineCmd.getMap());
					List<Map<String, Object>> slotobjdtllist = null;
					for (Map<String, Object> slotobjitem : slotobjlist) {
						slotobjCmd.put("PARENT_SEQ_NO", slotobjitem.get("SEQ_NO"));												
						sbslotobj.append(String.format("ADD\t%s\t*%s\n", slotobjitem.get("OBJECT_NAME"), slotname));
						
						slotobjdtllist = viewDAO.selectSlotObjectDetail(slotobjCmd.getMap());
						
						for (Map<String, Object> slotobjdtlitem : slotobjdtllist) {
							sbslotobj.append(String.format("ADD\t%s\t*%s\n", slotobjdtlitem.get("OBJECT_NAME"), slotname));	
						}
					}					
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void AddSqlSlot(StringBuilder sb, StringBuilder sbcanonicalfm, StringBuilder sbslotmapping, JSONObject jsonobj, String ClassSeq, String SlotName, String Type) {
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);		
		if (slotArray != null) {
			for (int i = 0; i < slotArray.size(); i++) {				
				JSONObject slotobj = (JSONObject)slotArray.get(i);
		
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {	
					AddSqlSlot(sb, sbcanonicalfm, sbslotmapping, jsonobj, slotobj.get("TYPE_SEQ").toString(), SlotName + slotobj.get("SLOT_NAME").toString() + ".", Type);
				}
				else {					
					if (Type == "cf") {			
						if (!slotobj.get("OBJECT_DETAIL_NAME").toString().isEmpty()) {							
							sb.append(String.format("INSERT INTO canonical_form (_idx, slot_from, slot_to) VALUES ('%s', '%s=\"%s\"', '%s=\"%s\"');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), slotobj.get("OBJECT_DETAIL_NAME").toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''"), SlotName + slotobj.get("SLOT_NAME").toString(), slotobj.get("OBJECT_NAME").toString().replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"").replaceAll("'", "''")));														
							idx++;
						}
					}
					else {
						sb.append(String.format(", '%s' TEXT DEFAULT ''", SlotName + slotobj.get("SLOT_NAME")));
						if (slotobj.get("MIN_MAX_VALUE").toString().equals("3")) {
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '!=', %s, NULL);\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '*', '_WHERE_EXTRA_', 'ORDER BY CAST(\"%s\" AS FLOAT) LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							idx++;
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '!=', %s, NULL);\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '*', '_WHERE_EXTRA_', 'ORDER BY CAST(\"%s\" AS FLOAT) DESC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							idx++;
						}
						else if (slotobj.get("MIN_MAX_VALUE").toString().equals("1")) {
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '!=', %s, NULL);\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '*', '_WHERE_EXTRA_', 'ORDER BY CAST(\"%s\" AS FLOAT) LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							idx++;
						}
						else if (slotobj.get("MIN_MAX_VALUE").toString().equals("2")) {
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '!=', %s, NULL);\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							sbcanonicalfm.append(String.format("INSERT INTO canonical_form (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '*', '_WHERE_EXTRA_', 'ORDER BY CAST(\"%s\" AS FLOAT) DESC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), SlotName + slotobj.get("SLOT_NAME").toString()));
							idx++;
						}
						
						if (slotobj.get("SLOT_TYPE").toString().equals("S") && slotobj.get("TYPE_NAME").toString().equals("Sys.number")) {
							sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '*', '_WEHER_EXTRA_', 'ORDER BY(CAST \"%s\" AS FLOAT) ASC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME"), SlotName + slotobj.get("SLOT_NAME")));
							sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '*', '_WEHER_EXTRA_', 'ORDER BY(CAST \"%s\" AS FLOAT) DESC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME"), SlotName + slotobj.get("SLOT_NAME")));
						}
						else if (slotobj.get("SLOT_TYPE").toString().equals("O")) {
							CommandMap tempSlotCmdMap = new CommandMap();
							tempSlotCmdMap.put("SEQ_NO", slotobj.get("TYPE_SEQ"));
							Map<String, Object> tempSlotMap = viewDAO.selectSlot3(tempSlotCmdMap.getMap());
							if (tempSlotMap != null && tempSlotMap.get("TYPE_NAME").toString().equals("Sys.number")) {
								sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'min', '*', '_WEHER_EXTRA_', 'ORDER BY(CAST \"%s\" AS FLOAT) ASC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME"), SlotName + slotobj.get("SLOT_NAME")));
								sbslotmapping.append(String.format("INSERT INTO slot_mapping (_idx, tagged_slot_key, tagged_slot_value, query_cond, db_slot_key, db_slot_value) VALUES (%s, '%s', 'max', '*', '_WEHER_EXTRA_', 'ORDER BY(CAST \"%s\" AS FLOAT) DESC LIMIT 1');\n", idx, SlotName + slotobj.get("SLOT_NAME"), SlotName + slotobj.get("SLOT_NAME")));	
							}
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void AddSqlInstanceCol(StringBuilder sb, JSONObject jsonobj, String ClassSeq, String MainClassSeq, String TopSlotSeq, String SlotSeq, String SlotName, String ParentSeq, int typeSum, String Type) {	
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);
		if (slotArray != null) {
			Boolean bTopChk = false;
			for (int i = 0; i < slotArray.size(); i++) {
				JSONObject slotobj = (JSONObject)slotArray.get(i);
				int tempsum = typeSum + Integer.parseInt(slotobj.get("SEQ_NO").toString());
				
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {
					if (TopSlotSeq.isEmpty()){
						TopSlotSeq = slotobj.get("SEQ_NO").toString();
						bTopChk = true;
					}
					
					if (SlotSeq.isEmpty()) {
						AddSqlInstanceCol(sb, jsonobj, slotobj.get("TYPE_SEQ").toString(), MainClassSeq, TopSlotSeq, slotobj.get("SEQ_NO").toString(), SlotName + slotobj.get("SLOT_NAME").toString() + ".", ParentSeq + "_" + slotobj.get("SEQ_NO").toString(), tempsum, Type);
					}
					else {
						AddSqlInstanceCol(sb, jsonobj, slotobj.get("TYPE_SEQ").toString(), MainClassSeq, TopSlotSeq, slotobj.get("SEQ_NO").toString(), SlotName + slotobj.get("SLOT_NAME").toString() + ".", ParentSeq, tempsum, Type);
					}
					
					if (bTopChk) {
						TopSlotSeq = "";
					}
				}
				else {		
					if (Type == "tc") {
						sb.append(String.format(", '%s'", SlotName + slotobj.get("SLOT_NAME")));
					}
					else {
						String TempSlotSeq = "";
						if (SlotSeq.isEmpty()) {
							TempSlotSeq = String.format("%s_%s", MainClassSeq, slotobj.get("SEQ_NO"));
						}
						else {
							TempSlotSeq = String.format("%s_%s_%s", ParentSeq, typeSum, slotobj.get("SEQ_NO"));
						}	
									
						if (sb.toString().isEmpty()) {
							sb.append(TempSlotSeq);
						}
						else {
							sb.append(String.format(", %s", TempSlotSeq));
						}
					}
				}
			}
		}
	}
	
	private void Directorycopy(File sourceF, File targetF){		
		if (sourceF.exists()) {
			File[] ff = sourceF.listFiles();
			for (File file : ff) {
				File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());
				
				if(file.isDirectory()){
					temp.mkdir();
					Directorycopy(file, temp);
				} else {
					FileInputStream fis = null;
					FileOutputStream fos = null;
					try {
						fis = new FileInputStream(file);
						fos = new FileOutputStream(temp) ;
						byte[] b = new byte[4096];
						int cnt = 0;
						while((cnt=fis.read(b)) != -1){
							fos.write(b, 0, cnt);
						}
					} catch (Exception e) {
						log.debug(e.getMessage());
						e.printStackTrace();					
					} finally{
						try {
							fis.close();
							fos.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	private void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	
	private void deleteAllFiles(String path) {
		File file = new File(path);
		
		//폴더내 파일을 배열로 가져온다.
		if(file.exists()) {
			File[] tempFile = file.listFiles();
			if(tempFile.length > 0){
				for (int i = 0; i < tempFile.length; i++) {
					if(tempFile[i].isFile()){
						tempFile[i].delete();
					} else {
						//재귀함수
						deleteAllFiles(tempFile[i].getPath());
					}
					tempFile[i].delete();
				}
				file.delete();
			}
		}
	}
	
	private void DirectoryList(File sourceF, StringBuilder sb, Map<String, Object> map, String parentname){
		
		File[] ff = sourceF.listFiles();
		for (File file : ff) {			
			if(file.isDirectory()){
				sb.append(String.format("<li name=\"lilog\">%s</li>", file.getName()));
				sb.append("<ul>");
				DirectoryList(file, sb, map, file.getName());
				sb.append("</ul>");
			} else {
				sb.append(String.format("<li><a href=\"#\" onclick=\"fnOpenFile(this, '%s', '%s', '%s', '%s');\">%s</a></li>", map.get("PROJECT_NAME"), map.get("LANG_SEQ_NO"), map.get("USER_SEQ"), parentname, file.getName()));
			}
		}
	}

	@Override
	public List<String> copyDomain(Map<String, Object> map, List<Map<String, Object>> prjlist, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String[] ProjectSeqs = map.get("PROJECT_SEQ").toString().split(",");
				
		CommandMap cmmap = new CommandMap();	
		List<String> returnlist = new ArrayList<String>();
		Boolean nameChk = true;		
		
		for (int i = 0; i < ProjectSeqs.length; i++) {			
			for (Iterator iterator = prjlist.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();
				
				if (item.get("SEQ_NO").toString().equals(ProjectSeqs[i])) {
					cmmap.put("PROJECT_NAME", item.get("PROJECT_NAME"));
					cmmap.put("USER_SEQ_NO", map.get("USER_SEQ"));
					List<Map<String, Object>> prjchk = viewDAO.SelectProjectName(cmmap.getMap());
					
					if (prjchk.size() > 0) {
						if (prjchk.get(0).get("DELETE_FLAG").toString().equals("Y")) {							
							cmmap.put("PROJECT_USER_SEQ", map.get("USER_SEQ"));
							cmmap.put("SEQ_NO", prjchk.get(0).get("SEQ_NO"));
							cmmap.put("USER_API_KEY", map.get("USER_API_KEY"));
							deleteProject(cmmap.getMap(), request);
							cmmap.clear();
							//cmmap.put("OLD_PROJECT_NAME", item.get("PROJECT_NAME"));
							//cmmap.put("PROJECT_NAME", String.format("%s_%s", item.get("PROJECT_NAME"), map.get("USER_ID")));
							cmmap.put("PROJECT_NAME", item.get("PROJECT_NAME"));
							cmmap.put("DESCRIPTION", item.get("DESCRIPTION"));
							cmmap.put("TYPE", "C");
							cmmap.put("LANG_SEQ_NO", item.get("LANG_SEQ_NO"));
							cmmap.put("USER_SEQ", map.get("USER_SEQ"));
							cmmap.put("CHATBOT_USE_FLAG", item.get("CHATBOT_USE_FLAG"));
							cmmap.put("CHATBOT_DATA_FLAG", item.get("CHATBOT_DATA_FLAG"));
							cmmap.put("FILTER_VALUE", item.get("FILTER_VALUE"));							
						}
						else {
							nameChk = false;
							returnlist.add(cmmap.get("PROJECT_NAME").toString());
							break;
						}
					}
					else {
						cmmap.clear();
						//cmmap.put("OLD_PROJECT_NAME", item.get("PROJECT_NAME"));
						//cmmap.put("PROJECT_NAME", String.format("%s_%s", item.get("PROJECT_NAME"), map.get("USER_ID")));
						cmmap.put("PROJECT_NAME", item.get("PROJECT_NAME"));
						cmmap.put("DESCRIPTION", item.get("DESCRIPTION"));
						cmmap.put("TYPE", "C");
						cmmap.put("LANG_SEQ_NO", item.get("LANG_SEQ_NO"));
						cmmap.put("USER_SEQ", map.get("USER_SEQ"));
						cmmap.put("CHATBOT_USE_FLAG", item.get("CHATBOT_USE_FLAG"));
						cmmap.put("CHATBOT_DATA_FLAG", item.get("CHATBOT_DATA_FLAG"));
						cmmap.put("FILTER_VALUE", item.get("FILTER_VALUE"));
						cmmap.put("USER_API_KEY", map.get("USER_API_KEY"));
						//break;
					}
				}
			}
			if (nameChk) {
				SaveCopyDomain(map, cmmap, request);
			}
			nameChk = false;
		}		
		return returnlist;
	}
	
	@Override
	public void saveVersion(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub						
		CommandMap cmmap = new CommandMap();								
		cmmap.put("PROJECT_NAME", map.get("VERSION_NAME"));
		cmmap.put("DESCRIPTION", map.get("DESCRIPTION"));
		cmmap.put("TYPE", "V");
		cmmap.put("USER_SEQ", map.get("USER_SEQ"));
		cmmap.put("PARENT_SEQ_NO", map.get("PROJECT_SEQ"));
		cmmap.put("CHATBOT_USE_FLAG", map.get("CHATBOT_USE_FLAG"));
		cmmap.put("CHATBOT_DATA_FLAG", map.get("CHATBOT_DATA_FLAG"));
		cmmap.put("FILTER_VALUE", map.get("FILTER_VALUE"));
		
		SaveCopyDomain(map, cmmap, request);
	}
	
	@Override
	public void loadVersion(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub						
		CommandMap cmmap = new CommandMap();			
		cmmap.put("SEQ_NO", map.get("SEQ_NO"));
		cmmap.put("PROJECT_NAME", map.get("PROJECT_NAME"));
		cmmap.put("DESCRIPTION", map.get("DESCRIPTION"));
		cmmap.put("TYPE", "L");
		cmmap.put("USER_SEQ", map.get("USER_SEQ"));		
		cmmap.put("CHATBOT_USE_FLAG", map.get("CHATBOT_USE_FLAG"));
		cmmap.put("CHATBOT_DATA_FLAG", map.get("CHATBOT_DATA_FLAG"));
		cmmap.put("FILTER_VALUE", map.get("FILTER_VALUE"));
		
		SaveCopyDomain(map, cmmap, request);
	}
	
	@Override
	public void otherDomainCrate(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub						
		CommandMap cmmap = new CommandMap();
		//cmmap.put("OLD_PROJECT_NAME", map.get("OLD_PROJECT_NAME"));
		cmmap.put("PROJECT_NAME", map.get("PROJECT_NAME"));
		cmmap.put("DESCRIPTION", map.get("DESCRIPTION"));
		cmmap.put("TYPE", "O");
		cmmap.put("USER_SEQ", map.get("USER_SEQ"));
		cmmap.put("FROM_SEQ_NO", map.get("FROM_SEQ_NO"));
		cmmap.put("CHATBOT_USE_FLAG", map.get("CHATBOT_USE_FLAG"));
		cmmap.put("CHATBOT_DATA_FLAG", map.get("CHATBOT_DATA_FLAG"));
		cmmap.put("FILTER_VALUE", map.get("FILTER_VALUE"));
		
		SaveCopyDomain(map, cmmap, request);
		map.put("SEQ_NO", cmmap.get("SEQ_NO"));
	}
	
	private void SaveCopyDomain(Map<String, Object> map, CommandMap cmmap, HttpServletRequest request) throws Exception {
		CommandMap cdb = new CommandMap();
		CommandMap tempmap = new CommandMap();
		List<CommandMap> slotmaplist = new ArrayList<CommandMap>();
		Map<String, String> classmap = new HashMap<String, String>();
		
		insertProject(cmmap.getMap(), request);				
		tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
		tempmap.put("USER_SEQ", map.get("USER_SEQ"));
		insertUserDomain(tempmap.getMap());
		tempmap.clear();
		
		cmmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
    	
    	List<Map<String, Object>> clist = viewDAO.selectSlotClass2(cmmap.getMap());
    	for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
    		
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			tempmap.put("CLASS_NAME", citem.get("CLASS_NAME"));
			tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
			tempmap.put("CLASS_SEQ_NO", citem.get("SEQ_NO"));
			tempmap.put("DESCRIPTION", citem.get("DESCRIPTION"));
			tempmap.put("AGENT_SEQ", citem.get("AGENT_SEQ")); //sp에서 업데이트 처야함
			
			insertSlotClass(tempmap.getMap());
			
			cdb.put("SEQ_NO", tempmap.get("SEQ_NO"));
			cdb.put("COL_NAME", "SEQ_NO INT NOT NULL AUTO_INCREMENT PRIMARY KEY");			
		    CreateTable(cdb.getMap());
			
			viewDAO.insertSlot2(tempmap.getMap());
			
			CommandMap tempsc = new CommandMap();
			tempsc.put("SEQ_NO", tempmap.get("SEQ_NO"));
			tempsc.put("OLD_SEQ_NO", citem.get("SEQ_NO"));
			slotmaplist.add(tempsc);
			classmap.put(citem.get("SEQ_NO").toString(), tempmap.get("SEQ_NO").toString());
		}
    	
    	viewDAO.updateSlot2(cmmap.getMap());
    	
    	tempmap.clear();
    	tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));					
							
    	List<Map<String, Object>> slist = selectSlot(tempmap.getMap());    	
    	JSONParser jsonParser = new JSONParser();
        JSONObject slotobj = null;
		try {
			slotobj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slist));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	for (Iterator iterator = slotmaplist.iterator(); iterator.hasNext();) {
			CommandMap slotmapitem = (CommandMap) iterator.next();
			
			List<Map<String,Object>> slotlist = viewDAO.selectSlot2(slotmapitem.getMap());
			
			for (Iterator iterator2 = slotlist.iterator(); iterator2.hasNext();) {
				Map<String, Object> sitem = (Map<String, Object>) iterator2.next();
				
				cdb.put("SEQ_NO", slotmapitem.get("SEQ_NO"));
				cdb.put("COL_TYPE", "varchar(200)"); 					
				cdb.put("COL_NAME", String.format("%s_%s", slotmapitem.get("SEQ_NO"), sitem.get("SEQ_NO")));
									 
				if (!sitem.get("SLOT_TYPE").toString().equals("C")) {
					CreateColumn(cdb.getMap());
				}
			}
		}
    	
    	List<Map<String,Object>> slotmaplst = viewDAO.copyDomain(cmmap.getMap());
    	Map<String, String> slotmap = new HashMap<String, String>();
    	
    	for (Iterator iterator = slotmaplst.iterator(); iterator.hasNext();) {
			Map<String, Object> slotmapitem = (Map<String, Object>) iterator.next();
			
			slotmap.put(slotmapitem.get("OLD_SEQ_NO").toString(), slotmapitem.get("NEW_SEQ_NO").toString());				
		}
    	
    	String[] SlotSeqNos = null;	    	
    	String SlotSeqNo = "";
    	
    	List<Map<String,Object>> intentionlist = viewDAO.selectIntentsIntention2(cmmap.getMap());
    	for (Iterator iterator = intentionlist.iterator(); iterator.hasNext();) {
			Map<String, Object> intentionitem = (Map<String, Object>) iterator.next();
			tempmap.clear();
			SlotSeqNos = intentionitem.get("SLOT_SEQ_NO").toString().split(",");
			
			for (int j = 0; j < SlotSeqNos.length; j++) {
				if (j == 0) {
					SlotSeqNo = slotmap.get(SlotSeqNos[j]); 
				}
				else {
					SlotSeqNo += ", " + slotmap.get(SlotSeqNos[j]); 
				}
			}			
			tempmap.put("SEQ_NO", intentionitem.get("SEQ_NO"));
			tempmap.put("SLOT_SEQ_NO", SlotSeqNo);
			viewDAO.updateIntentsIntention(tempmap.getMap());
		}
    	
    	String[] arrSubSeq;
    	StringBuilder sbSubSeq = new StringBuilder();
    	Map<String, String> SubSeqmap = new HashMap<String, String>();    	
    	SlotSeqNos = null;	    	
    	SlotSeqNo = "";
    	List<Map<String,Object>> usersaylist = viewDAO.selectIntentsUserSays3(cmmap.getMap());
    	for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
			Map<String, Object> usersayitem = (Map<String, Object>) iterator.next();
			tempmap.clear();
			SlotSeqNos = usersayitem.get("SLOT_SEQ_NO").toString().split(",");
						
			for (int j = 0; j < SlotSeqNos.length; j++) {
				if (SlotSeqNos[j].indexOf("_") > -1) {						
					if (!SubSeqmap.containsKey(SlotSeqNos[j])) {
						arrSubSeq = SlotSeqNos[j].split("_");
						sbSubSeq.setLength(0);							
						CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
						SubSeqmap.put(SlotSeqNos[j], sbSubSeq.toString());
					}

					SlotSeqNos[j] = SubSeqmap.get(SlotSeqNos[j]);					
				}
				else
					SlotSeqNos[j] = slotmap.get(SlotSeqNos[j]);
				
				if (j == 0) {
					SlotSeqNo = SlotSeqNos[j]; 
				}
				else {
					SlotSeqNo += "," + SlotSeqNos[j];
				}
			}
			
			tempmap.put("SEQ_NO", usersayitem.get("SEQ_NO"));
			tempmap.put("SLOT_SEQ_NO", SlotSeqNo);			
			viewDAO.updateCopyIntentUsersay(tempmap.getMap());					
		}
    	
    	tempmap.clear();
    	tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));	    	
    	tempmap.put("WHERE_SQL", "");    	
    	
		List<Map<String, Object>> definelist = viewDAO.selectSlotDefine2(tempmap.getMap());
		
    	for (Iterator iterator = definelist.iterator(); iterator.hasNext();) {
			Map<String, Object> defineitem = (Map<String, Object>) iterator.next();
			if (!SubSeqmap.containsKey(defineitem.get("SUB_SLOT_SEQ").toString())) {
				arrSubSeq = defineitem.get("SUB_SLOT_SEQ").toString().split("_");
				sbSubSeq.setLength(0);
				CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
				SubSeqmap.put(defineitem.get("SUB_SLOT_SEQ").toString(), sbSubSeq.toString());
			}
			
			tempmap.put("SEQ_NO", defineitem.get("SEQ_NO"));
			tempmap.put("SUB_SLOT_SEQ", SubSeqmap.get(defineitem.get("SUB_SLOT_SEQ").toString()));
			viewDAO.updateSlotDefine(tempmap.getMap());	
		}
    	
    	tempmap.clear();
    	tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
    	int tempsum = 0;
    	StringBuilder sbSlotSubSeq = new StringBuilder();
    	List<Map<String, Object>> slotSublist = viewDAO.selectSlotSub(tempmap.getMap());
    	for (Iterator iterator = slotSublist.iterator(); iterator.hasNext();) {
    		Map<String, Object> slotsubitem = (Map<String, Object>) iterator.next();
			slotsubitem.put("SLOT_SUB_SEQ_NEW", slotsubitem.get("SLOT_SUB_SEQ").toString().replaceAll("temp", ""));
			log.debug(slotsubitem);
			log.debug(SubSeqmap);				
			if (!SubSeqmap.containsKey(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString())) {
				arrSubSeq = slotsubitem.get("SLOT_SUB_SEQ_NEW").toString().split("_");
				sbSubSeq.setLength(0);
				CommonUtils.replaceNewSeq(arrSubSeq, classmap, slotmap, sbSubSeq);
				SubSeqmap.put(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString(), sbSubSeq.toString());
			}	
			tempmap.put("SLOT_SUB_SEQ", slotsubitem.get("SLOT_SUB_SEQ"));
			tempmap.put("SLOT_SUB_SEQ_NEW", SubSeqmap.get(slotsubitem.get("SLOT_SUB_SEQ_NEW").toString()));

			sbSubSeq.setLength(0);
			sbSlotSubSeq.setLength(0);
			tempsum = 0;
			arrSubSeq = slotsubitem.get("SLOT_SUB_SEQ_NEW").toString().split("_");
			
			for (int i = 0; i < arrSubSeq.length - 2; i++) {
				if (i == 0) {
					sbSlotSubSeq.append(classmap.get(arrSubSeq[i]));
					sbSubSeq.append(classmap.get(arrSubSeq[i]));						
					tempsum += Integer.parseInt(classmap.get(arrSubSeq[i]));
				}
				else {						
					sbSlotSubSeq.append("." + slotmap.get(arrSubSeq[i]));
					if (i < 2) {
						sbSubSeq.append("_" + slotmap.get(arrSubSeq[i]));	
					}						
					tempsum += Integer.parseInt(slotmap.get(arrSubSeq[i]));
				}					
			}
			tempmap.put("SLOT_SUB_NAME_SEQ", String.format("%s.%s", sbSlotSubSeq.toString(), slotmap.get(arrSubSeq[arrSubSeq.length - 1])));
			tempmap.put("SLOT_COLUMN_NAME", String.format("%s_%s_%s", sbSubSeq.toString(), tempsum, slotmap.get(arrSubSeq[arrSubSeq.length - 1])));
			log.debug(tempmap.getMap());
			viewDAO.updateSlotSub2(tempmap.getMap());	
			
			cdb.put("SEQ_NO", slotsubitem.get("CLASS_SEQ_NO"));
			cdb.put("COL_TYPE", "varchar(200)");
			cdb.put("COL_NAME", tempmap.get("SLOT_COLUMN_NAME"));
			CreateColumn(cdb.getMap());
		}
    	
    	tempmap.clear();
        tempmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
        List<Map<String, Object>> slotOldlist = selectSlot(tempmap.getMap());
        
        tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
        List<Map<String, Object>> slotNewlist = selectSlot(tempmap.getMap());
                
        JSONObject jsonOldObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotOldlist));
        JSONObject jsonNewObj = (JSONObject)jsonParser.parse(CommonUtils.SetSlotListToJson(slotNewlist));

        JSONArray objArray;
        StringBuilder sbOldcol = new StringBuilder();
        StringBuilder sbNewcol = new StringBuilder();
        
    	for (Iterator iterator = slotmaplist.iterator(); iterator.hasNext();) {
			CommandMap slotmapcmd = (CommandMap) iterator.next();
			sbOldcol.setLength(0);
			sbNewcol.setLength(0);
			objArray = (JSONArray)jsonOldObj.get(slotmapcmd.get("OLD_SEQ_NO").toString());						
			if (objArray != null) {				
				CommonUtils.slotRecursive(slotOldlist, sbOldcol, slotmapcmd.get("OLD_SEQ_NO").toString(), "", jsonOldObj, objArray, "", slotmapcmd.get("OLD_SEQ_NO").toString(), Integer.parseInt(slotmapcmd.get("OLD_SEQ_NO").toString()));				
				objArray = (JSONArray)jsonNewObj.get(slotmapcmd.get("SEQ_NO").toString());						
				CommonUtils.slotRecursive(slotNewlist, sbNewcol, slotmapcmd.get("SEQ_NO").toString(), "", jsonNewObj, objArray, "", slotmapcmd.get("SEQ_NO").toString(), Integer.parseInt(slotmapcmd.get("SEQ_NO").toString()));
			
				if (sbOldcol.length() > 0 && sbNewcol.length() > 0) {
					slotmapcmd.put("OLD_COL", sbOldcol.toString());
					slotmapcmd.put("NEW_COL", sbNewcol.toString());				
					viewDAO.insertSlotInstans2(slotmapcmd.getMap());	
				}	
			}		
		}
	}	
		
	@Override
	public Boolean SaveChatbotTalk(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		//String Path = "E:\\dev\\file\\webtool\\www_data\\chat_history";
		//String FileName = String.format("\\%s_chat_history.txt", map.get("USER_ID"));			
		String Path = String.format("%s/chat_history", request.getSession().getServletContext().getInitParameter("enginePath"));
		String FileName = String.format("/%s_chat_history.txt", map.get("USER_ID"));
				
		try {			
			File f = new File(Path);
			if(!f.exists()) {
				f.mkdir();
			}		
			
			Boolean bChk = true;
			f = new File(Path + FileName);
			if (!f.exists()) {
				f.createNewFile();
				bChk = false;
			}
			
			PrintWriter pww= new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
			
			if (map.get("TYPE").toString().equals("U") && bChk) {
				pww.write("\n");
			}
			
			pww.write(String.format("%s: %s\n", map.get("TYPE"), map.get("SAY")));			
			pww.flush();
			pww.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public String Notice(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub				
		String Path = "";
		log.debug(map);
		if (map.get("Type").toString().equals("N")) {
			Path = String.format("%s/notice.txt", request.getSession().getServletContext().getInitParameter("enginePath"));
		}
		else {			
			String Filename = String.format("IS-%s-@-%s", map.get("userseq"), map.get("prjname").toString());						
			String tempPath = "";
			if (bServer) {		
				String apiKey = GetCookie("API", request);
				if (apiKey.equals("null"))				
					tempPath = String.format("%s/KB/%s/dialog_domain/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("langseq").toString().equals("1")) ? "KOR" : "ENG");
				else
					tempPath = String.format("%s/KB/%s/dialog_domain/%s/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("langseq").toString().equals("1")) ? "KOR" : "ENG", apiKey.substring(0, 3), apiKey);
			}
			else			
				tempPath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\";									
			
			Path = String.format("%s%s/Logs/%s/%s", tempPath, Filename, map.get("parentname"), map.get("filename"));
		}
									
		StringBuilder sb = new StringBuilder();
								
		File f = new File(Path);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        String line = null;
		        while ((line = br.readLine()) != null) {
		        	sb.append(String.format("%s<br/>", line.replaceAll("<", "&lt;").replaceAll(">", "&gt;")));
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
		}		
		return sb.toString();
	}

	@Override
	public String getDomainLogs(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub					
		StringBuilder sb = new StringBuilder();
								
		String Filename = "";
		String tempPath = "";
		if (bServer) {			
			if (map.get("API_KEY").toString().equals("null")) {
				tempPath = String.format("%s/KB/%s/dialog_domain/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG");
				Filename = String.format("IS-%s-@-%s", map.get("USER_SEQ"), map.get("PROJECT_NAME").toString());
			}
			else {				
				tempPath = String.format("%s/KB/%s/dialog_domain/%s/%s/", request.getSession().getServletContext().getInitParameter("enginePath"), (map.get("LANG_SEQ_NO").toString().equals("1")) ? "KOR" : "ENG", map.get("API_KEY").toString().substring(0, 3), map.get("API_KEY").toString());
				Filename = map.get("PROJECT_NAME").toString();
			}
		}
		else			
			tempPath = "E:\\dev\\file\\webtool\\www_data\\KB\\KOR\\dialog_domain\\";
						
		String Path = String.format("%s%s/Logs", tempPath, Filename);
		log.debug(Path);
		File f = new File(Path);
		
		sb.append("<ul>");
		DirectoryList(f, sb, map, "");
		sb.append("</ul>");
		
		if (sb.toString().equals("<ul></ul>")) {
			sb.setLength(0);
		} 
		
		return sb.toString();
	}
	
	@Override
	public String LoadChatbotTalk(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		//String Path = "E:\\dev\\file\\webtool\\www_data\\chat_history";
		//String FileName = String.format("\\%s_chat_history.txt", map.get("USER_ID"));			
		String Path = String.format("%s/chat_history", request.getSession().getServletContext().getInitParameter("enginePath"));
		String FileName = String.format("/%s_chat_history.txt", map.get("USER_ID"));
				
		StringBuilder sb = new StringBuilder();
								
		File f = new File(Path + FileName);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        String line = null;
		        while ((line = br.readLine()) != null) {
		        	sb.append(String.format("%s<br/>", line));
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
		}		
		return sb.toString();
	}
	
	@Override
	public void insertChatbotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotObjMap;
        CommandMap SlotObjDetailMap;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("OBJECT_ITEM");
			StringBuilder sb = new StringBuilder();
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotObjMap = new CommandMap();
                SlotObjMap.put("OBJECT_NAME", SlotObj.get("OBJECT_NAME"));
                SlotObjMap.put("USER_SEQ", map.get("USER_SEQ"));
                
                if (SlotObj.get("SEQ_NO").toString().equals("0")) {
                	viewDAO.insertChatbotObject(SlotObjMap.getMap());                    
				}
                else {                	
        			SlotObjMap.put("SEQ_NO", SlotObj.get("SEQ_NO"));
                    
                	viewDAO.updateChatbotObject(SlotObjMap.getMap());
                	viewDAO.deleteChatbotObjectDetail(SlotObjMap.getMap());                	
                } 
                sb.append(String.format("%s \t ", SlotObj.get("OBJECT_NAME").toString()));
                                
                JSONArray objDetailArray = (JSONArray)SlotObj.get("OBJECT_DETAIL");
                for(int j=0 ; j< objDetailArray.size() ; j++) {
                	JSONObject SlotObjDetail = (JSONObject)objDetailArray.get(j);
                	SlotObjDetailMap = new CommandMap();
                	SlotObjDetailMap.put("OBJECT_NAME", SlotObjDetail.get("OBJECT_NAME"));
                	SlotObjDetailMap.put("PARENT_SEQ_NO", SlotObjMap.get("SEQ_NO"));
                	SlotObjDetailMap.put("USER_SEQ", SlotObjMap.get("USER_SEQ"));
                	
                	viewDAO.insertChatbotObjectDetail(SlotObjDetailMap.getMap());
                	if (j == 0)                	
                		sb.append(SlotObjDetail.get("OBJECT_NAME").toString());
                	else
                		sb.append("|" + SlotObjDetail.get("OBJECT_NAME").toString());
                }
                sb.append("\n");
            }			
			 
			//String Path = "E:\\dev\\file\\webtool\\www_data\\chat_history";
			//String FileName = String.format("\\%s_chat_slot.txt", map.get("USER_ID"));			
			String Path = String.format("%s/chat_history", request.getSession().getServletContext().getInitParameter("enginePath"));
			String FileName = String.format("/%s_chat_slot.txt", map.get("USER_ID"));
			
			FileWriterWithEncoding writer;
			try {			
				File t = new File(Path);
				if(!t.exists()) {				
					t.mkdir();
				}				
				
				writer = new FileWriterWithEncoding(Path + FileName, "EUC-KR");
				writer.write(sb.toString());
				writer.close();				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.debug(e.getMessage());
				e.printStackTrace();
			}											
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace();
		}                
	}	
	
	@Override
	public void deleteChatbotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteChatbotObject(map);
	}
	
	@Override
	public void deleteChatbotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteChatbotObjectDetail(map);
	}
	
	@Override
	public void insertChatbotLean(Map<String, Object> map) throws InterruptedException {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotObjMap;
        CommandMap SlotObjDetailMap;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("TRAIN_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("UP_ITEM");			
			
			viewDAO.insertChatbotTrain(map);						
			
			SlotObjMap = new CommandMap();
			SlotObjMap.put("USER_SEQ", map.get("USER_SEQ"));
			SlotObjMap.put("PARENT_SEQ_NO", map.get("SEQ_NO"));
			SlotObjMap.put("TRAIN_TYPE", "U");
			SlotObjMap.put("TRAIN_SAY", map.get("U_SAY"));
			
			viewDAO.insertChatbotTrainDetail(SlotObjMap.getMap());
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject ChatTrainObj = (JSONObject)objArray.get(i);
                SlotObjMap.put("TRAIN_TYPE", "UP");
    			SlotObjMap.put("TRAIN_SAY", ChatTrainObj.get("SAY"));
                viewDAO.insertChatbotTrainDetail(SlotObjMap.getMap());
            }
			
			SlotObjMap.put("TRAIN_TYPE", "S");
			SlotObjMap.put("TRAIN_SAY", map.get("S_SAY"));
			
			viewDAO.insertChatbotTrainDetail(SlotObjMap.getMap());
			
			objArray = (JSONArray)jsonObj.get("SP_ITEM");

			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject ChatTrainObj = (JSONObject)objArray.get(i);
                SlotObjMap.put("TRAIN_TYPE", "SP");
    			SlotObjMap.put("TRAIN_SAY", ChatTrainObj.get("SAY"));
                viewDAO.insertChatbotTrainDetail(SlotObjMap.getMap());
            }
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace();
		}                
	}
	
	@Override
	public void deleteChatbotLean(Map<String, Object> map) {
		// TODO Auto-generated method stub
		if (map.get("DEL_TYPE").toString().equals("S")) {
			viewDAO.deleteChatbotTrainDetail(map);
		}
		else if (map.get("DEL_TYPE").toString().equals("U")) {
			viewDAO.deleteChatbotTrainDetail(map);
			viewDAO.updateChatbotTrainDetail(map);
		}
		else {
			viewDAO.deleteChatbotTrain(map);
			viewDAO.deleteChatbotTrainDetailAll(map);
		}
	}
	
	@Override
	public void saveChatbotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException {
		// TODO Auto-generated method stub
		//String Path = "E:\\dev\\file\\webtool\\www_data\\chat_history";
		//String FileName = String.format("\\%s_chat_train.txt", map.get("USER_ID"));			
		String Path = String.format("%s/chat_history", request.getSession().getServletContext().getInitParameter("enginePath"));
		String FileName = String.format("/%s_chat_train.txt", map.get("USER_ID"));
		
		FileWriterWithEncoding writer;
		try {			
			File t = new File(Path);
			if(!t.exists()) {				
				t.mkdir();
			}				
			
			writer = new FileWriterWithEncoding(Path + FileName, "EUC-KR");
			writer.write(map.get("DATA").toString());
			writer.close();				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public String GetPort(List<String> arrUsingPort) throws Exception {
		Random random = new Random();
		Ports.SetPort();
		String tempPort = Ports.Port.get(random.nextInt(2999));
		Boolean bChk = false;
		
		if (arrUsingPort.contains(tempPort)) {
			return GetPort(arrUsingPort);
		}		
		else {
			return tempPort;
		}
	}
	
	@Override
	public String GetSlotMapping(Map<String, Object> map, HttpServletRequest request) throws Exception {
		Map<String,Object> encodingdata = selectEncoding(map);
		if (encodingdata.get("ENCODING_NAME").toString().equals("UTF-8"))
			map.put("ENCODING", "UTF8");	
		else
			map.put("ENCODING", "CP949");
		
		Runtime rt = Runtime.getRuntime();
		String Path = GetPath("D", false, request) + GetDomainName("D", false, map, request);		
		String logPath = String.format("%s/log/%s.auto_tagging.txt", Path, map.get("UNIQID").toString());				
		String cmd = String.format("%s/dial -cmd AUTO_TAGGING -dic %s/KB/%s -domain %s -utter %s -log_file %s -FILE_ENC %s -access_key %s", request.getSession().getServletContext().getInitParameter("enginePath"), request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("DL", request), GetDomainName("D", false, map, request), map.get("UTTR").toString(), logPath, map.get("ENCODING").toString(), GetCookie("SELECT_API", request));
		log.debug(cmd);
		Process proc = rt.exec(cmd); 
        proc.waitFor();				        	
		
		StringBuilder sb = new StringBuilder();
								
		File f = new File(logPath);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        String line = null;
		        while ((line = br.readLine()) != null) {
		        	sb.append(String.format("%s|", line));		        	
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();
		    }
		}
		f.delete();
		
		return sb.toString();
	}
	
	@Override
	public void SetLeanLog(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		Map<String,Object> encodingdata = selectEncoding(map);
		if (encodingdata.get("ENCODING_NAME").toString().equals("UTF-8"))
			map.put("ENCODING", "UTF8");	
		else
			map.put("ENCODING", "CP949");
							
		Runtime rt = Runtime.getRuntime();
		String txtPath = GetPath("D", false, request) + GetDomainName("D", false, map, request);		
		String logPath = String.format("%s/log/%s.learn_slu.log", txtPath, map.get("UNIQID").toString());		
		txtPath += String.format("/%s.txt", GetDomainName("D", false, map, request));

		String cmd;

		Process proc = rt.exec(String.format("chmod -R 777 %s/%s",
				GetPath("D", false, request),
				GetDomainName("D", false, map, request)));
		proc.waitFor();

		// 영어 모델의 경우
		if (map.get("LANG_SEQ_NO") != null && map.get("LANG_SEQ_NO").toString().equals("2")) {
			cmd = String.format("%s/dial.english -cmd LEARN_SLU -dic %s/KB/%s -domain %s -fn %s > %s 2>&1",
					request.getSession().getServletContext().getInitParameter("enginePath"),
					request.getSession().getServletContext().getInitParameter("enginePath"),
					GetCookie("DL", request), GetDomainName("D", false, map, request),
					txtPath, logPath);

		} else {
			cmd = String.format("%s/dial -cmd LEARN_SLU -dic %s/KB/%s -domain %s -fn %s -log_file %s"
							+ " -FILE_ENC %s -access_key %s",
					request.getSession().getServletContext().getInitParameter("enginePath"),
					request.getSession().getServletContext().getInitParameter("enginePath"),
					GetCookie("DL", request), GetDomainName("D", false, map, request),
					txtPath, logPath, map.get("ENCODING").toString(),
					GetCookie("SELECT_API", request));
		}

		log.debug(cmd);

		String [] totalCmd = {"/bin/sh", "-c", cmd};
		proc = rt.exec(totalCmd);

        viewDAO.updateProjectLearnCheck(map);
        //proc.waitFor();
        
        /*InputStream is = proc.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader br = new BufferedReader(isr);

        String line;
        StringBuilder sb = new StringBuilder();
        
        while((line=br.readLine())!= null){
        	log.debug(line);
        	System.out.println(line);
        	sb.append(line);
        }*/
        
		//return sb.toString();
	}
	
	@Override
	public String GetLeanLog(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String Path = String.format("%s%s/log/%s.learn_slu.log", GetPath("D", false, request), GetDomainName("D", false, map, request), map.get("UNIQID").toString());		
		
		StringBuilder sb = new StringBuilder();
								
		File f = new File(Path);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        String line = null;
		        while ((line = br.readLine()) != null) {
		        	line = line.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		        	sb.append(line + "<br/>");
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
		}		
		return sb.toString();
	}
	
	@Override
	public void DialogStart(Map<String, Object> map, HttpServletRequest request) throws Exception {

		Boolean englishDialog = (map.get("LANG_SEQ_NO").toString().equals("2"));

		if (!englishDialog) {
			EngineStop(map, request);
		}
		
		Map<String,Object> encodingdata = selectEncoding(map);
		if (encodingdata.get("ENCODING_NAME").toString().equals("UTF-8"))
			map.put("ENCODING", "UTF8");	
		else
			map.put("ENCODING", "CP949");
		
		String txtPath = String.format("%s%s/log/dialog_%s.txt",
				GetPath("D", false, request),
				GetDomainName("D", false, map, request),
				map.get("PORT").toString());
		
		Runtime rt = Runtime.getRuntime();		
		String cmd;

		// 영어 모델의 경우
		if (englishDialog) {
			cmd = String.format("%s/dial.english -dic %s/KB/%s -domain %s 2> %s",
					request.getSession().getServletContext().getInitParameter("enginePath"),
					request.getSession().getServletContext().getInitParameter("enginePath"),
					GetCookie("DL", request),
					GetDomainName("D", false, map, request),
					txtPath);

		} else {
			cmd = String.format("%s/dial -cmd DIALOG -port %s -dic %s/KB/%s -domain %s "
					+ "-FILE_ENC %s -log_file %s -access_key %s",
					request.getSession().getServletContext().getInitParameter("enginePath"),
					map.get("PORT").toString(),
					request.getSession().getServletContext().getInitParameter("enginePath"),
					GetCookie("DL", request),
					GetDomainName("D", false, map, request),
					map.get("ENCODING").toString(), txtPath, GetCookie("SELECT_API", request));
		}

		log.debug(cmd);

		String [] totalCmd = {"/bin/sh", "-c", cmd};
		Process proc = rt.exec(totalCmd);

		if (englishDialog) {
			Thread dialogThread = new Thread(DialogRunningThread.getInstance(proc));
			dialogThread.setDaemon(true);
			dialogThread.start();
		}

	}
	
	@Override
	public void EngineStop(Map<String, Object> map, HttpServletRequest request) throws IOException, InterruptedException {

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			// 영어 모델의 엔진 run/stop은 DialogRunningThread 내에서 제어
			DialogRunningThread.stopDialProc();
			return;
		}
		String cmd = String.format("kill -9 $(lsof -t -i:%s)", map.get("PORT").toString());
		log.debug(cmd);
		FileWriterWithEncoding writer = new FileWriterWithEncoding(String.format("%senginestop%s.sh", GetPath("D", false, request), map.get("PORT").toString()), "UTF-8");		
		writer.write(cmd.toString());
		writer.close();
		
		Runtime rt = Runtime.getRuntime();		
		cmd = String.format("sh %senginestop%s.sh", GetPath("D", false, request), map.get("PORT").toString());
		log.debug(cmd);
		Process proc = rt.exec(cmd);
		proc.waitFor();
		
		File file = new File(String.format("%senginestop%s.sh", GetPath("D", false, request), map.get("PORT").toString()));
		file.delete();
		
		String txtPath = String.format("%s%s/log/dialog_%s.txt", GetPath("D", false, request), GetDomainName("D", false, map, request), map.get("PORT").toString());
		file = new File(txtPath);
		if (file.exists()) {
			file.delete();	
		}
	}

	@Override
	public String DialogInputEnglish(Map<String, Object> map, HttpServletRequest request) throws Exception {

		String userMsg = "";
		String sysUtter = "";

		if (!map.get("TYPE").toString().equals("S")) {
			userMsg = map.get("MSG").toString().replaceAll("\\|@", " ") + "\n";
		}
		sysUtter = DialogRunningThread.getInstance().sendMsgToDialProc(userMsg);

		// 최신 버전과 동일하게 답변 SET (JSON) 구성
		if (!map.get("TYPE").toString().equals("S")) {
			sysUtter = sysUtter.split("\tSLU1:")[0];
		}
		sysUtter = sysUtter.split("system:")[1];
		sysUtter = sysUtter.replaceAll("\n", "").replaceAll("<BR>", "");

		StringBuilder sb = new StringBuilder();
		String dialog_end = "FALSE";
		sb.append("{\"SYS_UTTER\":\"").append(sysUtter).append("\",")
				.append("\"DIALOG_END\":\"").append(dialog_end).append("\"}");
//				.append("\"DEV_LOG\":\"").append(devLog).append("\"}");

		map.put("STATUS", "SUCCESS");

		return sb.toString();
	}

	@Override
	public String DialogInput(Map<String, Object> map, HttpServletRequest request) throws Exception {

		String cmd;
		if (map.get("TYPE").toString().equals("S"))			
			cmd = String.format("curl --data {\"INIT_SLOT_VALUES\":[]} http://localhost:%s/dialog_start", map.get("PORT").toString());	
		else
			cmd = String.format("curl --data {\"USER_UTTER\":\"%s\"} http://localhost:%s/dialog_ing", map.get("MSG").toString(), map.get("PORT").toString());
		log.debug(cmd);
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);	
        proc.waitFor();
		
        String response = "";
        InputStream is = proc.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader brr = new BufferedReader(isr);

        String line;        
        
        while((line=brr.readLine())!= null) {             	
        	response += line;
        }
        
        map.put("STATUS", response.trim());
        
        String txtPath = String.format("%s%s/log/dialog_%s.txt",
						GetPath("D", false, request),
						GetDomainName("D", false, map, request),
						map.get("PORT").toString());
        
        StringBuilder sb = new StringBuilder();
        File f = new File(txtPath);
				if (f.exists()) {
					try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        line = null;
		        while ((line = br.readLine()) != null) {
		        	//sb.append(String.format("%s<br/>", line));
		        	sb.append(line);
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
		}
		
		return sb.toString();		
	}
	
	@Override
	public String DialogStop(Map<String, Object> map, HttpServletRequest request) throws Exception {

		if (map.get("LANG_SEQ_NO").toString().equals("2")) {
			return "OK";
		}

		String cmd = String.format("curl http://localhost:%s/dialog_stop", map.get("PORT").toString());
		log.debug(cmd); 
		
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);		
        proc.waitFor();				
								
		InputStream is = proc.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader br = new BufferedReader(isr);

        String line;
        StringBuilder sb = new StringBuilder();
        
        while((line=br.readLine())!= null){
        	sb.append(line);
        }
        
		return sb.toString();
	}

	@Override
	public void getDevLogEnglish(Map<String, Object> map, HttpServletRequest request)
			throws Exception {

		String txtPath = String.format("%s%s/log/dialog_%s.txt",
				GetPath("D", false, request),
				GetDomainName("D", false, map, request),
				map.get("PORT").toString());

		//DEV_LOG 구성
		String line;
		StringBuilder logSb = new StringBuilder();
		File f = new File(txtPath);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
				line = null;
				while ((line = br.readLine()) != null) {
					logSb.append(String.format("%s \n", line));
				}
			} catch (IOException e) {
				log.debug(e.getMessage());
				e.printStackTrace();
			}
		}

		String devLog = logSb.toString().replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");

		map.put("STR", devLog);

	}

	@Override
	public List<Map<String, Object>> selectDomainVersion(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectDomainVersion(map);
	}
		
	@Override
	public List<Map<String, Object>> selectLanguage(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLanguage(map);
	}
	
	@Override	
	public void insertUserSayFile(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String strFilePath = "";		
		try {
			strFilePath = fileUtils.SaveFile("", request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		
		//map.put("SLOT_LIST", String.format("[%s]", map.get("SLOT_LIST")));
		//map.put("INTENTS_LIST", String.format("[%s]", map.get("INTENTS_LIST")));
		
		CommandMap cmdmap = new CommandMap();		
		List<Map<String,Object>> list = null;		
		String[] arr;		
		String[] arritem;
		String[] arritemsub;
		String[] arrtempnm;
		String[] arrtempnmsub;		
		
		List<String> arrslot = new ArrayList<String>();
		String intentnm;
		String tempnm;
		int itempcnt;
		Pattern p = Pattern.compile("<[a-zA-Z가-힣0-9_\\-]+.[.a-zA-Z가-힣0-9_\\-]+=[\"`',%\\+\\/\\\\?~!@#$%^&*·;:=|\\(\\)\\[\\]{}a-zA-Z가-힣0-9.\\s_\\-]+>");
		Matcher m;
		JSONParser jsonParser = new JSONParser();
		JSONObject slotObj = null;
		
		if (!map.get("SLOT_LIST").toString().isEmpty()) {
			slotObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));
		}
		
		//JSONObject intentObj = (JSONObject)jsonParser.parse((String)map.get("INTENTS_LIST"));        
		JSONArray slotjsonArray = null;        
        JSONArray intentjsonArray;
        String saytag = ""; 
        String slottag = "";
        String newslotseq = "";
        String intention = "";
        StringBuilder slotname = new StringBuilder();
        StringBuilder slotmapda = new StringBuilder();        
        List<String> arrslotseq = new ArrayList<String>();        
        List<UserSaySortObj> arrslotseqsort = new ArrayList<UserSaySortObj>();
        
        if (map.get("TYPE").toString().contains("L")) {
        	String strWhere = "";
        	if (map.get("AGENT_SEQ") != null && !map.get("AGENT_SEQ").toString().equals("0") && !map.get("AGENT_SEQ").toString().isEmpty()) {
        		//strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";    		
        		strWhere = "AND AGENT_SEQ = " + map.get("AGENT_SEQ").toString();
    		}
        	
        	map.put("WHERE_SQL", strWhere);
        	list = selectIntents(map);
		}
        
        Map<String, Object> cntitem = selectCountMng(map);			
        map.put("TABLE", "intents_user_says");
        map.put("WHERE", "INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = " + map.get("PROJECT_SEQ").toString() + ")");        
		Map<String, Object> cntchk = selectCountCheck(map);

        int iTotCnt = Integer.parseInt(cntchk.get("CNT").toString());
        int iMaxCnt = Integer.parseInt(cntitem.get("USERSAY_COUNT").toString());
        int iUploadCnt = 0;
		File f = new File(strFilePath);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(f), "utf-8"))) {
				String line = null;
				
				List<Map<String, Object>> scolorlist = viewDAO.selectSlotColorCheck2(map);
        		Map<String, Map<String, String>> slotColorMap = CommonUtils.setSlotColorToMap(scolorlist);
        		List<Map<String, Object>> slotSublist = viewDAO.selectSlotSub(map);
        		Map<String, String> slotSubMap = new HashMap<String, String>();
        		
        		for (Iterator iterator = slotSublist.iterator(); iterator.hasNext();) {
					Map<String, Object> item = (Map<String, Object>) iterator.next();
					slotSubMap.put(item.get("SLOT_SUB_NAME").toString(), item.get("SLOT_SUB_SEQ").toString());
				}
        		
        		List<Map<String, Object>> intentionlist = viewDAO.selectIntention(map);
        		
        		Map<String, String> intentionMap = new HashMap<String, String>();
        		for (Iterator iterator = intentionlist.iterator(); iterator.hasNext();) {
        			Map<String, Object> item = (Map<String, Object>) iterator.next();
        			intentionMap.put(String.format("%s=\"%s\"", item.get("INTENTION_SLOT"), item.get("INTENTION_SLOT_VALUE")), item.get("INTENTION_NAME").toString());
        		}
        		
		        while ((line = br.readLine()) != null) {
		        	/*if (iUploadCnt > 100) {
		        		map.put("STATUS", "UC");
						throw new Exception();
					}*/
		        	iUploadCnt++;
		        	iTotCnt++;
		        	if (iTotCnt > iMaxCnt) {
		        		map.put("STATUS", "TC");
						throw new Exception();
					}		        	
		        	
		        	arr = line.split("\t");
		        	arrslot.clear();
		        	arrslotseq.clear();
		        	arrslotseqsort.clear();		        	
		        	slotname.setLength(0);
		        	slotmapda.setLength(0);
		        	newslotseq = "";
		        	intention = "";		        	
		        	cmdmap.clear();
		        	
		        	if (arr.length == 3) {
		        		intentnm = arr[2].substring(0, arr[2].indexOf('('));
		        		tempnm = arr[2].substring(intentnm.length() + 1, arr[2].indexOf(')'));
		        		if (!tempnm.isEmpty()) {
		        			arrtempnm = tempnm.replaceAll(" ", "").split(",");
		        			tempnm = "";
		        			for (int i = 0; i < arrtempnm.length; i++) {		        				
		        				arrtempnmsub = arrtempnm[i].trim().split("=");
		        				if (arrtempnmsub.length == 1) {
		        					if (tempnm.isEmpty())
				        				tempnm += arrtempnm[i];	
				        			else
				        				tempnm += ", " + arrtempnm[i].trim();	
								}
		        				else {
		        					if (arrtempnmsub[1].equals("=value")) {
		        						if (tempnm.isEmpty())
					        				tempnm += arrtempnm[i];	
					        			else
					        				tempnm += ", " + arrtempnm[i].trim();	
									}
		        					else {
		        						if (intentionMap.containsKey(arrtempnm[i])) {
		        							if (intention.isEmpty())
	        									intention = intentionMap.get(arrtempnm[i]);	
		        							else
		        								intention = String.format("%s %s", intention, intentionMap.get(arrtempnm[i]));
		        							
										}
		        					}
		        				}
							}
						}		        		
        					        		
		        		if (!tempnm.isEmpty()) {		        		
		        			intentnm += String.format("(%s)", tempnm);		        						        		
						}
		        		
		        		saytag = arr[1];
			        	slottag = arr[1];
		        		m = p.matcher(arr[1]);
		        		
		        		while (m.find()) {
		        			arrslot.add(m.group());
		        		}
		        		
		        		String sitem;
		        		String slotseq = null;
		        		String slotcolor = "";
		        		Boolean bcolorChk = false;
				        	
		        		//Map<String, SlotObject> slotMap = (Map<String, SlotObject>)map.get("SLOT_MAP");
		        		Map<String, String> tempcolormap = null;		        		
		        		SlotObject sobj = null;

		        		for (Iterator slotitem = arrslot.iterator(); slotitem.hasNext();) {
							sitem = (String)slotitem.next();
							sobj = null;
							bcolorChk = false;
							arritem = sitem.substring(1, sitem.length() - 1).split("=");							
							arritemsub = arritem[0].split("\\.");
							slotseq = "";
							slotcolor = "";
							
							if (arritemsub.length == 2) {
								slotjsonArray = (JSONArray)slotObj.get(arritemsub[0]);
								
								int iCnt = 0;
								for(int i=0 ; i< slotjsonArray.size() ; i++){
					                JSONObject objitem = (JSONObject)slotjsonArray.get(i);
					                
					                if (objitem.get("SLOT_NAME").equals(arritemsub[1])) {
					                	slotseq = objitem.get("SEQ_NO").toString();
					                	slotcolor = objitem.get("COLOR").toString();					                	
										iCnt++;										
									}	                
					            }
								
								if (iCnt == 0) {
									map.put("STATUS", "NS");
									throw new Exception();
								}
							}
							else {
								if (slotSubMap.containsKey(arritem[0])) {
									slotseq = slotSubMap.get(arritem[0]);
									slotcolor = slotseq.substring(0, slotseq.indexOf("_")); // class_seq
																	
									if (slotColorMap.containsKey(slotcolor)) {											
										tempcolormap = slotColorMap.get(slotcolor);
										slotcolor = arritem[0].substring(arritem[0].indexOf(".") + 1);																			
										if (tempcolormap.containsKey(slotcolor)) {												
											slotcolor = tempcolormap.get(slotcolor);												
										}
										else
											bcolorChk = true;											
									}
									else
										bcolorChk = true;
									
									if (bcolorChk) {
										map.put("CLASS_SEQ_NO", slotseq.substring(0, slotseq.indexOf("_")));
										map.put("SLOT_NAME", arritem[0].substring(arritem[0].indexOf(".") + 1));
										slotcolor = selectSlotColor(map);
									}																									
								}
								else {
									map.put("STATUS", "NS");
									throw new Exception();
								}
							}
							
							if (!slotseq.isEmpty() && !slotcolor.isEmpty()) {								
								saytag = saytag.replace(sitem, "<span style=\"color:" + slotcolor + "\">" + arritem[1] + "</span>");
								slottag = slottag.replace(sitem, "<span style=\"color:" + slotcolor + "\">@" + arritem[0] + "</span>");
								
								arrslotseq.add(slotseq);														
								
								if (slotname.length() != 0) {
									slotname.append(String.format(",%s=value", arritem[0]));
									slotmapda.append(String.format(",%s=\"%s\"", arritem[0], arritem[1]));
								}
								else {
									slotname.append(String.format("%s=value", arritem[0]));
									slotmapda.append(String.format("%s=\"%s\"", arritem[0], arritem[1]));
								}
							}					        
						}
		        		
		        		for (int i = 0; i < arrslotseq.size(); i++) {
							if (arrslotseq.get(i).indexOf("_") > -1) {
								arrslotseqsort.add(new UserSaySortObj(Integer.parseInt(arrslotseq.get(i).substring(arrslotseq.get(i).lastIndexOf("_") + 1)), arrslotseq.get(i)));					
							}
							else {
								arrslotseqsort.add(new UserSaySortObj(Integer.parseInt(arrslotseq.get(i)), arrslotseq.get(i)));
							}				
						}
						
						Collections.sort(arrslotseqsort, new Comparator<UserSaySortObj>() {
				        	@Override
				    		public int compare(UserSaySortObj o1, UserSaySortObj o2) {
				    		    return o1.getSeqNo().compareTo(o2.getSeqNo());
				    		}
				        });
						
						for (int i = 0; i < arrslotseqsort.size(); i++) {
							if (i == 0) 
								newslotseq = arrslotseqsort.get(i).getSlotSeq();
							else
								newslotseq = String.format("%s,%s", newslotseq, arrslotseqsort.get(i).getSlotSeq());
						}
						
		        		cmdmap.put("SAY", arr[0]);
		        		cmdmap.put("SAY_TAG", saytag);
		        		cmdmap.put("SLOT_TAG", slottag);		        		
		        		cmdmap.put("SLOT_SEQ_NO", newslotseq);
						cmdmap.put("SLOT_NAME", slotname.toString());
						cmdmap.put("SLOT_MAP", arr[1]);
						cmdmap.put("SLOT_MAPDA", slotmapda.toString());						
						cmdmap.put("INTENTION", intention);
						
		        		if (map.get("TYPE").toString().contains("W")) {
		        			if (map.get("INTENT_NAME").toString().equals(intentnm)) {
		        				cmdmap.put("INTENT_SEQ", map.get("SEQ_NO"));
			        			viewDAO.insertIntentUserSay(cmdmap.getMap());
							}			        			
						}
		        		else {
		        			Boolean insertchk = false;
		        			for (Map<String, Object> intentmap : list) {
		        				if (intentmap.get("INTENT_NAME").toString().equals(intentnm)) {
		        					cmdmap.put("INTENT_SEQ", intentmap.get("SEQ_NO"));		        					
								}
		        			}
		        			
		        			if (!cmdmap.containsKey("INTENT_SEQ")) {
		        				map.put("INTENT_NAME", intentnm);
	        					viewDAO.insertIntent(map);
	        					cmdmap.put("INTENT_SEQ", map.get("SEQ_NO"));
	        					Map<String,Object> tempmap = new HashMap<String, Object>();
	        					tempmap.put("SEQ_NO", map.get("SEQ_NO"));
	        					tempmap.put("INTENT_NAME", map.get("INTENT_NAME"));
	        					tempmap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
	        					list.add(tempmap);
	        					map.remove("SEQ_NO");
	        					map.remove("INTENT_NAME");	        						        					
							}
		        			viewDAO.insertIntentUserSay(cmdmap.getMap());
		        		}	
					}
		        	else {
		        		if (!line.trim().isEmpty()) {		        			
		        			throw new Exception();	
						}
		        	}
		        }
		    } catch (IOException e) {
		    	map.put("STATUS", "F");
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
			f.delete();
		}
	}
	
	@Override	
	public void insertTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String strFilePath = "";
		
		try {			
			String Path = String.format("%s%s/", GetPath("T", false, request), GetDomainName("T", false, map, request));
			//String Path = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s\\", map.get("LANG").toString(), map.get("PROJECT_NAME").toString());
			log.debug(Path);
			strFilePath = fileUtils.SaveFile(Path, request);
			
			viewDAO.insertTutorDialogMap(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}
	
	@Override
	public void deleteTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String Path = String.format("%s%s/%s", GetPath("T", false, request), GetDomainName("T", false, map, request), map.get("FILE_NAME").toString());
		//String Path = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString(), map.get("FILE_NAME").toString());
		log.debug(Path);
		File file = new File(Path);
		file.delete();
		viewDAO.deleteTutorDialogMap(map);
	}
	
	@Override
	public void updateTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		viewDAO.updateTutorDialogMap(map);
		
		String fullname = "";
		
		if (GetCookie("TL", request).equals("KOR"))
			fullname = "korean";
		else
			fullname = "english";
		
		String cmdtype = "compile";
		if (map.get("TYPE").toString().equals("C"))
			cmdtype = "convert";
		else
			cmdtype = "learn_slu";
		
		String FilePath = String.format("%s%s/%s", GetPath("T", false, request), GetDomainName("T", false, map, request), map.get("FILE_NAME").toString());								
		log.debug(FilePath);
		File fromFile = new File(FilePath);		
		FilePath = String.format("%s%s/ED-%s-@-%s", GetPath("T", false, request), GetDomainName("T", false, map, request), GetCookie("TU", request), map.get("FILE_NAME").toString());
		log.debug(FilePath);
		File toFile = new File(FilePath);
		copyFile(fromFile, toFile);
		
		String cmd = String.format("%s/dial_tutor.rest %s %s %s/KB/%s %s %s", request.getSession().getServletContext().getInitParameter("enginePath"), cmdtype, fullname, request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("TL", request), GetDomainName("T", false, map, request), FilePath);			
		//String FilePath = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString(), map.get("FILE_NAME").toString());
		//String cmd = String.format("E:\\dev\\file\\webtool\\www_data\\dial_tutor.rest compile %s %s\\%s %s %s", fullname, "E:\\dev\\file\\webtool\\KB", map.get("LANG").toString(), map.get("PROJECT_NAME").toString(), FilePath);
				
		log.debug(cmd);
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);		
        proc.waitFor();				
								
		/*InputStream is = proc.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader br = new BufferedReader(isr);

        String line;
        StringBuilder sb = new StringBuilder();
        
        while((line=br.readLine())!= null){
        	sb.append(String.format("%s<br />", line));
        }
        log.debug(sb.toString());*/
		
		map.put("MSG", "OK");
	}
	
	@Override
	public String getTutorDialogMission(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub

		String Path = String.format("%s%s/ED-%s-@-%s.mission.table.txt", GetPath("T", false, request), GetDomainName("T", false, map, request), GetCookie("TU", request), map.get("PROJECT_NAME").toString());
		log.debug(Path);
		StringBuilder sb = new StringBuilder();
								
		File f = new File(Path);
		if (f.exists()) {
			map.put("FILE_EXISTS", "Y");
			FileInputStream fis = new FileInputStream(Path);
			InputStreamReader isr = new InputStreamReader(fis, "MS949");
			try(BufferedReader br = new BufferedReader(isr)) {
				String line = null;
		        while ((line = br.readLine()) != null) {
		        	if (line.startsWith("id")) {
		        		if (sb.length() == 0)
		        			sb.append(String.format("%s|", line));	
		        		else
		        			sb.append(String.format("§%s|", line));
					}
		        	if (line.startsWith("description")) {	
	        			sb.append(line);												
					}
		        }
		        br.close();
			} catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
			isr.close();
			fis.close();
		}		
		else 
			map.put("FILE_EXISTS", "N");
		
		return sb.toString();
	}
	
	@Override	
	public String openDialogFile(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub				
		String strFilePath = String.format("%s%s/%s", GetPath("T", false, request), GetDomainName("T", false, map, request), map.get("FILE_NAME").toString());		
		//String strFilePath = String.format("E:\\dev\\file\\webtool\\KB\\%s\\dialog_domain\\%s\\%s", map.get("LANG").toString(), map.get("PROJECT_NAME").toString(), map.get("FILE_NAME").toString());
		ExcelUtils exu = new ExcelUtils();		
		String result = exu.ExcelToHtml(strFilePath); 
		return result;
	}
	
	@Override
	public void DownloadIntent(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		String FileName = "";
		String where = "";
		
		if (map.get("AGENT_SEQ").toString().isEmpty() || map.get("AGENT_SEQ").toString().equals("0")) {
			FileName = String.format("[Intents][%s]", map.get("PROJECT_NAME"));
			
			if (map.containsKey("SEQ_NO")) {				
				FileName = String.format("[Intents][%s][All tasks][%s]", map.get("PROJECT_NAME"), map.get("INTENT_NAME"));
				where += " AND A.SEQ_NO = " + map.get("SEQ_NO");
				map.put("WHERE", where);
			}
		}
		else {
			Map<String, Object> agent = viewDAO.selectAgent(map);
			where = " AND A.AGENT_SEQ = " + agent.get("SEQ_NO");
			
			if (map.containsKey("SEQ_NO")) {
				where += " AND A.SEQ_NO = " + map.get("SEQ_NO");
				FileName = String.format("[Intents][%s][%s][%s]", map.get("PROJECT_NAME"), agent.get("AGENT_NAME"), map.get("INTENT_NAME"));
			}
			else {
				FileName = String.format("[Intents][%s][%s]", map.get("PROJECT_NAME"), agent.get("AGENT_NAME"));	
			}
			map.put("WHERE", where);
		}
		
		String Path = String.format("%s%s/%s", GetPath("D", false, request), GetDomainName("D", false, map, request), FileName); 
		
		map.put("Path", Path + ".txt");
		map.put("FileName", FileName + ".txt");
		
		Map<String,Object> encodingdata = selectEncoding(map);
		map.put("ENCODING", encodingdata.get("ENCODING_NAME"));
		
		LeanDomain(map, Path);
	}
	
	@Override
	public void TuTorRunStart(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		EngineStop(map, request);
		Runtime rt = Runtime.getRuntime();
		String cmd = "";
		String lang = "";
				
		if (GetCookie("TL", request).equals("KOR")) {
			lang = "korean";			
		}
		else {
			lang = "english";
		}
		cmd = String.format("%s/dial_tutor.rest dialog %s UTF8 %s/KB/%s %s %s %s", request.getSession().getServletContext().getInitParameter("enginePath"), lang, request.getSession().getServletContext().getInitParameter("enginePath"), GetCookie("TL", request), map.get("PROJECT_NAME").toString(), map.get("ID").toString(), map.get("PORT").toString());
		
		log.debug(cmd);		
		Process proc = rt.exec(cmd);				
	}
	
	@Override
	public String TutorRunInput(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// // TODO Auto-generated method stub
		String cmd;
		
		if (map.get("TYPE").toString().equals("S"))
			cmd = String.format("curl -d {\"INIT_SLOT_VALUES\":[]} http://localhost:%s/dialog_start", map.get("PORT").toString());
		else
			cmd = String.format("curl -d {\"USER_UTTER\":\"%s\"} http://localhost:%s/dialog_ing", map.get("MSG").toString(), map.get("PORT").toString());		
	 	
		log.debug(cmd);
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);		
        proc.waitFor();				
								
		InputStream is = proc.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader br = new BufferedReader(isr);

        String line;
        StringBuilder sb = new StringBuilder();
        
        while((line=br.readLine())!= null){        	      
        	sb.append(line);
        }
        
		return sb.toString();		
	}

	@Override
	public void TutorDialogStop(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		/*String cmd = String.format("kill -9 $(lsof -t -i:%s)", map.get("PORT").toString());
		log.debug(cmd); 
		
		String[] arrcmd = new String[]{"kill", "-9", String.format("$(lsof -t -i:%s)", map.get("PORT").toString())};
		
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(arrcmd);*/
		
		EngineStop(map, request);
	}

  @Override
  public Map<String, Object> getBizChatbotUser(Map<String, Object> map) throws Exception {
    return viewDAO.getBizChatbotUser(map);
  }

  class Ascending implements Comparator<Integer> {
		@Override
		public int compare(Integer o1, Integer o2) {		
			return o1.compareTo(o2);
		}
	}
}

class DialogRunningThread implements Runnable {

	private static DialogRunningThread instance = new DialogRunningThread();
	private static Logger log = Logger.getLogger(DialogRunningThread.class);
	// LOCK for process 동기화
	private static final Object proc_LOCK = new Object();
	private static Process proc;
	private InputStream out;
	private OutputStream in;

	private DialogRunningThread() {
	}

	public static DialogRunningThread getInstance() {
		return instance;
	}

	public static DialogRunningThread getInstance(Process process) {
		// 구동 시 전 proc kill 하고 다시 할당
		stopDialProc();
		synchronized (proc_LOCK) {
			proc = process;
		}
		return instance;
	}

	@Override
	public void run() {
		log.debug("DialogRunningThread runs.");

		out = proc.getInputStream();
		in = proc.getOutputStream();

		synchronized (proc_LOCK) {
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			try {
				out.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			log.debug("DialogRunningThread is over. " + proc.exitValue());
		}
	}

	public static synchronized long getPidOfProcess() {
		long pid = -1;

		try {
			if (proc.getClass().getName().equals("java.lang.UNIXProcess")) {
				Field f = proc.getClass().getDeclaredField("pid");
				f.setAccessible(true);
				pid = f.getLong(proc);
				f.setAccessible(false);
			}
		} catch (Exception e) {
			pid = -1;
		}
		return pid;
	}

	public static boolean isAlive(Process p) {
		try {
			p.exitValue();
			return false;
		}
		catch (IllegalThreadStateException e) {
			return true;
		}
	}

	public String sendMsgToDialProc (String msg) throws IOException {

		String answerSet = "";
		byte[] bufferOut = new byte[4000];
		byte[] bufferIn;
		int i = 0;

		log.debug("DIALOG process PID : " + getPidOfProcess());

		while (isAlive(proc)) {
			int no = 0;

			i ++;
			if (i > 10) {
				break;
			}
			try {

				no = out.available();

				if (no > 0) {
					int n = out.read(bufferOut, 0, Math.min(no, bufferOut.length));
					answerSet = new String(bufferOut, 0, n, "UTF-8");
					log.debug("System Answer : " + answerSet);
					break;
				}

				// process 에 msg 보내기
				if (!msg.equals("")) {
					log.debug("User Says : " + msg);
					bufferIn = msg.getBytes();
					in.write(bufferIn, 0, Math.min(msg.length()+1, bufferIn.length));
					in.flush();
					msg = "";
				}


			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
		}

		return answerSet;
	}

	public static synchronized void stopDialProc() {
		log.debug("Stop dial.english");

		if (proc == null || !isAlive(proc)) {
			log.debug("the process is not running.");
			return;
		}

		long pid = getPidOfProcess();
		if (pid == -1) {
			log.debug("PID is -1 . exit.");
			return;
		}

		String cmd = String.format("kill -9 %s", pid);
		log.debug(cmd);

		Runtime rt = Runtime.getRuntime();
		Process killProc = null;

		try {
			killProc = rt.exec(cmd);
			killProc.waitFor();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
