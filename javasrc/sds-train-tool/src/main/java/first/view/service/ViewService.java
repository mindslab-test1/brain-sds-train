package first.view.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import first.common.common.CommandMap;

public interface ViewService {
	void CreateTable(Map<String, Object> map);
	
	void DropTable(Map<String, Object> map);
	
	void CreateColumn(Map<String, Object> map);
	
	void AlterColumn(Map<String, Object> map);

	void DropColumn(Map<String, Object> map);	
	
	void insertProject(Map<String, Object> map, HttpServletRequest request);

	void insertTutorProject(Map<String, Object> map, HttpServletRequest request);
	
	void insertSlotClass(Map<String, Object> map);	
	
	void insertSlot(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String selectSlotColor(Map<String, Object> map);
	
	void insertSlotTypeClass(Map<String, Object> map);
	
	void deleteSlotTypeClass(Map<String, Object> map);
	
	void deleteSlotTypeClassAll(Map<String, Object> map);
	
	void deleteProject(Map<String, Object> map, HttpServletRequest request);
	
	void deleteTutorProject(Map<String, Object> map, HttpServletRequest request);
	
	void deleteSlotClass(Map<String, Object> map);
	
	void deleteSlotClassSeq(Map<String, Object> map);	
	
	void deleteSlot(Map<String, Object> map) throws Exception;
	
	void deleteReferSlotClass(Map<String, Object> map);
	
	void updateProject(Map<String, Object> map, HttpServletRequest request);
	
	void updateProjectDeleteFlag(Map<String, Object> map);
	
	void updateTutorProject(Map<String, Object> map, HttpServletRequest request);
	
	void updateTutorProjectDeleteFlag(Map<String, Object> map);
	
	void updateSlotClass(Map<String, Object> map) throws JsonGenerationException, JsonMappingException, IOException;
	
	void updateSlotClassNewCheck(Map<String, Object> map);
	
	void updateSlot(Map<String, Object> map, HttpServletRequest request) throws JsonGenerationException, JsonMappingException, IOException;
	
	void updateSlotSort(Map<String, Object> map);
	
	void insertSlotObjectDetail(Map<String, Object> map);

	void insertSlotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException;	

	void deleteSlotObject(Map<String, Object> map, HttpServletRequest request);

	void deleteSlotObjectDetailSeq(Map<String, Object> map, HttpServletRequest request);
	
	void deleteSlotObjectDetail(Map<String, Object> map);
	
	void updateSlotObject(Map<String, Object> map, HttpServletRequest request);
	
	void insertChatbotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException;
	
	void deleteChatbotObject(Map<String, Object> map);

	void deleteChatbotObjectDetail(Map<String, Object> map);
	
	void insertChatbotLean(Map<String, Object> map) throws InterruptedException;

	void deleteChatbotLean(Map<String, Object> map);
	
	void saveChatbotObject(Map<String, Object> map, HttpServletRequest request) throws InterruptedException;
	
	void insertSlotInstans(Map<String, Object> map);
	
	void updateSlotInstans(Map<String, Object> map);
	
	void deleteSlotInstans(Map<String, Object> map);
	
	void deleteSlotInstansAll(Map<String, Object> map);
	
	void insertSlotInstansExcel(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void insertSlotObjectExcel(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void insertSlotMapping(Map<String, Object> map);
	
	void updateSlotMapping(Map<String, Object> map);
	
	void deleteSlotMapping(Map<String, Object> map);
	
	void insertIntent(Map<String, Object> map);
	
	void deleteIntent(Map<String, Object> map);
	
	void deleteIntentUserSay(Map<String, Object> map);
	
	void deleteIntentUserSayDetail(Map<String, Object> map);
	
	void deleteIntentsUserSays(Map<String, Object> map);
	
	void deleteIntentsIntention(Map<String, Object> map);
	
	void deleteIntentsObject(Map<String, Object> map);
	
	void deleteIntentsObjectDetail(Map<String, Object> map);	
	
	void insertReplaceIntent(Map<String, Object> map);
	
	void updateReplaceIntent(Map<String, Object> map);
	
	void deleteReplaceIntent(Map<String, Object> map);
		
	void insertEditIntent(Map<String, Object> map);
	
	void insertAgent(Map<String, Object> map);	

	void insertOnlyAgent(Map<String, Object> map);
	
	void insertAgentMonitoring(Map<String, Object> map);
		
	void updateAgentPosition(Map<String, Object> map);

	void updateAgentPositionStraightly(Map<String, Object> map);

	void insertAgentGraph(Map<String, Object> map);
	
	void deleteAgentGraph(Map<String, Object> map);
	
	void deleteAgent(Map<String, Object> map);
	
	void updateAgentSortNo(Map<String, Object> map);
	
	void insertUser(Map<String, Object> map);

	void updateUser(Map<String, Object> map);
	
	void updateUserPassword(Map<String, Object> map);

	void updateUserLastLogin(Map<String, Object> map);

	void deleteUser(Map<String, Object> map);
	
	void insertLua(Map<String, Object> map);

	void updateLua(Map<String, Object> map);
	
	void deleteLua(Map<String, Object> map);
	
	void insertIntention(Map<String, Object> map);
	
	void deleteIntention(Map<String, Object> map);
	
	void updateIntentionGroup(Map<String, Object> map);
	
	void insertLanguage(Map<String, Object> map);

	void deleteLanguage(Map<String, Object> map);
	
	void insertUserDomain(Map<String, Object> map);	
	
	void deleteUserDomain(Map<String, Object> map);
	
	void updateEncoding(Map<String, Object> map);
	
	void updateCountMng(Map<String, Object> map);
	
	void insertTutorUserDomain(Map<String, Object> map);	
	
	void deleteTutorUserDomain(Map<String, Object> map);
	
	void insertReferClass(Map<String, Object> map);	
	
	void insertSampleDomain(Map<String, Object> map);
	
	void deleteSampleDomain(Map<String, Object> map);
	
	void copyIntents(Map<String, Object> map) throws Exception;
	
	void copyTask(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	List<String> copyDomain(Map<String, Object> map, List<Map<String, Object>> prjlist, HttpServletRequest request) throws Exception;
	
	void saveVersion(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void loadVersion(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void otherDomainCrate(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	List<Map<String, Object>> selectProject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectProjectDelete(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectCreateProject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectProjectName(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectSampleDomain(Map<String, Object> map) throws Exception;
	
	
	
	List<Map<String, Object>> selectTutorProject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectTutorProjectDelete(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectTutorCreateProject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectTutorProjectName(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectTutorDialogMap(Map<String, Object> map) throws Exception;
	
	
	
	List<Map<String, Object>> selectSlotClass(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotClassRefer(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotClassName(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlot(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlot2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotTypeCheck(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotTypeClassCheck(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectSlotAddition(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectFunction(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotType(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotObjectPaging(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectChatbotObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectChatbotObjectDetail(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectChatbotTrainDetail(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotMapping(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntents(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentIntention(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectReplaceIntent(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectFliingSlot(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgents(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentsCopy(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectAgent(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectLeanNextAgent(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentMornitoring(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentActions(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSystemDAtype(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserList(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserAllList(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectLuaList(Map<String, Object> map) throws Exception;	
	
	List<Map<String, Object>> selectRererenceClass(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectLua(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectUser(Map<String, Object> map) throws Exception;
	
	Map<String, Object> loginUser(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectIntent(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectEditIntent(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectEncoding(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectCountMng(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectCountCheck(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntention(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserDomain(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) throws Exception;
	
	Boolean DialogsLearning(Map<String, Object> map, HttpServletRequest request) throws Exception;		
	
	Boolean SaveChatbotTalk(Map<String, Object> map, HttpServletRequest request) throws Exception;

	String Notice(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String getDomainLogs(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String LoadChatbotTalk(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String GetPort(List<String> arrUsingPort) throws Exception;
	
	String GetSlotMapping(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void SetLeanLog(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String GetLeanLog(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void DialogStart(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String DialogInput(Map<String, Object> map, HttpServletRequest request) throws Exception;

	String DialogInputEnglish(Map<String, Object> map, HttpServletRequest request) throws Exception;

	String DialogStop(Map<String, Object> map, HttpServletRequest request) throws Exception;

	void getDevLogEnglish(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void EngineStop(Map<String, Object> map, HttpServletRequest request) throws IOException, InterruptedException;
	
	List<Map<String, Object>> selectDomainVersion(Map<String, Object> map) throws Exception;	
	
	List<Map<String, Object>> selectLanguage(Map<String, Object> map) throws Exception;	
	
	void insertUserSayFile(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void insertTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void deleteTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void updateTutorDialogMap(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String getTutorDialogMission(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String openDialogFile(Map<String, Object> map, HttpServletRequest request);
	
	void TuTorRunStart(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String TutorRunInput(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void TutorDialogStop(Map<String, Object> map, HttpServletRequest request) throws Exception;

	String GetPath(String Type, Boolean LoginChk, HttpServletRequest request);

	String GetDomainName(String Type, Boolean LoginChk, Map<String, Object> map, HttpServletRequest request);
	
	void DownloadIntent(Map<String, Object> map, HttpServletRequest request) throws Exception;

	void updateMigration(Map<String, Object> map) throws Exception;

	Map<String, Object> getBizChatbotUser(Map<String, Object> map) throws Exception;
}
