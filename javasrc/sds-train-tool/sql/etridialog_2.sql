-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 221.143.50.221    Database: etridialog
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent_actions`
--

DROP TABLE IF EXISTS `agent_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_actions` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `_TYPE` char(1) DEFAULT NULL,
  `_CONDITION` varchar(5000) DEFAULT NULL,
  `ACT` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_actions_1` (`AGENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_filling_slot`
--

DROP TABLE IF EXISTS `agent_filling_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_filling_slot` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `SLOT_SEQ` varchar(100) DEFAULT NULL,
  `SLOT_NAME` varchar(1000) DEFAULT NULL,
  `_CONDITION` varchar(5000) DEFAULT NULL,
  `PROGRESS_SLOT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_filling_slot_1` (`AGENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_graph`
--

DROP TABLE IF EXISTS `agent_graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_graph` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `SOURCE_ID` int(11) DEFAULT NULL,
  `TARGET_ID` int(11) DEFAULT NULL,
  `_CONDITION` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_graph_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=2287 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_intention`
--

DROP TABLE IF EXISTS `agent_intention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_intention` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `FILLING_SEQ` int(11) DEFAULT NULL,
  `UTTR` varchar(5000) DEFAULT NULL,
  `ACT` varchar(5000) DEFAULT NULL,
  `INTENTION_TYPE` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_intention_1` (`AGENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=4432 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_monitoring`
--

DROP TABLE IF EXISTS `agent_monitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_monitoring` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `_TYPE` char(1) DEFAULT NULL,
  `INTEREST` int(11) DEFAULT NULL,
  `WARNING` int(11) DEFAULT NULL,
  `DANGER` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_object`
--

DROP TABLE IF EXISTS `agent_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_object` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `INTENTION_SEQ` int(11) DEFAULT NULL,
  `OBJECT_VALUE` varchar(100) DEFAULT NULL,
  `LIMIT_DA` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_object_1` (`AGENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=3827 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_object_detail`
--

DROP TABLE IF EXISTS `agent_object_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_object_detail` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `OBJECT_SEQ` int(11) DEFAULT NULL,
  `OBJECT_TYPE` char(1) DEFAULT NULL,
  `OBJECT_VALUE` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agent_object_detail_1` (`AGENT_SEQ`),
  KEY `IDX_agent_object_detail_2` (`OBJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=6777 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `agents` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_NAME` varchar(50) DEFAULT NULL,
  `RESET_SLOT` varchar(500) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `LEFT_POS` smallint(6) DEFAULT NULL,
  `TOP_POS` smallint(6) DEFAULT NULL,
  `TASK_TYPE` varchar(10) DEFAULT NULL,
  `RELATED_SLOTS` varchar(200) DEFAULT NULL,
  `SORT_NO` tinyint(4) DEFAULT '0',
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FROM_COPY` varchar(100) DEFAULT NULL,
  `TASK_GOAL` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_agents_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1859 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatbot_object`
--

DROP TABLE IF EXISTS `chatbot_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `chatbot_object` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  `OBJECT_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatbot_object_detail`
--

DROP TABLE IF EXISTS `chatbot_object_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `chatbot_object_detail` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  `OBJECT_NAME` varchar(100) DEFAULT NULL,
  `PARENT_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatbot_train`
--

DROP TABLE IF EXISTS `chatbot_train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `chatbot_train` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatbot_train_detail`
--

DROP TABLE IF EXISTS `chatbot_train_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `chatbot_train_detail` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  `TRAIN_TYPE` varchar(3) DEFAULT NULL,
  `TRAIN_SAY` varchar(500) DEFAULT NULL,
  `PARENT_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `count_mng`
--

DROP TABLE IF EXISTS `count_mng`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `count_mng` (
  `DOMAIN_COUNT` int(11) DEFAULT NULL,
  `USERSAY_COUNT` int(11) DEFAULT NULL,
  `SLOTOBJ_COUNT` int(11) DEFAULT NULL,
  `INSTANCE_COUNT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `count_mng`
--

LOCK TABLES `count_mng` WRITE;
/*!40000 ALTER TABLE `count_mng` DISABLE KEYS */;
INSERT INTO `count_mng` VALUES (10,3000,4000,2000);
/*!40000 ALTER TABLE `count_mng` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `encoding`
--

DROP TABLE IF EXISTS `encoding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `encoding` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `ENCODING_NAME` varchar(20) DEFAULT NULL,
  `USE_YN` char(1) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `encoding`
--

LOCK TABLES `encoding` WRITE;
/*!40000 ALTER TABLE `encoding` DISABLE KEYS */;
INSERT INTO `encoding` VALUES (1,'EUC-KR','N'),(2,'UTF-8','Y');
/*!40000 ALTER TABLE `encoding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functions`
--

DROP TABLE IF EXISTS `functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `functions` (
  `SEQ_NO` int(11) NOT NULL,
  `FUNCTION_NAME` varchar(30) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `PARAM_COUNT` tinyint(4) DEFAULT NULL,
  `REMARKS` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `functions`
--

LOCK TABLES `functions` WRITE;
/*!40000 ALTER TABLE `functions` DISABLE KEYS */;
INSERT INTO `functions` VALUES (1,'Count','(\"슬롯이름\")',1,''),(2,'Value','(\"슬롯이름\")',1,''),(3,'ExistValue','(\"슬롯이름\")',1,''),(4,'ExistValueFromDialog','(\"슬롯이름\")',1,''),(5,'ExistValueAtHistory','(\"슬롯이름\", \"검사할시점\")',2,''),(6,'ExistSlotAtCurrentUtter','(\"슬롯이름\")',1,''),(7,'ExistSlotAtPreviousUtter','(\"슬롯이름\")',1,''),(8,'ExistSlotAtUtterHistory','(\"슬롯이름\", \"검사할시점\")',2,''),(9,'ExistClassAtCurrentUtter','(\"클래스이름\")',1,''),(10,'ExistClassAtPreviousUtter','(\"클래스이름\")',1,''),(11,'HasValue','(\"슬롯이름\", \"슬롯값\")',2,''),(12,'HasValueHistory','(\"슬롯이름\", \"슬롯값\")',2,''),(13,'HasValueAtCurrentUtter','(\"슬롯이름\", \"슬롯값\")',2,''),(14,'HasValueAtPreviousUtter','(\"슬롯이름\", \"슬롯값\")',2,''),(15,'IsDAType','(\"발화자\", \"DA type\")',2,''),(16,'IsDATypeAtCurrentUtter','(\"DA type\")',1,''),(17,'IsDATypeAtPreviousUtter','(\"발화자\", \"DA type\")',2,''),(18,'IsDATypeAtUtterHistory','(\"발화자\", \"DA type\", \"검사할시점\")',3,''),(19,'IsDATypeAtCurrentTask','(\"발화자\", \"DA type\")',2,''),(20,'IsDA','(\"발화자\", \"의도\")',2,''),(21,'IsConfirmTrue','()',0,''),(22,'Set','(\"슬롯이름\", \"슬롯값\")',2,''),(23,'Reset','(\"슬롯이름\", \"클래스이름\", ... )',1,''),(24,'RecoverHistory','()',0,''),(25,'ShowHistory','()',0,''),(26,'ExistValueAtDB','(\"슬롯이름\")',1,''),(28,'PrintDate','(\"슬롯이름\")',1,''),(29,'PrintDateWeek','(\"슬롯이름\")',1,''),(30,'PrintDateToday','(\"슬롯이름\")',1,''),(31,'PrintTime','(\"슬롯이름\")',1,''),(32,'PrintTimeWithoutSec','(\"슬롯이름\")',1,''),(33,'PrintTimeAMPM','(\"슬롯이름\")',1,''),(34,'PrintTimeAMPMWithoutSec','(\"슬롯이름\")',1,''),(35,'CountHistory','(\"슬롯이름\")',1,'');
/*!40000 ALTER TABLE `functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intention`
--

DROP TABLE IF EXISTS `intention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intention` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `INTENTION_NAME` varchar(50) DEFAULT NULL,
  `INTENTION_SLOT` varchar(100) DEFAULT NULL,
  `INTENTION_SLOT_VALUE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intention_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents`
--

DROP TABLE IF EXISTS `intents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `INTENT_NAME` varchar(500) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `COPY_FLAG` char(1) DEFAULT 'N',
  `FROM_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_1` (`PROJECT_SEQ`),
  KEY `IDX_intents_2` (`AGENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1495 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_edit`
--

DROP TABLE IF EXISTS `intents_edit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_edit` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `EDIT_VALUE` longtext,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_edit_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_intention`
--

DROP TABLE IF EXISTS `intents_intention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_intention` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `INTENT_SEQ` int(11) DEFAULT NULL,
  `UTTR` varchar(5000) DEFAULT NULL,
  `ACT` varchar(5000) DEFAULT NULL,
  `SLOT_SEQ_NO` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_intention_1` (`INTENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=862 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_object`
--

DROP TABLE IF EXISTS `intents_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_object` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `INTENT_SEQ` int(11) DEFAULT NULL,
  `INTENTION_SEQ` int(11) DEFAULT NULL,
  `OBJECT_VALUE` varchar(100) DEFAULT NULL,
  `LIMIT_DA` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_object_1` (`INTENT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1297 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_object_detail`
--

DROP TABLE IF EXISTS `intents_object_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_object_detail` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `INTENT_SEQ` int(11) DEFAULT NULL,
  `OBJECT_SEQ` int(11) DEFAULT NULL,
  `OBJECT_TYPE` char(1) DEFAULT NULL,
  `OBJECT_VALUE` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_object_detail_1` (`INTENT_SEQ`),
  KEY `IDX_intents_object_detail_2` (`OBJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1776 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_replace`
--

DROP TABLE IF EXISTS `intents_replace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_replace` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `R_BEFORE` varchar(500) DEFAULT NULL,
  `R_AFTER` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_replace_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intents_user_says`
--

DROP TABLE IF EXISTS `intents_user_says`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `intents_user_says` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `SAY` varchar(500) DEFAULT NULL,
  `INTENT_SEQ` int(11) DEFAULT NULL,
  `SLOT_SEQ_NO` varchar(100) DEFAULT NULL,
  `SAY_TAG` varchar(1000) DEFAULT NULL,
  `SLOT_TAG` varchar(1000) DEFAULT NULL,
  `SLOT_NAME` varchar(500) DEFAULT NULL,
  `SLOT_MAP` varchar(1000) DEFAULT NULL,
  `SLOT_MAPDA` varchar(1000) DEFAULT NULL,
  `INTENTION` varchar(100) DEFAULT '',
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_intents_user_says_1` (`INTENT_SEQ`),
  KEY `IDX_intents_user_says_2` (`SLOT_SEQ_NO`,`INTENTION`)
) ENGINE=InnoDB AUTO_INCREMENT=22394 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `LANG_NAME` varchar(50) DEFAULT NULL,
  `ENGINE_NAME` varchar(30) DEFAULT NULL,
  `DIC_NAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'한국어','dial','nlp_dict'),(2,'English','dial.english','nlp_dict.eng');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports`
--

DROP TABLE IF EXISTS `ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `ports` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `SERVER_PORT` varchar(5) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_NAME` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `USER_SEQ_NO` int(11) DEFAULT NULL,
  `PARENT_SEQ_NO` int(11) DEFAULT NULL,
  `FROM_SEQ_NO` int(11) DEFAULT NULL,
  `LANG_SEQ_NO` int(11) DEFAULT NULL,
  `DELETE_FLAG` char(1) DEFAULT 'N',
  `CHATBOT_USE_FLAG` char(1) DEFAULT NULL,
  `CHATBOT_DATA_FLAG` char(1) DEFAULT NULL,
  `FILTER_VALUE` varchar(10) DEFAULT NULL,
  `LEARN_CHECK` char(1) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_project_1` (`USER_SEQ_NO`),
  KEY `IDX_project_2` (`PARENT_SEQ_NO`,`DELETE_FLAG`)
) ENGINE=InnoDB AUTO_INCREMENT=571 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reference_class`
--

DROP TABLE IF EXISTS `reference_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `reference_class` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `CLASS_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_reference_class_1` (`AGENT_SEQ`),
  KEY `IDX_reference_class_2` (`CLASS_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sample_domain`
--

DROP TABLE IF EXISTS `sample_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `sample_domain` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `SAMPLE_NAME` varchar(100) DEFAULT NULL,
  `PROJECT_NAME` varchar(100) DEFAULT NULL,
  `USER_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot`
--

DROP TABLE IF EXISTS `slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `SLOT_NAME` varchar(50) DEFAULT NULL,
  `CLASS_SEQ_NO` int(11) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `slot_type` char(1) DEFAULT NULL,
  `type_seq` int(11) DEFAULT NULL,
  `type_name` varchar(50) DEFAULT NULL,
  `SORT_NO` smallint(6) DEFAULT NULL,
  `COLOR` varchar(8) DEFAULT NULL,
  `SLOT_DESCRIPTION` varchar(100) DEFAULT NULL,
  `PRCE_SLOT_SEQ` varchar(100) DEFAULT NULL,
  `PRCE_SLOT_NAME` varchar(50) DEFAULT NULL,
  `SENSE_SEQ` int(11) DEFAULT NULL,
  `SENSE_NAME` varchar(50) DEFAULT NULL,
  `INSTANCE_FLAG` char(1) DEFAULT 'Y',
  `TYPE_SUB_SEQ` varchar(100) DEFAULT NULL,
  `PRCE_SLOT_SUB_SEQ` varchar(100) DEFAULT NULL,
  `SET_FLAG` char(1) DEFAULT 'N',
  `MIN_MAX_VALUE` tinyint(4) DEFAULT NULL,
  `DEFAULT_VALUE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_1` (`CLASS_SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=1872 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_addition`
--

DROP TABLE IF EXISTS `slot_addition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_addition` (
  `SLOT_SEQ_NO` varchar(100) NOT NULL,
  `DEFAULT_VALUE` varchar(100) DEFAULT NULL,
  `SLOT_DESCRIPTION` varchar(100) DEFAULT NULL,
  `PRCE_SLOT_SEQ` int(11) DEFAULT NULL,
  `PRCE_SLOT_NAME` varchar(50) DEFAULT NULL,
  `PRCE_SLOT_SUB_SEQ` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SLOT_SEQ_NO`),
  KEY `IDX_slot_addition_1` (`SLOT_SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_class`
--

DROP TABLE IF EXISTS `slot_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_class` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `CLASS_NAME` varchar(50) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `NEW_CHECK` char(1) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_class_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1316 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_color`
--

DROP TABLE IF EXISTS `slot_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_color` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `CLASS_SEQ` int(11) DEFAULT NULL,
  `SLOT_NAME` varchar(100) DEFAULT NULL,
  `COLOR` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_color_1` (`PROJECT_SEQ`,`CLASS_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=1806 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_define`
--

DROP TABLE IF EXISTS `slot_define`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_define` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `AGENT_SEQ` int(11) DEFAULT NULL,
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  `TARGET_SEQ` int(11) DEFAULT NULL,
  `TARGET_NAME` varchar(100) DEFAULT NULL,
  `CON` varchar(5000) DEFAULT NULL,
  `ACT` varchar(5000) DEFAULT NULL,
  `TARGET_SUB_SEQ` varchar(100) DEFAULT NULL,
  `SUB_SLOT_SEQ` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_define_1` (`PROJECT_SEQ`,`SLOT_SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_instance`
--

DROP TABLE IF EXISTS `slot_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_instance` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  `CLASS_SEQ_NO` int(11) DEFAULT NULL,
  `INSTANCE_VAL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_mapping`
--

DROP TABLE IF EXISTS `slot_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_mapping` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `TAGGED_SLOT_KEY` varchar(50) DEFAULT NULL,
  `TAGGED_SLOT_VALUE` varchar(50) DEFAULT NULL,
  `QUERY_COND` varchar(5) DEFAULT NULL,
  `DB_SLOT_KEY` varchar(50) DEFAULT NULL,
  `DB_SLOT_VALUE` varchar(100) DEFAULT NULL,
  `PROJECT_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_mapping_1` (`PROJECT_SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_object`
--

DROP TABLE IF EXISTS `slot_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_object` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `OBJECT_NAME` varchar(50) DEFAULT NULL,
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_object_1` (`SLOT_SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=3073 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_object_detail`
--

DROP TABLE IF EXISTS `slot_object_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_object_detail` (
  `SEQ_NO` bigint(20) NOT NULL AUTO_INCREMENT,
  `OBJECT_NAME` varchar(50) DEFAULT NULL,
  `PARENT_SEQ_NO` int(11) DEFAULT NULL,
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_slot_object_detail_1` (`PARENT_SEQ_NO`),
  KEY `IDX_slot_object_detail_2` (`SLOT_SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=6079 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_sub`
--

DROP TABLE IF EXISTS `slot_sub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_sub` (
  `SLOT_SUB_SEQ` varchar(100) NOT NULL,
  `SLOT_SUB_NAME` varchar(100) DEFAULT NULL,
  `SLOT_SUB_NAME_SEQ` varchar(100) DEFAULT NULL,
  `SLOT_COLUMN_NAME` varchar(50) DEFAULT NULL,
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  `CLASS_SEQ_NO` int(11) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SLOT_SUB_SEQ`),
  KEY `IDX_slot_sub_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_type_class`
--

DROP TABLE IF EXISTS `slot_type_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot_type_class` (
  `SLOT_SEQ_NO` int(11) DEFAULT NULL,
  `CLASS_SEQ_NO` int(11) DEFAULT NULL,
  `TYPE_SEQ_NO` int(11) DEFAULT NULL,
  KEY `IDX_slot_type_class_1` (`TYPE_SEQ_NO`),
  KEY `IDX_slot_type_class_2` (`SLOT_SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_type`
--

DROP TABLE IF EXISTS `sys_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_type` (
  `SEQ_NO` int(11) NOT NULL,
  `TYPE_NAME` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `sys_type`
--

LOCK TABLES `sys_type` WRITE;
/*!40000 ALTER TABLE `sys_type` DISABLE KEYS */;
INSERT INTO `sys_type` VALUES (3,'person'),(4,'place'),(5,'date'),(6,'time');
/*!40000 ALTER TABLE `sys_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_dialog_map`
--

DROP TABLE IF EXISTS `tutor_dialog_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_dialog_map` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  `FILE_NAME` varchar(200) DEFAULT NULL,
  `LOAD_DATE` datetime DEFAULT NULL,
  `TUTOR_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tutor_project`
--

DROP TABLE IF EXISTS `tutor_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_project` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `USER_SEQ_NO` int(11) DEFAULT NULL,
  `LANG_SEQ_NO` int(11) DEFAULT NULL,
  `DELETE_FLAG` char(1) DEFAULT 'N',
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tutor_users_domain`
--

DROP TABLE IF EXISTS `tutor_users_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_users_domain` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(50) DEFAULT NULL,
  `USER_PWD` varchar(100) DEFAULT NULL,
  `USER_NAME` varchar(50) DEFAULT NULL,
  `USER_TYPE` varchar(4) DEFAULT NULL,
  `REMARKS` varchar(1000) DEFAULT NULL,
  `API_KEY` varchar(40) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `LAST_LOGIN_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_users_1` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
-- INSERT INTO `users` VALUES (1,'admin','AHRmFEcuszvo0L1v%2FFCpPg%3D%3D','관리자','SA','',NULL);
INSERT INTO `users` (USER_ID, USER_PWD, USER_NAME, USER_TYPE, REMARKS, API_KEY) VALUES ('admin','AHRmFEcuszvo0L1v%2FFCpPg%3D%3D','관리자','SA','',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_domain`
--

DROP TABLE IF EXISTS `users_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_domain` (
  `SEQ_NO` int(11) NOT NULL AUTO_INCREMENT,
  `USER_SEQ` int(11) DEFAULT NULL,
  `PROJECT_SEQ` int(11) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NO`),
  KEY `IDX_users_domain_1` (`PROJECT_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=506 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `v_instens_user_say`
--

DROP TABLE IF EXISTS `v_instens_user_say`;
/*!50001 DROP VIEW IF EXISTS `v_instens_user_say`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `v_instens_user_say` AS SELECT 
 1 AS `SEQ_NO`,
 1 AS `SLOT_SEQ_NO`,
 1 AS `INTENT_SEQ`,
 1 AS `SLOT_TAG`,
 1 AS `INTENTION`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_slot_object`
--

DROP TABLE IF EXISTS `v_slot_object`;
/*!50001 DROP VIEW IF EXISTS `v_slot_object`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `v_slot_object` AS SELECT 
 1 AS `SEQ_NO`,
 1 AS `OBJECT_NAME`,
 1 AS `SLOT_SEQ_NO`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_type_list`
--

DROP TABLE IF EXISTS `v_type_list`;
/*!50001 DROP VIEW IF EXISTS `v_type_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `v_type_list` AS SELECT 
 1 AS `PROJECT_SEQ`,
 1 AS `SLOT_TYPE`,
 1 AS `SEQ_NO`,
 1 AS `CLASS_SEQ`,
 1 AS `TYPE_NAME`,
 1 AS `COLOR`,
 1 AS `SUB_SLOT_TYPE`,
 1 AS `TYPE_SEQ`,
 1 AS `SET_FLAG`,
 1 AS `SLOT_TYPE_NAME`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'etridialog'
--
/*!50003 DROP PROCEDURE IF EXISTS `parent_class_select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `parent_class_select`(CLASS_SEQ INT)
BEGIN
  	DECLARE WCHK TINYINT DEFAULT 0;
    DECLARE TEMP_SEQ INT;
      
  	DROP TEMPORARY TABLE IF EXISTS tmp_class;
  	CREATE TEMPORARY TABLE tmp_class (
  		CLASS_SEQ_NO	INT
  	);
  	
 	WHILE WCHK = 0 DO
  		SELECT CLASS_SEQ_NO into TEMP_SEQ
  		FROM slot_type_class
  		WHERE TYPE_SEQ_NO = CLASS_SEQ
        GROUP BY CLASS_SEQ_NO;
          
 		IF FOUND_ROWS() = 0 THEN
  			SET WCHK = 1;
  		ELSE
  			SET CLASS_SEQ = TEMP_SEQ;
  			INSERT INTO tmp_class(CLASS_SEQ_NO) VALUES(TEMP_SEQ);
  		END IF;                        
 	END WHILE;
    
    SELECT CLASS_SEQ_NO FROM tmp_class;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_agent_sort_init` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_agent_sort_init`()
BEGIN
  	DECLARE P_WCHK TINYINT DEFAULT 0;
    DECLARE A_WCHK TINYINT DEFAULT 0;
    DECLARE TEMP_P_IDX INT;
    DECLARE TEMP_A_IDX INT;
    DECLARE TEMP_P_SEQ INT;
    DECLARE TEMP_A_SEQ INT;
    
    DROP TEMPORARY TABLE IF EXISTS tmp_test;
    CREATE TEMPORARY TABLE tmp_test (
  		IDX	int primary key auto_increment,
        SEQ_NO	int,
        _TYPE char(1),
        SORT_NO int
  	);
    
    
	DROP TEMPORARY TABLE IF EXISTS tmp_project;
    CREATE TEMPORARY TABLE tmp_project (
  		IDX	int primary key auto_increment,
        SEQ_NO	int        
  	);
    
    INSERT INTO tmp_project(SEQ_NO)
    SELECT SEQ_NO
    FROM project;  	
    
    SET TEMP_P_IDX = 1;
	WHILE P_WCHK = 0 DO  
		SET TEMP_P_SEQ = (SELECT SEQ_NO FROM tmp_project WHERE IDX = TEMP_P_IDX);
        
        insert into tmp_test (SEQ_NO, _TYPE, SORT_NO) values(TEMP_P_SEQ, 'P', 0);
        
		IF (select exists(select * from project where SEQ_NO=TEMP_P_SEQ limit 1)) = 1 THEN
			DROP TEMPORARY TABLE IF EXISTS tmp_agent;
			CREATE TEMPORARY TABLE tmp_agent (
				IDX	int primary key auto_increment,
				SEQ_NO	int        
			);
			
			INSERT INTO tmp_agent(SEQ_NO)
			SELECT SEQ_NO
			FROM agents
			WHERE PROJECT_SEQ = TEMP_P_SEQ AND SORT_NO IS NOT NULL
			ORDER BY SORT_NO, SEQ_NO;
			
			SET TEMP_A_IDX = 1;
            SET A_WCHK = 0;
			WHILE A_WCHK = 0 DO  						
                SET TEMP_A_SEQ = (SELECT SEQ_NO FROM tmp_agent WHERE IDX = TEMP_A_IDX);
                insert into tmp_test (SEQ_NO, _TYPE, SORT_NO) values(TEMP_A_SEQ, 'T', TEMP_A_IDX - 1);
				IF (select exists(select * from agents where SEQ_NO=TEMP_A_SEQ limit 1)) = 1 THEN
					UPDATE agents SET SORT_NO = TEMP_A_IDX - 1
					WHERE SEQ_NO = TEMP_A_SEQ;
					SET TEMP_A_IDX = TEMP_A_IDX + 1;
                ELSE
					SET A_WCHK = 1;
                END IF;                
			END WHILE; 
            
            SET TEMP_P_IDX = TEMP_P_IDX + 1;
		ELSE
			SET P_WCHK = 1;
		END IF;
	END WHILE;           
    select * from tmp_test;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_copy_domain` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_copy_domain`(OLD_PRJ_SEQ INT, NEW_PRJ_SEQ INT)
BEGIN	     
    DROP TEMPORARY TABLE IF EXISTS tmp_copy;
	CREATE TEMPORARY TABLE tmp_copy (
		OLD_SEQ_NO	INT,
		NEW_SEQ_NO	INT,
		C_TYPE		VARCHAR(5)
	);
	
	-- Agent 복사 시작
	INSERT INTO agents (AGENT_NAME, RESET_SLOT, PROJECT_SEQ, LEFT_POS, TOP_POS, TASK_TYPE, RELATED_SLOTS, SORT_NO, DESCRIPTION)
	SELECT AGENT_NAME, RESET_SLOT, NEW_PRJ_SEQ, LEFT_POS, TOP_POS, TASK_TYPE, RELATED_SLOTS, SORT_NO, DESCRIPTION
	FROM agents
	WHERE PROJECT_SEQ = OLD_PRJ_SEQ
    ORDER BY SEQ_NO;
	
    insert into tmp_copy 
	select a.*, 'a' from (
		select a.SEQ_NO OLD_SEQ_NO, b.SEQ_NO NEW_SEQ_NO from agents a inner join agents b
		on a.AGENT_NAME = b.AGENT_NAME
		where a.PROJECT_SEQ = OLD_PRJ_SEQ and b.PROJECT_SEQ = NEW_PRJ_SEQ
	) a;
    
    INSERT INTO agent_graph (PROJECT_SEQ, SOURCE_ID, TARGET_ID, _CONDITION)
    SELECT NEW_PRJ_SEQ, SOURCE_ID, TARGET_ID, _CONDITION
	FROM agent_graph
	WHERE PROJECT_SEQ = OLD_PRJ_SEQ;
    
    update agent_graph a inner join agents b
	on a.SOURCE_ID = b.SEQ_NO and b.PROJECT_SEQ = OLD_PRJ_SEQ inner join agents	c
	on b.AGENT_NAME = c.AGENT_NAME and c.PROJECT_SEQ = NEW_PRJ_SEQ
	set a.SOURCE_ID = c.SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ;

	update agent_graph a inner join agents b
	on a.TARGET_ID = b.SEQ_NO and b.PROJECT_SEQ = OLD_PRJ_SEQ inner join agents	c
	on b.AGENT_NAME = c.AGENT_NAME and c.PROJECT_SEQ = NEW_PRJ_SEQ
	set a.TARGET_ID = c.SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ;   
    
    insert into  agent_filling_slot (AGENT_SEQ, SLOT_SEQ, SLOT_NAME, _CONDITION, PROGRESS_SLOT)
	select b.NEW_SEQ_NO AGENT_SEQ, a.SLOT_SEQ, a.SLOT_NAME, a._CONDITION, a.PROGRESS_SLOT
	from agent_filling_slot a inner join (
	select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from agents sa inner join agents sb
	on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.AGENT_NAME = sb.AGENT_NAME) b
	where a.AGENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;	
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'afs'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_filling_slot,(SELECT @ROWNUM := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_filling_slot,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    insert into agent_intention (AGENT_SEQ, FILLING_SEQ, UTTR, ACT, INTENTION_TYPE)
	select b.NEW_SEQ_NO AGENT_SEQ, a.FILLING_SEQ, a.UTTR, a.ACT, a.INTENTION_TYPE
	from agent_intention a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from agents sa inner join agents sb
		on sa.AGENT_NAME = sb.AGENT_NAME where sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ) b 
	where a.AGENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    update agent_intention a inner join tmp_copy b
	on a.FILLING_SEQ = b.OLD_SEQ_NO
	set a.FILLING_SEQ = b.NEW_SEQ_NO 
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and a.FILLING_SEQ <> 0 and b.C_TYPE = 'afs';
    
	insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ai'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_intention,(SELECT @ROWNUM := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_intention,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ)  order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
        
    insert into agent_object (AGENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA)
	select b.NEW_SEQ_NO AGENT_SEQ, a.INTENTION_SEQ, a.OBJECT_VALUE, LIMIT_DA
	from agent_object a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from agents sa inner join agents sb
		on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.AGENT_NAME = sb.AGENT_NAME) b
	where a.AGENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ao'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_object,(SELECT @ROWNUM := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_object,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    update agent_object a inner join tmp_copy b
	on a.INTENTION_SEQ = b.OLD_SEQ_NO
	set a.INTENTION_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'ai';
    
    insert into agent_object_detail (AGENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE)
	select b.NEW_SEQ_NO AGENT_SEQ, a.OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE
	from agent_object_detail a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from agents sa inner join agents sb
		on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.AGENT_NAME = sb.AGENT_NAME) b
	where a.AGENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    update agent_object_detail a inner join tmp_copy b
	on a.OBJECT_SEQ = b.OLD_SEQ_NO
	set a.OBJECT_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'ao';
    
    insert into agent_monitoring (PROJECT_SEQ, AGENT_SEQ, _TYPE, INTEREST, WARNING, DANGER)
    select NEW_PRJ_SEQ, AGENT_SEQ, _TYPE, INTEREST, WARNING, DANGER
    from agent_monitoring
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    
    update agent_monitoring a inner join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO
	set a.AGENT_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'a';   		    
    
    insert into agent_actions (PROJECT_SEQ, AGENT_SEQ, _TYPE, _CONDITION, ACT)
    select NEW_PRJ_SEQ, AGENT_SEQ, _TYPE, _CONDITION, ACT
    from agent_actions
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    
	update agent_actions a inner join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO
	set a.AGENT_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'a';
    -- Agent 복사 끝
        
    -- Intents 복사 시작
    insert into intents (INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, FROM_SEQ_NO)
	select INTENT_NAME, NEW_PRJ_SEQ, IFNULL(b.NEW_SEQ_NO, 0) NEW_SEQ_NO, a.SEQ_NO
	from intents a left outer join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO and b.C_TYPE = 'a'
	where a.PROJECT_SEQ = OLD_PRJ_SEQ
    order by a.SEQ_NO;
      
    insert into tmp_copy 
	select a.*, 'i' from (
		select a.SEQ_NO OLD_SEQ_NO, b.SEQ_NO NEW_SEQ_NO from intents a inner join intents b
		on a.FROM_SEQ_NO = b.FROM_SEQ_NO
		where a.PROJECT_SEQ = OLD_PRJ_SEQ and b.PROJECT_SEQ = NEW_PRJ_SEQ
        order by a.SEQ_NO
	) a;
	
    insert into intents_intention (INTENT_SEQ, UTTR, ACT, SLOT_SEQ_NO)
	select b.NEW_SEQ_NO INTENT_SEQ, a.UTTR, a.ACT, a.SLOT_SEQ_NO
	from intents_intention a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.SEQ_NO = sb.FROM_SEQ_NO where sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ) b 
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ii'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM := 0) R where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = NEW_PRJ_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    insert into intents_object (INTENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA)
	select b.NEW_SEQ_NO INTENT_SEQ, a.INTENTION_SEQ, a.OBJECT_VALUE, LIMIT_DA
	from intents_object a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
        
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'io'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM := 0) R where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = NEW_PRJ_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    update intents_object a inner join tmp_copy b
	on a.INTENTION_SEQ = b.OLD_SEQ_NO
	set a.INTENTION_SEQ = b.NEW_SEQ_NO
	where a.INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'ii';
    
    insert into intents_object_detail (INTENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE)
	select b.NEW_SEQ_NO INTENT_SEQ, a.OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE
	from intents_object_detail a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b	
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;

	update intents_object_detail a inner join tmp_copy b
	on a.OBJECT_SEQ = b.OLD_SEQ_NO
	set a.OBJECT_SEQ = b.NEW_SEQ_NO
	where a.INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'io';
    
    insert into intents_user_says (SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA, INTENTION)
	select SAY, b.NEW_SEQ_NO INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA, INTENTION
	from intents_user_says a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.PROJECT_SEQ = OLD_PRJ_SEQ and sb.PROJECT_SEQ = NEW_PRJ_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b	
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    insert into intents_replace (PROJECT_SEQ, R_BEFORE, R_AFTER)
    select NEW_PRJ_SEQ, R_BEFORE, R_AFTER
    from intents_replace 
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    
    insert into intents_edit (PROJECT_SEQ, EDIT_VALUE)
    select NEW_PRJ_SEQ, EDIT_VALUE
    from intents_edit 
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    
    insert into intention (PROJECT_SEQ, INTENTION_NAME, INTENTION_SLOT, INTENTION_SLOT_VALUE)
    select NEW_PRJ_SEQ, INTENTION_NAME, INTENTION_SLOT, INTENTION_SLOT_VALUE
    from intention
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    -- Intents 복사 끝
	
	-- Slot 업데이트 및 복사 시작
    -- 자바단에서 만든 slot_class agent_seq 업데이트
    update slot_class a inner join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO
	set a.AGENT_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'a';
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'sc'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from slot_class,(SELECT @ROWNUM := 0) R where PROJECT_SEQ = OLD_PRJ_SEQ order by CLASS_NAME, SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from slot_class,(SELECT @ROWNUM2 := 0) R where PROJECT_SEQ = NEW_PRJ_SEQ order by CLASS_NAME, SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 's'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from slot,(SELECT @ROWNUM := 0) R where PROJECT_SEQ = OLD_PRJ_SEQ order by SLOT_NAME, CLASS_SEQ_NO, SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from slot,(SELECT @ROWNUM2 := 0) R where PROJECT_SEQ = NEW_PRJ_SEQ order by SLOT_NAME, CLASS_SEQ_NO, SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
      
    insert into slot_object(OBJECT_NAME, SLOT_SEQ_NO)
	select OBJECT_NAME, b.NEW_SEQ_NO
	from slot_object a inner join tmp_copy b
	on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
	where b.C_TYPE = 's'
	order by a.SEQ_NO;
    
    insert into tmp_copy 
	select a.SEQ_NO, b.SEQ_NO, 'so'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from slot_object,(SELECT @ROWNUM := 0) R where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = OLD_PRJ_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from slot_object,(SELECT @ROWNUM2 := 0) R where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = NEW_PRJ_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
   
    insert into slot_object_detail(OBJECT_NAME, PARENT_SEQ_NO, SLOT_SEQ_NO)
	select OBJECT_NAME, PARENT_SEQ_NO, b.NEW_SEQ_NO
	from slot_object_detail a inner join tmp_copy b
	on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
	where b.C_TYPE = 's'
	order by a.SEQ_NO;

	update slot_object_detail a inner join tmp_copy b
	on a.PARENT_SEQ_NO = b.OLD_SEQ_NO
	set a.PARENT_SEQ_NO = b.NEW_SEQ_NO
	where a.SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'so';
    
    insert slot_type_class (SLOT_SEQ_NO, CLASS_SEQ_NO, TYPE_SEQ_NO)
	select b.NEW_SEQ_NO, CLASS_SEQ_NO, TYPE_SEQ_NO
	from slot_type_class a inner join tmp_copy b
    on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
	where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = OLD_PRJ_SEQ) and b.C_TYPE = 's';
    
	update slot_type_class a inner join tmp_copy b
	on a.CLASS_SEQ_NO = b.OLD_SEQ_NO
	set a.CLASS_SEQ_NO = b.NEW_SEQ_NO
	where a.SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'sc';

	update slot_type_class a inner join tmp_copy b
	on a.TYPE_SEQ_NO = b.OLD_SEQ_NO
	set a.TYPE_SEQ_NO = b.NEW_SEQ_NO
	where a.SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 'sc';
    
    insert into slot_define (PROJECT_SEQ, AGENT_SEQ, SLOT_SEQ_NO, TARGET_SEQ, TARGET_NAME, CON, ACT, SUB_SLOT_SEQ)
	select NEW_PRJ_SEQ, AGENT_SEQ, SLOT_SEQ_NO, TARGET_SEQ, TARGET_NAME, CON, ACT, SUB_SLOT_SEQ
	from slot_define
	where PROJECT_SEQ = OLD_PRJ_SEQ
    order by SEQ_NO;

	update slot_define a inner join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO
	set a.AGENT_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'a';

	update slot_define a inner join tmp_copy b
	on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
	set a.SLOT_SEQ_NO = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 's';

	update slot_define a inner join tmp_copy b
	on a.TARGET_SEQ = b.OLD_SEQ_NO
	set a.TARGET_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 's';
        
    insert slot_color (PROJECT_SEQ, CLASS_SEQ, SLOT_NAME, COLOR)
	select NEW_PRJ_SEQ, CLASS_SEQ, SLOT_NAME, COLOR
	from slot_color
	WHERE PROJECT_SEQ = OLD_PRJ_SEQ;
    
	update slot_color a inner join tmp_copy b
	on a.CLASS_SEQ = b.OLD_SEQ_NO
	set a.CLASS_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'sc';
    
    insert into slot_mapping (PROJECT_SEQ_NO, TAGGED_SLOT_KEY, TAGGED_SLOT_VALUE, QUERY_COND, DB_SLOT_KEY, DB_SLOT_VALUE)
    select NEW_PRJ_SEQ, TAGGED_SLOT_KEY, TAGGED_SLOT_VALUE, QUERY_COND, DB_SLOT_KEY, DB_SLOT_VALUE
    from slot_mapping 
    where PROJECT_SEQ_NO = OLD_PRJ_SEQ;
    
    insert into slot_sub (SLOT_SUB_SEQ, SLOT_SUB_NAME, SLOT_SUB_NAME_SEQ, SLOT_SEQ_NO, CLASS_SEQ_NO, PROJECT_SEQ)
    select concat(SLOT_SUB_SEQ, 'temp'), SLOT_SUB_NAME, SLOT_SUB_NAME_SEQ, SLOT_SEQ_NO, CLASS_SEQ_NO, NEW_PRJ_SEQ
    from slot_sub
    where PROJECT_SEQ = OLD_PRJ_SEQ;
    
    update slot_sub a inner join tmp_copy b
	on a.CLASS_SEQ_NO = b.OLD_SEQ_NO
	set a.CLASS_SEQ_NO = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'sc';
    
    update slot_sub a inner join tmp_copy b
	on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
	set a.SLOT_SEQ_NO = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 's';
	-- Slot 업데이트 및 복사 끝
    
    -- agent_filling_slot slot 업데이트
    update agent_filling_slot a inner join tmp_copy b
	on a.SLOT_SEQ = b.OLD_SEQ_NO
	set a.SLOT_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = NEW_PRJ_SEQ) and b.C_TYPE = 's';
    -- agent_filling_slot slot 업데이트 끝
    
    -- reference_class 복사 시작
    insert into reference_class (PROJECT_SEQ, AGENT_SEQ, CLASS_SEQ)
	select NEW_PRJ_SEQ, AGENT_SEQ, CLASS_SEQ
	from reference_class
	where PROJECT_SEQ = OLD_PRJ_SEQ;
    
    update reference_class a inner join tmp_copy b
	on a.AGENT_SEQ = b.OLD_SEQ_NO
	set a.AGENT_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'a';
    
    update reference_class a inner join tmp_copy b
	on a.CLASS_SEQ = b.OLD_SEQ_NO
	set a.CLASS_SEQ = b.NEW_SEQ_NO
	where a.PROJECT_SEQ = NEW_PRJ_SEQ and b.C_TYPE = 'sc';
    -- reference_class 복사 끝
       
    select OLD_SEQ_NO, NEW_SEQ_NO
    from tmp_copy
    where C_TYPE = 's';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_copy_intent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_copy_intent`(P_AGENT_SEQ INT, P_INTENT_SEQ INT, P_RESPONSE_CHK CHAR(1))
BEGIN	     
	DECLARE NEW_INTENT_SEQ INT;
    DROP TEMPORARY TABLE IF EXISTS tmp_i_copy;
	CREATE TEMPORARY TABLE tmp_i_copy (
		OLD_SEQ_NO	INT,
		NEW_SEQ_NO	INT,
		C_TYPE		VARCHAR(5)
	);	
    
    -- Intents 복사 시작
    insert into intents (INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG)
	select INTENT_NAME, PROJECT_SEQ, P_AGENT_SEQ, 'Y'
	from intents
	where SEQ_NO = P_INTENT_SEQ;
        
    SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
    
    insert into tmp_i_copy value(P_INTENT_SEQ, NEW_INTENT_SEQ, 'i');
	
    insert into intents_user_says (SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
	select SAY, NEW_INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA
	from intents_user_says 
	where INTENT_SEQ = P_INTENT_SEQ
    order by SEQ_NO;
    
    IF P_RESPONSE_CHK = 'Y' THEN
		insert into intents_intention (INTENT_SEQ, UTTR, ACT, SLOT_SEQ_NO)
		select NEW_INTENT_SEQ, UTTR, ACT, SLOT_SEQ_NO
		from intents_intention
		where INTENT_SEQ = P_INTENT_SEQ
		order by SEQ_NO;
		        
		insert into tmp_i_copy 
		select a.SEQ_NO, b.SEQ_NO, 'ii'
		from (
			select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM := 0) R where INTENT_SEQ = P_INTENT_SEQ order by SEQ_NO
		) a inner join (
			select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ = NEW_INTENT_SEQ order by SEQ_NO
		) b
		on a.ROWNUM = b.ROWNUM;
		
		insert into intents_object (INTENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA)
		select NEW_INTENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA
		from intents_object
		where INTENT_SEQ = P_INTENT_SEQ
		order by SEQ_NO;
			
		insert into tmp_i_copy 
		select a.SEQ_NO, b.SEQ_NO, 'io'
		from (
			select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM := 0) R where INTENT_SEQ = P_INTENT_SEQ order by SEQ_NO
		) a inner join (
			select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ = NEW_INTENT_SEQ order by SEQ_NO
		) b
		on a.ROWNUM = b.ROWNUM;
		
		update intents_object a inner join tmp_i_copy b
		on a.INTENTION_SEQ = b.OLD_SEQ_NO
		set a.INTENTION_SEQ = b.NEW_SEQ_NO
		where a.INTENT_SEQ = NEW_INTENT_SEQ and b.C_TYPE = 'ii';
		
		insert into intents_object_detail (INTENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE)
		select NEW_INTENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE
		from intents_object_detail 
		where INTENT_SEQ = P_INTENT_SEQ
		order by SEQ_NO;

		update intents_object_detail a inner join tmp_i_copy b
		on a.OBJECT_SEQ = b.OLD_SEQ_NO
		set a.OBJECT_SEQ = b.NEW_SEQ_NO
		where a.INTENT_SEQ = NEW_INTENT_SEQ and b.C_TYPE = 'io';	
	END IF;                      
    -- Intents 복사 끝	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_copy_task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_copy_task`(P_PROJECT_SEQ INT, P_OLD_AGENT_SEQ INT, P_NEW_AGENT_SEQ INT, P_COPY_CHECK CHAR(1))
BEGIN	     	
	DECLARE OLD_PROJECT_SEQ INT;
    DROP TEMPORARY TABLE IF EXISTS tmp_t_copy;
	CREATE TEMPORARY TABLE tmp_t_copy (
		OLD_SEQ_NO	INT,
		NEW_SEQ_NO	INT,
		C_TYPE		VARCHAR(5)
	);	        
    
    -- 이전 Agent의 Project 번호 저장
    SELECT PROJECT_SEQ into OLD_PROJECT_SEQ
	FROM agents
    WHERE SEQ_NO = P_OLD_AGENT_SEQ;
    
    -- Agent 복사 시작
    insert into  agent_filling_slot (AGENT_SEQ, SLOT_SEQ, SLOT_NAME, _CONDITION, PROGRESS_SLOT)
	select P_NEW_AGENT_SEQ, SLOT_SEQ, SLOT_NAME, _CONDITION, PROGRESS_SLOT
	from agent_filling_slot 
	where AGENT_SEQ = P_OLD_AGENT_SEQ
    order by SEQ_NO;	
    	
    insert into tmp_t_copy 
	select a.SEQ_NO, b.SEQ_NO, 'afs'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_filling_slot,(SELECT @ROWNUM := 0) R where AGENT_SEQ = P_OLD_AGENT_SEQ order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_filling_slot,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ = P_NEW_AGENT_SEQ order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    insert into agent_intention (AGENT_SEQ, FILLING_SEQ, UTTR, ACT, INTENTION_TYPE)
	select P_NEW_AGENT_SEQ, FILLING_SEQ, UTTR, ACT, INTENTION_TYPE
	from agent_intention
	where AGENT_SEQ = P_OLD_AGENT_SEQ
    order by SEQ_NO;
    
    update agent_intention a inner join tmp_t_copy b
	on a.FILLING_SEQ = b.OLD_SEQ_NO
	set a.FILLING_SEQ = b.NEW_SEQ_NO 
	where a.AGENT_SEQ = P_NEW_AGENT_SEQ and a.FILLING_SEQ <> 0 and b.C_TYPE = 'afs';
        
	insert into tmp_t_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ai'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_intention,(SELECT @ROWNUM := 0) R where AGENT_SEQ = P_OLD_AGENT_SEQ order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_intention,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ = P_NEW_AGENT_SEQ order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
	
    insert into agent_object (AGENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA)
	select P_NEW_AGENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA
	from agent_object
	where AGENT_SEQ = P_OLD_AGENT_SEQ
    order by SEQ_NO;
    
    insert into tmp_t_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ao'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from agent_object,(SELECT @ROWNUM := 0) R where AGENT_SEQ = P_OLD_AGENT_SEQ order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from agent_object,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ = P_NEW_AGENT_SEQ order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    update agent_object a inner join tmp_t_copy b
	on a.INTENTION_SEQ = b.OLD_SEQ_NO
	set a.INTENTION_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 'ai';
    
    insert into agent_object_detail (AGENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE)
	select P_NEW_AGENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE
	from agent_object_detail
	where AGENT_SEQ = P_OLD_AGENT_SEQ
    order by SEQ_NO;
    
    update agent_object_detail a inner join tmp_t_copy b
	on a.OBJECT_SEQ = b.OLD_SEQ_NO
	set a.OBJECT_SEQ = b.NEW_SEQ_NO
	where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 'ao';
    
    insert into agent_monitoring (PROJECT_SEQ, AGENT_SEQ, _TYPE, INTEREST, WARNING, DANGER)
    select P_PROJECT_SEQ, P_NEW_AGENT_SEQ, _TYPE, INTEREST, WARNING, DANGER
    from agent_monitoring
    where AGENT_SEQ = P_OLD_AGENT_SEQ;
    
    insert into agent_actions (PROJECT_SEQ, AGENT_SEQ, _TYPE, _CONDITION, ACT)
    select P_PROJECT_SEQ, P_NEW_AGENT_SEQ, _TYPE, _CONDITION, ACT
    from agent_actions
    where AGENT_SEQ = P_OLD_AGENT_SEQ;
    -- Agent 복사 끝
	
    -- Intents 복사 시작
    insert into intents (INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, FROM_SEQ_NO)
	select INTENT_NAME, P_PROJECT_SEQ, P_NEW_AGENT_SEQ, SEQ_NO
	from intents 
	where AGENT_SEQ = P_OLD_AGENT_SEQ
    order by SEQ_NO;
      
    insert into tmp_t_copy 
	select a.*, 'i' from (
		select a.SEQ_NO OLD_SEQ_NO, b.SEQ_NO NEW_SEQ_NO from intents a inner join intents b
		on a.SEQ_NO = b.FROM_SEQ_NO
		where a.AGENT_SEQ = P_OLD_AGENT_SEQ and b.AGENT_SEQ = P_NEW_AGENT_SEQ
        order by a.SEQ_NO
	) a;
	
    insert into intents_intention (INTENT_SEQ, UTTR, ACT, SLOT_SEQ_NO)
	select b.NEW_SEQ_NO INTENT_SEQ, a.UTTR, a.ACT, a.SLOT_SEQ_NO
	from intents_intention a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.SEQ_NO = sb.FROM_SEQ_NO where sa.AGENT_SEQ = P_OLD_AGENT_SEQ and sb.AGENT_SEQ = P_NEW_AGENT_SEQ) b 
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
    
    insert into tmp_t_copy 
	select a.SEQ_NO, b.SEQ_NO, 'ii'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM := 0) R where INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_OLD_AGENT_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_intention,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_NEW_AGENT_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    insert into intents_object (INTENT_SEQ, INTENTION_SEQ, OBJECT_VALUE, LIMIT_DA)
	select b.NEW_SEQ_NO INTENT_SEQ, a.INTENTION_SEQ, a.OBJECT_VALUE, LIMIT_DA
	from intents_object a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.AGENT_SEQ = P_OLD_AGENT_SEQ and sb.AGENT_SEQ = P_NEW_AGENT_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;
        
    insert into tmp_t_copy 
	select a.SEQ_NO, b.SEQ_NO, 'io'
	from (
		select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM := 0) R where INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_OLD_AGENT_SEQ) order by SEQ_NO
	) a inner join (
		select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from intents_object,(SELECT @ROWNUM2 := 0) R where INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_NEW_AGENT_SEQ) order by SEQ_NO
	) b
	on a.ROWNUM = b.ROWNUM;
    
    update intents_object a inner join tmp_t_copy b
	on a.INTENTION_SEQ = b.OLD_SEQ_NO
	set a.INTENTION_SEQ = b.NEW_SEQ_NO
	where a.INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_NEW_AGENT_SEQ) and b.C_TYPE = 'ii';
    
    insert into intents_object_detail (INTENT_SEQ, OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE)
	select b.NEW_SEQ_NO INTENT_SEQ, a.OBJECT_SEQ, OBJECT_TYPE, OBJECT_VALUE
	from intents_object_detail a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.AGENT_SEQ = P_OLD_AGENT_SEQ and sb.AGENT_SEQ = P_NEW_AGENT_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b	
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;

	update intents_object_detail a inner join tmp_t_copy b
	on a.OBJECT_SEQ = b.OLD_SEQ_NO
	set a.OBJECT_SEQ = b.NEW_SEQ_NO
	where a.INTENT_SEQ in (select SEQ_NO from intents where AGENT_SEQ = P_NEW_AGENT_SEQ) and b.C_TYPE = 'io';
    
    insert into intents_user_says (SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA, INTENTION)
	select SAY, b.NEW_SEQ_NO INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA, INTENTION
	from intents_user_says a inner join (
		select sa.SEQ_NO OLD_SEQ_NO, sb.SEQ_NO NEW_SEQ_NO from intents sa inner join intents sb
		on sa.AGENT_SEQ = P_OLD_AGENT_SEQ and sb.AGENT_SEQ = P_NEW_AGENT_SEQ and sa.SEQ_NO = sb.FROM_SEQ_NO) b	
	where a.INTENT_SEQ = b.OLD_SEQ_NO
    order by a.SEQ_NO;	
    -- Intents 복사 끝
	
    IF P_COPY_CHECK = 'Y' THEN
		-- Slot 업데이트 및 복사 시작
		-- 자바단에서 만든 slot_class agent_seq 업데이트          
        insert into tmp_t_copy 
		select a.SEQ_NO, b.SEQ_NO, 'sc'
		from (
			select @ROWNUM := @ROWNUM + 1 AS ROWNUM, a.SEQ_NO from slot_class a inner join slot_class b on a.CLASS_NAME = b.CLASS_NAME,(SELECT @ROWNUM := 0) R where a.PROJECT_SEQ = OLD_PROJECT_SEQ and b.AGENT_SEQ = P_NEW_AGENT_SEQ order by a.SEQ_NO
		) a inner join (
			select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from slot_class,(SELECT @ROWNUM2 := 0) R where AGENT_SEQ = P_NEW_AGENT_SEQ order by SEQ_NO
		) b
		on a.ROWNUM = b.ROWNUM;
		
		insert into tmp_t_copy 
		select a.SEQ_NO, b.SEQ_NO, 's'
		from (
			select @ROWNUM := @ROWNUM + 1 AS ROWNUM, a.SEQ_NO from slot a inner join slot_class b on a.CLASS_SEQ_NO = b.SEQ_NO inner join slot_class c on b.CLASS_NAME = c.CLASS_NAME,(SELECT @ROWNUM := 0) R where b.PROJECT_SEQ = OLD_PROJECT_SEQ and c.AGENT_SEQ = P_NEW_AGENT_SEQ order by a.SEQ_NO
		) a inner join (
			select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, a.SEQ_NO from slot a inner join slot_class b on a.CLASS_SEQ_NO = b.SEQ_NO,(SELECT @ROWNUM2 := 0) R where b.AGENT_SEQ = P_NEW_AGENT_SEQ order by a.SEQ_NO
		) b
		on a.ROWNUM = b.ROWNUM;
		
		insert into slot_object(OBJECT_NAME, SLOT_SEQ_NO)
		select OBJECT_NAME, b.NEW_SEQ_NO
		from slot_object a inner join tmp_t_copy b
		on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
		where b.C_TYPE = 's'
		order by a.SEQ_NO;
		
		insert into tmp_t_copy
		select a.SEQ_NO, b.SEQ_NO, 'so'
		from (			            
			select @ROWNUM := @ROWNUM + 1 AS ROWNUM, SEQ_NO from slot_object,(SELECT @ROWNUM := 0) R where SLOT_SEQ_NO IN (select sa.SEQ_NO from slot sa inner join slot_class sb on sa.CLASS_SEQ_NO = sb.SEQ_NO inner join slot_class sc on sb.CLASS_NAME = sc.CLASS_NAME where sb.PROJECT_SEQ = OLD_PROJECT_SEQ and sc.AGENT_SEQ = P_NEW_AGENT_SEQ) order by SEQ_NO
		) a inner join (
			select @ROWNUM2 := @ROWNUM2 + 1 AS ROWNUM, SEQ_NO from slot_object,(SELECT @ROWNUM2 := 0) R where SLOT_SEQ_NO IN (select sa.SEQ_NO from slot sa inner join slot_class sb on sa.CLASS_SEQ_NO = sb.SEQ_NO where sb.AGENT_SEQ = P_NEW_AGENT_SEQ) order by SEQ_NO
		) b
		on a.ROWNUM = b.ROWNUM;
		
        
		insert into slot_object_detail(OBJECT_NAME, PARENT_SEQ_NO, SLOT_SEQ_NO)
		select OBJECT_NAME, PARENT_SEQ_NO, b.NEW_SEQ_NO
		from slot_object_detail a inner join tmp_t_copy b
		on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
		where b.C_TYPE = 's'
		order by a.SEQ_NO;
                
        update slot_object_detail a inner join tmp_t_copy b
        on a.PARENT_SEQ_NO = b.OLD_SEQ_NO
        set a.PARENT_SEQ_NO = b.NEW_SEQ_NO
        where a.SLOT_SEQ_NO IN (select a.SEQ_NO from slot a inner join slot_class b on a.CLASS_SEQ_NO = b.SEQ_NO where b.AGENT_SEQ = P_NEW_AGENT_SEQ) and b.C_TYPE = 'so';
		
		insert slot_type_class (SLOT_SEQ_NO, CLASS_SEQ_NO, TYPE_SEQ_NO)
		select b.NEW_SEQ_NO, CLASS_SEQ_NO, TYPE_SEQ_NO
		from slot_type_class a inner join tmp_t_copy b
		on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
		where b.C_TYPE = 's';
        
		update slot_type_class a inner join tmp_t_copy b
		on a.CLASS_SEQ_NO = b.OLD_SEQ_NO
		set a.CLASS_SEQ_NO = b.NEW_SEQ_NO
		where a.SLOT_SEQ_NO IN (select a.SEQ_NO from slot a inner join slot_class b on a.CLASS_SEQ_NO = b.SEQ_NO where b.AGENT_SEQ = P_NEW_AGENT_SEQ) and b.C_TYPE = 'sc';

		update slot_type_class a inner join tmp_t_copy b
		on a.TYPE_SEQ_NO = b.OLD_SEQ_NO
		set a.TYPE_SEQ_NO = b.NEW_SEQ_NO
		where a.SLOT_SEQ_NO IN (select a.SEQ_NO from slot a inner join slot_class b on a.CLASS_SEQ_NO = b.SEQ_NO where b.AGENT_SEQ = P_NEW_AGENT_SEQ) and b.C_TYPE = 'sc';
		        
		insert into slot_define (PROJECT_SEQ, AGENT_SEQ, SLOT_SEQ_NO, TARGET_SEQ, TARGET_NAME, CON, ACT, SUB_SLOT_SEQ)
		select P_PROJECT_SEQ, P_NEW_AGENT_SEQ, b.NEW_SEQ_NO, a.TARGET_SEQ, a.TARGET_NAME, a.CON, a.ACT, a.SUB_SLOT_SEQ
		from slot_define a inner join tmp_t_copy b
        on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
		where b.C_TYPE = 's'
		order by a.SEQ_NO;
        
		update slot_define a inner join tmp_t_copy b
		on a.TARGET_SEQ = b.OLD_SEQ_NO
		set a.TARGET_SEQ = b.NEW_SEQ_NO
		where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 's';
		
		insert slot_color (PROJECT_SEQ, CLASS_SEQ, SLOT_NAME, COLOR)
		select P_PROJECT_SEQ, NEW_SEQ_NO, SLOT_NAME, COLOR
		from slot_color a inner join tmp_t_copy b
        on a.CLASS_SEQ = b.OLD_SEQ_NO
		WHERE b.C_TYPE = 'sc';	
        
        insert into slot_sub (SLOT_SUB_SEQ, SLOT_SUB_NAME, SLOT_SUB_NAME_SEQ, SLOT_SEQ_NO, CLASS_SEQ_NO, PROJECT_SEQ)
		select concat(SLOT_SUB_SEQ, 'temp'), SLOT_SUB_NAME, SLOT_SUB_NAME_SEQ, SLOT_SEQ_NO, NEW_SEQ_NO, P_PROJECT_SEQ
		from slot_sub a inner join tmp_t_copy b
        on a.CLASS_SEQ_NO = b.OLD_SEQ_NO
		WHERE b.C_TYPE = 'sc';	
        
        update slot_sub a inner join tmp_t_copy b
		on a.SLOT_SEQ_NO = b.OLD_SEQ_NO
		set a.SLOT_SEQ_NO = b.NEW_SEQ_NO 
		where a.PROJECT_SEQ = P_PROJECT_SEQ and b.C_TYPE = 's';
		-- Slot 업데이트 및 복사 끝
		
		-- agent_filling_slot slot 업데이트
		update agent_filling_slot a inner join tmp_t_copy b
		on a.SLOT_SEQ = b.OLD_SEQ_NO
		set a.SLOT_SEQ = b.NEW_SEQ_NO
		where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 's';
		-- agent_filling_slot slot 업데이트 끝
		
		-- reference_class 복사 시작
		insert into reference_class (PROJECT_SEQ, AGENT_SEQ, CLASS_SEQ)
		select P_PROJECT_SEQ, AGENT_SEQ, CLASS_SEQ
		from reference_class
		where AGENT_SEQ = P_OLD_AGENT_SEQ;
		
		update reference_class a inner join tmp_t_copy b
		on a.AGENT_SEQ = b.OLD_SEQ_NO
		set a.AGENT_SEQ = b.NEW_SEQ_NO
		where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 'a';
		
		update reference_class a inner join tmp_t_copy b
		on a.CLASS_SEQ = b.OLD_SEQ_NO
		set a.CLASS_SEQ = b.NEW_SEQ_NO
		where a.AGENT_SEQ = P_NEW_AGENT_SEQ and b.C_TYPE = 'sc';
		-- reference_class 복사 끝
	END IF;
    
    select OLD_SEQ_NO, NEW_SEQ_NO
    from tmp_t_copy    
    where C_TYPE = 's';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_create_port` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_DATE,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_create_port`(START_IDX INT, END_IDX INT)
BEGIN
WHILE START_IDX <= END_IDX DO
insert into ports (SERVER_PORT) values (START_IDX);
set START_IDX = START_IDX + 1;
END WHILE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_delete_domain` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_delete_domain`(P_SEQ_NO INT)
BEGIN	     	
	delete from agent_object_detail where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = P_SEQ_NO);
	delete from agent_object where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = P_SEQ_NO);
	delete from agent_intention where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = P_SEQ_NO);
	delete from agent_filling_slot where AGENT_SEQ in (select SEQ_NO from agents where PROJECT_SEQ = P_SEQ_NO);
	delete from agent_graph where PROJECT_SEQ = P_SEQ_NO;
    delete from agent_monitoring where PROJECT_SEQ = P_SEQ_NO;
    delete from agent_actions where PROJECT_SEQ = P_SEQ_NO;
	delete from agents where PROJECT_SEQ = P_SEQ_NO;
	delete from slot_object_detail where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = P_SEQ_NO);
	delete from slot_object where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = P_SEQ_NO);
	delete from slot_type_class where SLOT_SEQ_NO IN (select SEQ_NO from slot where PROJECT_SEQ = P_SEQ_NO);
	delete from slot_define where PROJECT_SEQ = P_SEQ_NO;
	delete from slot where PROJECT_SEQ = P_SEQ_NO;    
	delete from slot_class where PROJECT_SEQ = P_SEQ_NO;
    delete from slot_color where PROJECT_SEQ = P_SEQ_NO;
    delete from slot_sub where PROJECT_SEQ = P_SEQ_NO;
    delete from slot_mapping where PROJECT_SEQ_NO = P_SEQ_NO;
	delete from intents_object_detail where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = P_SEQ_NO);
	delete from intents_object where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = P_SEQ_NO);
	delete from intents_intention where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = P_SEQ_NO);
	delete from intents_user_says where INTENT_SEQ in (select SEQ_NO from intents where PROJECT_SEQ = P_SEQ_NO);
    delete from intents_replace where PROJECT_SEQ = P_SEQ_NO;
    delete from intents_edit where PROJECT_SEQ = P_SEQ_NO;
	delete from intents where PROJECT_SEQ = P_SEQ_NO;
    delete from intention where PROJECT_SEQ = P_SEQ_NO;
	delete from users_domain where PROJECT_SEQ = P_SEQ_NO;    
	delete from project where SEQ_NO = P_SEQ_NO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_delete_task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_delete_task`(P_SEQ_NO INT)
BEGIN	     
	DROP TEMPORARY TABLE IF EXISTS tmp_task_del;
	CREATE TEMPORARY TABLE tmp_task_del (
		SEQ_NO	INT,
		_TYPE	CHAR(1)
	);
    
    insert into tmp_task_del(SEQ_NO, _TYPE) 
    select b.SEQ_NO, 'S'
    from slot_class a inner join slot b
    on a.SEQ_NO = b.CLASS_SEQ_NO 
    where a.AGENT_SEQ = P_SEQ_NO;
	
    insert into tmp_task_del(SEQ_NO, _TYPE) 
    select b.SEQ_NO, 'C'
    from slot_class a inner join slot_color b
    on a.SEQ_NO = b.CLASS_SEQ
    where a.AGENT_SEQ = P_SEQ_NO;
    
    insert into tmp_task_del(SEQ_NO, _TYPE) 
    select SEQ_NO, 'I'
    from intents
    where AGENT_SEQ = P_SEQ_NO;
    
	delete from agent_object_detail where AGENT_SEQ = P_SEQ_NO;
    delete from agent_object where AGENT_SEQ = P_SEQ_NO;
    delete from agent_intention where AGENT_SEQ = P_SEQ_NO;
    delete from agent_filling_slot where AGENT_SEQ = P_SEQ_NO;
    delete from agent_graph where SOURCE_ID = P_SEQ_NO or TARGET_ID = P_SEQ_NO;
    delete from agent_monitoring where AGENT_SEQ = P_SEQ_NO;
    delete from agent_actions where AGENT_SEQ = P_SEQ_NO;
    delete from agents where SEQ_NO = P_SEQ_NO;        
    delete a from slot_object_detail as a JOIN tmp_task_del b on a.SLOT_SEQ_NO = b.SEQ_NO where _TYPE = 'S';
    delete a from slot_object as a JOIN tmp_task_del b on a.SLOT_SEQ_NO = b.SEQ_NO where _TYPE = 'S';
	delete a from slot_type_class as a JOIN tmp_task_del b on a.SLOT_SEQ_NO = b.SEQ_NO where _TYPE = 'S';
	delete a from slot_define as a JOIN tmp_task_del b on a.SLOT_SEQ_NO = b.SEQ_NO where _TYPE = 'S';
    delete a from slot_color as a JOIN tmp_task_del b on a.SEQ_NO = b.SEQ_NO where _TYPE = 'C';
    delete a from slot_sub as a JOIN tmp_task_del b on a.SLOT_SEQ_NO = b.SEQ_NO where _TYPE = 'S';
	delete a from slot as a JOIN tmp_task_del b on a.SEQ_NO = b.SEQ_NO where _TYPE = 'S';    
	delete from slot_class where AGENT_SEQ = P_SEQ_NO;	
    delete a from intents_object_detail as a JOIN tmp_task_del b on a.INTENT_SEQ = b.SEQ_NO where _TYPE = 'I';
    delete a from intents_object as a JOIN tmp_task_del b on a.INTENT_SEQ = b.SEQ_NO where _TYPE = 'I';
    delete a from intents_intention as a JOIN tmp_task_del b on a.INTENT_SEQ = b.SEQ_NO where _TYPE = 'I';
    delete a from intents_user_says as a JOIN tmp_task_del b on a.INTENT_SEQ = b.SEQ_NO where _TYPE = 'I';
    delete from intents where AGENT_SEQ = P_SEQ_NO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_delete_tutor_domain` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_delete_tutor_domain`(P_SEQ_NO INT)
BEGIN	     		
	delete from tutor_dialog_map where PROJECT_SEQ = P_SEQ_NO;
	delete from tutor_users_domain where PROJECT_SEQ = P_SEQ_NO;
    delete from tutor_project where SEQ_NO = P_SEQ_NO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_insert_basic_intents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_insert_basic_intents`(P_SEQ_NO INT)
BEGIN
	DECLARE NEW_INTENT_SEQ INT;
	insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('affirm', P_SEQ_NO, 0, 'N');
    
    SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
    
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그래', NEW_INTENT_SEQ, '', '그래', '그래', '', '그래', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그래 좋습니다', NEW_INTENT_SEQ, '', '그래 좋습니다', '그래 좋습니다', '', '그래 좋습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그래 좋아', NEW_INTENT_SEQ, '', '그래 좋아', '그래 좋아', '', '그래 좋아', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그래 좋아요', NEW_INTENT_SEQ, '', '그래 좋아요', '그래 좋아요', '', '그래 좋아요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그래요', NEW_INTENT_SEQ, '', '그래요', '그래요', '', '그래요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그렇게 해 주세요', NEW_INTENT_SEQ, '', '그렇게 해 주세요', '그렇게 해 주세요', '', '그렇게 해 주세요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그렇게 해 주십시오', NEW_INTENT_SEQ, '', '그렇게 해 주십시오', '그렇게 해 주십시오', '', '그렇게 해 주십시오', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그렇게 해 줘', NEW_INTENT_SEQ, '', '그렇게 해 줘', '그렇게 해 줘', '', '그렇게 해 줘', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그렇게 해 줘요', NEW_INTENT_SEQ, '', '그렇게 해 줘요', '그렇게 해 줘요', '', '그렇게 해 줘요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그렇게 해줘요', NEW_INTENT_SEQ, '', '그렇게 해줘요', '그렇게 해줘요', '', '그렇게 해줘요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('네', NEW_INTENT_SEQ, '', '네', '네', '', '네', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('네 좋습니다', NEW_INTENT_SEQ, '', '네 좋습니다', '네 좋습니다', '', '네 좋습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('네 좋아요', NEW_INTENT_SEQ, '', '네 좋아요', '네 좋아요', '', '네 좋아요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아 그래', NEW_INTENT_SEQ, '', '아 그래', '아 그래', '', '아 그래', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아 그래요', NEW_INTENT_SEQ, '', '아 그래요', '아 그래요', '', '아 그래요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예', NEW_INTENT_SEQ, '', '예', '예', '', '예', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 그렇게 해 주세요', NEW_INTENT_SEQ, '', '예 그렇게 해 주세요', '예 그렇게 해 주세요', '', '예 그렇게 해 주세요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 좋아요', NEW_INTENT_SEQ, '', '예 좋아요', '예 좋아요', '', '예 좋아요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 할게요', NEW_INTENT_SEQ, '', '예 할게요', '예 할게요', '', '예 할게요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 할께요', NEW_INTENT_SEQ, '', '예 할께요', '예 할께요', '', '예 할께요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 할래요', NEW_INTENT_SEQ, '', '예 할래요', '예 할래요', '', '예 할래요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 합니다', NEW_INTENT_SEQ, '', '예 합니다', '예 합니다', '', '예 합니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 해 주세요', NEW_INTENT_SEQ, '', '예 해 주세요', '예 해 주세요', '', '예 해 주세요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('예 해주세요', NEW_INTENT_SEQ, '', '예 해주세요', '예 해주세요', '', '예 해주세요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응', NEW_INTENT_SEQ, '', '응', '응', '', '응', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 그래', NEW_INTENT_SEQ, '', '응 그래', '응 그래', '', '응 그래', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 그렇게 해줘', NEW_INTENT_SEQ, '', '응 그렇게 해줘', '응 그렇게 해줘', '', '응 그렇게 해줘', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 좋아', NEW_INTENT_SEQ, '', '응 좋아', '응 좋아', '', '응 좋아', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 한다', NEW_INTENT_SEQ, '', '응 한다', '응 한다', '', '응 한다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 할게', NEW_INTENT_SEQ, '', '응 할게', '응 할게', '', '응 할게', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 할께', NEW_INTENT_SEQ, '', '응 할께', '응 할께', '', '응 할께', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 할래', NEW_INTENT_SEQ, '', '응 할래', '응 할래', '', '응 할래', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('응 해줘', NEW_INTENT_SEQ, '', '응 해줘', '응 해줘', '', '응 해줘', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('좋습니다', NEW_INTENT_SEQ, '', '좋습니다', '좋습니다', '', '좋습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('좋아', NEW_INTENT_SEQ, '', '좋아', '좋아', '', '좋아', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('좋아요', NEW_INTENT_SEQ, '', '좋아요', '좋아요', '', '좋아요', '');	
				
    
    insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('negate', P_SEQ_NO, 0, 'N');
	SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
    
    insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것도 아냐', NEW_INTENT_SEQ, '', '그것도 아냐', '그것도 아냐', '', '그것도 아냐', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것도 아뇨', NEW_INTENT_SEQ, '', '그것도 아뇨', '그것도 아뇨', '', '그것도 아뇨', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것도 아니요', NEW_INTENT_SEQ, '', '그것도 아니요', '그것도 아니요', '', '그것도 아니요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것도 아니오', NEW_INTENT_SEQ, '', '그것도 아니오', '그것도 아니오', '', '그것도 아니오', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것도 아닙니다', NEW_INTENT_SEQ, '', '그것도 아닙니다', '그것도 아닙니다', '', '그것도 아닙니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것은 아냐', NEW_INTENT_SEQ, '', '그것은 아냐', '그것은 아냐', '', '그것은 아냐', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것은 아뇨', NEW_INTENT_SEQ, '', '그것은 아뇨', '그것은 아뇨', '', '그것은 아뇨', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것은 아니오', NEW_INTENT_SEQ, '', '그것은 아니오', '그것은 아니오', '', '그것은 아니오', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것은 아니요', NEW_INTENT_SEQ, '', '그것은 아니요', '그것은 아니요', '', '그것은 아니요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그것은 아닙니다', NEW_INTENT_SEQ, '', '그것은 아닙니다', '그것은 아닙니다', '', '그것은 아닙니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그게 아냐', NEW_INTENT_SEQ, '', '그게 아냐', '그게 아냐', '', '그게 아냐', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그게 아뇨', NEW_INTENT_SEQ, '', '그게 아뇨', '그게 아뇨', '', '그게 아뇨', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그게 아니오', NEW_INTENT_SEQ, '', '그게 아니오', '그게 아니오', '', '그게 아니오', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그게 아니요', NEW_INTENT_SEQ, '', '그게 아니요', '그게 아니요', '', '그게 아니요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('그게 아닙니다', NEW_INTENT_SEQ, '', '그게 아닙니다', '그게 아닙니다', '', '그게 아닙니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아냐', NEW_INTENT_SEQ, '', '아냐', '아냐', '', '아냐', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아냐 싫어', NEW_INTENT_SEQ, '', '아냐 싫어', '아냐 싫어', '', '아냐 싫어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아뇨 아뇨', NEW_INTENT_SEQ, '', '아뇨 아뇨', '아뇨 아뇨', '', '아뇨 아뇨', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아뇨 됐습니다', NEW_INTENT_SEQ, '', '아뇨 됐습니다', '아뇨 됐습니다', '', '아뇨 됐습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아뇨 됐어', NEW_INTENT_SEQ, '', '아뇨 됐어', '아뇨 됐어', '', '아뇨 됐어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아뇨 됐어요', NEW_INTENT_SEQ, '', '아뇨 됐어요', '아뇨 됐어요', '', '아뇨 됐어요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니', NEW_INTENT_SEQ, '', '아니', '아니', '', '아니', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 됐습니다', NEW_INTENT_SEQ, '', '아니 됐습니다', '아니 됐습니다', '', '아니 됐습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 됐어', NEW_INTENT_SEQ, '', '아니 됐어', '아니 됐어', '', '아니 됐어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 됐어요', NEW_INTENT_SEQ, '', '아니 됐어요', '아니 됐어요', '', '아니 됐어요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 싫습니다', NEW_INTENT_SEQ, '', '아니 싫습니다', '아니 싫습니다', '', '아니 싫습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 싫어', NEW_INTENT_SEQ, '', '아니 싫어', '아니 싫어', '', '아니 싫어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니 싫어요', NEW_INTENT_SEQ, '', '아니 싫어요', '아니 싫어요', '', '아니 싫어요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니다', NEW_INTENT_SEQ, '', '아니다', '아니다', '', '아니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오', NEW_INTENT_SEQ, '', '아니오', '아니오', '', '아니오', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오 됐습니다', NEW_INTENT_SEQ, '', '아니오 됐습니다', '아니오 됐습니다', '', '아니오 됐습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오 됐어', NEW_INTENT_SEQ, '', '아니오 됐어', '아니오 됐어', '', '아니오 됐어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오 됐어요', NEW_INTENT_SEQ, '', '아니오 됐어요', '아니오 됐어요', '', '아니오 됐어요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오 싫습니다', NEW_INTENT_SEQ, '', '아니오 싫습니다', '아니오 싫습니다', '', '아니오 싫습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니오 싫어', NEW_INTENT_SEQ, '', '아니오 싫어', '아니오 싫어', '', '아니오 싫어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 아니요', NEW_INTENT_SEQ, '', '아니요 아니요', '아니요 아니요', '', '아니요 아니요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 됐습니다', NEW_INTENT_SEQ, '', '아니요 됐습니다', '아니요 됐습니다', '', '아니요 됐습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 됐어', NEW_INTENT_SEQ, '', '아니요 됐어', '아니요 됐어', '', '아니요 됐어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 됐어요', NEW_INTENT_SEQ, '', '아니요 됐어요', '아니요 됐어요', '', '아니요 됐어요', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 싫습니다', NEW_INTENT_SEQ, '', '아니요 싫습니다', '아니요 싫습니다', '', '아니요 싫습니다', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아니요 싫어', NEW_INTENT_SEQ, '', '아니요 싫어', '아니요 싫어', '', '아니요 싫어', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('아닙니다', NEW_INTENT_SEQ, '', '아닙니다', '아닙니다', '', '아닙니다', '');
                
	insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('unknown', P_SEQ_NO, 0, 'N');
END ;;
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `p_insert_basic_intents_english`(P_SEQ_NO INT)
BEGIN
	DECLARE NEW_INTENT_SEQ INT;
	insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('affirm', P_SEQ_NO, 0, 'N');
    
    SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
    
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Yes, I will', NEW_INTENT_SEQ, '', 'Yes, I will', 'Yes, I will', '', 'Yes, I will', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I will', NEW_INTENT_SEQ, '', 'I will', 'I will', '', 'I will', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Yes', NEW_INTENT_SEQ, '', 'Yes', 'Yes', '', 'Yes', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Good', NEW_INTENT_SEQ, '', 'Good', 'Good', '', 'Good', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Great', NEW_INTENT_SEQ, '', 'Great', 'Great', '', 'Great', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I like it', NEW_INTENT_SEQ, '', 'I like it', 'I like it', '', 'I like it', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Wonderful', NEW_INTENT_SEQ, '', 'Wonderful', 'Wonderful', '', 'Wonderful', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Good job', NEW_INTENT_SEQ, '', 'Good job', 'Good job', '', 'Good job', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I see', NEW_INTENT_SEQ, '', 'I see', 'I see', '', 'I see', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I understand', NEW_INTENT_SEQ, '', 'I understand', 'I understand', '', 'I understand', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Yeah', NEW_INTENT_SEQ, '', 'Yeah', 'Yeah', '', 'Yeah', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Very nice', NEW_INTENT_SEQ, '', 'Very nice', 'Very nice', '', 'Very nice', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Very good', NEW_INTENT_SEQ, '', 'Very good', 'Very good', '', 'Very good', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Oh Good', NEW_INTENT_SEQ, '', 'Oh Good', 'Oh Good', '', 'Oh Good', '');	
				
    
    insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('negate', P_SEQ_NO, 0, 'N');
	SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
    
    insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I hate it', NEW_INTENT_SEQ, '', 'I hate it', 'I hate it', '', 'I hate it', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I don\'t agree', NEW_INTENT_SEQ, '', 'I don\'t agree', 'I don\'t agree', '', 'I don\'t agree', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('No', NEW_INTENT_SEQ, '', 'No', 'No', '', 'No', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Never', NEW_INTENT_SEQ, '', 'Never', 'Never', '', 'Never', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('No, I don\'t', NEW_INTENT_SEQ, '', 'No, I don\'t', 'No, I don\'t', '', 'No, I don\'t', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('No I am not', NEW_INTENT_SEQ, '', 'No I am not', 'No I am not', '', 'No I am not', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Nope', NEW_INTENT_SEQ, '', 'Nope', 'Nope', '', 'Nope', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Not that', NEW_INTENT_SEQ, '', 'Not that', 'Not that', '', 'Not that', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Shut up', NEW_INTENT_SEQ, '', 'Shut up', 'Shut up', '', 'Shut up', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('No thanks', NEW_INTENT_SEQ, '', 'No thanks', 'No thanks', '', 'No thanks', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('No thank you', NEW_INTENT_SEQ, '', 'No thank you', 'No thank you', '', 'No thank you', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Never gonna do that', NEW_INTENT_SEQ, '', 'Never gonna do that', 'Never gonna do that', '', 'Never gonna do that', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Not a good idea', NEW_INTENT_SEQ, '', 'Not a good idea', 'Not a good idea', '', 'Not a good idea', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('It will never happen', NEW_INTENT_SEQ, '', 'It will never happen', 'It will never happen', '', 'It will never happen', '');
                
	insert into intents(INTENT_NAME, PROJECT_SEQ, AGENT_SEQ, COPY_FLAG) 
		values ('unknown', P_SEQ_NO, 0, 'N');
	SELECT MAX(SEQ_NO) into NEW_INTENT_SEQ
	FROM intents;
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Talk dirty to me', NEW_INTENT_SEQ, '', 'Talk dirty to me', 'Talk dirty to me', '', 'Talk dirty to me', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I think I\'m crazy', NEW_INTENT_SEQ, '', 'I think I\'m crazy', 'I think I\'m crazy', '', 'I think I\'m crazy', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Roll a die', NEW_INTENT_SEQ, '', 'Roll a die', 'Roll a die', '', 'Roll a die', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Set a 10 minute time', NEW_INTENT_SEQ, '', 'Set a 10 minute time', 'Set a 10 minute time', '', 'Set a 10 minute time', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Should I drink and drive?', NEW_INTENT_SEQ, '', 'Should I drink and drive?', 'Should I drink and drive?', '', 'Should I drink and drive?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('I\'m constipated', NEW_INTENT_SEQ, '', 'I\'m constipated', 'I\'m constipated', '', 'I\'m constipated', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Hey, Cortana', NEW_INTENT_SEQ, '', 'Hey, Cortana', 'Hey, Cortana', '', 'Hey, Cortana', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Have you ever been pregnant?', NEW_INTENT_SEQ, '', 'Have you ever been pregnant?', 'Have you ever been pregnant?', '', 'Have you ever been pregnant?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Get me my money', NEW_INTENT_SEQ, '', 'Get me my money', 'Get me my money', '', 'Get me my money', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Do you want to be on top or bottom?', NEW_INTENT_SEQ, '', 'Do you want to be on top or bottom?', 'Do you want to be on top or bottom?', '', 'Do you want to be on top or bottom?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Are you serious right now?', NEW_INTENT_SEQ, '', 'Are you serious right now?', 'Are you serious right now?', '', 'Are you serious right now?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Am I Batman or Superman?', NEW_INTENT_SEQ, '', 'Am I Batman or Superman?', 'Am I Batman or Superman?', '', 'Am I Batman or Superman?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Can I borrow some money?', NEW_INTENT_SEQ, '', 'Can I borrow some money?', 'Can I borrow some money?', '', 'Can I borrow some money?', '');
	insert into intents_user_says(SAY, INTENT_SEQ, SLOT_SEQ_NO, SAY_TAG, SLOT_TAG, SLOT_NAME, SLOT_MAP, SLOT_MAPDA)
				value('Dance for me', NEW_INTENT_SEQ, '', 'Dance for me', 'Dance for me', '', 'Dance for me', '');
END$$
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TB_BOARD_INSERT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`etridialog2`@`localhost` PROCEDURE `TB_BOARD_INSERT`(
  _TITLE	NVARCHAR(100), 
 _CONTENTS	NVARCHAR(4000), 
 _HIT_CNT	INT, 
 _CREA_ID	VARCHAR(30))
BEGIN
	INSERT INTO TB_BOARD (IDX, TITLE, CONTENTS, HIT_CNT, CREA_ID)
    SELECT IFNULL(MAX(IDX), 0) + 1, _TITLE, _CONTENTS, _HIT_CNT, _CREA_ID 
    FROM TB_BOARD;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_instens_user_say`
--

/*!50001 DROP VIEW IF EXISTS `v_instens_user_say`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`etridialog2`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_instens_user_say` AS select `intents_user_says`.`SEQ_NO` AS `SEQ_NO`,`intents_user_says`.`SLOT_SEQ_NO` AS `SLOT_SEQ_NO`,`intents_user_says`.`INTENT_SEQ` AS `INTENT_SEQ`,`intents_user_says`.`SLOT_TAG` AS `SLOT_TAG`,`intents_user_says`.`INTENTION` AS `INTENTION` from `intents_user_says` where (`intents_user_says`.`SLOT_SEQ_NO` <> '') group by `intents_user_says`.`SLOT_SEQ_NO`,`intents_user_says`.`INTENT_SEQ`,`intents_user_says`.`INTENTION` union all select `intents_user_says`.`SEQ_NO` AS `SEQ_NO`,`intents_user_says`.`SLOT_SEQ_NO` AS `SLOT_SEQ_NO`,`intents_user_says`.`INTENT_SEQ` AS `INTENT_SEQ`,`intents_user_says`.`SAY` AS `SAY`,`intents_user_says`.`INTENTION` AS `INTENTION` from `intents_user_says` where ((`intents_user_says`.`SLOT_SEQ_NO` = '') and (`intents_user_says`.`INTENTION` <> '')) group by `intents_user_says`.`INTENT_SEQ`,`intents_user_says`.`INTENTION` union all select `intents_user_says`.`SEQ_NO` AS `SEQ_NO`,`intents_user_says`.`SLOT_SEQ_NO` AS `SLOT_SEQ_NO`,`intents_user_says`.`INTENT_SEQ` AS `INTENT_SEQ`,`intents_user_says`.`SAY` AS `SAY`,`intents_user_says`.`INTENTION` AS `INTENTION` from `intents_user_says` where ((`intents_user_says`.`SLOT_SEQ_NO` = '') and (`intents_user_says`.`INTENTION` = '')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_slot_object`
--

/*!50001 DROP VIEW IF EXISTS `v_slot_object`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`etridialog2`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_slot_object` AS select `slot_object`.`SEQ_NO` AS `SEQ_NO`,`slot_object`.`OBJECT_NAME` AS `OBJECT_NAME`,`slot_object`.`SLOT_SEQ_NO` AS `SLOT_SEQ_NO` from `slot_object` union all select `slot_object_detail`.`PARENT_SEQ_NO` AS `PARENT_SEQ_NO`,`slot_object_detail`.`OBJECT_NAME` AS `OBJECT_NAME`,`slot_object_detail`.`SLOT_SEQ_NO` AS `SLOT_SEQ_NO` from `slot_object_detail` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_type_list`
--

/*!50001 DROP VIEW IF EXISTS `v_type_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`etridialog2`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_type_list` AS select `slot_class`.`PROJECT_SEQ` AS `PROJECT_SEQ`,'C' AS `SLOT_TYPE`,`slot_class`.`SEQ_NO` AS `SEQ_NO`,`slot_class`.`SEQ_NO` AS `CLASS_SEQ`,`slot_class`.`CLASS_NAME` AS `TYPE_NAME`,'' AS `COLOR`,'C' AS `SUB_SLOT_TYPE`,0 AS `TYPE_SEQ`,'Y' AS `SET_FLAG`,'' AS `SLOT_TYPE_NAME` from `slot_class` union all select `slot`.`PROJECT_SEQ` AS `PROJECT_SEQ`,'O' AS `SLOT_TYPE`,`slot`.`SEQ_NO` AS `SEQ_NO`,`slot`.`CLASS_SEQ_NO` AS `CLASS_SEQ`,`slot`.`SLOT_NAME` AS `TYPE_NAME`,`slot`.`COLOR` AS `COLOR`,`slot`.`slot_type` AS `SUB_SLOT_TYPE`,`slot`.`type_seq` AS `TYPE_SEQ`,`slot`.`SET_FLAG` AS `SET_FLAG`,`slot`.`type_name` AS `SLOT_TYPE_NAME` from `slot` union all select 0 AS `PROJECT_SEQ`,'S' AS `SLOT_TYPE`,`sys_type`.`SEQ_NO` AS `SEQ_NO`,0 AS `CLASS_SEQ`,`sys_type`.`TYPE_NAME` AS `TYPE_NAME`,'' AS `COLOR`,'S' AS `SUB_SLOT_TYPE`,0 AS `TYPE_SEQ`,'Y' AS `SET_FLAG`,'' AS `SLOT_TYPE_NAME` from `sys_type` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17 14:12:32
