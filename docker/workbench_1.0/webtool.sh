#!/bin/bash
# v1.0

# format) docker 외부 전체 경로:내부 경로
volume="
 --volume `pwd`/workspace/webtool:/webtool 
 --volume `pwd`/workspace:/workspace
"

# format) docker 외부 포트:내부 포트
web_port="9080:80"
ssh_port="9022:22"

image=webtool
version="0.1"

run_type="$1"

if [[ "$run_type" == "build" ]] ; then
    docker build --rm --tag $image:$version .
    exit 1
fi

if [[ "$run_type" == "stop" ]] ; then
    docker stop $image
    docker rm $image
    exit 1
fi

# save image
if [[ "$run_type" == "save" ]] ; then
    docker save $image | gzip - > $image.$version.tar.gz
    exit 1
fi

# load image
if [[ "$run_type" == "load" ]] ; then
    docker load < $image.$version.tar.gz
    exit 1
fi

if [[ "$run_type" == "clean" ]] ; then
    docker ps -a
    docker images

    docker ps -a | grep "Exited" | cut -d' ' -f1 | xargs docker rm
    docker images | grep "<none>" | perl -ple 's/\s+/\t/g;' | cut -f3 | xargs docker rmi
    docker images
    exit 1
fi

if [[ "$run_type" == "ssh" ]] ; then
    ssh -p 9022 webtool@localhost

    exit 1
fi

# run as daemon
run_as="--detach --restart always"
cmd=""

# run as terminal
# if [[ "$run_type" == "bash" ]] ; then
#     run_as="-it --rm"
#     cmd="/bin/bash"
# fi

docker run $run_as \
    --hostname webtool \
    -p $web_port -p $ssh_port \
    $volume \
    --name webtool \
    webtool:$version $cmd


# delete docker images

# 1. check docker image id
# $ docker images

# REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
# webtool             0.1                 def2f3cd6bd3        8 minutes ago       1.058 GB

# 2. delete docker image
# $ docker rmi def2f3cd6bd3

