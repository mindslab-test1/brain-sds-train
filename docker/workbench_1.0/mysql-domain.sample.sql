-- MySQL dump 10.13  Distrib 5.5.47, for Linux (x86_64)
--
-- Host: localhost    Database: webtool
-- ------------------------------------------------------
-- Server version	5.5.47-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Test_Weather_renew`
--

DROP TABLE IF EXISTS `Test_Weather_renew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test_Weather_renew` (
  `_idx` text,
  `Weather.city` text,
  `Weather.Gu` text,
  `Weather.date` text,
  `Weather.info` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test_Weather_renew`
--

LOCK TABLES `Test_Weather_renew` WRITE;
/*!40000 ALTER TABLE `Test_Weather_renew` DISABLE KEYS */;
INSERT INTO `Test_Weather_renew` VALUES ('2','서울시','강남구','내일','현재 날씨는 맑고 온도는 18도 입니다. 강수 확률은 11% 이며, 미세먼지농도는 보통 수준입니다.'),('3','서울시','서초구','오늘','현재 날씨는 맑고 온도는 15도 입니다. 강수 확률은 2% 이며, 미세먼지농도는 보통 수준입니다.'),('4','서울시','서초구','내일','현재 날씨는 맑고 온도는 18도 입니다. 강수 확률은 12% 이며, 미세먼지농도는 보통 수준입니다.'),('5','서울시','종로구','오늘','현재 날씨는 맑고 온도는 16도 입니다. 강수 확률은 2% 이며, 미세먼지농도는 보통 수준입니다.'),('6','서울시','종로구','내일','현재 날씨는 맑고 온도는 19도 입니다. 강수 확률은 12% 이며, 미세먼지농도는 보통 수준입니다.'),('7','성남시','분당구','오늘','현재 날씨는 흐리고 온도는 16도 입니다. 강수 확률은 42% 이며, 미세먼지농도는 보통 수준입니다.'),('8','성남시','분당구','내일','현재 날씨는 흐리고 온도는 16도 입니다. 강수 확률은 90% 이며, 미세먼지농도는 보통 수준입니다.'),('1','서울시','강남구','오늘','현재 날씨는 맑고 온도는 20도 입니다. 강수 확률은 1% 이며, 미세먼지농도는 보통 수준입니다.');
/*!40000 ALTER TABLE `Test_Weather_renew` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-17 15:43:05
