

su -

yum groupinstall "Development Tools"
yum install -y make kernel-devel kernel-headers gcc perl bzip2 

yum install -y http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.i686.rpm

yum install -y dkms


export KERN_DIR=/lib/modules/3.10.101-1.el6.elrepo.x86_64

reboot && ./VBoxLinuxAdditions.run



echo 'ejpark ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers



# docker
# http://pyrasis.com/book/DockerForTheReallyImpatient/Chapter02
# https://docs.docker.com/engine/installation/linux/centos/
sudo yum install -y http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
sudo yum install -y docker-io

sudo chkconfig docker on

sudo service docker start

sudo docker run hello-world


sudo groupadd docker
sudo usermod -aG docker ejpark

reboot



sudo mount -t vboxsf -ogid=ejpark,uid=ejpark workspace workspace



# kernel update for docker 1.11 : kernel 2.6 -> 3.10
# http://bicofino.io/2014/10/25/install-kernel-3-dot-10-on-centos-6-dot-5/
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-6-6.el6.elrepo.noarch.rpm
yum --enablerepo=elrepo-kernel install kernel-lt  


# WARN[0001] Your Linux kernel version 2.6.32-573.26.1.el6.x86_64 can be unstable running docker. Please upgrade your kernel to 3.10.0.





wget https://get.docker.com/builds/Linux/x86_64/docker-latest.tgz
tar -xvzf docker-latest.tgz
mv docker/* /usr/bin/








# yum -y install perl-CPAN perl-DBI sshpass libstdc++-devel.i686 libgcc.i686 libcurl-devel.i686 sqlite-devel.i686 glibc-devel.i686 glibc-devel php php-pdo kernel-devel gcc perl




#  - perl 모듈 설치
# sudo perl -MCPAN -e'install "DBD::SQLite"'

#  - perl 모듈 설치 (엑셀 변환이 안될 경우)
# sudo perl -MCPAN -e'install YAML'
# sudo perl -MCPAN -e'install "Spreadsheet::WriteExcel"'

#  - 버클리 디비 4.8 설치
# tar xfz db-4.8.30.NC.tar.gz
# cd db-4.8.30.NC/build_unix/

# export CFLAGS="-m32"
# ../dist/configure
# make
# make install

#  - webtool 사용자 추가
# groupadd webtool
# useradd -g webtool webtool
# passwd webtool 
# # 암호: webtool2012 
# # 암호는 www/webtool/api/env.php 에서 아래와 동일하게 설정
# $_post_data["_SSH_ID_"]   = "webtool";
# $_post_data["_SSH_PASS_"] = "webtool2012";

#  - ssh 포트 122 추가
# vi /etc/ssh/sshd_config 에 아래 두줄 추가

# Port 22
# Port 122

#  - 방화벽에 122, 80 번 포트 추가 

# vi /etc/sysconfig/iptables 아래 두줄 추가

# echo "-A INPUT -m state --state NEW -m tcp -p tcp --dport 8022 -j ACCEPT" >> /etc/sysconfig/iptables
# echo "-A INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT" >> /etc/sysconfig/iptables

 

