#!/usr/bin/env bash

maum_root=${MAUM_ROOT}
if [ -z "${maum_root}" ]; then
  echo 'MAUM_ROOT is not defined!'
  exit 1
fi

test -d ${maum_root} || mkdir -p ${maum_root}

export LD_LIBRARY_PATH=${maum_root}/lib:$LD_LIBRARY_PATH

repo_root=$(pwd)
[ -n $(which nproc) ] && {
  NPROC=$(nproc)
} || {
  NPROC=$(cat /proc/cpuinfo | grep cores | wc -l)
}
echo "[brain-sds-train]" repo_root: ${repo_root}, NPROC: ${NPROC}, MAUM_ROOT: ${MAUM_ROOT}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "${OS}" = "ubuntu" ]; then
    sudo apt-get install make cmake g++ g++-4.8
    sudo apt-get install python-pip python-dev build-essential
  else
    sudo yum install epel-release
    sudo yum install -y gcc gcc-c++ libarchive-devel.x86_64 \
      python-devel.x86_64  glibc.x86_64 autoconf make
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    sudo python get-pip.py
    rm get-pip.py
  fi
  sudo pip install --upgrade pip
  sudo pip install boto3 grpcio==1.9.1 requests numpy theano
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo "Last commit for build.sh: ${sha1}"

# not docker build && update commit
if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo " get_requirements had been done!"
fi

__CC=${CC}
__CXX=${CXX}
if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc
    __CC=/usr/bin/gcc
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++
    __CXX=/usr/bin/g++
  fi
  CMAKE=/usr/bin/cmake3
else
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc-5
    __CC=/usr/bin/gcc-4.8
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++-5
    __CXX=/usr/bin/g++-4.8
  fi
  CMAKE=/usr/bin/cmake
fi

GCC_VER=$(${__CC} -dumpversion)

build_base="build-debug" && [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && build_base="build-deploy-debug"
build_dir=${PWD}/${build_base}-${GCC_VER}

# 다른 프로젝트의 build.sh tar .... 의 명령을 실행할 때 build.sh clean-deploy를 한 적이 있을 경우
# CMakeCache.txt가 예전 MAUM_ROOT(deploy-XXXXX)를 바라보는 문제가 있으므로 빌드 디렉토리를 새로 만든다
function build_sds_train() {
  if [ "$MAUM_BUILD_DEPLOY" = true ]; then
    test -d ${build_dir} && rm -rf ${build_dir}
  fi
  
  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}
  
  ${CMAKE} \
    -DCMAKE_PREFIX_PATH=${maum_root} \
    -DCMAKE_INSTALL_PREFIX=${maum_root} \
    -DCMAKE_C_COMPILER=${__CC} \
    -DCMAKE_CXX_COMPILER=${__CXX} \
    -DCMAKE_BUILD_TYPE=Debug ..
  
  if [ "$1" = "proto" ]; then
    (cd proto && make install -j${NPROC})
    (cd pysrc/proto && make install -j${NPROC})
  else
    if [ "${MAUM_BUILD_DEPLOY}" = "true" ]; then
      make install -j${NPROC}
    else
      make install
    fi
  fi
}

function build_libmaum() {
  git clone git@github.com:mindslabi-ai/libmaum.git temp-libmaum
  cd temp-libmaum
  ./build.sh ${maum_root}
  rm -rf temp-libmaum
}

function check_libmaum() {
  if [ -f ${maum_root}/lib/libmaum-pb.so ]; then
    echo "libmaum has built"
    return 0;
  else
    echo "libmaum has not built, now build libmaum first."
    return 1;
  fi
}

if ! check_libmaum; then
  build_libmaum
fi

build_sds_train


  


